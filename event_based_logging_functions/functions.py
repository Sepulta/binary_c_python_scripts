"""
Event-based logging functions
"""

import os
import re
import pandas as pd

from event_based_logging_functions.event_headers import event_headers_dict


def split_event_types_to_files(input_file, remove_original_file=False):
    """
    Function to split the event types to file per event type
    """

    # Set separator
    separator = "\t"

    # Get dirname
    dirname = os.path.dirname(input_file)

    ####################
    # Loop over the events file to find the unique event names
    unique_events_list = []
    with open(input_file, "r") as f:
        for line in f:
            values = line.strip().split()
            event_type = values[1]

            if not event_type in unique_events_list:
                unique_events_list.append(event_type)

    ####################
    # Construct output filenames
    unique_events_filename_dict = {}
    for unique_event in unique_events_list:
        unique_event_filename = os.path.join(
            dirname, "total_{}_events.dat".format(unique_event)
        )
        unique_events_filename_dict[unique_event] = unique_event_filename

    ####################
    # Create files
    for unique_event, unique_event_filename in unique_events_filename_dict.items():
        with open(unique_event_filename, "w") as unique_event_f:
            unique_event_f.write(
                separator.join(event_headers_dict[unique_event]) + "\n"
            )

    ####################
    # Loop over the total file again and write the events to each of the events files
    for unique_event, unique_event_filename in unique_events_filename_dict.items():
        with open(unique_event_filename, "a") as event_f:
            with open(input_file, "r") as source_f:
                for line in source_f:
                    events_values = line.strip().split()
                    event_type = events_values[1]

                    if event_type == unique_event:
                        # Check if we have the same length of values as to what we expect based on the parameter list
                        if not len(events_values) == len(
                            event_headers_dict[event_type]
                        ):
                            print(
                                "Length of readout values ({}) is not equal to length of parameters ({})".format(
                                    len(events_values),
                                    len(event_headers_dict[event_type]),
                                )
                            )
                            raise ValueError

                        # Write line
                        event_f.write(separator.join(events_values[:]) + "\n")

    ####################
    # Remove original file
    if remove_original_file:
        os.remove(input_file)


def write_events_to_combined_file(target_file, source_file_dict):
    """
    Function to write the events types to a combined file. Here we take the approach of using conventional filehandles and for loops, as this might be quite memory intensive
    """

    is_first_file = True
    separator = "\t"

    # Open target file
    with open(target_file, "w") as target_fh:
        # Loop over the metallicities
        for metallicity in source_file_dict.keys():

            # If this file is the first one to get handled, we want to write the header + metallicity to it.
            with open(source_file_dict[metallicity], "r") as source_fh:
                # Handle header
                if is_first_file:
                    header_line = next(source_fh).strip().split()
                    header_line.append("metallicity")

                    target_fh.write(separator.join(header_line) + "\n")
                else:
                    _ = next(source_fh)

                # Loop over normal lines
                for line in source_fh:
                    values = line.strip().split()

                    # add metallicity value
                    values.append(metallicity)
                    target_fh.write(separator.join(values) + "\n")

            # Set the first_file flag to false
            is_first_file = False


def combine_events_metallicities(population_results_dir, combined_events_dir):
    """
    Function to find all the events files for each event type and combine them
    """

    ##########
    # Loop over all metallicities and find files to readout and match
    metallicity_events_dict = {}
    unique_events = []
    for metallicity_dir in os.listdir(population_results_dir):
        if metallicity_dir.startswith("Z"):
            metallicity = metallicity_dir[1:]
            metallicity_events_dict[metallicity] = {}

            # For each metallicity we want to find all the specific event files and loop over their results and append them to the total file
            for basename_file in os.listdir(
                os.path.join(population_results_dir, metallicity_dir)
            ):
                # regexp match and find the event type
                match = re.match(r"total_(\S+)_events\.dat", basename_file)
                if match:
                    event_type = match.group(1)
                    metallicity_events_dict[metallicity][event_type] = os.path.join(
                        population_results_dir, metallicity_dir, basename_file
                    )

                    if not event_type in unique_events:
                        unique_events.append(event_type)

    ##########
    # Create unique events dict so we can deal with building the combined files on a per-event-type basis
    unique_events_dict = {}
    for unique_event in unique_events:
        unique_events_dict[unique_event] = {}

        for metallicity in metallicity_events_dict.keys():
            if unique_event in metallicity_events_dict[metallicity]:
                unique_events_dict[unique_event][metallicity] = metallicity_events_dict[
                    metallicity
                ][unique_event]

    ##########
    # Loop over files, create file if it doesnt exist
    os.makedirs(combined_events_dir, exist_ok=True)
    for (
        unique_event_type,
        unique_event_type_sourcefile_dict,
    ) in unique_events_dict.items():
        unique_event_type_target_file = os.path.join(
            combined_events_dir,
            "combined_total_{}_events.dat".format(unique_event_type),
        )
        print(unique_event_type_target_file)
        write_events_to_combined_file(
            target_file=unique_event_type_target_file,
            source_file_dict=unique_event_type_sourcefile_dict,
        )


def filter_events_based_on_merging_systems(combined_events_dir, convolution_dir):
    """
    Function to find the events based on the merging systems

    This function can be multiprocessed if we combine it with the previous function and not first make a very big file, but keep it in chunks and use multiprocessing
    """

    ##########
    # Find combined total events files
    total_combined_events_sourcefile_dict = {}
    for combined_basename_file in os.listdir(combined_events_dir):
        match = re.match(r"combined_total_(\S+)_events\.dat", combined_basename_file)
        if match:
            event_type = match.group(1)
            total_combined_events_sourcefile_dict[event_type] = os.path.join(
                combined_events_dir, combined_basename_file
            )

    ##########
    # Open the file containing the convolution results and get the uuid's that merge
    convolution_result_filename = os.path.join(
        convolution_dir, "convolution_results.h5"
    )
    combined_df = pd.read_hdf(convolution_result_filename, "/data/combined_dataframes")
    merging_uuids = set(combined_df["uuid"])

    ##########
    # Loop over the events files and select those that are included in the merging uuid list
    for (
        unique_event_type,
        total_combined_events_sourcefile,
    ) in total_combined_events_sourcefile_dict.items():
        ##########
        # Set up file that contains the events belonging to merging systems
        merging_combined_events_sourcefile = os.path.join(
            convolution_dir, "combined_merging_{}_events.dat".format(unique_event_type)
        )

        ##########
        # open both files and write
        with open(merging_combined_events_sourcefile, "w") as target_fh:
            with open(total_combined_events_sourcefile, "r") as source_fh:
                # write header
                target_fh.write(next(source_fh))

                # Loop over data and check if line matches
                for line in source_fh:
                    values = line.strip().split()

                    if values[0] in merging_uuids:
                        target_fh.write(line)


def add_filtered_events_to_hdf5_file(convolution_dir):
    """
    Function to add filtered events to the hdf5 file
    """

    ##########
    # Find combined merging events files
    combined_filtered_events_sourcefile_dict = {}
    for combined_basename_file in os.listdir(convolution_dir):
        match = re.match(r"combined_merging_(\S+)_events\.dat", combined_basename_file)
        if match:
            event_type = match.group(1)
            combined_filtered_events_sourcefile_dict[event_type] = os.path.join(
                convolution_dir, combined_basename_file
            )

    ##########
    # Open the file containing the convolution results and get the uuid's that merge
    convolution_result_filename = os.path.join(
        convolution_dir, "convolution_results.h5"
    )

    ##########
    # Loop over the resultfiles, read out as pandas
    for (
        unique_event_type,
        combined_filtered_events_sourcefile,
    ) in combined_filtered_events_sourcefile_dict.items():
        event_df = pd.read_csv(combined_filtered_events_sourcefile, sep="\s+")

        # Write to hdf5 file
        event_df.to_hdf(
            convolution_result_filename, key="data/events/{}".format(unique_event_type)
        )
