import os
import numpy as np
import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.extra_plot_functions import (
    place_arrows,
    add_bracket,
)
from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.add_zams_core_mass_shift_schematic_to_axis import (
    add_zams_core_mass_shift_schematic_to_axis,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def plot_zams_core_mass_shift_schematic(
    result_directory_core_mass_shift,
    old_prescription_result_directory_root,
    plot_settings=None,
):
    """
    Function to plot the ZAMS mass to remnant mass with min co_core mass shift
    """

    # Set up figure
    fig = plt.figure(figsize=(20, 12))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=8, ncols=1)

    ax_zams_to_remnant_mass_core_mass_shift = fig.add_subplot(gs[:, 0])

    # Call function to add the plot to the axis
    (
        fig,
        ax_zams_to_remnant_mass_core_mass_shift,
    ) = add_zams_core_mass_shift_schematic_to_axis(
        fig=fig,
        ax_zams_to_remnant_mass_core_mass_shift=ax_zams_to_remnant_mass_core_mass_shift,
        result_directory_core_mass_shift=result_directory_core_mass_shift,
        old_prescription_result_directory_root=old_prescription_result_directory_root,
    )

    # Override the limits if we have input:
    if plot_settings.get("min_xlim", None) is not None:
        ax_zams_to_remnant_mass_core_mass_shift.set_xlim(
            [
                plot_settings["min_xlim"],
                ax_zams_to_remnant_mass_core_mass_shift.get_xlim()[1],
            ]
        )
    if plot_settings.get("max_xlim", None) is not None:
        ax_zams_to_remnant_mass_core_mass_shift.set_xlim(
            [
                ax_zams_to_remnant_mass_core_mass_shift.get_xlim()[0],
                plot_settings["max_xlim"],
            ]
        )
    if plot_settings.get("min_ylim", None) is not None:
        ax_zams_to_remnant_mass_core_mass_shift.set_ylim(
            [
                plot_settings["min_ylim"],
                ax_zams_to_remnant_mass_core_mass_shift.get_ylim()[1],
            ]
        )
    if plot_settings.get("max_ylim", None) is not None:
        ax_zams_to_remnant_mass_core_mass_shift.set_ylim(
            [
                ax_zams_to_remnant_mass_core_mass_shift.get_ylim()[1],
                plot_settings["max_ylim"],
            ]
        )

    # Handle the plotting
    show_and_save_plot(fig, plot_settings)
