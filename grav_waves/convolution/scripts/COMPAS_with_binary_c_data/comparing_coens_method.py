"""
Main function to start running the convolution through COMPAS.

The functions are based on the tutorials from compas
"""

import os
import h5py


from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.convert_binary_c_structure_to_compas_structure import (
    convert_binaryc_data_to_compas_data,
)
from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.run_convolution_compas import (
    run_convolution_compas,
)
from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.run_convolution_binaryc import (
    run_convolution_binaryc,
)


import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors

import numpy as np

from david_phd_functions.plotting.utils import (
    add_plot_info,
    show_and_save_plot,
    align_axes,
)
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

#
from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.plot_functions.plot_mass_quantity_over_redshift import (
    get_data_plot_mass_quantity_over_redshift as get_data_compas,
)
from grav_waves.convolution.functions.plot_routines.plot_convolved_rate_density_per_mass_quantity import (
    get_data as get_data_custom,
)

from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    linestyle_list,
)

import pandas as pd


from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    dco_type_query,
    plot_2d_results,
)


matplotlib.use("tkAgg")


def compare_sfr_and_redshift_and_metallicity(
    binaryc_filename, compas_filename, plot_settings={}
):
    """
    Function to compare the sfr etc things for both datasets
    """

    add_sfr_distributions = True
    add_metallicity_fractions = True
    add_MSSFR_distributions = True
    add_sfr_fraction = True

    ##################
    # Read out binary_c info
    binaryc_file = h5py.File(binaryc_filename)

    #
    binary_c_redshift_bin_centers = binaryc_file["sfr_data/redshift_bin_centers"][()]
    binary_c_redshift_bin_edges = binaryc_file["sfr_data/redshift_bin_edges"][()]
    binary_c_metallicity_bin_centers = binaryc_file["sfr_data/metallicity_bin_centers"][
        ()
    ]
    binary_c_metallicity_bin_edges = binaryc_file["sfr_data/metallicity_bin_edges"][()]

    binary_c_starformation_array = binaryc_file["sfr_data/starformation_array"][()]
    binary_c_metallicity_fraction_array = binaryc_file[
        "sfr_data/metallicity_fraction_array"
    ][()]
    binary_c_metallicity_weighted_starformation_array = binaryc_file[
        "sfr_data/metallicity_weighted_starformation_array"
    ][()][:, 1:-1]

    ##################
    # Read out compas info
    compas_file = h5py.File(compas_filename)

    #
    compas_redshift_bin_centers = compas_file[
        "convolution_settings/redshift_bin_centers"
    ][()]
    compas_redshift_bin_edges = compas_file["convolution_settings/redshift_bin_edges"][
        ()
    ]
    compas_metallicity_bin_centers = compas_file[
        "convolution_settings/metallicity_bin_centers"
    ][()]
    compas_metallicity_bin_edges = compas_file[
        "convolution_settings/metallicity_bin_edges"
    ][()]

    compas_starformation_array = compas_file[
        "convolution_settings/starformation_array"
    ][()]
    compas_metallicity_fraction_array = compas_file[
        "convolution_settings/metallicity_fraction_array"
    ][()]
    compas_metallicity_weighted_starformation_array = compas_file[
        "convolution_settings/metallicity_weighted_starformation_array"
    ][()]

    #######
    # Set up figure
    fig = plt.figure(figsize=(30, 20))

    #
    gs = fig.add_gridspec(nrows=7, ncols=10)

    # Create axes
    ax_compas_hidden = fig.add_subplot(gs[0, 2:8], frame_on=False)
    ax_compas_hidden.set_xticks([])
    ax_compas_hidden.set_yticks([])
    ax_binary_c_hidden = fig.add_subplot(gs[4, 2:8], frame_on=False)
    ax_binary_c_hidden.set_xticks([])
    ax_binary_c_hidden.set_yticks([])

    ax_compas_sfr = fig.add_subplot(gs[0, 2:5])
    ax_binaryc_sfr = fig.add_subplot(gs[4, 2:5])

    ax_compas_sfr.set_xticklabels([])
    ax_binaryc_sfr.set_xticklabels([])

    ax_compas_metallicity_fraction = fig.add_subplot(gs[1:3, 2:5])
    ax_binaryc_metallicity_fraction = fig.add_subplot(gs[5:7, 2:5])

    ax_compas_metallicity_fraction_cb = fig.add_subplot(gs[1:3, 0])
    ax_binaryc_metallicity_fraction_cb = fig.add_subplot(gs[5:7, 0])

    ax_compas_sfr_fraction = fig.add_subplot(gs[0, 5:8])
    ax_binaryc_sfr_fraction = fig.add_subplot(gs[4, 5:8])

    ax_compas_sfr_fraction.yaxis.tick_right()
    ax_binaryc_sfr_fraction.yaxis.tick_right()
    ax_compas_sfr_fraction.yaxis.set_ticks_position("both")
    ax_binaryc_sfr_fraction.yaxis.set_ticks_position("both")

    ax_compas_sfr_fraction.set_xticklabels([])
    ax_binaryc_sfr_fraction.set_xticklabels([])

    ax_compas_MSSFR = fig.add_subplot(gs[1:3, 5:8])
    ax_binaryc_MSSFR = fig.add_subplot(gs[5:7, 5:8])

    ax_compas_MSSFR.yaxis.tick_right()
    ax_binaryc_MSSFR.yaxis.tick_right()

    ax_compas_MSSFR_cb = fig.add_subplot(gs[1:3, -1])
    ax_binaryc_MSSFR_cb = fig.add_subplot(gs[5:7, -1])

    ###############
    # SFR distributions
    if add_sfr_distributions:
        # Plot sfr distribution
        ax_binaryc_sfr.plot(binary_c_redshift_bin_centers, binary_c_starformation_array)
        ax_compas_sfr.plot(compas_redshift_bin_centers, compas_starformation_array)

    ###############
    # metallicity fractions
    if add_metallicity_fractions:
        # Find max values
        min_plot_value_binary_c = binary_c_metallicity_fraction_array[
            binary_c_metallicity_fraction_array > 0
        ].min()
        max_plot_value_binary_c = binary_c_metallicity_fraction_array[
            binary_c_metallicity_fraction_array > 0
        ].max()

        min_plot_value_compas = compas_metallicity_fraction_array[
            compas_metallicity_fraction_array > 0
        ].min()
        max_plot_value_compas = compas_metallicity_fraction_array[
            compas_metallicity_fraction_array > 0
        ].max()

        #
        min_plot_value = np.min([min_plot_value_binary_c, min_plot_value_compas])
        max_plot_value = np.max([max_plot_value_binary_c, max_plot_value_compas])

        norm = colors.Normalize(vmin=min_plot_value, vmax=max_plot_value)

        # Plot metallicity_fraction grids
        X, Y = np.meshgrid(
            binary_c_redshift_bin_centers, binary_c_metallicity_bin_centers
        )

        # plot values
        ax_binaryc_metallicity_fraction.pcolormesh(
            X,
            Y,
            binary_c_metallicity_fraction_array.T,
            norm=norm,
            antialiased=True,
            rasterized=True,
        )
        ax_binaryc_metallicity_fraction.set_yscale("log")
        align_axes(
            fig=fig,
            axes_list=[ax_binaryc_metallicity_fraction, ax_binaryc_sfr],
            which_axis="x",
        )

        # make colorbar
        _ = matplotlib.colorbar.ColorbarBase(
            ax_binaryc_metallicity_fraction_cb,
            norm=norm,
        )

        # Plot metallicity_fraction grids
        X, Y = np.meshgrid(compas_redshift_bin_centers, compas_metallicity_bin_centers)

        # plot values
        ax_compas_metallicity_fraction.pcolormesh(
            X,
            Y,
            compas_metallicity_fraction_array.T,
            norm=norm,
            antialiased=True,
            rasterized=True,
        )
        ax_compas_metallicity_fraction.set_yscale("log")
        align_axes(
            fig=fig,
            axes_list=[ax_compas_metallicity_fraction, ax_compas_sfr],
            which_axis="x",
        )

        # make colorbar
        _ = matplotlib.colorbar.ColorbarBase(
            ax_compas_metallicity_fraction_cb,
            norm=norm,
        )

    ###############
    # sfr fraction
    if add_sfr_fraction:
        ax_binaryc_sfr_fraction.plot(
            binary_c_redshift_bin_centers,
            np.divide(
                np.sum(binary_c_metallicity_weighted_starformation_array, axis=0),
                binary_c_starformation_array,
            ),
        )
        ax_compas_sfr_fraction.plot(
            compas_redshift_bin_centers,
            np.divide(
                np.sum(compas_metallicity_weighted_starformation_array, axis=1),
                compas_starformation_array,
            ),
        )

        ax_compas_sfr_fraction.axhline(1, linestyle="--", alpha=0.25, color="red")
        ax_binaryc_sfr_fraction.axhline(1, linestyle="--", alpha=0.25, color="red")

        #
        ax_compas_sfr_fraction.set_ylim(0, 1.1)
        ax_binaryc_sfr_fraction.set_ylim(0, 1.1)

    ###############
    # MSSFR distributions
    if add_MSSFR_distributions:
        #
        min_plot_value_binary_c = binary_c_metallicity_weighted_starformation_array[
            binary_c_metallicity_weighted_starformation_array > 0
        ].min()
        max_plot_value_binary_c = binary_c_metallicity_weighted_starformation_array[
            binary_c_metallicity_weighted_starformation_array > 0
        ].max()

        min_plot_value_compas = compas_metallicity_weighted_starformation_array[
            compas_metallicity_weighted_starformation_array > 0
        ].min()
        max_plot_value_compas = compas_metallicity_weighted_starformation_array[
            compas_metallicity_weighted_starformation_array > 0
        ].max()

        #
        min_plot_value = np.min([min_plot_value_binary_c, min_plot_value_compas])
        max_plot_value = np.max([max_plot_value_binary_c, max_plot_value_compas])

        norm = colors.Normalize(vmin=min_plot_value, vmax=max_plot_value)

        # Plot metallicity_fraction grids
        X, Y = np.meshgrid(
            binary_c_redshift_bin_centers, binary_c_metallicity_bin_centers
        )

        # plot values
        ax_binaryc_MSSFR.pcolormesh(
            X,
            Y,
            binary_c_metallicity_weighted_starformation_array,
            norm=norm,
            antialiased=True,
            rasterized=True,
        )
        ax_binaryc_MSSFR.set_yscale("log")
        align_axes(
            fig=fig,
            axes_list=[ax_binaryc_MSSFR, ax_binaryc_sfr_fraction],
            which_axis="x",
        )

        # make colorbar
        cbar = matplotlib.colorbar.ColorbarBase(
            ax_binaryc_MSSFR_cb,
            norm=norm,
        )

        # Plot metallicity_fraction grids
        X, Y = np.meshgrid(compas_redshift_bin_centers, compas_metallicity_bin_centers)

        # plot values
        ax_compas_MSSFR.pcolormesh(
            X,
            Y,
            compas_metallicity_weighted_starformation_array.T,
            norm=norm,
            antialiased=True,
            rasterized=True,
        )
        ax_compas_MSSFR.set_yscale("log")
        align_axes(
            fig=fig, axes_list=[ax_compas_MSSFR, ax_compas_sfr_fraction], which_axis="x"
        )

        # make colorbar
        cbar = matplotlib.colorbar.ColorbarBase(
            ax_compas_MSSFR_cb,
            norm=norm,
        )

    # Final makeup
    ax_compas_hidden.set_title("Compas method")
    ax_binary_c_hidden.set_title("binary_c method")

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def compare_total_intrinsic_rate_density_over_redshift(
    compas_filename,
    custom_filename,
    mass_quantity,
    bins,
    include_compas_plot=True,
    include_custom_plot=True,
    plot_settings={},
):
    """
    Function to compare the mass quantity over redshift for the two different convolution methods
    """

    DIFF_MAX_THRESHOLD = 3

    #
    add_contours = False

    #######
    # Set up figure
    fig = plt.figure(figsize=(30, 20))

    #
    gs = fig.add_gridspec(nrows=2, ncols=11)

    # Create axes
    ax_compas = fig.add_subplot(gs[0, 0:10])
    ax_custom = fig.add_subplot(gs[1, 0:10])

    #####################
    # COMPAS
    if include_compas_plot:
        #####################
        # Get compas data
        x_data, y_data, x_bins, weights = get_data_compas(
            compas_filename, mass_quantity
        )

        # calculate the 2-d histogram and find the max value
        pre_hist = np.histogram2d(x_data, y_data, bins=[x_bins, bins], weights=weights)

        bincenters = (x_bins[1:] + x_bins[:-1]) / 2
        total_intrinsic_rate = np.sum(pre_hist[0], axis=1)

        # Plot the data
        ax_compas.plot(bincenters, total_intrinsic_rate)
        ax_compas.set_yscale("log")
        ax_compas.set_xlim(0, 8)
        ax_compas.set_ylim(
            [
                total_intrinsic_rate[bincenters <= 8].min(),
                total_intrinsic_rate[bincenters <= 8].max(),
            ]
        )

    #####################
    # CUSTOM
    if include_custom_plot:
        #####################
        # Get compas data
        x_data, y_data, x_bins, weights = get_data_compas(
            custom_filename, mass_quantity
        )

        # calculate the 2-d histogram and find the max value
        pre_hist = np.histogram2d(x_data, y_data, bins=[x_bins, bins], weights=weights)

        bincenters = (x_bins[1:] + x_bins[:-1]) / 2
        total_intrinsic_rate = np.sum(pre_hist[0], axis=1)

        # Plot the data
        ax_custom.plot(bincenters, total_intrinsic_rate)
        ax_custom.set_yscale("log")
        ax_custom.set_xlim(0, 8)
        ax_custom.set_ylim(
            [
                total_intrinsic_rate[bincenters <= 8].min(),
                total_intrinsic_rate[bincenters <= 8].max(),
            ]
        )

    ##########
    # Make up

    # Align axes
    align_axes(fig=fig, axes_list=[ax_compas, ax_custom], which_axis="x")

    # Add labels
    ax_compas.set_ylabel(r"rate Intrinsic [dN/dGpc3/dYr]")
    ax_custom.set_ylabel(r"rate Intrinsic [dN/dGpc3/dYr]")

    ax_custom.set_xlabel("Redshift [z]")

    #
    ax_compas.set_title("COMPAS convolution")
    ax_custom.set_title("CUSTOM convolution")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def compare_mass_quantity_over_redshift(
    compas_filename,
    custom_filename,
    mass_quantity,
    bins,
    include_compas_plot=True,
    include_custom_plot=True,
    plot_settings={},
):
    """
    Function to compare the mass quantity over redshift for the two different convolution methods
    """

    DIFF_MAX_THRESHOLD = 3

    #
    add_contours = True

    #######
    # Set up figure
    fig = plt.figure(figsize=(30, 20))

    #
    gs = fig.add_gridspec(nrows=2, ncols=11)

    # Create axes
    ax_compas = fig.add_subplot(gs[0, 0:10])
    ax_compas_cb = fig.add_subplot(gs[0, 10])

    ax_custom = fig.add_subplot(gs[1, 0:10])
    ax_custom_cb = fig.add_subplot(gs[1, 10])

    #####################
    # COMPAS
    if include_compas_plot:
        #####################
        # Get compas data
        x_data, y_data, x_bins, weights = get_data_compas(
            compas_filename, mass_quantity
        )

        # calculate the 2-d histogram and find the max value
        pre_hist = np.histogram2d(x_data, y_data, bins=[x_bins, bins], weights=weights)
        global_max_val = np.max(pre_hist[0])

        # Determine the norm
        norm = matplotlib.colors.LogNorm(
            vmin=10 ** (np.log10(global_max_val) - DIFF_MAX_THRESHOLD),
            vmax=global_max_val,
        )

        #####################
        # plot compas data

        # Plot density
        _ = ax_compas.hist2d(
            x_data, y_data, bins=[x_bins, bins], weights=weights, norm=norm
        )

        # make colorbar
        cbar = matplotlib.colorbar.ColorbarBase(
            ax_compas_cb, cmap=matplotlib.cm.viridis, norm=norm, extend="min"
        )
        cbar.ax.set_ylabel("rate Intrinsic [dN/dGpc3/dYr]")

        # Add contourlevels
        if add_contours:
            contourlevels = [
                10 ** (np.log10(global_max_val) - 3),
                10 ** (np.log10(global_max_val) - 2),
                10 ** (np.log10(global_max_val) - 1),
            ]

            x_bins_centers = (x_bins[1:] + x_bins[:-1]) / 2
            bins_centers = (bins[1:] + bins[:-1]) / 2

            _ = ax_compas.contour(
                x_bins_centers,
                bins_centers,
                pre_hist[0].T,
                levels=contourlevels,
                colors="k",
                linestyles=linestyle_list,
            )

            # Set lines on the colorbar
            for contour_i, _ in enumerate(contourlevels):
                cbar.ax.plot(
                    [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
                    [contourlevels[contour_i]] * 2,
                    "black",
                    linestyle=linestyle_list[contour_i],
                )

    #####################
    # CUSTOM
    if include_custom_plot:
        #####################
        # Get compas data
        x_data, y_data, x_bins, weights = get_data_compas(
            custom_filename, mass_quantity
        )

        # calculate the 2-d histogram and find the max value
        pre_hist = np.histogram2d(x_data, y_data, bins=[x_bins, bins], weights=weights)
        global_max_val = np.max(pre_hist[0])

        # Determine the norm
        norm = matplotlib.colors.LogNorm(
            vmin=10 ** (np.log10(global_max_val) - DIFF_MAX_THRESHOLD),
            vmax=global_max_val,
        )

        #####################
        # plot compas data

        # Plot density
        _ = ax_custom.hist2d(
            x_data, y_data, bins=[x_bins, bins], weights=weights, norm=norm
        )

        # make colorbar
        cbar = matplotlib.colorbar.ColorbarBase(
            ax_custom_cb, cmap=matplotlib.cm.viridis, norm=norm, extend="min"
        )
        cbar.ax.set_ylabel("rate Intrinsic [dN/dGpc3/dYr]")

        # Add contourlevels
        if add_contours:
            contourlevels = [
                10 ** (np.log10(global_max_val) - 3),
                10 ** (np.log10(global_max_val) - 2),
                10 ** (np.log10(global_max_val) - 1),
            ]

            x_bins_centers = (x_bins[1:] + x_bins[:-1]) / 2
            bins_centers = (bins[1:] + bins[:-1]) / 2

            _ = ax_custom.contour(
                x_bins_centers,
                bins_centers,
                pre_hist[0].T,
                levels=contourlevels,
                colors="k",
                linestyles=linestyle_list,
            )

            # Set lines on the colorbar
            for contour_i, _ in enumerate(contourlevels):
                cbar.ax.plot(
                    [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
                    [contourlevels[contour_i]] * 2,
                    "black",
                    linestyle=linestyle_list[contour_i],
                )

    ##########
    # Make up

    # Align axes
    align_axes(fig=fig, axes_list=[ax_compas, ax_custom], which_axis="x")
    align_axes(fig=fig, axes_list=[ax_compas, ax_custom], which_axis="y")

    # Add labels
    ax_compas.set_ylabel(r"Primary mass [M$_{\odot}$]")
    ax_custom.set_ylabel(r"Primary mass [M$_{\odot}$]")

    ax_custom.set_xlabel("Redshift [z]")

    #
    ax_compas.set_title("COMPAS convolution")
    ax_custom.set_title("CUSTOM convolution")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


## Setting
sim_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED"
dco_type = "combined_dco"

# Targets
target_dir_restructure = os.path.join(sim_dir, "population_results_compas_structure")
target_filename_restructure = os.path.join(
    target_dir_restructure, "{}_compas_structure.hdf5".format(dco_type)
)

#
target_dir_convolution = os.path.join(
    os.getcwd(), "results/convolution_comparison_compas_coen"
)
target_filename_convolution_coen = os.path.join(
    target_dir_convolution,
    "convolved_{}_compas_structure_coens_method.hdf5".format(dco_type),
)
target_filename_compas_convolution_binaryc = os.path.join(
    target_dir_convolution,
    "convolved_{}_compas_structure_binaryc_method.hdf5".format(dco_type),
)

#
combined_dataset_filename_binaryc = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/output/binary_c_convolution/convolution/without_detection_probability/combined_dataset.h5"
convolution_binaryc_filename = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/output/binary_c_convolution/convolution/without_detection_probability/convolution_results.h5"

# Create directories
os.makedirs(target_dir_restructure, exist_ok=True)
os.makedirs(target_dir_convolution, exist_ok=True)

#
convert_data_to_compas_structure = False
run_compas_convolution_coen = False
run_compas_convolution_binaryc = False
run_binaryc_convolution = True

# Take original data and write to new structure
if convert_data_to_compas_structure:
    convert_binaryc_data_to_compas_data(
        input_filename=combined_dataset_filename_binaryc,
        target_filename=target_filename_restructure,
    )

# Take the new structure dataset and run the convolution
if run_compas_convolution_coen:
    run_convolution_compas(
        source_filename=target_filename_restructure,
        target_filename=target_filename_convolution_coen,
    )

# Take the new structure dataset and run the convolution but with the weights from binary_c and stuff
if run_compas_convolution_binaryc:
    run_convolution_compas(
        source_filename=target_filename_restructure,
        target_filename=target_filename_compas_convolution_binaryc,
        use_binary_c_weight_method=True,
        binary_c_weights_column="number_per_solar_mass",
    )

# Run the convolution with binary-c
if run_binaryc_convolution:
    run_convolution_binaryc()


#
binaryc_filename = convolution_binaryc_filename
compas_filename = target_filename_compas_convolution_binaryc


def compare_merger_rate_output(binaryc_filename, compas_filename, plot_settings={}):
    """
    Function to compare the merger rate output of the two codes
    """

    plot_total_rate_distributions = True
    plot_rate_redshift_vs_quantity_distributions = True
    plot_rate_redshift_vs_quantity_distributions_fraction = True

    #####################
    # Get binary_c data
    binaryc_combined_dataframe = pd.read_hdf(
        binaryc_filename, "/data/combined_dataframes"
    )

    # Create DCO query
    bhbh_query = dco_type_query("bhbh")
    dco_mask = binaryc_combined_dataframe.eval(bhbh_query)

    # Open file
    binaryc_file = h5py.File(binaryc_filename)

    # Readout merger rate array
    binaryc_merger_rate_array, redshifts = readout_rate_array(
        datafile=binaryc_file, rate_key="merger_rate", mask=dco_mask, max_redshift=9
    )

    # Get metallicity info
    metallicities_binaryc = binaryc_combined_dataframe["metallicity"].to_numpy()[
        dco_mask
    ]

    # Get some settings
    binary_c_metallicity_bin_centers = binaryc_file["sfr_data/metallicity_bin_centers"][
        ()
    ]
    binary_c_metallicity_bin_edges = binaryc_file["sfr_data/metallicity_bin_edges"][()]

    #
    metallicity_bins = binary_c_metallicity_bin_edges

    # calculate the hist based on the metallicity bins
    binaryc_hist = histogram_with_2d_weights(
        metallicities_binaryc, metallicity_bins, binaryc_merger_rate_array
    )

    # ##################
    # # Read out compas info
    compas_file = h5py.File(compas_filename)

    stellar_type_1 = compas_file["BSE_Double_Compact_Objects/Stellar_Type(1)"][()]
    stellar_type_2 = compas_file["BSE_Double_Compact_Objects/Stellar_Type(2)"][()]

    stellar_type_1_bh = stellar_type_1 == 14
    stellar_type_2_bh = stellar_type_2 == 14

    dco_mask = stellar_type_1_bh * stellar_type_2_bh

    metallicities_compas = compas_file["BSE_System_Parameters/Metallicity@ZAMS(1)"][()]
    metallicities_compas = metallicities_compas[dco_mask]

    #
    compas_redshift_bin_centers = compas_file[
        "convolution_settings/redshift_bin_centers"
    ][()]
    compas_redshift_bin_edges = compas_file["convolution_settings/redshift_bin_edges"][
        ()
    ]
    compas_metallicity_bin_centers = compas_file[
        "convolution_settings/metallicity_bin_centers"
    ][()]
    compas_metallicity_bin_edges = compas_file[
        "convolution_settings/metallicity_bin_edges"
    ][()]

    # Read out the rate array
    compas_merger_rate_data = compas_file["convolution_data"]["merger_rate"][()]
    compas_merger_rate_data = compas_merger_rate_data[:90, :]
    compas_redshift_bin_centers = compas_redshift_bin_centers[:90]

    #
    compas_hist = histogram_with_2d_weights(
        metallicities_compas, metallicity_bins, compas_merger_rate_data
    )

    #######
    # Set up figure
    fig = plt.figure(figsize=(30, 20))

    #
    gs = fig.add_gridspec(nrows=7, ncols=10)

    # Create axes
    ax_compas_hidden = fig.add_subplot(gs[0, 2:8], frame_on=False)
    ax_compas_hidden.set_xticks([])
    ax_compas_hidden.set_yticks([])
    ax_binary_c_hidden = fig.add_subplot(gs[4, 2:8], frame_on=False)
    ax_binary_c_hidden.set_xticks([])
    ax_binary_c_hidden.set_yticks([])

    ax_compas_total_rate = fig.add_subplot(gs[0, 2:5])
    ax_binaryc_total_rate = fig.add_subplot(gs[4, 2:5])

    ax_compas_total_rate.set_xticklabels([])
    ax_binaryc_total_rate.set_xticklabels([])

    ax_compas_rate_per_quantity = fig.add_subplot(gs[1:3, 2:5])
    ax_binaryc_rate_per_quantity = fig.add_subplot(gs[5:7, 2:5])

    ax_compas_rate_per_quantity_cb = fig.add_subplot(gs[1:3, 0])
    ax_binaryc_rate_per_quantity_cb = fig.add_subplot(gs[5:7, 0])

    ax_compas_rate_per_quantity_fraction = fig.add_subplot(gs[1:3, 5:8])
    ax_binaryc_rate_per_quantity_fraction = fig.add_subplot(gs[5:7, 5:8])

    ax_compas_rate_per_quantity_fraction.yaxis.tick_right()
    ax_binaryc_rate_per_quantity_fraction.yaxis.tick_right()

    ax_compas_rate_per_quantity_fraction_cb = fig.add_subplot(gs[1:3, -1])
    ax_binaryc_rate_per_quantity_fraction_cb = fig.add_subplot(gs[5:7, -1])

    ################
    # Plot total rate distributions
    if plot_total_rate_distributions:
        ax_binaryc_total_rate.plot(redshifts, np.sum(binaryc_hist, axis=1))
        ax_compas_total_rate.plot(
            compas_redshift_bin_centers, np.sum(compas_hist, axis=1)
        )

        #
        ax_binaryc_total_rate.set_yscale("log")
        ax_compas_total_rate.set_yscale("log")

    ################
    # Plot rate per redshift per quantity evolution
    if plot_rate_redshift_vs_quantity_distributions:
        # Calculate norm for scaling
        min_val_hists = np.min(
            [
                np.min(compas_hist[compas_hist != 0]),
                np.min(binaryc_hist[binaryc_hist != 0]),
            ]
        )
        max_val_hists = np.max([np.max(compas_hist), np.max(binaryc_hist)])

        norm = colors.LogNorm(
            vmin=10
            ** np.floor(
                np.log10(max_val_hists) - plot_settings.get("probability_floor", 4)
            ),
            vmax=max_val_hists,
        )

        # binary_c plot
        X, Y = np.meshgrid(compas_redshift_bin_centers, compas_metallicity_bin_centers)

        plot_2d_results(
            fig=fig,
            axes_dict={
                "all_axis": ax_compas_rate_per_quantity,
                "colorbar_axis": ax_compas_rate_per_quantity_cb,
            },
            X=X,
            Y=Y,
            add_ratio=False,
            y_scale="log",
            results_dict={"all": compas_hist.T},
            norm=norm,
        )

        # binary_c plot
        X, Y = np.meshgrid(redshifts, binary_c_metallicity_bin_centers)

        plot_2d_results(
            fig=fig,
            axes_dict={
                "all_axis": ax_binaryc_rate_per_quantity,
                "colorbar_axis": ax_binaryc_rate_per_quantity_cb,
            },
            X=X,
            Y=Y,
            add_ratio=False,
            y_scale="log",
            results_dict={"all": binaryc_hist.T},
            norm=norm,
        )

        #
        ax_binaryc_rate_per_quantity.set_xlabel("Redshift [z]")
        ax_binaryc_rate_per_quantity.set_ylabel("Metallicity")

        ax_binaryc_rate_per_quantity_cb.yaxis.set_label_position("left")
        ax_binaryc_rate_per_quantity_cb.set_ylabel(r"Rate")

        ax_compas_rate_per_quantity.set_xlabel("Redshift [z]")
        ax_compas_rate_per_quantity.set_ylabel("Metallicity")

        ax_compas_rate_per_quantity_cb.yaxis.set_label_position("left")
        ax_compas_rate_per_quantity_cb.set_ylabel(r"Rate")

        for tick in ax_compas_rate_per_quantity_cb.yaxis.get_majorticklabels():
            tick.set_horizontalalignment("left")

        for tick in ax_binaryc_rate_per_quantity_cb.yaxis.get_majorticklabels():
            tick.set_horizontalalignment("left")

    ################
    # Plot rate per redshift per quantity vs total rate disttributions
    if plot_rate_redshift_vs_quantity_distributions_fraction:
        total_rate_compas_hist = np.sum(compas_hist, axis=1)
        fraction_compas_hist = compas_hist / total_rate_compas_hist[:, np.newaxis]

        total_rate_binaryc_hist = np.sum(binaryc_hist, axis=1)
        fraction_binaryc_hist = binaryc_hist / total_rate_binaryc_hist[:, np.newaxis]

        # Calculate norm for scaling
        min_val_hists = np.min(
            [
                np.min(fraction_compas_hist[fraction_compas_hist != 0]),
                np.min(fraction_binaryc_hist[fraction_binaryc_hist != 0]),
            ]
        )
        max_val_hists = np.max(
            [np.max(fraction_compas_hist), np.max(fraction_binaryc_hist)]
        )

        norm_ratio = colors.Normalize(vmin=0, vmax=max_val_hists)

        # binary_c plot
        X, Y = np.meshgrid(compas_redshift_bin_centers, compas_metallicity_bin_centers)

        plot_2d_results(
            fig=fig,
            axes_dict={
                "all_axis": ax_compas_rate_per_quantity_fraction,
                "colorbar_axis": ax_compas_rate_per_quantity_fraction_cb,
            },
            X=X,
            Y=Y,
            add_ratio=False,
            y_scale="log",
            results_dict={"all": fraction_compas_hist.T},
            norm=norm_ratio,
        )

        # binary_c plot
        X, Y = np.meshgrid(redshifts, binary_c_metallicity_bin_centers)

        plot_2d_results(
            fig=fig,
            axes_dict={
                "all_axis": ax_binaryc_rate_per_quantity_fraction,
                "colorbar_axis": ax_binaryc_rate_per_quantity_fraction_cb,
            },
            X=X,
            Y=Y,
            add_ratio=False,
            y_scale="log",
            results_dict={"all": fraction_binaryc_hist.T},
            norm=norm_ratio,
        )

        #
        ax_binaryc_rate_per_quantity_fraction.yaxis.set_label_position("right")
        ax_binaryc_rate_per_quantity_fraction.set_xlabel("Redshift [z]")
        ax_binaryc_rate_per_quantity_fraction.set_ylabel("Metallicity")
        ax_binaryc_rate_per_quantity_fraction_cb.set_ylabel(r"Fraction to total rate")

        ax_compas_rate_per_quantity_fraction.yaxis.set_label_position("right")
        ax_compas_rate_per_quantity_fraction.set_xlabel("Redshift [z]")
        ax_compas_rate_per_quantity_fraction.set_ylabel("Metallicity")
        ax_compas_rate_per_quantity_fraction_cb.set_ylabel(r"Fraction to total rate")

    align_axes(
        fig=fig,
        axes_list=[ax_compas_total_rate, ax_compas_rate_per_quantity],
        which_axis="x",
    )
    align_axes(
        fig=fig,
        axes_list=[ax_binaryc_total_rate, ax_binaryc_rate_per_quantity],
        which_axis="x",
    )

    align_axes(
        fig=fig,
        axes_list=[ax_compas_rate_per_quantity, ax_compas_rate_per_quantity_fraction],
        which_axis="y",
    )
    align_axes(
        fig=fig,
        axes_list=[ax_binaryc_rate_per_quantity, ax_binaryc_rate_per_quantity_fraction],
        which_axis="y",
    )

    # Final makeup
    ax_compas_hidden.set_title("Compas method")
    ax_binary_c_hidden.set_title("binary_c method")

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


#
compare_merger_rate_output(
    binaryc_filename,
    compas_filename,
    plot_settings={"show_plot": True, "output_name": "convolution_comparison.pdf"},
)

# compare_sfr_and_redshift_and_metallicity(binaryc_filename, compas_filename, plot_settings={'output_name': 'sfr_comparison.pdf'})
# compare_total_intrinsic_rate_density_over_redshift(target_filename_convolution_coen, target_filename_convolution_binaryc, 'primary_mass', np.arange(0, 70, 1), plot_settings={'show_plot': True})
# compare_mass_quantity_over_redshift(target_filename_convolution_coen, target_filename_convolution_binaryc, 'primary_mass', np.arange(0, 70, 1), plot_settings={'show_plot': True})
