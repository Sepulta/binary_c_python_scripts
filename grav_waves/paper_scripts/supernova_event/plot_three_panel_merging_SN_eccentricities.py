"""
Plot routine to plot the pre and post eccentricity of merging systems that have undergone a PPISN supernova
"""

import os
import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

from grav_waves.functions.events import merge_with_event_dataframe, print_rows_and_cols
from grav_waves.paper_scripts.primary_mass_distribution.plot_utils import (
    get_rate_data,
)


custom_mpl_settings.load_mpl_rc()
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def get_vmin(hist, custom_min):
    """
    Function to get the minimum value of a histogram OR the custom min if thats
    """

    hist_min = np.min(hist[hist > 0])

    return np.max([custom_min, hist_min])


def plot_three_panel_merging_SN_eccentricities(
    combined_dataframes, rate_array, sn_event_dataframe, plot_settings={}
):
    """
    Example function to combine the DCO data with supernova events and
    """

    ###########
    # Handle data
    merged_sn_df = merge_with_event_dataframe(
        main_dataframe=combined_dataframes, event_dataframe=sn_event_dataframe
    )
    print_rows_and_cols(merged_sn_df, "merged_sn_df")

    merged_sn_df["rates"] = rate_array[merged_sn_df["initial_indices"].to_numpy()]
    ppisn_SN = merged_sn_df.query("SN_type == 21")

    # Set bins
    numbins = 25
    x_bins = np.linspace(0, 1, numbins)
    y_bins = np.linspace(0, 1, numbins)

    #
    all_SN_hists = []
    first_SN_hists = []
    second_SN_hists = []

    # Select PPISN and those that are bound
    bound_PPISN_SN = ppisn_SN.query("pre_SN_ecc >= 0")

    # Select first and second
    first_PPISN_SN = bound_PPISN_SN.query("SN_counter == 1")
    second_PPISN_SN = bound_PPISN_SN.query("SN_counter == 2")

    # Get all PPISN SN values
    all_SN_hist, _, _ = np.histogram2d(
        bound_PPISN_SN["pre_SN_ecc"],
        bound_PPISN_SN["post_SN_ecc"],
        bins=[x_bins, y_bins],
        weights=bound_PPISN_SN["probability"],
    )

    # First PPISN
    first_SN_hist, _, _ = np.histogram2d(
        first_PPISN_SN["pre_SN_ecc"],
        first_PPISN_SN["post_SN_ecc"],
        bins=[x_bins, y_bins],
        weights=first_PPISN_SN["probability"],
    )

    # Second PPISN
    second_SN_hist, _, _ = np.histogram2d(
        second_PPISN_SN["pre_SN_ecc"],
        second_PPISN_SN["post_SN_ecc"],
        bins=[x_bins, y_bins],
        weights=second_PPISN_SN["probability"],
    )

    ######
    # Set norm
    second_max_val = sorted(all_SN_hist.flatten())[-2]
    norm = colors.LogNorm(
        vmin=get_vmin(all_SN_hist, plot_settings.get("min_prob_val", 1e-8)),
        vmax=second_max_val,
    )

    ##########
    # Set up figure logic
    fig = plt.figure(figsize=(26, 8))
    gs = fig.add_gridspec(nrows=1, ncols=10)

    all_SN_axis = fig.add_subplot(gs[:, 0:3])
    first_SN_axis = fig.add_subplot(gs[:, 3:6])
    second_SN_axis = fig.add_subplot(gs[:, 6:9])

    ax_cb = fig.add_subplot(gs[:, -1:])

    # make bincenters
    x_bincenters = (x_bins[1:] + x_bins[:-1]) / 2
    y_bincenters = (y_bins[1:] + y_bins[:-1]) / 2

    X, Y = np.meshgrid(x_bincenters, y_bincenters)

    # All SN
    _ = all_SN_axis.pcolormesh(
        X,
        Y,
        all_SN_hist.T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # First SN
    _ = first_SN_axis.pcolormesh(
        X,
        Y,
        first_SN_hist.T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # Second SN
    _ = second_SN_axis.pcolormesh(
        X,
        Y,
        second_SN_hist.T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(ax_cb, norm=norm, extend="both")
    cbar.ax.set_ylabel(r"Number per formed solar mass")

    ###
    # Make-up
    first_SN_axis.set_xlabel("Pre-SN eccentricity")
    all_SN_axis.set_ylabel("Post-SN eccentricity")

    first_SN_axis.set_yticklabels([])
    second_SN_axis.set_yticklabels([])

    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #######
    # Settings
    min_prob_val = 1e-12
    show_plot = False
    testing = False

    #######
    # Result root
    result_root = "/home/david/data_projects/binary_c_data/GRAV_WAVES/server_results/"

    # Plot output dir
    output_dir = os.path.join(
        this_file_dir,
        "plots/EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED",
    )

    ######
    # Get the dataframes
    # file = "/home/david/data_projects/binary_c_data/GRAV_WAVES/server_results/EVENTS_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5"
    file = "/home/david/data_projects/binary_c_data/GRAV_WAVES/server_results/EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5"

    # Load DCO dataframe
    combined_dataframes = pd.read_hdf(file, key="data/combined_dataframes")

    # Load rate array for merging at Z=0
    rate_array = get_rate_data(
        file,
        rate_type="merger_rate",
        redshift_value=0,
        mask=np.ones(len(combined_dataframes.index), dtype=bool),
    )[0]

    # Filter
    # combined_dataframes = combined_dataframes[combined_dataframes.mass_1 < 8]

    # Select Supernovae
    sn_event_dataframe = pd.read_hdf(file, key="data/events/SN_BINARY")

    # Call to plot routine
    basename = "merging_SN_pre_post_eccentricity.pdf"
    plot_three_panel_merging_SN_eccentricities(
        combined_dataframes=combined_dataframes,
        rate_array=rate_array,
        sn_event_dataframe=sn_event_dataframe,
        plot_settings={
            "min_prob_val": min_prob_val,
            "show_plot": show_plot,
            "output_name": os.path.join(output_dir, basename),
        },
    )
