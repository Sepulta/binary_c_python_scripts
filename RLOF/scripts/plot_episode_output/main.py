"""
main function for the plotting of the
"""

import os

from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict
from RLOF.scripts.plot_episode_output.general_plot_function import (
    general_plot_routine as general_episode_plot_routine,
)

simulation_dir = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions"
rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(simulation_dir)

# Readout the dataset dict
rlof_episode_plots_combined_filename = os.path.join("test.pdf")
verbose = 1
rlof_episode_plots_dir = "output/"

# Run the plot routine
general_episode_plot_routine(
    rlof_episode_dataset_dict=rlof_episode_dataset_dict,
    output_dir=rlof_episode_plots_dir,
    combined_output_pdf=rlof_episode_plots_combined_filename,
    verbose=verbose,
)
