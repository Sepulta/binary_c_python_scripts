"""
Extra functions for the population scripts
"""

import os
import time
import json
import pandas as pd

from binarycpython.utils.ensemble import binaryc_json_serializer

from ensemble_functions.ensemble_functions import (
    inflate_ensemble_with_lists,
    find_columnames_recursively,
)
from binarycpython.utils.grid_options_defaults import moe_di_stefano_default_options


def extract_MS_accretor_data_ensemble_df(input_ensemble_df):
    """
    Function to read out an ensemble df and extract the MS-only data
    """

    dirname = os.path.dirname(input_ensemble_df)
    basename = os.path.basename(input_ensemble_df)
    new_basename = "stable_MS_{}".format(basename)
    output_ensemble_df = os.path.join(dirname, new_basename)

    # Readout
    df = pd.read_csv(input_ensemble_df, header=0, sep="\s+")
    ms_df = df[df.st_acc == 1]
    del ms_df["st_acc"]
    stable_ms_df = ms_df[ms_df.stable == 0]
    del stable_ms_df["stable"]

    # write
    stable_ms_df.to_csv(output_ensemble_df, header=0, sep="\t")

    return output_ensemble_df


def split_off_ensemble_projects(ensemble_filename):
    """
    Function to load the ensemble data and split off the sub projects of the ensemble

    Writes to the same directory
    """

    output_filenames = []
    dirname = os.path.dirname(ensemble_filename)

    #
    ensemble_data = json.load(open(ensemble_filename, "r"))

    # Loop over all subprojects
    for subproject_key in ensemble_data["ensemble"].keys():
        # Extract data and anme
        sub_project = ensemble_data["ensemble"][subproject_key]
        sub_project_name = "subproject_{}_ensemble_data.json".format(subproject_key)
        full_path_sub_project_name = os.path.join(dirname, sub_project_name)

        # Write to file
        with open(full_path_sub_project_name, "w") as f:
            f.write(json.dumps(sub_project))
        output_filenames.append(full_path_sub_project_name)

    # # RLOF project
    # RLOF_sub_project = ensemble_data["ensemble"]["RLOF"]
    # RLOF_sub_project_name = "RLOF_ensemble_data.json"
    # full_path_RLOF_sub_project_name = os.path.join(dirname, RLOF_sub_project_name)

    # with open(full_path_RLOF_sub_project_name, "w") as f:
    #     f.write(json.dumps(RLOF_sub_project))
    # output_filenames.append(full_path_RLOF_sub_project_name)

    return output_filenames


def convert_ensemble_to_dataframe(ensemble_filename, verbose=0):
    """
    Function to convert the ensembles to dataframes and write them to a csv file
    """

    dirname = os.path.dirname(ensemble_filename)
    basename = os.path.basename(ensemble_filename)
    output_name = basename[:-4] + "csv"
    full_path_output_name = os.path.join(dirname, output_name)

    # Open ensemble file
    ensemble_data = json.load(open(ensemble_filename, "r"))

    # Convert to dataframe
    if verbose:
        print("Converting ensemble data to dataframe")
        start = time.time()
    try:
        columnames = find_columnames_recursively(ensemble_data)
    except:
        columnames = find_columnames_recursively({"ensemble": ensemble_data})
    data_list = inflate_ensemble_with_lists(ensemble_data)

    df = pd.DataFrame(data_list)
    df = df.transpose()
    df.columns = columnames + ["probability"]
    if verbose:
        print("\tTook {}s".format(time.time() - start))

    # write to file
    if verbose:
        print("Writing dataframe to file")
        start = time.time()
    df.to_csv(full_path_output_name, sep="\t", header=True, index=False)
    if verbose:
        print("\tTook {}s".format(time.time() - start))

    return full_path_output_name


def handle_ensemble_results(
    population_object, simname, metallicity, inflate_ensembles=True
):
    """
    Function to handle writing the ensemble results
    """

    #################
    # Get ensemble output and write it to file
    ensemble_output = population_object.grid_ensemble_results
    ensemble_output_filename = os.path.join(
        population_object.custom_options["data_dir"], "ensemble_output.json"
    )

    ensemble_output["metadata"]["simname"] = simname
    ensemble_output["metadata"]["metallicity"] = metallicity

    # Write the metadata
    with open(
        os.path.join(population_object.custom_options["data_dir"], "metadata.json"), "w"
    ) as f:
        json.dump(ensemble_output["metadata"], f, default=binaryc_json_serializer)

    # Write the total ensemble
    with open(ensemble_output_filename, "w") as f:
        json.dump(ensemble_output, f, default=binaryc_json_serializer)

    # rewrite the chunks to be smaller
    for chunk_file in [
        os.path.join(population_object.custom_options["data_dir"], el)
        for el in os.listdir(population_object.custom_options["data_dir"])
        if el.startswith("ensemble_output_")
    ]:
        print("compacting chunk file {}".format(chunk_file))

        try:
            # Read out
            with open(chunk_file, "r") as f:
                chunk_file_data = json.loads(f.read())

            # Write back
            with open(chunk_file, "w") as output_f:
                output_f.write(json.dumps(chunk_file_data))
        except:
            print("something went wrong when re-reading and re-writing the chunks")

    #################
    # Split total ensemble into the sub-projects
    print("Splitting ensemble in sub-projects")
    sub_project_ensemble_filenames = split_off_ensemble_projects(
        ensemble_output_filename
    )

    #################
    # Convert sub-project ensembles to dataframes
    for sub_project_ensemble_filename in sub_project_ensemble_filenames:

        if not "general_info" in os.path.basename(sub_project_ensemble_filename):
            print(
                "Converting {} to dataframe and csv".format(
                    sub_project_ensemble_filename
                )
            )

            if inflate_ensembles:
                dataframe_name = convert_ensemble_to_dataframe(
                    sub_project_ensemble_filename
                )

                if "RLOF" in os.path.basename(sub_project_ensemble_filename):
                    #################
                    # Split off dataframes into queried results
                    stable_ms_dataframe_name = extract_MS_accretor_data_ensemble_df(
                        dataframe_name
                    )

    return population_object


def add_distribution_grid_variables(
    population_object, variation_dict, resolution, MS_population_range=None
):
    """
    Function to add the distribution variables
    """

    #
    if variation_dict["distributions"] == "normal":
        population_object.add_grid_variable(
            name="lnm1",
            longname="Primary mass",
            valuerange=[0.1, 120],
            samplerfunc="const(math.log(0.1), math.log(120), {})".format(
                resolution["M_1"]
            ),
            precode="M_1=math.exp(lnm1)",
            probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 121, -1.3, -2.3, -2.3)*M_1",
            dphasevol="dlnm1",
            parameter_name="M_1",
            condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
        )

        population_object.add_grid_variable(
            name="q",
            longname="Mass ratio",
            valuerange=["0.1/M_1", 1],
            samplerfunc="const(0.1/M_1, 1, {})".format(resolution["q"]),
            probdist="flatsections(q, [{'min': 0.1/M_1, 'max': 1.0, 'height': 1}])",
            dphasevol="dq",
            precode="M_2 = q * M_1",
            parameter_name="M_2",
            condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
        )

        population_object.add_grid_variable(
            name="log10per",  # in days
            longname="log10(Orbital_Period)",
            valuerange=[0.15, 5.5],
            samplerfunc="const(0.15, 5.5, {})".format(resolution["per"]),
            precode="""orbital_period = 10** log10per
sep = calc_sep_from_period(M_1, M_2, orbital_period)
sep_min = calc_sep_from_period(M_1, M_2, 10**0.15)
sep_max = calc_sep_from_period(M_1, M_2, 10**5.5)""",
            probdist="sana12(M_1, M_2, sep, orbital_period, sep_min, sep_max, math.log10(10**0.15), math.log10(10**5.5), -0.55)",
            parameter_name="orbital_period",
            dphasevol="dlog10per",
        )
    elif variation_dict["distributions"] == "ms":
        population_object.Moe_di_Stefano_2017(
            options={
                "multiplicity_modulator": [0, 1, 0, 0],
                "resolutions": {
                    "M": [resolution["M_1"], resolution["q"], 0, 0],
                    "logP": [resolution["per"], 0, 0],
                    "ecc": [resolution["e"], 0, 0],
                },
                "ranges": MS_population_range
                if MS_population_range
                else moe_di_stefano_default_options["ranges"],
            }
        )

    return population_object
