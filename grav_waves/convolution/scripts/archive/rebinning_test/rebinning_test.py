"""
Script to test the rebinning
"""

import h5py
import numpy as np

from grav_waves.settings import convolution_settings, config_dict_cosmology

#
input_dataset_filename = "../hdf5_output_rewrite/output/convolution_without_detector_probability_results/convolution_without_detection_probability.h5"
output_dataset_filename = "../hdf5_output_rewrite/output/convolution_without_detector_probability_results/rebinned_convolution_without_detection_probability.h5"

stepsize = 0.05
new_bins = np.arange(
    0 - 0.5 * stepsize,
    (convolution_settings["max_interpolation_redshift"] - 1) + 0.5 * stepsize,
    stepsize,
)

# convolution_rebin_redshift(
#     input_dataset_filename,
#     output_dataset_filename,
#     new_redshift_bins=new_bins
# )

hdf5_file = h5py.File(output_dataset_filename, "r")
rate_data = hdf5_file["data"]

# from grav_waves.convolution.functions.plot_routines.plot_convolved_rate_density_per_metallicity import plot_convolved_rate_density_per_metallicity


# contourlevels=[1e-1, 1e0, 1e1, 1e2]
# linestyles_contourlevels=['solid', 'dashed', '-.', ':']
# plot_convolved_rate_density_per_metallicity(
#     output_dataset_filename,
#     rate_type='merger_rate',
#     querylist=[{'name': 'channel_1', 'query': 'comenv_counter >= 1'}, {'name': 'channel_2', 'query': 'comenv_counter==0'}],
#     add_ratio=True,
#     plot_settings={
#         'output_name': 'plot_convolved_rate_density_per_mass_ratio.pdf',
#         'show_plot': True,
#         'antialiased': True,
#         'rasterized': True,
#     },
#     contourlevels=contourlevels,
#     linestyles_contourlevels=linestyles_contourlevels,

# )


mass_type = "chirpmass"
mass_bins = np.arange(0, 50, 1)
from grav_waves.convolution.functions.plot_routines.plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity import (
    plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity,
)


contourlevels = [1e-1, 1e0, 1e1, 1e2]
linestyles_contourlevels = ["solid", "dashed", "-.", ":"]
plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity(
    output_dataset_filename,
    rate_type="merger_rate",
    querylist=[
        {"name": "channel_1", "query": "comenv_counter >= 1"},
        {"name": "channel_2", "query": "comenv_counter==0"},
    ],
    add_ratio=True,
    mass_type=mass_type,
    mass_bins=mass_bins,
    plot_settings={
        "output_name": "plot_convolved_rate_density_per_mass_ratio.pdf",
        "show_plot": True,
        "antialiased": True,
        "rasterized": True,
    },
    contourlevels=contourlevels,
    linestyles_contourlevels=linestyles_contourlevels,
)


quit()
from grav_waves.convolution.functions.plot_routines.plot_convolved_rate_density_per_mass_ratio import (
    plot_convolved_rate_density_per_mass_ratio,
)


# contourlevels=[1e-1, 1e0, 1e1, 1e2]
# linestyles_contourlevels=['solid', 'dashed', '-.', ':']
# plot_convolved_rate_density_per_mass_ratio(
#     output_dataset_filename,
#     rate_type='merger_rate',
#     querylist=[{'name': 'channel_1', 'query': 'comenv_counter >= 1'}, {'name': 'channel_2', 'query': 'comenv_counter==0'}],
#     add_ratio=True,
#     plot_settings={
#         'output_name': 'plot_convolved_rate_density_per_mass_ratio.pdf',
#         'show_plot': True,
#         'antialiased': True,
#         'rasterized': True,
#     },
#     contourlevels=contourlevels,
#     linestyles_contourlevels=linestyles_contourlevels,

# )

# plot_rate_density_at_zero_redshift
from grav_waves.convolution.functions.plot_routines.plot_massratio_density_at_zero_redshift import (
    plot_massratio_density_at_zero_redshift,
)

plot_massratio_density_at_zero_redshift(
    convolution_dataset_hdf5_filename=output_dataset_filename,
    divide_by_binsize=False,
    rate_type="merger_rate",
    add_cdf=True,
    add_ratio=True,
    querylist=[
        {"name": "channel_1", "query": "comenv_counter >= 1"},
        {"name": "channel_2", "query": "comenv_counter==0"},
    ],
    plot_settings={
        "show_plot": True,
        "output_name": "plot_massratio_density_at_zero_redshift.pdf",
        "antialiased": True,
        "rasterized": True,
    },
)

#####
# # total merger rate plot
# from grav_waves.convolution.functions.plot_routines.plot_total_intrinsic_rate_density_at_all_lookbacks import (
#     plot_total_intrinsic_rate_density_at_all_lookbacks,
# )
# plot_total_intrinsic_rate_density_at_all_lookbacks(
#     output_dataset_filename,
#     rate_type=rate_type,
#     querylist=[{'name': 'channel_1', 'query': 'comenv_counter >= 1'}, {'name': 'channel_2', 'query': 'comenv_counter==0'}],
#     add_cdf=True,
#     add_ratio=True,
#     plot_settings={
#         'output_name': 'plot_total_intrinsic_rate_density_at_all_lookbacks_test.pdf',
#         'show_plot': True,
#         'antialiased': True,
#         'rasterized': True,
#     }
# )

#####
# plot_rate_density_at_zero_redshift
from grav_waves.convolution.functions.plot_routines.plot_rate_density_at_zero_redshift import (
    plot_rate_density_at_zero_redshift,
)

# # convolution_dataset_hdf5_filename, mass_quantity, mass_bins, rate_type, querylist=None, divide_by_binsize=True, add_ratio=False, add_cdf=False, plot_settings=None
# plot_rate_density_at_zero_redshift(
#     convolution_dataset_hdf5_filename=output_dataset_filename,
#     mass_quantity=mass_type,
#     mass_bins=mass_bins,
#     divide_by_binsize=divide_by_binsize,
#     rate_type=rate_type,
#     add_cdf=True,
#     add_ratio=True,
#     querylist=[{'name': 'channel_1', 'query': 'comenv_counter >= 1'}, {'name': 'channel_2', 'query': 'comenv_counter==0'}],
#     plot_settings={
#         'show_plot': True,
#         'output_name': 'plot_rate_density_at_zero_redshift.pdf',
#         'antialiased': True,
#         'rasterized': True,
#     }
# )

#####
# plot_convolved_rate_density_per_mass_quantity
from grav_waves.convolution.functions.plot_routines.plot_convolved_rate_density_per_mass_quantity import (
    plot_convolved_rate_density_per_mass_quantity,
)

# plot_convolved_rate_density_per_mass_quantity(
#     convolution_dataset_hdf5_filename=output_dataset_filename,
#     rate_type=rate_type,
#     mass_type=mass_type,
#     mass_bins=mass_bins,
#     divide_by_binsize=divide_by_binsize,
#     querylist=[{'name': 'channel_1', 'query': 'comenv_counter >= 1'}, {'name': 'channel_2', 'query': 'comenv_counter==0'}],
#     contourlevels=[1e-1, 1e0, 1e1, 1e2],
#     linestyles_contourlevels=['solid', 'dashed', '-.', ':'],
#     plot_settings={
#         'show_plot': True,
#         'output_name': "plot_convolved_rate_density_per_mass_quantity.pdf",
#         'antialiased': True,
#         'rasterized': True,
#     }
# )

# convolution_settings['global_query'] = '(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter >= 1)'
# convolution_settings['global_query'] = '(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter==0)'

# from grav_waves.gw_analysis.functions.general_plot_function import general_plot_function as general_individual_datasets_plot_function

# individual_datasets_plot_output_pdf = 'individual_datasets_plots.pdf'
# general_individual_datasets_plot_function(
#     dataset_dict_populations,

#     result_dir=dataset_dict_populations['population_result_dir'],
#     plot_dir=dataset_dict_populations['population_plot_dir'],

#     type_key='bhbh',
#     combined_pdf_output_name=individual_datasets_plot_output_pdf,
#     convolution_settings=convolution_settings
# )
# quit()
# convolution_settings['global_query'] = '(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter >= 1)'
# individual_datasets_plot_output_pdf = 'individual_datasets_plots_channel_1.pdf'
# general_individual_datasets_plot_function(
#     dataset_dict_populations,

#     result_dir=dataset_dict_populations['population_result_dir'],
#     plot_dir=dataset_dict_populations['population_plot_dir'],

#     type_key='bhbh',
#     combined_pdf_output_name=individual_datasets_plot_output_pdf,
#     convolution_settings=convolution_settings
# )

# convolution_settings['global_query'] = '(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter==0)'
# individual_datasets_plot_output_pdf = 'individual_datasets_plots_channel_2.pdf'
# general_individual_datasets_plot_function(
#     dataset_dict_populations,

#     result_dir=dataset_dict_populations['population_result_dir'],
#     plot_dir=dataset_dict_populations['population_plot_dir'],

#     type_key='bhbh',
#     combined_pdf_output_name=individual_datasets_plot_output_pdf,
#     convolution_settings=convolution_settings
# )
# quit()
# Plot the combined pdf
