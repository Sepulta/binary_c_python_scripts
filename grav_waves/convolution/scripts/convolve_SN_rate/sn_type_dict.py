SN_type_dict = {
    0: "None",
    1: "IA_He",
    2: "IA_ELD",
    3: "IA_CHAND",
    4: "AIC",
    5: "ECAP",
    6: "IA_He_Coal",
    7: "IA_CHAND_Coal",
    8: "NS_NS",
    9: "GRB_COLLAPSAR",
    10: "HeStarIa",
    11: "IBC",
    12: "II",
    13: "IIa",
    14: "WDKICK",
    15: "TZ",
    16: "AIC_BH",
    17: "BH_BH",
    18: "BH_NS",
    19: "IA_Hybrid_HeCOWD",
    20: "IA_Hybrid_HeCOWD_subluminous",
    21: "PPISN",
    22: "PISN",
    23: "PHDIS",
}
