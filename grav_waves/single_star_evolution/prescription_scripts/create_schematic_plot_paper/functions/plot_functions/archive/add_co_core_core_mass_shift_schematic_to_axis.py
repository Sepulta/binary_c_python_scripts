import os
import numpy as np
import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.extra_plot_functions import (
    place_arrows,
    add_bracket,
)
from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_utils import (
    add_pre_SN_mass_plot,
    add_no_prescription_plot,
    add_old_prescription_plot,
    add_fiducial_plot,
    add_mass_removal_plot,
    add_core_mass_shift_plot,
    get_df_bh,
    calculate_min_max_zams_ppisn_masses,
    add_makeup_plot,
    get_dataframes,
)


from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def add_co_core_core_mass_shift_schematic_to_axis(
    fig,
    ax_co_core_mass_to_remnant_mass_core_mass_shift,
    result_directory_core_mass_shift,
    old_prescription_result_directory_root,
    add_old_prescription_line=True,
    add_mass_removed_mass_removal=True,
    add_fiducial_mass_removal=True,
    create_shaded_regions=True,
    create_supernova_lines=True,
):
    """
    Function to plot the CO core mass vs the remnant mass schematic plot for CO core mass shift
    """

    # Settings
    alpha_regions = 0.2

    # Get metallicity values
    metallicity_value = float(
        os.path.basename(result_directory_core_mass_shift).split("_Z")[-1]
    )

    ########################
    # Load old prescription data
    old_prescription_result_dir = os.path.join(
        old_prescription_result_directory_root, "0"
    )

    #
    old_prescription_datafile = os.path.join(old_prescription_result_dir, "output.dat")
    old_prescription_df = pd.read_table(old_prescription_datafile, sep="\s+")
    old_prescription_df = old_prescription_df.sort_values(by="zams_mass")

    # Select BHs only
    old_prescription_df = old_prescription_df[old_prescription_df.stellar_type == 14]

    ########################
    # Load core mass shift data
    core_mass_shift_fiducial_dir = os.path.join(result_directory_core_mass_shift, "0")
    core_mass_shift_dirs = {
        subdir: os.path.join(result_directory_core_mass_shift, subdir)
        for subdir in os.listdir(result_directory_core_mass_shift)
        if not subdir == "0"
    }

    #
    core_mass_shift_fiducial_datafile = os.path.join(
        core_mass_shift_fiducial_dir, "output.dat"
    )
    core_mass_shift_fiducial_df = pd.read_table(
        core_mass_shift_fiducial_datafile, sep="\s+"
    )
    core_mass_shift_fiducial_df = core_mass_shift_fiducial_df.sort_values(
        by="zams_mass"
    )

    #
    core_mass_shift_datafile = os.path.join(
        core_mass_shift_dirs[list(core_mass_shift_dirs.keys())[0]], "output.dat"
    )
    core_mass_shift_df = pd.read_table(core_mass_shift_datafile, sep="\s+")
    core_mass_shift_df = core_mass_shift_df.sort_values(by="zams_mass")

    # select only BHs
    core_mass_shift_fiducial_df = core_mass_shift_fiducial_df[
        core_mass_shift_fiducial_df.stellar_type == 14
    ]
    core_mass_shift_df = core_mass_shift_df[core_mass_shift_df.stellar_type == 14]

    # Get max mass
    zams_mass_for_max_ppisn = core_mass_shift_fiducial_df.loc[
        core_mass_shift_fiducial_df["mass"].idxmax()
    ]["zams_mass"]
    fiducial_max_mass_ppisn = core_mass_shift_fiducial_df[
        core_mass_shift_fiducial_df.zams_mass == zams_mass_for_max_ppisn
    ]["mass"].to_numpy()[0]

    #########
    # Plot the core mass shift subplot

    # Add old_prescription line
    if add_old_prescription_line:
        ax_co_core_mass_to_remnant_mass_core_mass_shift.plot(
            old_prescription_df["pre_sn_co_core_mass"],
            old_prescription_df["mass"],
            linestyle="-.",
            alpha=0.8,
            label="Farmer et al. '19 prescription",
        )

    ################
    # Core mass shift plot
    if add_mass_removed_mass_removal:
        ax_co_core_mass_to_remnant_mass_core_mass_shift.plot(
            core_mass_shift_df["pre_sn_co_core_mass"],
            core_mass_shift_df["mass"],
            linestyle="--",
            alpha=0.8,
            label=r"New prescription - $\Delta$ $M_{\mathrm{Extra\ mass\ loss}}$",
        )

    #########
    # Add fiducial values
    if add_fiducial_mass_removal:
        ax_co_core_mass_to_remnant_mass_core_mass_shift.plot(
            core_mass_shift_fiducial_df["pre_sn_co_core_mass"],
            core_mass_shift_fiducial_df["mass"],
            # 'ro'
            color="black",
            label="New prescription",
        )

    ##############
    # Create shaded region under the main line that indicates which prescription
    if create_shaded_regions:
        # Get mass first ppisn
        first_ppisn_mass = core_mass_shift_df[core_mass_shift_df.sn_type == 21].iloc[0][
            "pre_sn_co_core_mass"
        ]
        fryer_area_df = core_mass_shift_df[
            core_mass_shift_df.pre_sn_co_core_mass < first_ppisn_mass
        ].copy(deep=True)
        new_prescription_area = core_mass_shift_df[
            core_mass_shift_df.pre_sn_co_core_mass >= first_ppisn_mass
        ].copy(deep=True)

        #
        ax_co_core_mass_to_remnant_mass_core_mass_shift.fill_between(
            fryer_area_df["pre_sn_co_core_mass"],
            fryer_area_df["mass"],
            y2=0,
            color="yellow",
            alpha=alpha_regions,
        )
        ax_co_core_mass_to_remnant_mass_core_mass_shift.fill_between(
            new_prescription_area["pre_sn_co_core_mass"],
            new_prescription_area["mass"],
            y2=0,
            color="pink",
            alpha=alpha_regions,
        )

    #################
    # Add lines on the top to indicate which supernova type
    if create_supernova_lines:
        y_value_sn_mechanism_line = fiducial_max_mass_ppisn + 2.5

        ##
        # PPISN
        ppisn_area = core_mass_shift_df[
            core_mass_shift_df.pre_sn_co_core_mass >= first_ppisn_mass
        ][core_mass_shift_df.stellar_type == 14]

        min_ppisn_zams = ppisn_area["pre_sn_co_core_mass"].to_numpy().min()
        max_ppisn_zams = ppisn_area["pre_sn_co_core_mass"].to_numpy().max()
        size_between_ppisn = max_ppisn_zams - min_ppisn_zams
        midpoint_ppisn = (max_ppisn_zams + min_ppisn_zams) / 2

        #
        fig, ax_co_core_mass_to_remnant_mass_core_mass_shift = place_arrows(
            fig,
            ax_co_core_mass_to_remnant_mass_core_mass_shift,
            midpoint_ppisn,
            y_value_sn_mechanism_line,
            size_between_ppisn,
            text="pulsational pair-instability + core-collapse",
        )

        #################
        # Plot the CC SN

        below_ppisn_area = core_mass_shift_df[
            core_mass_shift_df.pre_sn_co_core_mass < min_ppisn_zams
        ]

        min_below_ppisn_zams = below_ppisn_area["pre_sn_co_core_mass"].to_numpy().min()
        max_below_ppisn_zams = below_ppisn_area["pre_sn_co_core_mass"].to_numpy().max()

        size_between_below_ppisn = max_below_ppisn_zams - min_below_ppisn_zams
        midpoint_below_ppisn = (max_below_ppisn_zams + min_below_ppisn_zams) / 2

        #
        fig, ax_co_core_mass_to_remnant_mass_core_mass_shift = place_arrows(
            fig,
            ax_co_core_mass_to_remnant_mass_core_mass_shift,
            midpoint_below_ppisn,
            y_value_sn_mechanism_line,
            size_between_below_ppisn,
            text="core-collapse",
        )

    ###########
    # # Add bracket indicating the extra PPISN loss
    # core_mass_shift_add_bracket_extra_mass_loss = True
    # if core_mass_shift_add_bracket_extra_mass_loss:
    #     # Get max value for ppisn
    #     zams_mass_for_max_ppisn = mass_removal_fiducial_df.loc[mass_removal_fiducial_df['mass'].idxmax()]['zams_mass']
    #     fiducial_max_mass_ppisn = mass_removal_fiducial_df[mass_removal_fiducial_df.zams_mass==zams_mass_for_max_ppisn]['mass'].to_numpy()[0]
    #     mass_removed_max_mass_ppisn = mass_removal_df[mass_removal_df.zams_mass==zams_mass_for_max_ppisn]['mass'].to_numpy()[0]

    #     location_max_fiducial_mass = (zams_mass_for_max_ppisn, fiducial_max_mass_ppisn)
    #     location_max_mass_removal_mass = (zams_mass_for_max_ppisn, mass_removed_max_mass_ppisn)
    #     location_startpoint = (zams_mass_for_max_ppisn+25, (fiducial_max_mass_ppisn+mass_removed_max_mass_ppisn)/2)
    #     fig, ax_zams_to_remnant_mass_mass_removal = add_bracket(
    #         fig,
    #         ax_zams_to_remnant_mass_mass_removal,
    #         location_max_fiducial_mass,
    #         location_max_mass_removal_mass,
    #         location_startpoint,
    #         -2,
    #         linestyle='solid',
    #         color='black',
    #         linewidth=5
    #     )
    #     ax_zams_to_remnant_mass_mass_removal.text(
    #         location_startpoint[0]+5,
    #         location_startpoint[1]-1,
    #         s=r"$\Delta$ $M_{\mathrm{Extra\ mass\ loss}}$",
    #         fontsize=20
    #     )

    #################
    # Make up

    #
    ax_co_core_mass_to_remnant_mass_core_mass_shift.set_xlim(
        0, core_mass_shift_fiducial_df["pre_sn_co_core_mass"].max() + 10
    )
    ax_co_core_mass_to_remnant_mass_core_mass_shift.set_ylim(
        0, fiducial_max_mass_ppisn + 10
    )

    # Configure axes etc
    (
        handles,
        labels,
    ) = ax_co_core_mass_to_remnant_mass_core_mass_shift.get_legend_handles_labels()
    ax_co_core_mass_to_remnant_mass_core_mass_shift.legend(
        handles[::-1], labels[::-1], loc="lower left", bbox_to_anchor=(0.01, 0.5)
    )

    #
    ax_co_core_mass_to_remnant_mass_core_mass_shift.set_xlabel(
        r"CO core mass [M$_{\odot}$]"
    )
    ax_co_core_mass_to_remnant_mass_core_mass_shift.set_ylabel(
        r"Remnant mass [M$_{\odot}$]", labelpad=60
    )

    ax_co_core_mass_to_remnant_mass_core_mass_shift.set_title(
        "Black hole remnant mass distribution for single star evolution\nat Z={}".format(
            metallicity_value
        )
    )

    #################
    # Make up
    add_makeup_plot(
        ax_co_core_mass_to_remnant_mass_core_mass_shift,
        min_zams_mass,
        max_zams_mass,
        fiducial_max_mass_ppisn,
        metallicity_value,
    )

    return fig, ax_co_core_mass_to_remnant_mass_core_mass_shift
