"""
Function to plot the merger rate for mass vs metallicity. Both for merging in hubble time and whenever
"""

import numpy as np

from matplotlib import gridspec
import matplotlib.colors
import matplotlib.pyplot as plt
import matplotlib as mpl

from grav_waves.settings import (
    AGE_UNIVERSE_IN_YEAR,
)
from grav_waves.gw_analysis.functions.functions import (
    make_df,
)
from grav_waves.convolution.functions.convolution_functions import (
    get_dataset_metallicity_info,
)

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()
mpl.rc("text", usetex=False)


def plot_merger_rate_by_metallicity_by_mass_quantity(
    merger_dict,
    mass_quantity,
    mass_bins,
    dco_type,
    convolution_settings,
    display_log_string=False,
    plot_settings=None,
):
    """
    Function to plot the yield per metallicity per mass quantity
    """

    if convolution_settings.get("global_query", None):
        plot_settings["runname"] = plot_settings.get(
            "runname", ""
        ) + " (query: {})".format(convolution_settings.get("global_query"))

    if not plot_settings:
        plot_settings = {}

    bins_x = mass_bins

    # get metallicity info and generate the y-bins
    (
        sorted_metallicity_keys,
        sorted_metallicity_keys_type,
        sorted_metallicity_values,
        sorted_metallicity_logvalues,
        logmetallicity_stepsize,
        bins_log10metallicity_values,
    ) = get_dataset_metallicity_info(merger_dict, dco_type)
    sorted_metallicity_logvalues = np.array(sorted_metallicity_logvalues)
    bins_y_log = (
        sorted_metallicity_logvalues - 0.5 * np.diff(sorted_metallicity_logvalues)[0]
    )
    bins_y_log = list(bins_y_log)
    bins_y_log.append(bins_y_log[-1] + logmetallicity_stepsize[-1])

    #####
    # Create the dataframes

    # loop over all the metallicities and combine the dfs
    for i, el in enumerate(sorted_metallicity_keys_type):
        info_dict = merger_dict[el]

        if mass_quantity == "any_mass":
            all_df_pre = make_df(
                info_dict["filename"],
                info_dict["metallicity"],
                probability_to_number_to_solarmass_factor=convolution_settings[
                    "mass_in_stars"
                ],
                binary_fraction_factor=convolution_settings["binary_fraction"],
                limit_merging_time=False,
            )

            # Add the columns
            all_df_pre["primary_mass"] = all_df_pre[["mass_1", "mass_2"]].max(axis=1)
            all_df_pre["total_mass"] = all_df_pre["mass_1"] + all_df_pre["mass_2"]

            # Do query if its passed
            if convolution_settings.get("global_query", None):
                all_df_pre = all_df_pre.query(convolution_settings.get("global_query"))

            # Rename columns
            all_df_primary = all_df_pre.copy(deep=True)
            all_df_primary = all_df_primary.rename(columns={"mass_1": "any_mass"})
            all_df_primary = all_df_primary.drop(columns=["mass_2"])

            # Rename columns
            all_df_secondary = all_df_pre.copy(deep=True)
            all_df_secondary = all_df_secondary.rename(columns={"mass_2": "any_mass"})
            all_df_secondary = all_df_secondary.drop(columns=["mass_1"])

            all_df = all_df_primary.append(all_df_secondary, ignore_index=True)
            all_df["number_per_solar_mass"] = all_df["number_per_solar_mass"] / 2

        else:
            all_df = make_df(
                info_dict["filename"],
                info_dict["metallicity"],
                probability_to_number_to_solarmass_factor=convolution_settings[
                    "mass_in_stars"
                ],
                binary_fraction_factor=convolution_settings["binary_fraction"],
                limit_merging_time=False,
            )

            # Add the columns
            all_df["primary_mass"] = all_df[["mass_1", "mass_2"]].max(axis=1)
            all_df["total_mass"] = all_df["mass_1"] + all_df["mass_2"]

            # Do query if its passed
            if convolution_settings.get("global_query", None):
                all_df = all_df.query(convolution_settings.get("global_query"))

        # Create merging dataframe and drop the columns that are not necessary
        merging_df = all_df[all_df.merger_time_values_in_years < AGE_UNIVERSE_IN_YEAR]

        keep_columns = set(["number_per_solar_mass", "index", mass_quantity])
        drop_columns = list(set(merging_df.columns) - keep_columns)

        all_df = all_df.drop(columns=drop_columns)
        all_df["metallicity"] = info_dict["metallicity"]

        merging_df = merging_df.drop(columns=drop_columns)
        merging_df["metallicity"] = info_dict["metallicity"]

        #
        if i == 0:
            combined_all_df = all_df.copy(deep=True)
            combined_merging_df = merging_df.copy(deep=True)
        else:
            combined_all_df = combined_all_df.append(all_df, ignore_index=True)
            combined_merging_df = combined_merging_df.append(
                merging_df, ignore_index=True
            )

    #######
    # Pre-bin
    histogram_all = np.histogram2d(
        combined_all_df[mass_quantity],
        np.log10(combined_all_df["metallicity"]),
        bins=[bins_x, bins_y_log],
        weights=combined_all_df["number_per_solar_mass"],
    )

    histogram_merging = np.histogram2d(
        combined_merging_df[mass_quantity],
        np.log10(combined_merging_df["metallicity"]),
        bins=[bins_x, bins_y_log],
        weights=combined_merging_df["number_per_solar_mass"],
    )

    min_val = np.min(
        [
            np.min(histogram_all[0][histogram_all[0] != 0]),
            np.min(histogram_merging[0][histogram_merging[0] != 0]),
        ]
    )
    max_val = np.max(
        [
            np.max(histogram_all[0][histogram_all[0] != 0]),
            np.max(histogram_merging[0][histogram_merging[0] != 0]),
        ]
    )

    #####
    # Plotting

    # Set up canvas and grid
    fig = plt.figure(constrained_layout=False, figsize=(32, 32))
    gs = fig.add_gridspec(ncols=12, nrows=2)

    ax_merging = fig.add_subplot(gs[0, :10])
    ax_all = fig.add_subplot(gs[1, :10])

    #
    _ = ax_all.hist2d(
        combined_all_df[mass_quantity],
        np.log10(combined_all_df["metallicity"]),
        bins=[bins_x, bins_y_log],
        weights=combined_all_df["number_per_solar_mass"],
        norm=matplotlib.colors.LogNorm(vmin=min_val, vmax=max_val),
    )
    _ = ax_merging.hist2d(
        combined_merging_df[mass_quantity],
        np.log10(combined_merging_df["metallicity"]),
        bins=[bins_x, bins_y_log],
        weights=combined_merging_df["number_per_solar_mass"],
        norm=matplotlib.colors.LogNorm(vmin=min_val, vmax=max_val),
    )

    # make colorbar
    colorbar_ax = fig.add_subplot(gs[:, -1])
    cbar = matplotlib.colorbar.ColorbarBase(
        colorbar_ax,
        cmap=matplotlib.cm.viridis,
        norm=matplotlib.colors.LogNorm(vmin=min_val, vmax=max_val),
    )
    cbar.ax.set_ylabel("Yield (number per formed solar mass")

    # Add metallicity lines
    dataset_metallicities = [
        float(key.split("_")[0][1:]) for key in sorted_metallicity_keys_type
    ]
    for dataset_metallicity in dataset_metallicities:
        ax_all.plot(
            bins_x,
            [np.log10(dataset_metallicity) for el in range(len(bins_x))],
            "-.",
            color="green",
            alpha=0.5,
            linewidth=2,
        )
        ax_merging.plot(
            bins_x,
            [np.log10(dataset_metallicity) for el in range(len(bins_x))],
            "-.",
            color="green",
            alpha=0.5,
            linewidth=2,
        )

    # Some makeup
    ax_all.set_xlabel(r"%s [M$_{\odot}$]" % mass_quantity)
    ax_merging.set_xlabel(r"%s [M$_{\odot}$]" % mass_quantity)

    fig.suptitle(
        "Yield (number per formed solar mass) per metallicity per any mass: {}".format(
            dco_type
        )
    )

    ax_all.set_title("All")
    ax_merging.set_title("merging in hubble time")
    ax_merging.set_xticklabels([])

    gs.update(hspace=0.5)
    ax_merging.set_ylabel("Log10 metallicity")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
