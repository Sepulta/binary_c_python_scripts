import os

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)
filename = "list.txt"


def return_filehandle(filename):
    """
    Function to return the filehandle
    """

    source_file_filehandle = open(filename, "r")

    return source_file_filehandle


def _dict_from_line_source_file(line):
    """
    Function that creates a dict from a binary_c arg line
    """

    if line.startswith("binary_c "):
        line = line.replace("binary_c ", "")

    split_line = line.split()
    arg_dict = {}

    for i in range(0, len(split_line), 2):
        try:
            arg_dict[split_line[i]] = int(split_line[i + 1])
        except ValueError:
            try:
                arg_dict[split_line[i]] = float(split_line[i + 1])
            except ValueError:
                arg_dict[split_line[i]] = str(split_line[i + 1])

    return arg_dict


def create_generator_for_sourcefile(filehandle):
    """
    Function to create a generator for handling systems from a sourcefile
    """

    for line in filehandle:
        yield _dict_from_line_source_file(line)


#
filehandle = return_filehandle(filename)
generator = create_generator_for_sourcefile(filehandle)

print(generator)

for system_dict in generator:
    print(system_dict)
