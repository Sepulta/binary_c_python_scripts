"""
Script to set up the functionality to get add the population settings to the plot
"""

import json
import h5py

if __name__ == "__main__":
    # filenames
    convolution_filename = "/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/convolution/scripts/tmp/add_population_settings/results/convolution_results.h5"
    population_settings_filename = "/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/convolution/scripts/tmp/add_population_settings/results/population_settings.json"

    # Open the file
    convolution_filehandle = h5py.File(convolution_filename, "a")

    add_population_settings_to_hdf5file(
        convolution_filehandle=convolution_filehandle,
        population_settings_filename=population_settings_filename,
    )

    # close the file
    convolution_filehandle.close()
