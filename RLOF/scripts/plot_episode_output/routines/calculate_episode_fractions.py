"""
Function to calculate the fractions of things we are interesting in based on the data of the episodes

TODO: make a deeper analysis on the RLOF episode. Look at the donors and accretors and what phase they're in. Could do it as a print now.
"""

import json
import pandas as pd
from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict

from grav_waves.settings import convolution_settings

import warnings
from pandas.core.common import SettingWithCopyWarning

warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)


def calculate_fractions_dict(dataset_dict, base_query=None):
    """
    Function to calculate fractions of interest.
    """

    # Read out data and create dataframe
    df = pd.read_csv(dataset_dict["filename"], sep="\s+", header=0)

    # Add columns
    df["time_spent_mt"] = df["final_time"] - df["initial_time"]
    df["ratio_angular_momentum_stream_excess_returned_to_orbit"] = df[
        "total_angular_momentum_stream_excess_returned_to_orbit"
    ] / (df["initial_orbital_angular_momentum"])
    df["ratio_angular_momentum_stellar_overspin_returned_to_orbit"] = df[
        "total_angular_momentum_stellar_overspin_returned_to_orbit"
    ] / (df["initial_orbital_angular_momentum"])
    df["ratio_time_spent_disk_masstransfer"] = (
        df["total_time_spent_disk_masstransfer"] / df["time_spent_mt"]
    )
    df["ratio_mass_transferred_through_disk"] = df[
        "total_mass_transferred_through_disk"
    ] / (df["total_mass_lost"] + df["total_mass_accreted"])

    ## convert to yield per solarmass
    # Multiply the probability by a binary fraction
    df["probability"] *= convolution_settings["binary_fraction"]

    # Multiply the probability by a conversion factor to get the number per solar mass
    df["number_per_solar_mass"] = (
        df["probability"] * convolution_settings["mass_in_stars"]
    )

    # perform base query
    if not base_query is None:
        df = df.query(base_query)

    # calculate integrated based on time and mass
    df["integrated_disk_time_weighted"] = (
        df["total_time_spent_disk_masstransfer"] * df["number_per_solar_mass"]
    )
    df["integrated_disk_mass_weighted"] = (
        df["total_mass_transferred_through_disk"] * df["number_per_solar_mass"]
    )

    df["integrated_total_time_weighted"] = (
        df["time_spent_mt"] * df["number_per_solar_mass"]
    )
    df["integrated_total_mass_weighted"] = (
        df["total_mass_lost"] + df["total_mass_accreted"]
    ) * df["number_per_solar_mass"]

    res_dict = {
        "total": df["number_per_solar_mass"].sum(),
        "base_query": base_query,
        # Fraction of occurence of disk during any moment in the episode
        "fraction_disk_anytime_based_on_time": df[
            df.ratio_time_spent_disk_masstransfer > 0
        ]["number_per_solar_mass"].sum()
        / df["number_per_solar_mass"].sum(),
        "fraction_disk_anytime_based_on_mass": df[
            df.ratio_mass_transferred_through_disk > 0
        ]["number_per_solar_mass"].sum()
        / df["number_per_solar_mass"].sum(),
        # total fraction integrated through time, weighted by time or by mass
        "fraction_integrated_disk_time_weight": df[
            "integrated_disk_time_weighted"
        ].sum()
        / df["integrated_total_time_weighted"].sum(),
        "fraction_integrated_disk_mass_weight": df[
            "integrated_disk_mass_weighted"
        ].sum()
        / df["integrated_total_mass_weighted"].sum(),
    }

    # print(json.dumps(res_dict, indent=4))

    # return res_dict


# simulation_dir = '/home/david/projects/binary_c_root/results/RLOF/LOW_RES_RLOF_SANA_2021_ENSEMBLE_fractions'
simulation_dir = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions"
rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(simulation_dir)
dataset_dict = rlof_episode_dataset_dict["datasets"]

base_query = "initial_stability == 0 & initial_stellar_type_accretor == 1"
calculate_fractions_dict(dataset_dict["0.02"], base_query=base_query)
