"""
Script containing utility functions for the schematic plot routine
"""

import os
import pandas as pd
import numpy as np
import matplotlib.transforms as mtransforms

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.extra_plot_functions import (
    place_arrows,
    add_bracket,
)

alpha_regions = 0.2


def add_bracket_extra_mass_loss_plot(
    fig, ax, zams_mass_for_max_ppisn, fiducial_max_mass_ppisn, variation_max_mass_ppisn
):
    """
    Function to add the bracket for the extra mass loss
    """

    # Get location for the brackets
    location_max_fiducial_mass = (
        zams_mass_for_max_ppisn,
        fiducial_max_mass_ppisn,
    )
    location_max_mass_removal_mass = (
        zams_mass_for_max_ppisn,
        variation_max_mass_ppisn,
    )
    location_startpoint = (
        zams_mass_for_max_ppisn + 25,
        (fiducial_max_mass_ppisn + variation_max_mass_ppisn) / 2,
    )

    # Plot the brackets
    fig, ax = add_bracket(
        fig,
        ax,
        location_max_fiducial_mass,
        location_max_mass_removal_mass,
        location_startpoint,
        -2,
        linestyle="solid",
        color="black",
        linewidth=5,
    )

    # Add text
    ax.text(
        location_startpoint[0] + 5,
        location_startpoint[1] - 1,
        s=r"$\Delta M_{\mathrm{PPI,\ Extra}}$",
        fontsize=20,
    )


def get_dataset_regions_and_mass(dataset_used_for_regions, max_zams_mass, df_dict):
    """
    Function to get the regions dataset and first_ppisn_mass and fryer df
    """

    if dataset_used_for_regions == "no_prescription":
        region_dataset = df_dict["no_prescription_df"]
    if dataset_used_for_regions == "fiducial":
        region_dataset = df_dict["fiducial_df"]
    if dataset_used_for_regions == "old_prescription":
        region_dataset = df_dict["old_prescription_df"]
    if dataset_used_for_regions == "variation":
        region_dataset = df_dict["variation_df"]

    # Get mass first ppisn
    first_ppisn_mass = max_zams_mass
    if dataset_used_for_regions != "no_prescription":
        first_ppisn_mass = region_dataset[region_dataset.sn_type == 21].iloc[0][
            "zams_mass"
        ]

    # Get the region for CC
    fryer_area_df = region_dataset[region_dataset.zams_mass < first_ppisn_mass].copy(
        deep=True
    )

    return region_dataset, fryer_area_df, first_ppisn_mass


def add_shaded_regions_plot(ax, dataset_used_for_regions, max_zams_mass, df_dict):
    """
    Function to add the shaded regions to a plot
    """

    # Get regions and interface mass
    region_dataset, fryer_area_df, first_ppisn_mass = get_dataset_regions_and_mass(
        dataset_used_for_regions=dataset_used_for_regions,
        max_zams_mass=max_zams_mass,
        df_dict=df_dict,
    )

    #
    ax.fill_between(
        fryer_area_df["zams_mass"],
        fryer_area_df["mass"],
        y2=0,
        color="yellow",
        alpha=alpha_regions,
    )

    # Plot region of PPISN
    if not dataset_used_for_regions == "no_prescription":
        ax.fill_between(
            region_dataset[region_dataset.zams_mass >= first_ppisn_mass]["zams_mass"],
            region_dataset[region_dataset.zams_mass >= first_ppisn_mass]["mass"],
            y2=0,
            color="pink",
            alpha=alpha_regions,
        )


def add_supernova_lines_plot(
    fig,
    ax,
    fiducial_max_mass_ppisn,
    dataset_used_for_regions,
    max_zams_mass,
    x_parameter,
    df_dict,
):
    """
    Function to add the supernova lines to the plot
    """

    y_value_sn_mechanism_line = fiducial_max_mass_ppisn + 2.5

    # Get regions and interface mass
    region_dataset, fryer_area_df, first_ppisn_mass = get_dataset_regions_and_mass(
        dataset_used_for_regions=dataset_used_for_regions,
        max_zams_mass=max_zams_mass,
        df_dict=df_dict,
    )

    #################
    # Plot the CC SN
    # below_ppisn_area = region_dataset[region_dataset.zams_mass<min_ppisn_zams]

    min_below_ppisn_zams = fryer_area_df["zams_mass"].to_numpy().min()
    max_below_ppisn_zams = fryer_area_df["zams_mass"].to_numpy().max()

    size_between_below_ppisn = max_below_ppisn_zams - min_below_ppisn_zams
    midpoint_below_ppisn = (max_below_ppisn_zams + min_below_ppisn_zams) / 2

    #
    fig, ax = place_arrows(
        fig,
        ax,
        midpoint_below_ppisn,
        y_value_sn_mechanism_line,
        size_between_below_ppisn,
        text="core-collapse",
    )

    #################
    # Plot the PPI SN
    if not dataset_used_for_regions == "no_prescription":
        ppisn_area = region_dataset[region_dataset.zams_mass >= first_ppisn_mass][
            region_dataset.stellar_type == 14
        ]

        min_ppisn_zams = ppisn_area["zams_mass"].to_numpy().min()
        max_ppisn_zams = ppisn_area["zams_mass"].to_numpy().max()
        size_between_ppisn = max_ppisn_zams - min_ppisn_zams
        midpoint_ppisn = (max_ppisn_zams + min_ppisn_zams) / 2

        #
        fig, ax = place_arrows(
            fig,
            ax,
            midpoint_ppisn,
            y_value_sn_mechanism_line,
            size_between_ppisn,
            text="pulsational pair-instability + core-collapse",
        )


def get_dataframes(
    old_prescription_result_directory_root,
    result_directory_variation,
    no_prescription_result_directory_root,
    variation_name,
):
    """
    Function to get the dataframes
    """

    ########################
    # Get old prescription df and select BHs
    old_prescription_df = get_df_and_filter_bh(
        os.path.join(old_prescription_result_directory_root, "0")
    )

    ########################
    # get fiducial df
    fiducial_name = "0" if variation_name != "mass_removal_multiplier" else "1"
    fiducial_df = get_df_and_filter_bh(
        os.path.join(result_directory_variation, fiducial_name)
    )

    ########################
    # get no-prescription df
    if no_prescription_result_directory_root is not None:
        no_prescription_df = get_df_and_filter_bh(
            os.path.join(no_prescription_result_directory_root, "0")
        )
    else:
        no_prescription_df = None

    ########################
    # Load mass removal data
    variation_dirs = {
        subdir: os.path.join(result_directory_variation, subdir)
        for subdir in os.listdir(result_directory_variation)
        if not subdir == fiducial_name
    }
    variation_df = get_df_and_filter_bh(variation_dirs[list(variation_dirs.keys())[0]])

    df_dict = {
        "old_prescription_df": old_prescription_df,
        "fiducial_df": fiducial_df,
        "no_prescription_df": no_prescription_df,
        "variation_df": variation_df,
    }

    return (old_prescription_df, fiducial_df, no_prescription_df, variation_df, df_dict)


def calculate_min_max_zams_ppisn_masses(fiducial_df, variation_df, x_parameter):
    """
    Function to calculate min and max ppisn masses
    """

    ########################
    # Calculate the min and max zams that undergoes ppisn for both the dataframes
    min_ppisn_zams = np.min(
        [
            fiducial_df[fiducial_df.sn_type == 21][x_parameter].min(),
            variation_df[variation_df.sn_type == 21][x_parameter].min(),
        ]
    )

    max_ppisn_zams = np.max(
        [
            fiducial_df[fiducial_df.sn_type == 21][x_parameter].max(),
            variation_df[variation_df.sn_type == 21][x_parameter].max(),
        ]
    )

    ########################
    # Get max mass and corresponding ZAMS mass
    zams_mass_for_max_ppisn = fiducial_df.loc[fiducial_df["mass"].idxmax()][x_parameter]
    fiducial_max_mass_ppisn = fiducial_df[
        fiducial_df[x_parameter] == zams_mass_for_max_ppisn
    ]["mass"].to_numpy()[0]
    variation_max_mass_ppisn = variation_df[
        variation_df[x_parameter] == zams_mass_for_max_ppisn
    ]["mass"].to_numpy()[0]

    return (
        min_ppisn_zams,
        max_ppisn_zams,
        zams_mass_for_max_ppisn,
        fiducial_max_mass_ppisn,
        variation_max_mass_ppisn,
    )


def add_makeup_plot(
    ax, min_zams_mass, max_zams_mass, fiducial_max_mass_ppisn, metallicity_value
):
    """
    Function to standard makeup to the plot
    """

    # Set limits
    ax.set_xlim(min_zams_mass, max_zams_mass)
    ax.set_ylim(0, fiducial_max_mass_ppisn + 10)

    #################
    # Configure axes etc
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles[::-1], labels[::-1], loc="lower left", bbox_to_anchor=(0.01, 0.5))

    # Set axis labels
    ax.set_ylabel(r"Remnant mass [M$_{\odot}$]")
    ax.set_xlabel(r"ZAMS mass [M$_{\odot}$]")

    #
    ax.set_title(
        "Black hole remnant mass distribution for single star evolution\nat Z={}".format(
            metallicity_value
        )
    )


def add_core_mass_shift_plot(ax, core_mass_shift_df, x_parameter):
    """
    Function to add the results with extra mass removed
    """

    ax.plot(
        core_mass_shift_df[x_parameter],
        core_mass_shift_df["mass"],
        linestyle="--",
        alpha=0.8,
        label=r"New prescription - $\Delta M_{\mathrm{CO,\ PPI}}$",
    )


def add_mass_removal_plot(ax, mass_removal_df, x_parameter):
    """
    Function to add the results with extra mass removed
    """

    ax.plot(
        mass_removal_df[x_parameter],
        mass_removal_df["mass"],
        linestyle="--",
        alpha=0.8,
        label=r"New prescription - $\Delta M_{\mathrm{PPI,\ Extra}}$",
    )


def add_mass_removal_multiplier_plot(ax, mass_removal_multiplier_df, x_parameter):
    """
    Function to add the results with a mass multiplier enabled
    """

    ax.plot(
        mass_removal_multiplier_df[x_parameter],
        mass_removal_multiplier_df["mass"],
        linestyle="--",
        alpha=0.8,
        label=r"New prescription with $\beta_{\mathrm{multiplier}}$",
    )


def add_fiducial_plot(ax, fiducial_df, x_parameter):
    """
    Function to add the fiducial ppisn rresutls
    """

    ax.plot(
        fiducial_df[x_parameter],
        fiducial_df["mass"],
        color="black",
        label="New prescription",
    )


def add_old_prescription_plot(ax, old_prescription_df, x_parameter):
    """
    Function to add plot for data from farmer+19
    """

    ax.plot(
        old_prescription_df[x_parameter],
        old_prescription_df["mass"],
        linestyle="-.",
        alpha=0.8,
        label="Farmer et al. '19 prescription",
    )


def add_no_prescription_plot(ax, no_prescription_df, x_parameter):
    """
    Function to add plot for data where no PPISn prescription is used
    """

    ax.plot(
        no_prescription_df[x_parameter],
        no_prescription_df["mass"],
        linestyle="-.",
        alpha=0.5,
        label="No PPISN prescription",
    )


def add_pre_SN_mass_plot(ax, fiducial_df, x_parameter):
    """
    Function to add plot for data where the new PPISN prescription is used
    """

    ax.plot(
        fiducial_df[x_parameter],
        fiducial_df["pre_sn_mass"],
        color="black",
        linestyle=":",
        label="Pre-SN mass",
        alpha=0.5,
    )


def add_ax_invisible(fig, gs_slice):
    """
    Function to add invisible axis
    """

    ax_invisible = fig.add_subplot(gs_slice, frame_on=False)
    ax_invisible.set_xticks([])
    ax_invisible.set_yticks([])

    return ax_invisible


def align_axes(fig, axes_list, which_axis="x"):
    """
    Function to align the x or y axis of a list of axes
    """

    if which_axis == "x":
        getter = "get_xlim"
        setter = "set_xlim"
    elif which_axis == "y":
        getter = "get_ylim"
        setter = "set_ylim"
    else:
        raise ValueError("not implemented yet")

    min_val = 1e9
    max_val = 1e-9

    # Find the min and max
    for axis in axes_list:
        lims = axis.__getattribute__(getter)()

        min_val = np.min([min_val, lims[0]])
        max_val = np.max([max_val, lims[1]])

    # Set the min and max
    for axis in axes_list:
        axis.__getattribute__(setter)([min_val, max_val])


def handle_overriding_xlim(fig, axes_list, plot_settings):
    """
    function to handle overriding the xlims
    """

    # Loop over axes
    for ax in axes_list:
        # Override the limits if we have input:
        if plot_settings.get("min_xlim", None) is not None:
            ax.set_xlim(
                [
                    plot_settings["min_xlim"],
                    ax.get_xlim()[1],
                ]
            )
        if plot_settings.get("max_xlim", None) is not None:
            ax.set_xlim(
                [
                    ax.get_xlim()[0],
                    plot_settings["max_xlim"],
                ]
            )
        if plot_settings.get("min_ylim", None) is not None:
            ax.set_ylim(
                [
                    plot_settings["min_ylim"],
                    ax.get_ylim()[1],
                ]
            )
        if plot_settings.get("max_ylim", None) is not None:
            ax.set_ylim(
                [
                    ax.get_ylim()[0],
                    plot_settings["max_ylim"],
                ]
            )


def add_labels_subplots(fig, axes_list, label_function_kwargs):
    """
    Function to loop over a list of axes of a figure and add subplots
    """

    # Add subplot labels
    for ax_i, ax in enumerate(axes_list):
        add_label_subplot(fig=fig, ax=ax, label_i=ax_i, **label_function_kwargs)


def add_label_subplot(
    fig,
    ax,
    label_i,
    x_loc=0.95,
    y_loc=0.95,
    fontsize=24,
    bbox_dict={"pad": 0.5, "facecolor": "None", "boxstyle": "round"},
):
    """
    Function to add a label to the subplot
    """

    subplot_labels = "abcdefg"

    trans = mtransforms.ScaledTranslation(10 / 72, -5 / 72, fig.dpi_scale_trans)
    ax.text(
        x_loc,
        y_loc,
        subplot_labels[label_i],
        transform=ax.transAxes + trans,
        fontsize=fontsize,
        verticalalignment="top",
        bbox=dict(**bbox_dict),
    )


def get_df(result_dir):
    """
    Function to get the dataframes
    """

    datafile = os.path.join(result_dir, "output.dat")
    df = pd.read_table(datafile, sep="\s+")
    df = df.sort_values(by="zams_mass")

    return df


def get_df_and_filter_bh(result_dir):
    """
    Function to get the dataframe and filter on bh only
    """

    df = get_df(result_dir)
    df_bh = df[df.stellar_type == 14]

    return df_bh
