"""
Function to plot the delay time given a dataframe
"""

import numpy as np

import matplotlib.pyplot as plt

#
from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    linestyle_list,
)

from david_phd_functions.plotting.utils import show_and_save_plot, add_plot_info
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def plot_delay_time(df, plot_channels=True, plot_settings={}):
    """
    Function to plot the delay time given a dataframe
    """

    ####
    # Delay time plot
    df["log10_delaytime"] = np.log10(df["time"])

    delaytime_bins = np.arange(0, 5, 0.1)

    ######
    # Plot
    fig = plt.figure(figsize=(20, 20))

    #
    gs = fig.add_gridspec(nrows=10, ncols=1)

    #
    ax = fig.add_subplot(gs[:, :])

    #
    ax.hist(
        df["log10_delaytime"],
        bins=delaytime_bins,
        weights=df["probability"],
        histtype="step",
        lw=4,
    )
    ax.set_yscale("log")

    if plot_channels:
        for channel_i, channel in enumerate(sorted(df["prev_stellar_type_2"].unique())):
            channel_df = df[df.prev_stellar_type_2 == channel]

            ax.hist(
                channel_df["log10_delaytime"],
                bins=delaytime_bins,
                weights=channel_df["probability"],
                histtype="step",
                lw=3,
                linestyle=linestyle_list[channel_i],
            )

    #######################
    # Save and finish
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
