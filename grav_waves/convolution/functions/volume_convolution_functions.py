"""
Functions to calculate the redshift shell volumes
"""

import numpy as np
import astropy.units as u

from grav_waves.settings import cosmo


def create_shell_volume_dict(redshift_center_values):
    """
    Function that can generate a dictionary of shell volumes based on an input center redshift array
    """

    # Get the redshift edges etc
    redshift_stepsize = np.diff(redshift_center_values)[0]
    redshift_edges = np.zeros(len(redshift_center_values) + 1)
    redshift_edges[1:] = redshift_center_values + redshift_stepsize / 2
    # print(redshift_edges)

    # Calculate comoving volumes
    comoving_volumes_at_redshift_edges = cosmo.comoving_volume(redshift_edges).to(
        u.Gpc**3
    )
    # print(comoving_volumes_at_redshift_edges)

    # Calculate the comoving shell volumes
    comoving_shell_volumes = np.diff(comoving_volumes_at_redshift_edges)
    # print(comoving_shell_volumes)

    # create dict with the value at center of 'shell'
    comoving_shell_volumes_dict = {
        redshift_center_values[i]: {
            "shell_volume": comoving_shell_volumes[i],
            "lower_edge_shell": redshift_edges[i],
            "upper_edge_shell": redshift_edges[i + 1],
            "center_shell": redshift_center_values[i],
        }
        for i in range(len(redshift_center_values))
    }

    return comoving_shell_volumes_dict


def create_redshift_volume_convolution_result_dict(convolution_result):
    """
    Function to create the redshift volume convolution result dict from the convolution dict that contains the intrinsic rates
    """

    # Open settings
    convolution_configuration = convolution_result["convolution_configuration"]

    # Readout time centers
    time_centers = convolution_configuration["time_centers"]

    shell_volume_dict = create_shell_volume_dict(time_centers)
    shell_volumes_sorted = (
        np.array(
            [
                shell_volume_dict[el]["shell_volume"].value
                for el in sorted(shell_volume_dict.keys())
            ]
        )
        * u.Gpc**3
    )

    # Create the dictionary containing all the redshift volume convolution results
    redshift_volume_convolution_results = {}
    redshift_volume_convolution_results["shell_volume_dict"] = shell_volume_dict
    redshift_volume_convolution_results["merger_rates"] = {}

    for mass_type in ["primary_mass", "chirpmass", "any_mass", "total_mass"]:
        mass_array = convolution_result["results"]["merger_rates"][
            "result_array_{}".format(mass_type)
        ]

        # Multiply by the shell volumes
        multiplied = (mass_array.T * shell_volumes_sorted).T
        # summed = np.sum(multiplied, axis=0)

        redshift_volume_convolution_results["merger_rates"][
            "result_array_{}".format(mass_type)
        ] = multiplied

    return redshift_volume_convolution_results
