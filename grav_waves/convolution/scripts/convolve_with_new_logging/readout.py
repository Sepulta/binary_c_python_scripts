import h5py
import pandas as pd


combined_dco_file = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/combined_dco_dataset.h5"
filtered_combined_dco_file = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/filtered_combined_dco_dataset.h5"


combined_dco_df = pd.read_hdf(combined_dco_file, "/data/combined_dataframes")
filtered_combined_dco_file = pd.read_hdf(
    filtered_combined_dco_file, "/data/combined_dataframes"
)


print(combined_dco_df)
print(combined_dco_df.columns)

print(filtered_combined_dco_file)
print(filtered_combined_dco_file.columns)
