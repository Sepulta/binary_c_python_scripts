"""
Function to add a variation to the axis and handle all the rest
"""

import os
import matplotlib as mpl

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_utils import (
    add_pre_SN_mass_plot,
    add_no_prescription_plot,
    add_old_prescription_plot,
    add_fiducial_plot,
    add_mass_removal_plot,
    add_core_mass_shift_plot,
    add_mass_removal_multiplier_plot,
    add_makeup_plot,
    calculate_min_max_zams_ppisn_masses,
    get_dataframes,
    add_bracket_extra_mass_loss_plot,
    add_supernova_lines_plot,
    add_shaded_regions_plot,
)

from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def add_zams_variation_to_axis(
    fig,
    ax,
    #
    result_directory_variation,
    old_prescription_result_directory_root,
    no_prescription_result_directory_root=None,
    #
    variation_name="mass_removal",
    dataset_used_for_regions="variation",
    #
    add_pre_SN_mass_line=True,
    add_no_prescription_line=False,
    add_old_prescription_line=True,
    add_fiducial_line=True,
    add_variation_line=True,
    #
    add_variation_bracket=True,
    add_shading_regions=True,
    add_supernova_lines=True,
):
    """
    Function to add the ZAMS mass to remnant mass with extra ppisn mass removal plot to an axis
    """

    # Settings
    min_zams_mass = 0
    max_zams_mass = 250
    x_parameter = "zams_mass"

    ########################
    # Get metallicity values
    metallicity_value = float(
        os.path.basename(result_directory_variation).split("_Z")[-1]
    )

    ########################
    # Get the dataframes
    (
        old_prescription_df,
        fiducial_df,
        no_prescription_df,
        variation_df,
        df_dict,
    ) = get_dataframes(
        old_prescription_result_directory_root,
        result_directory_variation,
        no_prescription_result_directory_root,
        variation_name=variation_name,
    )

    ########################
    # Get some masses
    (
        min_ppisn_zams,
        max_ppisn_zams,
        zams_mass_for_max_ppisn,
        fiducial_max_mass_ppisn,
        variation_max_mass_ppisn,
    ) = calculate_min_max_zams_ppisn_masses(fiducial_df, variation_df, x_parameter)

    #############################################
    # Plot masses

    ################
    # Pre-SN mass plot
    if add_pre_SN_mass_line:
        add_pre_SN_mass_plot(ax, fiducial_df, x_parameter)

    ################
    # No ppisn prescription plot
    if add_no_prescription_line:
        add_no_prescription_plot(ax, no_prescription_df, x_parameter)

    ################
    # old prescription mass plot
    if add_old_prescription_line:
        add_old_prescription_plot(ax, old_prescription_df, x_parameter)

    ################
    # variation plot
    if add_variation_line:
        if variation_name == "mass_removal":
            # Add plot for mass removal
            add_mass_removal_plot(ax, variation_df, x_parameter)

            ################
            # Add bracket indicating the extra PPISN loss
            if add_variation_bracket:
                add_bracket_extra_mass_loss_plot(
                    fig,
                    ax,
                    zams_mass_for_max_ppisn,
                    fiducial_max_mass_ppisn,
                    variation_max_mass_ppisn,
                )

        ################
        # Core mass shift plot
        if variation_name == "core_mass_shift":
            add_core_mass_shift_plot(ax, variation_df, x_parameter)

        ################
        # Core mass shift plot
        if variation_name == "mass_removal_multiplier":
            add_mass_removal_multiplier_plot(ax, variation_df, x_parameter)

    ################
    # fiducial mass plot
    if add_fiducial_line:
        add_fiducial_plot(ax, fiducial_df, x_parameter)

    ################
    # Create shaded region under the main line that indicates which prescription
    if add_shading_regions:
        add_shaded_regions_plot(
            ax=ax,
            dataset_used_for_regions=dataset_used_for_regions,
            max_zams_mass=max_zams_mass,
            df_dict=df_dict,
        )

    ################
    # Add lines on the top to indicate which supernova type
    if add_supernova_lines:
        add_supernova_lines_plot(
            fig=fig,
            ax=ax,
            fiducial_max_mass_ppisn=fiducial_max_mass_ppisn,
            dataset_used_for_regions=dataset_used_for_regions,
            max_zams_mass=max_zams_mass,
            df_dict=df_dict,
            x_parameter=x_parameter,
        )

    ################
    # Add makeup to plot
    add_makeup_plot(
        ax, min_zams_mass, max_zams_mass, fiducial_max_mass_ppisn, metallicity_value
    )

    return fig, ax
