"""
Plot functions for the cluster analysis
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

from ensemble_functions.ensemble_functions import generate_bins_from_keys

custom_mpl_settings.load_mpl_rc()

######
# Plot routine for the eccentricity vs
def plot_cluster_and_data(cluster_df, cluster_observations=None, plot_settings={}):
    """
    Function to plot the cluster with data
    """

    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=1)
    # axes_list = []

    # rates_axis = fig.add_subplot(gs[level * gs_scale : (level + 2) * gs_scale, 0])

    plot_axis = fig.add_subplot(gs[:, :])

    norm = colors.LogNorm(
        vmin=1e-5,
        vmax=1,
    )
    # norm = colors.Normalize(vmin=0, vmax=1)

    # Get the bins from the data
    log10period_bins = generate_bins_from_keys(
        cluster_df["log10orbital_period"].to_numpy()
    )
    log10period_bincenters = (log10period_bins[1:] + log10period_bins[:-1]) / 2
    print(log10period_bins)

    # period_bins = 10**log10period_bins
    # period_bincenters = 10**log10period_bincenters

    ecc_bins = generate_bins_from_keys(cluster_df["orbital_eccentricity"].to_numpy())
    ecc_bincenters = (ecc_bins[1:] + ecc_bins[:-1]) / 2

    # Do numpy hist first
    hist, _, _ = np.histogram2d(
        cluster_df["log10orbital_period"],
        cluster_df["orbital_eccentricity"],
        bins=[log10period_bins, ecc_bins],
        weights=cluster_df["probability"],
    )

    X, Y = np.meshgrid(log10period_bincenters, ecc_bincenters)

    #
    _ = plot_axis.pcolormesh(
        X,
        Y,
        hist.T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # ###########
    # # Plot the distributions
    # cluster_df["orbital_period_in_days"] = 365 * 10 ** cluster_df["log10orbital_period"]
    # period_values = cluster_df["orbital_period_in_days"].to_numpy()
    # eccentricity_values = cluster_df["orbital_eccentricity"].to_numpy()
    # probability_values = cluster_df["probability"].to_numpy()

    # # plot_axis.scatter(period_values, eccentricity_values)

    # # # Plot
    # # plt.pcolormesh(np.expand_dims(Lat, 0), np.expand_dims(Lon, 1), C*np.eye(5))

    # #
    # _ = plot_axis.pcolormesh(
    #     np.expand_dims(period_values, 0),
    #     np.expand_dims(eccentricity_values, 1),
    #     probability_values * np.eye(len(probability_values)),
    #     norm=norm,
    #     shading="auto",
    #     antialiased=plot_settings.get("antialiased", True),
    #     rasterized=plot_settings.get("rasterized", True),
    # )

    # plot_axis.set_xscale('log')

    # #############
    # # Add observations
    # if cluster_observations:
    #     plot_axis.errorbar(
    #         cluster_observations["orbital_period_in_days"],
    #         cluster_observations["orbital_eccentricity"],
    #         xerr=cluster_observations["orbital_period_in_days_error"],
    #         yerr=cluster_observations["orbital_eccentricity_error"],
    #         fmt="o",
    #     )

    # plot_axis.set_xscale("log")

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def plot_similarity_statistics(cluster_sample_statistics, plot_settings={}):
    """
    Routine to plot the similarity statistics
    """

    binstep = 0.05
    bins = np.arange(0, 1, binstep)
    bincenters = (bins[1:] + bins[:-1]) / 2

    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=1)
    # axes_list = []

    hist_self, _ = np.histogram(cluster_sample_statistics["self_kss_array"], bins=bins)
    hist_comp, _ = np.histogram(cluster_sample_statistics["comp_kss_array"], bins=bins)

    # Normalize
    hist_self = hist_self / np.sum(hist_self)
    hist_comp = hist_comp / np.sum(hist_comp)

    plot_axis = fig.add_subplot(gs[:, :])

    # plot_axis.hist(cluster_sample_statistics['self_kss_array'], bins=bins, density=True)
    # plot_axis.hist(cluster_sample_statistics['comp_kss_array'], bins=bins, density=True)

    plot_axis.plot(bincenters, hist_self)
    plot_axis.plot(bincenters, hist_comp)

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)
