import h5py
import json
import pandas as pd
import numpy as np
import os

import matplotlib.pyplot as plt

from david_phd_functions.plotting import custom_mpl_settings

from binarycpython.utils.stellar_types import stellar_type_dict_short

custom_mpl_settings.load_mpl_rc()
data_dir = os.path.join(
    os.environ["BINARYC_DATA_ROOT"],
    "testing_python",
    "compact_objects_single",
    "default_settings",
)
datafile_002 = os.path.join(data_dir, "CO_single_0.02", "CO_single_0.02.hdf5")
datafile_001 = os.path.join(data_dir, "CO_single_0.01", "CO_single_0.01.hdf5")
datafile_0002 = os.path.join(data_dir, "CO_single_0.002", "CO_single_0.002.hdf5")
datafile_0001 = os.path.join(data_dir, "CO_single_0.001", "CO_single_0.001.hdf5")
datafile_00002 = os.path.join(data_dir, "CO_single_0.0002", "CO_single_0.0002.hdf5")


def load_file_return_df(filename, dataset_name):
    # Load files
    # file_name = '/home/david/projects/binary_c_root/results/testing_python/supernova_mass_test/SN_types_z0.002.hdf5'
    f = h5py.File(filename, "r")

    # Show some keys
    # for key in f.keys():
    #     print(key) #Names of the groups in HDF5 file.

    for key in f["data"].keys():
        print(key)

    # for key in f['settings'].keys():
    #     print(key)

    # Load data and put in dataframe
    data = f["data"][dataset_name][()]
    # settings = f['settings']['used_settings'][()]
    # settings_json = json.loads(settings)

    headers = f["data"][dataset_name + "_header"][()]
    headers = [header.decode("utf-8") for header in headers]

    # metallicity = json.loads(settings)['binary_c_defaults']['metallicity']

    # Make df
    df = pd.DataFrame(data)
    df.columns = headers

    # for i in range(16):
    #     df['f_st_{}'.format(i)] = df['st_{}'.format(i)] / df['zams_mass']

    df = df.sort_values(by=["zams_mass_1"])

    return df


df_002 = load_file_return_df(datafile_002, "CO_single_0.02")
df_001 = load_file_return_df(datafile_001, "CO_single_0.01")
df_0002 = load_file_return_df(datafile_0002, "CO_single_0.002")
df_0001 = load_file_return_df(datafile_0001, "CO_single_0.001")
df_00002 = load_file_return_df(datafile_00002, "CO_single_0.0002")

plt.plot(df_002["zams_mass_1"], df_002["mass_1"], linestyle="-", label="Z=0.02")
plt.plot(df_001["zams_mass_1"], df_001["mass_1"], linestyle="--", label="Z=0.01")
plt.plot(df_0002["zams_mass_1"], df_0002["mass_1"], linestyle="-.", label="Z=0.002")
plt.plot(df_0001["zams_mass_1"], df_0001["mass_1"], linestyle=":", label="Z=0.001")
plt.plot(df_00002["zams_mass_1"], df_00002["mass_1"], marker="o", label="Z=0.0002")


plt.plot(df_002["zams_mass_1"], df_002["prev_mass_1"], linestyle="-", alpha=0.5)
plt.plot(df_001["zams_mass_1"], df_001["prev_mass_1"], linestyle="--", alpha=0.5)
plt.plot(df_0002["zams_mass_1"], df_0002["prev_mass_1"], linestyle="-.", alpha=0.5)
plt.plot(df_0001["zams_mass_1"], df_0001["prev_mass_1"], linestyle=":", alpha=0.5)
plt.plot(df_00002["zams_mass_1"], df_00002["prev_mass_1"], marker="o", alpha=0.5)

plt.yscale("log")
plt.legend()
plt.show()
