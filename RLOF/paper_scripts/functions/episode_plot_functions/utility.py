"""
Some utility functions for the episode plots
"""

import numpy as np
import pandas as pd

from david_phd_functions.plotting.linestyle_hatch_colorlist import (
    color_list,
    linestyle_list,
)


def handle_bootstrap(
    dataset_dict,
    metallicity,
    bins,
    ratio_dict,
    hist_dict,
    bootstrap_entire_population,
    bootstraps,
    convolution_settings,
    get_quantities_func,
):
    """
    Function to handle the bootstraps
    """

    ###########
    # Entire population method
    if bootstrap_entire_population:
        # Read out data and create dataframe
        df = pd.read_csv(dataset_dict[metallicity]["filename"], sep="\s+", header=0)

        # Get a list of indices
        indices = np.arange(len(df.index))

    # ##########
    # # Query bootstrap method
    else:
        # Get a list of indices
        indices = np.arange(len(ratio_dict[metallicity]["number_per_solar_mass"]))

    # Set up array that stores all the boostrapped results
    bootstrapped_rates_hist_vals = np.zeros((bootstraps, len(bins) - 1))
    bootstrapped_rates_cdf_vals = np.zeros((bootstraps, len(bins) - 1))

    # do bootstraps
    for bootstrap_i in range(bootstraps):
        print("Bootstrap {}".format(bootstrap_i))

        ###########
        # Entire population method
        if bootstrap_entire_population:
            copy_df = df.copy(deep=True)

            boot_index = np.random.choice(
                indices,
                size=len(indices),
                replace=True,
                # p=rate_array[0][indices] / np.sum(rate_array[0][indices]),
            )

            # Get weight and quantity
            weight, quantity = get_quantities_func(
                copy_df, convolution_settings, indices=boot_index
            )

            # Select the quantity values with these indices
            bootstrapped_masked_quantity_data = quantity

            # Select the rate values with these indices
            bootstrapped_weigth_array = weight

        #############
        # query bootstrap
        else:
            ##############################
            # Get bootstrap indices
            boot_index = np.random.choice(
                indices,
                size=len(indices),
                replace=True,
                # p=rate_array[0][indices] / np.sum(rate_array[0][indices]),
            )

            # Select the quantity values with these indices
            bootstrapped_masked_quantity_data = ratio_dict[metallicity]["ratio"][
                boot_index
            ]

            # Select the rate values with these indices
            bootstrapped_weigth_array = ratio_dict[metallicity][
                "number_per_solar_mass"
            ][boot_index]

        ##############################
        # Calculate the rate histogram
        bootstrapped_hist, _ = np.histogram(
            bootstrapped_masked_quantity_data,
            bins=bins,
            weights=bootstrapped_weigth_array,
        )
        cdf_array = np.cumsum(bootstrapped_hist) / np.sum(bootstrapped_hist)

        # Store unfiltered rate in array
        bootstrapped_rates_hist_vals[bootstrap_i] = bootstrapped_hist
        bootstrapped_rates_cdf_vals[bootstrap_i] = cdf_array

    # Calculate median and percentiles
    bootstrapped_median_percentiles_dict = get_median_percentiles(
        bootstrapped_rates_hist_vals
    )
    bootstrapped_median_percentiles_cdf_dict = get_median_percentiles(
        bootstrapped_rates_cdf_vals
    )

    hist_dict[metallicity]["median_percentiles"] = bootstrapped_median_percentiles_dict

    hist_dict[metallicity][
        "cdf_median_percentiles"
    ] = bootstrapped_median_percentiles_cdf_dict


def handle_cdf_bootstrap_plots(
    ax, hist_dict, ratio_dict, metallicity, index_i, bootstraps
):
    """
    Function to handle the CDF bootstrap plots (or not)
    """

    if bootstraps:
        lw = 4
        # Plot median and bootstrap
        ax.plot(
            hist_dict[metallicity]["centers"],
            hist_dict[metallicity]["cdf_median_percentiles"]["median"][0],
            lw=lw,
            c=color_list[index_i],
            zorder=13,
            label="Z = {} ({:.2f})".format(
                metallicity,
                ratio_dict[metallicity]["number_per_solar_mass"].sum(),
            ),
            linestyle=linestyle_list[index_i] if not index_i == 0 else "-",
        )

        #
        ax.fill_between(
            hist_dict[metallicity]["centers"],
            hist_dict[metallicity]["cdf_median_percentiles"]["90%_CI"][0],
            hist_dict[metallicity]["cdf_median_percentiles"]["90%_CI"][1],
            alpha=0.4,
            zorder=11,
            color=color_list[index_i],
        )  # 1-sigma

    else:
        ax.plot(
            hist_dict[metallicity]["centers"],
            hist_dict[metallicity]["cdf"],
            alpha=0.75,
            label="Z = {}".format(metallicity),
            linestyle=linestyle_list[index_i] if not index_i == 0 else "-",
            linewidth=4,
        )


def handle_normal_bootstrap_plots(
    ax, hist_dict, ratio_dict, metallicity, index_i, bootstraps
):
    """
    Function to handle the CDF bootstrap plots (or not)
    """

    if bootstraps:
        lw = 4
        # Plot median and bootstrap
        ax.plot(
            hist_dict[metallicity]["centers"],
            hist_dict[metallicity]["median_percentiles"]["median"][0],
            lw=lw,
            c=color_list[index_i],
            zorder=13,
            label="Z = {} ({:.2f})".format(
                metallicity,
                ratio_dict[metallicity]["number_per_solar_mass"].sum(),
            ),
            linestyle=linestyle_list[index_i] if not index_i == 0 else "-",
        )

        #
        ax.fill_between(
            hist_dict[metallicity]["centers"],
            hist_dict[metallicity]["median_percentiles"]["90%_CI"][0],
            hist_dict[metallicity]["median_percentiles"]["90%_CI"][1],
            alpha=0.4,
            zorder=11,
            color=color_list[index_i],
        )  # 1-sigma

    else:
        # Plot with the pre-calculated histogram values
        ax.plot(
            hist_dict[metallicity]["centers"],
            hist_dict[metallicity]["hist"],
            linewidth=4,
            color=color_list[index_i],
            zorder=13,
            linestyle=linestyle_list[index_i] if not index_i == 0 else "-",
        )


def add_cdf_percentage_line(ax, metallicity_hist_dict, fraction, index_i, plot_kwargs):
    """
    Function to add the cdf percentage line to the plot
    """

    y_val = fraction
    x_val = metallicity_hist_dict["centers"][
        np.argmin(np.abs(metallicity_hist_dict["cdf"] - y_val)) + 1
    ]

    ax.axhline(y_val, color="k", alpha=0.1, **plot_kwargs)
    ax.axvline(x_val, color=color_list[index_i], alpha=0.1, **plot_kwargs)


def get_median_percentiles(value_array):
    """
    Function to get the median and the percentiles from the data
    """

    result_dict = {}

    result_dict["median"] = np.percentile(value_array, [50], axis=0)

    result_dict["90%_CI"] = np.percentile(value_array, [5, 95], axis=0)

    # result_dict["1_sigma"] = np.percentile(value_array, [15.89, 84.1], axis=0)

    # result_dict["2_sigma"] = np.percentile(value_array, [2.27, 97.725], axis=0)

    return result_dict


def add_category_columns_mass(df):
    """
    Function to add the MT category columns for mass transferred
    """

    # Calculate fractions of total time spent in each category
    df["fraction_mass_transferred_through_disk_category_0"] = (
        df["total_mass_transferred_through_disk_MT_category_0"]
        / df["total_mass_transferred_through_disk"]
    )
    df["fraction_mass_transferred_through_disk_category_1"] = (
        df["total_mass_transferred_through_disk_MT_category_1"]
        / df["total_mass_transferred_through_disk"]
    )
    df["fraction_mass_transferred_through_disk_category_2"] = (
        df["total_mass_transferred_through_disk_MT_category_2"]
        / df["total_mass_transferred_through_disk"]
    )
    df["fraction_mass_transferred_through_disk_category_3"] = (
        df["total_mass_transferred_through_disk_MT_category_3"]
        / df["total_mass_transferred_through_disk"]
    )
    df["fraction_mass_transferred_through_disk_category_4"] = (
        df["total_mass_transferred_through_disk_MT_category_4"]
        / df["total_mass_transferred_through_disk"]
    )
    df["fraction_mass_transferred_through_disk_category_5"] = (
        df["total_mass_transferred_through_disk_MT_category_5"]
        / df["total_mass_transferred_through_disk"]
    )

    # Calculate error
    df["fractional_error_mass_transferred_through_disk_categories"] = 1 - (
        df["fraction_mass_transferred_through_disk_category_0"]
        + df["fraction_mass_transferred_through_disk_category_1"]
        + df["fraction_mass_transferred_through_disk_category_2"]
        + df["fraction_mass_transferred_through_disk_category_3"]
        + df["fraction_mass_transferred_through_disk_category_4"]
        + df["fraction_mass_transferred_through_disk_category_5"]
    )
    df["absolute_error_mass_transferred_through_disk_categories"] = (
        df["fractional_error_mass_transferred_through_disk_categories"]
        * df["total_mass_transferred_through_disk"]
    )

    return df


def add_number_per_formed_solarmass(df, convolution_settings):
    """
    Function to add the number per formed solar mass to the df
    """

    # Multiply the probability by a binary fraction
    df["probability"] *= convolution_settings["binary_fraction"]

    # Multiply the probability by a conversion factor to get the number per solar mass
    df["number_per_solar_mass"] = (
        df["probability"] / convolution_settings["average_mass_system"]
    )

    return df


def filter_df_stable_onto_ms(df):
    """
    Function to filter the df to include only stable MT onto MS stars
    """

    # Query the datafame
    df = df[df.initial_stability == 0]  # Stable mass transfer
    df = df[df.initial_stellar_type_accretor == 1]  # Accretion onto main sequence

    return df


def filter_on_disk_mt(df):
    """
    Function to filter the df to include disk MT only.

    We filter this on the following values:
    - total_time_spent_disk_masstransfer > 0
    - total_mass_transferred_through_disk > 0
    - total angmom stream excess returned to orbit > 0
    """

    disk_df = df[
        df.total_orbital_angular_momentum_stream_excess_returned_to_orbit > 0
    ].copy()
    disk_df = disk_df[disk_df.total_time_spent_disk_masstransfer > 0]
    disk_df = disk_df[disk_df.total_mass_transferred_through_disk > 0]

    return disk_df


def filter_df_all(df, convolution_settings, indices=None):
    """
    Function to run all the filters on the df.

    The function returns a dataframe with all stable mt onto ms, a dataframe filtered on disk mt, and the total prob of the stable mt onto ms
    """

    # Filter the dfs
    df = add_number_per_formed_solarmass(df, convolution_settings)

    # Select a specific set of indices
    if indices is not None:
        df = df.loc[indices, :]

    stable_mt_ms = filter_df_stable_onto_ms(df)
    disk_df = filter_on_disk_mt(stable_mt_ms)

    # Get total probability of ALL systems that transferring mass onto MS
    total_prob = stable_mt_ms["number_per_solar_mass"].sum()

    return stable_mt_ms, disk_df, total_prob
