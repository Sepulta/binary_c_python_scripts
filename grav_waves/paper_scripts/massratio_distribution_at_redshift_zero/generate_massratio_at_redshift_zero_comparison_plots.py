"""
Function to plot the paper plot for the primary mass at zero redshift comparison between variations

TODO: improve the re-sampling; use replacement? use weights? see https://maxhalford.github.io/blog/weighted-sampling-without-replacement/
"""

import os

import numpy as np

import h5py
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    extract_columns_from_querylist,
    quantity_name_dict,
    quantity_unit_dict,
    rate_type_name_dict,
    rate_density_units,
    add_columns_to_df,
    dco_type_query,
    readout_rate_array_specific_keys,
    create_centers_from_bins,
    get_closest_redshift_key,
    get_histogram_data,
    get_KDE_data,
    return_1d_plot_canvas,
    plot_1d_results,
    hatch_list,
    linestyle_list,
)
from grav_waves.convolution.functions.new_plot_routines.plot_kde_and_bootstrap_functions import (
    run_bootstrap,
)

from grav_waves.scripts.add_confidence_interval_powerlaw_peak.functions import (
    add_confidence_interval_powerlaw_peak_mass_ratio,
)


from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

# color_list = ["blue", "orange", "green", "red", "magenta"]


def handle_columns(combined_df, quantity, querylist, extra_columns=[]):
    """
    Function to handle adding the columns
    """

    used_columns = (
        [quantity] + extract_columns_from_querylist(querylist) + extra_columns
    )
    columns_to_add = [
        column for column in used_columns if not column in combined_df.columns
    ]
    combined_df = add_columns_to_df(combined_df, columns_to_add)

    return combined_df


def get_mask(combined_df, dco_type, general_query, query):
    """
    function to make the mask
    """

    dco_mask = combined_df.eval(dco_type_query(dco_type)).to_numpy()
    general_query_mask = (
        combined_df.eval(general_query)
        if general_query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    query_mask = (
        combined_df.eval(query)
        if query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    mask = dco_mask * general_query_mask * query_mask

    return mask


def get_rate_data(filename, rate_type, redshift_value, mask):
    """
    Function to get rate data
    """

    datafile = h5py.File(filename)
    all_redshift_keys = sorted(list(datafile["data/{}".format(rate_type)].keys()))
    closest_redshift_key = get_closest_redshift_key(redshift_value, all_redshift_keys)
    rate_array, _ = readout_rate_array_specific_keys(
        datafile=datafile,
        rate_key=rate_type,
        mask=mask,
        redshift_keys=[closest_redshift_key],
    )
    datafile.close()

    return rate_array


def get_data(
    filename,
    rate_type,
    dco_type,
    quantity,
    quantity_bins,
    divide_by_binsize,
    redshift_value,
    add_kde=False,
    bootstraps=0,
    num_procs=1,
    general_query=None,
    querylist=None,
):
    """
    Function to read out the data from the datafile
    """

    ####
    # Read out dataframe
    combined_df = pd.read_hdf(filename, "/data/combined_dataframes")

    # Add columns
    handle_columns(
        combined_df, quantity, querylist, extra_columns=["primary_undergone_ppisn"]
    )

    # Create masks and apply, taking into account that the query and general query can be None
    mask = get_mask(combined_df, dco_type, general_query, query=None)

    # Get the quantity data from the dataframe
    quantity_data = combined_df[quantity]
    primary_undergone_ppisn = combined_df["primary_undergone_ppisn"]

    # mask the quantity data
    masked_quantity_data = quantity_data[mask].to_numpy()
    masked_primary_undergone_ppisn = primary_undergone_ppisn[mask].to_numpy()

    ################
    # Read out rate data for this specific redshift
    rate_array = get_rate_data(filename, rate_type, redshift_value, mask)

    #####
    # Calculate the quantity and rate data where the primary underwent ppisn
    masked_quantity_data_where_primary_undergone_ppisn = masked_quantity_data[
        masked_primary_undergone_ppisn == 1
    ]
    rate_array_where_primary_undergone_ppisn = rate_array[
        :, masked_primary_undergone_ppisn == 1
    ]

    ################
    # Calculate the rate histogram
    hist, bincenter, truncated_bins = get_histogram_data(
        bins=quantity_bins, data_array=masked_quantity_data, weight_array=rate_array[0]
    )

    # Divide by binsize
    if divide_by_binsize:
        hist = hist / np.diff(quantity_bins)

    #
    rates_return_dict = {
        "centers": bincenter,
        "rates": hist,
    }

    # Calculate the rate histogram for the primary having undergone ppisn
    (
        hist_where_primary_undergone_ppisn,
        bincenter_where_primary_undergone_ppisn,
        truncated_bins_where_primary_undergone_ppisn,
    ) = get_histogram_data(
        bins=quantity_bins,
        data_array=masked_quantity_data_where_primary_undergone_ppisn,
        weight_array=rate_array_where_primary_undergone_ppisn[0],
    )

    # divide by binsize
    if divide_by_binsize:
        hist_where_primary_undergone_ppisn = (
            hist_where_primary_undergone_ppisn / np.diff(quantity_bins)
        )

    # Calculate the ratio between the two
    ratio_to_total_hist_where_primary_undergone_ppisn = np.divide(
        hist_where_primary_undergone_ppisn,
        hist,
        out=hist_where_primary_undergone_ppisn,
        where=hist != 0,
    )  # only divide nonzeros else 1

    #
    fraction_return_dict = {
        "centers": bincenter_where_primary_undergone_ppisn,
        "rates": ratio_to_total_hist_where_primary_undergone_ppisn,
    }

    # bootstrap
    if bootstraps:
        # Get a list of indices
        indices = np.arange(len(masked_quantity_data))

        # Set up array that stores all the boostrapped results
        bootstrapped_fraction_hist_vals = np.zeros(
            (bootstraps, len(fraction_return_dict["centers"]))
        )  # center_bins
        bootstrapped_rates_hist_vals = np.zeros(
            (bootstraps, len(rates_return_dict["centers"]))
        )  # center_bins

        #
        for bootstrap_i in range(bootstraps):
            print("Bootstrap {}".format(bootstrap_i))
            ##############################
            # Get bootstrap indices
            boot_index = np.random.choice(
                indices,
                size=len(indices),
                replace=True,
                # p=rate_array[0][indices] / np.sum(rate_array[0][indices]),
            )

            # Select the quantity values with these indices
            bootstrapped_masked_quantity_data = masked_quantity_data[boot_index]

            # Select the rate values with these indices
            bootstrapped_rate_array = rate_array[:, boot_index]

            # Select the data for whether the primary underwent PPISN or not
            bootstrapped_masked_primary_undergone_ppisn = (
                masked_primary_undergone_ppisn[boot_index]
            )

            # Calculate the quantity and rate data where the primary underwent ppisn
            bootstrapped_masked_quantity_data_where_primary_undergone_ppisn = (
                bootstrapped_masked_quantity_data[
                    bootstrapped_masked_primary_undergone_ppisn == 1
                ]
            )
            bootstrapped_rate_array_where_primary_undergone_ppisn = (
                bootstrapped_rate_array[
                    :, bootstrapped_masked_primary_undergone_ppisn == 1
                ]
            )

            ##############################
            # Calculate the rate histogram
            (
                bootstrapped_hist,
                bootstrapped_bincenter,
                bootstrapped_truncated_bins,
            ) = get_histogram_data(
                bins=quantity_bins,
                data_array=bootstrapped_masked_quantity_data,
                weight_array=bootstrapped_rate_array[0],
            )

            # Store unfiltered rate in array
            bootstrapped_rates_hist_vals[bootstrap_i] = bootstrapped_hist

            # Calculate the rate histogram for the primary having undergone ppisn
            (
                bootstrapped_hist_where_primary_undergone_ppisn,
                bootstrapped_bincenter_where_primary_undergone_ppisn,
                bootstrapped_truncated_bins_where_primary_undergone_ppisn,
            ) = get_histogram_data(
                bins=quantity_bins,
                data_array=bootstrapped_masked_quantity_data_where_primary_undergone_ppisn,
                weight_array=bootstrapped_rate_array_where_primary_undergone_ppisn[0],
            )

            # Calculate the ratio between the total rate and the one where primary underwent PPISN
            bootstrapped_ratio_to_total_hist_where_primary_undergone_ppisn = np.divide(
                bootstrapped_hist_where_primary_undergone_ppisn,
                bootstrapped_hist,
                out=bootstrapped_hist_where_primary_undergone_ppisn,
                where=bootstrapped_hist != 0,
            )  # only divide nonzeros else 1

            # Store in array
            bootstrapped_fraction_hist_vals[
                bootstrap_i
            ] = bootstrapped_ratio_to_total_hist_where_primary_undergone_ppisn

        ###########
        # Calculate median and percentiles for the unfiltered total rate
        # calculate 1- and 2- sigma percentiles TODO: fix this better. it doesnt make sense
        bootstrapped_rates_median = np.percentile(
            bootstrapped_rates_hist_vals, [50], axis=0
        )
        bootstrapped_rates_percentiles = np.percentile(
            bootstrapped_rates_hist_vals, [15.89, 84.1, 2.27, 97.725], axis=0
        )

        #
        rates_return_dict["median"] = bootstrapped_rates_median
        rates_return_dict["percentiles"] = bootstrapped_rates_percentiles

        ###########
        # Calculate median and percentiles for the ratio which undergoes ppisn
        # calculate 1- and 2- sigma percentiles
        bootstrapped_fraction_median = np.percentile(
            bootstrapped_fraction_hist_vals, [50], axis=0
        )
        bootstrapped_fraction_percentiles = np.percentile(
            bootstrapped_fraction_hist_vals, [15.89, 84.1, 2.27, 97.725], axis=0
        )

        # Store in results
        fraction_return_dict["median"] = bootstrapped_fraction_median
        fraction_return_dict["percentiles"] = bootstrapped_fraction_percentiles

    ################
    # Calculate the KDE data
    if add_kde:
        # Calculate KDE for the rates
        y_vals, center_KDEbins, kde_width = get_KDE_data(
            bins=truncated_bins,
            hist=hist,
            data_array=masked_quantity_data,
            weight_array=rate_array[0],
        )

        #
        rates_return_dict["kde_yvals"] = y_vals
        rates_return_dict["center_KDEbins"] = center_KDEbins

        ################
        # Calculate the bootstrap data
        if bootstraps:
            # TODO: Ask lieke why do we use x_KDE here instead of center_KDEbins
            _, median, percentiles = run_bootstrap(
                masses=masked_quantity_data,
                weights=rate_array[0],
                indices=np.arange(len(masked_quantity_data)),
                kde_width=kde_width,
                mass_bins=truncated_bins,
                center_KDEbins=center_KDEbins,
                num_procs=num_procs,
                bootstraps=bootstraps,
            )

            #
            rates_return_dict["median"] = median
            rates_return_dict["percentiles"] = percentiles

    #
    return_dict = {
        "rates_results": rates_return_dict,
        "fraction_results": fraction_return_dict,
    }

    return return_dict


def generate_primary_mass_at_redshift_zero_comparison_plot(
    convolved_datasets,
    massratio_bins,
    add_rates_plots=True,
    add_ratio_to_fiducial_plot=False,
    add_residual_to_fiducial_plot=False,
    add_fraction_primary_underwent_ppisn_plot=False,
    add_kde=False,
    bootstraps=0,
    plot_settings={},
):
    """
    Function to plot the paper plot for the primary mass at zero redshift comparison between variations
    """

    cmap = plt.cm.get_cmap("viridis", len(convolved_datasets) - 1)
    color_list = [cmap(i) for i in range(cmap.N)]

    # Add other color type at the end
    color_list.append("orange")

    #####################################
    # Calculate the results
    result_dict_rates = {}
    for convolved_dataset_key, convolved_dataset_filename in convolved_datasets.items():
        result_dict_rates[convolved_dataset_key] = get_data(
            filename=convolved_dataset_filename,
            rate_type="merger_rate",
            dco_type="bhbh",
            quantity="mass_ratio",
            quantity_bins=massratio_bins,
            divide_by_binsize=False,
            redshift_value=0,
            add_kde=add_kde,
            bootstraps=bootstraps,
        )

    # Settings
    linewidth = 5
    fig_scale = 15
    gs_scale = 1
    levels = (
        (2 * add_rates_plots)
        + add_ratio_to_fiducial_plot
        + add_residual_to_fiducial_plot
        + add_fraction_primary_underwent_ppisn_plot
    )
    level = 0

    # Set up figure logic
    fig = plt.figure(figsize=(1.5 * fig_scale, 1 * fig_scale))
    gs = fig.add_gridspec(nrows=levels * gs_scale, ncols=1)
    axes_list = []

    # Add rates to the plot
    if add_rates_plots:
        # set up the subplot
        rates_axis = fig.add_subplot(gs[level * gs_scale : (level + 2) * gs_scale, 0])
        level += 2
        axes_list.append(rates_axis)

        ylabel = "$\\frac{d\\mathcal{R}}{dq}$ [Gpc$^{-3}$yr$^{-1}$]"
        # ylabel = r"$\mathcal{R}_{merger}(z, M_{1})\ \left[\frac{d^{3}N_{merger}}{dt\ dm1\ dV_{\mathrm{c}}}\right]$" # personal

        # add label
        rates_axis.set_ylabel(
            ylabel,
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

        # Add results to plot
        for result_i, (result_name, result) in enumerate(result_dict_rates.items()):
            if (not add_kde) and (not bootstraps):
                # Plot histograms
                rates_axis.plot(
                    result["rates_results"]["centers"],
                    result["rates_results"]["rates"],
                    lw=linewidth,
                    c=color_list[result_i],
                    zorder=200,
                    alpha=1,
                    linestyle=linestyle_list[result_i],
                    label=result_name,
                )

            # Plot KDEs
            if add_kde:
                rates_axis.plot(
                    result["rates_results"]["center_KDEbins"],
                    result["rates_results"]["kde_yvals"],
                    lw=linewidth,
                    c=color_list[result_i],
                    zorder=13,
                )

                # Plot median and bootstrap
                rates_axis.plot(
                    result["rates_results"]["centers"],
                    result["rates_results"]["median"][0],
                    lw=linewidth,
                    c=color_list[result_i],
                    zorder=13,
                )

                rates_axis.fill_between(
                    result["rates_results"]["centers"],
                    result["rates_results"]["percentiles"][0],
                    result["rates_results"]["percentiles"][1],
                    alpha=0.4,
                    zorder=11,
                    color=color_list[result_i],
                )  # 1-sigma
                rates_axis.fill_between(
                    result["rates_results"]["centers"],
                    result["rates_results"]["percentiles"][2],
                    result["rates_results"]["percentiles"][3],
                    alpha=0.2,
                    zorder=10,
                    color=color_list[result_i],
                )  # 2-sgima

            else:
                if bootstraps:
                    # Plot median and bootstrap
                    rates_axis.plot(
                        result["rates_results"]["centers"],
                        result["rates_results"]["median"][0],
                        lw=linewidth,
                        c=color_list[result_i],
                        zorder=13,
                        linestyle=linestyle_list[result_i],
                        label=result_name,
                    )

                    rates_axis.fill_between(
                        result["rates_results"]["centers"],
                        result["rates_results"]["percentiles"][0],
                        result["rates_results"]["percentiles"][1],
                        alpha=0.4,
                        zorder=11,
                        color=color_list[result_i],
                    )  # 1-sigma
                    rates_axis.fill_between(
                        result["rates_results"]["centers"],
                        result["rates_results"]["percentiles"][2],
                        result["rates_results"]["percentiles"][3],
                        alpha=0.2,
                        zorder=10,
                        color=color_list[result_i],
                    )  # 2-sgima

        # set ylims
        rates_axis.set_ylim([1e-3, 1e1])

        # Make up
        rates_axis.set_yscale("log")

    # Add ratio to fiducial to plot
    if add_ratio_to_fiducial_plot:
        ratio_to_fiducial_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(ratio_to_fiducial_axis)

        # Add results to plot

    # add residual to fiducial plot:
    if add_residual_to_fiducial_plot:
        residual_to_fiducial_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(residual_to_fiducial_axis)

        # Add results to plot

    # Add fraction of primary underwent PPISN
    if add_fraction_primary_underwent_ppisn_plot:
        fraction_primary_underwent_ppisn_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(fraction_primary_underwent_ppisn_axis)

        #
        fraction_primary_underwent_ppisn_axis.set_ylabel(
            "Fraction primary\n underwent PPISN",
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

        # Add results to plot
        for result_i, (result_name, result) in enumerate(result_dict_rates.items()):
            # Plot histograms
            fraction_primary_underwent_ppisn_axis.plot(
                result["fraction_results"]["centers"],
                result["fraction_results"]["rates"],
                lw=linewidth,
                c=color_list[result_i],
                zorder=200,
                alpha=1,
                linestyle=linestyle_list[result_i],
            )

            # Plot KDEs
            if add_kde:
                fraction_primary_underwent_ppisn_axis.plot(
                    result["fraction_results"]["center_KDEbins"],
                    result["fraction_results"]["kde_yvals"],
                    lw=linewidth,
                    c=color_list[result_i],
                    zorder=13,
                    # label=result_name.title().replace("_", " ")
                )

            # Plot median and bootstrap
            if 1:  # TODO: update this
                if bootstraps:
                    fraction_primary_underwent_ppisn_axis.plot(
                        result["fraction_results"]["centers"],
                        result["fraction_results"]["median"][0],
                        lw=linewidth,
                        c=color_list[result_i],
                        zorder=13,
                    )

                    fraction_primary_underwent_ppisn_axis.fill_between(
                        result["fraction_results"]["centers"],
                        result["fraction_results"]["percentiles"][0],
                        result["fraction_results"]["percentiles"][1],
                        alpha=0.4,
                        zorder=11,
                        color=color_list[result_i],
                    )  # 1-sigma
                    fraction_primary_underwent_ppisn_axis.fill_between(
                        result["fraction_results"]["centers"],
                        result["fraction_results"]["percentiles"][2],
                        result["fraction_results"]["percentiles"][3],
                        alpha=0.2,
                        zorder=10,
                        color=color_list[result_i],
                    )  # 2-sgima

    ####
    # Add confidence interval
    fig, axes_list[0] = add_confidence_interval_powerlaw_peak_mass_ratio(
        fig=fig,
        ax=axes_list[0],
        data_root=os.path.join(os.environ["DATAFILES_ROOT"], "GW"),
    )

    # Remove x-ticklabels
    for axes in axes_list[:-1]:
        axes.set_xticklabels([])

    # Extra makeup
    axes_list[-1].set_xlabel(
        "{} [{}]".format(
            quantity_name_dict["primary_mass"],
            quantity_unit_dict["primary_mass"].to_string("latex"),
        ),
        fontsize=plot_settings.get("axislabel_fontsize", 26),
    )
    axes_list[0].legend(fontsize=plot_settings.get("legend_fontsize", 26))

    for axes in axes_list:
        axes.set_xlim(0, 1)

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #########
    #
    data_root = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"), "GRAV_WAVES/server_results/"
    )
    # data_root = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/"

    #########
    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"),
        "paper_gw/paper_tex/figures/massratio_distribution_at_redshift_zero_with_variations",
    )
    plot_dir = "plots"

    # #######
    # # Extra mass loss

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    #     r"$\Delta M_{\mathrm{PPI,\ Extra}} = 5$": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_5_PPISN_additional_massloss/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"$\Delta M_{\mathrm{PPI,\ Extra}} = 10$": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_10_PPISN_additional_massloss/convolution_results/convolution_results.h5",
    #     ),
    #     r"$\Delta M_{\mathrm{PPI,\ Extra}} = 15$": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_15_PPISN_additional_massloss/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"$\Delta M_{\mathrm{PPI,\ Extra}} = 20$": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_20_PPISN_additional_massloss/convolution_results/convolution_results.h5",
    #     ),
    #     # Old
    #     "Farmer et al. '19": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_FARMER19_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = (
    #     "primary_mass_distribution_at_redshift_zero_for_extra_massloss_variations.pdf"
    # )
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=np.arange(0, 100, 2),
    #     add_rates_plots=True,
    #     add_ratio_to_fiducial_plot=False,
    #     add_residual_to_fiducial_plot=False,
    #     add_fraction_primary_underwent_ppisn_plot=True,
    #     add_kde=False,
    #     bootstraps=0,
    #     plot_settings={
    #         "show_plot": False,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Core mass shift

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    #     r"$\Delta M_{\mathrm{CO,\ PPI}} = -5$": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-5_PPISN_core_mass_range_shift/convolution_results/convolution_results.h5",
    #     ),
    #     r"$\Delta M_{\mathrm{CO,\ PPI}} = -10$": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-10_PPISN_core_mass_range_shift/convolution_results/convolution_results.h5",
    #     ),
    #     r"$\Delta M_{\mathrm{CO,\ PPI}} = -15$": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"$\Delta M_{\mathrm{CO,\ PPI}} = -20$": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-20_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
    #     ),
    #     # Old prescriptions
    #     "Farmer et al. '19": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_FARMER19_FRYER_DELAYED//convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = (
    #     "primary_mass_distribution_at_redshift_zero_for_core_mass_shift_variations.pdf"
    # )
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=np.arange(0, 100, 2),
    #     add_rates_plots=True,
    #     add_fraction_primary_underwent_ppisn_plot=True,
    #     add_kde=False,
    #     bootstraps=0,
    #     plot_settings={
    #         "show_plot": False,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0.5,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Resolution study

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial HIGH RES": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    #     # Fiducial and variations
    #     "Fiducial SEMI RES": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "resolution_comparison.pdf"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=np.arange(0, 100, 2),
    #     add_rates_plots=True,
    #     add_fraction_primary_underwent_ppisn_plot=True,
    #     add_kde=False,
    #     bootstraps=50,
    #     plot_settings={
    #         "show_plot": False,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0.5,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Alpha_CE study

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial SEMI RES": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"$\alpha_{\mathrm{CE}} = 0.2$": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_alpha_ce_multiplier_0.2/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"$\alpha_{\mathrm{CE}} = 5$": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_alpha_ce_multiplier_5/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    #######
    # WR wind study

    #
    convolved_datasets = {
        # Fiducial and variations
        "Fiducial SEMI RES": os.path.join(
            data_root,
            "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
        ),
        r"$f_{\mathrm{WR}} = 0.2$": os.path.join(
            data_root,
            "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_wind_type_multiplier_WR_0.2/dco_convolution_results/convolution_results.h5",
        ),
    }

    #
    basename = "WR_wind_varition_semi_high_res.pdf"
    generate_primary_mass_at_redshift_zero_comparison_plot(
        convolved_datasets=convolved_datasets,
        massratio_bins=np.arange(0, 1, 0.05),
        add_rates_plots=True,
        add_fraction_primary_underwent_ppisn_plot=True,
        add_kde=False,
        bootstraps=50,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0.5,
            "axislabel_fontsize": 32,
            "legend_fontsize": 32,
        },
    )
