import numpy as np
import matplotlib.pyplot as plt

import emcee
import corner
import pandas as pd

plt.rcParams["figure.figsize"] = (20, 10)


def model(theta, age):
    a1, a2, a3, p1, p2, p3, T0 = theta
    model = (
        a1 * np.sin(2 * np.pi * age / p1)
        + a2 * np.sin(2 * np.pi * age / p2)
        + a3 * np.sin(2 * np.pi * age / p3)
        + T0
    )

    return model


def lnlike(theta, x, y, yerr):
    return -0.5 * np.sum(((y - model(theta, x)) / yerr) ** 2)


def lnprior(theta):
    a1, a2, a3, p1, p2, p3, T0 = theta

    if (
        (0 < a1 < 10.58)
        and (0 < a2 < 10.58)
        and (0 < a3 < 10.58)
        and (26000 - 1e4 < p1 < 26000 + 1e4)
        and (41000 - 1e4 < p2 < 41000 + 1e4)
        and (1e5 - 1e4 < p3 < 1e5 + 1e4)
        and (-10.58 < T0 < 5.46)
    ):
        return 0.0
    else:
        return -np.inf


def lnprob(theta, x, y, yerr):
    lp = lnprior(theta)
    if not np.isfinite(lp):
        return -np.inf
    return lp + lnlike(theta, x, y, yerr)


ice_data = pd.read_csv(
    "ice_core_data.txt",
    delimiter="\s+",
    names=["index", "2", "ages", "4", "delta_t"],
    index_col=0,
)
ice_data.plot(x="ages", y="delta_t")
plt.show()

age = ice_data["ages"]
T = ice_data["delta_t"]

Terr = 0.05 * np.mean(T)
data = (age, T, Terr)
nwalkers = 128
niter = 500
initial = np.array([1.0, 1.0, 1.0, 26000.0, 41000.0, 100000.0, -4.5])
ndim = len(initial)
p0 = [np.array(initial) + 1e-7 * np.random.randn(ndim) for i in range(nwalkers)]


def main(p0, nwalkers, niter, ndim, lnprob, data):
    sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob, args=data)

    print("Running burn-in...")
    p0, _, _ = sampler.run_mcmc(p0, 100)
    sampler.reset()

    print("Running production...")
    pos, prob, state = sampler.run_mcmc(p0, niter)

    return sampler, pos, prob, state


sampler, pos, prob, state = main(p0, nwalkers, niter, ndim, lnprob, data)


def plotter(sampler, age=age, T=T):
    plt.ion()
    plt.plot(age, T, label="Change in T")
    samples = sampler.flatchain
    for theta in samples[np.random.randint(len(samples), size=100)]:
        plt.plot(age, model(theta, age), color="r", alpha=0.1)
    plt.ticklabel_format(style="sci", axis="x", scilimits=(0, 0))
    plt.xlabel("Years ago")
    plt.ylabel(r"$\Delta$ T (degrees)")
    plt.legend()
    plt.show()


# sampler= main(p0)
plotter(sampler)
