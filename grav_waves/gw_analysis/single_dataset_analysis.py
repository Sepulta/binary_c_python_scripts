import os
import json
import copy
import datetime


from binarycpython.utils.plot_functions import plot_system

from grav_waves.gw_analysis.functions.functions import make_df


#
simname = "HIGH_RES_SCHNEIDER_MASS_LOSS_PPISN_OFF"
resultdir = "results/{}".format(simname)

with open(os.path.join(resultdir, "info_dict.json"), "r") as f:
    info_dict = json.loads(f.read())

#
dataset_name = "Z0.0001_bhbh"
dataset = info_dict[dataset_name]


df = make_df(dataset["filename"], dataset["metallicity"], limit_merging_time=False)
df["total_mass"] = df["mass_1"] + df["mass_2"]


print(df[df.mass_1 < 5].sort_values("mass_1")["mass_1"])


quit()
# print(df[df.total_mass>120])

single_system = df[df.total_mass > 120].iloc[3]
cmdline_arg, system_dict = get_cmdline_arg_single_system(dataset, single_system)
# print(df[df.undergone_ppisn_1==-1])
print(df[df.undergone_ppisn_2 == 1])
# print(cmdline_arg)
# print(single_system)


quit()
fig = plot_system(plot_type="mass_evolution", show_plot=True, **system_dict)
plt.show()
