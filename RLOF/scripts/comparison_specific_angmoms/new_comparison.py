"""
New script to show the impact of having an inner radius in these comparison


TODO: method to make video clip of conscecutive plots
TODO: add log scale and log sampling
TODO: add area where rmin overlaps with radius star
TODO: add plot that shows max factor at outer radius if disk: to the right
TODO: add plot that shows the q distribution at this particular radius of the accretor
"""

import matplotlib.pyplot as plt
import numpy as np
import astropy.units as u
import matplotlib.colors as colors

from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.masstransfer.functions import *

custom_mpl_settings.load_mpl_rc()

from functions import *

# Initial settings
Mdonor = 1
Maccretor = 1
a = 1
G = 1
factor_outer_edge_rl_accretor = 0.9

rloss_steps = 0.01
logq_steps = 0.01

# Set up arrays with radii and mass ratios
r_loss_array = np.arange(0.01, 1 + rloss_steps, rloss_steps)
q_array = 10 ** np.arange(-1, 1 + logq_steps, logq_steps)


inner_disk_radius = 0.02

inner_disk_radii = [0, 0.025, 0.05, 0.075, 0.1, 0.125, 0.15, 0.175, 0.2]
# inner_disk_radii = [0]
#
for inner_disk_radius in inner_disk_radii:
    print(
        "Plotting plot_disk_vs_isotropic for r_acc = {:.3f}".format(inner_disk_radius)
    )
    plot_disk_vs_isotropic(
        r_loss_array,
        q_array,
        factor_outer_edge_rl_accretor,
        inner_disk_radius=inner_disk_radius,
        include_iso=True,
        include_rcirc=True,
        include_rmin=True,
        plot_log=True,
        plot_settings={
            "show_plot": False,
            "output_name": "plots/ratio_disk_iso/racc_{:.3f}.png".format(
                inner_disk_radius
            ),
        },
    )
