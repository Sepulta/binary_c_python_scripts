import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams["axes.facecolor"] = ".9"
import proplot as pplt

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

from binarycpython.utils import stellar_types

from david_phd_functions.plotting.pdf_functions import (
    add_pdf_and_bookmark,
    generate_chapterpage_pdf,
)
from PyPDF2 import PdfFileMerger

from functions import (
    luminosity_from_temperature_for_given_radius,
    temperature_from_luminosity_for_given_radius,
)

import astropy.units as u
import astropy.constants as const


def LBV_factor(temp, lum):
    """
    Function to calculate the LBV factor
    """

    sigma_sb_converted = const.sigma_sb.to(u.Lsun / ((u.Rsun**2) * (u.K**4)))

    part_1 = 1e-5 / (4 * np.pi) ** 2
    part_2 = (lum * u.Lsun) / ((sigma_sb_converted**0.5) * (temp * u.K) ** 2)

    combined = part_1 * part_2
    lbv_factor = combined.value - 1

    return lbv_factor


# def LBV_factor(temp, lum):
#     """
#     Function to calculate the LBV factor
#     """


#     left = lum/temp**2

#     right = (10**5) * (4 * np.pi * sigma_sb_converted)**2

#     if left > right.value:
#         print('yoyoyo')
#         return 1
#     else:
#         return 0


def lum_for_temp_HD_limit(temp):
    """
    Function to calculate the lum that we need to have for HD limit for a given temp
    """

    minimum_luminosity = 600000

    sigma_sb_converted = const.sigma_sb.to(u.Lsun / ((u.Rsun**2) * (u.K**4)))
    lum_for_temp = (
        (10**5) * (4 * np.pi * sigma_sb_converted) ** (0.5) * (temp * u.K) ** 2
    ).value

    lum_for_temp[lum_for_temp < minimum_luminosity] = minimum_luminosity
    return lum_for_temp


def plot_combined_HR_diagram(
    result_dataframe_dict, color_type="rate", plot_settings=None
):
    """
    Function to plot the HR diagram
    """

    min_log10_lum = 5
    max_log10_lum = 6
    min_log10_temp = 3.4
    max_log10_temp = 6

    HD_color = "#472c28"
    HD_alpha = 0.25

    if not plot_settings:
        plot_settings = None

    # Plot
    fig = plt.figure(figsize=(16, 16))

    # Set up gridspec
    gs = fig.add_gridspec(nrows=1, ncols=10)

    # Create axis
    ax_HR = fig.add_subplot(gs[:, :8])
    ax_HR_cb = fig.add_subplot(gs[:, 9])

    # Set colormaps
    cmap1 = pplt.Colormap("viridis", l=100, name="Pacific")

    # Force the plot range to be within this. Keep in mind that this is in log
    ax_HR.set_ylim(min_log10_lum, max_log10_lum)
    ax_HR.set_xlim(min_log10_temp, max_log10_temp)

    # Create lines for the radius
    xlims = ax_HR.get_xlim()
    ylims = ax_HR.get_ylim()

    #
    log10lum_for_text = ylims[0] * 1.05
    lum_for_text = 10**log10lum_for_text

    ############
    # Plot the radii lines
    for radius in 10.0 ** np.arange(-5, 10):
        min_temp = 10 ** xlims[0]
        max_temp = 10 ** xlims[1]

        luminosity_min = luminosity_from_temperature_for_given_radius(min_temp, radius)
        luminosity_max = luminosity_from_temperature_for_given_radius(max_temp, radius)

        log10luminosity_min = np.log10(luminosity_min)
        log10luminosity_max = np.log10(luminosity_max)

        temp_for_text = temperature_from_luminosity_for_given_radius(
            lum_for_text, radius
        )
        log10temp_for_text = np.log10(temp_for_text)

        # Add radius lines
        ax_HR.plot(
            [xlims[0], xlims[1]],
            [log10luminosity_min, log10luminosity_max],
            alpha=0.25,
            color="grey",
        )

        angle = -(
            90
            - np.arctan(
                (xlims[1] - xlims[0]) / (log10luminosity_max - log10luminosity_min)
            )
        )
        # TODO: Calculate the angle better
        # ax_HR.text(log10temp_for_text, log10lum_for_text, r'%s R$_{\odot}$'%("{:.1e}".format(radius)), clip_on=True, rotation=angle, alpha=0.25, color='grey')

    ############
    # Generate the HD limit range
    log10temp_range = np.linspace(xlims[0], xlims[1], 100)
    temp_range = 10**log10temp_range
    HD_lums = lum_for_temp_HD_limit(temp_range)
    log10HD_lums = np.log10(HD_lums)
    top_line = np.ones(log10temp_range.shape) * ylims[1]

    ax_HR.plot(log10temp_range, log10HD_lums, "--", color=HD_color, alpha=HD_alpha)

    # Fill region
    ax_HR.fill_between(
        log10temp_range,
        log10HD_lums,
        top_line,
        where=(top_line > log10HD_lums),
        facecolor=HD_color,
        alpha=HD_alpha,
    )

    # Plot stellar type evolution
    for i, dataframe_key in enumerate(result_dataframe_dict):
        evolution_dataframe = result_dataframe_dict[dataframe_key][1]

        temp_shift = (xlims[1] - xlims[0]) / 20

        # Plot and scatter
        ax_HR.plot(
            np.log10(evolution_dataframe["Teff"]),
            np.log10(evolution_dataframe["luminosity"]),
            alpha=0.5,
        )
        ax_HR.text(
            np.log10(evolution_dataframe["Teff"].to_numpy())[0] + temp_shift,
            np.log10(evolution_dataframe["luminosity"].to_numpy())[0],
            "{}".format(dataframe_key),
            clip_on=True,
            # rotation=angle,
            # alpha=0.25,
            # color='grey'
        )

        if color_type == "rate":
            ax_HR_plot = ax_HR.scatter(
                np.log10(evolution_dataframe["Teff"]),
                np.log10(evolution_dataframe["luminosity"]),
                s=200,
                c=np.log10(np.abs(evolution_dataframe["wind_mass_loss_rate"])),
                cmap=cmap1,
            )
        elif color_type == "age":
            ax_HR_plot = ax_HR.scatter(
                np.log10(evolution_dataframe["Teff"]),
                np.log10(evolution_dataframe["luminosity"]),
                s=200,
                c=evolution_dataframe["time"],
                cmap=cmap1,
            )
        else:
            print("Unknown option, aborting")
            raise ValueError()

    fig.colorbar(ax_HR_plot, cax=ax_HR_cb)

    ######################
    # Make up

    # invert_xaxisvert axis
    ax_HR.invert_xaxis()

    #
    ax_HR.set_ylabel(r"log$_{10}$(L/L$_{\odot}$)")
    ax_HR.set_xlabel(r"log$_{10}$(T$_{eff}$/K)")
    if color_type == "rate":
        ax_HR_cb.set_ylabel(r"Wind mass loss rate [M$_{\odot}$]")
    elif color_type == "age":
        ax_HR_cb.set_ylabel(r"Age [Myr]")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_HR_diagram(result_dataframe_dict, color_type="rate", plot_settings=None):
    """
    Function to plot the HR diagram
    """

    min_log10_lum = 5
    max_log10_lum = 7
    min_log10_temp = 3.25
    max_log10_temp = 5

    if not plot_settings:
        plot_settings = None

    # Plot
    fig = plt.figure(figsize=(16, 16))

    # Set up gridspec
    gs = fig.add_gridspec(nrows=1, ncols=10)

    # Create axis
    ax_HR = fig.add_subplot(gs[:, :8])
    ax_HR_cb = fig.add_subplot(gs[:, 9])

    # Set colormaps
    cmap1 = pplt.Colormap("viridis", l=100, name="Pacific")

    # Force the plot range to be within this. Keep in mind that this is in log
    ax_HR.set_ylim(min_log10_lum, max_log10_lum)
    ax_HR.set_xlim(min_log10_temp, max_log10_temp)

    # Create lines for the radius
    xlims = ax_HR.get_xlim()
    ylims = ax_HR.get_ylim()

    #
    log10lum_for_text = ylims[0] * 1.05
    lum_for_text = 10**log10lum_for_text

    ############
    # Plot the radii lines
    for radius in 10.0 ** np.arange(-5, 10):
        min_temp = 10 ** xlims[0]
        max_temp = 10 ** xlims[1]

        luminosity_min = luminosity_from_temperature_for_given_radius(min_temp, radius)
        luminosity_max = luminosity_from_temperature_for_given_radius(max_temp, radius)

        log10luminosity_min = np.log10(luminosity_min)
        log10luminosity_max = np.log10(luminosity_max)

        temp_for_text = temperature_from_luminosity_for_given_radius(
            lum_for_text, radius
        )
        log10temp_for_text = np.log10(temp_for_text)

        # Add radius lines
        ax_HR.plot(
            [xlims[0], xlims[1]],
            [log10luminosity_min, log10luminosity_max],
            alpha=0.25,
            color="grey",
        )

        angle = -(
            90
            - np.arctan(
                (xlims[1] - xlims[0]) / (log10luminosity_max - log10luminosity_min)
            )
        )
        # TODO: Calculate the angle better
        # ax_HR.text(log10temp_for_text, log10lum_for_text, r'%s R$_{\odot}$'%("{:.1e}".format(radius)), clip_on=True, rotation=angle, alpha=0.25, color='grey')

    HD_color = "#472c28"
    HD_alpha = 0.25

    ############
    # Generate the HD limit range
    log10temp_range = np.linspace(xlims[0], xlims[1], 100)
    temp_range = 10**log10temp_range
    HD_lums = lum_for_temp_HD_limit(temp_range)
    log10HD_lums = np.log10(HD_lums)
    top_line = np.ones(log10temp_range.shape) * ylims[1]

    ax_HR.plot(log10temp_range, log10HD_lums, "--", color=HD_color, alpha=HD_alpha)

    # Fill region
    ax_HR.fill_between(
        log10temp_range,
        log10HD_lums,
        top_line,
        where=(top_line > log10HD_lums),
        facecolor=HD_color,
        alpha=HD_alpha,
    )

    # Plot stellar type evolution
    for i, dataframe_key in enumerate(result_dataframe_dict):
        evolution_dataframe = result_dataframe_dict[dataframe_key][1]

        print(evolution_dataframe)
        # Plot and scatter
        ax_HR.plot(
            np.log10(evolution_dataframe["Teff"]),
            np.log10(evolution_dataframe["luminosity"]),
            alpha=0.5,
        )

        if color_type == "rate":
            ax_HR_plot = ax_HR.scatter(
                np.log10(evolution_dataframe["Teff"]),
                np.log10(evolution_dataframe["luminosity"]),
                s=200,
                c=np.log10(np.abs(evolution_dataframe["wind_mass_loss_rate"])),
                cmap=cmap1,
            )
        elif color_type == "age":
            ax_HR_plot = ax_HR.scatter(
                np.log10(evolution_dataframe["Teff"]),
                np.log10(evolution_dataframe["luminosity"]),
                s=200,
                c=evolution_dataframe["time"],
                cmap=cmap1,
            )
        else:
            print("Unknown option, aborting")
            raise ValueError()

    fig.colorbar(ax_HR_plot, cax=ax_HR_cb)

    ######################
    # Make up

    # invert_xaxisvert axis
    ax_HR.invert_xaxis()

    #
    ax_HR.set_ylabel(r"log$_{10}$(L/L$_{\odot}$)")
    ax_HR.set_xlabel(r"log$_{10}$(T$_{eff}$/K)")
    if color_type == "rate":
        ax_HR_cb.set_ylabel(r"Wind mass loss rate [M$_{\odot}$]")
    elif color_type == "age":
        ax_HR_cb.set_ylabel(r"Age [Myr]")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_detailed_evolution(evolution_dataframe, plot_settings=None):
    """
    Function to plot detailed evolution of the
    """

    if not plot_settings:
        plot_settings = None

    # Plot
    fig = plt.figure(figsize=(16, 16))

    # Set up gridspec
    gs = fig.add_gridspec(nrows=5, ncols=1)

    # Create axis
    ax_masses = fig.add_subplot(gs[0, :])
    ax_luminosity = fig.add_subplot(gs[1, :])
    ax_radius = fig.add_subplot(gs[2, :])
    ax_stellar_type = fig.add_subplot(gs[3, :])
    ax_wind_mass_loss_rate = fig.add_subplot(gs[4, :])

    # Mass plots
    ax_masses.plot(
        evolution_dataframe["time"], evolution_dataframe["mass"], label="mass"
    )

    ax_masses.plot(
        evolution_dataframe["time"],
        evolution_dataframe["co_core_mass"],
        "-.",
        alpha=0.5,
        label="co_core mass",
    )
    ax_masses.plot(
        evolution_dataframe["time"],
        evolution_dataframe["he_core_mass"],
        ":",
        alpha=0.5,
        label="he_core mass",
    )
    ax_masses.legend()

    # Luminosity
    ax_luminosity.plot(evolution_dataframe["time"], evolution_dataframe["luminosity"])
    ax_luminosity.set_yscale("log")

    # Radius
    ax_radius.plot(evolution_dataframe["time"], evolution_dataframe["radius"])
    ax_radius.set_yscale("log")

    # stellar type
    ax_stellar_type.plot(
        evolution_dataframe["time"], evolution_dataframe["stellar_type"]
    )
    ax_stellar_type.set_ylim(0, evolution_dataframe["stellar_type"].unique().max() + 1)

    # add second y axis stellar types
    for st in evolution_dataframe["stellar_type"].unique():
        ax_stellar_type.hlines(
            st,
            evolution_dataframe["time"].iloc[0],
            evolution_dataframe["time"].iloc[-1],
            edgecolor="m",
            linestyle="--",
            zorder=-1,
            linewidth=2,
            alpha=0.5,
        )

    unique_st = evolution_dataframe["stellar_type"].unique()

    ax_stellar_type_ylims = ax_stellar_type.get_ylim()
    y_2_range_st = range(int(ax_stellar_type_ylims[0]), int(ax_stellar_type_ylims[1]))
    st_names = [
        stellar_types.STELLAR_TYPE_DICT_SHORT[el] if el in unique_st else ""
        for el in y_2_range_st
    ]

    ax_stellar_type_twin_y = ax_stellar_type.twinx()
    ax_stellar_type_twin_y.set_ylim(ax_stellar_type.get_ylim())  # same limits
    ax_stellar_type_twin_y.set_yticks(list(y_2_range_st))
    ax_stellar_type_twin_y.set_yticklabels(st_names, fontsize=15)

    # wind mass loss
    ax_wind_mass_loss_rate.plot(
        evolution_dataframe["time"], np.abs(evolution_dataframe["wind_mass_loss_rate"])
    )
    ax_wind_mass_loss_rate.set_yscale("log")

    # makeup
    ax_wind_mass_loss_rate.set_xlabel("Time [Myr]")
    ax_wind_mass_loss_rate.set_title("Wind mass loss rate")
    ax_wind_mass_loss_rate.set_ylabel(r"M$_{\odot}$ yr$^{-1}$")

    ax_stellar_type.set_title("Stellar type")
    ax_stellar_type.set_ylabel("Type")

    ax_radius.set_title("Radius")
    ax_radius.set_ylabel(r"R$_{\odot}$")

    ax_luminosity.set_title("Luminosity")
    ax_luminosity.set_ylabel(r"L$_{\odot}$")

    ax_masses.set_title("Masses")
    ax_masses.set_ylabel(r"M$_{\odot}$")

    plot_settings["hspace"] = 0.5

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_combined_detailed_evolution(result_dataframe_dict, plot_settings=None):
    """ """

    plot_evolution_in_logtime = False

    # Plot
    fig = plt.figure(figsize=(30, 30))
    fig.subplots_adjust(wspace=1)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=9, ncols=5)

    # Set frames
    ax_stellar_type_evolution = fig.add_subplot(gs[:2, :3])
    ax_wind_mass_loss_rate_evolution = fig.add_subplot(
        gs[2:5, :3], sharex=ax_stellar_type_evolution
    )
    ax_mass_evolution = fig.add_subplot(gs[5:7, :3], sharex=ax_stellar_type_evolution)
    ax_timestep_evolution = fig.add_subplot(
        gs[7:, :3], sharex=ax_stellar_type_evolution
    )

    ax_sn_stellar_types = fig.add_subplot(gs[:2, 3:])
    ax_sn_masses = fig.add_subplot(gs[2:5, 3:], sharex=ax_sn_stellar_types)

    # get pre-sn info
    zams_masses = []
    masses = []
    pre_sn_mass = []
    pre_sn_stellar_type = []
    pre_sn_core_mass = []
    pre_sn_co_core_mass = []
    pre_sn_he_core_mass = []

    #
    for i, dataframe_key in enumerate(result_dataframe_dict):
        supernova_dict = result_dataframe_dict[dataframe_key][0][0]

        zams_masses.append(dataframe_key)
        pre_sn_mass.append(supernova_dict["pre_sn_mass"])
        pre_sn_stellar_type.append(supernova_dict["pre_sn_stellar_type"])
        pre_sn_core_mass.append(supernova_dict["pre_sn_core_mass"])
        pre_sn_co_core_mass.append(supernova_dict["pre_sn_co_core_mass"])
        pre_sn_he_core_mass.append(supernova_dict["pre_sn_he_core_mass"])
        masses.append(supernova_dict["mass"])

    # Plot SN stuff
    ax_sn_masses.plot(
        zams_masses,
        pre_sn_mass,
        linestyle=custom_mpl_settings.LINESTYLE_TUPLE[0][0],
        label="pre_sn_mass",
    )
    ax_sn_masses.plot(
        zams_masses,
        pre_sn_core_mass,
        linestyle=custom_mpl_settings.LINESTYLE_TUPLE[1][0],
        label="pre_sn_core_mass",
    )
    ax_sn_masses.plot(
        zams_masses,
        pre_sn_co_core_mass,
        linestyle=custom_mpl_settings.LINESTYLE_TUPLE[2][0],
        label="pre_sn_co_core_mass",
    )
    ax_sn_masses.plot(
        zams_masses,
        pre_sn_he_core_mass,
        linestyle=custom_mpl_settings.LINESTYLE_TUPLE[3][0],
        label="pre_sn_he_core_mass",
    )
    ax_sn_masses.plot(zams_masses, masses, label="masses")
    ax_sn_masses.set_ylabel("Mass")
    ax_sn_masses.set_xlabel("ZAMS mass")
    ax_sn_masses.legend(framealpha=0.5)

    # Plot stellar types
    ax_sn_stellar_types.plot(
        zams_masses, pre_sn_stellar_type, label="pre-SN stellar type"
    )
    ax_sn_stellar_types.legend(framealpha=0.5)
    ax_sn_stellar_types.set_ylabel("Stellar type")

    # Plot stellar type evolution
    for i, dataframe_key in enumerate(result_dataframe_dict):
        dataframe = result_dataframe_dict[dataframe_key][1]
        ax_stellar_type_evolution.plot(
            dataframe["time"],
            dataframe["stellar_type"],
            label=dataframe_key,
            # linestyle=custom_mpl_settings.LINESTYLE_TUPLE[i][0]
        )
    ax_stellar_type_evolution.set_ylabel("Stellar type")
    ax_stellar_type_evolution.legend(framealpha=0.5)

    # Timestep evo
    for i, dataframe_key in enumerate(result_dataframe_dict):
        dataframe = result_dataframe_dict[dataframe_key][1]
        ax_timestep_evolution.plot(
            dataframe["time"].to_numpy()[1:],
            np.log10(
                dataframe["time"].to_numpy()[1:] - dataframe["time"].to_numpy()[:-1]
            ),
            label=dataframe_key,
        )
    ax_timestep_evolution.set_ylabel("timestep [Myr]")
    ax_timestep_evolution.legend(framealpha=0.5)
    # ax_timestep_evolution.set_yscale('log')

    #############################
    # wind rate evolution
    for i, dataframe_key in enumerate(result_dataframe_dict):
        dataframe = result_dataframe_dict[dataframe_key][1]
        ax_wind_mass_loss_rate_evolution.scatter(
            dataframe["time"],
            np.log10(np.abs(dataframe["wind_mass_loss_rate"].to_numpy())),
            # marker='o',
            # label=dataframe_key,
            # linestyle=custom_mpl_settings.LINESTYLE_TUPLE[i][0]
        )

    # makeup
    ax_wind_mass_loss_rate_evolution.set_xlabel("Time [Myr]")
    ax_wind_mass_loss_rate_evolution.set_title("Wind mass loss rate")
    ax_wind_mass_loss_rate_evolution.set_ylabel(r"M$_{\odot}$ yr$^{-1}$")
    # ax_wind_mass_loss_rate_evolution.set_yscale("log")
    ax_wind_mass_loss_rate_evolution.legend(loc=2)

    #############################
    # Mass evolution
    for i, dataframe_key in enumerate(result_dataframe_dict):
        dataframe = result_dataframe_dict[dataframe_key][1]
        p = ax_mass_evolution.plot(
            dataframe["time"],
            dataframe["mass"],
            label=dataframe_key,
        )
        ax_mass_evolution.plot(
            dataframe["time"],
            dataframe["co_core_mass"],
            label="Co core",
            linestyle=custom_mpl_settings.LINESTYLE_TUPLE[1][1],
            color=p[0].get_color(),
            alpha=0.5,
        )
        ax_mass_evolution.plot(
            dataframe["time"],
            dataframe["he_core_mass"],
            label="He core",
            linestyle=custom_mpl_settings.LINESTYLE_TUPLE[2][1],
            color=p[0].get_color(),
            alpha=0.5,
        )
    # ax_masses.plot(evolution_dataframe['time'], evolution_dataframe['co_core_mass'], '-.', alpha=0.5, label='co_core mass')
    # ax_masses.plot(evolution_dataframe['time'], evolution_dataframe['he_core_mass'], ':', alpha=0.5, label='he_core mass')
    # ax_masses.legend()

    # makeup
    ax_mass_evolution.set_xlabel("Time [Myr]")
    ax_mass_evolution.set_title("Mass evolution")
    ax_mass_evolution.set_ylabel(r"M")
    ax_mass_evolution.legend(
        loc=3, ncol=len(result_dataframe_dict.keys()), framealpha=0.5
    )

    plot_settings["hspace"] = 1

    if plot_evolution_in_logtime:
        ax_mass_evolution.set_xscale("log")
        ax_wind_mass_loss_rate_evolution.set_xscale("log")
        ax_stellar_type_evolution.set_xscale("log")
        ax_timestep_evolution.set_xscale("log")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


# HD_lums = np.log10(lum_for_temp_HD_limit(temp_range))
# XX, YY = np.meshgrid(temp_range, lum_range)
# # lum_for_temp_HD_limit(temp_range)
# LBV_factors = LBV_factor(XX, YY)
# LBV_factors[LBV_factors>0] = 1
# LBV_factors[YY<np.log10(600000)] = 0
# ax_HR.tricontourf(np.log10(XX.ravel()), np.log10(YY.ravel()), LBV_factors.ravel(), levels=[0.5, 2], colors=['blue'])
