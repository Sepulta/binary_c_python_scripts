import os
import json
import time
import pickle
import sys

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import (
    get_help_all,
    get_help,
    create_hdf5,
    output_lines,
)

from david_phd_functions.binaryc.personal_defaults import personal_defaults

"""
"""


def parse_function(self, output):
    """ """

    # extract info from the population instance
    # TODO: think about whether this is smart. Passing around this object might be an overkill

    # Get some information from the
    data_dir = self.custom_options["data_dir"]

    # # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # Create filename
    parameters = ["time", "mass_1", "zams_mass_1", "tm", "tn", "tkh"]

    # # Get indices of initial values
    # mass_1_index = parameters.index('zams_mass_1')
    # mass_2_index = parameters.index('zams_mass_2')
    # orbital_period_index = parameters.index('zams_period')
    # metallicity_index = parameters.index('metallicity')

    # separator='\t'

    # for line in output_lines(output):
    #     headerline = line.split()[0]
    #     if headerline=='DAVID_RLOFING':
    #         values = line.split()[1:]
    #         rlof_counter = int(values[rlof_counter_index])

    #         base_filename = "m1={}_m2={}_per={}_z={}_nr={}.dat".format(
    #             values[mass_1_index],
    #             values[mass_2_index],
    #             values[orbital_period_index],
    #             values[metallicity_index],
    #             rlof_counter)

    #         outfilename = os.path.join(data_dir, base_filename)

    #         if not os.path.exists(outfilename):
    #             with open(outfilename, 'w') as f:
    #                 f.write(separator.join(parameters)+'\n')

    #         with open(outfilename, 'a') as f:
    #             f.write(separator.join(values)+'\n')


test_pop.set(**personal_defaults)

test_pop.set(
    # david_logging_function=10,
    binary=0,
    amt_cores=1,
)

test_pop.add_grid_variable(
    name="lnm1",
    longname="Primary mass",
    valuerange=[1, 10],
    resolution="{}".format(resolution["M_1"]),
    spacingfunc="const(1, 10, {})".format(10),
    precode="M_1=math.exp(lnm1)",
    probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 100, -1.3, -2.3, -2.3)*M_1",
    dphasevol="dlnm1",
    parameter_name="M_1",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
)

# single evol to check things
# test_pop.set(M_1=150, M_2=112.525, orbital_period=0.359381)
# out = test_pop.evolve_single()
# print(out)

# test_pop.set()
# test_pop.evolve_population()
