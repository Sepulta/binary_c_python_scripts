import numpy as np
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()
# help(np.random)

K = 4

# print(np.random.rand(K))


def mean(vector):
    return sum(vector) / len(vector)


from scipy.stats import kurtosis
from scipy.stats import skew


def sample_statistics(K):
    vec = np.random.rand(K)

    mean_val = mean(vec)
    variance = np.var(vec)
    kurt = kurtosis(vec)
    skewness = skew(vec)

    stat_dict = {
        "mean": mean_val,
        "variance": variance,
        "kurtosis": kurt,
        "skewness": skewness,
    }

    return stat_dict


import matplotlib.pyplot as plt

k_vals = []
mean_vals = []
var_vals = []
kurt_vals = []
skew_vals = []

for el in range(1, 11):
    k = 4**el
    k_vals.append(k)
    stats = sample_statistics(k)

    mean_vals.append(stats["mean"])
    var_vals.append(stats["variance"])
    kurt_vals.append(stats["kurtosis"])
    skew_vals.append(stats["skewness"])

fig, axes = plt.subplots(nrows=2, ncols=2)


axes[0][0].axhline(0.5, label="True value")
axes[0][0].set_title("mean")
axes[0][0].plot(k_vals, mean_vals)
axes[0][0].set_xscale("log", basex=2)

axes[0][1].axhline(1.0 / 12.0, label="True value")
axes[0][1].set_title("variance")
axes[0][1].plot(k_vals, var_vals)
axes[0][1].set_xscale("log", basex=2)

axes[1][0].axhline(0, label="True value")
axes[1][0].set_title("Skew")
axes[1][0].plot(k_vals, skew_vals)
axes[1][0].set_xscale("log", basex=2)

axes[1][1].axhline(-6 / 5, label="True value")
axes[1][1].set_title("kurtosis")
axes[1][1].plot(k_vals, kurt_vals)
axes[1][1].set_xscale("log", basex=2)

plt.show()
print(sample_statistics(K))
