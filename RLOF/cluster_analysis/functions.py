"""
Function for the readout of the clusters
"""

import random
import statistics
import numpy as np
from RLOF.cluster_analysis.gios_codes import ndtest


def bootstrap_syrte(observation_dict, population_dict, n_bootstraps=1000):
    """
    Function to perform bootstrap sampling of the observations and self test
    """

    # Set up some result lists
    self_p2 = []
    self_kss2 = []

    comp_p2 = []
    comp_kss2 = []

    # Extract information
    observation_period_array = observation_dict["orbital_period_in_days"]
    observation_eccentricity_array = observation_dict["orbital_eccentricity"]
    num_observations = len(observation_period_array)

    population_period_array = population_dict["orbital_period_in_days"]
    population_eccentricity_array = population_dict["orbital_eccentricity"]
    population_weight_array = population_dict["probability"]
    num_populations = len(population_period_array)

    # Perform bootstrapping
    for bootstrap_iteration in range(n_bootstraps):

        # Do two random index choices and select the periods and eccentricities
        bootstrap_index_array_1 = random.choices(
            range(num_populations), weights=population_weight_array, k=num_observations
        )
        bootstrap_index_array_2 = random.choices(
            range(num_populations), weights=population_weight_array, k=num_observations
        )

        bootstrap_population_period_array_1 = population_period_array[
            bootstrap_index_array_1
        ]
        bootstrap_population_period_array_2 = population_period_array[
            bootstrap_index_array_2
        ]

        bootstrap_population_eccentricity_array_1 = population_eccentricity_array[
            bootstrap_index_array_1
        ]
        bootstrap_population_eccentricity_array_2 = population_eccentricity_array[
            bootstrap_index_array_2
        ]

        ################
        # Test self-match between two identical samples
        ks_test1 = ndtest.ks2d2s(
            bootstrap_population_period_array_1,
            bootstrap_population_eccentricity_array_1,
            bootstrap_population_period_array_1,
            bootstrap_population_eccentricity_array_1,
            extra=True,
        )
        #        print('Test pop1 vs pop1 = {}'.format( float(ks_test1[0])) )

        ################
        # Compute distance between two samples from the same pop
        #        print('Testing with syrte/ndtest twodim ks')
        ks_self = ndtest.ks2d2s(
            bootstrap_population_period_array_1,
            bootstrap_population_eccentricity_array_1,
            bootstrap_population_period_array_2,
            bootstrap_population_eccentricity_array_2,
            extra=True,
        )

        #        print('Test pop1 v pop2 = {}'.format( float(ks_self[0])) )
        self_p2.append(ks_self[0])
        self_kss2.append(ks_self[1])

        ################
        # Compute distance between pop sample and observations
        ks_comp = ndtest.ks2d2s(
            observation_period_array,
            observation_eccentricity_array,
            bootstrap_population_period_array_1,
            bootstrap_population_eccentricity_array_1,
            extra=True,
        )

        #        print('Test obs v bse1 = {}'.format( float(ks_comp[0])) )
        comp_p2.append(ks_comp[0])
        comp_kss2.append(ks_comp[1])

    # We also want to compute the statistically relevant quantities
    self_kss2_mean = statistics.mean(self_kss2)
    self_kss2_sigma = statistics.stdev(self_kss2)

    comp_kss2_mean = statistics.mean(comp_kss2)
    comp_kss2_sigma = statistics.stdev(comp_kss2)

    # print("self bootstrap, mean={}, sigma={}".format(self_kss2_mean, self_kss2_sigma))
    # print("comp bootstrap, mean={}, sigma={}".format(comp_kss2_mean, comp_kss2_sigma))

    return_dict = {
        "self_kss2_mean": self_kss2_mean,
        "self_kss2_sigma": self_kss2_sigma,
        "comp_kss2_mean": comp_kss2_mean,
        "comp_kss2_sigma": comp_kss2_sigma,
        "self_kss_array": np.array(self_kss2),
        "comp_kss_array": np.array(comp_kss2),
    }

    return return_dict


def normalize_df(df, weight_column="probability"):
    """
    Function to normalize a dataframe
    """

    df.loc[:, weight_column] = df[weight_column] / np.sum(df[weight_column])


def get_statistics(cluster_df, cluster_observations, n_bootstraps=100):
    """
    Function to get statistics on the match between the simulations and the observations.

    The input cluster df needs to contain the columns '' and '', and is expected to have cuts made to it already (it will be used as is)
    """

    # Select the correct columns
    population_dict = {
        "orbital_period_in_days": cluster_df["orbital_period_in_days"].to_numpy(),
        "orbital_eccentricity": cluster_df["orbital_eccentricity"].to_numpy(),
        "probability": cluster_df["probability"].to_numpy(),
    }

    # Perform bootstrapping and KS2 test distances
    statistics_dict = bootstrap_syrte(
        observation_dict=cluster_observations,
        population_dict=population_dict,
        n_bootstraps=n_bootstraps,
    )

    return statistics_dict
