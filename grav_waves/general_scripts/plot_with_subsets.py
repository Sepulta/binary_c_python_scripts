"""
Function that will take the data of the subsets and show plots.
"""

import os
import pickle
import numpy as np
from grav_waves.settings import liekes_channels_query_dict

from david_phd_functions.plotting.canvas_functions import return_canvas_with_subsets

import matplotlib as mpl

import matplotlib.pyplot as plt
import matplotlib.colors as colors

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

from scipy.ndimage.filters import gaussian_filter


def select_time_sliced_results(convolution_dataset, time_bin_key, rate_key, mass_key):
    """
    Function to select the time sliced results
    """

    if time_bin_key in convolution_dataset["time_binned"]["results"].keys():
        # if we do want to plot the time slice we need to select that data again:
        sliced_results = convolution_dataset["time_binned"]["results"][time_bin_key][
            rate_key
        ][mass_key]

        #
        sliced_results = sliced_results.T

        #
        plot_results = sliced_results
    else:
        msg = "Couldnt find the passed time_bin_key in the known keys. Aborting"
        raise KeyError(msg)

    return plot_results


####################
# Convolved merger rates 2d
def plot_convolved_rate_density_per_mass_quantity_with_subsets(
    convolution_dataset_dict,
    rate_type,
    mass_type,
    time_bin_key=None,
    plot_settings=None,
):
    """
    Plot for the convolved rate density (i.e. rate per gigaparsec cubed)

    Args:
        - convolution_dataset: dataset containing all the information about the simulation, results and initial settings
        - rate_type: option to plot either merger rate or formation rate
        - mass_type: option to plot which mass

        - time_bin_key (Optional): If passed then we plot only the results of that certain time bin
    """

    #
    if not plot_settings:
        plot_settings = {}

    # Get the data and split it off:
    all_convolution_dataset = convolution_dataset_dict["all"]

    subset_convolution_dataset_dict = {
        el: convolution_dataset_dict[el]
        for el in convolution_dataset_dict.keys()
        if not el == "all"
    }

    # unpack the convolution dataset
    convolution_settings = all_convolution_dataset["convolution_configuration"]

    # Get correct rate keyname:
    if rate_type == "merger_rate":
        rate_key = "merger_rates"
        long_name_rate_type = "Merger rate"
    elif rate_type == "formation_rate":
        rate_key = "formation_rates"
        long_name_rate_type = "Formation rate"

    # Get correct mass keyname
    if mass_type == "chirpmass":
        mass_bins = convolution_settings["chirpmass_bins"]
        mass_key = "result_array_chirpmass"
        long_name_mass = "Chirp mass"
    elif mass_type == "total_mass":
        mass_bins = convolution_settings["total_mass_bins"]
        mass_key = "result_array_total_mass"
        long_name_mass = "Total mass"
    elif mass_type == "primary_mass":
        mass_bins = convolution_settings["primary_mass_bins"]
        mass_key = "result_array_primary_mass"
        long_name_mass = "Primary mass"
    elif mass_type == "any_mass":
        mass_bins = convolution_settings["any_mass_bins"]
        mass_key = "result_array_any_mass"
        long_name_mass = "Any mass"

    #
    timebins = convolution_settings["time_bins"]

    # Get figure and axes etc
    amt_subsets = len(subset_convolution_dataset_dict.keys())
    fig, gs, axes_dict = return_canvas_with_subsets(amt_subsets)

    # Plot the total set of results
    all_results_axis = axes_dict["all_axis"]
    all_results_axis.set_title("All", fontsize=30)

    all_rate_result_dict = all_convolution_dataset["results"][rate_key]
    all_main_results = all_rate_result_dict[mass_key]

    # Decide which results will be plotted
    plot_results = all_main_results.T
    if not time_bin_key is None:
        plot_results = select_time_sliced_results(
            convolution_dataset, time_bin_key, rate_key, mass_key
        )

    # make dummy hist
    _, xedges, yedges = np.histogram2d(
        np.array([]), np.array([]), bins=[timebins, mass_bins]
    )

    # Rotate dataset and set up meshgrid
    all_main_results = all_main_results.T
    X, Y = np.meshgrid(xedges, yedges)

    # Plot the results
    all_results_hist = all_results_axis.pcolormesh(
        X,
        Y,
        plot_results.value,
        norm=colors.LogNorm(
            vmin=10
            ** np.floor(
                np.log10(all_main_results.value.max())
                - plot_settings.get("probability_floor", 4)
            ),
            vmax=all_main_results.value.max(),
        ),
        shading="auto",
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )

    contour_levels = [1e0, 1e1, 1e2]

    X, Y = np.meshgrid(timebins[1:], mass_bins[1:])
    CS = all_results_axis.contour(
        X,
        Y,
        plot_results.value,
        # gaussian_filter(plot_results.value, 5.),
        levels=contour_levels,
        colors="k",
        linestyles=["solid", "dashed", "-."],
    )

    # Plot all the subsets
    for i, subset_name in enumerate(subset_convolution_dataset_dict.keys()):

        subset_axis = axes_dict["subset_axes"][i]
        subset_convolution_dataset = convolution_dataset_dict[subset_name]

        subset_axis.set_title(subset_name, fontsize=30)

        #
        subset_rate_result_dict = subset_convolution_dataset["results"][rate_key]
        subset_main_results = subset_rate_result_dict[mass_key]

        # Decide which results will be plotted
        subset_plot_results = subset_main_results.T
        if not time_bin_key is None:
            subset_plot_results = select_time_sliced_results(
                convolution_dataset, time_bin_key, rate_key, mass_key
            )

        # make dummy hist
        _, xedges, yedges = np.histogram2d(
            np.array([]), np.array([]), bins=[timebins, mass_bins]
        )

        # Rotate dataset and set up meshgrid
        subset_main_results = subset_main_results.T
        X, Y = np.meshgrid(xedges, yedges)

        # Plot the results
        subset_results_hist = subset_axis.pcolormesh(
            X,
            Y,
            subset_plot_results.value,
            norm=colors.LogNorm(
                vmin=10
                ** np.floor(
                    np.log10(all_main_results.value.max())
                    - plot_settings.get("probability_floor", 4)
                ),
                vmax=all_main_results.value.max(),
            ),
            shading="auto",
            antialiased=plot_settings.get("antialiased", False),
            rasterized=plot_settings.get("rasterized", False),
        )

        X, Y = np.meshgrid(timebins[1:], mass_bins[1:])
        CS = subset_axis.contour(
            X,
            Y,
            subset_plot_results.value,
            levels=contour_levels,
            colors="k",
            linestyles=["solid", "dashed", "-."],
        )

    norm = colors.LogNorm(
        vmin=10
        ** np.floor(
            np.log10(all_main_results.value.max())
            - plot_settings.get("probability_floor", 4)
        ),
        vmax=all_main_results.value.max(),
    )
    cb = mpl.colorbar.ColorbarBase(
        axes_dict["colorbar_axis"],
        cmap=plt.get_cmap(),
        norm=norm,
    )
    cb.set_label("Some Units")

    # TODO: Check the actual units that are contained in the main results
    if convolution_settings[
        "convert_probability_with_binary_fraction_and_mass_in_stars"
    ]:
        cb.ax.set_ylabel("Rate density [{}]".format(all_main_results.unit))
    else:
        cb.ax.set_ylabel("Probability [{}]".format(all_main_results.unit))

    if convolution_settings["time_type"] == "lookback":
        axes_dict["invisible_axis"].set_xlabel(
            r"Lookback time [Gyr]", labelpad=60, fontsize=40
        )
    else:
        axes_dict["invisible_axis"].set_xlabel(r"Redshift", labelpad=60, fontsize=30)

    #
    axes_dict["invisible_axis"].set_ylabel(
        r"%s [M$_{\odot}$]" % long_name_mass, labelpad=100, fontsize=30
    )

    #
    title_text = (
        "Intrinsic {} density of merging BHBH systems per {} over cosmic time".format(
            long_name_rate_type.lower(), long_name_mass.lower()
        )
    )
    if not time_bin_key is None:
        time_type = convolution_settings["time_type"]
        bin_edges = all_convolution_dataset["time_binned"]["bins_edges"][time_bin_key]
        title_text += "\n with the rate resulting from stars formed between {} and {} {}{}".format(
            bin_edges[0],
            bin_edges[1],
            time_type,
            "(Gyr)" if time_type == "lookback" else "",
        )

    #
    plt.suptitle(title_text, y=0.9, fontsize=30)

    plot_settings["hspace"] = 0.25

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


# Total intrinsic rate density
def plot_total_intrinsic_rate_density_at_all_lookback_with_subsets(
    convolution_dataset_dict, rate_type, time_bin_key=None, plot_settings=None
):
    """
    Function to plot the total intrinsic merger rate density at all the lookback values

    TODO: add the fractions too
    """

    #
    if not plot_settings:
        plot_settings = {}

    # Get the data and split it off:
    all_convolution_dataset = convolution_dataset_dict["all"]

    subset_convolution_dataset_dict = {
        el: convolution_dataset_dict[el]
        for el in convolution_dataset_dict.keys()
        if not el == "all"
    }

    # unpack the convolution dataset
    convolution_settings = all_convolution_dataset["convolution_configuration"]

    # Select correct rate type:
    if rate_type == "merger_rate":
        rate_key = "merger_rates"
        long_name_rate_type = "Merger rate"
    elif rate_type == "formation_rate":
        rate_key = "formation_rates"
        long_name_rate_type = "Formation rate"

    # get time info
    timebins = convolution_settings["time_bins"]
    time_values = (timebins[1:] + timebins[:-1]) / 2

    #
    # Set up axes
    fig = plt.figure(figsize=(24, 16))

    gs = fig.add_gridspec(nrows=6, ncols=1)

    # Create axes
    axes_all = fig.add_subplot(gs[:4, 0])
    axes_fractions = fig.add_subplot(gs[4:, 0])

    # Plot ALL results
    #
    all_rate_result_dict = all_convolution_dataset["results"][rate_key]

    all_summed_merger_rate_density = np.sum(
        all_rate_result_dict["result_array_any_mass"],
        axis=1,
    )
    axes_all.plot(time_values, all_summed_merger_rate_density, color="k")

    # Plot the stacked form of the subset results
    fractions_of_all_dict = {}

    stacked_subset_array = np.zeros(all_summed_merger_rate_density.shape)
    stacked_fractions_subset_array = np.zeros(all_summed_merger_rate_density.shape)

    for i, subset_name in enumerate(subset_convolution_dataset_dict.keys()):
        #
        subset_convolution_dataset = convolution_dataset_dict[subset_name]
        subset_rate_result_dict = subset_convolution_dataset["results"][rate_key]

        #
        subset_summed_merger_rate_density = np.sum(
            subset_rate_result_dict["result_array_any_mass"], axis=1
        )
        axes_all.fill_between(
            time_values,
            stacked_subset_array,
            stacked_subset_array + subset_summed_merger_rate_density,
            label=subset_name,
            alpha=0.5,
        )

        #
        stacked_subset_array = stacked_subset_array + subset_summed_merger_rate_density

        #
        fraction_of_total = np.zeros(subset_summed_merger_rate_density.shape)
        np.divide(
            subset_summed_merger_rate_density.value,
            all_summed_merger_rate_density.value,
            out=fraction_of_total,
            where=all_summed_merger_rate_density != 0,
        )  # only divide nonzeros else 1

        fractions_of_all_dict[subset_name] = fraction_of_total

        # Plot
        width = np.diff(timebins)
        axes_fractions.bar(
            time_values,
            fraction_of_total,
            width=width,
            bottom=stacked_fractions_subset_array,
            label=subset_name,
            alpha=0.5,
        )

        stacked_fractions_subset_array = (
            stacked_fractions_subset_array + fraction_of_total
        )

    # Make up etc
    axes_all.legend()

    #
    title_text = "Intrinsic {} density as a function of redshift".format(
        long_name_rate_type.lower()
    )
    if not time_bin_key is None:
        time_type = convolution_settings["time_type"]
        bin_edges = convolution_dataset["time_binned"]["bins_edges"][time_bin_key]
        title_text += "\n with the rate resulting from stars formed between {} and {} {}{}".format(
            bin_edges[0],
            bin_edges[1],
            time_type,
            "(Gyr)" if time_type == "lookback" else "",
        )
    #
    axes_all.set_title(title_text, fontsize=20)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)

    # #
    # rate_result_dict = convolution_dataset['results'][rate_key]

    # # if slices:
    # if not time_bin_key is None:
    #     plot_results = select_time_sliced_results(convolution_dataset, time_bin_key, rate_key, mass_key)

    # # Get information about the total results
    # any_mass_summed_results = np.sum(rate_result_dict['result_array_any_mass'], axis=1)

    # # Set up axes
    # fig, axes = plt.subplots(ncols=1, nrows=1, figsize=(20, 20))

    # axes.plot(time_values, chirpmass_summed_results, label='chirpmass_summed_results', alpha=0.5)

    # # Add legend
    # axes.legend()

    # # TODO: Check the actual units that are contained in the main results
    # if convolution_settings['convert_probability_with_binary_fraction_and_mass_in_stars']:
    #     axes.set_ylabel("Rate density [{}]".format(rate_result_dict['result_array_any_mass'].unit))
    # else:
    #     axes.set_ylabel("Probability [{}]".format(rate_result_dict['result_array_any_mass'].unit))

    # #
    # if convolution_settings['time_type']=='lookback':
    #     axes.set_xlabel(r"Lookback time [Gyr]")
    # else:
    #     axes.set_xlabel(r"Redshift")
