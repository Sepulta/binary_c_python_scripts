"""
routine to generate figure 1 from neijssel19
"""

import os
import numpy as np
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

from grav_waves.gw_analysis.laptop_datasets import dataset_dict

from grav_waves.gw_analysis.functions.functions import (
    make_df,
    load_info_dict,
)

from grav_waves.settings import (
    resolution_settings,
    AGE_UNIVERSE_IN_YEAR,
    mass_in_stars,
    config_dict_cosmology,
)

from grav_waves.gw_analysis.functions.plot_functions import (
    plot_merger_rate_by_metallicity_by_total_mass,
)

from grav_waves.convolution.functions.convolution_functions import (
    get_dataset_metallicity_info,
)

MAIN_RESULT_DIR = "/home/david/projects/binary_c_root/results/GRAV_WAVES/"
MAIN_PLOT_DIR = "plots"

# List to loop over
DATASET_KEY_LIST = [
    "LOW_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_ON",
]
PLOT_TYPE_KEY_LIST = [
    "bhbh",
]

key = DATASET_KEY_LIST[0]
type_key = PLOT_TYPE_KEY_LIST[0]
sim_name = dataset_dict[key]["sim_name"]

result_dir = os.path.join(MAIN_RESULT_DIR, dataset_dict[key]["result_dir"])
plot_dir = os.path.join(MAIN_PLOT_DIR, dataset_dict[key]["plot_dir"])

# Load dataset
info_dict = load_info_dict(result_dir, rebuild=False)


#
def massratio_plot_combined(merger_dict, plot_settings=None):
    """
    Function to plot the yield of
    """

    type_key = "bhbh"
    binary_fraction = 0.7

    stepsize = 0.05
    massratio_bins = np.arange(0, 1 + stepsize, stepsize)
    massratio_centers = (massratio_bins[1:] + massratio_bins[:-1]) / 2

    if not plot_settings:
        plot_settings = {}

    #
    log10metallicities_in_solar = []

    # get metallicity info
    (
        sorted_metallicity_keys,
        sorted_metallicity_keys_type,
        _,
        sorted_metallicity_logvalues,
        logmetallicity_stepsize,
    ) = get_dataset_metallicity_info(merger_dict, type_key)

    # loop over all the metallicities
    for i, el in enumerate(sorted_metallicity_keys_type):
        info_dict = merger_dict[el]

        log10metallicity_in_solar = np.log10(
            info_dict["metallicity"] / config_dict_cosmology["solar_value"]
        )

        # print(info_dict)
        df = make_df(info_dict["filename"], limit_merging_time=False)

        # Limit by merging time
        df = df.query("%s < %f" % ("merger_time_values_in_years", AGE_UNIVERSE_IN_YEAR))

        #
        df["primary_mass"] = df[["mass_1", "mass_2"]].max(axis=1)
        df["secondary_mass"] = df[["mass_1", "mass_2"]].min(axis=1)

        df["massratio"] = df["secondary_mass"] / df["primary_mass"]

        #
        channel_1_df = df[
            (df.stable_rlof_counter == 1) & (df.comenv_counter == 1)
        ].copy(deep=True)
        channel_2_df = df[
            (df.stable_rlof_counter == 2) & (df.comenv_counter == 0)
        ].copy(deep=True)
        channel_3_df = df[
            (df.stable_rlof_counter == 0) & (df.comenv_counter == 1)
        ].copy(deep=True)

        fig = plt.figure(figsize=(16, 16))

        # Set up gridspec
        gs = fig.add_gridspec(nrows=8, ncols=1)

        # Create axis
        ax_distribution = fig.add_subplot(gs[:4, :])
        ax_fractions = fig.add_subplot(gs[6:, :])

        ###
        all_hist = ax_distribution.hist(
            df["massratio"],
            bins=massratio_bins,
            weights=df["probability"] * binary_fraction / mass_in_stars,
            label="Total",
            histtype="step",
            linewidth=3,
        )

        channel_1_hist = ax_distribution.hist(
            channel_1_df["massratio"],
            bins=massratio_bins,
            weights=channel_1_df["probability"] * binary_fraction / mass_in_stars,
            label="Channel 1",
            histtype="step",
            linewidth=3,
        )

        channel_2_hist = ax_distribution.hist(
            channel_2_df["massratio"],
            bins=massratio_bins,
            weights=channel_2_df["probability"] * binary_fraction / mass_in_stars,
            label="Channel 2",
            histtype="step",
            linewidth=3,
        )

        channel_3_hist = ax_distribution.hist(
            channel_3_df["massratio"],
            bins=massratio_bins,
            weights=channel_3_df["probability"] * binary_fraction / mass_in_stars,
            label="Channel 3",
            histtype="step",
            linewidth=3,
        )

        rest_array = all_hist[0] - (
            channel_1_hist[0] + channel_2_hist[0] + channel_3_hist[0]
        )

        ratio_channel_1_all = np.divide(
            channel_1_hist[0],
            all_hist[0],
            out=np.zeros_like(channel_1_hist[0]),
            where=all_hist[0] != 0,
        )
        ratio_channel_2_all = np.divide(
            channel_2_hist[0],
            all_hist[0],
            out=np.zeros_like(channel_2_hist[0]),
            where=all_hist[0] != 0,
        )
        ratio_channel_3_all = np.divide(
            channel_3_hist[0],
            all_hist[0],
            out=np.zeros_like(channel_3_hist[0]),
            where=all_hist[0] != 0,
        )
        ratio_channel_rest = np.divide(
            rest_array,
            all_hist[0],
            out=np.zeros_like(rest_array),
            where=all_hist[0] != 0,
        )

        ratio_channel_2_all_stacked = ratio_channel_1_all + ratio_channel_2_all
        ratio_channel_3_all_stacked = ratio_channel_2_all_stacked + ratio_channel_3_all
        ratio_channel_rest_stacked = ratio_channel_3_all_stacked + ratio_channel_rest

        #
        ax_fractions.bar(
            massratio_centers, ratio_channel_1_all, stepsize, label="channel 1"
        )
        ax_fractions.bar(
            massratio_centers,
            ratio_channel_2_all,
            stepsize,
            bottom=ratio_channel_1_all,
            label="channel 2",
        )
        ax_fractions.bar(
            massratio_centers,
            ratio_channel_3_all,
            stepsize,
            bottom=ratio_channel_2_all + ratio_channel_1_all,
            label="channel 3",
        )
        ax_fractions.bar(
            massratio_centers,
            ratio_channel_rest,
            stepsize,
            bottom=ratio_channel_3_all + ratio_channel_2_all + ratio_channel_1_all,
            label="channel rest",
        )

        ax_distribution.set_yscale("log")
        ax_distribution.legend(loc=1)
        plt.show()
        # quit()


massratio_plot_combined(info_dict)
