"""
Function to query and select some systems from the convolution datasets
"""

import os
import pandas as pd

from grav_waves.paper_scripts.primary_mass_distribution_at_redshift_with_variations.functions import (
    handle_columns,
    get_mask,
)


def readout_specific_system_settings(
    filename,
    quantity,
    dco_type,
    rate_type,
    general_query=None,
    querylist=None,
):
    """
    Function to readout specific system settings
    """

    ####
    # Read out dataframe
    combined_df = pd.read_hdf(filename, "/data/combined_dataframes")

    # Add columns
    handle_columns(
        combined_df, quantity, querylist, extra_columns=["primary_undergone_ppisn"]
    )

    # Create masks and apply, taking into account that the query and general query can be None
    mask = get_mask(combined_df, dco_type, general_query, query=None)

    # Get the quantity data from the dataframe
    quantity_data = combined_df[quantity]

    # Get queried dataframe
    queried = combined_df[mask]

    # Select only certain columns and then return them
    selected_columns = queried[
        ["metallicity", "zams_mass_1", "zams_period", "zams_mass_2", "random_seed"]
    ]

    return selected_columns, queried


if __name__ == "__main__":

    data_root = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"), "GRAV_WAVES/server_results/"
    )
    convolution_filename = os.path.join(
        data_root,
        "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    )
    general_query = "stable_rlof_counter == 0 & comenv_counter == 0 & primary_mass > 20 & primary_mass < 22"
    readout_specific_system_settings(
        convolution_filename,
        "primary_mass",
        "bhbh",
        "merger_rate",
        general_query=general_query,
    )
