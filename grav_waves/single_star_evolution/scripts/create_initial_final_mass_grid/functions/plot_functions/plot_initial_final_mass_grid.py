"""
Script containin the plot function

TODO: use the binsize calculator of the ensembles
"""

import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from ensemble_functions.ensemble_functions import generate_bins_from_keys

from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.utility_functions import (
    fill_missing_values,
    SN_type_dict,
)

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def plot_initial_final_mass_grid(datafile, custom_colorlimits=None, plot_settings=None):
    """
    Function to plot the grid in metallicity and mass
    """

    # contourlevels = [22]
    # linestyles = ['solid', 'dashed', '-.']

    # Open the data as a pandas dataframe
    df = pd.read_csv(datafile, header=0, sep="\s+")
    df["log10metallicity"] = np.log10(df["metallicity"])
    df = df.sort_values(by=["zams_mass", "log10metallicity"])

    # Filter out effective_metallicity == 0:
    df = df[df.effective_metallicity > 0]
    other_columns = [
        col for col in df.columns if not col in ["zams_mass", "metallicity"]
    ]

    # Fill missing values
    df = fill_missing_values(df, other_columns)

    unique_zams_mass_values = df["zams_mass"].sort_values().unique()
    unique_metallicity_values = df["metallicity"].sort_values().unique()
    unique_log10unique_metallicity_values = np.log10(unique_metallicity_values)

    z_vals = (
        df["mass"]
        .to_numpy()
        .reshape(
            len(unique_zams_mass_values), len(unique_log10unique_metallicity_values)
        )
    )

    #
    if not custom_colorlimits is None:
        norm = colors.Normalize(vmin=custom_colorlimits[0], vmax=custom_colorlimits[1])
    else:
        norm = colors.Normalize(
            vmin=z_vals[~np.isnan(z_vals)].min(), vmax=z_vals[~np.isnan(z_vals)].max()
        )

    ####
    #
    fig = plt.figure(figsize=(20, 20))

    #
    gs = fig.add_gridspec(nrows=1, ncols=12)

    # create axes
    ax = fig.add_subplot(gs[:, :10])
    ax_cb = fig.add_subplot(gs[:, 11])

    # Set up the colormesh plot
    colormesh = ax.pcolormesh(
        unique_metallicity_values,
        unique_zams_mass_values,
        z_vals,
        norm=norm,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        norm=norm,
    )
    cbar.ax.set_ylabel(r"Remnant mass [M$_{\odot}$]")

    #
    sn_type_vals = (
        df["sn_type"]
        .to_numpy()
        .reshape(
            len(unique_zams_mass_values), len(unique_log10unique_metallicity_values)
        )
    )

    #
    cs = ax.contour(
        unique_metallicity_values,
        unique_zams_mass_values,
        sn_type_vals,
        levels=[20, 21, 22],
        colors="red",
        linewidths=0.5,
    )

    # hatchlabels = []
    # plt.rcParams.update({'hatch.color': 'red', 'hatch.linewidth': 0.5})
    # # hatchlabels.append([plt.Rectangle((0, 0), 1, 1, fc=pc.get_facecolor()[0], hatch=pc.get_hatch()) for pc in cs.collections])

    # # Add contourlevels
    # _ = ax.contour(
    #     unique_metallicity_values,
    #     unique_zams_mass_values,
    #     z_vals,
    #     levels=contourlevels, colors='k', linestyles=linestyles,
    # )
    # for contour_i, _ in enumerate(contourlevels):
    #     cbar.ax.plot([cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]], [contourlevels[contour_i]]*2, 'black', linestyle=linestyles[contour_i])

    # plt.rcParams.update({'hatch.color': 'orange', 'hatch.linewidth': 0.5})
    # cs = ax.contourf(
    #     unique_metallicity_values,
    #     unique_zams_mass_values,
    #     sn_type_vals,
    #     levels = [21, 22],
    #     # colors='red',
    #     hatches=['\\'],
    #     alpha=0.01,
    # )
    # hatchlabels.append([plt.Rectangle((0, 0), 1, 1, fc=pc.get_facecolor()[0], hatch=pc.get_hatch()) for pc in cs.collections])

    # Set x-scale to log
    ax.set_xscale("log")
    ax.set_yscale(plot_settings["mass_scale"])

    ax.set_xlabel("log10 Metallicity (Z)")
    ax.set_ylabel(r"ZAMS mass [M$_{\odot}$]")

    # Handle saving
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #
    plot_initial_final_mass_grid(
        "../../output/output_vanilla/data/output.dat",
        custom_colorlimits=[0, 65],
        plot_settings={"show_plot": True},
    )
