"""
idea here is as follows:
- create combined dataframe for the SN types

TASKS:
TODO: filter out the SN_type == 0, we dont need that shit
TODO: convert and combine the types to names
"""

import os
import time
import shutil

import logging

import h5py
import pandas as pd

from grav_waves.settings import convolution_settings, config_dict_cosmology

from grav_waves.convolution.scripts.convolve_SN_rate.functions import (
    create_combined_sn_dataset,
)


if __name__ == "__main__":
    simname_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED"
    convolution_settings["logger"].setLevel(logging.DEBUG)

    from grav_waves.gw_analysis.functions.functions import make_dataset_dict

    dataset_dict_sn = make_dataset_dict(simname_dir)

    create_combined_sn_dataset(
        dataset_dict=dataset_dict_sn,
        convolution_configuration=convolution_settings,
        cosmology_configuration=config_dict_cosmology,
        output_filename=os.path.join(
            os.path.dirname(__file__), "results/combined_sn_dataset.hdf5"
        ),
    )
