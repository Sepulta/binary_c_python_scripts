"""
Main script to do the conversion of binary_c to compas data

See notes.org for the structure
"""

import os
import h5py

import pandas as pd

from grav_waves.settings import (
    convolution_settings,
)

from grav_waves.gw_analysis.functions.functions import (
    make_df,
    load_info_dict,
)
from grav_waves.settings import AGE_UNIVERSE_IN_YEAR

from grav_waves.settings import logger


def convert_binaryc_data_to_compas_data(input_filename, target_filename):
    """
    Function to convert the binary-c data to the COMPAS structure
    """

    #
    drop_columns = [
        "mass_1",
        "zams_mass_1",
        "mass_2",
        "zams_mass_2",
        "stellar_type_1",
        "prev_stellar_type_1",
        "stellar_type_2",
        "prev_stellar_type_2",
        "metallicity",
        "probability",
        "separation",
        "eccentricity",
        "period",
        "zams_period",
        "zams_separation",
        "zams_eccentricity",
        "prev_eccentricity",
        "prev_period",
        "prev_separation",
        "prev_mass_1",
        "prev_core_mass_1",
        "prev_CO_core_mass_1",
        "prev_He_core_mass_1",
        "prev_mass_2",
        "prev_core_mass_2",
        "prev_CO_core_mass_2",
        "prev_He_core_mass_2",
        "fallback_1",
        "fallback_2",
        "fallback_mass_1",
        "fallback_mass_2",
        "random_seed",
        "undergone_ppisn_1",
        "undergone_ppisn_2",
        "formation_time_in_years",
        "inspiral_time_in_years",
        "merger_time_in_years",
        "comenv_counter",
        "rlof_counter",
        "stable_rlof_counter",
        "undergone_CE_with_HG_donor",
        "undergone_CE_with_MS_donor",
        "evol_hist_count",
        "evol_hist",
        "formation_time_values_in_years",
        "inspiral_time_values_in_years",
        "merger_time_values_in_years",
        "chirpmass",
    ]

    logger.debug("started")

    #
    target_h5file = h5py.File(target_filename, "w")

    #####
    # Read out combined df
    combined_df = pd.read_hdf(input_filename, "/data/combined_dataframes")

    # Convert column names for 'BSE_Double_Compact_Objects' group
    combined_df[
        "Stellar_Type@ZAMS(1)"
    ] = 1  # Stellar_Type@ZAMS(1): NOTE: we just assume a MS
    combined_df[
        "Stellar_Type@ZAMS(2)"
    ] = 1  # Stellar_Type@ZAMS(2): NOTE: we just assume a MS

    combined_df[
        "CH_on_MS(1)"
    ] = 0  # CH_on_MS(1): NOTE: I assume this means whether the star evolves Chemically homogeneous on MS?
    combined_df[
        "CH_on_MS(2)"
    ] = 0  # CH_on_MS(2): NOTE: I assume this means whether the star evolves Chemically homogeneous on MS?

    combined_df["Mass(1)"] = combined_df["mass_1"]  # Mass(1)
    combined_df["Mass(2)"] = combined_df["mass_2"]  # Mass(2)

    combined_df["Mass@ZAMS(1)"] = combined_df["zams_mass_1"]  # Mass@ZAMS(1)
    combined_df["Mass@ZAMS(2)"] = combined_df["zams_mass_2"]  # Mass@ZAMS(2)
    print(combined_df.keys())
    combined_df["Time"] = (
        combined_df["formation_time_values_in_years"] / 1e6
    )  # Time: NOTE: this is the time at which both stars have become the CO right?  In COMPAS its measured in Myr
    combined_df["Coalescence_Time"] = (
        combined_df["merger_time_values_in_years"]
        - combined_df["formation_time_values_in_years"]
    ) / 1e6  # Coalescence_Time: NOTE: I assume this is the inspiral time? In COMPAS its measured in Myr
    combined_df["Merges_Hubble_Time"] = (
        combined_df["merger_time_values_in_years"] < AGE_UNIVERSE_IN_YEAR
    ).astype(
        int
    )  # Merges_Hubble_Time: NOTE: AGE_UNIVERSE_IN_YEAR is 13.7e9 and merger_time = time + coalescence_time

    combined_df["Stellar_Type(1)"] = combined_df[
        "stellar_type_1"
    ]  # Stellar_Type(1): NOTE: Assuming this is the exact same system as hurley up until stellar_type=  15
    combined_df["Stellar_Type(2)"] = combined_df[
        "stellar_type_2"
    ]  # Stellar_Type(1): NOTE: Assuming this is the exact same system as hurley up until stellar_type=  15

    # Convert column names for 'BSE_System_Parameters' group
    combined_df["Metallicity@ZAMS(1)"] = combined_df[
        "metallicity"
    ]  # Metallicity@ZAMS(1): metallicity of the dataset

    # Convert column names for 'BSE_Common_Envelopes'
    combined_df[
        "Immediate_RLOF>CE"
    ] = 0  # Immediate_RLOF>CE: NOTE: all are 0 cause i dont track that info
    combined_df["Optimistic_CE"] = combined_df[
        "undergone_CE_with_HG_donor"
    ]  # Optimistic_CE

    # Add probability info to the df
    # combined_df['Probability'] = combined_df['probability']
    combined_df["number_per_solar_mass"] = combined_df["number_per_solar_mass_values"]

    # Drop all the columns that arent necessary
    real_drop_columns = [
        column for column in drop_columns if column in combined_df.columns
    ]
    combined_df = combined_df.drop(columns=real_drop_columns)
    print(combined_df.columns)

    # Add SEED as the index column
    combined_df["SEED"] = combined_df.index

    ####
    # Write to groups
    logger.debug("writing to new structure")

    # DCO group
    target_h5file.create_group("BSE_Double_Compact_Objects")
    dco_group_cols = [
        "number_per_solar_mass",
        "Stellar_Type@ZAMS(1)",
        "Stellar_Type@ZAMS(2)",
        "CH_on_MS(1)",
        "CH_on_MS(2)",
        "Mass(1)",
        "Mass(2)",
        "Time",
        "Coalescence_Time",
        "Merges_Hubble_Time",
        "Stellar_Type(1)",
        "Stellar_Type(2)",
        "SEED",
    ]
    for el in dco_group_cols:
        target_h5file["BSE_Double_Compact_Objects"].create_dataset(
            el, data=combined_df[el].to_numpy()
        )

    # system param group
    target_h5file.create_group("BSE_System_Parameters")
    system_param_group_cols = [
        "SEED",
        "Metallicity@ZAMS(1)",
        "Stellar_Type@ZAMS(1)",
        "Stellar_Type@ZAMS(2)",
        "CH_on_MS(1)",
        "CH_on_MS(2)",
        # NOTE: these needed to be in here too
        "Mass@ZAMS(1)",
        "Mass@ZAMS(2)",
    ]
    for el in system_param_group_cols:
        target_h5file["BSE_System_Parameters"].create_dataset(
            el, data=combined_df[el].to_numpy()
        )

    # Comenv group
    target_h5file.create_group("BSE_Common_Envelopes")
    common_env_cols = ["SEED", "Immediate_RLOF>CE", "Optimistic_CE"]
    for el in common_env_cols:
        target_h5file["BSE_Common_Envelopes"].create_dataset(
            el, data=combined_df[el].to_numpy()
        )

    #
    logger.debug("finished")
