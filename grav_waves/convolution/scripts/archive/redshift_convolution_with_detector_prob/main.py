"""
Script that does the following steps sequentially:
- Run the convolution (redshift only) with the standard settings WITHOUT including the detector probability
- Run the convolution (redshift only) with the standard settings WITH the detector probability
- Take the results, combine that with redshift comoving shells and calculate the 'detectable' distribution
"""

import os
import copy
import pickle
import numpy as np

from grav_waves.gw_analysis.settings import config_dict_cosmology, convolution_settings
from grav_waves.convolution.functions.convolution_functions import new_convolution_main
from grav_waves.convolution.functions.general_plot_function import general_plot_routine

from grav_waves.convolution.functions.volume_convolution_functions import (
    create_redshift_volume_convolution_result_dict,
)

########
# Configure the settings for the datasets
# Load dataset
# from grav_waves.gw_analysis.server_datasets import dataset_dict
from grav_waves.gw_analysis.laptop_datasets import dataset_dict

# data info
data_info = {
    "dataset_dict": dataset_dict,
    "dataset_name": "HIGH_RES_SCHNEIDER_MASS_LOSS_PPISN_ON",
    "type_key": "bhbh",
    "main_result_dir": os.path.abspath("../../../gw_analysis/results"),
    "main_plot_dir": os.path.abspath("plots/"),
}

####################
# Configure settings for the cosmology settings we use
# Load the default cosmology configuration
cosmology_configuration = copy.deepcopy(config_dict_cosmology)
cosmology_configuration["metallicity_distribution_function"] = "coen19"

#########################
# Configure settings for the convolution settings we use
# Load the default convolution configuration
convolution_configuration = copy.deepcopy(convolution_settings)
convolution_configuration["amt_cores"] = 4

##################
# Redshift convolution

#
time_stepsize = 0.01
redshift_max_time = 20
convolution_configuration["time_type"] = "redshift"

# Set the time stuff in the convolution config
convolution_configuration["stepsize"] = time_stepsize
convolution_configuration["time_bins"] = np.arange(
    0.0000 - 0.5 * time_stepsize, redshift_max_time + 0.5 * time_stepsize, time_stepsize
)
convolution_configuration["time_centers"] = (
    convolution_configuration["time_bins"][1:]
    + convolution_configuration["time_bins"][:-1]
) / 2

convolution_configuration["min_loop_time"] = 0
convolution_configuration["max_loop_time"] = redshift_max_time

# #############################
# # Do the convolution WITHOUT detector probability
# redshift_convolution_without_detector_probability_filename = 'results/redshift_convolution_without_detector_probability.p'
# new_convolution_main(
#     data_info,
#     convolution_configuration,
#     cosmology_configuration,
#     redshift_convolution_without_detector_probability_filename,
#     # save_time_bins=save_time_bins,
#     store_each_timestep=False,
# )

# # # Plot the results of the
# results_redshift_convolution_without_detector_probability = pickle.load(open(redshift_convolution_without_detector_probability_filename, "rb" ))
# general_plot_routine(results_redshift_convolution_without_detector_probability, main_plot_dir='plots/without_detection')


# #############################
# # Do the convolution WITH detector probability
convolution_configuration["add_detection_probability"] = True
convolution_configuration["include_formation_rates"] = False

redshift_convolution_with_detector_probability_filename = (
    "results/redshift_convolution_with_detector_probability_lowres.p"
)
plot_dir = "plots/with_detection_lowres"
# new_convolution_main(
#     data_info,
#     convolution_configuration,
#     cosmology_configuration,
#     redshift_convolution_with_detector_probability_filename,
#     # save_time_bins=save_time_bins,
#     store_each_timestep=False,
# )

# # Plot the results of the
# results_redshift_convolution_with_detector_probability = pickle.load(open(redshift_convolution_with_detector_probability_filename, "rb" ))
# general_plot_routine(results_redshift_convolution_with_detector_probability, main_plot_dir=plot_dir)

from grav_waves.convolution.functions.general_plot_function import (
    general_plot_routine_volume_convolved,
)

# Load result dict, add info and plot
results_redshift_convolution_with_detector_probability = pickle.load(
    open(redshift_convolution_with_detector_probability_filename, "rb")
)
redshift_volume_convolution_results = create_redshift_volume_convolution_result_dict(
    results_redshift_convolution_with_detector_probability
)
results_redshift_convolution_with_detector_probability[
    "redshift_volume_convolution_results"
] = redshift_volume_convolution_results

#
#
general_plot_routine_volume_convolved(
    results_redshift_convolution_with_detector_probability,
    plot_dir,
)
# general_plot_routine_volume_convolved(
#     results_redshift_convolution_with_detector_probability, plot_dir,
#     max_plot_time=2.6
# )
