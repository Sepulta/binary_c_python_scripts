"""
Function to extract the main-sequence accretor information from the dataframe and create a new dataframe
"""

import os
import pandas as pd


def extract_MS_accretor_data_ensemble_df(input_ensemble_df):
    """
    Function to read out an ensemble df and extract the MS-only data
    """

    dirname = os.path.dirname(input_ensemble_df)
    basename = os.path.basename(input_ensemble_df)
    new_basename = "stable_MS_{}".format(basename)
    output_ensemble_df = os.path.join(dirname, new_basename)

    # Readout
    df = pd.read_csv(input_ensemble_df, header=0, sep="\s+")
    ms_df = df[df.st_acc == 1]
    del ms_df["st_acc"]
    stable_ms_df = ms_df[ms_df.stable == 0]
    del stable_ms_df["stable"]

    # write
    stable_ms_df.to_csv(output_ensemble_df, header=0, sep="\t")

    return output_ensemble_df


if __name__ == "__main__":

    input_ensemble_df = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_SANA/population_results/Z0.02/RLOF_ensemble_data.csv"

    extract_MS_accretor_data_ensemble_df(input_ensemble_df)
