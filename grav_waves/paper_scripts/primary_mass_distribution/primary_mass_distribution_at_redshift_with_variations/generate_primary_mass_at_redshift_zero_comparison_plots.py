import os
import numpy as np

from grav_waves.paper_scripts.primary_mass_distribution.main_plot_function import (
    generate_primary_mass_at_redshift_zero_plot,
)

if __name__ == "__main__":
    #########
    #
    data_root = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"), "GRAV_WAVES/server_results/"
    )
    # data_root = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/"

    #########
    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"),
        "paper_gw/paper_tex/figures/primary_mass_distribution_at_redshift_zero_with_variations",
    )
    plot_dir = "plots"

    # configuration:
    primary_mass_bins = np.arange(0, 100, 2)
    divide_by_binsize = True
    add_rates_plots = True
    add_ratio_to_fiducial_plot = False
    add_residual_to_fiducial_plot = False
    add_fraction_primary_underwent_ppisn_plot = True
    add_comparison_to_observations = True

    add_kde = False
    bootstraps = 5
    show_plot = False

    # #######
    # # Test

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     r"Event based (SEMI HIGH RES)": os.path.join(
    #         data_root,
    #         "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"new fiducial suggestion: $\xi{\mathrm{thermal\ accretion\ rate\ multiplier}} = 0$ (SEMI HIGH RES)": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_accretion_limit_thermal_multiplier_0.0/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "test.pdf"
    # generate_primary_mass_at_redshift_zero_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     bootstraps=bootstraps,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # quit()

    # #######
    # # Extra mass loss

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    #     r"$\Delta M_{\mathrm{PPI,\ Extra}} = 5$": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_5_PPISN_additional_massloss/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"$\Delta M_{\mathrm{PPI,\ Extra}} = 10$": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_10_PPISN_additional_massloss/convolution_results/convolution_results.h5",
    #     ),
    #     r"$\Delta M_{\mathrm{PPI,\ Extra}} = 15$": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_15_PPISN_additional_massloss/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"$\Delta M_{\mathrm{PPI,\ Extra}} = 20$": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_20_PPISN_additional_massloss/convolution_results/convolution_results.h5",
    #     ),
    #     # Old
    #     "Farmer et al. '19": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_FARMER19_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = (
    #     "primary_mass_distribution_at_redshift_zero_for_extra_massloss_variations.pdf"
    # )
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    #######
    # Core mass shift

    #
    convolved_datasets = {
        # Fiducial and variations
        "Fiducial": os.path.join(
            data_root,
            "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
        ),
        r"$\Delta M_{\mathrm{CO,\ PPI}} = -5$": os.path.join(
            data_root,
            "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-5_PPISN_core_mass_range_shift/convolution_results/convolution_results.h5",
        ),
        r"$\Delta M_{\mathrm{CO,\ PPI}} = -10$": os.path.join(
            data_root,
            "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-10_PPISN_core_mass_range_shift/convolution_results/convolution_results.h5",
        ),
        r"$\Delta M_{\mathrm{CO,\ PPI}} = -15$": os.path.join(
            data_root,
            "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
        ),
        r"$\Delta M_{\mathrm{CO,\ PPI}} = -20$": os.path.join(
            data_root,
            "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-20_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
        ),
        # Old prescriptions
        "Farmer et al. '19": os.path.join(
            data_root,
            "HIGH_RES_SCHNEIDER_WIND_PPISN_FARMER19_FRYER_DELAYED//convolution_results/convolution_results.h5",
        ),
    }

    #
    basename = "primary_mass_distribution_at_redshift_zero_for_core_mass_shift_variations_test.pdf"
    generate_primary_mass_at_redshift_zero_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
        add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
        add_comparison_to_observations=add_comparison_to_observations,
        add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
        bootstraps=bootstraps,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 24,
        },
    )
    quit()

    # #######
    # # Resolution study

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial HIGH RES": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    #     # Fiducial and variations
    # r"Event based (SEMI HIGH RES)": os.path.join(
    #     data_root,
    #     "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    # ),
    # }

    # #
    # basename = "resolution_comparison_semi_high_res.pdf"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # Alpha_CE study

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    # r"Event based (SEMI HIGH RES)": os.path.join(
    #     data_root,
    #     "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    # ),
    #     r"$\alpha_{\mathrm{CE}} = 0.2$": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_alpha_ce_multiplier_0.2/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"$\alpha_{\mathrm{CE}} = 5$": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_alpha_ce_multiplier_5/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "alpha_CE_variation_semi_high_res.pdf"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # WR wind study

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    # r"Event based (SEMI HIGH RES)": os.path.join(
    #     data_root,
    #     "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    # ),
    #     r"$f_{\mathrm{WR}} = 0.2$": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_wind_type_multiplier_WR_0.2/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"$f_{\mathrm{WR}} = 5$": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_wind_type_multiplier_WR_5/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "WR_WIND_variation_semi_high_res.pdf"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # SN Mechanism

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    # r"Event based (SEMI HIGH RES)": os.path.join(
    #     data_root,
    #     "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    # ),
    #     r"Rapid SN mechanism": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_RAPID/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "SN_engine_variation_semi_high_res.pdf"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # Maximum mass variation

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial SEMI RES (delayed SN mechanism)": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"$M_{\mathrm{primary,\ max}} = 150$": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "maximum_mass_variation.pdf"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # PPISN prescription variation

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial (SEMI RES)": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"No PPISN (SEMI RES)": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_OFF_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     ),
    #     "Farmer et al. '19 (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_FARMER19_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "PPISN_prescription_variation.pdf"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # Thermal limit multiplier

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"$\xi_{\mathrm{thermal\ accretion\ rate\ limit}} = 100$": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_accretion_limit_thermal_multiplier_100/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"$\xi_{\mathrm{thermal\ accretion\ rate\ limit}} = 1$": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_accretion_limit_thermal_multiplier_1/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "thermal_accretion_rate_limit_multiplier_variation.pdf"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # Without any CE systems

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial (SEMI RES)": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     ),
    #     # Fiducial and variations
    #     "Fiducial without CE (SEMI RES)": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_without_CE.pdf"
    # general_query = "comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # Without any stable MT systems systems

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial (SEMI RES)": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     ),
    #     # Fiducial and variations
    #     "Fiducial without stable MT (SEMI RES)": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_without_stable_MT.pdf"
    # general_query = "stable_rlof_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # Without any stable MT systems systems

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial (SEMI RES)": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     ),
    #     # Fiducial and variations
    #     "Fiducial without stable MT and CE s(SEMI RES)": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_without_stable_MT_and_CE.pdf"
    # general_query = "stable_rlof_counter == 0 & comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # Fiducial max mass = 150 resolution comparison

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial max mass=150(SEMI RES)": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
    #     ),
    #     "Fiducial max mass=150 (MID SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "max_mass_150_resolution_comparison.pdf"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     # general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # lower res comparison of accretion limit multiplier

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial max mass=150(SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\xi_{\mathrm{thermal\ accretion\ rate\ limit}} = 100$ (MID SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_accretion_limit_thermal_multiplier_100/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\xi_{\mathrm{thermal\ accretion\ rate\ limit}} = 1$ (MID SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_accretion_limit_thermal_multiplier_1/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\xi_{\mathrm{thermal\ accretion\ rate\ limit}} = 0.1$ (MID SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_accretion_limit_thermal_multiplier_0.1/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\xi_{\mathrm{thermal\ accretion\ rate\ limit}} = 0.01$ (MID SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_accretion_limit_thermal_multiplier_0.01/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\xi_{\mathrm{thermal\ accretion\ rate\ limit}} = 0.0$ (MID SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_accretion_limit_thermal_multiplier_0.01/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "lower_res_thermal_limit_multiplier.pdf"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     # general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # lower res comparison of accretion limit multiplier

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial max mass=150 (MID RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\xi_{\mathrm{thermal\ accretion\ rate\ limit}} = 100$ (MID RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_accretion_limit_thermal_multiplier_100/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\xi_{\mathrm{thermal\ accretion\ rate\ limit}} = 1$ (MID RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_accretion_limit_thermal_multiplier_1/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\xi_{\mathrm{thermal\ accretion\ rate\ limit}} = 0.1$ (MID RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_accretion_limit_thermal_multiplier_0.1/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\xi_{\mathrm{thermal\ accretion\ rate\ limit}} = 0.01$ (MID RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_accretion_limit_thermal_multiplier_0.01/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\xi_{\mathrm{thermal\ accretion\ rate\ limit}} = 0.0$ (MID RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_accretion_limit_thermal_multiplier_0.01/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # general_query = "comenv_counter == 0"
    # basename = "lower_res_thermal_limit_multiplier_without_CE.pdf"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # lower res comparison of fixed beta accretion efficiency

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial max mass=150(SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\beta_{MT} = 0.0$ (MID SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_fixed_beta_mt_0.0/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\beta_{MT} = 0.25$ (MID SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_fixed_beta_mt_0.25/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\beta_{MT} = 0.5$ (MID SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_fixed_beta_mt_0.5/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\beta_{MT} = 0.75$ (MID SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_fixed_beta_mt_0.75/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\beta_{MT} = 1.0$ (MID SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_fixed_beta_mt_1.0/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "lower_res_fixed_beta.pdf"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     # general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # lower res comparison of low accretion rate and L2 mass loss

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     "Fiducial max mass=150(SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
    #     ),
    #     r"Fiducial max mass=150 $\xi_{\mathrm{thermal\ accretion\ rate\ limit}} = 0.01$ & L2 mass loss (MID SEMI RES)": os.path.join(
    #         data_root,
    #         "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_accretion_limit_thermal_multiplier_0.01_nonconservative_angmom_gamma_-3/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "low_res_low_thermal_multiplier_L2_massloss.pdf"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     # general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # No kicks variation

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    # r"Event based (SEMI HIGH RES)": os.path.join(
    #     data_root,
    #     "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    # ),
    #     r"Fiducial $\sigma_{\mathrm{kick}} = 0$ (SNII & SNIBC) (SEMI HIGH RES)": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_sn_kick_dispersion_II_0sn_kick_dispersion_IBC_0/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "semi_high_res_no_kick_variation.pdf"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    # #######
    # # No kicks variation

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    # r"Event based (SEMI HIGH RES)": os.path.join(
    #     data_root,
    #     "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    # ),
    #     r"Fiducial $\sigma_{\mathrm{kick}} = 0$ (SNII & SNIBC) (SEMI HIGH RES)": os.path.join(
    #         data_root,
    #         "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_sn_kick_dispersion_II_0sn_kick_dispersion_IBC_0/dco_convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "semi_high_res_no_kick_variation_no_MT.pdf"
    # general_query = "stable_rlof_counter == 0 & comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
    #     add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
    #     add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 24,
    #     },
    # )

    #######
    # Supereddington accretion variation

    #
    convolved_datasets = {
        # Fiducial and variations
        r"Event based (SEMI HIGH RES)": os.path.join(
            data_root,
            "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
        ),
        r"$\xi{\mathrm{eddington\ accretion\ rate\ multiplier}} = 10$ (SEMI HIGH RES)": os.path.join(
            data_root,
            "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_accretion_limit_eddington_steady_multiplier_10/dco_convolution_results/convolution_results.h5",
        ),
    }

    #
    basename = "semi_high_res_supereddington.pdf"
    generate_primary_mass_at_redshift_zero_comparison_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
        add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
        add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
        add_kde=add_kde,
        bootstraps=bootstraps,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 24,
        },
    )

    #######
    # New fiducial suggestion

    #
    convolved_datasets = {
        # Fiducial and variations
        r"Event based (SEMI HIGH RES)": os.path.join(
            data_root,
            "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
        ),
        r"new fiducial suggestion: $\xi{\mathrm{thermal\ accretion\ rate\ multiplier}} = 0$ (SEMI HIGH RES)": os.path.join(
            data_root,
            "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_accretion_limit_thermal_multiplier_0.0/dco_convolution_results/convolution_results.h5",
        ),
    }

    #
    basename = "semi_high_res_fiducial_comparison.pdf"
    generate_primary_mass_at_redshift_zero_comparison_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_ratio_to_fiducial_plot=add_ratio_to_fiducial_plot,
        add_residual_to_fiducial_plot=add_residual_to_fiducial_plot,
        add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
        add_kde=add_kde,
        bootstraps=bootstraps,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 24,
        },
    )
