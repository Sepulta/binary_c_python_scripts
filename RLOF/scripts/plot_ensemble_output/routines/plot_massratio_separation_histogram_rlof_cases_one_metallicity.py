"""
Function to plot the RLOF massratio separation distribution for stable mass transfer onto MS via a disk for different RLOF cases.
    normalized by all mass transfer onto MS via disk
"""

import os
import json
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

from ensemble_functions.ensemble_functions import (
    get_recursive_sum,
    merge_all_subdicts,
    return_keys_bins_and_bincenters,
    flatten_data_ensemble2d,
    query_dict,
    recursively_merge_all_subdicts,
    merge_recursive_until_stop,
)
from RLOF.scripts.plot_ensemble_output.settings import (
    rlof_a_stellar_type_range,
    rlof_b_stellar_type_range,
    rlof_c_stellar_type_range,
)
from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)

from scipy.ndimage import gaussian_filter
from grav_waves.settings import convolution_settings


def plot_massratio_separation_histogram_rlof_cases_one_metallicity(
    dataset, testing, verbose=0, global_query_dict=None, plot_settings=None
):
    """
    Function to plot the RLOF massratio separation distribution for stable mass transfer onto MS via a disk for different RLOF cases
    """

    if not plot_settings:
        plot_settings = {}

    #
    if verbose:
        print(
            "Running plot_massratio_separation_histogram_rlof_cases_one_metallicity for Z = {}\n\toutput: {}".format(
                dataset["metallicity"], os.path.abspath(plot_settings["output_name"])
            )
        )

        if global_query_dict:
            print(
                "\twith global query: {}".format(global_query_dict["global_query_name"])
            )
            print(
                "\t\tqueries: {}".format(
                    ", ".join(
                        [
                            "{} between {}".format(
                                query["parameter_key"], query["value_range"]
                            )
                            for query in global_query_dict["query_list"]
                        ]
                    )
                )
            )

    # Get the correct ensemble data from the dict
    if verbose:
        print("\t\tLoading data")
    probability_to_yield_conversion_factor = (
        convolution_settings["binary_fraction"] * convolution_settings["mass_in_stars"]
    )
    ensemble_data = get_ensemble_data(
        dataset,
        testing,
        probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
    )["RLOF"]["fraction_separation_massratio"]["mass"]

    if global_query_dict:
        if verbose:
            print("\tPerforming queries")
        for query in global_query_dict["query_list"]:
            if verbose:
                print(
                    "\t\tPerforming query {}".format(
                        "{} between {}".format(
                            query["parameter_key"], query["value_range"]
                        )
                    )
                )
            ensemble_data = query_dict(
                ensemble_data, query["parameter_key"], query["value_range"]
            )

    # Get stable MT
    if verbose:
        print("\t\tSelecting stable MT")
    stable_ensemble_data = query_dict(ensemble_data, "stable", [-0.5, 0.5])

    # Get MT onto MS
    if verbose:
        print("\t\tSelecting MT onto MS")
    ms_stable_ensemble = query_dict(stable_ensemble_data, "st_acc", [0.5, 1.5])

    # Get MS via disk
    if verbose:
        print("\t\tSelecting disk MT")
    ms_disk_mass_stable_ensemble = query_dict(ms_stable_ensemble, "disk", [0.5, 1.5])
    merged_ms_disk_mass_stable_ensemble = merge_recursive_until_stop(
        ms_disk_mass_stable_ensemble, "r_acc_sep"
    )
    total_ms_disk_mass_stable_ensemble = get_recursive_sum(ms_disk_mass_stable_ensemble)

    # Get bins and stuff
    if verbose:
        print("\t\tGetting the bins")
    (
        log10_fraction_accretor_separation_keys,
        bins_log10_accretor_separation,
        bincenters_log10_accretor_separation,
    ) = return_keys_bins_and_bincenters(
        merged_ms_disk_mass_stable_ensemble, "r_acc_sep"
    )
    (
        log10_massratio_keys,
        bins_log10_massratio,
        bincenters_log10_massratio,
    ) = return_keys_bins_and_bincenters(
        merge_all_subdicts(merged_ms_disk_mass_stable_ensemble["r_acc_sep"]),
        "q_acc_don",
    )

    # Get RLOF cases
    if verbose:
        print("\t\tSelecting RLOF cases")
    rlof_b_ms_disk_mass_stable_ensemble = query_dict(
        ms_disk_mass_stable_ensemble, "st_don", rlof_b_stellar_type_range
    )
    rlof_c_ms_disk_mass_stable_ensemble = query_dict(
        ms_disk_mass_stable_ensemble, "st_don", rlof_c_stellar_type_range
    )

    # First merge the whole set until we only have the last 2 depths left
    if verbose:
        print("\t\tMerging until fractions key")
    merged_rlof_b_ms_disk_mass_stable_ensemble = merge_recursive_until_stop(
        rlof_b_ms_disk_mass_stable_ensemble, "r_acc_sep"
    )
    merged_rlof_c_ms_disk_mass_stable_ensemble = merge_recursive_until_stop(
        rlof_c_ms_disk_mass_stable_ensemble, "r_acc_sep"
    )

    # Set up dict to store the huistograms in
    if verbose:
        print("\t\tFlattening data")
    flattenend_rlof_b_ms_disk_mass_stable_ensemble = flatten_data_ensemble2d(
        merged_rlof_b_ms_disk_mass_stable_ensemble, "r_acc_sep", "q_acc_don"
    )
    flattenend_rlof_c_ms_disk_mass_stable_ensemble = flatten_data_ensemble2d(
        merged_rlof_c_ms_disk_mass_stable_ensemble, "r_acc_sep", "q_acc_don"
    )

    # Create histograms
    if verbose:
        print("\t\tCreating histograms")
    hist_data_rlof_b, _, _ = np.histogram2d(
        flattenend_rlof_b_ms_disk_mass_stable_ensemble[0],
        flattenend_rlof_b_ms_disk_mass_stable_ensemble[1],
        bins=[bins_log10_accretor_separation, bins_log10_massratio],
        weights=flattenend_rlof_b_ms_disk_mass_stable_ensemble[2],
    )
    hist_data_rlof_c, _, _ = np.histogram2d(
        flattenend_rlof_c_ms_disk_mass_stable_ensemble[0],
        flattenend_rlof_c_ms_disk_mass_stable_ensemble[1],
        bins=[bins_log10_accretor_separation, bins_log10_massratio],
        weights=flattenend_rlof_c_ms_disk_mass_stable_ensemble[2],
    )

    # Normalize
    if verbose:
        print("\t\tNormalising histograms")
    normed_hist_data_rlof_b = (
        hist_data_rlof_b / total_ms_disk_mass_stable_ensemble
        if total_ms_disk_mass_stable_ensemble != 0
        else np.zeros(hist_data_rlof_b.shape)
    )
    normed_hist_data_rlof_c = (
        hist_data_rlof_c / total_ms_disk_mass_stable_ensemble
        if total_ms_disk_mass_stable_ensemble != 0
        else np.zeros(hist_data_rlof_c.shape)
    )
    combined_normed_hist_data = normed_hist_data_rlof_b + normed_hist_data_rlof_c
    global_max_val = combined_normed_hist_data.max()

    # apply gaussian blur
    from scipy.ndimage import gaussian_filter

    if verbose:
        print("\t\tApplying gaussian blur")
    normed_hist_data_rlof_b = gaussian_filter(normed_hist_data_rlof_b, sigma=2)
    normed_hist_data_rlof_c = gaussian_filter(normed_hist_data_rlof_c, sigma=2)

    ######################
    # Set up figure
    if verbose:
        print("\t\tPlotting figure")
    fig = plt.figure(figsize=(24, 10))

    #
    gs = fig.add_gridspec(nrows=1, ncols=8)

    # create axes
    ax_case_b = fig.add_subplot(gs[0, 0:3])
    ax_case_c = fig.add_subplot(gs[0, 3:6])

    ax_invisible = fig.add_subplot(gs[0, :6], frame_on=False)
    ax_invisible.set_xticks([])
    ax_invisible.set_yticks([])

    # Create colorbar
    ax_cb = fig.add_subplot(gs[0, 7])

    # Remove y-ticks
    ax_case_c.set_yticklabels([])

    # Set titles
    ax_case_b.set_title("RLOF B")
    ax_case_c.set_title("RLOF C")

    #
    ax_invisible.set_xlabel("")
    ax_case_b.set_ylabel(r"log$_{10}$($M_{\mathrm{acc}}$/$M_{\mathrm{don}}$)")
    ax_invisible.set_xlabel(r"log$_{10}$($R_{\mathrm{acc}}$ / $a$)", labelpad=50)

    # Make meshgrid of bins
    X, Y = np.meshgrid(bincenters_log10_accretor_separation, bincenters_log10_massratio)

    DIFF_MAX_THRESHOLD = 3

    ######################
    # Plot the data

    #
    hist_rlof_b = ax_case_b.pcolormesh(
        X,
        Y,
        normed_hist_data_rlof_b.T,
        norm=colors.LogNorm(
            vmin=10 ** (np.log10(global_max_val) - DIFF_MAX_THRESHOLD),
            vmax=global_max_val,
        ),
        shading="auto",
        antialiased=True,
        rasterized=True,
    )
    hist_rlof_c = ax_case_c.pcolormesh(
        X,
        Y,
        normed_hist_data_rlof_c.T,
        norm=colors.LogNorm(
            vmin=10 ** (np.log10(global_max_val) - DIFF_MAX_THRESHOLD),
            vmax=global_max_val,
        ),
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    #
    contour_levels = [1e-5, 1e-4, 0.5 * 1e-3]
    CS_case_b = ax_case_b.contour(
        X,
        Y,
        normed_hist_data_rlof_b.T,
        levels=contour_levels,
        colors="k",
        linestyles=["solid", "dashed", "-."],
    )
    CS_case_c = ax_case_c.contour(
        X,
        Y,
        normed_hist_data_rlof_c.T,
        levels=contour_levels,
        colors="k",
        linestyles=["solid", "dashed", "-."],
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        cmap=matplotlib.cm.viridis,
        norm=matplotlib.colors.LogNorm(
            vmin=10 ** (np.log10(global_max_val) - DIFF_MAX_THRESHOLD),
            vmax=global_max_val,
        ),
        extend="min",
    )
    cbar.ax.set_ylabel(
        "Fraction of all stable mass-\ntransfer onto main-sequence\nstars through disks"
    )

    fig.tight_layout()

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)
