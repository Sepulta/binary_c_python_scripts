"""
Function to generate the table containing the fractions of mass weighted stable MT onto MS for disks and direct in different RLOF cases
    normalized by all stable MT onto MS
"""

import os
import json
import pandas as pd

from ensemble_functions.ensemble_functions import (
    get_recursive_sum,
    merge_all_subdicts,
    query_dict,
)
from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)
from RLOF.scripts.plot_ensemble_output.settings import (
    rlof_a_stellar_type_range,
    rlof_b_stellar_type_range,
    rlof_c_stellar_type_range,
)
from grav_waves.settings import convolution_settings


def generate_table_fraction_cases_stable_ms_disk(
    dataset,
    output_filename,
    testing,
    caption="",
    label="",
    global_query_dict=None,
    verbose=0,
):
    """
    Function to create the table to shows the fraction of MS to all MT
    """

    if verbose:
        print(
            "Running generate_table_fraction_cases_stable_ms_disk for Z = {}\n\toutput: {}".format(
                dataset["metallicity"], os.path.abspath(output_filename)
            )
        )

        if global_query_dict:
            print(
                "\twith global query: {}".format(global_query_dict["global_query_name"])
            )
            print(
                "\t\tqueries: {}".format(
                    ", ".join(
                        [
                            "{} between {}".format(
                                query["parameter_key"], query["value_range"]
                            )
                            for query in global_query_dict["query_list"]
                        ]
                    )
                )
            )

    #
    result_dict = {}

    # Get the correct ensemble data from the dict
    if verbose:
        print("\t\tLoading data")
    probability_to_yield_conversion_factor = (
        convolution_settings["binary_fraction"] * convolution_settings["mass_in_stars"]
    )
    ensemble_data = get_ensemble_data(
        dataset,
        testing,
        probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
    )["RLOF"]["fraction_separation_massratio"]["mass"]

    if global_query_dict:
        if verbose:
            print("\tPerforming queries")
        for query in global_query_dict["query_list"]:
            if verbose:
                print(
                    "\t\tPerforming query {}".format(
                        "{} between {}".format(
                            query["parameter_key"], query["value_range"]
                        )
                    )
                )
            ensemble_data = query_dict(
                ensemble_data, query["parameter_key"], query["value_range"]
            )

    # Get all stable mass ensemble
    if verbose:
        print("\t\tSelecting stable mass transfer")
    mass_stable_ensemble = query_dict(ensemble_data, "stable", [-0.5, 0.5])

    # Select onto MS
    if verbose:
        print("\t\tSelecting mass transfer on MS data")
    ms_mass_stable_ensemble = query_dict(mass_stable_ensemble, "st_acc", [0.5, 1.5])
    total_ms_mass_stable_ensemble = get_recursive_sum(ms_mass_stable_ensemble)

    # select onto disk
    if verbose:
        print("\t\tSelecting MT on disk")
    ms_disk_mass_stable_ensemble = query_dict(
        ms_mass_stable_ensemble, "disk", [0.5, 1.5]
    )
    ms_direct_mass_stable_ensemble = query_dict(
        ms_mass_stable_ensemble, "disk", [-0.5, 0.5]
    )

    # select cases
    if verbose:
        print("\t\tSelecting RLOF cases")
    rlof_a_ms_disk_mass_stable_ensemble = query_dict(
        ms_disk_mass_stable_ensemble, "st_don", rlof_a_stellar_type_range
    )
    rlof_b_ms_disk_mass_stable_ensemble = query_dict(
        ms_disk_mass_stable_ensemble, "st_don", rlof_b_stellar_type_range
    )
    rlof_c_ms_disk_mass_stable_ensemble = query_dict(
        ms_disk_mass_stable_ensemble, "st_don", rlof_c_stellar_type_range
    )

    rlof_a_ms_direct_mass_stable_ensemble = query_dict(
        ms_direct_mass_stable_ensemble, "st_don", rlof_a_stellar_type_range
    )
    rlof_b_ms_direct_mass_stable_ensemble = query_dict(
        ms_direct_mass_stable_ensemble, "st_don", rlof_b_stellar_type_range
    )
    rlof_c_ms_direct_mass_stable_ensemble = query_dict(
        ms_direct_mass_stable_ensemble, "st_don", rlof_c_stellar_type_range
    )

    # Get totals
    if verbose:
        print("\t\tCalculating totals")
    total_rlof_a_ms_disk_mass_stable_ensemble = get_recursive_sum(
        rlof_a_ms_disk_mass_stable_ensemble
    )
    total_rlof_b_ms_disk_mass_stable_ensemble = get_recursive_sum(
        rlof_b_ms_disk_mass_stable_ensemble
    )
    total_rlof_c_ms_disk_mass_stable_ensemble = get_recursive_sum(
        rlof_c_ms_disk_mass_stable_ensemble
    )

    total_rlof_a_ms_direct_mass_stable_ensemble = get_recursive_sum(
        rlof_a_ms_direct_mass_stable_ensemble
    )
    total_rlof_b_ms_direct_mass_stable_ensemble = get_recursive_sum(
        rlof_b_ms_direct_mass_stable_ensemble
    )
    total_rlof_c_ms_direct_mass_stable_ensemble = get_recursive_sum(
        rlof_c_ms_direct_mass_stable_ensemble
    )

    if verbose:
        print("\t\tCalculating fractions")
    fraction_rlof_a_ms_disk_mass_stable_ensemble = (
        total_rlof_a_ms_disk_mass_stable_ensemble / total_ms_mass_stable_ensemble
        if total_ms_mass_stable_ensemble != 0
        else 0
    )
    fraction_rlof_b_ms_disk_mass_stable_ensemble = (
        total_rlof_b_ms_disk_mass_stable_ensemble / total_ms_mass_stable_ensemble
        if total_ms_mass_stable_ensemble != 0
        else 0
    )
    fraction_rlof_c_ms_disk_mass_stable_ensemble = (
        total_rlof_c_ms_disk_mass_stable_ensemble / total_ms_mass_stable_ensemble
        if total_ms_mass_stable_ensemble != 0
        else 0
    )

    fraction_rlof_a_ms_direct_mass_stable_ensemble = (
        total_rlof_a_ms_direct_mass_stable_ensemble / total_ms_mass_stable_ensemble
        if total_ms_mass_stable_ensemble != 0
        else 0
    )
    fraction_rlof_b_ms_direct_mass_stable_ensemble = (
        total_rlof_b_ms_direct_mass_stable_ensemble / total_ms_mass_stable_ensemble
        if total_ms_mass_stable_ensemble != 0
        else 0
    )
    fraction_rlof_c_ms_direct_mass_stable_ensemble = (
        total_rlof_c_ms_direct_mass_stable_ensemble / total_ms_mass_stable_ensemble
        if total_ms_mass_stable_ensemble != 0
        else 0
    )

    # Store in dict
    if verbose:
        print("\t\tStoring in dict")
    result_dict["Disk"] = [
        fraction_rlof_a_ms_disk_mass_stable_ensemble,
        fraction_rlof_b_ms_disk_mass_stable_ensemble,
        fraction_rlof_c_ms_disk_mass_stable_ensemble,
    ]
    result_dict["Direct"] = [
        fraction_rlof_a_ms_direct_mass_stable_ensemble,
        fraction_rlof_b_ms_direct_mass_stable_ensemble,
        fraction_rlof_c_ms_direct_mass_stable_ensemble,
    ]

    #
    column_names = ["case A", "case B", "case C"]

    # put in dataframe:
    result_df = pd.DataFrame.from_dict(
        result_dict, columns=column_names, orient="index"
    )
    result_df = result_df.transpose()

    #
    caption = caption
    label = label

    # Create latex string
    latex_string = result_df.to_latex(
        index=True, label=label, caption=caption, float_format="%.2f"
    )
    # print(latex_string)

    # Export
    with open(output_filename, "w") as output_filehandle:
        output_filehandle.write(latex_string)
