def parse_function(output):
    """
    Function to process the output
    """

    #
    headers = [
        "time",
        "mass_1",
        "zams_mass_1",
        "mass_2",
        "zams_mass_2",
        "stellar_type_1",
        "previous_stellar_type_1",
        "stellar_type_2",
        "previous_stellar_type_2",
        "metallicity",
        "probability",
        "separation",
        "eccentricity",
        "period",
        "zams_period",
        "radius_1",
        "radius_2",
        "angmom_1",
        "angmom_2",
        "v_eq_1",
        "v_eq_ratio_1",
        "v_eq_2",
        "v_eq_ratio_2",
        "luminosty_1",
        "luminosity_2",
        "teff_1",
        "teff_2",
        "disk_accretion",
        "rlof_type",
        "rlof_stability",
        "rlofing",
        "roche_radius_1",
        "roche_radius_2",
        "omega_1",
        "omega_crit_1",
        "omega_2",
        "omega_crit_2",
    ]

    all_values = []

    # Going over the lines
    for line in output.splitlines():
        if line.startswith("DAVID_RLOFING_SYSTEM"):
            values = [float(el) for el in line.split()[1:]]

            #
            all_values.append(values)

    df = pd.DataFrame.from_records(all_values, columns=headers)

    # Convert types
    df["stellar_type_1"] = df.stellar_type_1.astype(int)
    df["stellar_type_2"] = df.stellar_type_2.astype(int)
    df["previous_stellar_type_1"] = df.previous_stellar_type_1.astype(int)
    df["previous_stellar_type_2"] = df.previous_stellar_type_2.astype(int)

    df["disk_accretion"] = df.disk_accretion.astype(int)
    df["rlof_type"] = df.rlof_type.astype(int)
    df["rlof_stability"] = df.rlof_stability.astype(int)
    df["rlofing"] = df.rlofing.astype(int)

    return df


def get_rlof_start_end(df):
    """
    Function to get information about start and end of rlof episodes
    """

    # find RLOFS
    df["rlofing_grp"] = (df.rlofing.diff(1) != 0).astype("int").cumsum()
    summary_df = pd.DataFrame(
        {
            "BeginTime": df.groupby("rlofing_grp").time.first(),
            "EndTime": df.groupby("rlofing_grp").time.last(),
            "Consecutive_timesteps": df.groupby("rlofing_grp").size(),
            "rlofing": df.groupby("rlofing_grp").rlofing.first(),
            "rlof_type_start": df.groupby("rlofing_grp").rlof_type.first(),
            "rlof_type_end": df.groupby("rlofing_grp").rlof_type.last(),
            "rlof_stability_start": df.groupby("rlofing_grp").rlof_stability.first(),
            "rlof_stability_end": df.groupby("rlofing_grp").rlof_stability.last(),
        }
    ).reset_index(drop=True)

    return summary_df
