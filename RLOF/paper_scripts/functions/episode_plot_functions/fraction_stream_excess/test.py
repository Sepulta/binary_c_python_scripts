"""
Routine to plot the fraction of initial orbital angular momentum transferred back from stream excess for stable mt onto ms
"""

import os

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

#
from grav_waves.settings import convolution_settings

#
from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict
from RLOF.paper_scripts.functions.episode_plot_functions.utility import filter_df_all

#
from david_phd_functions.plotting.custom_mpl_settings import LINESTYLE_TUPLE
from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

#
def plot_fraction_stream_excess(dataset_dict, add_cdf=False, plot_settings={}):
    """
    Function to plot the fraction of initial orbital angular momentum transferred back from stream excess for stable mt onto ms for all metallicities provided in the dataset_dict

    Args:
        dataset_dict: dictionary containing info for multiple metallicities
        plot_settings: dict controlling the plotting
    """

    min_ratio = 1
    max_ratio = 0
    fontsize = 30

    #
    ratio_dict = {}

    # Loop over metallicities
    for metallicity in dataset_dict:
        # Read out data and create dataframe
        df = pd.read_csv(dataset_dict[metallicity]["filename"], sep="\s+", header=0)

        # Filter the df
        stable_mt_ms, disk_df, total_prob = filter_df_all(df, convolution_settings)

        # Normalise to total probability of all stable MT onto MS
        disk_df.loc[:, "number_per_solar_mass"] = (
            disk_df["number_per_solar_mass"] / total_prob
        )

        disk_df.loc[:, "ratio"] = (
            disk_df["total_mass_transferred_through_disk_MT_category_PARTIALLY_THICK"]
            / disk_df["total_mass_transferred_through_disk"]
        )

        print(disk_df["ratio"].max())

        disk_df.loc[:, "ratio"] = (
            disk_df["total_mass_transferred_through_disk_MT_category_THICK"]
            / disk_df["total_mass_transferred_through_disk"]
        )

        print(disk_df["ratio"].max())


if __name__ == "__main__":
    ########
    # Set up data
    simulation_dir = (
        "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_SANA/"
    )
    simulation_dir = "/home/david/projects/binary_c_root/results/RLOF/CUSTOM_RES_SANA/"

    rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(simulation_dir)
    dataset_dict = rlof_episode_dataset_dict["datasets"]

    ########
    # Set filename
    target_dir = "/home/david/Dropbox/Academic/PHD/papers/paper_disks/paper_tex/figures/episode_plots"
    # target_dir = "plots"

    #
    basename = "fraction_stream_excess.pdf"

    #
    plot_fraction_stream_excess(
        dataset_dict,
        add_cdf=True,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(target_dir, basename),
        },
    )
