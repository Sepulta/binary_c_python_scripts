import os
import json
import time
import pickle
import sys
import math

import astropy.units as u
import astropy.constants as const

import matplotlib as mpl
import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import get_help_all, get_help, create_hdf5

import pandas as pd
import numpy as np

from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc
from david_phd_functions.grav_waves.dataframe_functions import cut_df_real

from david_phd_functions.plotting.plot_functions import (
    plot_rotation_periods,
    plot_orbit,
    plot_masses,
    plot_angular_momenta,
    add_stellar_types_bar,
)

load_mpl_rc()

## Quick script to get some output about which stars go supernova when.
def output_lines(output):
    """
    Function that outputs the lines that were recieved from the binary_c run.
    """
    return output.splitlines()


def parse_function(self, output):
    # extract info from the single evolution

    values_list = []
    parameters = [
        "time",
        "mass_1",
        "mass_2",
        "pms_mass_1",
        "pms_mass_2",
        "orbital_period",
        "probability",
        "angular_momentum_1",
        "angular_momentum_2",
        "orbital_angular_momentum",
        "stellar_type_1",
        "stellar_type_2",
        "separation",
        "eccentricity",
        "omega_1",
        "omega_crit_1",
        "omega_2",
        "omega_crit_2",
        "luminosity_1",
        "luminosity_2",
        "radius_1",
        "radius_2",
    ]

    # Go over the output.
    for el in output_lines(output):
        headerline = el.split()[0]

        # Check the header and act accordingly
        if headerline == "DAVID_PLOTTING_EXAMPLE":
            values = el.split()[1:]
            values_list.append(values)

    df = pd.DataFrame(values_list)
    df.columns = parameters
    return df


################################################################
# Set values for the grid

## Set values
test_pop = Population()

metallicity = 0.02
test_pop.set(
    # Physics options
    metallicity=metallicity,
    M_1=3,
    M_2=1,
    orbital_period=1000000000,
    # Grid options
    data_dir=os.path.join(
        os.environ["BINARYC_DATA_ROOT"],
        "testing_python",
        "massloss_test",
        "massloss_{}".format(metallicity),
    ),
    base_filename="massloss_{}.dat".format(metallicity),
    verbose=0,
    amt_cores=2,
    binary=1,
    parse_function=parse_function,
    # parse_function=parse_function,
)

# Optionally: Add custom logging functionality
test_pop.set(
    C_logging_code="""
    //                                                                                 18 19 20 21 22
Printf("DAVID_PLOTTING_EXAMPLE %30.12e %g %g %g %g %g %g %g %g %g %d %d %g %g %g %g %g %g %g %g %g %g \\n",
    //
    stardata->model.time, // 1
    stardata->star[0].mass, //2
    stardata->star[1].mass, //3

    stardata->star[0].pms_mass, //4
    stardata->star[1].pms_mass, //5
    stardata->common.orbit.period, //6
    stardata->model.probability, //7

    stardata->star[0].angular_momentum,     /* 8:  Angular momentum */
    stardata->star[1].angular_momentum,     /* 9:  Angular momentum */
    stardata->common.orbit.angular_momentum, //10

    stardata->star[0].stellar_type, //11
    stardata->star[1].stellar_type,  //12
    stardata->common.orbit.separation, //13
    stardata->common.orbit.eccentricity, //14

    stardata->star[0].omega, //15
    stardata->star[0].omega_crit, //16
    stardata->star[1].omega, //17
    stardata->star[1].omega_crit, //18

    // for HR
    stardata->star[0].luminosity, // 19
    stardata->star[1].luminosity, // 20
    stardata->star[0].radius,     // 21
    stardata->star[1].radius      // 22
    );
"""
)

# Evolve grid
start_evolvetime = time.time()
df = test_pop.evolve_single()
print("finished running (in {}s)".format(time.time() - start_evolvetime))
# Put them in correct datatypes
df = df.astype(np.float64)
df["stellar_type_1"] = df["stellar_type_1"].astype(np.int64)
df["stellar_type_2"] = df["stellar_type_2"].astype(np.int64)


# lower_time_bound = 60
# upper_time_bound = 80

lower_time_bound = 0
upper_time_bound = 15000


df_real = cut_df_real(df)
df_real_cut = df_real[
    (df_real.time >= lower_time_bound) & (df_real.time <= upper_time_bound)
]


plot_HR_diagram(df_real, use_astropy_values=False, show_stellar_types=True)

quit()
##
# fig = plot_rotation_periods(df_real_cut, show_plot=False, show_stellar_types=True)
# plt.show(block=False)

# fig = plot_orbit(df_real_cut, show_plot=False)
# plt.show(block=False)

# fig = plot_masses(df_real_cut, show_plot=False)
# plt.show(block=False)

fig = plot_angular_momenta(df_real_cut, show_plot=False)
plt.show(block=False)

# fig = plot_HR_diagram(df_real_cut, show_plot=False)
# plt.show(block=False)

# Really plot
plt.show()
