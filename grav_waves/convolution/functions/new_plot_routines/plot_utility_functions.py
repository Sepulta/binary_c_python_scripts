"""
Utility functions for the plotting
"""

import numpy as np
import astropy.units as u

import matplotlib
from matplotlib import colors

from david_phd_functions.grav_waves.dataframe_functions import add_chirpmass_column

from david_phd_functions.plotting.linestyle_hatch_colorlist import (
    color_list,
    hatch_list,
    linestyle_list,
)

from grav_waves.convolution.functions.new_plot_routines.plot_kde_and_bootstrap_functions import (
    calculate_kde_values,
)


rate_density_units = u.Gpc**-3 * u.yr**-1
dimensionless_unit = u.m / u.m

# Dictionary containing names for the rate types
rate_type_name_dict = {
    "merger_rate": "Merger rate",
    "formation_rate": "Formation rate",
    "detection_rate": "Detection rate",
    "birth_rate": "Birth rate",
}

# Dictionary containing names for the quantities
quantity_name_dict = {
    "primary_mass": "Primary mass",
    "secondary_mass": "Secondary mass",
    "chirp_mass": "Chirp mass",
    "total_mass": "Total mass",
    "any_mass": "Mass",
    "mass_ratio": "Mass ratio",
    "metallicity": "Metallicity",
    "birth_redshift": "Birth redshift",
    "eccentricity": "Eccentricity",
    # ZAMS mass quantity
    "zams_chirpmass": "ZAMS chirp mass",
    "zams_total_mass": "ZAMS total mass",
    "zams_primary_mass": "ZAMS primary mass",
    "zams_secondary_mass": "ZAMS secondary mass",
    "zams_any_mass": "ZAMS mass",
}

# Dictionary containing units for the quantities
quantity_unit_dict = {
    "primary_mass": u.Msun,
    "secondary_mass": u.Msun,
    "total_mass": u.Msun,
    "mass_ratio": dimensionless_unit,
    "chirp_mass": u.Msun,
    "metallicity": dimensionless_unit,
    "birth_redshift": dimensionless_unit,
    "eccentricity": dimensionless_unit,
    # ZAMS mass quantity
    "zams_chirpmass": u.Msun,
    "zams_total_mass": u.Msun,
    "zams_primary_mass": u.Msun,
    "zams_secondary_mass": u.Msun,
    "zams_any_mass": u.Msun,
}


########
# Plot functions
def plot_2d_results(
    fig,
    axes_dict,
    X,
    Y,
    results_dict,
    querylist=None,
    add_ratio=False,
    plot_settings={},
    x_scale="linear",
    y_scale="linear",
    x_label="",
    y_label="",
    norm=None,
    norm_ratio=None,
):
    """
    Function to plot 2d results

    TODO: update this function to override the norm
    """

    if norm is None:
        norm = colors.LogNorm(
            vmin=10
            ** np.floor(
                np.log10(results_dict["all"].max())
                - plot_settings.get("probability_floor", 4)
            ),
            vmax=results_dict["all"].max(),
        )
    if norm_ratio is None:
        norm_ratio = colors.Normalize(vmin=0, vmax=1)

    # make colorbar
    cb = matplotlib.colorbar.ColorbarBase(
        axes_dict["colorbar_axis"], norm=norm, extend="min"
    )
    cb_ratio = None

    # Make sure the location is correct
    axes_dict["colorbar_axis"].yaxis.tick_right()
    axes_dict["colorbar_axis"].yaxis.set_ticks_position("both")
    axes_dict["colorbar_axis"].yaxis.set_label_position("right")

    # Loop over querylist
    if querylist is not None:
        if add_ratio:
            # Consider using a logscale for the ratio
            cb_ratio = matplotlib.colorbar.ColorbarBase(
                axes_dict["colorbar_axis_ratio"],
                norm=norm_ratio,
            )
            cb_ratio.ax.set_ylabel("Ratio to total")

    # Plot the results
    _ = axes_dict["all_axis"].pcolormesh(
        X,
        Y,
        results_dict["all"],
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )
    axes_dict["all_axis"].set_xscale(x_scale)
    axes_dict["all_axis"].set_yscale(y_scale)
    axes_dict["all_axis"].set_xlabel(x_label)
    axes_dict["all_axis"].set_ylabel(y_label)

    # Plot the subsets
    if not querylist is None:
        for query_i, query_dict in enumerate(querylist):
            _ = axes_dict["subset_axes"][query_i].pcolormesh(
                X,
                Y,
                results_dict[query_dict["name"]],
                norm=norm,
                shading="auto",
                antialiased=plot_settings.get("antialiased", True),
                rasterized=plot_settings.get("rasterized", True),
            )

            axes_dict["subset_axes"][query_i].set_title(
                "{}".format(query_dict["name"]), fontsize=18
            )
            axes_dict["subset_axes"][query_i].set_xscale(x_scale)
            axes_dict["subset_axes"][query_i].set_yscale(y_scale)

            if query_i > 0:
                axes_dict["subset_axes"][query_i].set_yticklabels([])
            else:
                axes_dict["subset_axes"][query_i].set_ylabel(y_label)

            # Add ratio plots
            if add_ratio:
                ratio_to_total = np.divide(
                    results_dict[query_dict["name"]],
                    results_dict["all"],
                    out=np.zeros_like(results_dict[query_dict["name"]]),
                    where=results_dict["all"] != 0,
                )  # only divide nonzeros else 1

                ratio_to_total[ratio_to_total == 0] = np.nan
                _ = axes_dict["ratio_axes"][query_i].pcolormesh(
                    X,
                    Y,
                    ratio_to_total,
                    norm=norm_ratio,
                    shading="auto",
                    antialiased=plot_settings.get("antialiased", True),
                    rasterized=plot_settings.get("rasterized", True),
                )

                axes_dict["ratio_axes"][query_i].set_title(
                    "{}: Ratio to total".format(query_dict["name"]), fontsize=18
                )
                axes_dict["ratio_axes"][query_i].set_xscale(x_scale)
                axes_dict["ratio_axes"][query_i].set_yscale(y_scale)

                # Remove y-ticklabels and set ylabel
                if query_i > 0:
                    axes_dict["ratio_axes"][query_i].set_yticklabels([])
                else:
                    axes_dict["ratio_axes"][query_i].set_ylabel(y_label)

                axes_dict["ratio_axes"][query_i].set_xlabel(x_label)
            else:
                axes_dict["subset_axes"][query_i].set_xlabel(x_label)

    return fig, axes_dict, cb, cb_ratio


def plot_contourlevels(
    results_dict,
    X,
    Y,
    fig,
    axes_dict,
    cb,
    cb_ratio,
    contourlevels,
    ratio_contourlevels,
    linestyle_list,
    querylist,
    add_ratio,
):
    """
    Function to add contourlevels to the plots
    """

    # Set contourlevels and lines on the colorbar
    if (not contourlevels is None) and (not linestyle_list is None):
        _ = axes_dict["all_axis"].contour(
            X,
            Y,
            results_dict["all"],
            levels=contourlevels,
            colors="k",
            linestyles=linestyle_list,
        )

        # Set lines on the colorbar
        for contour_i, _ in enumerate(contourlevels):
            cb.ax.plot(
                [cb.ax.get_xlim()[0], cb.ax.get_xlim()[1]],
                [contourlevels[contour_i]] * 2,
                "black",
                linestyle=linestyle_list[contour_i],
            )

        # Add contours on the subplots
        if not querylist is None:
            for query_i, query_dict in enumerate(querylist):
                _ = axes_dict["subset_axes"][query_i].contour(
                    X,
                    Y,
                    results_dict[query_dict["name"]],
                    levels=contourlevels,
                    colors="k",
                    linestyles=linestyle_list,
                )

                if add_ratio:
                    ratio_to_total = np.divide(
                        results_dict[query_dict["name"]],
                        results_dict["all"],
                        out=np.zeros_like(results_dict[query_dict["name"]]),
                        where=results_dict["all"] != 0,
                    )  # only divide nonzeros else 1

                    # Add contourlines
                    if (not ratio_contourlevels is None) and (
                        not linestyle_list is None
                    ):
                        _ = axes_dict["ratio_axes"][query_i].contour(
                            X,
                            Y,
                            ratio_to_total,
                            levels=ratio_contourlevels,
                            colors="k",
                            linestyles=linestyle_list,
                        )

                    # Set lines on the colorbar
                    for contour_i, _ in enumerate(ratio_contourlevels):
                        cb_ratio.ax.plot(
                            [cb_ratio.ax.get_xlim()[0], cb_ratio.ax.get_xlim()[1]],
                            [ratio_contourlevels[contour_i]] * 2,
                            "black",
                            linestyle=linestyle_list[contour_i],
                        )

    return fig, axes_dict


def return_1d_plot_canvas(fig, gs, xlabel_text, add_ratio, add_cdf):
    """
    Function to return the 1-d plot canvas
    """

    #
    axes_dict = {}

    # Add subplots
    if add_ratio and add_cdf:
        axes_dict["ax"] = fig.add_subplot(gs[:5, :])
        axes_dict["ax_ratio"] = fig.add_subplot(gs[5:8, :], sharex=axes_dict["ax"])
        axes_dict["ax_cdf"] = fig.add_subplot(gs[8:, :])
        axes_dict["ax_cdf"].set_xlabel(xlabel_text)
    elif add_ratio:
        axes_dict["ax"] = fig.add_subplot(gs[:8, :])
        axes_dict["ax_ratio"] = fig.add_subplot(gs[8:, :], sharex=axes_dict["ax"])
        axes_dict["ax_ratio"].set_xlabel(xlabel_text)
    elif add_cdf:
        axes_dict["ax"] = fig.add_subplot(gs[:8, :])
        axes_dict["ax_cdf"] = fig.add_subplot(gs[8:, :])
        axes_dict["ax_cdf"].set_xlabel(xlabel_text)
    else:
        axes_dict["ax"] = fig.add_subplot(gs[:, :])
        axes_dict["ax"].set_xlabel(xlabel_text)

    return fig, axes_dict


def plot_1d_results(
    results_dict,
    axes_dict,
    bins,
    colors,
    hatch_list,
    querylist=None,
    base_querydict=None,
    add_kde=False,
    add_cdf=False,
    add_ratio=False,
    bootstraps=0,
):
    """
    Function to plot the 1d results
    """

    #########
    # Plot the histograms
    axes_dict["ax"].plot(
        results_dict["all"]["centers"],
        results_dict["all"]["rates"],
        lw=5,
        c=colors[0],
        zorder=200,
        linestyle="--",
    )

    # plot the KDE values
    if add_kde:
        axes_dict["ax"].plot(
            results_dict["all"]["center_KDEbins"],
            results_dict["all"]["kde_yvals"],
            lw=5,
            c=colors[0],
            label="All ({})".format(base_querydict["name"])
            if base_querydict
            else "All",
            zorder=13,
        )

    if bootstraps:
        axes_dict["ax"].fill_between(
            results_dict["all"]["center_KDEbins"],
            results_dict["all"]["percentiles"][0],
            results_dict["all"]["percentiles"][1],
            alpha=0.4,
            zorder=11,
            color=colors[0],
        )  # 1-sigma
        axes_dict["ax"].fill_between(
            results_dict["all"]["center_KDEbins"],
            results_dict["all"]["percentiles"][2],
            results_dict["all"]["percentiles"][3],
            alpha=0.2,
            zorder=10,
            color=colors[0],
        )  # 2-sgima

    # Add cdf
    if add_cdf:
        axes_dict["ax_cdf"].plot(
            results_dict["all"]["centers"],
            np.cumsum(results_dict["all"]["rates"]),
            label="total",
        )

    # Plot queried results
    if not querylist is None:
        if add_ratio:
            # First make a empty shape that will contain the stacked results
            total_ratio_hist = np.zeros(results_dict["all"]["rates"].shape)

        for query_i, query_dict in enumerate(querylist):
            # Plot histogram
            axes_dict["ax"].plot(
                results_dict[query_dict["name"]]["centers"],
                results_dict[query_dict["name"]]["rates"],
                lw=5,
                c=colors[query_i + 1],
                zorder=query_i + 2,
                linestyle="--",
            )

            # Plot KDE and fill-between 0 and kde
            axes_dict["ax"].plot(
                results_dict[query_dict["name"]]["center_KDEbins"],
                results_dict[query_dict["name"]]["kde_yvals"],
                lw=5,
                c=colors[query_i + 1],
                label=query_dict["name"],
                zorder=query_i + 2,
            )
            axes_dict["ax"].fill_between(
                results_dict[query_dict["name"]]["center_KDEbins"],
                0,
                results_dict[query_dict["name"]]["kde_yvals"],
                color=colors[query_i + 1],
                zorder=query_i + 2,
                alpha=0.1,
            )
            axes_dict["ax"].fill_between(
                results_dict[query_dict["name"]]["center_KDEbins"],
                0,
                results_dict[query_dict["name"]]["kde_yvals"],
                hatch=hatch_list[query_i],
                color=colors[query_i + 1],
                facecolor="none",
                edgecolor=colors[query_i + 1],
                zorder=query_i + 3,
            )

            # Plot the bootstrapped stuff
            if bootstraps:
                axes_dict["ax"].fill_between(
                    results_dict[query_dict["name"]]["center_KDEbins"],
                    results_dict[query_dict["name"]]["percentiles"][0],
                    results_dict[query_dict["name"]]["percentiles"][1],
                    alpha=0.4,
                    zorder=11,
                    color=colors[query_i + 1],
                )  # 1-sigma
                axes_dict["ax"].fill_between(
                    results_dict[query_dict["name"]]["center_KDEbins"],
                    results_dict[query_dict["name"]]["percentiles"][2],
                    results_dict[query_dict["name"]]["percentiles"][3],
                    alpha=0.2,
                    zorder=10,
                    color=colors[query_i + 1],
                )  # 2-sgima

            # Add cdf
            if add_cdf:
                axes_dict["ax_cdf"].plot(
                    results_dict[query_dict["name"]]["centers"],
                    np.cumsum(results_dict[query_dict["name"]]["rates"]),
                    label=query_dict["name"],
                    color=colors[query_i + 1],
                )

            # Add ratio
            if add_ratio:
                ratio_to_total = np.divide(
                    results_dict[query_dict["name"]]["rates"],
                    results_dict["all"]["rates"],
                    where=results_dict["all"]["rates"] != 0,
                    out=np.zeros_like(results_dict[query_dict["name"]]["rates"]),
                )
                axes_dict["ax_ratio"].bar(
                    results_dict[query_dict["name"]]["centers"],
                    ratio_to_total,
                    width=bins,
                    bottom=total_ratio_hist,
                    color=colors[query_i + 1],
                    label=query_dict["name"],
                )
                total_ratio_hist += ratio_to_total


def get_histogram_data(bins, data_array, weight_array):
    """
    Function to get the histogram data.

    Also returns the truncated bins where the ends containin only zeros are chopped off
    """

    # Determine the mass bins
    bin_size = np.diff(bins)
    bincenter = (bins[1:] + bins[:-1]) / 2

    # bin and take into account the divison by mass
    hist = np.histogram(data_array, bins=bins, weights=weight_array)[0]

    # Select the non-zero bins and split off the empty ones
    # NOTE: without this, toms method does not work
    non_zero_bins_indices = np.nonzero(hist)[0]

    if non_zero_bins_indices.size != 0:
        truncated_bins = bins[
            non_zero_bins_indices.min() : non_zero_bins_indices.max() + 1
        ]
    else:
        truncated_bins = bins
    return hist, bincenter, truncated_bins


def get_KDE_data(bins, hist, data_array, weight_array):
    """
    Function to get the KDE data
    """

    bin_size = np.diff(bins)

    x_KDE_width = 0.1
    kde_width = 0.1 * bin_size[0]

    x_KDE = np.arange(bins.min(), bins.max() + x_KDE_width, x_KDE_width)
    center_KDEbins = (x_KDE[:-1] + x_KDE[1:]) / 2.0

    # calculate_kde_values
    y_vals = calculate_kde_values(
        data=data_array,
        weights=weight_array,
        lower_bound=data_array.min(),
        upper_bound=data_array.max(),
        bw_method=kde_width,
        center_KDEbins=center_KDEbins,
        hist=hist,
    )

    return y_vals, center_KDEbins, kde_width


########
# Functions
def get_num_subsets(querylist):
    """
    Function to return the number of subsets
    """

    if querylist is None:
        num_subsets = 0
    else:
        num_subsets = len(querylist)

    return num_subsets


########
# Functions for columns of dataframe
def isfloat(num):
    try:
        float(num)
        return True
    except:
        return False


def extract_columns_from_querylist(querylist):
    """
    Wrapper function for extract_columns_from_query to handle a list of queries
    """

    #
    columns = []

    #
    if querylist is None:
        return columns

    #
    for querydict in querylist:
        if not querydict is None:
            columns += extract_columns_from_query(querydict["query"])

    #
    return columns


def extract_columns_from_query(query):
    """
    Function to extract column names from the query
    """

    # dict with old and new values
    replace_dict = {
        "=": "",
        ")": "",
        "(": "",
        "&": "",
        "<": "",
        ">": "",
    }

    # Return empty list if there is no query
    if query is None:
        return []

    # replace values
    for old, new in replace_dict.items():
        query = query.replace(old, new)

    # remove the numbers
    columns = [el for el in query.split() if not isfloat(el)]

    #
    return columns


def dco_type_query(dco_type):
    """
    Function to filter the correct dco type in a dataframe

    dco_type can be 'bhbh', 'bhns', 'nsns', 'combined_dco'
    """

    if dco_type == "bhbh":
        return "stellar_type_1 == 14 & stellar_type_2 == 14"
    if dco_type == "nsns":
        return "stellar_type_1 == 13 & stellar_type_2 == 13"
    if dco_type == "bhns":
        return "(stellar_type_1 == 14 & stellar_type_2 == 13) | (stellar_type_1 == 13 & stellar_type_2 == 14)"
    if dco_type == "combined_dco":
        return "mass_1==mass_1"
    else:
        msg = "dco_type '{}' is unknown. Abort".format(dco_type)
        raise ValueError(msg)


#####
# Some useful numpy functions
def create_centers_from_bins(bins):
    """
    Function to create centers from bin edges
    """

    return (bins[1:] + bins[:-1]) / 2


def create_bins_from_centers(centers):
    """
    Function to create a set of bin edges from a set of bin centers. Assumes the two endpoints have the same binwidth as their neighbours
    """

    # Create bin edges minus the outer two
    bin_edges = (centers[1:] + centers[:-1]) / 2

    # Add to left
    bin_edges = np.append(np.array(bin_edges[0] - np.diff(centers)[0]), bin_edges)

    # Add to right
    bin_edges = np.append(bin_edges, np.array(bin_edges[-1] + np.diff(centers)[-1]))

    return bin_edges


#####
# Functions related to the structure of rate data and reading that out
def readout_rate_array(datafile, rate_key, mask, max_redshift=1e90):
    """
    Function to read out the rate array

    input:
        datafile has to be an open HDF5 filehandle
    """

    # Read out redshifts and systems to set up the array

    redshifts = np.array(
        sorted(list(datafile["data/{}".format(rate_key)].keys()), key=float)
    )
    redshifts_floats = np.array([float(redshift) for redshift in redshifts])

    # Mask redshifts
    redshift_mask = redshifts_floats <= max_redshift
    redshifts = redshifts[redshift_mask]
    redshifts_floats = redshifts_floats[redshift_mask]

    # Readout systems and mask to get the shape
    systems = datafile["data/{}/{}".format(rate_key, redshifts[0])][()]
    systems = systems[mask]

    # Set up merger rate array
    rate_array = np.zeros(shape=(len(redshifts), len(systems)))

    # Fill the array
    for redshift_i, redshift in enumerate(redshifts):
        # Readout rate data
        rate_data = datafile["data/{}/{}".format(rate_key, redshift)][()]

        # Mask data
        masked_rate_data = rate_data[mask]

        # Write to array
        rate_array[redshift_i, :] = masked_rate_data

    return rate_array, redshifts_floats


def readout_rate_array_specific_keys(datafile, rate_key, mask, redshift_keys):
    """
    Function to read out the rate array

    input:
        datafile has to be an open HDF5 filehandle
    """

    # Turn redshift_keys into float_values
    redshifts_floats = [float(redshift_key) for redshift_key in redshift_keys]

    # Read out redshifts and systems to set up the array
    all_redshifts = np.array(sorted(list(datafile["data/{}".format(rate_key)].keys())))

    # Readout systems and mask to get the shape
    systems = datafile["data/{}/{}".format(rate_key, all_redshifts[0])][()]
    systems = systems[mask]

    # Set up merger rate array
    rate_array = np.zeros(shape=(len(redshift_keys), len(systems)))

    # Fill the array
    for redshift_i, redshift in enumerate(redshift_keys):
        # Readout rate data
        rate_data = datafile["data/{}/{}".format(rate_key, redshift)][()]

        # Mask data
        masked_rate_data = rate_data[mask]

        # Write to array
        rate_array[redshift_i, :] = masked_rate_data

    return rate_array, redshifts_floats


def get_closest_redshift_key(redshift_value, all_redshift_keys):
    """
    Function to get the redshift key that lies closest to the desired redshift
    """

    #
    all_redshift_floats = np.array(
        [float(redshift_key) for redshift_key in all_redshift_keys]
    )

    # calculate diff
    diff_redshift_floats = all_redshift_floats - redshift_value

    # min index
    argmin_arr = np.argmin(np.abs(diff_redshift_floats))

    closest_key = all_redshift_keys[argmin_arr]

    #
    return all_redshift_keys[argmin_arr]


def histogram_with_2d_weights(quantity_array, quantity_bins, weight_array):
    """
    Function that makes a histogram with 1-d input quantities and bins, and a 2-d weights array.

    So basically histogram in 1 dimension

    TODO: this function could be improved by using the digitized indices and doing a cumsum in each bin, so that we dont have to loop anymore
    """

    # Set up histogram
    hist = np.zeros(shape=(weight_array.shape[0], len(quantity_bins) - 1))

    # create a list of indices that our binning uses:
    quantity_indices = np.digitize(quantity_array, quantity_bins)

    # Loop over the rows and do the histogram
    for redshift_i, weight_row in enumerate(weight_array):
        #
        local_hist = np.histogram(
            quantity_array, bins=quantity_bins, weights=weight_row
        )

        # Store in local hist
        hist[redshift_i] = local_hist[0]

    #
    return hist


####
# Mask
def generate_mask(df, general_query, dco_type, query=None):
    """
    Function to generate the mask
    """

    # Get individual masks
    dco_mask = df.eval(dco_type_query(dco_type)).to_numpy()
    general_query_mask = (
        df.eval(general_query)
        if general_query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    query_mask = (
        df.eval(query)
        if query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )

    # combine
    combined_mask = dco_mask * general_query_mask * query_mask

    return combined_mask


###############
# Functions to add columns to the dataframes
def add_columns_to_df(df, columns):
    """
    Function to add columns to the dataframe

    requires several columns to be present already
    """

    # loop over all columns
    for column in columns:
        if not column is None:
            if column in column_add_function_dict.keys():
                df = column_add_function_dict[column](df)
            else:
                raise ValueError(
                    "No function available to add the column {}".format(column)
                )

    return df


# specific functions
def add_primary_mass(df):
    """
    Function to add the primary mass column
    """

    if "primary_mass" in df.columns:
        return df

    df["primary_mass"] = df[["mass_1", "mass_2"]].max(axis=1)

    return df


def add_secondary_mass(df):
    """
    Function to add the secondary mass column
    """

    if "secondary_mass" in df.columns:
        return df

    df["secondary_mass"] = df[["mass_1", "mass_2"]].min(axis=1)

    return df


def add_total_mass(df):
    """
    Function to add total mass
    """

    if "total_mass" in df.columns:
        return df

    df["total_mass"] = df["mass_1"] + df["mass_2"]

    return df


def add_mass_ratio(df):
    """
    Function to add the mass ratio (m2/m1) to df

    requires primary_mass and secondary_mass
    """

    if "mass_ratio" in df.columns:
        return df

    # Add required columns
    df = add_primary_mass(df)
    df = add_secondary_mass(df)

    #
    df["mass_ratio"] = df["secondary_mass"] / df["primary_mass"]

    return df


def add_chirp_mass(df):
    """
    Function to add the chirp mass column
    """

    if "chirp_mass" in df.columns:
        return df

    df = add_chirpmass_column(
        df, m1_name="mass_1", m2_name="mass_2", chirpmass_name="chirp_mass"
    )

    return df


def add_zams_primary_mass(df):
    """
    Function to add the ZAMS mass of the current primary mass
    """

    # Get indices of primary and secondary
    indices_star_1_is_primary = np.where(df["mass_1"] >= df["mass_2"], True, False)
    indices_star_2_is_primary = np.where(df["mass_1"] < df["mass_2"], True, False)

    # Set masses
    df["zams_primary_mass"] = -1
    df["zams_primary_mass"][indices_star_1_is_primary] = df["zams_mass_1"][
        indices_star_1_is_primary
    ]
    df["zams_primary_mass"][indices_star_2_is_primary] = df["zams_mass_2"][
        indices_star_2_is_primary
    ]

    return df


def add_zams_secondary_mass(df):
    """
    Function to add the ZAMS mass of the current secondary mass
    """

    # Get indices of primary and secondary
    indices_star_1_is_primary = np.where(df["mass_1"] >= df["mass_2"], True, False)
    indices_star_2_is_primary = np.where(df["mass_1"] < df["mass_2"], True, False)

    #
    df["zams_secondary_mass"] = -1
    df["zams_secondary_mass"][indices_star_1_is_primary] = df["zams_mass_2"][
        indices_star_1_is_primary
    ]
    df["zams_secondary_mass"][indices_star_2_is_primary] = df["zams_mass_1"][
        indices_star_2_is_primary
    ]

    return df


def add_primary_undergone_ppisn(df):
    """
    Function to add primary_undergone_ppisn to df

    We use the undergone_ppisn_1 and undergone_ppisn_2 and set the values
    """

    # Get indices of primary and secondary
    indices_star_1_is_primary = np.where(df["mass_1"] >= df["mass_2"], True, False)
    indices_star_2_is_primary = np.where(df["mass_1"] < df["mass_2"], True, False)

    df.loc[:, "primary_undergone_ppisn"] = -1
    df.loc[indices_star_1_is_primary, "primary_undergone_ppisn"] = df[
        "undergone_ppisn_1"
    ][indices_star_1_is_primary]
    df.loc[indices_star_2_is_primary, "primary_undergone_ppisn"] = df[
        "undergone_ppisn_2"
    ][indices_star_2_is_primary]

    # # add to df
    # df['primary_undergone_ppisn'] = -1
    # df['primary_undergone_ppisn'][indices_star_1_is_primary] = df['undergone_ppisn_1'][indices_star_1_is_primary]
    # df['primary_undergone_ppisn'][indices_star_2_is_primary] = df['undergone_ppisn_2'][indices_star_2_is_primary]

    return df


def add_secondary_undergone_ppisn(df):
    """
    Function to add primary_undergone_ppisn to df

    We use the undergone_ppisn_1 and undergone_ppisn_2 and set the values
    """

    # Get indices of primary and secondary
    indices_star_1_is_primary = np.where(df["mass_1"] >= df["mass_2"], True, False)
    indices_star_2_is_primary = np.where(df["mass_1"] < df["mass_2"], True, False)

    # add to df
    df["secondary_undergone_ppisn"] = -1
    df["secondary_undergone_ppisn"][indices_star_1_is_primary] = df[
        "undergone_ppisn_2"
    ][indices_star_1_is_primary]
    df["secondary_undergone_ppisn"][indices_star_2_is_primary] = df[
        "undergone_ppisn_1"
    ][indices_star_2_is_primary]

    return df


def add_dummy(df):
    """
    DUmmy function, doesnt add
    """

    return df


# Dict containing the functions for the column adding
column_add_function_dict = {
    "primary_mass": add_primary_mass,
    "secondary_mass": add_secondary_mass,
    "total_mass": add_total_mass,
    "mass_ratio": add_mass_ratio,
    "chirp_mass": add_chirp_mass,
    "zams_primary_mass": add_zams_primary_mass,
    "zams_secondary_mass": add_zams_secondary_mass,
    "primary_undergone_ppisn": add_primary_undergone_ppisn,
    "secondary_undergone_ppisn": add_secondary_undergone_ppisn,
    "birth_redshift": add_dummy,
}
