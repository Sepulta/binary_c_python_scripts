import matplotlib.pyplot as plt

############################################################
# binary c interface functionality
############################################################

############################################################
# Function to run a star and return the results in a
# dictionary
def run_star(parameter_list):
    global logkappa, loggamma

    # set parameters
    if nparams == 6:
        logalpha, logtorquef, logkappa, loggamma, logfM, logfJ = parameter_list
    else:
        logalpha, logtorquef, logfM, logfJ = parameter_list
        # in this case, logkappa and loggamma are global variables
        logkappa, loggamma = globals()["logkappa"], globals()["loggamma"]

    if False:
        # Run a quick made-up test rather than a real system:
        # this is useful for debugging
        test_result = {
            "M_disc": 10**logfM * 10.0,
            "J_disc": 10**logfJ * 1e53,
            "R_out": 1e16 * 10**logkappa,
            "T_out": 100 * 10**logalpha,
            "Hout_Rout": 0.1 * 10**loggamma,
        }
        return test_result

    # binary_c executable location
    binary_c = os.environ["HOME"] + "/progs/stars/binary_c/binary_c"

    ############################################################
    # binary_c takes command-line arguments to set its many
    # parameters. Some are fixed, some vary because of the MCMC.
    #
    # read the fixed arguments from the default_args file
    # (you could specify them here)
    fixed_args = Path("../default_args").read_text()

    # make varying disc-specific args
    vary_args = "--cbdisc_alpha {cbdisc_alpha:g} --cbdisc_torquef {cbdisc_torquef:g} --cbdisc_kappa {cbdisc_kappa:g} --cbdisc_gamma {cbdisc_gamma:g} --comenv_disc_mass_fraction {comenv_disc_mass_fraction:g} --comenv_disc_angmom_fraction {comenv_disc_angmom_fraction:g}".format(
        cbdisc_alpha=10.0**logalpha,
        cbdisc_torquef=10.0**logtorquef,
        cbdisc_kappa=10.0**logkappa,
        cbdisc_gamma=10.0**loggamma,
        comenv_disc_mass_fraction=10.0**logfM,
        comenv_disc_angmom_fraction=10.0**logfJ,
    )

    # construct complete arg string
    args = binary_c + " " + fixed_args + " " + vary_args
    if vb:
        print("Run star with varying args ", vary_args)

    ############################################################
    # run the star, grab the results in the binary_c_output
    # string.
    binary_c_output = str(
        subprocess.run(args, shell=True, capture_output=True, encoding="utf-8").stdout
    )

    # remove "Tick" lines from the output
    p = re.compile("Tick.*")
    binary_c_output = p.sub("", binary_c_output)

    ############################################################
    # binary_c_output now contains some JSON, extract it to
    # a Python dictionary
    json_dict = {}
    try:
        # use json.loads to load from the binary_c_output string
        json_dict = json.loads(binary_c_output)
    except:
        # complain on failure, but this can happen e.g.
        # if there is no disc
        print("no json")

    # convert JSON data, stored in strings, to floats
    if json_dict and "IRAS" in json_dict:
        results = json_dict["IRAS"]

        # convert to floats
        for x in results:
            results[x] = float(results[x])

        # include lifetime
        results["lifetime"] = json_dict["IRAS final"]["lifetime"]

        if vb:
            print(iras)
    else:
        # no results: return empty dictionary
        results = {}

    # return results
    return results


############################################################
# MCMC functionality
############################################################


def log_prior(parameters):
    ############################################################
    # a function that returns the MCMC prior
    #
    # In our case, these are flat:
    # the function returns 0 in the regions where
    # the parameter is valid, and -np.inf (-infinity)
    # in regions where it is invalid.

    ############################################################
    # parameters holds 6 or 4 variables: name them
    if nparams == 6:
        logalpha, logtorquef, logkappa, loggamma, logfM, logfJ = parameters
    else:
        logalpha, logtorquef, logfM, logfJ = parameters
        # in this case logkappa and loggamma are their global values

    # return 0 # flat priors everywhere

    ############################################################
    # note: if you want to fix a parameter, set its prior to:
    #
    # 1.0-shift_scale < parameter/initial_parameter < 1.0+shift_scale
    #
    # or just don't include it in the list of parameters

    if (
        -7.0 < logalpha < 0.0
        and -5.0 < logtorquef < -1.0  # -7 to 0
        and -4.0 < logfM < -1.0  # -5 to -1
        and -4.0 < logfJ < 0.0  # -4 to -1
        and (  # -4 to 0
            (
                nparams == 6
                and -4.0 < logkappa < 4.0
                and 1.0 < 10**loggamma < 2.0  # -4 to 4  # 1 < gamma < 2
            )
            or nparams == 4
        )
    ):
        lprior = 0.0
    else:
        lprior = -np.inf

    return lprior


############################################################


def log_likelihood(parameters, args):
    ############################################################
    # calculate the log likelihood of the model results
    #
    # model_results is a dictionary of results for the disc

    # the result is in log_like
    log_like = 0.0

    # calculate the model results for the parameters passed in
    model_results = run_star(parameters)

    # model_results can be empty: in which case the likelihood should be
    # very negative because the model failed
    if model_results:
        for o in obs:
            # difference between model and observation
            delta = model_results[o] - obs_stats["mean"][o]

            # this is the log of the Gaussian
            dlog_like = -0.5 * (delta**2 / obs_stats["sigma2"][o])

            # these terms in the likelihood are constant, so
            # are usually ignored but perhaps you want to ignore them
            # + math.log(obs_stats['sigma2'][o]) + math.log(2*math.pi))
            if vb:
                print(
                    "obs ",
                    o,
                    " = ",
                    obs_stats["mean"][o],
                    " +- ",
                    obs_stats["sigma"][o],
                    " : model_results ",
                    model_results[o],
                    ", delta = ",
                    delta / obs_stats["sigma"][o],
                    "sigma : dlog_likelihood=",
                    dlog_like,
                )

            # hence add the log likelihood from this observation
            log_like = log_like + dlog_like
    else:
        # -infinity is very negative
        log_like = -np.inf

    if vb:
        print("log likelihood ", log_like)
    return log_like


############################################################

############################################################
def log_probability(parameters):
    ############################################################
    # log probability

    # first, calculate the prior probability
    lprior = log_prior(parameters)

    # if the prior is not infinitely negative, add the likelihood
    if not np.isfinite(lprior):
        lprob = -np.inf
    else:
        lprob = lprior + log_likelihood(parameters, obs_stats)

    return lprob


############################################################

############################################################
# Plotting functionality
############################################################

############################################################
# global matplotlib plot setup:
# use TeX for output
plt.rc("font", **{"family": "sans-serif", "sans-serif": ["Helvetica"]})
plt.rc("text", usetex=True)
plt.rcParams["text.latex.preamble"] = r"\usepackage{amsmath}"


def plot_samples(ndim, sampler, labels):
    fig, axes = plt.subplots(ndim, figsize=(10, 7), sharex=True)
    samples = sampler.get_chain()
    for i in range(ndim):
        ax = axes[i]
        ax.plot(samples[:, :, i], "k", alpha=0.3)
        ax.set_xlim(0, len(samples))
        ax.set_ylabel(labels[i])
        ax.yaxis.set_label_coords(-0.1, 0.5)
        axes[-1].set_xlabel("step number")
        plt.savefig("mcmc1.pdf")
        plt.close()


def corner_plot(flat_samples, labels):
    # do it with the corner module
    figure = corner.corner(flat_samples, labels=labels, color="b")

    # save it
    plt.savefig("mcmc2.pdf")
    plt.show()


def gtc_plot(flat_samples, labels, nbins):
    # Recommended: use the gtc module to make
    # Giant Triangle Confusograms.
    #
    # If your dataset is not large, you will need
    # to reduce nBins (default == 30).
    gtc = pygtc.plotGTC(
        flat_samples,
        paramNames=labels,
        figureSize="MNRAS_page",  # or MNRAS_column
        customTickFont={
            "family": "Helvetica Neue LT Std",
            "size": 5,
            "weight": "normal",
        },
        customLabelFont={
            "family": "Helvetica Neue LT Std",
            "size": 8,
            "weight": "normal",
        },
        tickShifts=(0, 0),
        mathTextFontSet="stixsans",
        nContourLevels=3,
        sigmaContourLevels=True,  # use 2D sigmas
        nBins=30,
        smoothingKernel=0.0,
        plotDensity=False,
        filledPlots=True,
        panelSpacing="tight",
        labelRotation=(False, False),
    )

    # show on screen
    # plt.show()

    # save to pdf
    plt.savefig("mcmc3.pdf")
