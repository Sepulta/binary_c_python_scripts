import os
import sys
from io import StringIO
import json


class Capturing(list):
    """
    Context manager to capture output and store it
    """

    def __enter__(self):
        """On entry we capture the stdout output"""

        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        """On exit we release the capture again"""

        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio  # free up some memory
        sys.stdout = self._stdout


def write_dummy_dataset(main_output_dir, system_parameters_per_metallicity_dict):
    """
    Function to write the dummy dataset:

    The required columns are
    """

    dco_type = "bhbh"

    # Create the directory
    os.makedirs(main_output_dir, exist_ok=True)

    #
    population_results_main = os.path.join(main_output_dir, "population_results")
    os.makedirs(population_results_main, exist_ok=True)

    # Set up info dict:
    info_dict = {}
    info_dict_filename = os.path.join(population_results_main, "info_dict.json")

    #
    for metallicity in system_parameters_per_metallicity_dict:
        metallicity_dir = os.path.join(
            population_results_main, "Z{}".format(metallicity)
        )
        os.makedirs(metallicity_dir, exist_ok=True)

        #
        dataset_filename = os.path.join(metallicity_dir, "{}.dat".format(dco_type))

        system_parameters = system_parameters_per_metallicity_dict[metallicity]

        # Write the system to the file
        with open(dataset_filename, "w") as f:
            # Write header
            f.write("\t".join(sorted(system_parameters.keys())))
            f.write("\n")

            # Write values
            f.write(
                "\t".join(
                    [
                        str(system_parameters[key])
                        for key in sorted(system_parameters.keys())
                    ]
                )
            )

        # Fill info dict
        info_dict["Z{}_{}".format(metallicity, dco_type)] = {
            "filename": dataset_filename,
            "type": dco_type,
            "metallicity": float(metallicity),
        }

    # Write info_dict:
    with open(info_dict_filename, "w") as f:
        f.write(
            json.dumps(
                info_dict,
            )
        )

    print("Wrote info_dict to {}".format(info_dict_filename))
    print("Wrote datafile to {}".format(dataset_filename))

    return system_parameters
