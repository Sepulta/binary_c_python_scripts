"""
Script to test the new dataframe method
"""

import os
import numpy as np

from grav_waves.convolution.functions.convolution_functions import (
    new_convolution_main as convolution_main,
)
from grav_waves.gw_analysis.functions.functions import make_dataset_dict
from grav_waves.settings import (
    config_dict_cosmology,
    convolution_settings,
)

# Main simulation dir
main_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/MID_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED"

dataset_dict_populations = make_dataset_dict(main_dir)

convolution_output_dir = os.path.join(
    main_dir, "convolution_without_detector_probability_results"
)
convolution_plot_dir = os.path.join(
    main_dir, "convolution_without_detector_probability_plots"
)

os.makedirs(convolution_output_dir, exist_ok=True)
os.makedirs(convolution_plot_dir, exist_ok=True)

# Set stepping
stepsize = 0.5
convolution_settings["stepsize"] = stepsize
convolution_settings["time_bins"] = np.arange(
    0.0001 - 0.5 * stepsize,
    (convolution_settings["max_interpolation_redshift"] - 1) + 0.5 * stepsize,
    stepsize,
)
convolution_settings["time_centers"] = (
    convolution_settings["time_bins"][1:] + convolution_settings["time_bins"][:-1]
) / 2
convolution_settings["use_new_dataframe_method"] = True
convolution_settings["add_detection_probability"] = False
convolution_settings[
    "global_query"
] = "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0)"

redshift_convolution_without_detector_probability_filename = os.path.join(
    convolution_output_dir, "convolution_without_detection_probability.p"
)

convolution_main(
    dataset_dict=dataset_dict_populations,
    convolution_configuration=convolution_settings,
    cosmology_configuration=config_dict_cosmology,
    output_filename=redshift_convolution_without_detector_probability_filename,
    dco_type="bhbh",
    verbosity=1,
)
