"""
Functions that deal with dataframes in input data
"""


import numpy as np
import pandas as pd

from grav_waves.gw_analysis.functions.functions import make_df
from grav_waves.settings import AGE_UNIVERSE_IN_YEAR


####
# Metallicity info functions
def get_dataset_metallicity_info(info_dict, type_key, testing=False):
    """
    Function to get and sort the metallicity values and keys that are present in the datasets.

    Here it is assumed that the sampling in metallicity was in log10

    we sort the values from low to high

    Output:
        sorted_metallicity_keys: Sorted keys of the info dict
        sorted_metallicity_keys_type: the sorted keys of the relevent metallicities
        sorted_metallicity_values: sorted values of the metallicity

        sorted_log10metallicity_values: sorted list of log10 metallicity values
        stepsizes_log10metallicity_values: stepsizes in log10 that each metallicity represents
    """

    # Read out the keys for the correct type and sort
    sorted_metallicity_keys = sorted(
        info_dict.keys(),
        key=lambda metallicity: float(metallicity.split("_")[0][1:]),
        reverse=True,
    )
    sorted_metallicity_keys_type = [
        el for el in sorted_metallicity_keys if el.endswith("_{}".format(type_key))
    ]
    sorted_metallicity_values = [
        info_dict[el]["metallicity"] for el in sorted_metallicity_keys_type
    ]

    # turn into log10 values
    # NOTE: why is this in reversed order?
    sorted_log10metallicity_values = np.array(
        [np.log10(info_dict[el]["metallicity"]) for el in sorted_metallicity_keys_type][
            ::-1
        ]
    )
    # sorted_log10metallicity_values = np.array([np.log10(info_dict[el]['metallicity']) for el in sorted_metallicity_keys_type])

    # Create stepsizes array
    if not testing:
        # Create hist edges for the log10metallicity values:
        bins_log10metallicity_values = (
            sorted_log10metallicity_values[1:] + sorted_log10metallicity_values[:-1]
        ) / 2

        # Add one to the left and one to the right:
        bins_log10metallicity_values = np.array(
            [
                sorted_log10metallicity_values[0]
                + (sorted_log10metallicity_values[0] - bins_log10metallicity_values[0])
            ]
            + list(bins_log10metallicity_values)
        )
        bins_log10metallicity_values = np.array(
            list(bins_log10metallicity_values)
            + [
                sorted_log10metallicity_values[-1]
                + (
                    sorted_log10metallicity_values[-1]
                    - bins_log10metallicity_values[-1]
                )
            ]
        )

        stepsizes_log10metallicity_values = np.diff(bins_log10metallicity_values)
    else:
        bins = np.zeros(1)
        stepsizes_log10metallicity_values = np.ones(len(sorted_metallicity_values)) * (
            1 / len(sorted_metallicity_values)
        )

    #
    return (
        sorted_metallicity_keys,
        sorted_metallicity_keys_type,
        sorted_metallicity_values,
        sorted_log10metallicity_values,
        stepsizes_log10metallicity_values,
        bins_log10metallicity_values,
    )


####
# Dataframe functions
def load_dataframes(
    info_dict, convolution_configuration, dco_type, limit_merging_time=True
):
    """
    Function to open and load the dataframes into memory.

    During this we:
    - Add some extra columns to the datafram (chirpmass, total mass)

    - We create a copy of the number_per_solar_mass and name it number_per_solar_mass_values. That is the one we should use in our output
        This column will be the one that will be modified if we want to incorporate the detection probabilities

    - We create a column of merger_time_values and formation_time_values that will contain either the normal time values or redshift values
    """

    dataframe_dict = {}

    # get metallicity info
    _, sorted_metallicity_keys_type, _, _, _, _ = get_dataset_metallicity_info(
        info_dict, dco_type, testing=convolution_configuration["testing_mode"]
    )

    # Loop over the metallicities
    convolution_configuration["logger"].info("Preloading data")
    for metallicity_key in sorted_metallicity_keys_type:
        # Open dict
        metallicity_dict = info_dict[metallicity_key]
        convolution_configuration["logger"].info(
            "Loading {}".format(metallicity_dict["filename"])
        )

        # Load the dataframe, and pass in the conversion and binary fraction values.
        merging_df = make_df(
            metallicity_dict["filename"],
            metallicity_dict["metallicity"],
            average_mass_system=convolution_configuration["average_mass_system"],
            binary_fraction_factor=convolution_configuration["binary_fraction"],
            limit_merging_time=limit_merging_time,
        )

        # Limit on time to prevent too much data from being there
        if limit_merging_time:
            merging_df = merging_df[
                merging_df.merger_time_values_in_years < AGE_UNIVERSE_IN_YEAR
            ]

        # Make a copy of the number_per_solar_mass
        merging_df["number_per_solar_mass_values"] = merging_df[
            "number_per_solar_mass"
        ].values

        # Create extra mass columns:
        merging_df["total_mass"] = merging_df["mass_1"] + merging_df["mass_2"]
        merging_df["primary_mass"] = merging_df[["mass_1", "mass_2"]].max(axis=1)
        merging_df["secondary_mass"] = merging_df[["mass_1", "mass_2"]].min(axis=1)

        # First query location. This query is the global query where we can still access things ending with _1 or _2
        # Querying here will limit the subset on which you can query the any mass
        if convolution_configuration.get("global_query", None):
            merging_df = merging_df.query(convolution_configuration.get("global_query"))

        # Store in the dict
        dataframe_dict[metallicity_key] = merging_df

    #
    return dataframe_dict


def create_combined_df(
    dataframe_dict, sorted_metallicity_keys_type, convolution_configuration
):
    """
    Function to set up the combined dataframe of all systems, with the global query and merging time already taken care of by load_dataframes
    """

    # Set the columns to keep
    keep_columns = convolution_configuration["include_dataframe_columns_output"] + [
        "local_index",
        "formation_time_values_in_years",
        "merger_time_values_in_years",
        "number_per_solar_mass_values",
    ]

    # Create combined dataframe:
    combined_dataframe = pd.DataFrame()

    # Loop over the metallicities
    convolution_configuration["logger"].info("Looping over datasets")
    for metallicity_key in sorted_metallicity_keys_type:
        # Open dataframe
        convolution_configuration["logger"].info(
            "Loading dataframe for {}".format(metallicity_key)
        )

        # Open the dataframe
        metallicity = float(metallicity_key.split("_")[0][1:])
        merging_df = dataframe_dict[metallicity_key]
        merging_df["metallicity"] = metallicity

        #
        merging_df["local_index"] = merging_df.index + merging_df["metallicity"]

        # Drop columns from the dataframe
        drop_columns = [
            column for column in merging_df.columns if not column in keep_columns
        ]
        reduced_merging_df = merging_df.drop(columns=drop_columns)

        # Add the dataframes to the new dataframe
        combined_dataframe = pd.concat(
            [combined_dataframe, reduced_merging_df], ignore_index=True
        )

    # return
    return combined_dataframe
