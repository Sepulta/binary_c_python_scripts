import pandas as pd


df = pd.read_hdf("output/test.hdf5", "data/combined_dataframes")
print(df["metallicity"].unique())
print(df)
filtered_df = pd.read_hdf("output/filtered_test.hdf5", "data/combined_dataframes")
print(filtered_df["metallicity"].unique())
print(filtered_df)
