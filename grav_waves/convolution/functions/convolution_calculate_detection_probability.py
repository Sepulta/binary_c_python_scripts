"""
Function to calcualte the detection probability array
"""

import numpy as np

from grav_waves.settings import cosmo

from COMPAS.utils.PythonScripts.CosmicIntegration.selection_effects import (
    detection_probability,
)


def calculate_detection_probability_array(
    redshift_value, combined_data_array, convolution_configuration
):
    """
    Function to calculate the detection probability array:
    - given a configuration for the detector (SNR threshold and sensitivity, i.e. design, 02 etc)
    - and the current redshift and distance in MPC of the merging event
    - we calculate the detection probability of the system given mass_1 and mass_2
    """

    #
    distance_in_mpc = cosmo.luminosity_distance(redshift_value).value

    # Get array of detection probs
    detection_probability_array = detection_probability(
        combined_data_array["mass_1"] * (1 + redshift_value),
        combined_data_array["mass_2"] * (1 + redshift_value),
        redshift_value,
        distance_in_mpc,
        convolution_configuration["snr_threshold"],
        sensitivity=convolution_configuration["sensitivity"],
    )

    # Multiply column of number_per_solar_mass
    detection_probability_array *= detection_probability_array

    return detection_probability_array
