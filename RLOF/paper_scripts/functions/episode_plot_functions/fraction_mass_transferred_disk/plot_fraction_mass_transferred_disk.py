"""
Routine to plot the fraction of total time spent transferring mass through a disk
"""

import os

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

#
from grav_waves.settings import convolution_settings

#
from RLOF.scripts.plot_episode_output.functions import (
    get_rlof_episode_dataset_dict,
    get_rlof_episode_dataset_dict_new,
)
from RLOF.paper_scripts.functions.episode_plot_functions.utility import filter_df_all

#
from david_phd_functions.plotting.linestyle_hatch_colorlist import (
    linestyle_list,
    color_list,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

from RLOF.paper_scripts.functions.episode_plot_functions.utility import (
    filter_df_all,
    get_median_percentiles,
    add_cdf_percentage_line,
    handle_normal_bootstrap_plots,
    handle_cdf_bootstrap_plots,
    handle_bootstrap,
)


load_mpl_rc()


def get_quantities(df, convolution_settings, **kwargs):
    """
    Function to get the quantities we want to show
    """

    # Filter the df
    stable_mt_ms, disk_df, total_prob = filter_df_all(
        df, convolution_settings, **kwargs
    )

    # Calculate normalized number per solarmass
    number_per_solar_mass = disk_df["number_per_solar_mass"] / total_prob

    # Calculate average mass weighted synchronicity factor
    average_mass_transferred_through_disk = disk_df[
        "total_mass_transferred_through_disk"
    ] / (disk_df["total_mass_transferred"])

    return number_per_solar_mass, average_mass_transferred_through_disk


def get_data(dataset_dict, bootstraps, bootstrap_entire_population):
    """
    Function to get the data
    """

    #
    ratio_dict = {}
    hist_dict = {}

    # Determine bins
    bins = 10 ** np.linspace(-5, 1.0, 40)
    bincenters = (bins[1:] + bins[:-1]) / 2

    #################
    # Loop over metallicities and get the data
    for metallicity in dataset_dict:
        # Read out data and create dataframe
        df = pd.read_csv(dataset_dict[metallicity]["filename"], sep="\s+", header=0)

        weight, quantity = get_quantities(df, convolution_settings)

        # Add ratio angmom returned vs initial orbital ang mom
        ratio_dict[metallicity] = {
            "ratio": quantity,
            "number_per_solar_mass": weight,
            "bins": bins,
        }

        # Calculate the histogram here?
        hist, _ = np.histogram(
            ratio_dict[metallicity]["ratio"],
            bins=bins,
            weights=ratio_dict[metallicity]["number_per_solar_mass"],
        )
        cdf_array = np.cumsum(hist) / np.sum(hist)

        # Calculate cdf
        hist_dict[metallicity] = {"hist": hist, "centers": bincenters, "cdf": cdf_array}

        # Do bootstrapping
        if bootstraps:
            handle_bootstrap(
                dataset_dict=dataset_dict,
                metallicity=metallicity,
                bins=bins,
                ratio_dict=ratio_dict,
                hist_dict=hist_dict,
                bootstrap_entire_population=bootstrap_entire_population,
                bootstraps=bootstraps,
                convolution_settings=convolution_settings,
                get_quantities_func=get_quantities,
            )

    return ratio_dict, hist_dict


def plot_fraction_mass_transferred_disk(
    dataset_dict,
    add_cdf=False,
    bootstraps=0,
    bootstrap_entire_population=True,
    plot_settings={},
):
    """
    Function to plot the fraction of mass transfered through disk when the MT is initially stable for all metallicities provided in the dataset_dict

    Args:
        dataset_dict: dictionary containing info for multiple metallicities
        plot_settings: dict controlling the plotting
    """

    min_ratio = 1
    max_ratio = 0
    fontsize = 30

    # Some settings
    fontsize = 30

    # Calculate the data and bootstrap if necessary
    ratio_dict, hist_dict = get_data(
        dataset_dict,
        bootstraps=bootstraps,
        bootstrap_entire_population=bootstrap_entire_population,
    )

    ######
    # Plot
    fig = plt.figure(figsize=(16, 16))

    #
    gs = fig.add_gridspec(nrows=10, ncols=1)

    #
    if not add_cdf:
        ax = fig.add_subplot(gs[:, :])
    else:
        ax = fig.add_subplot(gs[:7, :])
        ax_cdf = fig.add_subplot(gs[7:, :])

    #
    for i, metallicity in enumerate(sorted(ratio_dict)):
        # the histogram of the data
        n, bins, _ = ax.hist(
            ratio_dict[metallicity]["ratio"],
            bins=ratio_dict[metallicity]["bins"],
            weights=ratio_dict[metallicity]["number_per_solar_mass"],
            alpha=0.25,
            # label="Z = {} ({:.2f})".format(
            #     metallicity,
            #     ratio_dict[metallicity]["number_per_solar_mass"].sum(),
            # ),
            histtype="step",
            linestyle=linestyle_list[i] if not i == 0 else "-",
            linewidth=4,
        )

        #####
        # Handle bootstrap plots
        handle_normal_bootstrap_plots(
            ax, hist_dict, ratio_dict, metallicity, index_i=i, bootstraps=bootstraps
        )

        if add_cdf:
            #####
            # Handle the cdf plot and whether to plot bootstrap
            handle_cdf_bootstrap_plots(
                ax_cdf,
                hist_dict,
                ratio_dict,
                metallicity,
                index_i=i,
                bootstraps=bootstraps,
            )

            ####
            # Add 50% line
            add_cdf_percentage_line(
                ax_cdf, hist_dict[metallicity], fraction=0.5, index_i=i, plot_kwargs={}
            )

    ###
    # make up
    ax.set_xlim([1e-3, 10])
    ax.set_ylim([1e-3, 1])

    # set legend
    ax.legend(loc=2, fontsize=fontsize)

    # Set labels
    ax.set_ylabel(
        "Normalized number of systems\nper formed solarmass", fontsize=fontsize
    )
    ax.set_xlabel(
        "Fraction of mass transferred through disk",
        fontsize=fontsize,
    )

    # Set scales
    ax.set_xscale("log")
    ax.set_yscale("log")

    #
    if add_cdf:
        ax_cdf.set_ylim([0, 1.1])
        ax_cdf.set_xscale("log")
        ax_cdf.set_xlabel(ax.get_xlabel())
        ax.set_xlabel("")
        ax.set_xticklabels([])
        ax_cdf.set_xlim(ax.get_xlim())

    #
    fig.tight_layout()

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    ###########
    # Settings
    bootstraps = 50
    bootstrap_entire_population = True
    add_cdf = True

    ########
    # Set result root
    result_root = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"),
        "RLOF/server_results/",
    )

    ########
    # Set Output dir
    target_dir = "/home/david/Dropbox/Academic/PHD/papers/paper_disks/paper_tex/figures/episode_plots"
    target_dir = "plots"

    ########
    # Create datasets loop and run
    datasets = [
        # "MID_RES_SANA",
        # "MID_LOW_RES_RLOF_2021_SANA",
        # "MID_LOW_RES_RLOF_2021_SANA_RLOF_enable_stream_integration_project_1",
        "MID_RES_RLOF_2021_SANA",
        "MID_RES_RLOF_2021_SANA_RLOF_enable_stream_integration_project_1",
    ]
    for simname in datasets:
        simulation_dir = os.path.join(result_root, simname)
        rlof_episode_dataset_dict = get_rlof_episode_dataset_dict_new(simulation_dir)
        dataset_dict = rlof_episode_dataset_dict["datasets"]

        #
        basename = "{}_fraction_mass_transferred_disk.pdf".format(simname)

        #
        plot_fraction_mass_transferred_disk(
            dataset_dict,
            bootstraps=bootstraps,
            bootstrap_entire_population=bootstrap_entire_population,
            add_cdf=add_cdf,
            plot_settings={
                "show_plot": False,
                "output_name": os.path.join(target_dir, basename),
            },
        )
