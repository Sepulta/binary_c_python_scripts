"""
Routine to plot the disk mt time categories
"""

import os

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

#
from grav_waves.settings import convolution_settings

#
from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict
from RLOF.paper_scripts.functions.episode_plot_functions.utility import (
    filter_df_all,
    add_category_columns_mass,
)

#
from david_phd_functions.plotting.custom_mpl_settings import LINESTYLE_TUPLE
from david_phd_functions.plotting.utils import show_and_save_plot, add_plot_info
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def plot_mt_category_disk_mass(dataset_dict, add_cdf=False, plot_settings={}):
    """
    Function to plot the fraction of mass transfered through disk when the MT is initially stable for all metallicities provided in the dataset_dict

    Args:
        dataset_dict: dictionary containing info for multiple metallicities
        plot_settings: dict controlling the plotting
    """

    # Determine bins
    bin_size = 0.1
    bins = np.arange(0, 1 + bin_size, bin_size)
    bincenters = (bins[1:] + bins[:-1]) / 2

    #
    min_ratio = 1
    max_ratio = 0

    #
    ratio_dict = {}

    #
    for metallicity in dataset_dict:
        # Read out data and create dataframe
        df = pd.read_csv(dataset_dict[metallicity]["filename"], sep="\s+", header=0)

        # Filter the df
        stable_mt_ms, disk_df, total_prob = filter_df_all(df, convolution_settings)

        # Query the datafame
        df = df[df.initial_stability == 0]  # Stable mass transfer
        df = df[df.initial_stellar_type_accretor == 1]  # Accretion onto main sequence

        # Normalise to total probability of all stable MT onto MS
        disk_df["number_per_solar_mass"] = disk_df["number_per_solar_mass"] / total_prob

        # Add columns related to the MT category calculation
        disk_df = add_category_columns_mass(disk_df)

        #
        print(
            "Maximum fractional error mass categories: {}".format(
                disk_df[
                    "fractional_error_mass_transferred_through_disk_categories"
                ].max()
            )
        )
        print(
            "Maximum absolute error mass categories: {}".format(
                disk_df["absolute_error_mass_transferred_through_disk_categories"].max()
            )
        )

        hist_cat_4 = np.histogram(
            disk_df["fraction_mass_transferred_through_disk_category_4"],
            bins=bins,
            weights=disk_df["number_per_solar_mass"],
        )
        print(hist_cat_4)

        # ######
        # # Plot
        # fig = plt.figure(figsize=(20, 20))

        # #
        # gs = fig.add_gridspec(nrows=10, ncols=1)
        # ax = fig.add_subplot(gs[:, :])

        # stacked_histograms = np.zeros(bincenters.shape[0])

        # for i in range(6):
        #     # Calculate the histograms
        #     hist = np.histogram(disk_mt['fraction_mass_transferred_through_disk_category_{}'.format(i)], bins=bins, weights=disk_mt['number_per_solar_mass'], density=True)

        #     #
        #     ax.bar(bincenters, hist[0], bin_size, label='category {}'.format(i), bottom=stacked_histograms)

        #     stacked_histograms += hist[0]

        #     # ax.bar(
        #     #         ,
        #     #     array_total,
        #     #     binsize,
        #     #     label='total{}'.format(' (base_query: {})'.format(return_string_querylist(base_querylist)) if base_querylist is not None else ''),
        #     #     color='blue',
        #     #     alpha=0.5,
        #     #     hatch='/',
        #     #     antialiased=True,
        #     #     rasterized=True,
        #     # )
        # plt.show()

    # # set legend
    # ax.legend(loc=2)

    # # Set labels
    # ax.set_ylabel("Normalized number of systems")

    # ax.set_xlabel("Fraction of mass transferred through disk during RLOF episode")

    # ax.set_title("Fraction of total mass transferred through a disk during RLOF episode\nFor systems with initially stable RLOF onto MS. Normalized by all stable MT onto on MS", fontsize=24)

    # # Set scales
    # ax.set_xscale('log')
    # ax.set_yscale('log')

    # if add_cdf:
    #     ax_cdf.set_ylim([0, 1.1])
    #     ax_cdf.set_xscale('log')
    #     ax_cdf.set_xlabel(ax.get_xlabel())
    #     ax.set_xlabel('')
    #     ax_cdf.set_xlim(ax.get_xlim())

    # #######################
    # # Save and finish
    # show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    ########
    # Set up data
    simulation_dir = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions"
    rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(simulation_dir)
    dataset_dict = rlof_episode_dataset_dict["datasets"]

    ########
    # Set filename
    target_dir = "/home/david/Dropbox/Academic/PHD/papers/paper_disks/paper_tex/figures/episode_plots"
    target_dir = "plots"

    #
    basename = "plot_mt_category_disk_mass.pdf"

    #
    plot_mt_category_disk_mass(
        dataset_dict,
        add_cdf=True,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(target_dir, basename),
        },
    )
