"""
Function to specifically generate all the tables and plots for the paper
"""

import os

#
from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_dataset_for_simname,
    copy_to_paper_directory_function,
)

# Tables
from RLOF.scripts.plot_ensemble_output.routines.table_MS_fraction_all import (
    generate_table_MS_fraction_all,
)

# FIgures

# # Datasets
# population_datasets_sana = generate_dataset_for_simname('/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions/')
# population_datasets_ms = generate_dataset_for_simname('/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_MOE_DISTEFANO_2021_ENSEMBLE_fractions/')


def generate_figures_and_tables_paper(
    population_datasets_sana,
    population_datasets_msm,
    output_dir,
    verbose=0,
    testing=True,
    copy_to_paper_directory=False,
):
    """
    This function only gets a specified which output directory it should write to

    It will then generate the figures and copy them to the designated location for the paper
    """

    ##########
    # Set up

    # Register directories here
    local_figure_directory = os.path.join(output_dir, "figures")
    local_tables_directory = os.path.join(output_dir, "tables")
    os.makedirs(local_figure_directory, exist_ok=True)
    os.makedirs(local_tables_directory, exist_ok=True)

    # Set the directories of the paper
    directory_paper_disks = os.path.expanduser("~/PHD/papers/paper_disks/paper_tex")
    directory_paper_disks_figures = os.path.join(directory_paper_disks, "figures")
    directory_paper_disks_tables = os.path.join(directory_paper_disks, "tables")

    #############
    # Tables

    ## Table 1:
    # Table for fraction of MT onto MS vs all MT for sana dataset
    # fiducial
    print(
        "Generating table 1: Table for fraction of MT onto MS vs all MT for sana dataset"
    )
    table_MS_fraction_all_filename = "fractions_onto_ms_table_fiducial.tex"
    generate_table_MS_fraction_all(
        population_datasets_sana["datasets"],
        output_filename=os.path.join(
            local_tables_directory, table_MS_fraction_all_filename
        ),
        caption="Fractions of mass transfer onto main-sequence stars normalised to all mass transfer, weighted by either mass transferred or time spent.",
        label="table:fractions_onto_ms_table_fiducial",
        testing=testing,
        verbose=verbose,
    )
    if copy_to_paper_directory:
        copy_to_paper_directory_function(
            local_tables_directory,
            directory_paper_disks_tables,
            table_MS_fraction_all_filename,
            verbose=verbose,
        )


# generate_figures_and_tables_paper(
#     os.path.abspath('paper_output'),
#         verbose=1, testing=True, copy_to_paper_directory=True
# )
