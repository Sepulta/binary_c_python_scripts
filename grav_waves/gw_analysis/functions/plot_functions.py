"""
Plotting routines for the per-dataset info
"""

import os

import numpy as np

from matplotlib import gridspec
import matplotlib.colors
import matplotlib.pyplot as plt
import matplotlib as mpl

import pycbc.catalog

from grav_waves.settings import (
    mink_belc_2015_bhbh,
    LINESTYLE_TUPLE,
    AGE_UNIVERSE_IN_YEAR,
    resolution_settings,
    config_dict_cosmology,
    coens_channels_query_dict,
    liekes_channels_query_dict,
)
from grav_waves.gw_analysis.functions.functions import (
    make_df,
    load_info_dict,
    calculate_numbers,
)
from grav_waves.convolution.functions.convolution_functions import (
    get_dataset_metallicity_info,
)

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()
mpl.rc("text", usetex=False)
# https://proplot.readthedocs.io/en/latest/cycles.html

##
# Utility functions
def filter_ppisn_type(df, dict_ppisn, ppisn_type, ppisn_name=""):
    """
    Function to filter a dataframe that contains PPISN_NONE
    """

    if (
        len(
            df[
                (df["undergone_ppisn_1"] == ppisn_type)
                ^ (df["undergone_ppisn_2"] == ppisn_type)
            ].index
        )
        > 0
    ):
        print("===================================")
        print(
            "Found PPISN: {} systems in {} {} ({})".format(
                ppisn_type,
                dict_ppisn.get("filename", ""),
                dict_ppisn.get("metallicity", ""),
                ppisn_name,
            )
        )
        print("===================================")
        # df = df - df[(df['undergone_ppisn_1'] == ppisn_type) ^ (df['undergone_ppisn_2'] == ppisn_type)]

    df = df[df.undergone_ppisn_1 != ppisn_type]
    df = df[df.undergone_ppisn_2 != ppisn_type]

    return df


# Rates with assumed star formation and/or density of metallicity
def plot_MWEG_merger_rates(info_dict, plot_settings=None):
    """
    Function to plot the MWEG merger rates of the three different types of mergers

    Added to this plot are the results of mink_belc_2015_bhbh
    """

    if not plot_settings:
        plot_settings = {}

    sorted_keys = sorted(
        info_dict.keys(), key=lambda metallicity: float(metallicity.split("_")[0][1:])
    )

    metallicities_bhbh = [
        info_dict[el]["metallicity"]
        for el in sorted_keys
        if info_dict[el]["type"] == "bhbh"
    ]
    metallicities_bhns = [
        info_dict[el]["metallicity"]
        for el in sorted_keys
        if info_dict[el]["type"] == "bhns"
    ]
    metallicities_nsns = [
        info_dict[el]["metallicity"]
        for el in sorted_keys
        if info_dict[el]["type"] == "nsns"
    ]

    mweg_merger_rates_bhbh = [
        info_dict[el]["merger_rate_MWEG_myr"]
        for el in sorted_keys
        if info_dict[el]["type"] == "bhbh"
    ]
    mweg_merger_rates_bhns = [
        info_dict[el]["merger_rate_MWEG_myr"]
        for el in sorted_keys
        if info_dict[el]["type"] == "bhns"
    ]
    mweg_merger_rates_nsns = [
        info_dict[el]["merger_rate_MWEG_myr"]
        for el in sorted_keys
        if info_dict[el]["type"] == "nsns"
    ]

    fig = plt.figure(figsize=(32, 32))

    plt.scatter(
        [0.02] * len(mink_belc_2015_bhbh),
        [mink_belc_2015_bhbh[el]["0.02"]["a"] for el in mink_belc_2015_bhbh],
        marker="d",
        label="Z={} a Values de mink belczyniski 2015".format(0.02),
    )

    # plt.scatter([0.02] * len(mink_belc_2015_bhbh),
    #     [mink_belc_2015_bhbh[el]["0.02"]["b"] for el in mink_belc_2015_bhbh],
    #     label='Z={} b Values de mink belczyniski 2015'.format(0.02))

    plt.scatter(
        [0.002] * len(mink_belc_2015_bhbh),
        [mink_belc_2015_bhbh[el]["0.002"]["a"] for el in mink_belc_2015_bhbh],
        marker="d",
        label="Z={} a Values de mink belczyniski 2015".format(0.002),
    )

    # plt.scatter([0.002] * len(mink_belc_2015_bhbh),
    #     [mink_belc_2015_bhbh[el]["0.002"]["b"] for el in mink_belc_2015_bhbh],
    #     label='Z={} b Values de mink belczyniski 2015'.format(0.002))

    # Plot milkyway equivalent merger rate
    plt.plot(metallicities_bhbh, mweg_merger_rates_bhbh, label="bhbh mergers")
    plt.plot(metallicities_bhns, mweg_merger_rates_bhns, "--", label="bhns mergers")
    plt.plot(metallicities_nsns, mweg_merger_rates_nsns, "-.", label="nsns mergers")

    plt.legend()
    plt.title(
        r"$\mathcal{R}_{gal}$ = merging_prob_per_solar_mass*SFR*pow(10, 6) $[Myr^{-1}]$"
    )
    plt.xlabel("Metallicity [Z]")
    plt.ylabel("Milkyway equivalent\nmerger rate")
    plt.yscale("log")
    plt.xscale("log")
    plt.xticks(labels=[0.02, 0.002, 0.0002], ticks=[0.02, 0.002, 0.0002])

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_RVOL_merger_rates(info_dict, plot_settings=None):
    """
    Function to plot the Volumetric merger rates of the three different types of mergers
    """

    if not plot_settings:
        plot_settings = {}

    sorted_keys = sorted(
        info_dict.keys(), key=lambda metallicity: float(metallicity.split("_")[0][1:])
    )

    metallicities_bhbh = [
        info_dict[el]["metallicity"]
        for el in sorted_keys
        if info_dict[el]["type"] == "bhbh"
    ]
    metallicities_bhns = [
        info_dict[el]["metallicity"]
        for el in sorted_keys
        if info_dict[el]["type"] == "bhns"
    ]
    metallicities_nsns = [
        info_dict[el]["metallicity"]
        for el in sorted_keys
        if info_dict[el]["type"] == "nsns"
    ]

    rvol_bhbh = [
        info_dict[el]["rvol"] for el in sorted_keys if info_dict[el]["type"] == "bhbh"
    ]
    rvol_bhns = [
        info_dict[el]["rvol"] for el in sorted_keys if info_dict[el]["type"] == "bhns"
    ]
    rvol_nsns = [
        info_dict[el]["rvol"] for el in sorted_keys if info_dict[el]["type"] == "nsns"
    ]

    # Plot Volumetric merger rate
    fig = plt.figure(figsize=(32, 32))

    plt.plot(metallicities_bhbh, rvol_bhbh, label="bhbh mergers")
    plt.plot(metallicities_bhns, rvol_bhns, "--", label="bhns mergers")
    plt.plot(metallicities_nsns, rvol_nsns, "-.", label="nsns mergers")

    plt.legend()
    plt.xlabel("Metallicity [Z]")
    plt.ylabel("Volumetric merger rate")
    plt.title(
        r"$\mathcal{R}_{vol}$ = 10 yr$^{-1}$ Gpc$^{-3}$ $[\rho/0.01 Mpc^{-3}]$ $[\mathcal{R}_{gal}/Myr^{-1}]$"
    )
    plt.yscale("log")
    plt.xscale("log")
    plt.xticks(labels=[0.02, 0.002, 0.0002], ticks=[0.02, 0.002, 0.0002])

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


# Sort of convolution
def merging_time_convolved_with_SFR(dataset_dict, plot_settings=None, df_kwargs=None):
    """
    Function that plots the merging with a SFR convolved

    ## Situation:
    We want to take the above merging time distribution,
    for this specific metallicity, and 'concolve' a star formation rate with it.
    This star formation history, although in reality a continous one,
    can be discretized into blocks.
    We can take this star formation rate, 'bin' it, (so it will be 3.5
    solar mass per year in a bin of say 1e6 year. So 3.5e6 solar mass at that time),
    and multiply that with the merger time distribution.
    Each new bin will shift the merger time distribution by
    the binwidth * prev n bins, mimicking stars formed later.

    Take a star formation rate of 3.5 solar mass per year. for 10gyr:
    Stepsize = 1 gyr
    """

    if not plot_settings:
        plot_settings = {}

    if not df_kwargs:
        df_kwargs = {}

    df = make_df(
        dataset_dict["filename"],
        probability_to_number_to_solarmass_factor=convolution_settings["mass_in_stars"],
        binary_fraction_factor=convolution_settings["binary_fraction"],
        **df_kwargs,
    )

    step = 0.01
    bins = np.arange(0 - step / 2, 10 + step / 2, step)
    binwidth = bins[1:] - bins[:-1]
    bin_centers = (bins[1:] + bins[:-1]) / 2

    ## Plot bins
    # plt.bar(bins+0.5, [3.5]*len(bins), 1, edgecolor='black')
    # plt.ylabel("Solar mass per year")
    # plt.xlabel("Time [Gyr]")
    # plt.show()

    ## Plot years where a artificial starbusts happens.
    # plt.plot(bin_centers, 3.5 * binwidth * 1e9, 'go')
    # plt.ylabel("Mass stars formed [Msol]")
    # plt.xlabel("Time offset [Gyr]")
    # plt.show()

    amt_per_step = df["merger_time_values_in_years"].size
    merger_times = np.zeros(amt_per_step * len(bin_centers))
    merger_amounts = np.zeros(amt_per_step * len(bin_centers))
    for i, bin_center in enumerate(bin_centers):
        merger_times[amt_per_step * i : amt_per_step * (i + 1)] = df[
            "merger_time_values_in_years"
        ] + (bin_center * 1e9)
        merger_amounts[amt_per_step * i : amt_per_step * (i + 1)] = (
            df["probability"] * 3.5 * binwidth[i] * 1e9
        )

    bins = np.linspace(0, 5e4, 50)

    # Plot convolved info
    plt.hist(
        merger_times / 1e6,
        weights=merger_amounts,
        bins=bins,
        histtype="step",
        linewidth=8,
    )
    plt.yscale("log")
    # plt.xscale('log')
    plt.title(
        "Merger time with a star formation convolved for {} (Z={})".format(
            dataset_dict["type"], dataset_dict["metallicity"]
        )
    )
    plt.ylabel(r"Probability")
    plt.xlabel(r"Merger time (Time after starburst) [Myr]")
    plt.axvline(AGE_UNIVERSE_IN_YEAR / 1e6)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


# Plot with observations overlaid
def plot_all_chirpmasses_with_observations(
    merger_info,
    type_key,
    convolution_settings,
    display_log_string=False,
    plot_settings=None,
):
    """
    Function to plot all the different chirpmasses and plot the observed systems from gwtc1 and 2 on there.
    """

    if not plot_settings:
        plot_settings = {}

    if convolution_settings.get("global_query", None):
        plot_settings["runname"] = plot_settings.get(
            "runname", ""
        ) + " (query: {})".format(convolution_settings.get("global_query"))

    # Set up canvas and grid
    fig3 = plt.figure(constrained_layout=False)
    gs = fig3.add_gridspec(
        4,
        4,
    )
    ax1 = fig3.add_subplot(gs[0, :])
    ax2 = fig3.add_subplot(gs[1:, :], sharex=ax1)

    # Mass steps
    if type_key == "bhbh":
        bins = resolution_settings["plot_all_chirpmasses_with_observations"]["bhbh"]
    elif type_key == "bhns":
        bins = resolution_settings["plot_all_chirpmasses_with_observations"]["bhns"]
    elif type_key == "nsns":
        bins = resolution_settings["plot_all_chirpmasses_with_observations"]["nsns"]
    else:
        print("Key unknown")
        raise ValueError

    # Add calculated chirp masses
    max_val = 0
    el_i = 0

    # loop
    for el in sorted(merger_info.keys(), reverse=True):
        info_dict = merger_info[el]
        if info_dict["type"] == type_key:

            merging_df = make_df(
                info_dict["filename"],
                info_dict["metallicity"],
                probability_to_number_to_solarmass_factor=convolution_settings[
                    "mass_in_stars"
                ],
                binary_fraction_factor=convolution_settings["binary_fraction"],
            )

            # Do query if its passed
            if convolution_settings.get("global_query", None):
                merging_df = merging_df.query(convolution_settings.get("global_query"))

            (n, bins, _) = ax2.hist(
                merging_df["chirpmass"],
                bins=bins,
                weights=merging_df["number_per_solar_mass"],
                density=False,
                label=r"{} ({:.2e} M$^{{-1}}_{{\odot}}$)".format(
                    "Z={:.4g}".format(info_dict["metallicity"])
                    if not display_log_string
                    else "Log10 Z={:.4g}".format(np.log10(info_dict["metallicity"])),
                    merging_df["number_per_solar_mass"].sum(),
                ),
                histtype="step",
                linestyle=LINESTYLE_TUPLE[el_i][1] if not el_i == 0 else "-",
                linewidth=4,
            )
            local_max_val = np.max(n)

            if local_max_val > max_val:
                max_val = local_max_val
            el_i += 1

    if type_key == "bhbh":
        # Add observations gwtc-2
        c_gwtc2 = pycbc.catalog.Catalog(source="gwtc-1")
        mchirp, elow, ehigh = c_gwtc2.median1d("mchirp", return_errors=True)
        ax1.errorbar(
            mchirp,
            mchirp / np.max(mchirp),
            xerr=[-elow, ehigh],
            fmt="o",
            markersize=7,
            label="Observations gwtc-1",
            alpha=0.7,
        )

        # Add observations gwtc-2
        c_gwtc2 = pycbc.catalog.Catalog(source="gwtc-2")
        mchirp, elow, ehigh = c_gwtc2.median1d("mchirp", return_errors=True)
        ax1.errorbar(
            mchirp,
            mchirp / np.max(mchirp),
            xerr=[-elow, ehigh],
            fmt="o",
            markersize=7,
            label="Observations gwtc-2",
            alpha=0.7,
        )

    # Some makeup
    ax1.yaxis.set_ticklabels([])
    ax2.set_xlabel(
        r"Chirp mass $\mathcal{M} = (m1\ m2)^{3/5}/(m1+m2)^{1/5}$ [M]", fontsize=26
    )
    ax2.set_ylabel("Normalized yield (number per formed solar mass)", fontsize=26)

    ax2.set_yscale("log")

    gs.update(hspace=0)
    ax1.set_title(
        "Chirp mass distributions of binary_c fiducial runs: {}".format(type_key)
    )

    ax2.legend(fontsize=12, loc=1, framealpha=0.5, ncol=2)

    fig3.subplots_adjust(top=0.85, hspace=0.5)

    # Add info and plot the figure
    fig3 = add_plot_info(fig3, plot_settings)
    show_and_save_plot(fig3, plot_settings)


# Plot routines from other papers
def stevenson_plot(
    info_dict,
    type_sub_key,
    convolution_settings,
    display_log_string=False,
    plot_settings=None,
):
    """
    Function to plot the plot of marchant 19. Putting the merging and total population next to. https://ui.adsabs.harvard.edu/abs/2019ApJ...882...36M/abstract
    eachother.
    """

    if convolution_settings.get("global_query", None):
        plot_settings["runname"] = plot_settings.get(
            "runname", ""
        ) + " (query: {})".format(convolution_settings.get("global_query"))

    if not plot_settings:
        plot_settings = {}

    # Limit and bins
    xlim_max = 50
    bins = resolution_settings["stevenson_plot"]["bhbh"]

    # Load the dataframes
    ppisn_on_df_merging = make_df(
        info_dict[type_sub_key]["filename"],
        info_dict[type_sub_key]["metallicity"],
        probability_to_number_to_solarmass_factor=convolution_settings["mass_in_stars"],
        binary_fraction_factor=convolution_settings["binary_fraction"],
        limit_merging_time=True,
    )
    ppisn_on_df_all = make_df(
        info_dict[type_sub_key]["filename"],
        info_dict[type_sub_key]["metallicity"],
        probability_to_number_to_solarmass_factor=convolution_settings["mass_in_stars"],
        binary_fraction_factor=convolution_settings["binary_fraction"],
        limit_merging_time=False,
    )

    # Do query if its passed
    if convolution_settings.get("global_query", None):
        ppisn_on_df_merging = ppisn_on_df_merging.query(
            convolution_settings.get("global_query")
        )
        ppisn_on_df_all = ppisn_on_df_all.query(
            convolution_settings.get("global_query")
        )

    # Check the dataframes on PPISN_NONE:
    ppisn_on_df_merging = filter_ppisn_type(
        ppisn_on_df_merging, info_dict, ppisn_type=5
    )
    ppisn_on_df_all = filter_ppisn_type(ppisn_on_df_all, info_dict, ppisn_type=5)

    #####################
    # same for PHDIS
    ppisn_on_df_merging = filter_ppisn_type(
        ppisn_on_df_merging, info_dict, ppisn_type=4
    )
    ppisn_on_df_all = filter_ppisn_type(ppisn_on_df_all, info_dict, ppisn_type=4)

    # Create subplots
    fig = plt.figure(constrained_layout=False, figsize=(20, 20))
    gs = fig.add_gridspec(8, 2)

    # Set up the axes for the merging dataframes
    axes_merging = []
    ax_merging_1 = fig.add_subplot(gs[:6, 0])
    ax_merging_2 = fig.add_subplot(gs[6:, 0])

    axes_merging.append(ax_merging_1)
    axes_merging.append(ax_merging_2)

    # Set up the axes for the total dataframes
    axes_all = []
    ax_all_1 = fig.add_subplot(gs[:6, 1])
    ax_all_2 = fig.add_subplot(gs[6:, 1])

    axes_all.append(ax_all_1)
    axes_all.append(ax_all_2)

    # Create all the dataframes
    total_merging_df = ppisn_on_df_merging
    no_ppisn_merging_df = ppisn_on_df_merging[
        (ppisn_on_df_merging["undergone_ppisn_1"] == 0)
        & (ppisn_on_df_merging["undergone_ppisn_2"] == 0)
    ]
    any_ppisn_merging_df = ppisn_on_df_merging[
        (ppisn_on_df_merging["undergone_ppisn_1"] == 1)
        | (ppisn_on_df_merging["undergone_ppisn_2"] == 1)
    ]

    total_all_df = ppisn_on_df_all
    no_ppisn_all_df = ppisn_on_df_all[
        (ppisn_on_df_all["undergone_ppisn_1"] == 0)
        & (ppisn_on_df_all["undergone_ppisn_2"] == 0)
    ]
    any_ppisn_all_df = ppisn_on_df_all[
        (ppisn_on_df_all["undergone_ppisn_1"] == 1)
        | (ppisn_on_df_all["undergone_ppisn_2"] == 1)
    ]

    #############################################
    # Plotting
    total_merging = axes_merging[0].hist(
        total_merging_df["chirpmass"],
        weights=total_merging_df["number_per_solar_mass"],
        bins=bins,
        alpha=1,
        histtype="step",
        color="black",
        label="All systems",
    )

    no_ppisn_merging = axes_merging[0].hist(
        no_ppisn_merging_df["chirpmass"],
        weights=no_ppisn_merging_df["number_per_solar_mass"],
        bins=bins,
        alpha=0.25,
        label="No PPISN",
    )
    axes_merging[0].hist(
        no_ppisn_merging_df["chirpmass"],
        weights=no_ppisn_merging_df["number_per_solar_mass"],
        bins=bins,
        linewidth=2,
        histtype="step",
        color=no_ppisn_merging[-1][-1].get_facecolor(),
        alpha=1,
        linestyle="-.",
    )

    any_ppisn_merging = axes_merging[0].hist(
        any_ppisn_merging_df["chirpmass"],
        weights=any_ppisn_merging_df["number_per_solar_mass"],
        bins=bins,
        linewidth=2,
        alpha=0.5,
        label="Any PPISN",
    )
    axes_merging[0].hist(
        any_ppisn_merging_df["chirpmass"],
        weights=any_ppisn_merging_df["number_per_solar_mass"],
        bins=bins,
        histtype="step",
        color=any_ppisn_merging[-1][-1].get_facecolor(),
        alpha=1,
        linewidth=2,
        linestyle="dotted",
    )

    axes_merging[0].set_yscale("log")
    axes_merging[0].set_xlim(0, xlim_max)
    axes_merging[0].legend(loc=2, fontsize=12, framealpha=0.5)
    axes_merging[0].set_ylabel("Yield (number per formed solar mass)")

    # Fraction of total in that bin
    totals_merging = total_merging[0]

    fraction_no_ppisn_merging = np.divide(
        no_ppisn_merging[0],
        totals_merging,
        out=np.zeros_like(no_ppisn_merging[0]),
        where=totals_merging != 0,
    )
    fraction_any_ppisn_merging = np.divide(
        any_ppisn_merging[0],
        totals_merging,
        out=np.zeros_like(any_ppisn_merging[0]),
        where=totals_merging != 0,
    )

    added_fractions_merging = fraction_no_ppisn_merging + fraction_any_ppisn_merging
    filtered_added_fractions_merging = added_fractions_merging[
        np.isfinite(added_fractions_merging)
    ]

    # assert np.all(filtered_added_fractions == filtered_added_fractions[0])
    masses_merging = (no_ppisn_merging[1][1:] + no_ppisn_merging[1][:-1]) / 2

    axes_merging[1].plot(
        masses_merging, fraction_no_ppisn_merging, label="Fraction No PPISN"
    )
    axes_merging[1].plot(
        masses_merging, fraction_any_ppisn_merging, label="Fraction any PPISN"
    )
    axes_merging[1].plot(masses_merging, added_fractions_merging, alpha=0.2)
    axes_merging[1].legend(loc=2, fontsize=12, framealpha=0.5)
    axes_merging[1].set_ylabel("Fraction", fontsize=16)

    # Set some extra makeup
    axes_merging[-1].set_xlabel("Chirpmass")

    axes_merging[0].set_xticklabels([])
    axes_merging[1].set_xticklabels([])
    axes_merging[1].set_xlim(0, xlim_max)

    axes_merging[0].set_title("Merging systems")

    #
    total_all = axes_all[0].hist(
        total_all_df["chirpmass"],
        weights=total_all_df["number_per_solar_mass"],
        bins=bins,
        alpha=1,
        histtype="step",
        color="black",
        label="All systems",
    )

    #
    no_ppisn_all = axes_all[0].hist(
        no_ppisn_all_df["chirpmass"],
        weights=no_ppisn_all_df["number_per_solar_mass"],
        bins=bins,
        alpha=0.2,
        label="No PPISN",
    )
    axes_all[0].hist(
        no_ppisn_all_df["chirpmass"],
        weights=no_ppisn_all_df["number_per_solar_mass"],
        bins=bins,
        linewidth=2,
        histtype="step",
        color=no_ppisn_all[-1][-1].get_facecolor(),
        alpha=1,
        linestyle="-.",
    )

    any_ppisn_all = axes_all[0].hist(
        any_ppisn_all_df["chirpmass"],
        weights=any_ppisn_all_df["number_per_solar_mass"],
        bins=bins,
        alpha=0.5,
        label="Any PPISN",
    )
    axes_all[0].hist(
        any_ppisn_all_df["chirpmass"],
        weights=any_ppisn_all_df["number_per_solar_mass"],
        bins=bins,
        histtype="step",
        color=any_ppisn_all[-1][-1].get_facecolor(),
        alpha=1,
        linewidth=2,
        linestyle="dotted",
    )

    #
    axes_all[0].set_yscale("log")
    axes_all[0].set_xlim(0, xlim_max)
    axes_all[0].legend(loc=2, fontsize=12, framealpha=0.5)
    axes_all[0].set_ylabel("Yield (number per formed solar mass)")

    # Fraction of total in that bin
    totals_all = total_all[0]
    fraction_no_ppisn_all = np.divide(
        no_ppisn_all[0],
        totals_all,
        out=np.zeros_like(no_ppisn_all[0]),
        where=totals_all != 0,
    )
    fraction_any_ppisn_all = np.divide(
        any_ppisn_all[0],
        totals_all,
        out=np.zeros_like(any_ppisn_all[0]),
        where=totals_all != 0,
    )

    added_fractions_all = fraction_no_ppisn_all + fraction_any_ppisn_all
    filtered_added_fractions_all = added_fractions_all[np.isfinite(added_fractions_all)]

    # assert np.all(filtered_added_fractions == filtered_added_fractions[0])
    masses_all = (no_ppisn_all[1][1:] + no_ppisn_all[1][:-1]) / 2

    axes_all[1].plot(masses_all, fraction_no_ppisn_all, label="Fraction No PPISN")
    axes_all[1].plot(masses_all, fraction_any_ppisn_all, label="Fraction any PPISN")
    axes_all[1].plot(masses_all, added_fractions_all, alpha=0.2)
    axes_all[1].legend(loc=2, fontsize=12, framealpha=0.5)
    axes_all[1].set_ylabel("Fraction", fontsize=16)

    # Set some extra makeup
    axes_all[-1].set_xlabel("Chirpmass")

    axes_all[0].set_xticklabels([])
    axes_all[1].set_xticklabels([])
    axes_all[1].set_xlim(0, xlim_max)

    axes_all[0].set_title("All systems")

    axes_all[0].set_title("Distribution of PPISN vs no PPISN and fractions of total")

    ##################################
    # Finalising

    # Check if the dataframes actually add up to the first
    assert sorted(ppisn_on_df_all.index.values.tolist()) == sorted(
        no_ppisn_all_df.index.values.tolist() + any_ppisn_all_df.index.values.tolist()
    )
    assert sorted(ppisn_on_df_merging.index.values.tolist()) == sorted(
        no_ppisn_merging_df.index.values.tolist()
        + any_ppisn_merging_df.index.values.tolist()
    )

    # General limits:
    # Change ylim of both plots to the max range of both
    ylim_merging = axes_merging[0].get_ylim()
    ylim_all = axes_all[0].get_ylim()

    new_ylim = (min(ylim_merging[0], ylim_all[0]), max(ylim_merging[1], ylim_all[1]))

    axes_merging[0].set_ylim(new_ylim)
    axes_all[0].set_ylim(new_ylim)

    # # RED
    # plt.suptitle("Chirpmass distribution and its parts for {} {}".format(
    #         "Z={:.4g}".format(dict_ppisn_on['metallicity']) if not display_log_string else "Log10 Z={:.4g}".format(np.log10(dict_ppisn_on['metallicity'])),
    #         'bhbh'
    #     )
    # )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def marchant_plot(
    main_result_root,
    ppisn_on_name,
    ppisn_off_name,
    keyname,
    convolution_settings,
    display_log_string=False,
    limit_merging_time=True,
    plot_settings=None,
):
    """
    Function to plot the plot of marchant 19. Putting the merging and total population next to
    eachother.
    """

    if convolution_settings.get("global_query", None):
        plot_settings["runname"] = plot_settings.get(
            "runname", ""
        ) + " (query: {})".format(convolution_settings.get("global_query"))

    if not plot_settings:
        plot_settings = {}

    # Limit and bins
    xlim_max = 50
    nbins = resolution_settings["marchant_plot"]["nbins"]
    bins = np.linspace(0, 101, nbins)

    # Load the dictionaries
    info_dict_ppisn_on = load_info_dict(
        os.path.join(main_result_root, ppisn_on_name), rebuild=False
    )
    info_dict_ppisn_off = load_info_dict(
        os.path.join(main_result_root, ppisn_off_name), rebuild=False
    )

    dict_ppisn_on = info_dict_ppisn_on[keyname]
    dict_ppisn_off = info_dict_ppisn_off[keyname]

    # Load the dataframes
    ppisn_on_df_merging = make_df(
        dict_ppisn_on["filename"],
        dict_ppisn_on["metallicity"],
        probability_to_number_to_solarmass_factor=convolution_settings["mass_in_stars"],
        binary_fraction_factor=convolution_settings["binary_fraction"],
        limit_merging_time=True,
    )
    ppisn_on_df_all = make_df(
        dict_ppisn_on["filename"],
        dict_ppisn_on["metallicity"],
        probability_to_number_to_solarmass_factor=convolution_settings["mass_in_stars"],
        binary_fraction_factor=convolution_settings["binary_fraction"],
        limit_merging_time=False,
    )

    ppisn_off_df_merging = make_df(
        dict_ppisn_off["filename"],
        dict_ppisn_off["metallicity"],
        probability_to_number_to_solarmass_factor=convolution_settings["mass_in_stars"],
        binary_fraction_factor=convolution_settings["binary_fraction"],
        limit_merging_time=True,
    )
    ppisn_off_df_all = make_df(
        dict_ppisn_off["filename"],
        dict_ppisn_off["metallicity"],
        probability_to_number_to_solarmass_factor=convolution_settings["mass_in_stars"],
        binary_fraction_factor=convolution_settings["binary_fraction"],
        limit_merging_time=False,
    )

    # Do query if its passed
    if convolution_settings.get("global_query", None):
        ppisn_on_df_merging = ppisn_on_df_merging.query(
            convolution_settings.get("global_query")
        )
        ppisn_on_df_all = ppisn_on_df_all.query(
            convolution_settings.get("global_query")
        )
        ppisn_off_df_merging = ppisn_off_df_merging.query(
            convolution_settings.get("global_query")
        )
        ppisn_off_df_all = ppisn_off_df_all.query(
            convolution_settings.get("global_query")
        )

    # Check the dataframes on PPISN_NONE:
    ppisn_on_df_merging = filter_ppisn_type(
        ppisn_on_df_merging, dict_ppisn_on, ppisn_name=ppisn_on_name, ppisn_type=5
    )
    ppisn_on_df_all = filter_ppisn_type(
        ppisn_on_df_all, dict_ppisn_on, ppisn_name=ppisn_on_name, ppisn_type=5
    )

    ppisn_off_df_merging = filter_ppisn_type(
        ppisn_off_df_merging, dict_ppisn_off, ppisn_name=ppisn_off_name, ppisn_type=5
    )
    ppisn_off_df_all = filter_ppisn_type(
        ppisn_off_df_all, dict_ppisn_off, ppisn_name=ppisn_off_name, ppisn_type=5
    )

    #####################
    # same for PHDIS
    ppisn_on_df_merging = filter_ppisn_type(
        ppisn_on_df_merging, dict_ppisn_on, ppisn_name=ppisn_on_name, ppisn_type=4
    )
    ppisn_on_df_all = filter_ppisn_type(
        ppisn_on_df_all, dict_ppisn_on, ppisn_name=ppisn_on_name, ppisn_type=4
    )

    ppisn_off_df_merging = filter_ppisn_type(
        ppisn_off_df_merging, dict_ppisn_off, ppisn_name=ppisn_off_name, ppisn_type=4
    )
    ppisn_off_df_all = filter_ppisn_type(
        ppisn_off_df_all, dict_ppisn_off, ppisn_name=ppisn_off_name, ppisn_type=4
    )

    # Create subplots
    fig = plt.figure(constrained_layout=False, figsize=(20, 20))
    gs = fig.add_gridspec(8, 2)

    # Set up the axes for the merging dataframes
    axes_merging = []
    ax_merging_1 = fig.add_subplot(gs[:6, 0])
    ax_merging_2 = fig.add_subplot(gs[6:7, 0])
    ax_merging_3 = fig.add_subplot(gs[7:8, 0])

    axes_merging.append(ax_merging_1)
    axes_merging.append(ax_merging_2)
    axes_merging.append(ax_merging_3)

    # Set up the axes for the total dataframes
    axes_all = []
    ax_all_1 = fig.add_subplot(gs[:6, 1])
    ax_all_2 = fig.add_subplot(gs[6:7, 1])
    ax_all_3 = fig.add_subplot(gs[7:8, 1])

    axes_all.append(ax_all_1)
    axes_all.append(ax_all_2)
    axes_all.append(ax_all_3)

    #############################################
    # Plotting

    # Plotting the merging pop
    with_ppisne_merging = axes_merging[0].hist(
        ppisn_on_df_merging["chirpmass"],
        weights=ppisn_on_df_merging["number_per_solar_mass"],
        bins=bins,
        histtype="step",
        linewidth=4,
        label="With PPISNe",
    )
    without_ppisne_merging = axes_merging[0].hist(
        ppisn_off_df_merging["chirpmass"],
        weights=ppisn_off_df_merging["number_per_solar_mass"],
        bins=bins,
        histtype="step",
        linestyle="--",
        linewidth=4,
        label="Without PPISNe",
    )

    no_ppisn_merging_df = ppisn_on_df_merging[
        (ppisn_on_df_merging["undergone_ppisn_1"] == 0)
        & (ppisn_on_df_merging["undergone_ppisn_2"] == 0)
    ]
    no_ppisn_merging = axes_merging[0].hist(
        no_ppisn_merging_df["chirpmass"],
        weights=no_ppisn_merging_df["number_per_solar_mass"],
        bins=bins,
        alpha=0.25,
        label="No PPISN",
    )
    axes_merging[0].hist(
        no_ppisn_merging_df["chirpmass"],
        weights=no_ppisn_merging_df["number_per_solar_mass"],
        bins=bins,
        linewidth=2,
        histtype="step",
        color=no_ppisn_merging[-1].patches[-1].get_facecolor(),
        alpha=1,
        linestyle="-.",
    )

    single_ppisn_merging_df = ppisn_on_df_merging[
        (ppisn_on_df_merging["undergone_ppisn_1"] == 1)
        ^ (ppisn_on_df_merging["undergone_ppisn_2"] == 1)
    ]
    single_ppisn_merging = axes_merging[0].hist(
        single_ppisn_merging_df["chirpmass"],
        weights=single_ppisn_merging_df["number_per_solar_mass"],
        bins=bins,
        linewidth=2,
        alpha=0.5,
        label="Single PPISN",
    )
    axes_merging[0].hist(
        single_ppisn_merging_df["chirpmass"],
        weights=single_ppisn_merging_df["number_per_solar_mass"],
        bins=bins,
        histtype="step",
        color=single_ppisn_merging[-1].patches[-1].get_facecolor(),
        alpha=1,
        linewidth=2,
        linestyle="dotted",
    )

    double_ppisn_merging_df = ppisn_on_df_merging[
        (ppisn_on_df_merging["undergone_ppisn_1"] == 1)
        & (ppisn_on_df_merging["undergone_ppisn_2"] == 1)
    ]
    double_ppisn_merging = axes_merging[0].hist(
        double_ppisn_merging_df["chirpmass"],
        weights=double_ppisn_merging_df["number_per_solar_mass"],
        bins=bins,
        linewidth=2,
        alpha=0.5,
        label="Double PPISN",
    )
    axes_merging[0].hist(
        double_ppisn_merging_df["chirpmass"],
        weights=double_ppisn_merging_df["number_per_solar_mass"],
        bins=bins,
        histtype="step",
        color=double_ppisn_merging[-1].patches[-1].get_facecolor(),
        alpha=1,
        linewidth=2,
        linestyle="-",
    )

    axes_merging[0].set_yscale("log")
    axes_merging[0].set_xlim(0, xlim_max)
    axes_merging[0].legend(loc=2, fontsize=12, framealpha=0.5)
    axes_merging[0].set_ylabel("Yield (number per formed solar mass)")

    # Fraction of total in that bin
    totals_merging = with_ppisne_merging[0]
    fraction_no_ppisn_merging = np.divide(
        no_ppisn_merging[0],
        totals_merging,
        out=np.zeros_like(no_ppisn_merging[0]),
        where=totals_merging != 0,
    )
    fraction_single_ppisn_merging = np.divide(
        single_ppisn_merging[0],
        totals_merging,
        out=np.zeros_like(single_ppisn_merging[0]),
        where=totals_merging != 0,
    )
    fraction_double_ppisn_merging = np.divide(
        double_ppisn_merging[0],
        totals_merging,
        out=np.zeros_like(double_ppisn_merging[0]),
        where=totals_merging != 0,
    )
    added_fractions_merging = (
        fraction_no_ppisn_merging
        + fraction_single_ppisn_merging
        + fraction_double_ppisn_merging
    )
    filtered_added_fractions_merging = added_fractions_merging[
        np.isfinite(added_fractions_merging)
    ]

    # assert np.all(filtered_added_fractions == filtered_added_fractions[0])
    masses_merging = (no_ppisn_merging[1][1:] + no_ppisn_merging[1][:-1]) / 2

    axes_merging[1].plot(
        masses_merging, fraction_no_ppisn_merging, label="Fraction No PPISN"
    )
    axes_merging[1].plot(
        masses_merging, fraction_single_ppisn_merging, label="Fraction Single PPISN"
    )
    axes_merging[1].plot(
        masses_merging, fraction_double_ppisn_merging, label="Fraction Double PPISN"
    )
    axes_merging[1].plot(masses_merging, added_fractions_merging, alpha=0.2)
    axes_merging[1].legend(loc=2, fontsize=12, framealpha=0.5)
    axes_merging[1].set_ylabel("Fraction", fontsize=16)

    # Ratio of with and without ppisne
    axes_merging[2].plot(
        masses_merging,
        np.divide(
            with_ppisne_merging[0],
            without_ppisne_merging[0],
            out=np.zeros_like(with_ppisne_merging[0]),
            where=without_ppisne_merging[0] != 0,
        ),
        label="Ratio with_ppisne/without_ppisne",
    )
    axes_merging[2].plot(
        masses_merging, [1 for el in masses_merging], linestyle="--", alpha=0.5
    )
    axes_merging[2].set_ylabel("Ratio", fontsize=16)

    # Set some extra makeup
    axes_merging[-1].set_xlabel("Chirpmass")

    axes_merging[0].set_xticklabels([])
    axes_merging[1].set_xticklabels([])
    axes_merging[1].set_xlim(0, xlim_max)
    axes_merging[2].set_xlim(0, xlim_max)

    # Plotting the total pop
    with_ppisne_all = axes_all[0].hist(
        ppisn_on_df_all["chirpmass"],
        weights=ppisn_on_df_all["number_per_solar_mass"],
        bins=bins,
        histtype="step",
        linewidth=4,
        label="With PPISNe",
    )
    without_ppisne_all = axes_all[0].hist(
        ppisn_off_df_all["chirpmass"],
        weights=ppisn_off_df_all["number_per_solar_mass"],
        bins=bins,
        histtype="step",
        linestyle="--",
        linewidth=4,
        label="Without PPISNe",
    )

    no_ppisn_all_df = ppisn_on_df_all[
        (ppisn_on_df_all["undergone_ppisn_1"] == 0)
        & (ppisn_on_df_all["undergone_ppisn_2"] == 0)
    ]
    no_ppisn_all = axes_all[0].hist(
        no_ppisn_all_df["chirpmass"],
        weights=no_ppisn_all_df["number_per_solar_mass"],
        bins=bins,
        alpha=0.2,
        label="No PPISN",
    )
    axes_all[0].hist(
        no_ppisn_all_df["chirpmass"],
        weights=no_ppisn_all_df["number_per_solar_mass"],
        bins=bins,
        linewidth=2,
        histtype="step",
        color=no_ppisn_all[-1].patches[-1].get_facecolor(),
        alpha=1,
        linestyle="-.",
    )

    single_ppisn_all_df = ppisn_on_df_all[
        (ppisn_on_df_all["undergone_ppisn_1"] == 1)
        ^ (ppisn_on_df_all["undergone_ppisn_2"] == 1)
    ]
    single_ppisn_all = axes_all[0].hist(
        single_ppisn_all_df["chirpmass"],
        weights=single_ppisn_all_df["number_per_solar_mass"],
        bins=bins,
        alpha=0.5,
        label="Single PPISN",
    )
    axes_all[0].hist(
        single_ppisn_all_df["chirpmass"],
        weights=single_ppisn_all_df["number_per_solar_mass"],
        bins=bins,
        histtype="step",
        color=single_ppisn_all[-1].patches[-1].get_facecolor(),
        alpha=1,
        linewidth=2,
        linestyle="dotted",
    )

    double_ppisn_all_df = ppisn_on_df_all[
        (ppisn_on_df_all["undergone_ppisn_1"] == 1)
        & (ppisn_on_df_all["undergone_ppisn_2"] == 1)
    ]
    double_ppisn_all = axes_all[0].hist(
        double_ppisn_all_df["chirpmass"],
        weights=double_ppisn_all_df["number_per_solar_mass"],
        bins=bins,
        alpha=0.5,
        label="Double PPISN",
    )
    axes_all[0].hist(
        double_ppisn_all_df["chirpmass"],
        weights=double_ppisn_all_df["number_per_solar_mass"],
        bins=bins,
        histtype="step",
        color=double_ppisn_all[-1].patches[-1].get_facecolor(),
        alpha=1,
        linewidth=2,
        linestyle="-",
    )

    #
    axes_all[0].set_yscale("log")
    axes_all[0].set_xlim(0, xlim_max)
    axes_all[0].legend(loc=2, fontsize=12, framealpha=0.5)
    axes_all[0].set_ylabel("Yield (number per formed solar mass)")

    # Fraction of total in that bin
    totals_all = with_ppisne_all[0]
    fraction_no_ppisn_all = np.divide(
        no_ppisn_all[0],
        totals_all,
        out=np.zeros_like(no_ppisn_all[0]),
        where=totals_all != 0,
    )
    fraction_single_ppisn_all = np.divide(
        single_ppisn_all[0],
        totals_all,
        out=np.zeros_like(single_ppisn_all[0]),
        where=totals_all != 0,
    )
    fraction_double_ppisn_all = np.divide(
        double_ppisn_all[0],
        totals_all,
        out=np.zeros_like(double_ppisn_all[0]),
        where=totals_all != 0,
    )
    added_fractions_all = (
        fraction_no_ppisn_all + fraction_single_ppisn_all + fraction_double_ppisn_all
    )
    filtered_added_fractions_all = added_fractions_all[np.isfinite(added_fractions_all)]

    # assert np.all(filtered_added_fractions == filtered_added_fractions[0])
    masses_all = (no_ppisn_all[1][1:] + no_ppisn_all[1][:-1]) / 2

    axes_all[1].plot(masses_all, fraction_no_ppisn_all, label="Fraction No PPISN")
    axes_all[1].plot(
        masses_all, fraction_single_ppisn_all, label="Fraction Single PPISN"
    )
    axes_all[1].plot(
        masses_all, fraction_double_ppisn_all, label="Fraction Double PPISN"
    )
    axes_all[1].plot(masses_all, added_fractions_all, alpha=0.2)
    axes_all[1].legend(loc=2, fontsize=12, framealpha=0.5)
    axes_all[1].set_ylabel("Fraction", fontsize=16)

    # Ratio of with and without ppisne
    axes_all[2].plot(
        masses_all,
        np.divide(
            with_ppisne_all[0],
            without_ppisne_all[0],
            out=np.zeros_like(with_ppisne_all[0]),
            where=without_ppisne_all[0] != 0,
        ),
        label="Ratio with_ppisne/without_ppisne",
    )
    axes_all[2].plot(masses_all, [1 for el in masses_all], linestyle="--", alpha=0.5)
    axes_all[2].set_ylabel("Ratio", fontsize=16)

    # Set some extra makeup
    axes_all[-1].set_xlabel("Chirpmass")

    axes_all[0].set_xticklabels([])
    axes_all[1].set_xticklabels([])
    axes_all[1].set_xlim(0, xlim_max)
    axes_all[2].set_xlim(0, xlim_max)

    ##################################
    # Finalising
    # from grav_waves.gw_analysis.functions.functions import get_cmdline_arg_single_system
    # print(ppisn_on_df_all['undergone_ppisn_1'].unique())
    # print(ppisn_on_df_all['undergone_ppisn_2'].unique())
    # print(ppisn_on_df_all[ppisn_on_df_all.undergone_ppisn_1==5].iloc[0])
    # cmdline= get_cmdline_arg_single_system(dict_ppisn_on, ppisn_on_df_all[ppisn_on_df_all.undergone_ppisn_1==5].iloc[0])
    # print(cmdline)
    # quit()

    # import json
    # with open("/vol/ph/astro_code/dhendriks/astro_codes/binaryc/binary_c_python_scripts/grav_waves/gw_analysis/tmp_plot_functions/failing_system.json", 'w') as f:
    #      f.write(json.dumps(ppisn_on_df_all[ppisn_on_df_all.undergone_ppisn_1==5].iloc[0].to_dict()))

    # Check if the dataframes actually add up to the first
    assert sorted(ppisn_on_df_all.index.values.tolist()) == sorted(
        no_ppisn_all_df.index.values.tolist()
        + single_ppisn_all_df.index.values.tolist()
        + double_ppisn_all_df.index.values.tolist()
    )
    assert sorted(ppisn_on_df_merging.index.values.tolist()) == sorted(
        no_ppisn_merging_df.index.values.tolist()
        + single_ppisn_merging_df.index.values.tolist()
        + double_ppisn_merging_df.index.values.tolist()
    )

    # General limits:
    # Change ylim of both plots to the max range of both
    ylim_merging = axes_merging[0].get_ylim()
    ylim_all = axes_all[0].get_ylim()

    new_ylim = (min(ylim_merging[0], ylim_all[0]), max(ylim_merging[1], ylim_all[1]))
    axes_merging[0].set_ylim(new_ylim)
    axes_all[0].set_ylim(new_ylim)

    # RED
    axes_all[0].set_title(
        "Chirpmass distribution and its parts for {} {}".format(
            "Z={:.4g}".format(dict_ppisn_on["metallicity"])
            if not display_log_string
            else "Log10 Z={:.4g}".format(np.log10(dict_ppisn_on["metallicity"])),
            "bhbh",
        )
    )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


# Routines for times for single dataset
def plot_inspiral_time(dataset_dict, convolution_configuration, plot_settings=None):
    """
    Function to plot the inspiral time distribution
    """
    if not plot_settings:
        plot_settings = {}

    df = make_df(
        dataset_dict["filename"],
        probability_to_number_to_solarmass_factor=convolution_configuration[
            "mass_in_stars"
        ],
        binary_fraction_factor=convolution_configuration["binary_fraction"],
    )

    fig = plt.figure(figsize=(16, 16))

    # Merging time
    plt.title(
        "{} merger time distribution for systems merging in {:.2e} yr".format(
            dataset_dict["type"], AGE_UNIVERSE_IN_YEAR
        )
    )
    bins = 10.0 ** np.arange(0, 5, 0.5)

    plt.hist(
        df["inspiral_time_values_in_years"] / 1e6,
        bins=bins,
        weights=df["number_per_solar_mass"],
    )
    plt.yscale("log")
    plt.xscale("log")
    plt.ylabel(r"Yield (number per formed solar mass)")
    plt.xlabel(r"Merger time [Myr]")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_delay_time(dataset_dict, convolution_configuration, plot_settings=None):
    """
    Function to plot the delay time distribution of the dataset
    """

    if not plot_settings:
        plot_settings = {}

    df = make_df(
        dataset_dict["filename"],
        probability_to_number_to_solarmass_factor=convolution_configuration[
            "mass_in_stars"
        ],
        binary_fraction_factor=convolution_configuration["binary_fraction"],
    )

    # Delay time distribution
    plt.title(
        "BHBH Delay time distribution for systems merging in {:.2e} yr".format(
            AGE_UNIVERSE_IN_YEAR
        )
    )
    plt.hist(df["time"], bins=20, weights=df["number_per_solar_mass"], density=True)
    plt.yscale("log")
    plt.ylabel(r"Yield (number per formed solar mass)")
    plt.xlabel(r"Formation time [Myr]")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_merging_time(
    dataset_dict, convolution_configuration, plot_settings=None, df_kwargs=None
):
    """
    Function to plot the merging time
    """

    if not plot_settings:
        plot_settings = {}

    if not df_kwargs:
        df_kwargs = {}

    df = make_df(
        dataset_dict["filename"],
        probability_to_number_to_solarmass_factor=convolution_configuration[
            "mass_in_stars"
        ],
        binary_fraction_factor=convolution_configuration["binary_fraction"],
        **df_kwargs,
    )

    # Merging time
    plt.title(
        "BHBH merger time distribution for systems merging in {:.2e} yr".format(
            AGE_UNIVERSE_IN_YEAR
        )
    )
    bins = 10.0 ** np.arange(1, 7, 0.5)
    bins = 10.0 ** np.arange(7, 10, 0.2)

    plt.hist(
        df["merger_time_values_in_years"],
        bins=bins,
        weights=df["number_per_solar_mass"],
        histtype="step",
        linewidth=8,
    )
    plt.yscale("log")
    plt.xscale("log")
    plt.ylabel(r"Yield (number per formed solar mass)")
    plt.xlabel(r"Merger time (Time after starburst) [Myr]")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_timescales_plot(
    merger_dict,
    type_key,
    convolution_settings,
    display_log_string=False,
    plot_settings=None,
):
    """
    Plot routine to plot all the black holes
    """

    if convolution_settings.get("global_query", None):
        plot_settings["runname"] = plot_settings.get(
            "runname", ""
        ) + " (query: {})".format(convolution_settings.get("global_query"))

    if not plot_settings:
        plot_settings = {}

    # Set up canvas and grid
    fig = plt.figure(constrained_layout=False)
    gs = fig.add_gridspec(
        6,
        4,
    )
    ax1 = fig.add_subplot(gs[:2, :])
    ax2 = fig.add_subplot(gs[2:4, :])
    ax3 = fig.add_subplot(gs[4:, :])

    if type_key == "bhbh":
        bins = resolution_settings["plot_timescales_plot"]["bhbh"]
    elif type_key == "bhns":
        bins = resolution_settings["plot_timescales_plot"]["bhns"]
    elif type_key == "nsns":
        bins = resolution_settings["plot_timescales_plot"]["nsns"]
    else:
        print("Key unknown")
        raise ValueError

    # Sort metallicity keys
    sorted_metallicity_keys = sorted(
        merger_dict.keys(),
        key=lambda metallicity: float(metallicity.split("_")[0][1:]),
        reverse=True,
    )
    sorted_metallicity_keys_type = [
        el for el in sorted_metallicity_keys if el.endswith("_{}".format(type_key))
    ]

    # loop over all the metallicities
    for el in sorted_metallicity_keys_type:
        info_dict = merger_dict[el]

        all_df = make_df(
            info_dict["filename"],
            info_dict["metallicity"],
            limit_merging_time=True,
            probability_to_number_to_solarmass_factor=convolution_settings[
                "mass_in_stars"
            ],
            binary_fraction_factor=convolution_settings["binary_fraction"],
        )

        # Do query if its passed
        if convolution_settings.get("global_query", None):
            all_df = all_df.query(convolution_settings.get("global_query"))

        formation_time = ax1.hist(
            all_df["formation_time_values_in_years"] / 1e6,
            bins=bins,
            weights=all_df["number_per_solar_mass"],
            histtype="step",
            linewidth=2,
            label="{}".format(
                "Z={:.4g}".format(info_dict["metallicity"])
                if not display_log_string
                else "Log10 Z={:.4g}".format(np.log10(info_dict["metallicity"])),
            ),
        )

        #
        inspiral_time = ax2.hist(
            all_df["inspiral_time_values_in_years"] / 1e6,
            bins=bins,
            weights=all_df["number_per_solar_mass"],
            histtype="step",
            linewidth=2,
            label="{}".format(
                "Z={:.4g}".format(info_dict["metallicity"])
                if not display_log_string
                else "Log10 Z={:.4g}".format(np.log10(info_dict["metallicity"])),
            ),
        )

        #
        merger_time = ax3.hist(
            all_df["merger_time_values_in_years"] / 1e6,
            bins=bins,
            weights=all_df["number_per_solar_mass"],
            histtype="step",
            linewidth=2,
            label="{}".format(
                "Z={:.4g}".format(info_dict["metallicity"])
                if not display_log_string
                else "Log10 Z={:.4g}".format(np.log10(info_dict["metallicity"])),
            ),
        )

    #
    ax1.legend(fontsize=10, framealpha=0.5, ncol=2)

    # Scaling
    ax1.set_yscale("log")
    ax1.set_xscale("log")

    # Scaling
    ax2.set_yscale("log")
    ax2.set_xscale("log")

    # Scaling
    ax3.set_yscale("log")
    ax3.set_xscale("log")

    # Set y-scale
    min_y = np.min(
        [np.min(ax1.get_ylim()), np.min(ax2.get_ylim()), np.min(ax3.get_ylim())]
    )
    max_y = np.max(
        [np.max(ax1.get_ylim()), np.max(ax2.get_ylim()), np.max(ax3.get_ylim())]
    )

    ax1.set_ylim([min_y, max_y])
    ax2.set_ylim([min_y, max_y])
    ax3.set_ylim([min_y, max_y])

    # Some makeup
    ax3.set_xlabel(r"Time [Myr]")

    #
    ax1.set_title("Formation time distribution", fontsize=16)
    ax2.set_title("Inspiral time distribution", fontsize=16)
    ax3.set_title("Merger time (formation + inspiral) distribution", fontsize=16)

    ax1.set_ylabel("Yield (number per formed solar mass)", fontsize=16)
    ax2.set_ylabel("Yield (number per formed solar mass)", fontsize=16)
    ax3.set_ylabel("Yield (number per formed solar mass)", fontsize=16)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


## Massratio triangles
# Single dataset
def plot_massratio_triangle(info_dict, convolution_settings, plot_settings=None):
    """
    Function to plot the mass ratio max min triangle
    """

    if not plot_settings:
        plot_settings = {}

    merging_df_all = make_df(
        info_dict["filename"],
        info_dict["metallicity"],
        probability_to_number_to_solarmass_factor=convolution_settings["mass_in_stars"],
        binary_fraction_factor=convolution_settings["binary_fraction"],
        limit_merging_time=False,
    )
    merging_df_all["biggest_mass"] = merging_df_all[["mass_1", "mass_2"]].max(axis=1)
    merging_df_all["smallest_mass"] = merging_df_all[["mass_1", "mass_2"]].min(axis=1)

    merging_df_merging = merging_df_all[
        merging_df_all.merger_time_values_in_years < AGE_UNIVERSE_IN_YEAR
    ]

    # Figure
    fig, axes = plt.subplots(nrows=1, ncols=2, sharey=True)

    bins_step = 2.5
    bins_x = np.arange(0, 100 + bins_step, bins_step)
    bins_y = bins_x

    axes[0].hist2d(
        merging_df_all["biggest_mass"],
        merging_df_all["smallest_mass"],
        bins=[bins_x, bins_y],
        weights=merging_df_all["number_per_solar_mass"],
        norm=matplotlib.colors.LogNorm(),
    )
    axes[0].plot([0, 100], [0, 100])

    axes[1].hist2d(
        merging_df_merging["biggest_mass"],
        merging_df_merging["smallest_mass"],
        bins=[bins_x, bins_y],
        weights=merging_df_merging["number_per_solar_mass"],
        norm=matplotlib.colors.LogNorm(),
    )
    axes[1].plot([0, 100], [0, 100])

    axes[0].set_aspect("equal")
    axes[1].set_aspect("equal")

    # Chirp mass
    axes[0].set_title(
        "Triangle plot for Z={}{} systems merging in {:.2e} yr".format(
            info_dict["metallicity"], info_dict["type"], AGE_UNIVERSE_IN_YEAR
        )
    )
    axes[0].set_ylabel(r"M2 [M$_{\odot}$]")
    axes[0].set_xlabel(r"M1 [M$_{\odot}$]")
    axes[1].set_xlabel(r"M1 [M$_{\odot}$]")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_combined_massratio_triangle(
    merger_info,
    type_key,
    convolution_settings,
    display_log_string=False,
    plot_settings=None,
):
    """
    Same as above, but then combined into 1 plot so they can share the colorbar
    """

    if convolution_settings.get("global_query", None):
        plot_settings["runname"] = plot_settings.get(
            "runname", ""
        ) + " (query: {})".format(convolution_settings.get("global_query"))

    if not plot_settings:
        plot_settings = {}

    #
    sorted_metallicity_keys = sorted(
        merger_info.keys(), key=lambda metallicity: float(metallicity.split("_")[0][1:])
    )[::-1]
    sorted_metallicity_keys_type = [
        el for el in sorted_metallicity_keys if el.endswith("_{}".format(type_key))
    ]
    amt_plots = len(sorted_metallicity_keys_type)

    #
    fig = plt.figure(constrained_layout=False, figsize=(12, 5 * amt_plots))
    spec = gridspec.GridSpec(ncols=5, nrows=4 * amt_plots, figure=fig, hspace=2)
    plot_axes = []

    #
    for i in range(amt_plots):
        lower = 4 * i
        upper = 4 * (i + 1)

        # Create axes
        new_ax_left = fig.add_subplot(spec[lower:upper, :2])
        new_ax_right = fig.add_subplot(spec[lower:upper, 2:4])

        # Add axes to list
        plot_axes.append([new_ax_left, new_ax_right])

    # Create colorbar axis
    colorbar_ax = fig.add_subplot(spec[:, 4])

    # Set dummy min and max
    min_prob = 1e20
    max_prob = -1

    if type_key == "bhbh":
        bins_x = resolution_settings["plot_combined_massratio_triangle"]["bhbh"]["x"]
        bins_y = resolution_settings["plot_combined_massratio_triangle"]["bhbh"]["y"]
    elif type_key == "bhns":
        bins_x = resolution_settings["plot_combined_massratio_triangle"]["bhns"]["x"]
        bins_y = resolution_settings["plot_combined_massratio_triangle"]["bhns"]["y"]
    elif type_key == "nsns":
        bins_x = resolution_settings["plot_combined_massratio_triangle"]["nsns"]["x"]
        bins_y = resolution_settings["plot_combined_massratio_triangle"]["nsns"]["y"]
    else:
        print("Key unknown")
        raise ValueError

    # Loop to get the numbers
    for i, sub_key in enumerate(sorted_metallicity_keys_type):

        # Select the population
        df_all = make_df(
            merger_info[sub_key]["filename"],
            probability_to_number_to_solarmass_factor=convolution_settings[
                "mass_in_stars"
            ],
            binary_fraction_factor=convolution_settings["binary_fraction"],
            limit_merging_time=False,
        )

        # Do query if its passed
        if convolution_settings.get("global_query", None):
            df_all = df_all.query(convolution_settings.get("global_query"))

        if df_all.empty:
            continue

        df_all["biggest_mass"] = df_all[["mass_1", "mass_2"]].max(axis=1)
        df_all["smallest_mass"] = df_all[["mass_1", "mass_2"]].min(axis=1)

        # Select the merging only
        df_merging = df_all[df_all.merger_time_values_in_years < AGE_UNIVERSE_IN_YEAR]
        if df_merging.empty:
            continue

        # Get probabilities
        h_all = np.histogram2d(
            df_all["biggest_mass"],
            df_all["smallest_mass"],
            bins=[bins_x, bins_y],
            weights=df_all["number_per_solar_mass"],
        )
        h_merging = np.histogram2d(
            df_merging["biggest_mass"],
            df_merging["smallest_mass"],
            bins=[bins_x, bins_y],
            weights=df_merging["number_per_solar_mass"],
        )

        # Get lowest value
        for histogram in [h_all, h_merging]:
            # Check for min and max
            ding = histogram[0]
            min_val = np.min(ding[ding != 0])
            max_val = np.max(ding[ding != 0])

            if min_val < min_prob:
                min_prob = min_val
            if max_val > max_prob:
                max_prob = max_val

    # Loop to make the plot
    for i, sub_key in enumerate(sorted_metallicity_keys_type):
        info_dict = merger_info[sub_key]

        # Select the population
        df_all = make_df(
            merger_info[sub_key]["filename"],
            probability_to_number_to_solarmass_factor=convolution_settings[
                "mass_in_stars"
            ],
            binary_fraction_factor=convolution_settings["binary_fraction"],
            limit_merging_time=False,
        )

        # Do query if its passed
        if convolution_settings.get("global_query", None):
            df_all = df_all.query(convolution_settings.get("global_query"))

        df_all["biggest_mass"] = df_all[["mass_1", "mass_2"]].max(axis=1)
        df_all["smallest_mass"] = df_all[["mass_1", "mass_2"]].min(axis=1)

        # Select the merging only
        df_merging = df_all[df_all.merger_time_values_in_years < AGE_UNIVERSE_IN_YEAR]

        # Get probabilities
        h_all = plot_axes[i][0].hist2d(
            df_all["biggest_mass"],
            df_all["smallest_mass"],
            bins=[bins_x, bins_y],
            weights=df_all["number_per_solar_mass"],
            norm=matplotlib.colors.LogNorm(vmin=min_prob, vmax=max_prob),
        )
        h_merging = plot_axes[i][1].hist2d(
            df_merging["biggest_mass"],
            df_merging["smallest_mass"],
            bins=[bins_x, bins_y],
            weights=df_merging["number_per_solar_mass"],
            norm=matplotlib.colors.LogNorm(vmin=min_prob, vmax=max_prob),
        )

        # Diagonals
        if type_key == "bhbh":
            plot_axes[i][0].plot([0, 100], [0, 100], "--", alpha=0.5)
            plot_axes[i][1].plot([0, 100], [0, 100], "--", alpha=0.5)

        # Set title
        plot_axes[i][0].set_title(
            "{} All".format(
                "Z={:.4g}".format(info_dict["metallicity"])
                if not display_log_string
                else "Log10 Z={:.4g}".format(np.log10(info_dict["metallicity"])),
            ),
            fontsize=12,
        )
        plot_axes[i][1].set_title(
            "{} Merging".format(
                "Z={:.4g}".format(info_dict["metallicity"])
                if not display_log_string
                else "Log10 Z={:.4g}".format(np.log10(info_dict["metallicity"])),
            ),
            fontsize=12,
        )

        # Remove xticks
        if not i == amt_plots - 1:
            plot_axes[i][0].set_xticklabels([])
            plot_axes[i][1].set_xticklabels([])
        plot_axes[i][1].set_yticklabels([])

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        colorbar_ax,
        cmap=matplotlib.cm.viridis,
        norm=matplotlib.colors.LogNorm(vmin=min_prob, vmax=max_prob),
    )
    cbar.ax.set_ylabel("Yield (number per formed solar mass)")

    #
    fig.text(
        0.04,
        0.5,
        r"Secondary mass [M$_{\odot}$]",
        va="center",
        ha="center",
        rotation="vertical",
    )

    #
    fig.text(0.5, 0.04, r"Primary mass [M$_{\odot}$]", va="center", ha="center")

    #
    fig.suptitle("Mass triangle plots for {}".format(type_key))

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


# Plot to show the rates of different types of SN
def plot_sn_rates(
    result_dir,
    convolution_settings,
    limit_merging_in_hubbletime=True,
    plot_settings=None,
):
    """
    TODO: figure out which systems lead to a PPISN_TYPE = -1 ..
    TODO: add extra quantities to show
    """

    #
    if not plot_settings:
        plot_settings = {}

    # Sort metallicity keys
    metallicity_dirs = [el for el in os.listdir(result_dir) if el.startswith("Z0.")]
    sorted_metallicity_dirs = sorted(
        metallicity_dirs,
        key=lambda metallicity: float(metallicity.split("_")[0][1:]),
        reverse=True,
    )

    #
    common_columns = [
        "merger_time_values_in_years",
        "metallicity",
        "number_per_solar_mass",
    ]
    primary_rename_dict = {
        "mass_1": "mass",
        "stellar_type_1": "stellar_type",
        "undergone_ppisn_1": "undergone_ppisn_type",
    }
    secondary_rename_dict = {
        "mass_2": "mass",
        "stellar_type_2": "stellar_type",
        "undergone_ppisn_2": "undergone_ppisn_type",
    }

    # Set up some lists
    metallicity_values = []
    individual_ccsne = []
    individual_ppisn = []
    individual_pisn = []
    individual_phdis = []
    both_ccsne = []
    both_ppisne = []
    both_pisne = []

    #
    for metallicity_dir in sorted_metallicity_dirs:
        metallicity = float(metallicity_dir[1:])
        metallicity_values.append(metallicity)

        # Load the whole dataframe
        all_objects_file = os.path.join(
            result_dir, metallicity_dir, "total_compact_objects.dat"
        )
        all_df = make_df(
            all_objects_file,
            probability_to_number_to_solarmass_factor=convolution_settings[
                "mass_in_stars"
            ],
            binary_fraction_factor=convolution_settings["binary_fraction"],
            limit_merging_time=limit_merging_in_hubbletime,
        )

        # Do query if its passed
        if convolution_settings.get("global_query", None):
            all_df = all_df.query(convolution_settings.get("global_query"))
            plot_settings["runname"] = plot_settings.get(
                "runname", ""
            ) + " (query: {})".format(convolution_settings.get("global_query"))

        # Rename columns
        primary_drop_columns = (
            set(all_df.columns) - set(primary_rename_dict.keys()) - set(common_columns)
        )

        all_df_primary = all_df.copy(deep=True)
        all_df_primary = all_df_primary.rename(columns=primary_rename_dict)
        all_df_primary = all_df_primary.drop(columns=primary_drop_columns)

        # Rename columns
        secondary_drop_columns = (
            set(all_df.columns)
            - set(secondary_rename_dict.keys())
            - set(common_columns)
        )

        all_df_secondary = all_df.copy(deep=True)
        all_df_secondary = all_df_secondary.rename(columns=secondary_rename_dict)
        all_df_secondary = all_df_secondary.drop(columns=secondary_drop_columns)

        all_df_combined = all_df_primary.append(all_df_secondary, ignore_index=True)
        all_df_combined["number_per_solar_mass"] = (
            all_df_combined["number_per_solar_mass"] / 2
        )

        # Filter out the error PPISNs
        all_df_combined = all_df_combined[all_df_combined.undergone_ppisn_type != 4]

        # Filter out the Nonetype PPISNs
        all_df_combined = all_df_combined[all_df_combined.undergone_ppisn_type != 5]

        # Filter out the -1 values. Not sure how they end up here anyway
        all_df_combined = all_df_combined[all_df_combined.undergone_ppisn_type != -1]

        # get values
        individual_ccsne_prob = all_df_combined.query("undergone_ppisn_type==0")[
            "number_per_solar_mass"
        ].sum()
        individual_ppisn_prob = all_df_combined.query("undergone_ppisn_type==1")[
            "number_per_solar_mass"
        ].sum()
        individual_pisn_prob = all_df_combined.query("undergone_ppisn_type==2")[
            "number_per_solar_mass"
        ].sum()
        individual_phdis_prob = all_df_combined.query("undergone_ppisn_type==3")[
            "number_per_solar_mass"
        ].sum()

        # # both stars doing the same
        both_ccsne_prob = all_df.query("undergone_ppisn_1==0 and undergone_ppisn_2==0")[
            "number_per_solar_mass"
        ].sum()
        both_ppisne_prob = all_df.query(
            "undergone_ppisn_1==1 and undergone_ppisn_2==1"
        )["number_per_solar_mass"].sum()
        both_pisne_prob = all_df.query("undergone_ppisn_1==2 and undergone_ppisn_2==2")[
            "number_per_solar_mass"
        ].sum()

        # store values
        individual_ccsne.append(individual_ccsne_prob)
        individual_ppisn.append(individual_ppisn_prob)
        individual_pisn.append(individual_pisn_prob)
        individual_phdis.append(individual_phdis_prob)

        both_ccsne.append(both_ccsne_prob)
        both_ppisne.append(both_ppisne_prob)
        both_pisne.append(both_pisne_prob)

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(20, 20))

    # Plotting:
    ax.plot(metallicity_values, individual_ccsne, label="Individual CC")
    ax.plot(metallicity_values, individual_ppisn, label="Individual PPISN")
    ax.plot(metallicity_values, individual_pisn, label="Individual PISN")
    ax.plot(metallicity_values, individual_phdis, label="Individual PHDIS")

    ax.plot(
        metallicity_values,
        both_ccsne,
        "--",
        label="Both CC",
    )
    ax.plot(metallicity_values, both_ppisne, "--", label="Both PPISN")
    ax.plot(metallicity_values, both_pisne, "--", label="Both PISN")

    # set scale
    ax.set_yscale("log")
    ax.set_xscale("log")

    # set labels
    ax.set_xlabel("Log metallicity")
    ax.set_ylabel("Log Yield (number per formed solar mass)")

    #
    ax.set_title("Yield of SN types (number per formed solar mass)")

    # legend
    plt.legend(loc=2, framealpha=0.5, fontsize=16)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


## Density plot for yield per metallicity per quanitity (combining all datasets onto 1 plot)
def plot_merger_rate_by_metallicity_by_merger_time(
    merger_dict, type_key, convolution_settings, plot_settings=None
):
    """
    Plot routine to plot all the black holes
    """

    if convolution_settings.get("global_query", None):
        plot_settings["runname"] = plot_settings.get(
            "runname", ""
        ) + " (query: {})".format(convolution_settings.get("global_query"))

    if not plot_settings:
        plot_settings = {}

    # Set up canvas and grid
    fig = plt.figure(constrained_layout=False)

    spec = gridspec.GridSpec(ncols=8, nrows=4, figure=fig, hspace=2)
    ax1 = fig.add_subplot(spec[:, :7])

    # Create colorbar axis
    colorbar_ax = fig.add_subplot(spec[:, 7])

    # get metallicity info
    (
        sorted_metallicity_keys,
        sorted_metallicity_keys_type,
        sorted_metallicity_values,
        sorted_metallicity_logvalues,
        logmetallicity_stepsize,
        bins_log10metallicity_values,
    ) = get_dataset_metallicity_info(merger_dict, type_key)

    # loop over all the metallicities
    for i, el in enumerate(sorted_metallicity_keys_type):
        info_dict = merger_dict[el]

        metallicity = el[1:].split("_")[0]

        merging_df = make_df(
            info_dict["filename"],
            info_dict["metallicity"],
            probability_to_number_to_solarmass_factor=convolution_settings[
                "mass_in_stars"
            ],
            binary_fraction_factor=convolution_settings["binary_fraction"],
            limit_merging_time=True,
        )

        # Do query if its passed
        if convolution_settings.get("global_query", None):
            merging_df = merging_df.query(convolution_settings.get("global_query"))

        keep_columns = set(
            [
                "number_per_solar_mass",
                "index",
                "merger_time_values_in_years",
                "undergone_ppisn_1",
                "undergone_ppisn_2",
                "mass_1",
                "mass_2",
            ]
        )
        drop_columns = list(set(merging_df.columns) - keep_columns)

        merging_df = merging_df.drop(columns=drop_columns)
        merging_df["metallicity"] = info_dict["metallicity"]

        #
        if i == 0:
            combined_df = merging_df.copy(deep=True)
        else:
            combined_df = combined_df.append(merging_df, ignore_index=True)

    # add lookback redshift
    # age_universe = cosmo.age(0)
    # combined_df['redshift'] = combined_df.apply(lambda row : astropy.cosmology.z_at_value(cosmo.age, float(age_universe.value-row['merger_time']/1e9) * u.Gyr), axis = 1)
    # print(combined_df['redshift'])

    # x_axis: merger time
    # bins_x = np.linspace(0, 14, 3*14)
    bins_x_log = np.linspace(-3, 2, 5 * 14)

    # y_axis: metallicity
    sorted_metallicity_logvalues = np.array(sorted_metallicity_logvalues)
    bins_y_log = (
        sorted_metallicity_logvalues - 0.5 * np.diff(sorted_metallicity_logvalues)[0]
    )
    bins_y_log = list(bins_y_log)
    bins_y_log.append(bins_y_log[-1] + logmetallicity_stepsize[-1])

    #
    histogram = np.histogram2d(
        np.log10(combined_df["merger_time_values_in_years"] / 1e9),
        np.log10(combined_df["metallicity"]),
        bins=[bins_x_log, bins_y_log],
        weights=combined_df["number_per_solar_mass"],
    )

    min_val = np.min(histogram[0][histogram[0] != 0])
    max_val = np.max(histogram[0][histogram[0] != 0])

    h = ax1.hist2d(
        np.log10(combined_df["merger_time_values_in_years"] / 1e9),
        np.log10(combined_df["metallicity"]),
        bins=[bins_x_log, bins_y_log],
        weights=combined_df["number_per_solar_mass"],
        norm=matplotlib.colors.LogNorm(vmin=min_val, vmax=max_val),
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        colorbar_ax,
        cmap=matplotlib.cm.viridis,
        norm=matplotlib.colors.LogNorm(vmin=min_val, vmax=max_val),
    )
    cbar.ax.set_ylabel("number_per_solar_mass")

    # Some makeup
    ax1.set_xlabel(r"Log10 Merger time [Gyr]")
    ax1.set_title(
        "Yield (number per formed solar mass) per merger time per metallicity: {}".format(
            type_key
        )
    )

    spec.update(hspace=0)
    ax1.set_ylabel("Log10 metallicity")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


## Density plots for yield per merger time per quantity. single dataset
def plot_merger_rate_per_mergertime_total_mass(dataset_dict, plot_settings=None):
    """
    Function to plot the merger rate per delay time (y axis), for range of masses (x axis)

    For merging in hubble time
    """

    if not plot_settings:
        plot_settings = {}

    df = make_df(
        dataset_dict["filename"],
        probability_to_number_to_solarmass_factor=convolution_settings["mass_in_stars"],
        binary_fraction_factor=convolution_settings["binary_fraction"],
    )
    df["total_mass"] = df["mass_1"] + df["mass_2"]

    bins_x = np.arange(0, 120, 5)
    bins_y = np.linspace(0, 14 * 1e3, 50)
    bins_y_log = np.linspace(-1, 10, 16)

    # Figure
    fig, axes = plt.subplots(nrows=1, ncols=1)

    h = axes.hist2d(
        df["total_mass"],
        np.log10(df["merger_time_values_in_years"] / 1e6),
        bins=[bins_x, bins_y_log],
        weights=df["number_per_solar_mass"],
        norm=matplotlib.colors.LogNorm(),
    )

    # make colorbar
    cb = fig.colorbar(h[3], ax=axes)
    cb.ax.set_ylabel("Yield (number per formed solar mass)")

    axes.set_ylabel("log Merger rate [Myr]")
    axes.set_xlabel(r"Total mass [M$_{\odot}$]")

    axes.set_title(
        "Yield (number per formed solar mass) per total mass and per merger time"
    )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_merger_rate_per_mergertime_chirpmass(dataset_dict, plot_settings=None):
    """
    Function to plot the merger rate per delay time (y axis), for range of masses (x axis)

    For merging in hubble time
    """

    if not plot_settings:
        plot_settings = {}

    df = make_df(
        dataset_dict["filename"],
        probability_to_number_to_solarmass_factor=convolution_settings["mass_in_stars"],
        binary_fraction_factor=convolution_settings["binary_fraction"],
    )

    bins_x = np.arange(0, 45, 2.5)
    bins_y = np.linspace(0, 14 * 1e3, 50)
    bins_y_log = np.linspace(-1, 10, 16)

    # Figure
    fig, axes = plt.subplots(nrows=1, ncols=1)

    h = axes.hist2d(
        df["chirpmass"],
        np.log(df["merger_time_values_in_years"] / 1e6),
        bins=[bins_x, bins_y_log],
        weights=df["number_per_solar_mass"],
        norm=matplotlib.colors.LogNorm(),
    )

    # colorbar
    cb = fig.colorbar(h[3], ax=axes)
    cb.ax.set_ylabel("Yield (number per formed solar mass)")

    axes.set_ylabel("log Merger rate [Myr]")
    axes.set_xlabel(r"Chirp mass [M$_{\odot}$]")

    axes.set_title(
        "Yield (number per formed solar mass) per chirp mass and per merger time"
    )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


## Density plots for yield per merger time per quantity. Subplot for each metallicity
def plot_combined_merger_rate_per_mergertime_total_mass(
    merger_info,
    type_key,
    convolution_settings,
    display_log_string=False,
    plot_settings=None,
):
    """
    Same as above, but then combined into 1 plot so they can share the colorbar
    """

    if convolution_settings.get("global_query", None):
        plot_settings["runname"] = plot_settings.get(
            "runname", ""
        ) + " (query: {})".format(convolution_settings.get("global_query"))

    if not plot_settings:
        plot_settings = {}

    # get metallicity info
    (
        sorted_metallicity_keys,
        sorted_metallicity_keys_type,
        sorted_metallicity_values,
        sorted_metallicity_logvalues,
        logmetallicity_stepsize,
        bins_log10metallicity_values,
    ) = get_dataset_metallicity_info(merger_info, type_key)

    # y_axis: metallicity
    sorted_metallicity_logvalues = np.array(sorted_metallicity_logvalues)
    bins_y_log = (
        sorted_metallicity_logvalues - 0.5 * np.diff(sorted_metallicity_logvalues)[0]
    )
    bins_y_log = list(bins_y_log)
    bins_y_log.append(bins_y_log[-1] + logmetallicity_stepsize)

    #
    amt_plots = len(sorted_metallicity_keys_type)

    #
    fig = plt.figure(constrained_layout=False, figsize=(12, amt_plots * 5))
    spec = gridspec.GridSpec(ncols=5, nrows=4 * amt_plots, figure=fig, hspace=2)
    plot_axes = []

    for i in range(amt_plots):
        lower = 4 * i
        upper = 4 * (i + 1)

        new_ax = fig.add_subplot(spec[lower:upper, :4])
        plot_axes.append(new_ax)

    # Create colorbar axis
    colorbar_ax = fig.add_subplot(spec[:, 4])

    # Set dummy min and max
    min_prob = 1e20
    max_prob = -1

    # Set bins
    if type_key == "bhbh":
        bins_x = resolution_settings[
            "plot_combined_merger_rate_per_mergertime_total_mass"
        ]["bhbh"]["x"]
        bins_y_log = resolution_settings[
            "plot_combined_merger_rate_per_mergertime_total_mass"
        ]["bhbh"]["y"]
    elif type_key == "bhns":
        bins_x = resolution_settings[
            "plot_combined_merger_rate_per_mergertime_total_mass"
        ]["bhns"]["x"]
        bins_y_log = resolution_settings[
            "plot_combined_merger_rate_per_mergertime_total_mass"
        ]["bhns"]["y"]
    elif type_key == "nsns":
        bins_x = resolution_settings[
            "plot_combined_merger_rate_per_mergertime_total_mass"
        ]["nsns"]["x"]
        bins_y_log = resolution_settings[
            "plot_combined_merger_rate_per_mergertime_total_mass"
        ]["nsns"]["y"]
    else:
        print("Key unknown")
        raise ValueError

    # Loop to get the numbers
    for i, sub_key in enumerate(sorted_metallicity_keys_type):
        df = make_df(
            merger_info[sub_key]["filename"],
            probability_to_number_to_solarmass_factor=convolution_settings[
                "mass_in_stars"
            ],
            binary_fraction_factor=convolution_settings["binary_fraction"],
        )

        # Do query if its passed
        if convolution_settings.get("global_query", None):
            df = df.query(convolution_settings.get("global_query"))

        if df.empty:
            continue

        df["total_mass"] = df["mass_1"] + df["mass_2"]

        h = np.histogram2d(
            df["total_mass"],
            np.log10(df["merger_time_values_in_years"] / 1e6),
            bins=[bins_x, bins_y_log],
            weights=df["number_per_solar_mass"],
        )

        # Check for min and max
        ding = h[0]
        min_val = np.min(ding[ding != 0])
        max_val = np.max(ding[ding != 0])

        if min_val < min_prob:
            min_prob = min_val
        if max_val > max_prob:
            max_prob = max_val

    # Loop to make the plot
    for i, sub_key in enumerate(sorted_metallicity_keys_type):
        info_dict = merger_info[sub_key]

        df = make_df(
            merger_info[sub_key]["filename"],
            probability_to_number_to_solarmass_factor=convolution_settings[
                "mass_in_stars"
            ],
            binary_fraction_factor=convolution_settings["binary_fraction"],
        )

        # Do query if its passed
        if convolution_settings.get("global_query", None):
            df = df.query(convolution_settings.get("global_query"))

        if df.empty:
            continue

        df["total_mass"] = df["mass_1"] + df["mass_2"]

        h = plot_axes[i].hist2d(
            df["total_mass"],
            np.log10(df["merger_time_values_in_years"] / 1e6),
            bins=[bins_x, bins_y_log],
            weights=df["number_per_solar_mass"],
            norm=matplotlib.colors.LogNorm(vmin=min_prob, vmax=max_prob),
        )

        # Set title
        plot_axes[i].set_title(
            "{}".format(
                "Z={:.4g}".format(info_dict["metallicity"])
                if not display_log_string
                else "Log10 Z={:.4g}".format(np.log10(info_dict["metallicity"])),
            ),
            fontsize=12,
        )

        # Remove xticks
        if not i == amt_plots - 1:
            plot_axes[i].set_xticklabels([])

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        colorbar_ax,
        cmap=matplotlib.cm.viridis,
        norm=matplotlib.colors.LogNorm(vmin=min_prob, vmax=max_prob),
    )
    cbar.ax.set_ylabel("Yieldd (number per formed solar mass)")

    fig.text(
        0.04,
        0.5,
        "log Merger time [Myr]",
        va="center",
        ha="center",
        rotation="vertical",
    )

    plot_axes[-1].set_xlabel(r"Total mass [M$_{\odot}$]")
    plot_axes[0].set_title(
        "Combined yield (number per formed solar mass) per merger time\nper total mass: {}".format(
            type_key
        )
    )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_combined_merger_rate_per_mergertime_chirpmass(
    merger_info,
    type_key,
    convolution_settings,
    display_log_string=False,
    plot_settings=None,
):
    """
    Same as above, but then combined into 1 plot so they can share the colorbar
    """

    if convolution_settings.get("global_query", None):
        plot_settings["runname"] = plot_settings.get(
            "runname", ""
        ) + " (query: {})".format(convolution_settings.get("global_query"))

    if not plot_settings:
        plot_settings = {}

    #
    sorted_metallicity_keys = sorted(
        merger_info.keys(), key=lambda metallicity: float(metallicity.split("_")[0][1:])
    )[::-1]
    sorted_metallicity_keys_type = [
        el for el in sorted_metallicity_keys if el.endswith("_{}".format(type_key))
    ]
    amt_plots = len(sorted_metallicity_keys_type)

    #
    fig = plt.figure(constrained_layout=False, figsize=(12, amt_plots * 5))
    spec = gridspec.GridSpec(ncols=3, nrows=4 * amt_plots, figure=fig, hspace=2)
    plot_axes = []

    for i in range(amt_plots):
        lower = 4 * i
        upper = 4 * (i + 1)

        new_ax = fig.add_subplot(spec[lower:upper, :2])
        plot_axes.append(new_ax)

    # Create colorbar axis
    colorbar_ax = fig.add_subplot(spec[:, 2])

    # Set dummy min and max
    min_prob = 1e20
    max_prob = -1

    # Set bins
    if type_key == "bhbh":
        bins_x = resolution_settings[
            "plot_combined_merger_rate_per_mergertime_chirpmass"
        ]["bhbh"]["x"]
        bins_y_log = resolution_settings[
            "plot_combined_merger_rate_per_mergertime_chirpmass"
        ]["bhbh"]["y"]
    elif type_key == "bhns":
        bins_x = resolution_settings[
            "plot_combined_merger_rate_per_mergertime_chirpmass"
        ]["bhns"]["x"]
        bins_y_log = resolution_settings[
            "plot_combined_merger_rate_per_mergertime_chirpmass"
        ]["bhns"]["y"]
    elif type_key == "nsns":
        bins_x = resolution_settings[
            "plot_combined_merger_rate_per_mergertime_chirpmass"
        ]["nsns"]["x"]
        bins_y_log = resolution_settings[
            "plot_combined_merger_rate_per_mergertime_chirpmass"
        ]["nsns"]["y"]
    else:
        print("Key unknown")
        raise ValueError

    # Loop to get the numbers
    for i, sub_key in enumerate(sorted_metallicity_keys_type):
        df = make_df(
            merger_info[sub_key]["filename"],
            probability_to_number_to_solarmass_factor=convolution_settings[
                "mass_in_stars"
            ],
            binary_fraction_factor=convolution_settings["binary_fraction"],
        )

        # Do query if its passed
        if convolution_settings.get("global_query", None):
            df = df.query(convolution_settings.get("global_query"))

        if df.empty:
            continue

        h = np.histogram2d(
            df["chirpmass"],
            np.log10(df["merger_time_values_in_years"] / 1e6),
            bins=[bins_x, bins_y_log],
            weights=df["number_per_solar_mass"],
        )

        # Check for min and max
        ding = h[0]
        min_val = np.min(ding[ding != 0])
        max_val = np.max(ding[ding != 0])

        if min_val < min_prob:
            min_prob = min_val
        if max_val > max_prob:
            max_prob = max_val

    # Loop to make the plot
    for i, sub_key in enumerate(sorted_metallicity_keys_type):
        info_dict = merger_info[sub_key]

        df = make_df(
            merger_info[sub_key]["filename"],
            probability_to_number_to_solarmass_factor=convolution_settings[
                "mass_in_stars"
            ],
            binary_fraction_factor=convolution_settings["binary_fraction"],
        )

        # Do query if its passed
        if convolution_settings.get("global_query", None):
            df = df.query(convolution_settings.get("global_query"))

        if df.empty:
            continue

        h = plot_axes[i].hist2d(
            df["chirpmass"],
            np.log10(df["merger_time_values_in_years"] / 1e6),
            bins=[bins_x, bins_y_log],
            weights=df["number_per_solar_mass"],
            norm=matplotlib.colors.LogNorm(vmin=min_prob, vmax=max_prob),
        )

        # Set title
        plot_axes[i].set_title(
            "{}".format(
                "Z={:.4g}".format(info_dict["metallicity"])
                if not display_log_string
                else "Log10 Z={:.4g}".format(np.log10(info_dict["metallicity"])),
            ),
            fontsize=12,
        )

        # Remove xticks
        if not i == amt_plots - 1:
            plot_axes[i].set_xticklabels([])

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        colorbar_ax,
        cmap=matplotlib.cm.viridis,
        norm=matplotlib.colors.LogNorm(vmin=min_prob, vmax=max_prob),
    )
    cbar.ax.set_ylabel("Yield (number per formed solar mass)")

    #
    fig.text(
        0.04,
        0.5,
        "log Merger rate [Myr]",
        va="center",
        ha="center",
        rotation="vertical",
    )

    plot_axes[-1].set_xlabel(r"Chirpmass [M$_{\odot}$]")
    plot_axes[0].set_title(
        "Combined yield (number per formed solar mass) per merger time\nper chirpmass: {}".format(
            type_key
        )
    )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


# TODO: do the same for primary mass
# TODO: do the same for any mass

# Plot chirpmass (for single dataset)
def plot_chirp_mass(dataset_dict, convolution_settings, plot_settings=None):
    """
    Function to plot the chirp mass distribution of the dataset
    """

    if not plot_settings:
        plot_settings = {}

    df = make_df(
        dataset_dict["filename"],
        probability_to_number_to_solarmass_factor=convolution_settings["mass_in_stars"],
        binary_fraction_factor=convolution_settings["binary_fraction"],
    )

    # Chirp mass
    plt.title(
        "BHBH chirp mass distribution for systems merging in {:.2e} yr".format(
            AGE_UNIVERSE_IN_YEAR
        )
    )
    plt.hist(
        df["chirpmass"], bins=20, weights=df["number_per_solar_mass"], density=True
    )
    plt.yscale("log")
    plt.ylabel(r"Yield (number per formed solar mass)")
    plt.xlabel(r"Mass [M$_{\odot}$]")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


# Plot total mass (for all datasets)
def plot_total_masses(
    merger_info,
    type_key,
    convolution_settings,
    display_log_string=False,
    plot_settings=None,
):
    """
    Function to plot the total mass for all bbhs and all merging systems
    """

    if convolution_settings.get("global_query", None):
        plot_settings["runname"] = plot_settings.get(
            "runname", ""
        ) + " (query: {})".format(convolution_settings.get("global_query"))

    if not plot_settings:
        plot_settings = {}

    # Set up canvas and grid
    fig3 = plt.figure(constrained_layout=False)
    gs = fig3.add_gridspec(
        4,
        4,
    )
    ax1 = fig3.add_subplot(gs[:2, :])
    ax2 = fig3.add_subplot(gs[2:, :], sharex=ax1)

    if type_key == "bhbh":
        bins = resolution_settings["plot_total_masses"]["bhbh"]
    elif type_key == "bhns":
        bins = resolution_settings["plot_total_masses"]["bhns"]
    elif type_key == "nsns":
        bins = resolution_settings["plot_total_masses"]["nsns"]
    else:
        print("Key unknown")
        raise ValueError

    # loop
    for el in sorted(merger_info.keys(), reverse=True):
        info_dict = merger_info[el]
        if info_dict["type"] == type_key:

            merging_df_all = make_df(
                info_dict["filename"],
                info_dict["metallicity"],
                probability_to_number_to_solarmass_factor=convolution_settings[
                    "mass_in_stars"
                ],
                binary_fraction_factor=convolution_settings["binary_fraction"],
                limit_merging_time=False,
            )
            merging_df_all["total_mass"] = (
                merging_df_all["mass_1"] + merging_df_all["mass_2"]
            )

            # Do query if its passed
            if convolution_settings.get("global_query", None):
                merging_df_all = merging_df_all.query(
                    convolution_settings.get("global_query")
                )

            merging_df_merging = merging_df_all[
                merging_df_all.merger_time_values_in_years < AGE_UNIVERSE_IN_YEAR
            ]

            # Plot all the BBHs
            (n, bins, _) = ax1.hist(
                merging_df_all["total_mass"],
                bins=bins,
                weights=merging_df_all["number_per_solar_mass"],
                histtype="step",
                # linestyle=settings.LINESTYLE_TUPLE[el_i][1] if not el_i==0 else '-',
                linewidth=4,
            )

            # Plot all the merging BBHs
            (n, bins, patches) = ax2.hist(
                merging_df_merging["total_mass"],
                bins=bins,
                weights=merging_df_merging["number_per_solar_mass"],
                label=r"{}".format(
                    "Z={:.4g}".format(info_dict["metallicity"])
                    if not display_log_string
                    else "Log10 Z={:.4g}".format(np.log10(info_dict["metallicity"])),
                ),
                histtype="step",
                # linestyle=settings.LINESTYLE_TUPLE[el_i][1] if not el_i==0 else '-',
                linewidth=4,
            )

    # Scaling
    ax1.set_yscale("log")
    ax2.set_yscale("log")

    # Set ranges equal
    min_y_lim = np.min([ax1.get_ylim()[0], ax2.get_ylim()[0]])
    max_y_lim = np.max([ax1.get_ylim()[1], ax2.get_ylim()[1]])

    ax1.set_ylim((min_y_lim, max_y_lim))
    ax2.set_ylim((min_y_lim, max_y_lim))

    ax2.legend()

    # Some makeup
    ax2.set_xlabel(r"Total mass [M$_{\odot}$]")
    plt.title(
        "Total mass all {} and merging {} fiducial runs".format(type_key, type_key)
    )
    plt.ylabel("# of stars")

    # Add info and plot the figure
    fig3 = add_plot_info(fig3, plot_settings)
    show_and_save_plot(fig3, plot_settings)


# Plot any mass (for all datasets)
def plot_any_mass_plot(
    merger_dict,
    type_key,
    convolution_settings,
    display_log_string=False,
    plot_settings=None,
):
    """
    Plot routine to plot all the black holes
    """

    if convolution_settings.get("global_query", None):
        plot_settings["runname"] = plot_settings.get(
            "runname", ""
        ) + " (query: {})".format(convolution_settings.get("global_query"))

    if not plot_settings:
        plot_settings = {}

    # Set up canvas and grid
    fig3 = plt.figure(constrained_layout=False)
    gs = fig3.add_gridspec(
        4,
        4,
    )
    ax1 = fig3.add_subplot(gs[:2, :])
    ax2 = fig3.add_subplot(gs[2:, :], sharex=ax1)

    if type_key == "bhbh":
        bins = resolution_settings["plot_any_mass_plot"]["bhbh"]
    elif type_key == "bhns":
        bins = resolution_settings["plot_any_mass_plot"]["bhns"]
    elif type_key == "nsns":
        bins = resolution_settings["plot_any_mass_plot"]["nsns"]
    else:
        print("Key unknown")
        raise ValueError

    # Sort metallicity keys
    sorted_metallicity_keys = sorted(
        merger_dict.keys(),
        key=lambda metallicity: float(metallicity.split("_")[0][1:]),
        reverse=True,
    )
    sorted_metallicity_keys_type = [
        el for el in sorted_metallicity_keys if el.endswith("_{}".format(type_key))
    ]

    # loop over all the metallicities
    for el in sorted_metallicity_keys_type:
        info_dict = merger_dict[el]

        all_df = make_df(
            info_dict["filename"],
            info_dict["metallicity"],
            probability_to_number_to_solarmass_factor=convolution_settings[
                "mass_in_stars"
            ],
            binary_fraction_factor=convolution_settings["binary_fraction"],
            limit_merging_time=False,
        )

        # Do query if its passed
        if convolution_settings.get("global_query", None):
            all_df = all_df.query(convolution_settings.get("global_query"))

        # Rename columns
        all_df_primary = all_df.copy(deep=True)
        all_df_primary = all_df_primary.rename(columns={"mass_1": "mass"})
        all_df_primary = all_df_primary.drop(columns=["mass_2"])

        # Rename columns
        all_df_secondary = all_df.copy(deep=True)
        all_df_secondary = all_df_secondary.rename(columns={"mass_2": "mass"})
        all_df_secondary = all_df_secondary.drop(columns=["mass_1"])

        all_df_combined = all_df_primary.append(all_df_secondary, ignore_index=True)
        all_df_combined["number_per_solar_mass"] = (
            all_df_combined["number_per_solar_mass"] / 2
        )

        (n, bins, _) = ax1.hist(
            all_df_combined["mass"],
            bins=bins,
            weights=all_df_combined["number_per_solar_mass"],
            histtype="step",
            linewidth=4,
        )

        combined_df_combined = all_df_combined[
            all_df_combined.merger_time_values_in_years < AGE_UNIVERSE_IN_YEAR
        ]

        (n, bins, _) = ax2.hist(
            combined_df_combined["mass"],
            bins=bins,
            weights=combined_df_combined["number_per_solar_mass"],
            histtype="step",
            linewidth=4,
            label="{}".format(
                "Z={:.4g}".format(info_dict["metallicity"])
                if not display_log_string
                else "Log10 Z={:.4g}".format(np.log10(info_dict["metallicity"])),
            ),
        )

    # Scaling
    ax1.set_yscale("log")
    ax2.set_yscale("log")

    # Set ranges equal
    min_y_lim = np.min([ax1.get_ylim()[0], ax2.get_ylim()[0]])
    max_y_lim = np.max([ax1.get_ylim()[1], ax2.get_ylim()[1]])

    ax1.set_ylim((min_y_lim, max_y_lim))
    ax2.set_ylim((min_y_lim, max_y_lim))

    ax2.legend(fontsize=12, loc=1, framealpha=0.5, ncol=2)

    # Some makeup
    ax2.set_xlabel(r"Mass [M$_{\odot}$]")
    plt.title("All individuals mass all {} and merging {}".format(type_key, type_key))
    plt.ylabel("# of stars")

    # Add info and plot the figure
    fig3 = add_plot_info(fig3, plot_settings)
    show_and_save_plot(fig3, plot_settings)


#
def plot_neijssel19_figure_1(merger_dict, convolution_settings, plot_settings=None):
    """
    Function to plot the yield of the datasets merging within hubble time
    """

    if convolution_settings.get("global_query", None):
        plot_settings["runname"] = plot_settings.get(
            "runname", ""
        ) + " (query: {})".format(convolution_settings.get("global_query"))

    if not plot_settings:
        plot_settings = {}

    #

    log10metallicities_in_solar = []

    result_dict = {
        "bhbh": {"merger_per_unit_mass": [], "error_values": []},
        "bhns": {"merger_per_unit_mass": [], "error_values": []},
        "nsns": {"merger_per_unit_mass": [], "error_values": []},
        "channel_1": {"merger_per_unit_mass": [], "error_values": []},
        "channel_2": {"merger_per_unit_mass": [], "error_values": []},
        "channel_3": {"merger_per_unit_mass": [], "error_values": []},
    }

    for type_key in ["bhbh", "bhns", "nsns"]:

        # get metallicity info
        (
            sorted_metallicity_keys,
            sorted_metallicity_keys_type,
            sorted_metallicity_values,
            sorted_log10metallicity_values,
            stepsizes_log10metallicity_values,
            bins_log10metallicity_values,
        ) = get_dataset_metallicity_info(merger_dict, type_key)

        # loop over all the metallicities
        for i, el in enumerate(sorted_metallicity_keys_type):
            info_dict = merger_dict[el]

            if type_key == "bhbh":
                log10metallicities_in_solar.append(
                    np.log10(
                        info_dict["metallicity"] / config_dict_cosmology["solar_value"]
                    )
                )

            # print(info_dict)
            df = make_df(
                info_dict["filename"],
                probability_to_number_to_solarmass_factor=convolution_settings[
                    "mass_in_stars"
                ],
                binary_fraction_factor=convolution_settings["binary_fraction"],
                limit_merging_time=False,
            )

            # Limit by merging time
            df = df.query(
                "%s < %f" % ("merger_time_values_in_years", AGE_UNIVERSE_IN_YEAR)
            )

            # Do query if its passed
            if convolution_settings.get("global_query", None):
                df = df.query(convolution_settings.get("global_query"))

            #
            res_all = calculate_numbers(df, convolution_settings)
            result_dict[type_key]["merger_per_unit_mass"].append(res_all[1])
            result_dict[type_key]["error_values"].append(res_all[2])

            if type_key == "bhbh":
                # Channel 1:
                channel_1_df = df.query(coens_channels_query_dict["channel_1"]).copy(
                    deep=True
                )

                res_channel_1 = calculate_numbers(channel_1_df, convolution_settings)
                result_dict["channel_1"]["merger_per_unit_mass"].append(
                    res_channel_1[1]
                )
                result_dict["channel_1"]["error_values"].append(res_channel_1[2])

                # Channel 2
                channel_2_df = df.query(coens_channels_query_dict["channel_2"]).copy(
                    deep=True
                )

                res_channel_2 = calculate_numbers(channel_2_df, convolution_settings)
                result_dict["channel_2"]["merger_per_unit_mass"].append(
                    res_channel_2[1]
                )
                result_dict["channel_2"]["error_values"].append(res_channel_2[2])

                # Channel 2
                channel_3_df = df.query(coens_channels_query_dict["channel_3"]).copy(
                    deep=True
                )

                res_channel_3 = calculate_numbers(channel_3_df, convolution_settings)
                result_dict["channel_3"]["merger_per_unit_mass"].append(
                    res_channel_3[1]
                )
                result_dict["channel_3"]["error_values"].append(res_channel_3[2])

    #
    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(16, 16))

    # Plot rates per type
    axes.errorbar(
        log10metallicities_in_solar,
        np.array(result_dict["bhbh"]["merger_per_unit_mass"]) / 1e-5,
        yerr=np.array(result_dict["bhbh"]["error_values"]) / 1e-5,
        label="BBH",
    )
    axes.errorbar(
        log10metallicities_in_solar,
        np.array(result_dict["bhns"]["merger_per_unit_mass"]) / 1e-5,
        yerr=np.array(result_dict["bhns"]["error_values"]) / 1e-5,
        label="BHNS",
    )
    axes.errorbar(
        log10metallicities_in_solar,
        np.array(result_dict["nsns"]["merger_per_unit_mass"]) / 1e-5,
        yerr=np.array(result_dict["nsns"]["error_values"]) / 1e-5,
        label="BNS",
    )

    # Plot bhbh channel rates
    channel_1_plot_values = (
        np.array(result_dict["channel_1"]["merger_per_unit_mass"]) / 1e-5
    )
    channel_2_plot_values = (
        channel_1_plot_values
        + np.array(result_dict["channel_2"]["merger_per_unit_mass"]) / 1e-5
    )
    channel_3_plot_values = (
        channel_2_plot_values
        + np.array(result_dict["channel_3"]["merger_per_unit_mass"]) / 1e-5
    )

    # Plot the channels
    axes.plot(log10metallicities_in_solar, channel_1_plot_values, color="black")
    axes.plot(log10metallicities_in_solar, channel_2_plot_values, color="black")
    axes.plot(log10metallicities_in_solar, channel_3_plot_values, color="black")

    # fill_between
    axes.fill_between(
        log10metallicities_in_solar,
        [0 for _ in range(len(log10metallicities_in_solar))],
        channel_1_plot_values,
        color="#a6a6a6",
        label="channel I",
    )
    axes.fill_between(
        log10metallicities_in_solar,
        channel_1_plot_values,
        channel_2_plot_values,
        color="#8d8d8d",
        label="channel II",
    )
    axes.fill_between(
        log10metallicities_in_solar,
        channel_2_plot_values,
        channel_3_plot_values,
        color="#737373",
        label="channel III",
    )

    axes.legend(loc=1)
    axes.set_xlabel(r"Log$_{10}$ (Z/Z$_{\odot}$)")
    axes.set_ylabel(r"dN$_{form}$/dM$_{SFR}$ [10$^{-5}$ M$^{-1}_{\odot}$]")

    axes.set_title(
        "merger rate yield per metallicity with channels as in Neijsel 2021",
        fontsize=26,
    )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_neijsel_fig1_channels_lieke(
    merger_dict, convolution_settings, plot_settings=None
):
    """
    Function to plot the yield of
    """

    if convolution_settings.get("global_query", None):
        plot_settings["runname"] = plot_settings.get(
            "runname", ""
        ) + " (query: {})".format(convolution_settings.get("global_query"))

    if not plot_settings:
        plot_settings = {}

    log10metallicities_in_solar = []

    result_dict = {
        "bhbh": {"merger_per_unit_mass": [], "error_values": []},
        "bhns": {"merger_per_unit_mass": [], "error_values": []},
        "nsns": {"merger_per_unit_mass": [], "error_values": []},
        "channel_1": {"merger_per_unit_mass": [], "error_values": []},
        "channel_2": {"merger_per_unit_mass": [], "error_values": []},
    }

    for type_key in ["bhbh", "bhns", "nsns"]:

        # get metallicity info
        (
            sorted_metallicity_keys,
            sorted_metallicity_keys_type,
            sorted_metallicity_values,
            sorted_log10metallicity_values,
            stepsizes_log10metallicity_values,
            bins_log10metallicity_values,
        ) = get_dataset_metallicity_info(merger_dict, type_key)

        # loop over all the metallicities
        for i, el in enumerate(sorted_metallicity_keys_type):
            info_dict = merger_dict[el]

            if type_key == "bhbh":
                log10metallicities_in_solar.append(
                    np.log10(
                        info_dict["metallicity"] / config_dict_cosmology["solar_value"]
                    )
                )

            # print(info_dict)
            df = make_df(
                info_dict["filename"],
                probability_to_number_to_solarmass_factor=convolution_settings[
                    "mass_in_stars"
                ],
                binary_fraction_factor=convolution_settings["binary_fraction"],
                limit_merging_time=False,
            )

            # Limit by merging time
            df = df.query(
                "%s < %f" % ("merger_time_values_in_years", AGE_UNIVERSE_IN_YEAR)
            )

            # Do query if its passed
            if convolution_settings.get("global_query", None):
                df = df.query(convolution_settings.get("global_query"))

            #
            res_all = calculate_numbers(df, convolution_settings)
            result_dict[type_key]["merger_per_unit_mass"].append(res_all[1])
            result_dict[type_key]["error_values"].append(res_all[2])

            if type_key == "bhbh":
                # Channel 1:
                # channel_1_df = df.query(combine_queries(liekes_channels_query_dict['channel_1'], ppisn_query_dict['one_ppisn'])).copy(deep=True)
                channel_1_df = df.query(liekes_channels_query_dict["channel_1"]).copy(
                    deep=True
                )

                res_channel_1 = calculate_numbers(channel_1_df, convolution_settings)
                result_dict["channel_1"]["merger_per_unit_mass"].append(
                    res_channel_1[1]
                )
                result_dict["channel_1"]["error_values"].append(res_channel_1[2])

                # Channel 2
                # channel_2_df = df.query(combine_queries(liekes_channels_query_dict['channel_2'], ppisn_query_dict['one_ppisn'])).copy(deep=True)
                channel_2_df = df.query(liekes_channels_query_dict["channel_2"]).copy(
                    deep=True
                )

                res_channel_2 = calculate_numbers(channel_2_df, convolution_settings)
                result_dict["channel_2"]["merger_per_unit_mass"].append(
                    res_channel_2[1]
                )
                result_dict["channel_2"]["error_values"].append(res_channel_2[2])
    #
    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(16, 16))

    # Plot rates per type
    axes.errorbar(
        log10metallicities_in_solar,
        np.array(result_dict["bhbh"]["merger_per_unit_mass"]) / 1e-5,
        yerr=np.array(result_dict["bhbh"]["error_values"]) / 1e-5,
        label="BBH",
    )
    axes.errorbar(
        log10metallicities_in_solar,
        np.array(result_dict["bhns"]["merger_per_unit_mass"]) / 1e-5,
        yerr=np.array(result_dict["bhns"]["error_values"]) / 1e-5,
        label="BHNS",
    )
    axes.errorbar(
        log10metallicities_in_solar,
        np.array(result_dict["nsns"]["merger_per_unit_mass"]) / 1e-5,
        yerr=np.array(result_dict["nsns"]["error_values"]) / 1e-5,
        label="BNS",
    )

    # Plot bhbh channel rates
    channel_1_plot_values = (
        np.array(result_dict["channel_1"]["merger_per_unit_mass"]) / 1e-5
    )
    channel_2_plot_values = (
        channel_1_plot_values
        + np.array(result_dict["channel_2"]["merger_per_unit_mass"]) / 1e-5
    )

    # Plot the channels
    axes.plot(log10metallicities_in_solar, channel_1_plot_values, color="black")
    axes.plot(log10metallicities_in_solar, channel_2_plot_values, color="black")

    # fill_between
    axes.fill_between(
        log10metallicities_in_solar,
        [0 for _ in range(len(log10metallicities_in_solar))],
        channel_1_plot_values,
        color="#a6a6a6",
        label="channel I",
    )
    axes.fill_between(
        log10metallicities_in_solar,
        channel_1_plot_values,
        channel_2_plot_values,
        color="#8d8d8d",
        label="channel II",
    )

    axes.legend(loc=1)
    axes.set_xlabel(r"Log$_{10}$ (Z/Z$_{\odot}$)")
    axes.set_ylabel(r"dN$_{form}$/dM$_{SFR}$ [10$^{-5}$ M$^{-1}_{\odot}$]")

    axes.set_title(
        "merger rate yield per metallicity with channels as in van Son 2021",
        fontsize=26,
    )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
