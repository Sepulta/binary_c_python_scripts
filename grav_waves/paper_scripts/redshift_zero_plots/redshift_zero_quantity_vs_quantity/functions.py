"""
Utility functions for the plot_rate_density_quantity_at_redshift_zero_with_variations_and_ppisn_fraction function
"""

import h5py
import numpy as np
import pandas as pd

from grav_waves.settings import convolution_settings as convolution_configuration

from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    extract_columns_from_querylist,
    quantity_name_dict,
    quantity_unit_dict,
    rate_type_name_dict,
    rate_density_units,
    add_columns_to_df,
    dco_type_query,
    readout_rate_array_specific_keys,
    create_centers_from_bins,
    get_closest_redshift_key,
    plot_2d_results,
    plot_contourlevels,
    linestyle_list,
)
from grav_waves.convolution.functions.new_plot_routines.plot_kde_and_bootstrap_functions import (
    run_bootstrap,
)
from grav_waves.convolution.functions.redshift_interpolation_functions import (
    load_interpolation_data,
)
from grav_waves.gw_analysis.functions.cosmology_functions import (
    redshift_to_lookback_time,
)

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.canvas_functions import return_canvas_with_subsets
from david_phd_functions.plotting import custom_mpl_settings
import astropy.units as u

custom_mpl_settings.load_mpl_rc()


def get_data(
    filename,
    rate_type,
    dco_type,
    x_quantity,
    x_quantity_bins,
    y_quantity,
    y_quantity_bins,
    redshift_value,
    divide_by_binsize=False,
    general_query=None,
    querydict=None,
):
    """
    Function to read out the data from the datafile
    """

    ####
    # Read out dataframe
    combined_df = pd.read_hdf(filename, "/data/combined_dataframes")

    # Add columns
    used_columns = [x_quantity, y_quantity] + extract_columns_from_querylist(
        [querydict]
    )  # TODO: add general query in here
    columns_to_add = [
        column for column in used_columns if not column in combined_df.columns
    ]
    combined_df = add_columns_to_df(combined_df, columns_to_add)

    if y_quantity == "birth_redshift":
        print("Calculating birth redshift")

        # Get the data from the dataframe
        merger_time_in_years_values = combined_df[
            "merger_time_values_in_years"
        ].to_numpy()

        redshift_interpolators = load_interpolation_data(convolution_configuration)

        combined_df["birth_redshift"] = 0

        # calculate current lookback time
        current_lookback_value = redshift_to_lookback_time(redshift_value)
        birth_lookback_time_values_in_yr = (
            current_lookback_value.to(u.yr).value
        ) + merger_time_in_years_values
        birth_lookback_time_values_in_gyr = birth_lookback_time_values_in_yr * (
            u.yr.to(u.Gyr)
        )
        birth_redshift = redshift_interpolators[
            "lookback_time_to_redshift_interpolator"
        ](birth_lookback_time_values_in_gyr)

        #
        combined_df["birth_redshift"] = birth_redshift

    # Create result dict
    result_dict = {}

    #
    result_dict["all"] = readout_rate_data_run_bootstraps(
        filename=filename,
        combined_df=combined_df,
        rate_type=rate_type,
        dco_type=dco_type,
        x_quantity=x_quantity,
        x_quantity_bins=x_quantity_bins,
        y_quantity=y_quantity,
        y_quantity_bins=y_quantity_bins,
        redshift_value=redshift_value,
        general_query=general_query,
        query=None,
    )

    # Loop over queries
    if querydict is not None:
        result_dict["queried"] = readout_rate_data_run_bootstraps(
            filename=filename,
            combined_df=combined_df,
            rate_type=rate_type,
            dco_type=dco_type,
            x_quantity=x_quantity,
            x_quantity_bins=x_quantity_bins,
            y_quantity=y_quantity,
            y_quantity_bins=y_quantity_bins,
            redshift_value=redshift_value,
            general_query=general_query,
            query=querydict["query"],
        )

    return result_dict


def readout_rate_data_run_bootstraps(
    filename,
    combined_df,
    rate_type,
    dco_type,
    x_quantity,
    x_quantity_bins,
    y_quantity,
    y_quantity_bins,
    redshift_value,
    general_query=None,
    query=None,
):
    """
    Function to read out the rate data and get the histogram data, the KDE data and the bootstrapped data
    """

    # Get the quantity data from the dataframe
    x_quantity_data = combined_df[x_quantity].to_numpy()
    y_quantity_data = combined_df[y_quantity].to_numpy()

    ################
    # Create masks and apply, taking into account that the query and general query can be None
    dco_mask = combined_df.eval(dco_type_query(dco_type)).to_numpy()
    general_query_mask = (
        combined_df.eval(general_query)
        if general_query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    query_mask = (
        combined_df.eval(query)
        if query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    mask = dco_mask * general_query_mask * query_mask

    # mask the quantity data
    masked_x_quantity_data = x_quantity_data[mask]
    masked_y_quantity_data = y_quantity_data[mask]

    ################
    # Read out rate data for this specific redshift
    datafile = h5py.File(filename)
    all_redshift_keys = sorted(list(datafile["data/{}".format(rate_type)].keys()))
    closest_redshift_key = get_closest_redshift_key(redshift_value, all_redshift_keys)
    rate_array, redshifts = readout_rate_array_specific_keys(
        datafile=datafile,
        rate_key=rate_type,
        mask=mask,
        redshift_keys=[closest_redshift_key],
    )
    datafile.close()

    ################
    # Create the 2-d hist
    hist, _, _ = np.histogram2d(
        masked_x_quantity_data,
        masked_y_quantity_data,
        bins=[x_quantity_bins, y_quantity_bins],
        weights=rate_array[0],
    )

    #
    return hist.T
