import numpy as np
import h5py as h5
import os
import time
import matplotlib.pyplot as plt
from astropy.cosmology import WMAP9 as cosmology
import scipy
from scipy.interpolate import interp1d
from scipy.stats import norm as NormDist


import warnings
import astropy.units as u
import argparse


def find_sfr(redshifts, a=0.01, b=2.77, c=2.90, d=4.70):
    """
    Calculate the star forming mass per unit volume per year following
    Neijssel+19 Eq. 6, using functional form of Madau & Dickinson 2014

    Args:
        redshifts --> [list of floats] List of redshifts at which to evaluate the sfr

    Returns:
        sfr       --> [list of floats] Star forming mass per unit volume per year for each redshift
    """
    # get value in mass per year per cubic Mpc and convert to per cubic Gpc then return
    sfr = (
        a
        * ((1 + redshifts) ** b)
        / (1 + ((1 + redshifts) / c) ** d)
        * u.Msun
        / u.yr
        / u.Mpc**3
    )
    return sfr.to(u.Msun / u.yr / u.Gpc**3).value
