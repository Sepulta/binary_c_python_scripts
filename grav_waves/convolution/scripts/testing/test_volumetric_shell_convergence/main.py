"""
Script to test the volumetric shells convergence. Should span the same volume whatever the size
"""

import numpy as np

from grav_waves.convolution.functions.volume_convolution_functions import (
    create_shell_volume_dict,
)

lower_redshift = 0
upper_redshift = 5

stepsizes = [0.5, 0.25, 0.1, 0.05, 0.025, 0.01]

for stepsize in stepsizes:
    redshift_range = np.arange(lower_redshift, upper_redshift + stepsize, stepsize)

    diction = create_shell_volume_dict(redshift_range)
    total_volume = 0
    for key in sorted(diction.keys()):
        subdict = diction[key]

        total_volume += subdict["shell_volume"]
    print(
        "For lower_redshift: {} upper_redshift: {} stepsize: {} total_volume: {}".format(
            lower_redshift, upper_redshift, stepsize, total_volume
        )
    )
