"""
Function to plot the primary mass vs metallicity at redshift zero plot
"""

import os
import json

import h5py
import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

import astropy.units as u

#
from grav_waves.convolution.functions.convolution_general_functions import (
    create_bins_from_centers,
)
from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    generate_mask,
    add_columns_to_df,
    quantity_name_dict,
    quantity_unit_dict,
)
from grav_waves.convolution.functions.redshift_interpolation_functions import (
    load_interpolation_data,
)
from grav_waves.gw_analysis.functions.cosmology_functions import (
    redshift_to_lookback_time,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def plot_primary_mass_vs_birth_redshift_at_redshift_zero(
    dataset_filename, primary_mass_bins, plot_settings={}
):
    """
    Function to plot the primary mass vs redshift zero plot
    """

    ################
    # Read out info
    hdf5file = h5py.File(dataset_filename)

    # Get metallicity info
    metallicity_bin_centers = np.array(hdf5file["sfr_data/metallicity_bin_centers"][()])
    log10metallicity_values = np.array(sorted(np.log10(metallicity_bin_centers)))
    log10metallicity_bins = create_bins_from_centers(log10metallicity_values)
    metallicity_bins = 10**log10metallicity_bins
    metallicity_centers = (metallicity_bins[1:] + metallicity_bins[:-1]) / 2

    # Get convolution settings
    convolution_configuration = json.loads(
        hdf5file["settings/convolution_configuration"][()]
    )

    # Get redshift bin data
    redshifts = [float(el) for el in list(hdf5file["data/merger_rate"].keys())]
    sorted_redshifts = sorted(redshifts)
    current_redshift_center = (
        sorted_redshifts[1] + sorted_redshifts[0]
    ) / 2  # NOTE: we are not per se at redshift 0 exactly, but rather in the center of the bin

    zero_key = str(min(redshifts))
    rate_information_at_redshift_zero = hdf5file[
        "data/merger_rate/{}".format(zero_key)
    ][()]

    hdf5file.close()

    #################
    # Read out combined df
    combined_df = pd.read_hdf(dataset_filename, "/data/combined_dataframes")
    combined_df = add_columns_to_df(combined_df, columns=["primary_mass"])

    # Create mask and filter shit
    mask = generate_mask(combined_df, general_query=None, dco_type="bhbh")

    # Get the data from the dataframe
    primary_mass_values = combined_df["primary_mass"].to_numpy()[mask]
    merger_time_in_years_values = combined_df["merger_time_values_in_years"].to_numpy()[
        mask
    ]

    # Mask the rate data accordingly
    masked_rate_information_at_redshift_zero = rate_information_at_redshift_zero[mask]

    #######################
    # Convert the time when systems were born into birth redshift

    # Load redshift interpolation
    convolution_configuration[
        "interpolator_data_output_filename"
    ] = "/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/interpolator_data_dict.p"
    redshift_interpolators = load_interpolation_data(convolution_configuration)

    # calculate current lookback time
    current_lookback_value = redshift_to_lookback_time(current_redshift_center)
    birth_lookback_time_values_in_yr = (
        current_lookback_value.to(u.yr).value
    ) + merger_time_in_years_values
    birth_lookback_time_values_in_gyr = birth_lookback_time_values_in_yr * (
        u.yr.to(u.Gyr)
    )
    birth_redshift = redshift_interpolators["lookback_time_to_redshift_interpolator"](
        birth_lookback_time_values_in_gyr
    )

    # Since we have rebinned the data, there will be some systems within this redshift bin that are included but could not have formed if they were merging at <current_lookback_value>
    non_zero_birth_redshift_indices = birth_redshift != 0

    # Apply that mask too
    birth_redshift = birth_redshift[non_zero_birth_redshift_indices]
    primary_mass_values = primary_mass_values[non_zero_birth_redshift_indices]
    masked_rate_information_at_redshift_zero = masked_rate_information_at_redshift_zero[
        non_zero_birth_redshift_indices
    ]

    # Create bins for the birth redshift
    min_factor = 1
    max_factor = 1
    birth_redshift_bins = 10 ** np.linspace(
        np.log10(birth_redshift.min() * min_factor),
        np.log10(birth_redshift.max() * max_factor),
        50,
    )

    # create histogram
    hist = np.histogram2d(
        primary_mass_values,
        birth_redshift,
        bins=[primary_mass_bins, birth_redshift_bins],
        weights=masked_rate_information_at_redshift_zero,
    )
    primary_mass_centers = (primary_mass_bins[1:] + primary_mass_bins[:-1]) / 2
    birth_redshift_centers = (birth_redshift_bins[1:] + birth_redshift_bins[:-1]) / 2

    X, Y = np.meshgrid(primary_mass_centers, birth_redshift_centers)

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(22, 18))
    gs = fig.add_gridspec(nrows=1, ncols=11)

    ax = fig.add_subplot(gs[:, :-2])
    ax_cb = fig.add_subplot(gs[:, -1])

    # Get the normalisation
    scale = "log"
    if scale == "linear":
        norm = colors.Normalize(vmin=hist[0].min(), vmax=hist[0].max())
    elif scale == "log":
        norm = colors.LogNorm(
            vmin=10 ** (np.log10(hist[0].max()) - 3),
            vmax=hist[0].max(),
        )

    _ = ax.pcolormesh(
        X,
        Y,
        hist[0].T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # make colorbar
    cb = matplotlib.colorbar.ColorbarBase(
        ax_cb, norm=norm, extend="min" if scale == "log" else None
    )
    cb.ax.set_ylabel(
        r"Merger rate density $\mathcal{R}_{merger}(z_{birth}, Z)\ \left[\frac{d^{2}N_{merger}}{dt\ dV_{\mathrm{c}}}\right]$",
        fontsize=plot_settings.get("axislabel_fontsize", 26),
    )

    ax.set_xlabel(
        "{} [{}]".format(
            quantity_name_dict["primary_mass"],
            quantity_unit_dict["primary_mass"].to_string("latex"),
        ),
        fontsize=plot_settings.get("axislabel_fontsize", 26),
    )
    ax.set_ylabel(
        "Birth redshift [z]", fontsize=plot_settings.get("axislabel_fontsize", 26)
    )
    ax.set_yscale("log")

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    fiducial_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5"

    #
    base_name = "primary_mass_vs_birth_redshift_at_redshift_zero.pdf"
    plot_primary_mass_vs_birth_redshift_at_redshift_zero(
        dataset_filename=fiducial_dataset,
        primary_mass_bins=np.arange(0, 70, 1),
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                "/home/david/Dropbox/Academic/PHD/papers/paper_gw/paper_tex/figures/fiducial_at_redshift_zero",
                base_name,
            ),
            "axislabel_fontsize": 42,
        },
    )
