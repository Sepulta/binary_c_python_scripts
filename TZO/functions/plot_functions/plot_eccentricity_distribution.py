"""
Function to plot the eccentricity distribution for a dataframe containing TZO events
"""

import numpy as np

import matplotlib.pyplot as plt

from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    linestyle_list,
)

from david_phd_functions.plotting.utils import show_and_save_plot, add_plot_info
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def plot_eccentricity_distribution(
    df, plot_ratio=True, plot_channels=False, plot_settings={}
):
    """
    Function to plot the distribution of eccentricities in the prev timestep
    """

    #
    bins = np.arange(0, 1, 0.02)
    bincenters = (bins[1:] + bins[:-1]) / 2
    hist = np.histogram(df["prev_eccentricity"], bins=bins, weights=df["probability"])

    ######
    # Plot
    fig = plt.figure(figsize=(20, 20))

    #
    gs = fig.add_gridspec(nrows=10, ncols=1)

    #
    if plot_ratio:
        ax = fig.add_subplot(gs[:7, :])
        ax_ratio = fig.add_subplot(gs[8:, :])
        ax_ratio.set_ylim([0, 1])
    else:
        ax = fig.add_subplot(gs[:, :])

    #
    ax.plot(bincenters, hist[0])
    ax.set_yscale("log")

    #
    channels_rates = {}
    for _, channel in enumerate(sorted(df["prev_stellar_type_2"].unique())):
        channel_df = df[df.prev_stellar_type_2 == channel]

        channels_rates[channel] = np.histogram(
            channel_df["prev_eccentricity"],
            bins=bins,
            weights=channel_df["probability"],
        )[0]

    #
    if plot_channels:
        for channel_i, channel in enumerate(sorted(df["prev_stellar_type_2"].unique())):
            ax.plot(bincenters, channels_rates[channel])

    #
    if plot_ratio:
        for channel_i, channel in enumerate(sorted(df["prev_stellar_type_2"].unique())):
            ax_ratio.plot(bincenters, channels_rates[channel] / hist[0])

    #######################
    # Save and finish
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
