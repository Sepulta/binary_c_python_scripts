"""
Main function to create the convolution pipeline with the new logging structure
"""

import os
import logging

from grav_waves.settings import convolution_settings, config_dict_cosmology

from grav_waves.convolution.scripts.convolve_with_new_logging.functions import (
    create_combined_dco_dataset,
)
from grav_waves.gw_analysis.functions.functions import make_dataset_dict


#
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


if __name__ == "__main__":
    ##
    #
    output_root = os.path.join(this_file_dir)

    #######################
    # Select simulation

    # Fiducial
    main_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED"
    convolution_output_dir = os.path.join(main_dir, "sn_convolution_results/")

    # Get the correct directories
    dataset_dict_dco = make_dataset_dict(main_dir)
    convolution_settings["logger"].setLevel(logging.DEBUG)

    create_combined_dco_dataset(
        dataset_dict=dataset_dict_dco,
        dco_type="combined_dco",
        convolution_configuration=convolution_settings,
        cosmology_configuration=config_dict_cosmology,
        output_filename="results/tmp/combined_dco_dataset.hdf5",
    )

    # combine_dataframes(
    #     simname_dir=main_dir,
    #     dco_type='combined_dco',
    #     convolution_configuration=convolution_settings
    # )

    # filename = '/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/population_results/Z0.0002865120269663782/total_compact_objects.dat'
    # return_dco_dataframe(
    #     filename=filename,
    #     dco_type='combined_dco',
    #     convolution_configuration=convolution_settings
    # )
