import os
import sys


binaryc_dir = os.getenv("BINARYC_DIR")

print(binaryc_dir)

import subprocess

from binarycpython.utils.functions import get_help


tbse_lines = subprocess.run(
    ["./tbse", "echolines"], stdout=subprocess.PIPE, check=True, cwd=binaryc_dir
).stdout.decode("utf-8")
split_tbse_lines = tbse_lines.split("\n")


# Clean list:
clean_list = [el.strip() for el in split_tbse_lines if not el.strip() == ""]
# print(len(clean_list))

# help(get_help)

same_as_default = []
not_same_as_default = []


for arg in clean_list:
    split_arg = arg.split()
    if len(split_arg) == 2:
        arg = split_arg[0]
        val = split_arg[1]
        help_dict = get_help(arg, print_help=False)

        try:
            default = help_dict["default"]
            if default == val:
                same_as_default.append([arg, val, default])
            else:
                not_same_as_default.append([arg, val, default])
        except TypeError as e:
            print(e)

print(len(same_as_default))
for el in not_same_as_default:
    print(el)
