"""
Function that runs all the plotting functions. Each of the routines should be able to handle all the stuff themselves, this is just a main point

Plots:
- theory plots
- ensemble plots
    - ensemble plots with querysets
- rlof episode plots
    - rlof episode plots with querysets


It does not plot:
- paper tables and plots
"""

import os

from PyPDF2 import PdfFileMerger
from david_phd_functions.plotting.pdf_functions import add_pdf_and_bookmark

#
from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
)
from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict

# #
from RLOF.scripts.theory_plots.general_theory_plot_function import (
    general_plot_routine as general_theory_plot_routine,
)
from RLOF.scripts.plot_ensemble_output.general_plot_routine import (
    general_plot_routine as general_ensemble_plot_routine,
)
from RLOF.scripts.plot_episode_output.general_plot_function import (
    general_plot_routine as general_episode_plot_routine,
)


def main_plot_function(
    main_dataset_dir,
    combined_pdf_filename,
    rebuild_results=True,
    ensemble_querysets=None,
    episode_querysets=None,
    verbose=1,
    testing=False,
    do_theory_plots=True,
    do_ensemble_plots=True,
    do_episode_plots=True,
):
    """
    Function that runs all the plotting functions
    TODO: test with each individual plot routines and combine into 1 s
    """

    population_datasets = generate_ensemble_dataset_dict(main_dataset_dir)

    main_plot_dir = os.path.join(main_dataset_dir, "plots")

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    #
    print(
        "Running all plot routines for simulation {}\n\t{}".format(
            population_datasets["simname"], population_datasets["simulation_path"]
        )
    )

    ###################
    # Theory plots
    if do_theory_plots:
        theory_plots_dir = os.path.join(main_plot_dir, "theory_plots")
        theory_plots_combined_pdf_filename = os.path.join(
            main_plot_dir, "combined_theory_plots.pdf"
        )

        os.makedirs(theory_plots_dir, exist_ok=True)

        # Run the plot routine
        general_theory_plot_routine(
            output_dir=theory_plots_dir,
            output_pdf_filename=theory_plots_combined_pdf_filename,
            rebuild_results=rebuild_results,
            verbose=verbose,
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            theory_plots_combined_pdf_filename,
            pdf_page_number,
            bookmark_text="Theory plots",
            add_source_bookmarks=True,
        )

    ###################
    # Ensemble plots
    if do_ensemble_plots:
        ensemble_plots_dir = os.path.join(main_dataset_dir, "ensemble_plots")
        os.makedirs(ensemble_plots_dir, exist_ok=True)

        rlof_ensemble_plots_combined_filename = os.path.join(
            main_plot_dir, "combined_rlof_ensemble_plots.pdf"
        )

        # get the dictionary of the datasets
        population_datasets = generate_ensemble_dataset_dict(
            population_datasets["simulation_path"]
        )

        # Run the plot routine
        general_ensemble_plot_routine(
            sim_dict=population_datasets,
            combined_output_filename=rlof_ensemble_plots_combined_filename,
            output_dir=ensemble_plots_dir,
            verbose=verbose,
            testing=testing,
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            rlof_ensemble_plots_combined_filename,
            pdf_page_number,
            bookmark_text="Ensemble plots",
            add_source_bookmarks=True,
        )

        ###################
        # Ensemble plots with querysets
        # TODO: fix plotting with querysets

    ###################
    # RLOF episode plots
    if do_episode_plots:
        rlof_episode_plots_dir = os.path.join(main_plot_dir, "rlof_episode_plots")
        os.makedirs(rlof_episode_plots_dir, exist_ok=True)

        # Readout the dataset dict
        rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(main_dataset_dir)

        rlof_episode_plots_combined_filename = os.path.join(
            main_plot_dir, "combined_rlof_episode_plots.pdf"
        )

        # Run the plot routine
        general_episode_plot_routine(
            rlof_episode_dataset_dict=rlof_episode_dataset_dict,
            output_dir=rlof_episode_plots_dir,
            combined_output_pdf=rlof_episode_plots_combined_filename,
            verbose=verbose,
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            rlof_episode_plots_combined_filename,
            pdf_page_number,
            bookmark_text="Episode plots",
            add_source_bookmarks=True,
        )

        ###################
        # RLOF episode plots with querysets
        # TODO: fix episode plotting routine with querysets

    # wrap up the pdf
    merger.write(combined_pdf_filename)
    merger.close()
    print("\t wrote combined pdf to {}".format(combined_pdf_filename))
