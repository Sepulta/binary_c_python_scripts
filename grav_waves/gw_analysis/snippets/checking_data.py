import pandas as pd
import matplotlib.pyplot as plt

import time

import numpy as np

from david_phd_functions.grav_waves.dataframe_functions import (
    add_chirpmass_column,
    add_inspiral_time_column,
)
from david_phd_functions.grav_waves.general_functions import calc_inspiral_time

from david_phd_functions.grav_waves.merger_rate_functions import (
    calculate_probabilty_per_solarmass,
)
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

from plot_functions import *

load_mpl_rc()

datafile_0002 = "/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/gw_analysis/results_bigsim/DEFAULT/GW_z0.002/bhbh.dat"
df_0002 = pd.read_table(datafile_0002, sep="\s+", index_col=None)

df_0002 = df_0002[df_0002.eccentricity >= 0]

first = df_0002.iloc[0]
print(first["merger_time"])
print(
    calc_inspiral_time(
        first["mass_1"], first["mass_2"], first["period"], first["eccentricity"]
    )
)

# print(first)

# with open(datafile_0002, 'r') as f:
#     first = f.readline()
#     print(len(first.split()))

#     second = f.readline()
#     print(len(second.split()))
