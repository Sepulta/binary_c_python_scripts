"""
Functions to handle calculating the star formation and meta;licity distributions
"""

import numpy as np

from grav_waves.gw_analysis.functions.cosmology_functions import (
    starformation_rate,
    metallicity_distribution,
    normalize_metallicities_distribution,
)

from grav_waves.convolution.functions.compas_metallicity_distribution import (
    find_metallicity_distribution,
)


def generate_metallicity_sfr_array(
    cosmology_configuration, time_centers, sorted_metallicity_log10values
):
    """
    Function that generates the 2d array containing

    - Time array
    - available metallicity array

    First it sets up the empty array

    Then we loop over all the values of time, generate the metallicity weighting and sfr values and put them into the array
    """

    # Get the SFR values:
    starformation_array = starformation_rate(
        time_centers, cosmology_configuration=cosmology_configuration, verbosity=1
    )

    #
    sampled_metallicity_values = 10**sorted_metallicity_log10values
    log_metallicities = np.log(sampled_metallicity_values)

    max_log_z = log_metallicities.max()
    min_log_z = log_metallicities.min()

    if cosmology_configuration["metallicity_distribution_function"] in [
        "vanSon21",
        "Neijssel12",
        "COMPAS",
    ]:
        # Calculate the metallicity distribution
        dPdlogZ, metallicities, _ = find_metallicity_distribution(
            time_centers,
            min_log_z,
            max_log_z,
            **cosmology_configuration["metallicity_distribution_args"],
        )
    else:
        raise ValueError(
            "Currently no other metallicity distribution choice is available. Abort"
        )

    # Select the metallicities that we have
    dPdLogZ_for_sampled_metallicities = dPdlogZ[
        :, np.digitize(sampled_metallicity_values, metallicities) - 1
    ]

    # Calculate the dlogZ (stepsizes) values, adding one to the end.
    extended_log_metallicities = np.append(
        np.array(log_metallicities[0] - np.diff(log_metallicities)[0]),
        log_metallicities,
    )
    dlogZ_sampled = np.diff(extended_log_metallicities)

    # Calculate dP/dlogZ * dlogZ
    dP = dPdLogZ_for_sampled_metallicities * dlogZ_sampled

    # Multiply by sfr:
    metallicity_weighted_starformation_array = (starformation_array * dP.T).T

    # TODO: remove this at this location. Modify it at the convolution step
    # We need to add two empty columns here to make sure the digitise does not multiply the wrong one
    metallicity_weighted_starformation_array = np.insert(
        metallicity_weighted_starformation_array, 0, 0, axis=0
    )
    metallicity_weighted_starformation_array = np.insert(
        metallicity_weighted_starformation_array,
        metallicity_weighted_starformation_array.shape[0],
        0,
        axis=0,
    )

    #
    return metallicity_weighted_starformation_array, dP, starformation_array
