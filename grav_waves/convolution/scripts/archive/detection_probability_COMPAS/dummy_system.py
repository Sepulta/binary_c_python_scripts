"""
Script to calculate the detection probability for a dummy system
"""

from COMPAS.postProcessing.Folders.CosmicIntegration.PythonScripts.selection_effects import *
from grav_waves.settings import cosmo

help(detection_probability)
snr_threshold = 7
redshift = 0.001
distance_in_mpc = cosmo.luminosity_distance(redshift)

mass_1_detector_frame = 10
mass_2_detector_frame = 5

mass_1_source_frame = mass_1_detector_frame * (1 + redshift)
mass_2_source_frame = mass_2_detector_frame * (1 + redshift)

current_detection_probability = detection_probability(
    mass_1_source_frame,
    mass_2_source_frame,
    redshift,
    distance_in_mpc.value,
    snr_threshold,
)


info_string = """Calculating the detection probability for:
	mass_1_detector_frame: {}
	mass_2_detector_frame: {}

	redshift: {}
	distance_in_mpc: {}
	mass_1_source_frame: {}
	mass_2_source_frame: {}

	current_detection_probability: {}
""".format(
    mass_1_detector_frame,
    mass_2_detector_frame,
    redshift,
    distance_in_mpc,
    mass_1_source_frame,
    mass_2_source_frame,
    current_detection_probability,
)
print(info_string)
