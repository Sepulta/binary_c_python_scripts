"""
Function to generate the plots for the paper showing the
"""

import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

from david_phd_functions.masstransfer.functions import (
    rochelobe_radius_accretor,
    rmin_fkr,
    rcirc_fkr,
)

LINTHRESH = 1
LINSCALE = 1

columnsize_mnras = (3, 4)

fontsize_labels = 50


def return_lubow_shu_values():
    """
    Function to return the values from the lubow & shu 1975 paper table 2
    """

    q_acc_don_array = [
        0.0667,
        0.15,
        0.3,
        0.5,
        0.75,
        1,
        1.6667,
        3.0,
        4.5,
        5.5,
        7.0,
        8.5,
        10.0,
        15.0,
    ]
    rmin_array = [
        0.0228,
        0.0276,
        0.0329,
        0.0382,
        0.0438,
        0.0488,
        0.0604,
        0.0793,
        0.0966,
        0.1066,
        0.1198,
        0.1316,
        0.1422,
        0.1714,
    ]
    rcirc_array = [
        0.0404,
        0.0492,
        0.0589,
        0.0683,
        0.0780,
        0.0865,
        0.1058,
        0.1363,
        0.1631,
        0.1781,
        0.1978,
        0.2147,
        0.2297,
        0.2698,
    ]

    return {
        "q_acc_don_array": q_acc_don_array,
        "rmin_array": rmin_array,
        "rcirc_array": rcirc_array,
    }


def return_paczsynski_values():
    """
    Function to return the values of paczynski on which the formala for the tidal radius is based on.

    These results are from table 1 in paczynski 1977
    """

    mu = [
        0.03,
        0.05,
        0.07,
        0.10,
        0.15,
        0.20,
        0.25,
        0.30,
        0.35,
        0.38,
        0.40,
        0.41,
        0.42,
        0.422,
        0.423,
        0.424,
        0.43,
        0.44,
        0.45,
        0.50,
        0.55,
        0.60,
        0.65,
        0.70,
        0.75,
        0.80,
        0.85,
        0.90,
        0.93,
        0.95,
        0.97,
    ]

    rmax = [
        0.600,
        0.585,
        0.567,
        0.544,
        0.511,
        0.480,
        0.451,
        0.427,
        0.404,
        0.393,
        0.387,
        0.385,
        0.386,
        0.387,
        0.365,
        0.361,
        0.352,
        0.341,
        0.332,
        0.302,
        0.276,
        0.256,
        0.236,
        0.216,
        0.198,
        0.180,
        0.159,
        0.137,
        0.120,
        0.107,
        0.090,
    ]

    q_acc_dons = (1 - np.array(mu)) / np.array(mu)

    return {"q_acc_don_array": q_acc_dons, "rtid_array": rmax}


####
def rmin_ulrich_burger(mass_donor, mass_accretor):
    """
    Function to calculate the minimum radius of approach of the stream

    expressed in terms of the separation of the binary
    """

    rmin = 0.0425 * np.power(
        (mass_accretor / mass_donor) + (np.power(mass_accretor, 2) / mass_donor),
        1.0 / 4.0,
    )

    return rmin


def rmin_ulrich_burger_binaryc_version(mass_donor, mass_accretor):
    """
    Function to calculate the minimum radius of approach of the stream

    expressed in terms of the separation of the binary
    """

    rmin = 0.0425 * np.power(
        (mass_accretor / mass_donor) + np.power(mass_accretor / mass_donor, 2),
        1.0 / 4.0,
    )

    return rmin


####
def rtid_fkr(mass_donor, mass_accretor):
    """
    Function to calculate the maximum radius of a circumstellar accretion disk in a binary system

    Based on the formula of FKR 5.122
    """

    return 0.9 * rochelobe_radius_accretor(
        mass_donor=mass_donor, mass_accretor=mass_accretor
    )


def rtid_warner(mass_donor, mass_accretor):
    """
    Function to calculate the maximum radius of the circumstellar accretion disk in a binary system

    based on warner 2.61. Based on paczynski 1977
    """

    massratio_accretor_donor = mass_accretor / mass_donor

    return ((0.6) / (1 + massratio_accretor_donor)) * rochelobe_radius_accretor(
        mass_donor=mass_donor, mass_accretor=mass_accretor
    )


####
def calc_beta_min(mass_donor, mass_accretor, radius_accretor):
    """
    Function to return the minimum beta that we can have at a given location.
    """

    rcirc = 1.7 * rmin_ulrich_burger_binaryc_version(
        mass_donor=mass_donor, mass_accretor=mass_accretor
    )

    rwind_max = 0.9 * rochelobe_radius_accretor(
        mass_donor=mass_donor, mass_accretor=mass_accretor
    )

    beta_min = (np.sqrt(rwind_max) - np.sqrt(rcirc)) / (
        np.sqrt(rwind_max) - np.sqrt(radius_accretor)
    )

    return beta_min


def nans(shape, dtype=float):
    a = np.empty(shape, dtype)
    a.fill(np.nan)
    return a


def calc_fraction_returned_conservative(mass_donor, mass_accretor, radius_accretor):
    """
    function to calculate the fraction of the angular momentum of the stream that is returned to the orbit in the conservative case
    """

    rcirc = 1.7 * rmin_ulrich_burger_binaryc_version(
        mass_donor=mass_donor, mass_accretor=mass_accretor
    )

    fraction_returned = 1 - np.sqrt(radius_accretor / rcirc)

    return fraction_returned


def calc_pdot_p_conservative(mass_donor, mass_accretor, radius_accretor):
    """
    Function to calculate pdot/p in the conservative case. pdot/p is expressed in terms of (mdot stream/md)

    using mdot stream flips the sign. so we express it in terms of the mass transfer rate. not the mass loss rate of the donor.
    """

    q_acc_don = mass_accretor / mass_donor

    rcirc = 1.7 * rmin_ulrich_burger_binaryc_version(
        mass_donor=mass_donor, mass_accretor=mass_accretor
    )

    pdot_p = 3 * (
        1
        - (1 / q_acc_don)
        + np.sqrt(rcirc)
        * np.sqrt(1 + (1 / q_acc_don))
        * (1 - np.sqrt(radius_accretor / rcirc))
    )

    return pdot_p


#####
def plot_radii(
    min_log10_q_acc_don=-2,
    max_log10_q_acc_don=2,
    q_acc_don_stepsize=0.1,
    plot_settings=None,
):
    """
    Function to plot the radii that are relevant to the project
    """

    if not plot_settings:
        plot_settings = {}

    # #
    # min_log10_q_acc_don = -2
    # max_log10_q_acc_don = 2
    # q_acc_don_stepsize = 0.1

    # Set the arrays of accretor and donor
    massratio_accretor_donor_array = 10 ** np.arange(
        min_log10_q_acc_don, max_log10_q_acc_don, q_acc_don_stepsize
    )

    mass_accretor_array = np.ones(massratio_accretor_donor_array.shape)
    mass_donor_array = mass_accretor_array / massratio_accretor_donor_array

    # Calculate the radi
    rochelobe_radius_accretor_array = rochelobe_radius_accretor(
        mass_donor=mass_donor_array, mass_accretor=mass_accretor_array
    )
    r_disk_out_array = 0.9 * rochelobe_radius_accretor_array

    lubow_shu_values = return_lubow_shu_values()

    #
    rmin_ulrich_burger_array = rmin_ulrich_burger(
        mass_donor=mass_donor_array, mass_accretor=mass_accretor_array
    )
    rmin_ulrich_burger_binaryc_version_array = rmin_ulrich_burger_binaryc_version(
        mass_donor=mass_donor_array, mass_accretor=mass_accretor_array
    )
    rmin_fkr_array = rmin_fkr(
        mass_donor=mass_donor_array, mass_accretor=mass_accretor_array
    )

    #
    rcirc_ulrich_burger_array = 1.7 * rmin_ulrich_burger_array
    rcirc_ulrich_burger_binaryc_version_array = (
        1.7 * rmin_ulrich_burger_binaryc_version_array
    )
    rcirc_fkr_array = rcirc_fkr(
        mass_donor=mass_donor_array, mass_accretor=mass_accretor_array
    )

    plot_alpha = 0.5

    ###
    # Plot
    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(16, 16))

    #
    axes.plot(
        massratio_accretor_donor_array,
        rochelobe_radius_accretor_array,
        label=r"$r_{\mathrm{RL,\ acc}}$",
    )
    axes.plot(
        massratio_accretor_donor_array, r_disk_out_array, label=r"$r_{disk,\ out}$"
    )

    (rmin_line,) = axes.plot(
        massratio_accretor_donor_array,
        rmin_ulrich_burger_array,
        label=r"$r_{\mathrm{min,\ Ulrich\ burger}}$",
    )
    axes.plot(
        massratio_accretor_donor_array,
        rmin_ulrich_burger_binaryc_version_array,
        label=r"$r_{\mathrm{min,\ Ulrich\ burger\ binaryc\ version}}$",
        linestyle="--",
        color=rmin_line.get_color(),
        alpha=plot_alpha,
    )
    axes.plot(
        massratio_accretor_donor_array,
        rmin_fkr_array,
        label=r"$r_{\mathrm{min,\ FKR\ version}}$",
        linestyle="-.",
        color=rmin_line.get_color(),
        alpha=plot_alpha,
    )

    (rcirc_line,) = axes.plot(
        massratio_accretor_donor_array,
        rcirc_ulrich_burger_array,
        label=r"$r_{\mathrm{circ,\ Ulrich\ burger}}$",
    )
    axes.plot(
        massratio_accretor_donor_array,
        rcirc_ulrich_burger_binaryc_version_array,
        label=r"$r_{\mathrm{circ,\ Ulrich\ burger\ binaryc\ version}}$",
        linestyle="--",
        color=rcirc_line.get_color(),
        alpha=plot_alpha,
    )
    axes.plot(
        massratio_accretor_donor_array,
        rcirc_fkr_array,
        label=r"$r_{\mathrm{circ,\ FKR\ version}}$",
        linestyle="-.",
        color=rcirc_line.get_color(),
        alpha=plot_alpha,
    )

    # Lubow shu values
    axes.scatter(
        lubow_shu_values["q_acc_don_array"],
        lubow_shu_values["rmin_array"],
        s=80,
        label=r"Lubow\&Shu 1975 r$_{min}$",
    )
    axes.scatter(
        lubow_shu_values["q_acc_don_array"],
        lubow_shu_values["rcirc_array"],
        s=80,
        label=r"Lubow\&Shu 1975 r$_{circ}$",
    )

    #
    axes.legend()

    axes.set_xscale("log")
    axes.set_yscale("log")

    axes.set_xlabel(r"$q_{\mathrm{acc}}$")
    axes.set_ylabel(r"$r/a$")

    axes.set_title("Relevant radii during mass transfer")

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


def plot_tidal_radii(
    min_log10_q_acc_don=-2,
    max_log10_q_acc_don=2,
    q_acc_don_stepsize=0.1,
    plot_settings=None,
):
    """
    Function to plot the radii that are relevant to the project
    """

    if not plot_settings:
        plot_settings = {}

    # #
    # min_log10_q_acc_don = -2
    # max_log10_q_acc_don = 2
    # q_acc_don_stepsize = 0.1

    # Set the arrays of accretor and donor
    massratio_accretor_donor_array = 10 ** np.arange(
        min_log10_q_acc_don, max_log10_q_acc_don, q_acc_don_stepsize
    )

    mass_accretor_array = np.ones(massratio_accretor_donor_array.shape)
    mass_donor_array = mass_accretor_array / massratio_accretor_donor_array

    # Calculate the radi
    rochelobe_radius_accretor_array = rochelobe_radius_accretor(
        mass_donor=mass_donor_array, mass_accretor=mass_accretor_array
    )

    rtid_fkr_array = rtid_fkr(
        mass_donor=mass_donor_array, mass_accretor=mass_accretor_array
    )
    rtid_warner_array = rtid_warner(
        mass_donor=mass_donor_array, mass_accretor=mass_accretor_array
    )

    lubow_shu_values = return_lubow_shu_values()
    paczynski_values = return_paczsynski_values()

    #
    rmin_ulrich_burger_binaryc_version_array = rmin_ulrich_burger_binaryc_version(
        mass_donor=mass_donor_array, mass_accretor=mass_accretor_array
    )

    #
    rcirc_ulrich_burger_binaryc_version_array = (
        1.7 * rmin_ulrich_burger_binaryc_version_array
    )

    plot_alpha = 0.5

    ###
    # Plot
    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(16, 16))

    #
    axes.plot(
        massratio_accretor_donor_array,
        rochelobe_radius_accretor_array,
        label=r"$r_{\mathrm{RL,\ acc}}$",
    )
    axes.plot(
        massratio_accretor_donor_array,
        rtid_fkr_array,
        label=r"$r_{disk,\ out\ (0.9RL,\ FKR)}$",
    )
    axes.plot(
        massratio_accretor_donor_array,
        rtid_warner_array,
        label=r"$r_{disk,\ out\ (0.9RL,\ warner)}$",
    )

    axes.plot(
        massratio_accretor_donor_array,
        rmin_ulrich_burger_binaryc_version_array,
        label=r"$r_{\mathrm{min,\ Ulrich\ burger\ binaryc\ version}}$",
        linestyle="--",
        # color=rmin_line.get_color()
        alpha=plot_alpha,
    )

    axes.plot(
        massratio_accretor_donor_array,
        rcirc_ulrich_burger_binaryc_version_array,
        label=r"$r_{\mathrm{circ,\ Ulrich\ burger\ binaryc\ version}}$",
        linestyle="--",
        # color=rcirc_line.get_color()
        alpha=plot_alpha,
    )

    # Paczynski values
    axes.scatter(
        paczynski_values["q_acc_don_array"],
        paczynski_values["rtid_array"],
        s=80,
        label=r"Paczynski 1977 r$_{tid}$",
    )

    # Lubow shu values
    axes.scatter(
        lubow_shu_values["q_acc_don_array"],
        lubow_shu_values["rmin_array"],
        s=80,
        label=r"Lubow\&Shu 1975 r$_{min}$",
    )
    axes.scatter(
        lubow_shu_values["q_acc_don_array"],
        lubow_shu_values["rcirc_array"],
        s=80,
        label=r"Lubow\&Shu 1975 r$_{circ}$",
    )

    #
    axes.legend()

    axes.set_xscale("log")
    axes.set_yscale("log")

    axes.set_xlabel(r"$q_{\mathrm{acc}}$")
    axes.set_ylabel(r"$r/a$")

    axes.set_title("Relevant radii during mass transfer")

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


# Plot minimum fraction to be lost at outer edge
def plot_beta_min(
    min_log10_q_acc_don=-2,
    max_log10_q_acc_don=2,
    min_log10_racc_sep=-4,
    max_log10_racc_sep=0,
    plot_settings=None,
):
    """
    Function to plot the minimum beta
    """

    if not plot_settings:
        plot_settings = {}

    # Set bounds and stepsize
    log10q_acc_don_stepsize = 0.01
    log10_racc_sep_stepsize = 0.01

    # Set up arrays
    q_accretor_donor_array = 10 ** np.arange(
        min_log10_q_acc_don, max_log10_q_acc_don, log10q_acc_don_stepsize
    )
    racc_sep_array = 10 ** np.arange(
        min_log10_racc_sep, max_log10_racc_sep, log10_racc_sep_stepsize
    )

    # Set up meshgrid
    X_racc_sep_array, Y_q_accretor_donor_array = np.meshgrid(
        racc_sep_array, q_accretor_donor_array
    )
    X_racc_sep_array_flat = np.ravel(X_racc_sep_array)
    Y_q_accretor_donor_array_flat = np.ravel(Y_q_accretor_donor_array)

    # Create beta_array
    # beta_min_array = np.zeros(Y_q_accretor_donor_array.shape)
    beta_min_array = nans(Y_q_accretor_donor_array.shape)
    beta_min_array_flat = np.ravel(beta_min_array)

    # TODO: improve this
    # Loop over the flattened arrays and impose limits
    # The requirements are: r accretor < rmin
    for i in range(len(Y_q_accretor_donor_array_flat)):
        mass_accretor = 1
        mass_donor = 1 / Y_q_accretor_donor_array_flat[i]

        rmin = rmin_ulrich_burger_binaryc_version(mass_donor, mass_accretor)
        rcirc = 1.7 * rmin_ulrich_burger_binaryc_version(
            mass_donor=mass_donor, mass_accretor=mass_accretor
        )
        rout = 0.9 * rochelobe_radius_accretor(
            mass_donor=mass_donor, mass_accretor=mass_accretor
        )

        if (X_racc_sep_array_flat[i] < rmin) and (rcirc < rout):
            beta_min = calc_beta_min(
                mass_donor, mass_accretor, X_racc_sep_array_flat[i]
            )
            beta_min_array_flat[i] = beta_min

    # reshape the array
    beta_min_array = np.reshape(beta_min_array_flat, beta_min_array.shape)

    fraction_to_lose = nans(Y_q_accretor_donor_array.shape)
    fraction_to_lose[~np.isnan(beta_min_array)] = (
        1 - beta_min_array[~np.isnan(beta_min_array)]
    )

    #
    min_plot_value = 0
    max_plot_value = 1

    #
    fig = plt.figure(figsize=(16, 10))

    #
    gs = fig.add_gridspec(nrows=1, ncols=11)

    # create axes
    ax_plot = fig.add_subplot(gs[0, 0:9])

    # Create colorbar
    ax_cb = fig.add_subplot(gs[0, 10])

    # plot values
    ax_plot.pcolormesh(
        X_racc_sep_array,
        Y_q_accretor_donor_array,
        fraction_to_lose,
        cmap=matplotlib.cm.viridis,
        vmin=min_plot_value,
        vmax=max_plot_value,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    #
    contour_levels = [0.25, 0.33, 0.5, 0.9]
    linestyles = ["solid", "dashed", "-.", ":"]
    CS_mt_region_1 = ax_plot.contour(
        X_racc_sep_array,
        Y_q_accretor_donor_array,
        fraction_to_lose,
        levels=contour_levels,
        colors="k",
        linestyles=linestyles,
    )

    ax_plot.set_yscale("log")
    ax_plot.set_xscale("log")

    ax_plot.set_ylabel(r"$q_{\mathrm{acc}}$", fontsize=fontsize_labels)
    ax_plot.set_xlabel(r"$r_{\mathrm{acc}}/a$", fontsize=fontsize_labels)

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        cmap=matplotlib.cm.viridis,
        norm=matplotlib.colors.Normalize(vmin=min_plot_value, vmax=max_plot_value),
    )
    cbar.ax.set_ylabel(
        r"Fraction of $\dot{M}_{\mathrm{stream}}$ to lose at outer"
        + "\nedge to remove excess angular momentum"
    )

    for contour_i in range(len(contour_levels)):
        cbar.ax.plot(
            [0, 1],
            [contour_levels[contour_i]] * 2,
            "black",
            linestyle=linestyles[contour_i],
        )

    fig.tight_layout()

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


# Plot the fraction of angular momentum of the mass stream that is returned to the orbit
def plot_fraction_returned(
    min_log10_q_acc_don=-2,
    max_log10_q_acc_don=2,
    min_log10_racc_sep=-4,
    max_log10_racc_sep=0,
    plot_settings=None,
):
    """
    Function to plot the fraction of angular momentum of the stream that is returned to the orbit
    """

    if not plot_settings:
        plot_settings = {}

    contour_levels = [0.25, 0.33, 0.5, 0.9]
    linestyles = ["solid", "dashed", "-.", ":"]

    # Set bounds and stepsize
    log10q_acc_don_stepsize = 0.005
    log10_racc_sep_stepsize = 0.005

    # Set up arrays
    q_accretor_donor_array = 10 ** np.arange(
        min_log10_q_acc_don, max_log10_q_acc_don, log10q_acc_don_stepsize
    )
    racc_sep_array = 10 ** np.arange(
        min_log10_racc_sep, max_log10_racc_sep, log10_racc_sep_stepsize
    )

    # Set up meshgrid
    X_racc_sep_array, Y_q_accretor_donor_array = np.meshgrid(
        racc_sep_array, q_accretor_donor_array
    )
    X_racc_sep_array_flat = np.ravel(X_racc_sep_array)
    Y_q_accretor_donor_array_flat = np.ravel(Y_q_accretor_donor_array)

    # Create beta_array
    # beta_min_array = np.zeros(Y_q_accretor_donor_array.shape)
    fraction_returned_array = nans(Y_q_accretor_donor_array.shape)
    fraction_returned_array_flat = np.ravel(fraction_returned_array)

    # TODO: improve this
    # Loop over the flattened arrays and impose limits
    # The requirements are: r accretor < rmin
    for i in range(len(Y_q_accretor_donor_array_flat)):
        mass_accretor = 1
        mass_donor = 1 / Y_q_accretor_donor_array_flat[i]

        rmin = rmin_ulrich_burger_binaryc_version(mass_donor, mass_accretor)

        if X_racc_sep_array_flat[i] < rmin:
            fraction_returned = calc_fraction_returned_conservative(
                mass_donor, mass_accretor, X_racc_sep_array_flat[i]
            )
            fraction_returned_array_flat[i] = fraction_returned

    # reshape the array
    fraction_returned_array = np.reshape(
        fraction_returned_array_flat, fraction_returned_array.shape
    )

    min_plot_value = 0
    max_plot_value = 1

    ################
    # Plotting
    fig = plt.figure(figsize=(16, 10))

    #
    gs = fig.add_gridspec(nrows=1, ncols=11)

    # create axes
    ax_plot = fig.add_subplot(gs[0, 0:9])

    # Create colorbar
    ax_cb = fig.add_subplot(gs[0, 10])

    # plot values
    ax_plot.pcolormesh(
        X_racc_sep_array,
        Y_q_accretor_donor_array,
        fraction_returned_array,
        cmap=matplotlib.cm.viridis,
        vmin=min_plot_value,
        vmax=max_plot_value,
        antialiased=True,
        rasterized=True,
    )

    # Create contour
    CS_mt_region_1 = ax_plot.contour(
        X_racc_sep_array,
        Y_q_accretor_donor_array,
        fraction_returned_array,
        levels=contour_levels,
        colors="k",
        linestyles=linestyles,
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        cmap=matplotlib.cm.viridis,
        norm=matplotlib.colors.Normalize(vmin=min_plot_value, vmax=max_plot_value),
    )
    cbar.ax.set_ylabel(r"Fraction of $\dot{J}_{\mathrm{stream}}$ returned to the orbit")

    for contour_i in range(len(contour_levels)):
        cbar.ax.plot(
            [0, 1],
            [contour_levels[contour_i]] * 2,
            "black",
            linestyle=linestyles[contour_i],
        )

    # Some make up
    ax_plot.set_yscale("log")
    ax_plot.set_xscale("log")

    ax_plot.set_ylabel(r"$q_{\mathrm{acc}}$", fontsize=fontsize_labels)
    ax_plot.set_xlabel(r"$r_{\mathrm{acc}}/a$", fontsize=fontsize_labels)

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


# Plot general pdot/p for conservative MT (disk torque and normal effect)
def plot_pdot_p_conservative(
    min_log10_q_acc_don=-2,
    max_log10_q_acc_don=2,
    min_log10_racc_sep=-4,
    max_log10_racc_sep=0,
    plot_settings=None,
):
    """
    Function to plot the minimum beta
    """

    if not plot_settings:
        plot_settings = {}

    # Set bounds and stepsize
    log10q_acc_don_stepsize = 0.005
    log10_racc_sep_stepsize = 0.005

    # Set up arrays
    q_accretor_donor_array = 10 ** np.arange(
        min_log10_q_acc_don, max_log10_q_acc_don, log10q_acc_don_stepsize
    )
    racc_sep_array = 10 ** np.arange(
        min_log10_racc_sep, max_log10_racc_sep, log10_racc_sep_stepsize
    )

    # Set up meshgrid
    X_racc_sep_array, Y_q_accretor_donor_array = np.meshgrid(
        racc_sep_array, q_accretor_donor_array
    )
    X_racc_sep_array_flat = np.ravel(X_racc_sep_array)
    Y_q_accretor_donor_array_flat = np.ravel(Y_q_accretor_donor_array)

    # Create beta_array
    # beta_min_array = np.zeros(Y_q_accretor_donor_array.shape)
    pdot_p_array = nans(Y_q_accretor_donor_array.shape)
    pdot_p_array_flat = np.ravel(pdot_p_array)

    # TODO: improve this
    # Loop over the flattened arrays and impose limits
    # The requirements are: r accretor < rmin
    for i in range(len(Y_q_accretor_donor_array_flat)):
        mass_accretor = 1
        mass_donor = 1 / Y_q_accretor_donor_array_flat[i]

        rmin = rmin_ulrich_burger_binaryc_version(mass_donor, mass_accretor)

        if X_racc_sep_array_flat[i] < rmin:
            pdot_p = calc_pdot_p_conservative(
                mass_donor, mass_accretor, X_racc_sep_array_flat[i]
            )
            pdot_p_array_flat[i] = pdot_p

    # reshape the array
    pdot_p_array = np.reshape(pdot_p_array_flat, pdot_p_array.shape)

    min_plot_value = pdot_p_array[~np.isnan(pdot_p_array)].min()
    max_plot_value = pdot_p_array[~np.isnan(pdot_p_array)].max()

    #############
    # Plot the data
    fig = plt.figure(figsize=(16, 10))

    #
    gs = fig.add_gridspec(nrows=1, ncols=11)

    # create axes
    ax_plot = fig.add_subplot(gs[0, 0:9])

    # Create colorbar
    ax_cb = fig.add_subplot(gs[0, 10])

    # plot values
    ax_plot.pcolormesh(
        X_racc_sep_array,
        Y_q_accretor_donor_array,
        pdot_p_array,
        cmap=matplotlib.cm.viridis,
        norm=colors.SymLogNorm(
            linthresh=LINTHRESH,
            linscale=LINSCALE,
            vmin=min_plot_value,
            vmax=max_plot_value,
        ),
        antialiased=True,
        rasterized=True,
    )

    # Contourlevels
    contour_levels = [-1e1, -1e0, 1e0, 1e1]
    linestyles = ["solid", "dashed", "-.", ":"]
    CS_mt_region_1 = ax_plot.contour(
        X_racc_sep_array,
        Y_q_accretor_donor_array,
        pdot_p_array,
        levels=contour_levels,
        colors="k",
        linestyles=linestyles,
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        cmap=matplotlib.cm.viridis,
        norm=colors.SymLogNorm(
            linthresh=LINTHRESH, linscale=1, vmin=min_plot_value, vmax=max_plot_value
        ),
    )
    cbar.ax.set_ylabel(r"$\dot{P}/P$")
    # for contour_i in range(len(contour_levels)):
    #     cbar.ax.plot([0, 1], [contour_levels[contour_i]]*2, 'black', linestyle=linestyles[contour_i])

    #
    ax_plot.set_yscale("log")
    ax_plot.set_xscale("log")

    ax_plot.set_ylabel(r"$q_{\mathrm{acc}}$", fontsize=fontsize_labels)
    ax_plot.set_xlabel(r"$r_{\mathrm{acc}}/a$", fontsize=fontsize_labels)

    fig.tight_layout()

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


# Plot pdot/p caused by disk torque only for conservative MT
def plot_pdot_p_conservative_disk_torque_only(
    min_log10_q_acc_don=-2,
    max_log10_q_acc_don=2,
    min_log10_racc_sep=-4,
    max_log10_racc_sep=0,
    plot_settings=None,
):
    """
    Function to plot the minimum beta
    """

    if not plot_settings:
        plot_settings = {}

    # Set bounds and stepsize
    log10q_acc_don_stepsize = 0.005
    log10_racc_sep_stepsize = 0.005

    # Set up arrays
    q_accretor_donor_array = 10 ** np.arange(
        min_log10_q_acc_don, max_log10_q_acc_don, log10q_acc_don_stepsize
    )
    racc_sep_array = 10 ** np.arange(
        min_log10_racc_sep, max_log10_racc_sep, log10_racc_sep_stepsize
    )

    # Set up meshgrid
    X_racc_sep_array, Y_q_accretor_donor_array = np.meshgrid(
        racc_sep_array, q_accretor_donor_array
    )
    X_racc_sep_array_flat = np.ravel(X_racc_sep_array)
    Y_q_accretor_donor_array_flat = np.ravel(Y_q_accretor_donor_array)

    # Create beta_array
    # beta_min_array = np.zeros(Y_q_accretor_donor_array.shape)
    pdot_p_disk_torque_only_array = nans(Y_q_accretor_donor_array.shape)
    pdot_p_disk_torque_only_array_flat = np.ravel(pdot_p_disk_torque_only_array)

    # TODO: improve this
    # Loop over the flattened arrays and impose limits
    # The requirements are: r accretor < rmin
    for i in range(len(Y_q_accretor_donor_array_flat)):
        mass_accretor = 1
        mass_donor = 1 / Y_q_accretor_donor_array_flat[i]

        rmin = rmin_ulrich_burger_binaryc_version(mass_donor, mass_accretor)
        rcirc = 1.7 * rmin_ulrich_burger_binaryc_version(
            mass_donor=mass_donor, mass_accretor=mass_accretor
        )

        if X_racc_sep_array_flat[i] < rmin:
            pdot_p_disk_torque_only = calc_pdot_p_conservative(
                mass_donor, mass_accretor, X_racc_sep_array_flat[i]
            ) - calc_pdot_p_conservative(mass_donor, mass_accretor, rcirc)
            pdot_p_disk_torque_only_array_flat[i] = pdot_p_disk_torque_only

    # reshape the array
    pdot_p_disk_torque_only_array = np.reshape(
        pdot_p_disk_torque_only_array_flat, pdot_p_disk_torque_only_array.shape
    )

    min_plot_value = pdot_p_disk_torque_only_array[
        ~np.isnan(pdot_p_disk_torque_only_array)
    ].min()
    max_plot_value = pdot_p_disk_torque_only_array[
        ~np.isnan(pdot_p_disk_torque_only_array)
    ].max()

    #
    fig = plt.figure(figsize=(16, 10))

    #
    gs = fig.add_gridspec(nrows=1, ncols=11)

    # create axes
    ax_plot = fig.add_subplot(gs[0, 0:9])

    # Create colorbar
    ax_cb = fig.add_subplot(gs[0, 10])

    # plot values
    ax_plot.pcolormesh(
        X_racc_sep_array,
        Y_q_accretor_donor_array,
        pdot_p_disk_torque_only_array,
        cmap=matplotlib.cm.viridis,
        # vmin=min_plot_value,
        # vmax=max_plot_value,
        norm=colors.LogNorm(
            # linthresh=LINTHRESH,
            # linscale=LINSCALE,
            vmin=min_plot_value,
            vmax=max_plot_value,
        ),
        antialiased=True,
        rasterized=True,
    )

    ax_plot.set_yscale("log")
    ax_plot.set_xscale("log")

    ax_plot.set_ylabel(r"$q_{\mathrm{acc}}$", fontsize=fontsize_labels)
    ax_plot.set_xlabel(r"$r_{\mathrm{acc}}/a$", fontsize=fontsize_labels)

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        cmap=matplotlib.cm.viridis,
        # norm=matplotlib.colors.Normalize(vmin=min_plot_value, vmax=max_plot_value),
        norm=colors.LogNorm(
            # linthresh=LINTHRESH,
            # linscale=LINSCALE,
            vmin=min_plot_value,
            vmax=max_plot_value,
        ),
    )
    cbar.ax.set_ylabel(r"$\dot{P}/P$")

    fig.tight_layout()

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


# Plot pdot/p caused by MT without disk torque for conservative MT
def plot_pdot_p_conservative_without_disk_torque(
    min_log10_q_acc_don=-2,
    max_log10_q_acc_don=2,
    min_log10_racc_sep=-4,
    max_log10_racc_sep=0,
    plot_settings=None,
):
    """
    Function to plot the minimum beta
    """

    if not plot_settings:
        plot_settings = {}

    # Set bounds and stepsize
    log10q_acc_don_stepsize = 0.005
    log10_racc_sep_stepsize = 0.005

    # Set up arrays
    q_accretor_donor_array = 10 ** np.arange(
        min_log10_q_acc_don, max_log10_q_acc_don, log10q_acc_don_stepsize
    )
    racc_sep_array = 10 ** np.arange(
        min_log10_racc_sep, max_log10_racc_sep, log10_racc_sep_stepsize
    )

    # Set up meshgrid
    X_racc_sep_array, Y_q_accretor_donor_array = np.meshgrid(
        racc_sep_array, q_accretor_donor_array
    )
    X_racc_sep_array_flat = np.ravel(X_racc_sep_array)
    Y_q_accretor_donor_array_flat = np.ravel(Y_q_accretor_donor_array)

    # Create beta_array
    # beta_min_array = np.zeros(Y_q_accretor_donor_array.shape)
    pdot_p_without_disk_torque_array = nans(Y_q_accretor_donor_array.shape)
    pdot_p_without_disk_torque_array_flat = np.ravel(pdot_p_without_disk_torque_array)

    # TODO: improve this
    # Loop over the flattened arrays and impose limits
    # The requirements are: r accretor < rmin
    for i in range(len(Y_q_accretor_donor_array_flat)):
        mass_accretor = 1
        mass_donor = 1 / Y_q_accretor_donor_array_flat[i]

        rmin = rmin_ulrich_burger_binaryc_version(mass_donor, mass_accretor)
        rcirc = 1.7 * rmin_ulrich_burger_binaryc_version(
            mass_donor=mass_donor, mass_accretor=mass_accretor
        )

        if X_racc_sep_array_flat[i] < rmin:
            pdot_p_without_disk_torque = calc_pdot_p_conservative(
                mass_donor, mass_accretor, rcirc
            )
            pdot_p_without_disk_torque_array_flat[i] = pdot_p_without_disk_torque

    # reshape the array
    pdot_p_without_disk_torque_array = np.reshape(
        pdot_p_without_disk_torque_array_flat, pdot_p_without_disk_torque_array.shape
    )

    min_plot_value = pdot_p_without_disk_torque_array[
        ~np.isnan(pdot_p_without_disk_torque_array)
    ].min()
    max_plot_value = pdot_p_without_disk_torque_array[
        ~np.isnan(pdot_p_without_disk_torque_array)
    ].max()

    #
    fig = plt.figure(figsize=(16, 10))

    #
    gs = fig.add_gridspec(nrows=1, ncols=11)

    # create axes
    ax_plot = fig.add_subplot(gs[0, 0:9])

    # Create colorbar
    ax_cb = fig.add_subplot(gs[0, 10])

    # plot values
    ax_plot.pcolormesh(
        X_racc_sep_array,
        Y_q_accretor_donor_array,
        pdot_p_without_disk_torque_array,
        cmap=matplotlib.cm.viridis,
        norm=colors.SymLogNorm(
            linthresh=LINTHRESH,
            linscale=LINSCALE,
            vmin=min_plot_value,
            vmax=max_plot_value,
        ),
        antialiased=True,
        rasterized=True,
    )

    #
    contour_levels = [-1e1, -1e0, 1e0, 1e1]
    linestyles = ["solid", "dashed", "-.", ":"]
    CS_mt_region_1 = ax_plot.contour(
        X_racc_sep_array,
        Y_q_accretor_donor_array,
        pdot_p_without_disk_torque_array,
        levels=contour_levels,
        colors="k",
        linestyles=linestyles,
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        cmap=matplotlib.cm.viridis,
        norm=colors.SymLogNorm(
            linthresh=LINTHRESH,
            linscale=LINSCALE,
            vmin=min_plot_value,
            vmax=max_plot_value,
        ),
    )
    cbar.ax.set_ylabel(r"$\dot{P}/P$")

    # Some make up
    ax_plot.set_yscale("log")
    ax_plot.set_xscale("log")

    ax_plot.set_ylabel(r"$q_{\mathrm{acc}}$", fontsize=fontsize_labels)
    ax_plot.set_xlabel(r"$r_{\mathrm{acc}}/a$", fontsize=fontsize_labels)

    fig.tight_layout()

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


# Plot minimum fraction to be lost at outer edge
def plot_pdot_p_conservative_fraction_disk_torque(
    min_log10_q_acc_don=-2,
    max_log10_q_acc_don=2,
    min_log10_racc_sep=-4,
    max_log10_racc_sep=0,
    plot_settings=None,
):
    """
    Function to plot the minimum beta
    """

    if not plot_settings:
        plot_settings = {}

    # Set bounds and stepsize
    log10q_acc_don_stepsize = 0.05
    log10_racc_sep_stepsize = 0.05

    # Set up arrays
    q_accretor_donor_array = 10 ** np.arange(
        min_log10_q_acc_don, max_log10_q_acc_don, log10q_acc_don_stepsize
    )
    racc_sep_array = 10 ** np.arange(
        min_log10_racc_sep, max_log10_racc_sep, log10_racc_sep_stepsize
    )

    # Set up meshgrid
    X_racc_sep_array, Y_q_accretor_donor_array = np.meshgrid(
        racc_sep_array, q_accretor_donor_array
    )
    X_racc_sep_array_flat = np.ravel(X_racc_sep_array)
    Y_q_accretor_donor_array_flat = np.ravel(Y_q_accretor_donor_array)

    # Create beta_array
    # beta_min_array = np.zeros(Y_q_accretor_donor_array.shape)
    pdot_p_fractional_diff_array = nans(Y_q_accretor_donor_array.shape)
    pdot_p_fractional_diff_array_flat = np.ravel(pdot_p_fractional_diff_array)

    # TODO: improve this
    # Loop over the flattened arrays and impose limits
    # The requirements are: r accretor < rmin
    for i in range(len(Y_q_accretor_donor_array_flat)):
        mass_accretor = 1
        mass_donor = 1 / Y_q_accretor_donor_array_flat[i]

        rmin = rmin_ulrich_burger_binaryc_version(mass_donor, mass_accretor)
        rcirc = 1.7 * rmin_ulrich_burger_binaryc_version(
            mass_donor=mass_donor, mass_accretor=mass_accretor
        )

        if X_racc_sep_array_flat[i] < rmin:
            pdot_p_fractional_diff = (
                calc_pdot_p_conservative(
                    mass_donor, mass_accretor, X_racc_sep_array_flat[i]
                )
                - calc_pdot_p_conservative(mass_donor, mass_accretor, rcirc)
            ) / calc_pdot_p_conservative(
                mass_donor, mass_accretor, X_racc_sep_array_flat[i]
            )
            pdot_p_fractional_diff_array_flat[i] = pdot_p_fractional_diff

    # reshape the array
    pdot_p_fractional_diff_array = np.reshape(
        pdot_p_fractional_diff_array_flat, pdot_p_fractional_diff_array.shape
    )

    # Made it absolute
    pdot_p_fractional_diff_array = np.abs(pdot_p_fractional_diff_array)

    min_plot_value = pdot_p_fractional_diff_array[
        ~np.isnan(pdot_p_fractional_diff_array)
    ].min()
    max_plot_value = pdot_p_fractional_diff_array[
        ~np.isnan(pdot_p_fractional_diff_array)
    ].max()

    #
    fig = plt.figure(figsize=(16, 10))

    #
    gs = fig.add_gridspec(nrows=1, ncols=11)

    # create axes
    ax_plot = fig.add_subplot(gs[0, 0:9])

    # Create colorbar
    ax_cb = fig.add_subplot(gs[0, 10])

    # plot values
    ax_plot.pcolormesh(
        X_racc_sep_array,
        Y_q_accretor_donor_array,
        pdot_p_fractional_diff_array,
        cmap=matplotlib.cm.viridis,
        norm=matplotlib.colors.LogNorm(vmin=min_plot_value, vmax=max_plot_value),
        antialiased=True,
        rasterized=True,
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        cmap=matplotlib.cm.viridis,
        # norm=matplotlib.colors.Normalize(vmin=min_plot_value, vmax=max_plot_value),
        norm=matplotlib.colors.LogNorm(vmin=min_plot_value, vmax=max_plot_value),
    )
    cbar.ax.set_ylabel(r"$\dot{P}/P$")

    ax_plot.set_yscale("log")
    ax_plot.set_xscale("log")

    ax_plot.set_ylabel(r"q$_{\mathrm{acc}}$", fontsize=fontsize_labels)
    ax_plot.set_xlabel(r"r$_{\mathrm{acc}}/a$", fontsize=fontsize_labels)

    fig.tight_layout()

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)
