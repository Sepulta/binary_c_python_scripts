"""
Example script to use some of the ensemble function utility

The scripts contained in ensemble_functions are complementary to the binarycpython/ensemble functions but its easier to just provide these functions now

TODO: the inflate_ensemble might be better used with just lists of lists. In that way the dataframes are not forced and it will take less time to combine them.
"""

import json

import pandas as pd
import numpy as np
from collections import OrderedDict

from plot_functions import plot_yield_per_channel
from ensemble_functions.ensemble_functions import (
    inflate_ensemble_with_lists,
    find_columnames_recursively,
)

import matplotlib.pyplot as plt


# Load ensemble data
ensemble_datafile = "ensemble_output-100.json"
with open(ensemble_datafile, "r") as f:
    ensemble_data = json.loads(f.read())["ensemble"]

# print(ensemble_data.keys())
yield_data = ensemble_data["Xyield"]


# An example of reading out the channels for each time and summing all the yields:
# plot_yield_per_channel(yield_data)

# An example of inflating the ensemble:
# First I inflate it to a list of lists


def plot_by_inflation(yield_data):
    """
    method to plot with the inflation and dataframes
    """

    data_list = inflate_ensemble_with_lists(yield_data)

    # Load it as an array and rotate
    data_array = np.array(data_list).T

    # I fetch the names of the columns like so:
    columnames = find_columnames_recursively(yield_data)

    # Which I update with a final column name
    columnames = columnames + ["yield_per_solarmass"]

    # Then I load it into a dataframe
    df = pd.DataFrame(data_array, columns=columnames)

    # Make sure we set the correct type in the columns (because we turn the list into an array and transpose it, the whole array is filled with strings rather than a mix of both)
    # df = df.astype({"time": 'float', "yield_per_solarmass": 'float'})
    df["time"] = df["time"].astype("float")
    df["yield_per_solarmass"] = df["yield_per_solarmass"].astype("float")

    # With the df we can easily query or groupby:
    for source in df["source"].unique():
        source_df = df[df.source == source]
        grouped_by_time = source_df.groupby(by="time")["yield_per_solarmass"].sum()

        plt.plot(
            np.array(grouped_by_time.index, dtype="float"),
            np.array(grouped_by_time),
            label=source,
        )

    # Add total line
    grouped_by_time = df.groupby(by="time")["yield_per_solarmass"].sum()
    plt.plot(
        np.array(grouped_by_time.index, dtype="float"),
        np.array(grouped_by_time),
        label="total",
        linestyle="--",
    )
    plt.yscale("log")

    plt.legend()
    plt.show()


data_list = inflate_ensemble_with_lists(yield_data)

# Load it as an array and rotate
data_array = np.array(data_list).T

# I fetch the names of the columns like so:
columnames = find_columnames_recursively(yield_data)

# Which I update with a final column name
columnames = columnames + ["yield_per_solarmass"]

# Then I load it into a dataframe
df = pd.DataFrame(data_array, columns=columnames)

# Make sure we set the correct type in the columns (because we turn the list into an array and transpose it, the whole array is filled with strings rather than a mix of both)
# df = df.astype({"time": 'float', "yield_per_solarmass": 'float'})
df["time"] = df["time"].astype("float")
df["yield_per_solarmass"] = df["yield_per_solarmass"].astype("float")


def generate_element_isotope_dict(dataframe):
    """
    Function to generate the element-isotope dict, where each element is a key, and the value is the list of isotopes
    """

    # Function to generate all the isotopes that belong to a certain element:
    element_isotope_dict = {}
    for isotope in sorted(df["isotope"].unique()):
        # Strip the numbers to create the element string
        element = isotope
        while element[-1].isnumeric():
            element = element[:-1]

        # Check if isotope list is present in dict:
        if not element in element_isotope_dict:
            element_isotope_dict[element] = []

        # Add isotope to dict
        element_isotope_dict[element].append(isotope)

    return element_isotope_dict


# Generate the element isotope dict
element_isotope_dict = generate_element_isotope_dict(df)

# Create an empty dataframe to store the new results in
element_df = pd.DataFrame()

# Loop over all the elements and find the rows that have the accompanying isotopes
for element in sorted(element_isotope_dict.keys()):
    # Select the isotopes
    current_element_df = df[df["isotope"].isin(element_isotope_dict["C"])]

    # Drop the isotope column and add the element column
    del current_element_df["isotope"]
    current_element_df["element"] = element

    ## Aggregate all the lines where the time, source and element are the same (otherwise we have multiple lines with the same time,source,element)
    # take the first of each groupby, except for the yield per solarmass, which should be summed
    aggegate_dict = {el: "first" for el in current_element_df.columns}
    aggegate_dict["yield_per_solarmass"] = "sum"

    grouped_current_element_df = current_element_df.groupby(
        by=["time", "source", "element"]
    ).agg(aggegate_dict)

    # Add that to the element df
    element_df = pd.concat([element_df, grouped_current_element_df], ignore_index=True)

print(element_df)
