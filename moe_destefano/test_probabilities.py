"""
Scrip to run ensemble for CEMP calculations

TODO: Read paper Lewis
TODO: Clean up this code
TODO: Ask rob the range of metallicities and masses
TODO: Make sure that the
"""

import json
import os

from binarycpython.utils.grid import Population

test_pop = Population()
# test_pop.Moe_de_Stefano_2017(
#     options={
#         "multiplicity_modulator": [1, 0, 0,0],
#         "resolutions":
#         {
#             "M": [10, 5, 0, 0],
#             "logP": [5, 0, 0],
#             "ecc": [0, 0, 0]
#         }
#     }
# )

# Set some default values
test_pop.set(M_2=0.07, orbital_period=1e60, metallicity=0.02)
test_pop.set(
    verbosity=10,
    amt_cores=1,
)

# Set ensemble values
test_pop.set(
    # Activate ensemble
    ensemble=1,
    ensemble_defer=1,
    ensemble_filters_off=1,
    ensemble_filter_EMP=1,
    combine_ensemble_with_thread_joining=True,
)

# Short test to see if the probabilities add up:
test_pop.set(_actually_evolve_system=False)


test_pop.Moe_de_Stefano_2017(
    options={
        "multiplicity_modulator": [0, 1, 0, 0],
        "resolutions": {"M": [2, 7, 0, 0], "logP": [7, 0, 0], "ecc": [0, 0, 0]},
        "normalize_multiplicities": "raw",
    }
)
test_pop.set(data_dir="results/")

# create local tmp_dir
test_pop.set(tmp_dir=os.path.join(test_pop.custom_options["data_dir"], "local_tmp_dir"))
os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

analytics_dict = test_pop.evolve()
print(analytics_dict)
