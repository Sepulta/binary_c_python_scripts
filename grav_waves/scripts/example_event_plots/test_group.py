import pandas as pd
import numpy as np

data = [
    [1, 0, 0, 1],
    [1, 1, 0, 2],
    [2, 0, 0, 1],
    [2, 1, 0, 2],
    [3, 1, 0, 1],
    [3, 0, 0, 2],
    [4, 1, 0, 1],
    [4, 0, 0, 2],
]

#
df = pd.DataFrame(data, columns=["uuid", "stability", "test", "time"])

# Make groups based on uuid
grouped = df.groupby("uuid")

# Find unique stability combinations within all groups
unique_RLOF_type_groups = set([str(el) for el in grouped["stability"].to_numpy()])
print(unique_RLOF_type_groups)
quit()
# Create groups
filtered_df_groups = {}
for unique_rlof_type_group in unique_RLOF_type_groups:
    filtered = grouped.filter(
        lambda x: x["stability"].to_list() == list(unique_rlof_type_group)
    )
    filtered_df_groups[str(unique_rlof_type_group)] = filtered
