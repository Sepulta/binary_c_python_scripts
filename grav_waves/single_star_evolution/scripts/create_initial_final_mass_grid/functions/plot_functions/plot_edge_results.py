import json

import matplotlib.pyplot as plt
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

import matplotlib as mpl

mpl.rcParams["axes.facecolor"] = ".9"

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot


def plot_ppisn_edge_info(result_filename, plot_settings=None):
    """
    Function to plot the ppisn edge information
    """

    #
    if not plot_settings:
        plot_settings = {}

    #########
    # Readout information

    # load again and plot the output
    with open(result_filename, "r") as f:
        edge_finder_outputs = json.loads(f.read())

    #
    found_metallicities = []
    jump_values = []

    initial_masses_left = []
    remnant_masses_left = []
    pre_sn_masses_left = []

    initial_masses_right = []
    remnant_masses_right = []
    pre_sn_masses_right = []

    ejected_mass_left = []
    ejected_mass_right = []

    total_envelope_mass_left = []
    total_envelope_mass_right = []

    h_envelope_mass_left = []
    h_envelope_mass_right = []

    # Loop over the metallicities and extract the values
    for metallicity in sorted(edge_finder_outputs.keys()):
        if edge_finder_outputs[metallicity]["found_edge"]:
            metallicity_result = edge_finder_outputs[metallicity]

            left_results = metallicity_result["left_guess_results"]
            right_results = metallicity_result["right_guess_results"]

            if not right_results["sn_type"] == 21.0:
                print("SN type not correct for the right value")
                raise ValueError

            found_metallicities.append(float(metallicity))
            jump_values.append(left_results["mass"] - right_results["mass"])

            #
            initial_masses_left.append(left_results["zams_mass"])
            remnant_masses_left.append(left_results["mass"])
            pre_sn_masses_left.append(left_results["pre_sn_mass"])
            ejected_mass_left.append(left_results["pre_sn_mass"] - left_results["mass"])
            total_envelope_mass_left.append(
                left_results["pre_sn_mass"] - left_results["pre_sn_co_core_mass"]
            )
            if left_results["pre_sn_he_core_mass"] > 0:
                h_envelope_mass_left.append(
                    left_results["pre_sn_mass"] - left_results["pre_sn_he_core_mass"]
                )
            else:
                h_envelope_mass_left.append(0)

            #
            initial_masses_right.append(right_results["zams_mass"])
            remnant_masses_right.append(right_results["mass"])
            pre_sn_masses_right.append(right_results["pre_sn_mass"])
            ejected_mass_right.append(
                right_results["pre_sn_mass"] - right_results["mass"]
            )
            total_envelope_mass_right.append(
                right_results["pre_sn_mass"] - right_results["pre_sn_co_core_mass"]
            )
            if right_results["pre_sn_he_core_mass"] > 0:
                h_envelope_mass_right.append(
                    right_results["pre_sn_mass"] - right_results["pre_sn_he_core_mass"]
                )
            else:
                h_envelope_mass_right.append(0)

    #########
    # Set up plot

    #
    fig = plt.figure(figsize=(16, 16))

    # Set up gridspec
    gs = fig.add_gridspec(nrows=4, ncols=1)

    # Create axis
    ax_masses = fig.add_subplot(gs[:2, 0])
    ax_jump_size = fig.add_subplot(gs[2:, 0])

    # Add the masses to the plots
    ax_masses.plot(found_metallicities, initial_masses_left, "bo", label="ZAMS mass")

    ax_masses.plot(
        found_metallicities, remnant_masses_left, "gd", label="Last CC remnant mass"
    )
    ax_masses.plot(
        found_metallicities,
        remnant_masses_right,
        "yd",
        label="First PPISN remnant mass",
    )

    # Pre-sn masses
    ax_masses.plot(
        found_metallicities, pre_sn_masses_left, "-.", label="Last CC pre-SN mass"
    )
    ax_masses.plot(
        found_metallicities, pre_sn_masses_right, "-.", label="First PPISN pre-SN mass"
    )

    ##
    # Jump size
    ax_jump_size.plot(
        found_metallicities,
        jump_values,
        label="abs(last cc remnant - first ppisn remnant)",
    )

    # Mass ejected
    ax_jump_size.plot(
        found_metallicities, ejected_mass_left, "--", label="Last CC mass ejected"
    )
    ax_jump_size.plot(
        found_metallicities, ejected_mass_right, "--", label="First PPISN mass ejected"
    )

    # envelope masses
    ax_jump_size.plot(
        found_metallicities,
        total_envelope_mass_left,
        ".",
        label="Last CC H+He envelope mass",
    )
    ax_jump_size.plot(
        found_metallicities,
        total_envelope_mass_right,
        ".",
        label="First PPISN H+He envelope mass",
    )

    # H-envelope masses
    ax_jump_size.plot(
        found_metallicities, h_envelope_mass_left, label="Last CC H envelope mass"
    )
    ax_jump_size.plot(
        found_metallicities, h_envelope_mass_right, label="First PPISN H envelope mass"
    )

    #
    ax_masses.legend()
    ax_jump_size.legend(framealpha=0.5, fontsize=16)

    ax_jump_size.set_title("Jump size")
    ax_jump_size.set_xscale("log")
    ax_jump_size.set_xlabel("Metallicity [Z]")
    ax_jump_size.set_ylabel(r"Mass difference [M$_{\odot}$]")

    ax_masses.set_xscale("log")
    ax_masses.set_xticklabels([])
    ax_masses.set_title("Masses")
    ax_masses.set_ylabel(r"Mass [M$_{\odot}$]")

    if plot_settings.get("plot_log", False):
        ax_masses.set_yscale("log")
        ax_jump_size.set_yscale("log")

    ax_jump_size.set_xlim(ax_masses.get_xlim())

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


# plot_ppisn_edge_info(edge_finder_outputs, {'show_plot': True, 'plot_log': False, 'output_name': 'linear_version.pdf'})
# plot_ppisn_edge_info(edge_finder_outputs, {'show_plot': True, 'plot_log': True, 'output_name': 'log_version.pdf'})
