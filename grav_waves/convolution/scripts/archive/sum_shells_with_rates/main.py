"""
Function to average the rates over a range of redshift
"""

import matplotlib.pyplot as plt

import numpy as np
import pickle

import astropy
import astropy.units as u
from grav_waves.settings import cosmo

from grav_waves.convolution.functions.volume_convolution_functions import (
    create_shell_volume_dict,
)

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

lower_redshift = 0
upper_redshift = 2

# Set the redshift bounds
redshift_bound_stepsize = 1

#
redshift_bounds = np.arange(
    lower_redshift, upper_redshift + redshift_bound_stepsize, redshift_bound_stepsize
)

# Set dataset
filename_dataset = "/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/general_scripts/results/MID_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW/all/redshift_without_detection_probability.p"
dataset = pickle.load(open(filename_dataset, "rb"))

mass_quantity_bins = dataset["convolution_configuration"]["primary_mass_bins"]
mass_quantity_centers = (mass_quantity_bins[1:] + mass_quantity_bins[:-1]) / 2


# select the redshift centers
redshift_range = dataset["convolution_configuration"]["time_centers"]

#
merger_rate_data = dataset["results"]["merger_rates"]["result_array_primary_mass"]

averaged_rate_array = np.zeros(
    (redshift_bounds.shape[0], merger_rate_data.shape[1])
) * (u.Msun / (u.Gpc**3 * u.yr))

# Create a dictionary with all the shell volumes
diction = create_shell_volume_dict(redshift_range)

#
for redshift_bound_i in range(len(redshift_bounds) - 1):
    lower_bound_redshift = redshift_bounds[redshift_bound_i]
    upper_bound_redshift = redshift_bounds[redshift_bound_i + 1]
    print(
        "Calculating the average merger rates between z = [{}, {}]".format(
            lower_bound_redshift, upper_bound_redshift
        )
    )

    current_lower_redshift_shell_edge = upper_redshift
    current_upper_redshift_shell_edge = lower_redshift

    current_merger_rate_averaged_array = np.zeros(merger_rate_data.shape[1]) * (
        u.Msun / (u.yr)
    )

    sum_individual_shell_volumes = 0
    sum_individual_redshift_rates = 0

    #
    for current_redshift_i, redshift in enumerate(sorted(diction.keys())):
        if lower_bound_redshift <= redshift <= upper_bound_redshift:
            subdict = diction[redshift]

            current_lower_redshift_shell_edge = np.min(
                [current_lower_redshift_shell_edge, subdict["lower_edge_shell"]]
            )
            current_upper_redshift_shell_edge = np.max(
                [current_upper_redshift_shell_edge, subdict["upper_edge_shell"]]
            )

            # Get shell volume
            shell_volume = subdict["shell_volume"]

            sum_individual_shell_volumes += shell_volume

            # Add current merger rate density * shell_volume to the array that contains the average
            current_merger_rate_averaged_array += (
                merger_rate_data[current_redshift_i] * shell_volume
            )
            sum_individual_redshift_rates += np.sum(
                merger_rate_data[current_redshift_i] * shell_volume
            )

    # Calculate current volume for this bounds
    comoving_volumes_at_redshift_edges = cosmo.comoving_volume(
        np.array([current_lower_redshift_shell_edge, current_upper_redshift_shell_edge])
    ).to(u.Gpc**3)
    current_comoving_volume_for_bounds = (
        comoving_volumes_at_redshift_edges[1] - comoving_volumes_at_redshift_edges[0]
    )
    averaged_rate_current_redshift_bounds = (
        current_merger_rate_averaged_array / current_comoving_volume_for_bounds
    )

    # Store in the final array
    averaged_rate_array[redshift_bound_i] = averaged_rate_current_redshift_bounds

    print(
        "Between the bounds z = [{}, {}]:".format(
            lower_bound_redshift, upper_bound_redshift
        )
    )
    print("\tTotal sum individual rates: {}".format(sum_individual_redshift_rates))
    print(
        "\tTotal sum individual shell volumes: {}".format(sum_individual_shell_volumes)
    )
    print(
        "\tAverage merger rate density: {}".format(
            np.sum(averaged_rate_current_redshift_bounds)
        )
    )

    # print(sum_individual_redshift_rates.unit.decompose(bases=[u.Msun, u.Gpc, u.yr]))
    # print(sum_individual_redshift_rates.unit.decompose())

import seaborn as sns

# # https://towardsdatascience.com/histograms-and-density-plots-in-python-f6bda88f5ac0
# https://stackoverflow.com/questions/27623919/weighted-gaussian-kernel-density-estimation-in-python/27623920#27623920

# #
# fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(16,16))
# # fig = plt.figure(figsize=(16,16))

# min_val = 1e-1
# for redshift_bound_i in range(len(redshift_bounds)-1):
#     lower_bound_redshift = redshift_bounds[redshift_bound_i]
#     upper_bound_redshift = redshift_bounds[redshift_bound_i + 1]
#     current_res = averaged_rate_array[redshift_bound_i]

#     # Set the minimum value
#     current_res[current_res.value<min_val] = min_val * current_res.unit
#     # print(mass_quantity_bins)
#     sns.distplot(current_res, kde=True, hist=False, norm_hist=False,
#                  bins=mass_quantity_bins,
#                  # color = 'darkblue',
#                  # hist_kws={'edgecolor':'black'},
#                  kde_kws={'linewidth': 4}, ax=axes, label='z=[{}, {}]'.format(lower_bound_redshift, upper_bound_redshift))

#     #
#     # axes.plot(mass_quantity_centers, current_res, label='z=[{}, {}]'.format(lower_bound_redshift, upper_bound_redshift))

# axes.set_yscale('log')
# axes.legend()

# plt.show()


import matplotlib.pyplot as plt
from statsmodels.nonparametric.kde import KDEUnivariate

kde1 = KDEUnivariate(mass_quantity_centers)

fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(16, 16))

min_val = 1e-5
for redshift_bound_i in range(len(redshift_bounds) - 1):
    lower_bound_redshift = redshift_bounds[redshift_bound_i]
    upper_bound_redshift = redshift_bounds[redshift_bound_i + 1]
    current_res = averaged_rate_array[redshift_bound_i]

    #
    kde1.fit(weights=current_res, bw=0.5, fft=False)
    res = np.array([kde1.evaluate(xi) for xi in kde1.support])
    res[res < min_val] = min_val

    axes.plot(
        kde1.support,
        res,
        label="z=[{}, {}]".format(lower_bound_redshift, upper_bound_redshift),
    )

    # # Set the minimum value
    # current_res[current_res.value<min_val] = min_val * current_res.unit
    # # print(mass_quantity_bins)
    # sns.distplot(current_res, kde=True, hist=False, norm_hist=False,
    #              bins=mass_quantity_bins,
    #              # color = 'darkblue',
    #              # hist_kws={'edgecolor':'black'},
    #              kde_kws={'linewidth': 4}, ax=axes, label='z=[{}, {}]'.format(lower_bound_redshift, upper_bound_redshift))

    #
    # axes.plot(mass_quantity_centers, current_res, label='z=[{}, {}]'.format(lower_bound_redshift, upper_bound_redshift))


axes.set_yscale("log")
axes.legend()

plt.show()
