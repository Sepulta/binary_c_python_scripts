import os
import time

import numpy as np

import matplotlib.colors
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt

from grav_waves.gw_analysis.functions.general_plot_function import general_plot_function

from grav_waves.gw_analysis.laptop_datasets import dataset_dict

from grav_waves.gw_analysis.functions.functions import (
    make_df,
    load_info_dict,
)

from grav_waves.gw_analysis.settings import (
    resolution_settings,
    AGE_UNIVERSE_IN_YEAR,
)

from grav_waves.gw_analysis.functions.plot_functions import (
    plot_merger_rate_by_metallicity_by_primary_mass,
)

MAIN_RESULT_DIR = "results"
MAIN_PLOT_DIR = "plots"

# List to loop over
DATASET_KEY_LIST = [
    # 'HIGH_RES_SCHNEIDER_MASS_LOSS_PPISN_OFF',
    "HIGH_RES_SCHNEIDER_MASS_LOSS_PPISN_ON",
]


PLOT_TYPE_KEY_LIST = [
    "bhbh",
    # 'bhns',
    # 'nsns',
]

key = DATASET_KEY_LIST[0]
type_key = PLOT_TYPE_KEY_LIST[0]

rebuild = dataset_dict[key]["rebuild"]
sim_name = dataset_dict[key]["sim_name"]
result_dir = os.path.join(MAIN_RESULT_DIR, dataset_dict[key]["result_dir"])
plot_dir = os.path.join(MAIN_PLOT_DIR, dataset_dict[key]["plot_dir"])

pdf_list = []

# Load dataset
info_dict = load_info_dict(result_dir, rebuild=rebuild)

plot_merger_rate_by_metallicity_by_primary_mass(
    info_dict, type_key, display_log_string=False, plot_settings={"show_plot": True}
)
