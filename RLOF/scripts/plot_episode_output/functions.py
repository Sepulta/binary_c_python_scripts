"""
Utility functions for the rlof episode plotting routines
"""

import os


def get_rlof_episode_dataset_dict(simulation_dir):
    """
    Function to get the a dict containing the rlof_episode datafiles using the main simulation directory
    """

    #
    rlof_episode_dataset_dict = {}

    #
    simulation_path = os.path.abspath(simulation_dir)
    simname = os.path.basename(simulation_path)

    rlof_episode_dataset_dict["simulation_path"] = simulation_path
    rlof_episode_dataset_dict["simname"] = simname

    #
    population_results_root = os.path.join(simulation_dir, "population_results")

    #
    dataset_dict = {}

    #
    for metallicity_dir in os.listdir(population_results_root):
        if metallicity_dir.startswith("Z"):

            metallicity_dict = {}

            metallicity = metallicity_dir[1:]
            metallicity_dict["metallicity"] = metallicity

            rlof_episode_datafile_path = os.path.join(
                population_results_root, metallicity_dir, "RLOF_episode_data.dat"
            )
            if os.path.isfile(rlof_episode_datafile_path):

                metallicity_dict["filename"] = rlof_episode_datafile_path

            #
            dataset_dict[metallicity] = metallicity_dict

    rlof_episode_dataset_dict["datasets"] = dataset_dict

    return rlof_episode_dataset_dict


def get_rlof_episode_dataset_dict_new(simulation_dir):
    """
    Function to get the a dict containing the rlof_episode datafiles using the main simulation directory
    """

    #
    rlof_episode_dataset_dict = {}

    #
    simulation_path = os.path.abspath(simulation_dir)
    simname = os.path.basename(simulation_path)

    rlof_episode_dataset_dict["simulation_path"] = simulation_path
    rlof_episode_dataset_dict["simname"] = simname

    #
    population_results_root = os.path.join(simulation_dir, "population_results")

    #
    dataset_dict = {}

    #
    for metallicity_dir in os.listdir(population_results_root):
        if metallicity_dir.startswith("Z"):

            metallicity_dict = {}

            metallicity = metallicity_dir[1:]
            metallicity_dict["metallicity"] = metallicity

            rlof_episode_datafile_path = os.path.join(
                population_results_root, metallicity_dir, "total_RLOF_events.dat"
            )
            if os.path.isfile(rlof_episode_datafile_path):

                metallicity_dict["filename"] = rlof_episode_datafile_path

            #
            dataset_dict[metallicity] = metallicity_dict

    rlof_episode_dataset_dict["datasets"] = dataset_dict

    return rlof_episode_dataset_dict
