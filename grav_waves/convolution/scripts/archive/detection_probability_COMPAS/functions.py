import numpy as np

from grav_waves.settings import cosmo

from COMPAS.utils.PythonScripts.CosmicIntegration.selection_effects import (
    detection_probability,
)


def find_largest_redshift_value(
    mass_1,
    mass_2,
    snr_threshold,
    sensitivity,
    redshift_min=0.001,
    redshift_max=4,
    redshift_samples=100,
):
    """
    Function to find the largest redshift that gives a non-zero probability
    """

    # Setup the range
    redshift_range = np.linspace(redshift_min, redshift_max, redshift_samples)
    distances_range = cosmo.luminosity_distance(redshift_range).value

    # Setup the mass ranges in the source frame
    mass_1_source_array = redshift_range * mass_1
    mass_2_source_array = redshift_range * mass_2

    # Set up the probability arrays
    probabilities_array = detection_probability(
        mass_1_source_array,
        mass_2_source_array,
        redshift_range,
        distances_range,
        snr_threshold,
        sensitivity=sensitivity,
    )

    # Find the zero
    first_zero_index = np.argmin(probabilities_array)
    highest_redshift = redshift_range[first_zero_index]

    return highest_redshift
