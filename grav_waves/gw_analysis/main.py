"""
Main point to call the routine to analyse the populations of DCOs and their properties
"""

import os

from grav_waves.settings import convolution_settings
from grav_waves.gw_analysis.functions.general_plot_function import (
    general_plot_function as population_plot_function,
)
from grav_waves.gw_analysis.functions.functions import make_dataset_dict

dco_type = "bhbh"
dataset_dict = make_dataset_dict(
    "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/MID_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED"
)

population_plot_function(
    dataset_dict,
    result_dir=dataset_dict["population_result_dir"],
    plot_dir=dataset_dict["population_plot_dir"],
    type_key="bhbh",
    combined_pdf_output_name="combined_output.pdf",
    convolution_settings=convolution_settings,
)
