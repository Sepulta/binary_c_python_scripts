"""
Function to plot a 3-panel with different variations
"""

import os
import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.utility_functions import (
    fill_missing_values,
    SN_type_dict,
    linestyle_tuple,
)

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def plot_initial_final_4piece(
    fiducial_dataset_filename,
    extra_massloss_dataset_filename,
    core_mass_shift_dataset_filename,
    add_contourlines=False,
    titles=None,
    custom_colorlimits=None,
    orientation="horizontal",
    plot_settings=None,
):
    """
    Function to plot a 3 panel with the different variations
    """

    contourlevels = [30, 40, 50]
    linestyles_list = ["solid", "dashed", "-."]

    #
    if plot_settings is None:
        plot_settings = {}

    ####
    # create Figure and axes
    if orientation == "horizontal":
        fig = plt.figure(figsize=(14 * 2, 4 * 4))
        gs = fig.add_gridspec(nrows=1, ncols=14)
        ax_fiducial = fig.add_subplot(gs[:, :4])
        ax_extra_massloss = fig.add_subplot(gs[:, 4:8])
        ax_core_mass_shift = fig.add_subplot(gs[:, 8:12])

    elif orientation == "vertical":
        fig = plt.figure(figsize=(4 * 4, 14 * 2))
        gs = fig.add_gridspec(nrows=3, ncols=5)
        ax_fiducial = fig.add_subplot(gs[0, :-2])
        ax_extra_massloss = fig.add_subplot(gs[1, :-2])
        ax_core_mass_shift = fig.add_subplot(gs[2, :-2])

    #
    ax_cb = fig.add_subplot(gs[:, -1])

    axes_list = [ax_fiducial, ax_extra_massloss, ax_core_mass_shift]
    datafile_list = [
        fiducial_dataset_filename,
        extra_massloss_dataset_filename,
        core_mass_shift_dataset_filename,
    ]

    #
    global_zmin = 1e90
    global_zmax = -1e90
    res_dict = {}

    # Loop over the datasets and plot the info
    for i, ax in enumerate(axes_list):
        # Open the data as a pandas dataframe
        df = pd.read_csv(datafile_list[i], header=0, sep="\s+")
        df["log10metallicity"] = np.log10(df["metallicity"])
        df = df.sort_values(by=["zams_mass", "log10metallicity"])
        other_columns = [
            col for col in df.columns if not col in ["zams_mass", "metallicity"]
        ]

        # Fill missing values
        df = fill_missing_values(df, other_columns)

        # Get unique values
        unique_zams_mass_values = df["zams_mass"].sort_values().unique()
        unique_metallicity_values = df["metallicity"].sort_values().unique()
        unique_log10unique_metallicity_values = np.log10(unique_metallicity_values)

        z_vals = (
            df["mass"]
            .to_numpy()
            .reshape(
                len(unique_zams_mass_values), len(unique_log10unique_metallicity_values)
            )
        )
        sn_type_vals = (
            df["sn_type"]
            .to_numpy()
            .reshape(
                len(unique_zams_mass_values), len(unique_log10unique_metallicity_values)
            )
        )

        # update global zmin and zmax
        global_zmin = np.min([global_zmin, z_vals[~np.isnan(z_vals)].min()])
        global_zmax = np.max([global_zmax, z_vals[~np.isnan(z_vals)].max()])

        # Store in result dict
        res_dict[i] = {
            "unique_metallicity_values": unique_metallicity_values,
            "unique_zams_mass_values": unique_zams_mass_values,
            "z_vals": z_vals,
            "sn_type_vals": sn_type_vals,
        }

    # Put the colorlimits and set the colorbar
    if not custom_colorlimits is None:
        norm = colors.Normalize(vmin=custom_colorlimits[0], vmax=custom_colorlimits[1])
    else:
        norm = colors.Normalize(vmin=global_zmin, vmax=global_zmax)

    # Loop over the datasets and plot the info
    for i, ax in enumerate(axes_list):
        # Set up the colormesh plot
        _ = ax.pcolormesh(
            res_dict[i]["unique_metallicity_values"],
            res_dict[i]["unique_zams_mass_values"],
            res_dict[i]["z_vals"],
            norm=norm,
            shading="auto",
            antialiased=True,
            rasterized=True,
        )

        # Put contour around PPISN
        _ = ax.contour(
            res_dict[i]["unique_metallicity_values"],
            res_dict[i]["unique_zams_mass_values"],
            res_dict[i]["sn_type_vals"],
            levels=[20, 21, 22],
            colors="red",
            linewidths=0.5,
        )

        # Create contourlevels for the mass
        if add_contourlines:
            _ = ax.contour(
                res_dict[i]["unique_metallicity_values"],
                res_dict[i]["unique_zams_mass_values"],
                res_dict[i]["z_vals"],
                levels=contourlevels,
                linestyles=linestyles_list,
                colors="k",
                linewidths=2,
            )

        # Set x-scale to log
        ax.set_xscale("log")
        ax.set_yscale(plot_settings["mass_scale"])

        if not titles is None:
            ax.set_title(titles[i])

        if i < 2:
            ax.set_xticklabels([])

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        norm=norm,
    )
    cbar.ax.set_ylabel(r"Remnant mass [M$_{\odot}$]")

    # add contourlevels to the colorbar
    for contour_i, _ in enumerate(contourlevels):
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [contourlevels[contour_i]] * 2,
            "black",
            linestyle=linestyles_list[contour_i],
            linewidth=2,
        )

    axes_list[-1].set_xlabel("log10 Metallicity (Z)")
    axes_list[1].set_ylabel(r"ZAMS mass [M$_{\odot}$]")

    fig.tight_layout()

    # Handle saving
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":

    #
    data_dir = "output/"
    fiducial_filename = os.path.join(data_dir, "output_vanilla/data/output.dat")

    #
    extra_massloss_filename = os.path.join(
        data_dir, "output_ten_solarmass_extra_removed/data/output.dat"
    )
    core_mass_shift_filename = os.path.join(
        data_dir, "output_five_core_mass_shift/data/output.dat"
    )
    plot_initial_final_3piece(
        fiducial_filename,
        extra_massloss_filename,
        core_mass_shift_filename,
        add_contourlines=False,
        titles=[
            "Fiducial",
            r"extra 10 M$_{\odot}$" + "\nremoved",
            r"5 M$_{\odot}$" + "\ncore mass shift",
        ],
        plot_settings={
            "show_plot": False,
            "output_name": "three_panel_variations.pdf",
            "mass_scale": "linear",
        },
    )

    plot_initial_final_3piece(
        fiducial_filename,
        extra_massloss_filename,
        core_mass_shift_filename,
        orientation="vertical",
        add_contourlines=False,
        titles=[
            "Fiducial",
            r"extra 10 M$_{\odot}$" + "\nremoved",
            r"5 M$_{\odot}$" + "\ncore mass shift",
        ],
        plot_settings={
            "show_plot": False,
            "output_name": "vertical_three_panel_variations.pdf",
            "mass_scale": "linear",
        },
    )

    # #
    # double_massloss_filename = os.path.join(data_dir, 'output_double_ppisn_massloss/data/output.dat')
    # half_massloss_filename = os.path.join(data_dir, 'output_half_ppisn_massloss/data/output.dat')
    # plot_initial_final_3piece(
    #     fiducial_filename,
    #     double_massloss_filename,
    #     half_massloss_filename,
    #     titles=["Fiducial", "Double ppisn masslos", "Half ppisn massloss"],
    #     plot_settings={"show_plot": False, 'output_name': 'three_panel_variations_half_double.pdf', 'mass_scale': 'linear'}
    # )
