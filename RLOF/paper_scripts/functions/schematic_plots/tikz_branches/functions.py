"""
Script containing functions to generate the tikz figure

TODO: add a bootstrap functionality here to calculate the sampling uncertainty
"""

import numpy as np
import pandas as pd


def calculate_branching_ratios(probabilities_dict):
    """
    Function to calculate the branching ratios of the disk MT
    """

    branching_ratios_dict = {}

    brancing_ratios_all_rlof_onto_ms = []
    branching_ratios_stable_rlof_onto_ms = []
    branching_ratios_stable_rlof_onto_ms_via_disk = []

    for metallicity in probabilities_dict:
        system_prob_dict = probabilities_dict[metallicity]["system_prob"]

        # Calculate fraction onto MS
        fraction_all_rlof_onto_ms = (
            system_prob_dict["rlof_onto_ms"] / system_prob_dict["all_rlof"]
        )
        brancing_ratios_all_rlof_onto_ms.append(fraction_all_rlof_onto_ms)

        # Calculate fraction onto ms stable
        fraction_stable_rlof_onto_ms = (
            system_prob_dict["stable_rlof_onto_ms"] / system_prob_dict["rlof_onto_ms"]
        )
        branching_ratios_stable_rlof_onto_ms.append(fraction_stable_rlof_onto_ms)

        # Calculate fraction onto ms stable via disk
        fraction_stable_rlof_onto_ms = (
            system_prob_dict["stable_ms_disk_mt_at_least_partial"]
            / system_prob_dict["stable_rlof_onto_ms"]
        )
        branching_ratios_stable_rlof_onto_ms_via_disk.append(
            fraction_stable_rlof_onto_ms
        )

    branching_ratios_dict["all_rlof_onto_ms"] = brancing_ratios_all_rlof_onto_ms
    branching_ratios_dict["all_rlof_onto_ms_mean"] = np.mean(
        np.array(brancing_ratios_all_rlof_onto_ms)
    )
    branching_ratios_dict["all_rlof_onto_ms_stddev"] = np.std(
        np.array(brancing_ratios_all_rlof_onto_ms)
    )

    branching_ratios_dict["stable_rlof_onto_ms"] = branching_ratios_stable_rlof_onto_ms
    branching_ratios_dict["stable_rlof_onto_ms_mean"] = np.mean(
        np.array(branching_ratios_stable_rlof_onto_ms)
    )
    branching_ratios_dict["stable_rlof_onto_ms_stddev"] = np.std(
        np.array(branching_ratios_stable_rlof_onto_ms)
    )

    branching_ratios_dict[
        "stable_rlof_onto_ms_via_disk"
    ] = branching_ratios_stable_rlof_onto_ms_via_disk
    branching_ratios_dict["stable_rlof_onto_ms_via_disk_mean"] = np.mean(
        np.array(branching_ratios_stable_rlof_onto_ms_via_disk)
    )
    branching_ratios_dict["stable_rlof_onto_ms_via_disk_stddev"] = np.std(
        np.array(branching_ratios_stable_rlof_onto_ms_via_disk)
    )

    return branching_ratios_dict


def generate_branch_data_dict(filename):
    """
    Function to calculate all the
    """

    result_dict = {
        "system_prob": {},
        "time_weighted_prob": {},
        "mass_weighted_prob": {},
    }
    prob_colname = "probability"

    # Read out data and create dataframe
    df = pd.read_csv(filename, sep="\s+", header=0)
    df["time_weighted_prob"] = df[prob_colname] * df["total_time_spent_masstransfer"]
    df["mass_weighted_prob"] = df[prob_colname] * df["total_mass_transferred"]
    print(df.columns)
    # All RLOF
    result_dict["system_prob"]["all_rlof"] = df[prob_colname].sum()
    result_dict["time_weighted_prob"]["all_rlof"] = df["time_weighted_prob"].sum()
    result_dict["mass_weighted_prob"]["all_rlof"] = df["mass_weighted_prob"].sum()

    ################################
    # Branch onto which stellar type
    non_ms_acc = df[df.initial_stellar_type_accretor > 1]
    ms_acc = df[df.initial_stellar_type_accretor == 1]

    print(ms_acc[["initial_stability", "final_stability"]])

    # onto MS
    result_dict["system_prob"]["rlof_onto_ms"] = ms_acc[prob_colname].sum()
    result_dict["time_weighted_prob"]["rlof_onto_ms"] = ms_acc[
        "time_weighted_prob"
    ].sum()
    result_dict["mass_weighted_prob"]["rlof_onto_ms"] = ms_acc[
        "mass_weighted_prob"
    ].sum()

    # Onto other
    result_dict["system_prob"]["rlof_onto_non_ms"] = non_ms_acc[prob_colname].sum()
    result_dict["time_weighted_prob"]["rlof_onto_non_ms"] = non_ms_acc[
        "time_weighted_prob"
    ].sum()
    result_dict["mass_weighted_prob"]["rlof_onto_non_ms"] = non_ms_acc[
        "mass_weighted_prob"
    ].sum()

    ################################
    # Branch stability onto MS
    stable_ms = ms_acc[ms_acc.initial_stability == 0]
    unstable_ms = ms_acc[ms_acc.initial_stability > 0]

    # stable MT
    result_dict["system_prob"]["stable_rlof_onto_ms"] = stable_ms[prob_colname].sum()
    result_dict["time_weighted_prob"]["stable_rlof_onto_ms"] = stable_ms[
        "time_weighted_prob"
    ].sum()
    result_dict["mass_weighted_prob"]["stable_rlof_onto_ms"] = stable_ms[
        "mass_weighted_prob"
    ].sum()

    # stable MT
    result_dict["system_prob"]["unstable_rlof_onto_ms"] = unstable_ms[
        prob_colname
    ].sum()
    result_dict["time_weighted_prob"]["unstable_rlof_onto_ms"] = unstable_ms[
        "time_weighted_prob"
    ].sum()
    result_dict["mass_weighted_prob"]["unstable_rlof_onto_ms"] = unstable_ms[
        "mass_weighted_prob"
    ].sum()

    ################################
    # Branch direct impact only or disk MT for stable MS MT
    stable_ms_direct_impact_only = stable_ms.query(
        "total_mass_transferred_through_disk == 0 & total_time_spent_disk_masstransfer == 0"
    )
    stable_ms_disk_mt_at_least_partial = stable_ms.query(
        "total_mass_transferred_through_disk > 0 & total_time_spent_disk_masstransfer > 0"
    )

    # differentiate between no disk MT at all
    result_dict["system_prob"][
        "stable_ms_direct_impact_only"
    ] = stable_ms_direct_impact_only[prob_colname].sum()

    result_dict["system_prob"][
        "stable_ms_disk_mt_at_least_partial"
    ] = stable_ms_disk_mt_at_least_partial[prob_colname].sum()

    # Calculate time spent disk MT in stable MT

    # result_dict["time_weighted_prob"]["unstable_rlof_onto_ms"] = stable_ms[
    #     "time_weighted_prob"
    # ].sum()

    return result_dict
