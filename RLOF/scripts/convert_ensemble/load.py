"""
Function to inflate an ensemble (without the )

TODO: check why the final columns is not filled
"""

import time
import json
import pandas as pd

from grav_waves.settings import convolution_settings
from RLOF.scripts.plot_ensemble_output.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)

from ensemble_functions.ensemble_functions import (
    inflate_ensemble_with_lists,
    find_columnames_recursively,
)

simulation_dir = (
    "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_SANA"
)
testing = False

ensemble_output_name = "results/output_ensemble.json"
inflated_ensemble_output_name = "results/inflated_output_ensemble.json"


inflated_ensemble_output_name = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_SANA/population_results/Z0.02/RLOF_ensemble_data.csv"

# reloading for timing purposes
print("reloading dataframe")
start = time.time()
df = pd.read_csv(inflated_ensemble_output_name, header=0, sep="\s+")
print("\tTook {}s".format(time.time() - start))

print(df)
print(df["probability"])

# # reloading for timing purposes
# print("reloading ensemble"); start=time.time()
# with open(ensemble_output_name, 'r') as f:
#     ensemble_data = json.loads(f.read())
# print("\tTook {}s".format(time.time()-start))
