"""
Main script to run all the routines to generate scripts

We only rely on the datasets being in their place and up to date
- The datasets should at max contain 3 metallicities

We can query the datasets with a set of global queries. These will be executed in each function. The output directory name will be set to the root dir + global_query_name
- In the
"""

import os

from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_dataset_for_simname,
    copy_to_paper_directory_function,
    create_table_pdf,
)

from RLOF.scripts.theory_plots.general_theory_plot_function import (
    general_plot_routine as general_theory_plot_routine,
)
from RLOF.scripts.generate_paper_figures_and_tables_scripts.generate_figures_and_tables_paper import (
    generate_figures_and_tables_paper,
)

#############################################################
# Configuration
#############################################################

# Select the datasets
population_datasets_sana = generate_dataset_for_simname(
    "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions/"
)
population_datasets_ms = generate_dataset_for_simname(
    "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_MOE_DISTEFANO_2021_ENSEMBLE_fractions/"
)

# Register this file
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)

#
root_output_dir = os.path.join(this_file_dir, "output")

# Register table directory here
local_figure_directory = os.path.join(os.path.dirname(this_file), "figures")
local_tables_directory = os.path.join(os.path.dirname(this_file), "tables")

# Set the directories of the paper
directory_paper_disks = os.path.expanduser("~/PHD/papers/paper_disks/paper_tex")
directory_paper_disks_figures = os.path.join(directory_paper_disks, "figures/new")
directory_paper_disks_tables = os.path.join(directory_paper_disks, "tables/new")

# Some switches to copy things or not
copy_to_paper_directory = False
testing = False  # Flag whether we are testing the code. If false then we use the entire available datasets
verbose = 1
# Flags for running the routines

###############
# Generate theory plots for paper
###############

#
general_theory_plot_routine(
    "output/theory",
    copy_to_paper_directory=copy_to_paper_directory,
    paper_figure_directory=directory_paper_disks_figures,
    verbose=verbose,
)

###############
# Generate figures and tables for the paper
###############

generate_figures_and_tables_paper(
    population_datasets_sana,
    population_datasets_ms,
    output_dir="output/paper",
    verbose=verbose,
    testing=testing,
    copy_to_paper_directory=copy_to_paper_directory,
)

quit()
