"""
routine to generate figure 1 from neijssel19
"""

import os
import numpy as np
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

from grav_waves.gw_analysis.laptop_datasets import dataset_dict

from grav_waves.gw_analysis.functions.functions import (
    make_df,
    load_info_dict,
)

from grav_waves.settings import (
    resolution_settings,
    AGE_UNIVERSE_IN_YEAR,
    mass_in_stars,
    config_dict_cosmology,
)

from grav_waves.gw_analysis.functions.plot_functions import (
    plot_merger_rate_by_metallicity_by_total_mass,
)

from grav_waves.convolution.functions.convolution_functions import (
    get_dataset_metallicity_info,
)

MAIN_RESULT_DIR = "/home/david/projects/binary_c_root/results/GRAV_WAVES/"
MAIN_PLOT_DIR = "plots"

# List to loop over
DATASET_KEY_LIST = [
    "LOW_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_ON",
]
PLOT_TYPE_KEY_LIST = [
    "bhbh",
]

key = DATASET_KEY_LIST[0]
type_key = PLOT_TYPE_KEY_LIST[0]
sim_name = dataset_dict[key]["sim_name"]

result_dir = os.path.join(MAIN_RESULT_DIR, dataset_dict[key]["result_dir"])
plot_dir = os.path.join(MAIN_PLOT_DIR, dataset_dict[key]["plot_dir"])

# Load dataset
info_dict = load_info_dict(result_dir, rebuild=False)


#
def massratio_plot_combined(merger_dict, plot_settings=None):
    """
    Function to plot the yield of
    """

    type_key = "bhbh"
    binary_fraction = 0.7

    stepsize = 0.1
    massratio_bins = np.arange(0, 1 + stepsize, stepsize)

    if not plot_settings:
        plot_settings = {}

    #
    log10metallicities_in_solar = []

    # get metallicity info
    (
        sorted_metallicity_keys,
        sorted_metallicity_keys_type,
        _,
        sorted_metallicity_logvalues,
        logmetallicity_stepsize,
    ) = get_dataset_metallicity_info(merger_dict, type_key)

    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(16, 16))

    # loop over all the metallicities
    for i, el in enumerate(sorted_metallicity_keys_type):
        info_dict = merger_dict[el]

        log10metallicity_in_solar = np.log10(
            info_dict["metallicity"] / config_dict_cosmology["solar_value"]
        )

        # print(info_dict)
        df = make_df(info_dict["filename"], limit_merging_time=False)

        # Limit by merging time
        df = df.query("%s < %f" % ("merger_time_values_in_years", AGE_UNIVERSE_IN_YEAR))

        #
        df["primary_mass"] = df[["mass_1", "mass_2"]].max(axis=1)
        df["secondary_mass"] = df[["mass_1", "mass_2"]].min(axis=1)

        df["massratio"] = df["secondary_mass"] / df["primary_mass"]

        axes.hist(
            df["massratio"],
            bins=massratio_bins,
            weights=df["probability"] * binary_fraction / mass_in_stars,
            label=log10metallicity_in_solar,
            histtype="step",
            linewidth=3,
            density=True,
        )
    axes.set_yscale("log")
    axes.legend(loc=1)
    plt.show()


massratio_plot_combined(info_dict)
