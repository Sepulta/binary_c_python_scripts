"""
Script to run a grid of single stars in mass and metallicity and plot that

TODO: continous new prescription fryer
"""

import os

import numpy as np
import pandas as pd

from david_phd_functions.binaryc.personal_defaults import personal_defaults
from david_phd_functions.backup_functions.functions import backup_if_exists

from grav_waves.settings import (
    project_specific_bse_settings,
    project_specific_grid_settings,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.generate_data_function import (
    generate_data_function,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.generate_edge_data_function import (
    generate_edge_data,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.general_plot_routine import (
    general_plot_routine,
)

#
this_file_dir = os.path.dirname(os.path.abspath(__file__))


def main(
    main_output_dir,
    sim_name,
    bse_settings,
    system_generator_settings,
    grid_settings,
    general_plot_config={},
    generate_data=False,
    generate_edge_data=False,
    generate_plot=False,
    generate_data_if_not_present=False,
):
    """
    main function to run a grid of stars in mass and metallicity
    """

    print("Called main function for m-Z grid.")
    print("Using physics settings:\n\t{}".format(bse_settings))
    print("Using system generator settings:\n\t{}".format(system_generator_settings))
    print("Using grid settings:\n\t{}".format(grid_settings))

    ##
    # Create names
    data_output_dir = os.path.join(main_output_dir, "data/")
    edge_data_output_dir = os.path.join(main_output_dir, "edge_data/")
    plot_output_dir = os.path.join(main_output_dir, "plots/")
    combined_plot_output_filename = os.path.join(main_output_dir, "combined.pdf")

    grid_dataset_output_filename = os.path.join(data_output_dir, "output.dat")
    edge_dataset_output_filename = os.path.join(edge_data_output_dir, "output.json")

    # Create directories
    os.makedirs(main_output_dir, exist_ok=True)
    os.makedirs(data_output_dir, exist_ok=True)
    os.makedirs(edge_data_output_dir, exist_ok=True)

    ##############################################
    # Check if the directory of the current simulation exists. If it does, then delete the old dir
    if generate_data or generate_edge_data:
        backup_if_exists(main_output_dir, remove_old_directory_after_backup=True)

    # Generate the data
    if generate_data:
        # Generate grid
        generate_data_function(
            output_dir=data_output_dir,
            system_generator_settings=system_generator_settings,
            grid_settings=grid_settings,
            physics_settings=bse_settings,
        )

    # Generate edge data
    if generate_edge_data:
        generate_edge_data(
            output_file=edge_dataset_output_filename,
            bse_settings=bse_settings,
            grid_settings=grid_settings,
            system_generator_settings=system_generator_settings,
        )

    # Generate the plots
    if generate_plot:
        general_plot_routine(
            input_dataset=grid_dataset_output_filename,
            output_dir=plot_output_dir,
            combined_output_filename=combined_plot_output_filename,
            sim_name=sim_name,
            edge_datafile=edge_dataset_output_filename,
            **general_plot_config,
        )


##
# Setting for this script
generate_data = False
generate_edge_data = False
generate_plots = False
generate_data_if_not_present = False

###
# Set up the base settings
system_generator_settings = {
    "mass_resolution": 292,
    "min_mass": 8,
    "max_mass": 300,
    "log10metallicity_resolution": 24,
    "min_log10_metallicity": -3.9,
    "max_log10_metallicity": -1.4,
    "include_zero_metallicity": True,
}

del project_specific_bse_settings["david_ppisn_logging"]
del project_specific_bse_settings["david_logging_function"]

# Create the BSE settings
bse_settings = {
    **personal_defaults,
    **project_specific_bse_settings,
    "BH_prescription": 3,
    # 'david_log_wind_ensemble': 0,
    "multiplicity": 1,
    "custom_timeout_time": 30,
    "minimum_timestep": 1e-8,
}

#
grid_settings = {**project_specific_grid_settings, "num_cores": 12, "verbosity": 1}

#
general_plot_config = {
    "include_initial_final_grid_plot": True,
    "include_initial_final_grid_fraction_plot": True,
    "include_mass_lost_supernova_plot": True,
    "include_mass_lost_supernova_fraction_plot": True,
    "include_mass_lost_wind_plot": True,
    "include_mass_lost_wind_fraction_plot": True,
    "include_fallback_grid_plot": True,
    "include_min_max_first_bh_mass_plot": True,
    "include_mapping_plot": True,
    "include_edge_plot": False,
}

main_args = {
    "system_generator_settings": system_generator_settings,
    "grid_settings": grid_settings,
    "general_plot_config": general_plot_config,
    "generate_data": generate_data,
    "generate_edge_data": generate_edge_data,
    "generate_plot": generate_plots,
    "generate_data_if_not_present": generate_data_if_not_present,
}

#
main_dir = this_file_dir

##
# Vanilla/default model
main(
    main_output_dir=os.path.join(main_dir, "output/output_vanilla_for_rob/"),
    sim_name="vanilla",
    bse_settings=bse_settings,
    **main_args,
)


def fill_missing_values(df, dummy_value):
    """
    Function to fill the missing values and return an updated dataframe

    In some situations the model fails, and we have a hole in our grid. We want to replace this hole by NaN values.
    """

    ##
    # Settings

    # columns that are the leading ones (i.e. shouldnt be overwritten)
    main_columns = ["effective_metallicity", "zams_mass"]

    #
    unique_zams_mass_values = df["zams_mass"].sort_values().unique()
    unique_effective_metallicity_values = (
        df["effective_metallicity"].sort_values().unique()
    )

    # Determine the columns that need to be filled with dummy values
    df_columns = df.columns
    fill_columns = list(set(df_columns) - set(main_columns))

    # Check if there are metallicity values for which not all of the zams masses are registered/successfully evolved
    for eff_met in unique_effective_metallicity_values:
        # Find missing zams values
        cur_zams_values = set(df[df.effective_metallicity == eff_met]["zams_mass"])
        missing_zams_values = set(unique_zams_mass_values) - cur_zams_values

        # Create placeholder data
        if missing_zams_values:
            dummy_lines = []
            for zams_value in missing_zams_values:
                dummy_line = {
                    "zams_mass": zams_value,
                    "effective_metallicity": eff_met,
                    **{col: dummy_value for col in fill_columns},
                }
                dummy_lines.append(dummy_line)

            # Create dataframe
            placeholder_df = pd.DataFrame(dummy_lines)

            # Add dataframe to total dataframe
            df = pd.concat([df, placeholder_df], ignore_index=True)

    # It is possible that there are duplicate lines in the data. Somehow, not sure how, but apparently so.
    df = df.drop_duplicates()

    return df


def process_and_fill(filename, output_filename, dummy_value, drop_columns):
    """
    Function to open the datafile, add missing values and
    """

    # Open the data as a pandas dataframe
    df = pd.read_csv(filename, header=0, sep="\s+")
    df["log10metallicity"] = np.log10(df["metallicity"])
    df = df.sort_values(by=["zams_mass", "log10metallicity"])

    # Fill missing values
    df = fill_missing_values(df, dummy_value=dummy_value)
    df = df.sort_values(by=["zams_mass", "log10metallicity"])

    # Re-order
    main_cols = ["effective_metallicity", "zams_mass"]
    reorder_columns = list(set(df.columns) - set(main_cols) - set(drop_columns))
    reordered_columns = main_cols + reorder_columns
    reordered_df = df[reordered_columns]

    # Re-sort
    reordered_df = reordered_df.sort_values(by=["effective_metallicity", "zams_mass"])

    # Rename columns
    reordered_df = reordered_df.rename(columns={"mass": "remnant_mass"})

    # Write to new file
    reordered_df.to_csv(
        output_filename,
        header=True,
        index=False,
        sep=" ",
    )


#
filename = os.path.join(main_dir, "output/output_vanilla_for_rob/data", "output.dat")
output_filename = os.path.join(
    main_dir, "output/output_vanilla_for_rob/data", "output_rectangular.dat"
)

#
process_and_fill(
    filename=filename,
    output_filename=output_filename,
    dummy_value=-1,
    drop_columns=["probability", "log10metallicity"],
)
