"""
Function to combine the birth redshift with the MSSFR array to calculate the rate array
"""

import copy

import numpy as np

from grav_waves.convolution.functions.convolution_general_functions import (
    create_bins_from_centers,
)


def calculate_rate_array(
    metallicity_weighted_starformation_array,
    convolution_configuration,
    combined_data_array,
    birth_redshift_array,
):
    """
    Function that combines the starformation rate, based on the birth_redshift_array with the MSSFR array,
    with the detection probability array and the yield per formed redshift.
    the number_per_solar_mass_values of datasets with the sfr+metallicity rates
    """

    # Extend the time bins to have the digitize put the stuff that falls out of the bounds in a column with 0 SFR
    extended_time_bins = copy.copy(convolution_configuration["time_bins"])
    extended_time_bins = np.insert(
        extended_time_bins,
        0,
        extended_time_bins[0] - np.diff(convolution_configuration["time_bins"])[0],
    )
    extended_time_bins = np.insert(
        extended_time_bins, extended_time_bins.size, extended_time_bins[-1] + 1e100
    )

    ###################
    # Calculate rate

    # Get the metallicities bins
    sorted_metallicity_bins = create_bins_from_centers(
        np.array(sorted(np.unique(combined_data_array["metallicity"])))
    )

    # Get indices for metallicity values
    metallicity_indices = (
        np.digitize(
            combined_data_array["metallicity"],
            bins=sorted_metallicity_bins,
            right=False,
        )
        - 1
    )

    # Get indices for birth redshift
    digitized_time_indices = (
        np.digitize(birth_redshift_array, bins=extended_time_bins, right=False) - 1
    )

    # Calculate rates
    digitised_rates = (
        metallicity_weighted_starformation_array[
            metallicity_indices, digitized_time_indices
        ].value
        * combined_data_array["number_per_solar_mass_values"]
    )

    # return the dict
    return digitised_rates
