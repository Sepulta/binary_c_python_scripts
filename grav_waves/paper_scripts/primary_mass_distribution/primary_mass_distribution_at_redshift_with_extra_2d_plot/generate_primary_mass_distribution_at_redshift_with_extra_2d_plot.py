"""
Function to make plots specifically for extra 2-d things
"""

import os
import numpy as np

from grav_waves.paper_scripts.primary_mass_distribution.main_plot_function import (
    generate_primary_mass_at_redshift_zero_plot,
)

if __name__ == "__main__":
    #########
    #
    data_root = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"), "GRAV_WAVES/server_results/"
    )

    #########
    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"),
        "paper_gw/paper_tex/figures/primary_mass_distribution_at_redshift_zero_with_extra_2d_plot",
    )
    plot_dir = "plots"

    # configuration:
    primary_mass_bins = np.arange(0, 100, 2)
    divide_by_binsize = True
    add_rates_plots = True
    add_secondary_2d_plot = True
    add_kde = False
    bootstraps = 50
    show_plot = False
    second_quantity = "eccentricity"
    second_quantity_bins_stepsize = 0.05
    second_quantity_bins = np.arange(
        0, 1 + second_quantity_bins_stepsize, second_quantity_bins_stepsize
    )

    #######
    # Fiducial

    #
    convolved_datasets = {
        # Fiducial and variations
        "Fiducial (SEMI RES)": os.path.join(
            data_root,
            "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
        ),
        # "Fiducial (HIGH RES)": os.path.join(
        #     data_root,
        #     "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
        # ),
    }

    #
    basename = "test.pdf"
    generate_primary_mass_at_redshift_zero_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_secondary_2d_plot=add_secondary_2d_plot,
        second_quantity=second_quantity,
        second_quantity_bins=second_quantity_bins,
        bootstraps=bootstraps,
        general_query=None,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 32,
        },
    )

    quit()

    # #######
    # # Without CE

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial without CE systems (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_eccentricity_second_axis_without_CE.pdf"
    # general_query = None
    # general_query = "comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # CE only

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial CE only (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_eccentricity_second_axis_CE_only.pdf"
    # general_query = None
    # general_query = "comenv_counter > 0 & stable_rlof_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Without stable MT

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial without stable MT systems (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_eccentricity_second_axis_without_stable_MT.pdf"
    # general_query = None
    # general_query = "stable_rlof_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Stable MT only

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial stable MT only (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_eccentricity_second_axis_stable_MT_only.pdf"
    # general_query = None
    # general_query = "stable_rlof_counter > 0 & comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Without any MT

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial without any MT systems (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_eccentricity_second_axis_without_any_MT.pdf"
    # general_query = None
    # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    #######
    # lower res with 0 kick variation

    #
    convolved_datasets = {
        # Fiducial and variations
        # "Fiducial max mass=150(SEMI RES)": os.path.join(
        #     data_root,
        #     "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
        # ),
        r"Fiducial max mass=150 $\sigma_{\mathrm{kick}} = 0$ (MID SEMI RES)": os.path.join(
            data_root,
            "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_sn_kick_dispersion_II_0sn_kick_dispersion_IBC_0/dco_convolution_results/convolution_results.h5",
        ),
    }

    #
    general_query = None
    basename = "low_res_zero_kick_variation_eccentricity_second_axis.pdf"
    # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    generate_primary_mass_at_redshift_zero_comparison_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_secondary_2d_plot=add_secondary_2d_plot,
        second_quantity=second_quantity,
        second_quantity_bins=second_quantity_bins,
        add_kde=add_kde,
        bootstraps=bootstraps,
        general_query=general_query,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 24,
        },
    )

    #######
    # Fiducial

    #
    convolved_datasets = {
        # Fiducial and variations
        "Fiducial (HIGH RES)": os.path.join(
            data_root,
            "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
        ),
    }

    #
    basename = "low_res_fiducial_with_eccentricity_second_axis.pdf"
    general_query = None
    # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    generate_primary_mass_at_redshift_zero_comparison_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_secondary_2d_plot=add_secondary_2d_plot,
        second_quantity=second_quantity,
        second_quantity_bins=second_quantity_bins,
        add_kde=add_kde,
        bootstraps=bootstraps,
        general_query=general_query,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 32,
        },
    )

    ############################
    #
    second_quantity = "mass_ratio"
    second_quantity_bins_stepsize = 0.05
    second_quantity_bins = np.arange(
        0, 1 + second_quantity_bins_stepsize, second_quantity_bins_stepsize
    )

    # #######
    # # Fiducial

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_massratio_second_axis.pdf"
    # general_query = None
    # # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Without CE

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial without CE systems (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_massratio_second_axis_without_CE.pdf"
    # general_query = None
    # general_query = "comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # CE only

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial CE only (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_massratio_second_axis_CE_only.pdf"
    # general_query = None
    # general_query = "comenv_counter > 0 & stable_rlof_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Without stable MT

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial without stable MT systems (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_massratio_second_axis_without_stable_MT.pdf"
    # general_query = None
    # general_query = "stable_rlof_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Stable MT only

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial stable MT systems only (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_massratio_second_axis_stable_MT_only.pdf"
    # general_query = None
    # general_query = "stable_rlof_counter > 0 & comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Without any MT

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial without any MT systems (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_massratio_second_axis_without_any_MT.pdf"
    # general_query = None
    # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    #######
    # lower res with 0 kick variation

    #
    convolved_datasets = {
        # Fiducial and variations
        # "Fiducial max mass=150(SEMI RES)": os.path.join(
        #     data_root,
        #     "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
        # ),
        r"Fiducial max mass=150 $\sigma_{\mathrm{kick}} = 0$ (MID SEMI RES)": os.path.join(
            data_root,
            "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_sn_kick_dispersion_II_0sn_kick_dispersion_IBC_0/dco_convolution_results/convolution_results.h5",
        ),
    }

    #
    basename = "low_res_zero_kick_variation_massratio_second_axis.pdf"
    # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    generate_primary_mass_at_redshift_zero_comparison_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_secondary_2d_plot=add_secondary_2d_plot,
        second_quantity=second_quantity,
        second_quantity_bins=second_quantity_bins,
        add_kde=add_kde,
        bootstraps=bootstraps,
        # general_query=general_query,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 24,
        },
    )

    #######
    # Fiducial

    #
    convolved_datasets = {
        # Fiducial and variations
        "Fiducial (HIGH RES)": os.path.join(
            data_root,
            "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
        ),
    }

    #
    basename = "low_res_fiducial_with_massratio_second_axis.pdf"
    general_query = None
    # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    generate_primary_mass_at_redshift_zero_comparison_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_secondary_2d_plot=add_secondary_2d_plot,
        second_quantity=second_quantity,
        second_quantity_bins=second_quantity_bins,
        add_kde=add_kde,
        bootstraps=bootstraps,
        general_query=general_query,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 32,
        },
    )
