"""
Function to convert the binary_c convolution structure to compas convolution structure. Almost

TODO: create method that does the binning upon readout to prevent big-memory issues
"""

import shutil
import h5py

import pandas as pd
import numpy as np

import matplotlib.pyplot as plt

import matplotlib
import matplotlib.pyplot as plt

import numpy as np

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def create_binned_quantity_and_redshift_array(
    redshift_and_rate_array, quantity_array, quantity_bins
):
    """
    Function to fill the quantity

    https://stackoverflow.com/questions/45363131/apply-numpy-histogram-to-multidimensional-array

    TODO: find a method to do the 'binning' of 2-d data with a 1-d array in those dimensions
    """

    # Create an empty array with the shape of the bins
    binned_quantity_and_redshift_array = np.zeros(
        shape=(len(redshift_array), len(quantity_bins) - 1)
    )
    tiled = np.tile(quantity_array, (redshift_and_rate_array.shape[0], 1))

    # hist, bin_edges = apply_along_axis(lambda x, y: histogram(x, bins=bins, weights=y), 0, B)
    # Loop over the redshift keys and fill the new array with the result of the histogram
    for redshift_i in range(masked_redshift_and_rate_array.shape[0]):
        #
        hist = np.histogram(
            masked_quantity_array,
            bins=quantity_bins,
            weights=masked_redshift_and_rate_array[redshift_i],
        )
        binned_quantity_and_redshift_array[redshift_i] = hist[0]

    print(binned_quantity_and_redshift_array)


def generate_bins_from_keys(input_array):
    """
    Function to generate the bins that the array of keys represents
    """

    stepsize = np.around(min(input_array[1:] - input_array[:-1]), decimals=10)
    bins = np.arange(
        input_array.min() - 0.5 * stepsize, input_array.max() + 1.5 * stepsize, stepsize
    )

    return bins


def flatten_arrays(redshift_and_rate_array, redshift_array, quantity_array):
    """
    Function to turn the quantity and redshift array in the same shape as the rate array and flatten them all

    This function assumes that everything is masked already
    """

    print(redshift_array[:, np.newaxis].shape)

    # Take quantity array, expand it and flatten it
    flat_quantity_array = (
        np.ones(redshift_and_rate_array.shape) * quantity_array[np.newaxis, :]
    ).flatten()

    # Take redshift array, expand it and flatten it
    flat_redshift_array = (
        np.ones(redshift_and_rate_array.shape) * redshift_array[:, np.newaxis]
    ).flatten()

    # Take redshift_and_rate_array and flatten it
    flat_redshift_and_rate_array = redshift_and_rate_array.flatten()

    return flat_quantity_array, flat_redshift_array, flat_redshift_and_rate_array


# # Create the data structure s.t. we can plot it
# flat_quantity_array, flat_redshift_array, flat_redshift_and_rate_array = flatten_arrays(masked_redshift_and_rate_array, redshift_array, masked_quantity_array)


def readout_rates_all_redshifts(
    datafile, rate_groupname, main_groupname="compas_style_data", max_redshift=10
):
    """
    Function to readout the merger rate data from the hdf5 file and return a numpy array
    """

    #
    h5file = h5py.File(datafile, "r")

    # Get keys and create redshift array
    new_keys = list(h5file["{}/{}".format(main_groupname, rate_groupname)].keys())
    redshift_array = np.array(new_keys, dtype="float")
    redshift_array = redshift_array[redshift_array <= max_redshift]

    # make output array
    amt_systems = h5file[
        "{}/{}/{}".format(main_groupname, rate_groupname, redshift_array[0])
    ][()]
    redshift_and_rate_array = np.zeros(shape=(len(redshift_array), len(amt_systems)))

    # Readout the data
    for redshift_i, redshift_key in enumerate(redshift_array):
        print(redshift_i, redshift_key)

        if float(redshift_key) < max_redshift:
            rate_array = h5file[
                "/{}/{}/{}".format(main_groupname, rate_groupname, redshift_key)
            ][()]
            redshift_and_rate_array[redshift_i] = rate_array
    h5file.close()

    return redshift_array, redshift_and_rate_array


def create_full_array(
    combined_df, hdf5_filename, redshift_key, rate_type_hdf5_key="merger_rate_data"
):
    """
    Function to turn the sparse array into a full array of rates
    """

    # Calculate the histogram
    rate_df = pd.read_hdf(
        hdf5_filename, "/data/{}/{}".format(rate_type_hdf5_key, str(redshift_key))
    )
    rate_df = rate_df.set_index("local_index")

    # Join
    joined_df = combined_df.join(rate_df, on="local_index", how="left")

    # Set the nan values to 0
    joined_df["merger_rates"] = joined_df["merger_rates"].replace(np.nan, 0)

    return joined_df["merger_rates"].to_numpy()


def copy_and_convert_data(input_file, output_file, convert_merger_rate_data=True):
    """
    Function to convert a hdf5 file to have rate data that can contain zeros
    """

    # copy file
    shutil.copyfile(file, output_file)

    h5file = h5py.File(output_file, "r")
    print(h5file.keys())
    h5file.close()

    # Add group
    h5file = h5py.File(output_file, "a")
    h5file.create_group("compas_style_data")
    h5file.create_group("compas_style_data/merger_rates")
    h5file.close()

    h5file = h5py.File(output_file, "r")
    redshift_keys_binary_c = list(h5file["data/merger_rate_data"].keys())
    h5file.close()

    # Get combined df
    combined_df = pd.read_hdf(output_file, "/data/combined_dataframes")

    # Drop all other columns
    drop_columns = [el for el in combined_df.columns if not el == "mass_1"]
    combined_df = combined_df.drop(columns=drop_columns)

    # Handle indices
    combined_df = combined_df.reset_index(drop=False)

    # loop
    for redshift_key in redshift_keys_binary_c:
        print(redshift_key)
        # create rate array
        rate_array = create_full_array(
            combined_df,
            output_file,
            redshift_key,
            rate_type_hdf5_key="merger_rate_data",
        )

        # Write to hdf5
        h5file = h5py.File(output_file, "a")
        h5file.create_dataset(
            "/compas_style_data/merger_rates/{}".format(str(redshift_key)),
            data=rate_array,
        )
        # TODO: add conversion of detection rate and birth rate in here
        h5file.close()

    h5file = h5py.File(output_file, "r")
    print(h5file["compas_style_data/merger_rates"].keys())
    h5file.close()


def dco_type_query(dco_type):
    """
    Function to filter the correct dco type in a dataframe

    dco_type can be 'bhbh', 'bhns', 'nsns', 'combined_dco'
    """

    if dco_type == "bhbh":
        return "stellar_type_1 == 14 & stellar_type_2 == 14"
    if dco_type == "nsns":
        return "stellar_type_1 == 13 & stellar_type_2 == 13"
    if dco_type == "bhns":
        return "(stellar_type_1 == 14 & stellar_type_2 == 13) | (stellar_type_1 == 13 & stellar_type_2 == 14)"
    if dco_type == "combined_dco":
        return "mass_1==mass_1"
    else:
        msg = "dco_type '{}' is unknown. Abort".format(dco_type)
        raise ValueError(msg)


#
file = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/without_detection_probability/rebinned_convolution_results.h5"
output_file = "test.hdf5"

# Get combined df
combined_df = pd.read_hdf(output_file, "/data/combined_dataframes")
combined_df["total_mass"] = combined_df["mass_1"] + combined_df["mass_2"]

# Get DCO mask
dco_query = dco_type_query("bhbh")
dco_mask = combined_df.eval(dco_query).to_numpy()

# Get general query mask
general_query = "total_mass <= 20"
general_query_mask = combined_df.eval(general_query).to_numpy()

# combined_mask = dco_mask * general_query_mask
combined_mask = dco_mask

# Readout the quantity that we want to plot
quantity_array = combined_df["total_mass"].to_numpy()
masked_quantity_array = quantity_array * combined_mask

# Readout and compose the redshift_and_rate_array and masking
redshift_array, redshift_and_rate_array = readout_rates_all_redshifts(
    output_file, rate_groupname="merger_rates", max_redshift=3
)
masked_redshift_and_rate_array = redshift_and_rate_array * combined_mask

#
redshift_bins = generate_bins_from_keys(redshift_array)
quantity_bins = np.arange(0, 120, 2)


create_binned_quantity_and_redshift_array(
    masked_redshift_and_rate_array, masked_quantity_array, quantity_bins
)
