import os
import json
import time
import pickle
import sys
import shutil

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import (
    get_help_all,
    get_help,
    create_hdf5,
    output_lines,
)

# from david_phd_functions.binaryc.personal_defaults import personal_defaults

"""
Script to run the CEMP grid for rob
"""


def parse_function(self, output):
    """
    Parse function for binary compact objects
    """

    output_file = os.path.join(self.custom_options["data_dir"], "sampling.txt")

    headers = ["mass_1", "mass_2", "period", "probability"]
    seperator = "\t"

    if not os.path.isfile(output_file):
        with open(output_file, "w") as output_filehandle:
            output_filehandle.write(seperator.join(headers) + "\n")

    #
    with open(output_file, "a+") as output_filehandle:
        #
        if output:
            for line in output.splitlines():
                print(line)
                if line.startswith("DAVID_COMPARE_TEST"):
                    values = line.split()[1:]

                    output_filehandle.write(seperator.join(values[1:]) + "\n")


## Make population and set value
test_pop = Population()
test_pop.set()

# Set values
test_pop.set(
    # Grid configuration
    max_evolution_time=15000,  # Myr
    combine_ensemble_with_thread_joining=True,
    minimum_timestep=1e-6,
    amt_cores=1,  # number of threads,
    # amt_cores=20, # number of threads,
    verbosity=5,
    metallicity=0.02,  # mass fraction of "metals"
    tmp_dir="template_results/python/local_tmp_dir",
    data_dir="template_results/python/results",
    parse_function=parse_function,
)

# To log the initial sampling
test_pop.set(
    C_logging_code="""
if (stardata->model.time > 0)
{
    Printf("DAVID_COMPARE_TEST %30.12e " // 1
        "%g %g %g %g\\n", // 2-5

        //
        stardata->model.time, // 1

        stardata->common.zero_age.mass[0], // 2
        stardata->common.zero_age.mass[1], // 3
        stardata->common.zero_age.orbital_period[0], // 4
        stardata->preferences->initial_probability // 5
    );
    /* Kill the simulation to save time */
    stardata->model.max_evolution_time = stardata->model.time - stardata->model.dtm;
};
"""
)

#
resolutions = {"M_1": 10, "q": 10, "per": 10}

#
test_pop.add_grid_variable(
    name="lnm1",
    longname="Primary mass",
    valuerange=[0.1, 80],
    resolution="{}".format(resolutions["M_1"]),
    spacingfunc="const(math.log(0.1), math.log(80.0), {})".format(resolutions["M_1"]),
    precode="M_1=math.exp(lnm1)",
    probdist="Kroupa2001(M_1)*M_1",
    dphasevol="dlnm1",
    parameter_name="M_1",
    gridtype="centred",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
)

test_pop.add_grid_variable(
    name="q",
    longname="Mass ratio",
    valuerange=["0.1/M_1", 1],
    resolution="{}".format(resolutions["q"]),
    spacingfunc="const(0.1/M_1, 1, {})".format(resolutions["q"]),
    probdist="flatsections(q, [{'min': 0.1/M_1, 'max': 1.0, 'height': 1}])",
    dphasevol="dq",
    precode="M_2 = q * M_1",
    parameter_name="M_2",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
    gridtype="centred",
)

test_pop.add_grid_variable(
    name="log10per",  # in days
    longname="log10(Orbital_Period)",
    valuerange=[-2.0, 12.0],
    resolution="{}".format(resolutions["per"]),
    spacingfunc="const(-2.0, 12.0, {})".format(resolutions["per"]),
    precode="""orbital_period = 10** log10per
sep = calc_sep_from_period(M_1, M_2, orbital_period)
""",
    probdist="gaussian(log10per, 4.8, 2.3, -2.0, 12.0)",
    parameter_name="orbital_period",
    dphasevol="dlog10per",
    gridtype="centred",
)

# create local tmp_dir
test_pop.set(tmp_dir=os.path.join(test_pop.custom_options["data_dir"], "local_tmp_dir"))

# Delete if it doesnt exist
if os.path.isdir(test_pop.grid_options["tmp_dir"]):
    shutil.rmtree(test_pop.grid_options["tmp_dir"])
os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

# Remove the old result file
if os.path.isfile(os.path.join(test_pop.custom_options["data_dir"], "sampling.txt")):
    os.remove(os.path.join(test_pop.custom_options["data_dir"], "sampling.txt"))

# Export settings:
test_pop.export_all_info(use_datadir=True)

# test_pop.write_binary_c_calls_to_file()

# Evolve grid
test_pop.evolve()

# # Get ensemble output and write it to file
# ensemble_output = test_pop.grid_ensemble_results

# with open(os.path.join(test_pop.custom_options['data_dir'], 'ensemble_output.json'), 'w') as f:
#     json.dump(ensemble_output, f, indent=4)
