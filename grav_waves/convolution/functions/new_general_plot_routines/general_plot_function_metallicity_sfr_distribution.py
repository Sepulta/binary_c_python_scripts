"""
General plot routine for the metallicity and sfr distribution
"""

import os

from PyPDF2 import PdfFileMerger
from david_phd_functions.plotting.pdf_functions import add_pdf_and_bookmark

from grav_waves.convolution.functions.new_plot_routines.sfr_metallicity_plot_functions import (
    plot_metallicity_probability_density_distribution,
    sfr_full_dpdlogZ,
    sfr_full_dpdlogZ_dlogZ,
    sfr_sampled_dpdlogZ,
    sfr_sampled_dpdlogZ_dlogZ,
    sfr_sampled_dpdlogZ_dlogZ_pdraw,
)


def general_plot_function(
    output_pdf_dir,
    combined_pdf_filename,
    redshifts,
    sfr_values,
    dPdlogZ,
    metallicities,
    sampled_metallicities,
    p_draw_metallicity,
    distribution,
):
    """
    Function to plot and combine all the a
    """

    show_plot = False
    os.makedirs(output_pdf_dir, exist_ok=True)

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    #
    print("Creating combined pdf for SFR and metallicity dist plots")

    # plot_metallicity_probability_density_distribution
    plot_metallicity_probability_density_distribution_filename = os.path.join(
        output_pdf_dir, "plot_metallicity_probability_density_distribution.pdf"
    )
    print("plotting plot_metallicity_probability_density_distribution")
    plot_metallicity_probability_density_distribution(
        redshifts,
        dPdlogZ,
        metallicities,
        sampled_metallicities,
        distribution,
        plot_settings={
            "show_plot": show_plot,
            "output_name": plot_metallicity_probability_density_distribution_filename,
        },
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        plot_metallicity_probability_density_distribution_filename,
        pdf_page_number,
        bookmark_text="plot_metallicity_probability_density_distribution",
    )

    # sfr_full_dpdlogZ
    sfr_full_dpdlogZ_filename = os.path.join(output_pdf_dir, "sfr_full_dpdlogZ.pdf")
    sfr_full_dpdlogZ(
        redshifts,
        sfr_values,
        dPdlogZ,
        metallicities,
        distribution,
        plot_settings={
            "show_plot": show_plot,
            "output_name": sfr_full_dpdlogZ_filename,
        },
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        sfr_full_dpdlogZ_filename,
        pdf_page_number,
        bookmark_text="sfr_full_dpdlogZ",
    )

    # sfr_full_dpdlogZ_dlogZ
    sfr_full_dpdlogZ_dlogZ_filename = os.path.join(
        output_pdf_dir, "sfr_full_dpdlogZ_dlogZ.pdf"
    )
    sfr_full_dpdlogZ_dlogZ(
        redshifts,
        sfr_values,
        dPdlogZ,
        metallicities,
        distribution,
        plot_settings={
            "show_plot": show_plot,
            "output_name": sfr_full_dpdlogZ_dlogZ_filename,
        },
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        sfr_full_dpdlogZ_dlogZ_filename,
        pdf_page_number,
        bookmark_text="sfr_full_dpdlogZ_dlogZ",
    )

    # Sampled values

    # sfr_sampled_dpdlogZ
    sfr_sampled_dpdlogZ_filename = os.path.join(
        output_pdf_dir, "sfr_sampled_dpdlogZ.pdf"
    )
    sfr_sampled_dpdlogZ(
        redshifts,
        sfr_values,
        dPdlogZ,
        metallicities,
        sampled_metallicities,
        distribution,
        plot_settings={
            "show_plot": show_plot,
            "output_name": sfr_sampled_dpdlogZ_filename,
        },
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        sfr_sampled_dpdlogZ_filename,
        pdf_page_number,
        bookmark_text="sfr_sampled_dpdlogZ",
    )

    # sfr_sampled_dpdlogZ_dlogZ
    sfr_sampled_dpdlogZ_dlogZ_filename = os.path.join(
        output_pdf_dir, "sfr_sampled_dpdlogZ_dlogZ.pdf"
    )
    sfr_sampled_dpdlogZ_dlogZ(
        redshifts,
        sfr_values,
        dPdlogZ,
        metallicities,
        sampled_metallicities,
        distribution,
        plot_settings={
            "show_plot": show_plot,
            "output_name": sfr_sampled_dpdlogZ_dlogZ_filename,
        },
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        sfr_sampled_dpdlogZ_dlogZ_filename,
        pdf_page_number,
        bookmark_text="sfr_sampled_dpdlogZ_dlogZ",
    )

    # sfr_sampled_dpdlogZ_dlogZ_pdraw
    sfr_sampled_dpdlogZ_dlogZ_pdraw_filename = os.path.join(
        output_pdf_dir, "sfr_sampled_dpdlogZ_dlogZ_pdraw.pdf"
    )
    sfr_sampled_dpdlogZ_dlogZ_pdraw(
        redshifts,
        sfr_values,
        dPdlogZ,
        metallicities,
        sampled_metallicities,
        p_draw_metallicity,
        distribution,
        plot_settings={
            "show_plot": show_plot,
            "output_name": sfr_sampled_dpdlogZ_dlogZ_pdraw_filename,
        },
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        sfr_sampled_dpdlogZ_dlogZ_pdraw_filename,
        pdf_page_number,
        bookmark_text="sfr_sampled_dpdlogZ_dlogZ_pdraw",
    )

    # wrap up the pdf
    merger.write(combined_pdf_filename)
    merger.close()
    print("\t wrote combined pdf to {}".format(combined_pdf_filename))

    print("Finished generating plots")
