"""
Function to plot the shell volumes
"""


import copy
import pickle
import json
import h5py
import numpy as np
import pandas as pd

import astropy.units as u

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors

from grav_waves.convolution.functions.convolution_functions import (
    generate_metallicity_sfr_array,
)
from grav_waves.convolution.functions.redshift_interpolation_functions import (
    test_redshift_to_lookback_time,
    test_lookback_time_to_redshift,
    create_interpolation_datasets,
)
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot

#
# matplotlib.use('Agg')

#
number_per_gigaparsec_cubed_per_year = 1 / u.Gpc**3 / u.yr
sfr_unit = u.Msun / u.Gpc**3 / u.yr


def plot_comoving_shell_volumes(convolution_results, plot_settings=None):
    """
    Function to plot the comoving shell volumes that correspond to the current dataset
    """

    if not plot_settings:
        plot_settings = {}

    # Open settings
    convolution_configuration = convolution_results["convolution_configuration"]

    redshift_volume_convolution_results = convolution_results[
        "redshift_volume_convolution_results"
    ]

    # Readout time centers
    time_centers = convolution_configuration["time_centers"]

    #
    shell_volume_dict = redshift_volume_convolution_results["shell_volume_dict"]

    # Readout the shell volumes themselves
    shell_volumes_sorted = (
        np.array(
            [
                shell_volume_dict[el]["shell_volume"].value
                for el in sorted(shell_volume_dict.keys())
            ]
        )
        * list(shell_volume_dict.values())[0]["shell_volume"].unit
    )

    #####
    # Plotting

    # Set up figure
    fig = plt.figure(figsize=(16, 16))

    # Set up gridspec
    gs = fig.add_gridspec(nrows=1, ncols=1)

    # Create axis
    ax_shell_volumes = fig.add_subplot(gs[0, 0])

    # Plot shell volumes on axis
    ax_shell_volumes.plot(time_centers, shell_volumes_sorted, "ro")

    # Limit range of time that we plot
    ax_shell_volumes.set_xlim(
        plot_settings.get("min_plot_time", ax_shell_volumes.get_xlim()[0]),
        plot_settings.get("max_plot_time", ax_shell_volumes.get_xlim()[1]),
    )

    # Makeup
    ax_shell_volumes.set_xlabel("Redshift")
    ax_shell_volumes.set_ylabel("Shell volume [{}]".format(shell_volumes_sorted.unit))
    ax_shell_volumes.set_title(
        "Size of conscecutive comoving shells for dz={}".format(
            np.diff(time_centers)[0]
        )
    )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
