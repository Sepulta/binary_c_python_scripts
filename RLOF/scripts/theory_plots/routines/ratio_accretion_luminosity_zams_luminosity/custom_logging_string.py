custom_logging_string = """
if (stardata->model.time < stardata->model.max_evolution_time)
{
    Printf("DAVID_SN " //
        "%g %g %g %g " // 2-5
        "%g\\n",

        //
        stardata->star[0].mass,
        stardata->star[0].luminosity,
        Teff(0),
        stardata->star[0].radius,

        stardata->star[0].tkh
    );
};
/* Kill the simulation after the first step */
stardata->model.max_evolution_time = stardata->model.time - stardata->model.dtm;
"""
