"""
Function to plot the panel for the primary mass distribution

TODO: add optional panel: absolute difference to the fiducial
TODO: add optional panel: fractional difference to the fiducial
"""

import os
import json
import h5py
import numpy as np
import pandas as pd

import astropy.units as u

import matplotlib
import matplotlib.pyplot as plt

from grav_waves.gw_analysis.functions.cosmology_functions import (
    redshift_to_lookback_time,
)
from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    filter_dco_type,
    get_rate_type_names,
    linestyle_list,
    add_contour_levels,
    get_num_subsets,
    plot_2d_results,
)
from grav_waves.convolution.functions.redshift_interpolation_functions import (
    load_interpolation_data,
)

from david_phd_functions.grav_waves.dataframe_functions import add_chirpmass_column
from david_phd_functions.plotting.canvas_functions import return_canvas_with_subsets
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


import h5py
import numpy as np
import pandas as pd

import astropy.units as u

from scipy import stats

import matplotlib.pyplot as plt

import matplotlib
from matplotlib import colors

from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    create_any_mass_df,
    filter_dco_type,
    get_rate_type_names,
    color_list,
    hatch_list,
)
from grav_waves.convolution.functions.plot_routines.plot_kde_and_bootstrap_functions import (
    calculate_kde_values,
    run_bootstrap,
)

from david_phd_functions.grav_waves.dataframe_functions import add_chirpmass_column
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def get_data(
    hdf5_filename,
    dco_type,
    redshift_key,
    bins,
    mass_quantity,
    rate_key_in_df,
    rate_type_hdf5_key,
    divide_by_mass_bins,
    bootstraps=0,
    num_procs=1,
    query=None,
):
    """
    Function to get the data out of the hdf5 file and query if necessary
    """

    #######
    # Read out the dataframe and query
    # Get combined df
    combined_df = pd.read_hdf(hdf5_filename, "/data/combined_dataframes")

    # Add columns
    combined_df = add_chirpmass_column(combined_df, m1_name="mass_1", m2_name="mass_2")
    combined_df["total_mass"] = combined_df["mass_1"] + combined_df["mass_2"]
    combined_df["primary_mass"] = combined_df[["mass_1", "mass_2"]].max(axis=1)
    combined_df["secondary_mass"] = combined_df[["mass_1", "mass_2"]].min(axis=1)

    #
    index_df = pd.read_hdf(
        hdf5_filename, "/data/{}/{}".format(rate_type_hdf5_key, str(redshift_key))
    )
    joined_df = index_df.join(combined_df, on="local_index")

    # Filter the dco type:
    joined_df = filter_dco_type(joined_df, dco_type)

    if not query is None:
        joined_df = joined_df.query(query)

    #
    data_array = joined_df[mass_quantity]
    weight_array = joined_df[rate_key_in_df]

    # Determine the mass bins
    bin_size = np.diff(bins)
    bincenter = (bins[1:] + bins[:-1]) / 2

    # bin and take into account the divison by mass
    hist = np.histogram(data_array, bins=bins, weights=weight_array)[0]
    if divide_by_mass_bins:
        hist = hist / mass_bin_size

    # Select the non-zero bins and split off the empty ones
    # NOTE: without this, toms method does not work
    non_zero_bins_indices = np.nonzero(hist)[0]
    bins = bins[non_zero_bins_indices.min() : non_zero_bins_indices.max() + 1]

    #
    return_dict = {
        "mass_centers": bincenter,
        "rates": hist,
    }

    ########################
    # KDE
    x_KDE_width = 0.1
    kde_width = 0.1 * bin_size[0]

    x_KDE = np.arange(bins.min(), bins.max() + x_KDE_width, x_KDE_width)
    center_KDEbins = (x_KDE[:-1] + x_KDE[1:]) / 2.0

    # calculate_kde_values
    y_vals = calculate_kde_values(
        data=data_array,
        weights=weight_array,
        lower_bound=data_array.min(),
        upper_bound=data_array.max(),
        bw_method=kde_width,
        center_KDEbins=center_KDEbins,
        hist=hist,
    )

    #
    return_dict["kde_yvals"] = y_vals
    return_dict["center_KDEbins"] = center_KDEbins

    ########################
    # Bootstrap
    if bootstraps:

        # TODO: Ask lieke why do we use x_KDE here instead of center_KDEbins
        _, median, percentiles = run_bootstrap(
            masses=data_array,
            weights=weight_array,
            indices=data_array.index,
            kde_width=kde_width,
            mass_bins=bins,
            center_KDEbins=center_KDEbins,
            num_procs=num_procs,
            bootstraps=bootstraps,
        )

        #
        return_dict["median"] = median
        return_dict["percentiles"] = percentiles

    #
    return return_dict


def plot_primary_mass_distribution(
    dataset_file_dict,
    alpha_dict,
    rate_type,
    dco_type,
    num_procs,
    bootstraps,
    bins,
    plot_settings,
):
    """ """

    mass_quantity = "primary_mass"

    # Select correct rate type:
    if rate_type == "merger_rate":
        rate_key_in_df = "merger_rates"
        long_name_rate_type = "Merger rate"
        rate_type_hdf5_key = "merger_rate_data"
    elif rate_type == "formation_rate":
        rate_key_in_df = "formation_rates"
        long_name_rate_type = "Formation rate"
        rate_type_hdf5_key = "formation_rate_data"

    # Get configuration and interpolators
    hdf5_file = h5py.File(dataset_file_dict["fiducial"], "r")
    settings = hdf5_file["settings"]
    convolution_configuration = json.loads(settings["convolution_configuration"][()])
    zero_key = str(
        min([float(el) for el in list(hdf5_file["data"][rate_type_hdf5_key].keys())])
    )
    hdf5_file.close()

    # Get data
    results = {}
    for dataset_name in dataset_file_dict:
        results[dataset_name] = get_data(
            hdf5_filename=dataset_file_dict[dataset_name],
            dco_type=dco_type,
            redshift_key=zero_key,
            bins=bins,
            mass_quantity=mass_quantity,
            rate_key_in_df=rate_key_in_df,
            rate_type_hdf5_key=rate_type_hdf5_key,
            divide_by_mass_bins=False,
            bootstraps=bootstraps,
            num_procs=num_procs,
        )

    ##
    # Set up figure

    fig = plt.figure(figsize=(20, 20))
    gs = matplotlib.gridspec.GridSpec(nrows=1, ncols=1, figure=fig)

    #
    axis_primary_mass_distribution = fig.add_subplot(gs[0, 0])

    #########
    # Plot the histograms
    for dataset_i, dataset_name in enumerate(dataset_file_dict):
        # axis_primary_mass_distribution.plot(
        #     results[dataset_name]['mass_centers'],
        #     results[dataset_name]['rates'],
        #     lw=5,
        #     c=color_list[dataset_i],
        #     zorder=200,
        #     linestyle='--'
        # )

        # plot the KDE values
        axis_primary_mass_distribution.plot(
            results[dataset_name]["center_KDEbins"],
            results[dataset_name]["kde_yvals"],
            lw=8,
            c=color_list[dataset_i],
            label=dataset_name,
            zorder=13,
            alpha=alpha_dict[dataset_name],
        )

        if bootstraps:
            print("filling")
            axis_primary_mass_distribution.fill_between(
                results[dataset_name]["center_KDEbins"],
                results[dataset_name]["percentiles"][0],
                results[dataset_name]["percentiles"][1],
                alpha=0.4,
                zorder=11,
                color=color_list[dataset_i],
            )  # 1-sigma
            axis_primary_mass_distribution.fill_between(
                results[dataset_name]["center_KDEbins"],
                results[dataset_name]["percentiles"][2],
                results[dataset_name]["percentiles"][3],
                alpha=0.2,
                zorder=10,
                color=color_list[dataset_i],
            )  # 2-sigma

    #
    axis_primary_mass_distribution.set_xlim([bins.min(), bins.max()])
    axis_primary_mass_distribution.set_yscale("log")
    axis_primary_mass_distribution.legend()

    axis_primary_mass_distribution.set_xlabel("Primary mass distribution")

    #
    long_name_rate_type = "Merger rate"
    number_density_per_year = 1 / u.Gpc**3 / u.yr
    plotting_unit = number_density_per_year
    xlabel_text = "{}".format(mass_quantity) + " [{}]".format(
        u.Msun.to_string("latex_inline")
    )

    axis_primary_mass_distribution.set_xlabel(xlabel_text)
    axis_primary_mass_distribution.set_ylabel(
        "{} density for {}".format(long_name_rate_type, mass_quantity)
        + r" [{}]".format(plotting_unit.unit.to_string("latex_inline"))
    )

    # Add info and plot the figure
    # fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    # # Global settings
    fiducial_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/without_detection_probability/rebinned_convolution_results.h5"
    ten_extra_massloss_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-10_CORE_MASS_SHIFT/convolution_results/without_detection_probability/rebinned_convolution_results.h5"
    twenty_extra_massloss_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_20_EXTRA_MASSLOSS_PPISN/convolution_results/without_detection_probability/rebinned_convolution_results.h5"

    dataset_file_dict = {
        "fiducial": fiducial_dataset,
        "Ten Msun extra loss": ten_extra_massloss_dataset,
        "Twenty Msun extra loss": twenty_extra_massloss_dataset,
    }

    #
    plot_primary_mass_distribution(
        dataset_file_dict=dataset_file_dict,
        alpha_dict={},
        rate_type="merger_rate",
        dco_type="bhbh",
        num_procs=5,
        bootstraps=0,
        bins=np.arange(0, 100, 1),
        plot_settings={
            "show_plot": True,
            "output_name": "output/plot_primary_mass_distribution_extra_mass_loss.pdf",
            "simulation_name": "test",
            "antialiased": True,
            "rasterized": True,
            "runname": "plot_primary_mass_distribution with extra mass loss sets",
        },
    )

    # Global settings
    fiducial_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/without_detection_probability/rebinned_convolution_results.h5"
    five_core_mass_shift_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-5_CORE_MASS_SHIFT/convolution_results/without_detection_probability/rebinned_convolution_results.h5"
    ten_core_mass_shift_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-10_CORE_MASS_SHIFT/convolution_results/without_detection_probability/rebinned_convolution_results.h5"

    dataset_file_dict = {
        "fiducial": fiducial_dataset,
        "Five Msun core mass shift": five_core_mass_shift_dataset,
        "Ten Msun Core mass shift": ten_core_mass_shift_dataset,
    }

    alpha_dict = {
        "fiducial": 1,
        "Five Msun core mass shift": 1,
        "Ten Msun Core mass shift": 1,
    }

    #
    plot_primary_mass_distribution(
        dataset_file_dict=dataset_file_dict,
        alpha_dict={},
        rate_type="merger_rate",
        dco_type="bhbh",
        num_procs=5,
        bootstraps=0,
        bins=np.arange(0, 100, 1),
        plot_settings={
            "show_plot": True,
            "output_name": "output/plot_primary_mass_distribution_mass_shift.pdf",
            "simulation_name": "test",
            "antialiased": True,
            "rasterized": True,
            "runname": "plot_primary_mass_distribution with core mass shift sets",
        },
    )

else:
    matplotlib.use("Agg")
