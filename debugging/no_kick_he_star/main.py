from binarycpython.utils.grid import Population
from david_phd_functions.binaryc.personal_defaults import personal_defaults


def parse_function(self, output):
    # print(repr(output))

    print(output)

    # for line in output.splitlines():
    #     if line.startswith("SPLITINFO"):
    #         print(line)


test_pop = Population()

test_pop.set(verbosity=2)
test_pop.set(parse_function=parse_function)

test_pop.set(**personal_defaults)

test_pop.set(
    amt_cores=1,
    # amt_cores=24,
    metallicity=0.00002,
    orbital_period=1e20,
    M_1=150,
    M_2=210,
    wind_mass_loss=2,
    BH_prescription=4,
    log_runtime_systems=0,
    PPISN_prescription=1
    # evolution_splitting=1,
    # evolution_splitting_sn_n=1,
    # evolution_splitting_maxdepth=2,
)

# Wrap in loop
output_1 = test_pop.evolve_single()
