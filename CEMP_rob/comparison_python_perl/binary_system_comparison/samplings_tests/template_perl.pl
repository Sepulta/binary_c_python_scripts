#!/usr/bin/env perl
use strict; # highly recommended
use 5.16.0; # highly recommended
use distribution_functions;
use binary_grid2; # required
use binary_grid::C; # required (uses C backend )
use rob_misc qw/ncpus/;

#
my $nthreads = 1;

# make a new stellar population
my $population = binary_grid2->new(
    # options can be given now ...
    metallicity => 0.02, # mass fraction of "metals"
    max_evolution_time => 15000, # Myr
    nthreads=>$nthreads, # number of threads,
    check_args=>1, # check that args exist
    vb=>1, # turn on verbose logging (can be 0,1,2...)
    return_array_refs=>1, # quicker data parsing mode
    log_args=>1,
    sort_args=>1,
    save_args=>1,
    log_args_dir=>'template_results/perl/tmp_dir',
    tmp=>'template_results/perl/tmp_dir',
    minimum_timestep=>1e-6,
);

$population->set(
   C_logging_code => '
if (stardata->model.time > 0)
{
    Printf("DAVID_COMPARE_TEST %30.12e " // 1
        "%g %g %g %g\\n", // 2-5

        //
        stardata->model.time, // 1

        stardata->common.zero_age.mass[0], // 2
        stardata->common.zero_age.mass[1], // 3
        stardata->common.zero_age.orbital_period[0], // 4
        stardata->preferences->initial_probability // 5
    );
    /* Kill the simulation to save time */
    stardata->model.max_evolution_time = stardata->model.time - stardata->model.dtm;
};',
);

# check structures are up to date
exit if($population->check_structure_sizes());

############################################################
#
# Now we set up the grid of stars.
#
# duplicity is 0 for single stars, 1 for binary stars
# and 2 for a mixed population sampled at equal times
my $duplicity = 1;
$population->{_grid_options}{multiplicity} =
    $duplicity == 0 ? 1 : 2;

# make a population of binary stars
my $nres = 10;
my $resolution = {
    m1 => $nres,
    q => $nres,
    P => $nres,
};
my $mmin = 0.1;
my $mmax = 80.0;

# Mass
$population->add_grid_variable(
    'name'       =>'lnm1',
    'longname'   =>'Primary mass',
    'range'      =>[log($mmin),log($mmax)],
    'resolution' => $resolution->{m1}, # just a counter for the grid
    'spacingfunc'=>"const(log($mmin),log($mmax),$resolution->{m1})",
    'precode'    =>'$m1=exp($lnm1);',
    'probdist'   =>'Kroupa2001($m1)*$m1',
    'method'     =>'grid',
    'gridtype'   =>'centred',
);

# q=M1/M2 distribution flat in q between 0.1/M1 and 1.0
$population->add_grid_variable
    (
    'name'       =>'q',
    'longname'   =>'Mass ratio',
    'range'      =>['0.1/$m1',1.0],
    'resolution'=>$resolution->{q},
    'spacingfunc'=>"const(0.1/\$m1,1.0,$resolution->{q})",
    'precode'     =>'$m2=$q*$m1;',
    'probdist'   =>'
flatsections($q,[{min=>0.1/$m1,max=>1.0,height=>1.0}])',
    'method'     => 'grid',
    'gridtype'   => 'centred',
);

# orbital period Duquennoy and Mayor 1991 distribution
my $Prange = [-2.0,12.0];
$population->add_grid_variable
     (
      'name'       =>'logper',
      'longname'   =>'log(Orbital_Period)',
      'range'      =>$Prange,
      'resolution' =>$resolution->{P},
      'spacingfunc'=>"const($Prange->[0],$Prange->[1],$resolution->{P})",
      'precode'=>'
$per = 10.0 ** $logper;
my $eccentricity = 0.0;
$sep = calc_sep_from_period($m1,$m2,$per) if(defined $m1 && defined $m2);
',
      'probdist'=>"gaussian(\$logper,4.8,2.3,$Prange->[0],$Prange->[1])",
      'method'     =>'grid',
      'gridtype'   => 'centred',
);

############################################################
# link population to custom data parser function
$population->set(
    parse_bse_function_pointer => \&main::parse_data
    );

my %init = $population->initial_abundance_hash('Karakas2002',0.02);
my %isotope_hash = $population->isotope_hash();
my @isotope_list = $population->isotope_list();
my %nuclear_mass_hash = $population->nuclear_mass_hash();
my @nuclear_mass_list = $population->nuclear_mass_list();
my @sources = $population->source_list();
my @ensemble = $population->ensemble_list();

# you can use Data::Dumper to see the contents
# of the above lists and hashes
if(0){
    print Data::Dumper->Dump([
        #\%init,
        #\%isotope_hash,
        #\@isotope_list,
        #\%nuclear_mass_hash,
        \@nuclear_mass_list,
        #\@sources,
        #\@ensemble
                         ]);
}


# Check if file exists
my $filename = 'template_results/perl/results/sampling.txt';
if (-e $filename) {
    print "file exits. Removing file";
    unlink $filename;
}

# evolution the stellar population (this takes some time)
$population->evolve();

# done : exit
exit;

############################################################
# subroutines
############################################################

sub parse_data
{
    my ($population, $results) = @_;

    #
    my $filename = 'template_results/perl/results/sampling.txt';
    unless (-e $filename) {
        open(FH, '>', $filename) or die $!;
        print FH "mass\tprobability\n";
        close(FH);
    }

    while(1)
    {
        # subsequent calls to tbse_line contain
        # (references to) arrays of data
        my $la = $population->tbse_line();

        # first element is the "header" line
        my $header = shift @$la;

        # break out of the loop if this is 'fin'
        last if ($header eq 'fin');

        # check if $header matches one of your
        # expected data lines, if so, act
        if($header eq 'DAVID_COMPARE_TEST')
        {
            my $time = $la->[0];
            my $mass_1 = $la->[1];
            my $mass_2 = $la->[2];
            my $period = $la->[3];
            my $probability = $la->[4];

            print "$mass_1 $mass_2 $period $probability\n";

            open(FH, '>>', $filename) or die $!;
            print FH "$mass_1 $mass_2 $period $probability\n";
            close(FH);
        }
    }
}
