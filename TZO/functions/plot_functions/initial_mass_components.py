"""
Function to plot the initial primary and secondary mass for systems that end up as TZO
"""

import os

import pandas as pd
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors

from david_phd_functions.plotting.utils import show_and_save_plot, add_plot_info
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def getRoundedThresholdv1(a, MinClipBins):
    intermediate = (MinClipBins[1:] + MinClipBins[:-1]) / 2
    return MinClipBins[np.discritize(a, intermediate)]


def plot_initial_mass_components(df, plot_settings={}):
    """
    Function to plot the delay time given a dataframe
    """

    #
    df["log10_zams_mass_1"] = np.log10(df["zams_mass_1"])
    df["log10_zams_mass_2"] = np.log10(df["zams_mass_2"])

    #
    mass_stepsize = 0.1

    min_zams_mass_1 = (
        np.floor(df["log10_zams_mass_1"].min() / mass_stepsize) * mass_stepsize
    )
    max_zams_mass_1 = (
        np.ceil(df["log10_zams_mass_1"].max() / mass_stepsize) * mass_stepsize
    )

    min_zams_mass_2 = (
        np.floor(df["log10_zams_mass_2"].min() / mass_stepsize) * mass_stepsize
    )
    max_zams_mass_2 = (
        np.ceil(df["log10_zams_mass_2"].max() / mass_stepsize) * mass_stepsize
    )

    log10_zams_mass_1_bins = np.arange(
        min_zams_mass_1 - mass_stepsize,
        max_zams_mass_1 + 2 * mass_stepsize,
        mass_stepsize,
    )
    log10_zams_mass_2_bins = np.arange(
        min_zams_mass_2 - mass_stepsize,
        max_zams_mass_2 + 2 * mass_stepsize,
        mass_stepsize,
    )

    ##
    # pre-bin
    hist = np.histogram2d(
        df["log10_zams_mass_1"],
        df["log10_zams_mass_2"],
        bins=[log10_zams_mass_1_bins, log10_zams_mass_2_bins],
        weights=df["probability"],
    )

    # Calculate norm
    norm = colors.LogNorm(
        # vmin=10**np.floor(np.log10(all_main_results.value.max())-plot_settings.get('probability_floor', 4)),
        vmin=hist[0][hist[0] > 0].min(),
        vmax=hist[0][hist[0] > 0].max(),
    )

    ######
    # Plot
    fig = plt.figure(figsize=(20, 20))

    #
    gs = fig.add_gridspec(nrows=1, ncols=11)

    #
    ax = fig.add_subplot(gs[:, :10])
    ax_cb = fig.add_subplot(gs[:, 10])

    #
    ax.hist2d(
        df["log10_zams_mass_1"],
        df["log10_zams_mass_2"],
        bins=[log10_zams_mass_1_bins, log10_zams_mass_2_bins],
        weights=df["probability"],
        norm=norm,
    )
    # ax.set_yscale('log')

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        norm=norm,
    )
    cbar.ax.set_ylabel(r"Number of systems")

    #######################
    # Save and finish
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    root_dir = "/home/david/projects/binary_c_root/results/TZO/server_results/MID_RES_TESTING/population_results/"
    resultdir = os.path.join(root_dir, "Z0.01")

    data_filename = os.path.join(resultdir, "all_tzo_events.dat")
    df = pd.read_csv(data_filename, sep="\s+")

    # Exclude channels where the other is a compact object:
    filtered_df = df[df.prev_stellar_type_2 < 10]

    plot_initial_mass_components(filtered_df, plot_settings={"show_plot": True})
