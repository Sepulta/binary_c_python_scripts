"""
Parse function for the PPISN project
"""

import os

from binarycpython.utils.functions import output_lines

from event_based_logging_functions.events_parser import events_parser

parameters_DCO = [
    "uuid",  # 0
    "time",  # 1
    "mass_1",
    "zams_mass_1",
    "mass_2",
    "zams_mass_2",  # 2-5
    "stellar_type_1",
    "prev_stellar_type_1",
    "stellar_type_2",
    "prev_stellar_type_2",  # 6-9
    "metallicity",
    "probability",  # 10-11
    "separation",
    "eccentricity",
    "period",
    "merger_time",  # 12-15
    "zams_period",
    "zams_separation",
    "zams_eccentricity",
    "prev_eccentricity",  # 16-19
    "prev_period",
    "prev_separation",
    "prev_mass_1",
    "prev_core_mass_1",  # 20-23
    "prev_CO_core_mass_1",
    "prev_He_core_mass_1",
    "prev_mass_2",
    "prev_core_mass_2",  # 24-27
    "prev_CO_core_mass_2",
    "prev_He_core_mass_2",
    "fallback_1",
    "fallback_2",  # 28-31
    "fallback_mass_1",
    "fallback_mass_2",
    "random_seed",
    "undergone_ppisn_1",  # 32-35
    "undergone_ppisn_2",
    "formation_time_in_years",
    "inspiral_time_in_years",
    "merger_time_in_years",  # 36-39
    "comenv_counter",
    "rlof_counter",
    "stable_rlof_counter",
    "undergone_CE_with_HG_donor",  # 40-43
    "undergone_CE_with_MS_donor",
    "evol_hist_count",
    "evol_hist",  # 44-46
]


def parse_function(self, output):
    """
    Parse function for binary compact objects
    """

    separator = "\t"

    # Get some information from the object
    data_dir = self.custom_options["data_dir"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    ####################
    # Handle the DCO output

    # Set outfile name
    outfilename = os.path.join(
        data_dir, "compact_objects-{}.dat".format(self.process_ID)
    )

    # Create filename
    parameters = parameters_DCO

    # Check if the file exists
    if not os.path.exists(outfilename):
        with open(outfilename, "w") as f:
            f.write(separator.join(parameters) + "\n")

    # Loop over the output
    for line in output_lines(output):
        # Handle the DCO line
        if line.startswith("DAVID_DCO"):
            values = line.split()[1:]

            # Check if the length of the output is the same as we expect
            if not len(values) == len(parameters):
                print(
                    "Length of readout values ({}) is not equal to length of parameters ({})".format(
                        len(values), len(parameters)
                    )
                )
                raise ValueError

            # Here we take into account that the last element is a string
            with open(outfilename, "a") as f:
                f.write(
                    separator.join(values[: len(parameters) - 1])
                    + separator
                    + '"{}"'.format(separator.join(values[len(parameters) - 1 :]))
                    + "\n"
                )

    ####################
    # Handle the events output
    events_parser(self, separator, data_dir, output)


def parse_function_single(self, output):
    """
    Parse function for binary compact objects
    """

    separator = "\t"

    # Get some information from the object
    data_dir = self.custom_options["data_dir"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    ####################
    # Handle the events output
    events_parser(self, separator, data_dir, output)
