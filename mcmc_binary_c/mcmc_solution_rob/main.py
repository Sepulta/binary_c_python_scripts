#!/usr/bin/env python3.8

#
# Required modules: if you don't have one, run
#
# pip3.8 install <module>
#
# import compress_pickle
# import emcee
# import json
# import math
# import matplotlib
# import matplotlib.pyplot as plt
# from multiprocessing import Pool,cpu_count
# import numpy as np
# import os
# from pathlib import Path
# import pprint
# import pygtc
# import re
# import subprocess
import sys

# import types

############################################################
#
# Script to run binary_c many times in an MCMC
# test for the best parameter set for our disc
#
# Based loosely on the emcee example at
# https://emcee.readthedocs.io/en/develop/user/line/
#
############################################################
#
# Use the first command-line argument to specify a task
#
# 'generate' which makes new data using MCMC, plots and
#            saves the resulting data in a .pkl.gz file
#            which can be loaded.
#
# 'restart' loads the existing .pkl.gz file and then restarts
#           the MCMC to add new samples to the chains (note
#           they are restarted at random positions which is
#           not quite correct!).
#
# 'load' : loads pre-existing data from a .pkl.gz file
#          and outputs plots.
#
#
# The second (optional) argument is the pickled-object
# (.pkl.gz) filename. This is mcmc.pkl.gz by default.
#
############################################################

### General configuration:


############################################################
# Get arguments input
############################################################

############################################################
# get task (see above) from the command line
task = sys.argv[1] or "load"
if len(sys.argv) >= 3:
    savefile = sys.argv[2]
else:
    savefile = "mcmc.pkl.gz"  # default data filename
ncpus = cpu_count()
vb = False

# say hello to the terminal
print("binary_c-disc MCMC : task = ", task, " ncpus=", ncpus, " savefile=", savefile)

############################################################
# Observational statistics:
#
# We give these in the form of ranges, e.g.
#
# 'M_disc' : [6e-3,0.02]
#
# where 6e-3 is the lower mass limit, 0.02 is the upper mass
# limit. We then convert this to a mu+-sigma form, where
# mu is the (6e-3 + 0.02)/2 and sigma is (0.02 - 6e-3)/2.
#
# You can, naturally, use whatever you like here.
#
# note: this should be global to prevent multiple pickling
#
# you can comment out each observation you do not wish to test
#


obs = {
    "M_disc": [6e-3, 0.02],
    "J_disc": [4.8e51, 6.1e52],
    "R_out": [6e15, 2e16],
    "T_out": [36, 50],
    "Hout_Rout": [2.8e15 / 6e15, 4e15 / 2e16],
    "Mdot_visc": [0.5 * 1e-7, 1.5 * 1e-7],
    "Rin_a": [1.75, 2.25],
}
obs_stats = {"mean": {}, "sigma": {}, "sigma2": {}}
for o in obs:
    obs_stats["mean"][o] = (obs[o][0] + obs[o][1]) * 0.5
    obs_stats["sigma"][o] = abs(obs[o][1] - obs[o][0]) * 0.5
    obs_stats["sigma2"][o] = (obs_stats["sigma"][o]) ** 2

# show the results
pprint.pprint(obs_stats)

############################################################
# parameters
#
# You can choose:
#
#
# nparams = 6 : this allows you to vary loggamma and logkappa
#               as well as the four below.
#
# nparams = 4 : this fixes loggamma and logkappa,
#               and allows you to vary log fM, log fJ,
#               log alpha and log torquef.
#
nparams = 4

if nparams == 6:
    # set LaTeX labels for each parameter
    labels = [
        r"$\log_{10}(\alpha_\mathrm{disc})$",
        r"$\log_{10}(f_\mathrm{tid})$",
        r"$\log_{10}(\kappa)$",
        r"$\log_{10}(\gamma)$",
        r"$\log_{10}(f_\mathrm{M})$",
        r"$\log_{10}(f_\mathrm{J})$",
    ]

    # set up initial parameters in log space
    initial_parameters = [
        math.log10(1e-3),  # log alpha
        math.log10(1e-3),  # log torquef
        math.log10(0.01),  # log kappa
        math.log10(1.4),  # log gamma
        math.log10(0.01),  # log fM
        math.log10(0.1),
    ]  # log fJ
else:
    labels = [
        r"$\log_{10}(\alpha_\mathrm{disc})$",
        r"$\log_{10}(f_\mathrm{tid})$",
        r"$\log_{10}(f_\mathrm{M})$",
        r"$\log_{10}(f_\mathrm{J})$",
    ]

    initial_parameters = [
        math.log10(1e-3),  # log alpha -3.457 +2.442 -2.259
        math.log10(1e-3),  # log torquef -4.005 +1.347 -1.372
        math.log10(0.01),  # log fM +1.405 -0.402
        math.log10(0.1),
    ]  # log fJ +0.885 -0.356
    logkappa = math.log10(0.01)
    loggamma = math.log10(1.4)


if task == "generate" or task == "restart":

    ############################################################
    # initial random shift scale used to initialize the MCMC
    # walkers
    shift_scale = 1e-3

    ############################################################
    # number of walkers
    #
    # should be at least 2 * nparams but also
    # we should use all cpus if possible
    # (minus 2 to leave some for accountancy/failure)
    nwalkers = max(1, nparams * 2, ncpus - 2)

    ############################################################
    # max number of iterations of the MC
    #
    # there will be (maxiter+1)*nwalkers calls to run_star :
    # this may take a while! but you need maxiter to be large
    # enough to create triangle confusograms.
    maxiter = 2000

    ############################################################
    # set up random initial positions for the walkers:
    # you should adjust shift_scale to set how far away these
    # are from the initial guess
    initial_guess = initial_parameters + shift_scale * np.random.randn(
        nwalkers, nparams
    )
    nwalkers, ndim = initial_guess.shape

    print("nwalkers = ", nwalkers, " ndim = ", ndim)

    ############################################################
    # Now we either run the MCMC or load pre-computed
    # data from the appropriate file and start a new MCMC.
    if task == "restart":
        ############################################################
        # use existing smples loaded from the pkl.gz file
        sampler = compress_pickle.load(savefile)
        print("loaded ", len(sampler.get_chain()), " prior iterations")
        with Pool(processes=ncpus) as pool:
            sampler.pool = pool
            sampler.run_mcmc(initial_guess, maxiter, progress=True, thin_by=1)
    else:
        ############################################################
        # run new MCMC
        with Pool(ncpus) as pool:
            sampler = emcee.EnsembleSampler(nwalkers, ndim, log_probability, pool=pool)
            sampler.run_mcmc(initial_guess, maxiter, progress=True, thin_by=1)

    ############################################################
    # save metadata in the sampler object (required for
    # later loading)
    sampler.local = types.SimpleNamespace()
    setattr(sampler.local, "ndim", ndim)
    setattr(sampler.local, "nwalkers", nwalkers)
    setattr(sampler.local, "nparams", nparams)
    setattr(sampler.local, "maxiter", maxiter)

    ############################################################
    # save the data for later processing
    compress_pickle.dump(sampler, savefile)

elif task == "load":
    ############################################################
    # load existing pickled object
    sampler = compress_pickle.load(savefile)

    # load metadata
    ndim = sampler.local.ndim
    nwalkers = sampler.local.nwalkers
    nparams = sampler.local.nparams
    print("loaded sampler : ndim=", ndim, " nwalkers=", nwalkers, " nparams=", nparams)

else:
    ############################################################
    # panic! unknown task
    print('Task is unknown : should be "generate" or "load"')
    exit()

############################################################
# we now have either generated or loaded the data
#
############################################################

############################################################
# discard the first 100 steps, thin to every 15 models
flat_samples = sampler.get_chain(discard=100, thin=15, flat=True)

############################################################
# results: these are the 50th percentile (mean) +- 34%
# i.e. 1D 1-sigma
for i in range(ndim):
    mcmc = np.percentile(flat_samples[:, i], [16, 50, 84])
    q = np.diff(mcmc)

    # show the result
    txt = "{3} = {0:.3f} +{1:.3f} -{2:.3f}"
    txt = txt.format(mcmc[1], q[0], q[1], labels[i])
    print(txt)

############################################################
# We can now plot the results
#
############################################################

############################################################
# make samples plot?
if 0:
    plot_samples(ndim, sampler, labels)

############################################################
# make corner plot of parameters
if 0:
    corner_plot(flat_samples, labels)

############################################################
# Make GTC plot
gtc_plot(flat_samples, labels, nbins)

############################################################
# done!
