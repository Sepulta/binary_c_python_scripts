"""
Function that plots all the dataset plots

TODO: change the set up s.t. the mass bins are passed to the function and can be adjusted easily from the outside of the functions
"""

import os
import traceback
import numpy as np

from PyPDF2 import PdfFileMerger
from david_phd_functions.plotting.pdf_functions import (
    add_pdf_and_bookmark,
    generate_chapterpage_pdf,
)

from grav_waves.gw_analysis.functions.functions import load_info_dict

from grav_waves.gw_analysis.functions.plot_functions import (
    plot_all_chirpmasses_with_observations,
    plot_MWEG_merger_rates,
    plot_RVOL_merger_rates,
    plot_total_masses,
    plot_any_mass_plot,
    plot_combined_massratio_triangle,
    plot_timescales_plot,
    plot_combined_merger_rate_per_mergertime_total_mass,
    plot_combined_merger_rate_per_mergertime_chirpmass,
    plot_merger_rate_by_metallicity_by_merger_time,
    marchant_plot,
    stevenson_plot,
    plot_sn_rates,
    plot_neijssel19_figure_1,
    plot_neijsel_fig1_channels_lieke,
)

from grav_waves.gw_analysis.functions.plot_routines.plot_merger_rate_by_metallicity_by_mass_quantity import (
    plot_merger_rate_by_metallicity_by_mass_quantity,
)

from grav_waves.settings import config_dict_cosmology
import matplotlib as mpl

mpl.rc("text", usetex=False)


def general_plot_function(
    dataset_dict,
    result_dir,
    plot_dir,
    dco_type,
    combined_pdf_output_name,
    convolution_settings=None,
    display_log_string=False,
    include_plot_MWEG_merger_rates=True,
    include_plot_RVOL_merger_rates=True,
    include_plot_all_chirpmasses_with_observations=True,
    include_plot_total_masses=True,
    include_plot_any_mass_plot=True,
    include_plot_combined_massratio_triangle=True,
    include_plot_timescales_plot=True,
    include_plot_combined_merger_rate_per_mergertime_total_mass=True,
    include_plot_combined_merger_rate_per_mergertime_chirpmass=True,
    include_plot_merger_rate_by_metallicity_by_merger_time=True,
    include_marchant_plots=True,
    include_stevenson_plots=True,
    include_plot_merger_rate_by_metallicity_by_mass_quantity=True,
    include_plot_sn_rates=True,
    include_plot_neijssel19_figure_1=True,
    include_plot_neijsel_fig1_channels_lieke=True,
):
    """
    Grand function that runs all the plotting routines

    Input:
        - dataset_dict: dictionary containing information about the dataset (location, simname etc)
        - plot_dir, result_dir: directories

        - display_log_string: whether to display the metallicity as a log10 based value
        - flags for which plot routine
    """

    # Create default for convolution_settings if not passed
    if not convolution_settings:
        convolution_settings = {}

    # Set format types
    format_types = ["pdf"]
    if format_types[-1] != "pdf":
        raise ValueError("last format type needs to be 'pdf'")

    # Check if we took the correct dco type
    if not dco_type in ["bhbh", "bhns", "nsns", "combined"]:
        print("Unkown dco type selected (dco_type)")
        raise ValueError

    # Create dictionary for mass bins

    mass_bin_dict = {}

    mass_bin_dict["bhbh"] = {
        "chirpmass": np.linspace(0, 80, 150),
        "primary_mass": np.linspace(0, 80, 100),
        "any_mass": np.linspace(0, 80, 100),
        "total_mass": np.linspace(0, 140, 150),
    }
    mass_bin_dict["bhns"] = {
        "chirpmass": np.linspace(0, 12, 100),
        "primary_mass": np.linspace(0, 80, 100),
        "any_mass": np.linspace(0, 80, 100),
        "total_mass": np.linspace(0, 80, 100),
    }
    mass_bin_dict["nsns"] = {
        "chirpmass": np.linspace(1, 2, 50),
        "primary_mass": np.linspace(1.4, 2.6, 50),
        "any_mass": np.linspace(1.4, 2.4, 50),
        "total_mass": np.linspace(2.4, 5, 50),
    }

    #########
    # Do the plot
    rebuild = dataset_dict["rebuild"]
    sim_name = dataset_dict["sim_name"]

    os.makedirs(os.path.join(plot_dir, dco_type, "pdf"), exist_ok=True)

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_list = []
    pdf_page_number = 0

    # Load dataset
    info_dict = load_info_dict(result_dir, rebuild=rebuild)

    # Sort metallicity keys
    sorted_metallicity_keys = sorted(
        info_dict.keys(),
        key=lambda metallicity: float(metallicity.split("_")[0][1:]),
        reverse=True,
    )
    sorted_metallicity_keys_type = [
        el for el in sorted_metallicity_keys if el.endswith("_{}".format(dco_type))
    ]

    print("Started general plotting function for {}: {}".format(sim_name, dco_type))
    print("\tUsing result_dir: {}".format(result_dir))
    print("\tUsing plot_dir: {}\n".format(plot_dir))

    ############################################################################################################
    # Rates plots
    rates_chapter_filename = os.path.join(
        plot_dir, dco_type, "pdf", "rates_chapter_chapter.pdf"
    )
    generate_chapterpage_pdf("Rates", rates_chapter_filename)
    pdf_list.append(rates_chapter_filename)
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger, pdf_list[-1], pdf_page_number, bookmark_text="Rates chapter"
    )

    #####
    # Plot mweg merger rates: 1-d plot with metallicity on x-axis and merger rate density on y-axis. Also includes data from de mink & belczynski.
    # Old plots, not very good anymore. milkyway equivalent merger rate
    if include_plot_MWEG_merger_rates:
        print("\tPlotting plot_MWEG_merger_rates")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            filename = os.path.join(
                plot_dir,
                dco_type,
                format_type,
                "MWEG_merger_rates.{}".format(format_type),
            )
            plot_MWEG_merger_rates(
                info_dict,
                plot_settings={
                    "output_name": filename,
                    "simulation_name": sim_name,
                    "show_plot": False,
                    "runname": "plot_MWEG_merger_rates: dco_type: {}".format(dco_type),
                },
            )
        pdf_list.append(filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger, pdf_list[-1], pdf_page_number, bookmark_text="MWEG_merger_rates"
        )

    #####
    # Volumetric merger rate plot: same as above but with some assumption of the density of galaxies, turnig it into a merger rate
    if include_plot_RVOL_merger_rates:
        print("\tPlotting plot_RVOL_merger_rates")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            filename = os.path.join(
                plot_dir,
                dco_type,
                format_type,
                "RVOL_merger_rates.{}".format(format_type),
            )
            plot_RVOL_merger_rates(
                info_dict,
                plot_settings={
                    "output_name": filename,
                    "simulation_name": sim_name,
                    "show_plot": False,
                    "runname": "plot_RVOL_merger_rates: dco_type: {}".format(dco_type),
                },
            )
        pdf_list.append(filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger, pdf_list[-1], pdf_page_number, bookmark_text="RVOL_merger_rates"
        )

    ############################################################################################################
    # SN type rates plots
    mass_quantity_chapter_filename = os.path.join(
        plot_dir, dco_type, "pdf", "sn_type_rates_chapter.pdf"
    )
    generate_chapterpage_pdf("SN type rate plots", mass_quantity_chapter_filename)
    pdf_list.append(mass_quantity_chapter_filename)
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger, pdf_list[-1], pdf_page_number, bookmark_text="SN type rate chapter"
    )

    ######
    # plot_sn_rates: yield of super nova types, metallicity on x-axis, yield per formed solar mass on y-axis. Plotted for those merging in hubble time and those not limited by any time
    if include_plot_sn_rates:
        print("\tPlotting plot_sn_rates merging in hubble time")

        ##########
        # merge in hubble time
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))

            filename = os.path.join(
                plot_dir,
                dco_type,
                format_type,
                "sn_rates_merging_in_hubbletime.{}".format(format_type),
            )
            plot_sn_rates(
                result_dir,
                convolution_settings,
                limit_merging_in_hubbletime=True,
                plot_settings={
                    "output_name": filename,
                    "block": False,
                    "show_plot": False,
                    "simulation_name": sim_name,
                    "runname": "plot_sn_rates: dco_type: {}".format(dco_type),
                },
            )
        pdf_list.append(filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            pdf_list[-1],
            pdf_page_number,
            bookmark_text="sn_rates merging in hubble time",
        )

        ##########
        # All
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))

            filename = os.path.join(
                plot_dir,
                dco_type,
                format_type,
                "sn_rates_merging_whenever.{}".format(format_type),
            )
            plot_sn_rates(
                result_dir,
                convolution_settings,
                limit_merging_in_hubbletime=False,
                plot_settings={
                    "output_name": filename,
                    "block": False,
                    "show_plot": False,
                    "simulation_name": sim_name,
                    "runname": "plot_sn_rates: dco_type: {}".format(dco_type),
                },
            )
        pdf_list.append(filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            pdf_list[-1],
            pdf_page_number,
            bookmark_text="sn_rates merging whenever",
        )

    ############################################################################################################
    # mass quantity plots
    mass_quantity_chapter_filename = os.path.join(
        plot_dir, dco_type, "pdf", "mass_quantity_chapter.pdf"
    )
    generate_chapterpage_pdf("Mass quantity plots", mass_quantity_chapter_filename)
    pdf_list.append(mass_quantity_chapter_filename)
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger, pdf_list[-1], pdf_page_number, bookmark_text="Mass quantities chapter"
    )

    ######
    # Combined chirpmass plot: Chirp mass distribution for all metallicities, and the top panel showing the observations. The x-axis shows chirp mass, y-axis shows yield per formed mass
    if include_plot_all_chirpmasses_with_observations:
        print("\tPlotting plot_all_chirpmasses_with_observations")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            filename = os.path.join(
                plot_dir,
                dco_type,
                format_type,
                "chirpmasses_with_observations.{}".format(format_type),
            )
            plot_all_chirpmasses_with_observations(
                info_dict,
                dco_type,
                convolution_settings,
                display_log_string=display_log_string,
                plot_settings={
                    "output_name": filename,
                    "simulation_name": sim_name,
                    "show_plot": False,
                    "runname": "plot_all_chirpmasses_with_observations: dco_type: {}".format(
                        dco_type
                    ),
                },
            )
        pdf_list.append(filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger, pdf_list[-1], pdf_page_number, bookmark_text="chirpmasses"
        )

    ######
    # Total mass plots: 1-d plot with total mass distribution for all and merging-in-hubble time systems. x-axis is total mass, y-axis is yield per formed solar mass
    if include_plot_total_masses:
        print("\tPlotting plot_total_masses")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            filename = os.path.join(
                plot_dir, dco_type, format_type, "total_mass.{}".format(format_type)
            )
            plot_total_masses(
                info_dict,
                dco_type,
                convolution_settings,
                display_log_string=display_log_string,
                plot_settings={
                    "output_name": filename,
                    "simulation_name": sim_name,
                    "show_plot": False,
                    "runname": "plot_total_masses: dco_type: {}".format(dco_type),
                },
            )
        pdf_list.append(filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger, pdf_list[-1], pdf_page_number, bookmark_text="total_mass"
        )

    ######
    # Any mass plots: same as above but with any mass (primary and secondary regarded as equals)
    if include_plot_any_mass_plot:
        print("\tPlotting plot_any_mass_plot")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            filename = os.path.join(
                plot_dir, dco_type, format_type, "any_mass.{}".format(format_type)
            )
            plot_any_mass_plot(
                info_dict,
                dco_type,
                convolution_settings,
                display_log_string=display_log_string,
                plot_settings={
                    "output_name": filename,
                    "simulation_name": sim_name + "_{}".format(dco_type),
                    "show_plot": False,
                    "runname": "plot_any_mass_plot: dco_type: {}".format(dco_type),
                },
            )
        pdf_list.append(filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger, pdf_list[-1], pdf_page_number, bookmark_text="any_mass"
        )

    # TODO: do a chirp mass without observations but same as above
    # TODO: do a primary mass same as above

    ############################################################################################################
    # timescale plots
    timescale_chapter_filename = os.path.join(
        plot_dir, dco_type, "pdf", "timescale_chapter.pdf"
    )
    generate_chapterpage_pdf("Timescale plots", timescale_chapter_filename)
    pdf_list.append(timescale_chapter_filename)
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger, pdf_list[-1], pdf_page_number, bookmark_text="Timescale chapter"
    )

    ######
    # plot_timescales_plot: distributions of timescales (formation, inspiral, merger) for systems merging in hubble time, x axis is the timescale, y-axis the yield per formed solar mass
    if include_plot_timescales_plot:
        print("\t Plotting plot_timescales_plot")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            filename = os.path.join(
                plot_dir,
                dco_type,
                format_type,
                "plot_timescales_plot.{}".format(format_type),
            )
            plot_timescales_plot(
                info_dict,
                dco_type,
                convolution_settings,
                display_log_string=display_log_string,
                plot_settings={
                    "output_name": filename,
                    "simulation_name": sim_name + "_{}".format(dco_type),
                    "show_plot": False,
                    "runname": "plot_timescales_plot: dco_type: {}".format(dco_type),
                },
            )
        pdf_list.append(filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger, pdf_list[-1], pdf_page_number, bookmark_text="plot_timescales_plot"
        )

    ######
    # Combined merger rate per merger time per metallicity: 2-d plot for systems merging in hubble time with merger time on x-axis,
    # metallicity on y-axis and yield per formed solar mass on z-axis (color)
    if include_plot_merger_rate_by_metallicity_by_merger_time:
        print("\tPlotting plot_merger_rate_by_metallicity_by_merger_time")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            filename = os.path.join(
                plot_dir,
                dco_type,
                format_type,
                "merger_rate_by_metallicity_by_merger_time.{}".format(format_type),
            )
            plot_merger_rate_by_metallicity_by_merger_time(
                info_dict,
                dco_type,
                convolution_settings,
                plot_settings={
                    "output_name": filename,
                    "simulation_name": sim_name,
                    "show_plot": False,
                    "runname": "plot_merger_rate_by_metallicity_by_merger_time: dco_type: {}".format(
                        dco_type
                    ),
                },
            )
        pdf_list.append(filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            pdf_list[-1],
            pdf_page_number,
            bookmark_text="merger_rate_by_metallicity_by_merger_time",
        )

    ######
    # Combined merger rate per merger time per total mass: Range of 2-d plots for systems merging in hubble time,
    # with total mass on x-axis and merger time on y-axis, plotted for each metallicity
    if include_plot_combined_merger_rate_per_mergertime_total_mass:
        print("\tPlotting plot_combined_merger_rate_per_mergertime_total_mass")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            filename = os.path.join(
                plot_dir,
                dco_type,
                format_type,
                "combined_merger_rate_per_merger_time_per_totalmass.{}".format(
                    format_type
                ),
            )
            plot_combined_merger_rate_per_mergertime_total_mass(
                info_dict,
                dco_type,
                convolution_settings,
                display_log_string=display_log_string,
                plot_settings={
                    "output_name": filename,
                    "simulation_name": sim_name,
                    "show_plot": False,
                    "runname": "plot_combined_merger_rate_per_mergertime_total_mass: dco_type: {}".format(
                        dco_type
                    ),
                },
            )
        pdf_list.append(filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            pdf_list[-1],
            pdf_page_number,
            bookmark_text="combined_merger_rate_per_merger_time_per_totalmass",
        )

    ######
    # Combined merger rate per merger time per total mass: same as above, but for chirp mass
    if include_plot_combined_merger_rate_per_mergertime_chirpmass:
        print("\tPlotting plot_combined_merger_rate_per_mergertime_chirpmass")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            filename = os.path.join(
                plot_dir,
                dco_type,
                format_type,
                "combined_merger_rate_per_merger_time_per_chirpmass.{}".format(
                    format_type
                ),
            )
            plot_combined_merger_rate_per_mergertime_chirpmass(
                info_dict,
                dco_type,
                convolution_settings,
                display_log_string=display_log_string,
                plot_settings={
                    "output_name": filename,
                    "simulation_name": sim_name + "_{}".format(dco_type),
                    "show_plot": False,
                    "runname": "plot_combined_merger_rate_per_mergertime_chirpmass: dco_type: {}".format(
                        dco_type
                    ),
                },
            )
        pdf_list.append(filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            pdf_list[-1],
            pdf_page_number,
            bookmark_text="combined_merger_rate_per_merger_time_per_chirpmass",
        )

    # TODO: add any mass version of above
    # TODO: add primary mass version of above

    ############################################################################################################
    # massratio plots
    massratio_chapter_filename = os.path.join(
        plot_dir, dco_type, "pdf", "massratio_chapter.pdf"
    )
    generate_chapterpage_pdf("Massratio plots", massratio_chapter_filename)
    pdf_list.append(massratio_chapter_filename)
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger, pdf_list[-1], pdf_page_number, bookmark_text="Massratio chapter"
    )

    ######
    # Mass triangles: Range of 2-d plots for systems both merging in hubble time and merging whenever,
    # with primary mass on x-axis and secondary mass, plotted for each metallicity
    if include_plot_combined_massratio_triangle:
        print("\tPlotting plot_combined_massratio_triangle")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            filename = os.path.join(
                plot_dir,
                dco_type,
                format_type,
                "combined_massratio_triangle.{}".format(format_type),
            )
            plot_combined_massratio_triangle(
                info_dict,
                dco_type,
                convolution_settings,
                display_log_string=display_log_string,
                plot_settings={
                    "output_name": filename,
                    "simulation_name": sim_name,
                    "block": False,
                    "show_plot": False,
                    "runname": "plot_combined_massratio_triangle: dco_type: {}".format(
                        dco_type
                    ),
                },
            )
        pdf_list.append(filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            pdf_list[-1],
            pdf_page_number,
            bookmark_text="combined_massratio_triangle",
        )

    ############################################################################################################
    # metallicity_and_mass_quantity plots
    metallicity_and_mass_quantity_chapter_filename = os.path.join(
        plot_dir, dco_type, "pdf", "metallicity_and_mass_quantity.pdf"
    )
    generate_chapterpage_pdf(
        "Metallicity per mass quantity plots",
        metallicity_and_mass_quantity_chapter_filename,
    )
    pdf_list.append(metallicity_and_mass_quantity_chapter_filename)
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        pdf_list[-1],
        pdf_page_number,
        bookmark_text="Metallicity per mass quantity chapter",
    )

    ######
    # plot_merger_rate_by_metallicity_by_mass_quantity: two 2-d plots, for merging in hubbletime and whenever,
    # with x-axis the mass quantity, y-axis metallicity, z-axis yield per formed solar mass
    if include_plot_merger_rate_by_metallicity_by_mass_quantity:
        for mass_quantity in ["primary_mass", "total_mass", "chirpmass", "any_mass"]:
            print(
                "\tPlotting plot_merger_rate_by_metallicity_by_{}".format(mass_quantity)
            )
            for format_type in format_types:
                print("\t\tSaving as {}".format(format_type))
                filename = os.path.join(
                    plot_dir,
                    dco_type,
                    format_type,
                    "merger_rate_by_metallicity_by_{}.{}".format(
                        mass_quantity, format_type
                    ),
                )
                plot_merger_rate_by_metallicity_by_mass_quantity(
                    merger_dict=info_dict,
                    dco_type=dco_type,
                    mass_quantity=mass_quantity,
                    mass_bins=mass_bin_dict[dco_type][mass_quantity],
                    convolution_settings=convolution_settings,
                    display_log_string=display_log_string,
                    plot_settings={
                        "output_name": filename,
                        "block": False,
                        "show_plot": False,
                        "simulation_name": sim_name,
                        "runname": "plot_merger_rate_by_metallicity_by_{}: dco_type: {}".format(
                            mass_quantity, dco_type
                        ),
                    },
                )
            pdf_list.append(filename)
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                pdf_list[-1],
                pdf_page_number,
                bookmark_text="merger_rate_by_metallicity_by_{}".format(mass_quantity),
            )

    ##################
    # Other paper plots
    if dco_type == "bhbh":
        other_paper_plots_chapter_filename = os.path.join(
            plot_dir, dco_type, "pdf", "other_paper_plots.pdf"
        )
        generate_chapterpage_pdf(
            "Other paper plots", other_paper_plots_chapter_filename
        )
        pdf_list.append(other_paper_plots_chapter_filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            pdf_list[-1],
            pdf_page_number,
            bookmark_text="Other paper plots chapter",
        )

        if dataset_dict["add_ppisn_plots"]:

            ######
            # Marchant plots: Plots as in paper Pablo Marchant (https://ui.adsabs.harvard.edu/abs/2019ApJ...882...36M/abstract). merging and all
            if include_marchant_plots:
                for type_sub_key in sorted_metallicity_keys_type:
                    print("\tPlotting marchant_plot: {}".format(type_sub_key))
                    for format_type in format_types:
                        print("\t\tSaving as {}".format(format_type))
                        filename = os.path.join(
                            plot_dir,
                            dco_type,
                            format_type,
                            "marchant_plots/{}_systems.{}".format(
                                type_sub_key, format_type
                            ),
                        )
                        marchant_plot(
                            dataset_dict[""],
                            dataset_dict["sim_name"],
                            dataset_dict["no_ppisn_companion_dataset"],
                            type_sub_key,
                            convolution_settings,
                            display_log_string=display_log_string,
                            plot_settings={
                                "output_name": filename,
                                "simulation_name": sim_name
                                + "_{}".format(type_sub_key),
                                "block": False,
                                "show_plot": False,
                                "runname": "marchant_plot: dco_type: {}".format(
                                    dco_type
                                ),
                            },
                        )
                    pdf_list.append(filename)
                    merger, pdf_page_number = add_pdf_and_bookmark(
                        merger,
                        pdf_list[-1],
                        pdf_page_number,
                        bookmark_text="marchant_plots_{}_systems.pdf".format(
                            type_sub_key
                        ),
                    )
            #####
            # Stevenson plots: Plots from stevenson 2020.
            if include_stevenson_plots:
                for type_sub_key in sorted_metallicity_keys_type:
                    print("\tPlotting stevenson_plot: {}".format(type_sub_key))
                    for format_type in format_types:
                        print("\t\tSaving as {}".format(format_type))
                        filename = os.path.join(
                            plot_dir,
                            dco_type,
                            format_type,
                            "stevenson_plots/{}_systems.{}".format(
                                type_sub_key, format_type
                            ),
                        )
                        stevenson_plot(
                            info_dict,
                            type_sub_key,
                            convolution_settings,
                            display_log_string=display_log_string,
                            plot_settings={
                                "output_name": filename,
                                "simulation_name": sim_name
                                + "_{}".format(type_sub_key),
                                "block": False,
                                "show_plot": False,
                                "runname": "stevenson_plot: dco_type: {}".format(
                                    dco_type
                                ),
                            },
                        )
                    pdf_list.append(filename)
                    merger, pdf_page_number = add_pdf_and_bookmark(
                        merger,
                        pdf_list[-1],
                        pdf_page_number,
                        bookmark_text="stevenson_plots_{}_systems.pdf".format(
                            type_sub_key
                        ),
                    )
    #####
    # Neijssel 19 figure 1: plots from Neijsel+19, figure 1. Showing the yield per formed solar mass, x-axis is metallicity, y-axis is yield per formed solar mass /10e-5
    if include_plot_neijssel19_figure_1:
        print("\tPlotting plot_neijssel19_figure_1")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            filename = os.path.join(
                plot_dir,
                dco_type,
                format_type,
                "neijssel19_figure_1.{}".format(format_type),
            )
            plot_neijssel19_figure_1(
                info_dict,
                convolution_settings,
                plot_settings={
                    "output_name": filename,
                    "block": False,
                    "show_plot": False,
                    "simulation_name": sim_name,
                    "runname": "plot_neijssel19_figure_1: dco_type: {}".format(
                        dco_type
                    ),
                },
            )
        pdf_list.append(filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger, pdf_list[-1], pdf_page_number, bookmark_text="neijssel19_figure_1"
        )

    #####
    # Neijsel fig 1 channels lieke: same as above, with liekes channels
    if include_plot_neijsel_fig1_channels_lieke:
        print("\tPlotting plot_neijsel_fig1_channels_lieke")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            filename = os.path.join(
                plot_dir,
                dco_type,
                format_type,
                "neijsel_fig1_channels_lieke.{}".format(format_type),
            )
            plot_neijsel_fig1_channels_lieke(
                info_dict,
                convolution_settings,
                plot_settings={
                    "output_name": filename,
                    "block": False,
                    "show_plot": False,
                    "simulation_name": sim_name + "_{}".format(dco_type),
                    "runname": "plot_neijsel_fig1_channels_lieke: dco_type: {}".format(
                        dco_type
                    ),
                },
            )
        pdf_list.append(filename)
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            pdf_list[-1],
            pdf_page_number,
            bookmark_text="neijssel19_figure_1 liekes channels",
        )

    # Round off the plot
    merger.write(combined_pdf_output_name)
    merger.close()
    print("Finished making plots. Wrote pdf to {}".format(combined_pdf_output_name))
