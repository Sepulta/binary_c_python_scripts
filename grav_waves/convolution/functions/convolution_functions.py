"""
File that contains the main function for the new convolution method;

TODO: remove all the functions that are associated with building the input dataset.
TODO: combine the cosmology configuration, just put it in the same convolution configuration
"""

import os
import json
import time
import shutil

import h5py
import numpy as np
import pandas as pd

import astropy.units as u

from grav_waves.convolution.functions.redshift_interpolation_functions import (
    load_interpolation_data,
)
from grav_waves.convolution.functions.convolution_sfr_functions import (
    generate_metallicity_sfr_array,
)
from grav_waves.convolution.functions.volume_convolution_functions import (
    create_shell_volume_dict,
)
from grav_waves.convolution.functions.convolution_multiprocessing import (
    convolution_multiprocessing,
)
from grav_waves.convolution.functions.convolution_general_functions import (
    JsonCustomEncoder,
    create_bins_from_centers,
)

#
dimensionless = u.m / u.m

####
# Main point
def convolution_main(
    input_filename,
    convolution_configuration,
    cosmology_configuration,
    output_filename,
    verbosity=0,
):
    """
    Main function for the new convolution.

    This function copies the input file to the output file and extend it.

    input:
        input_filename: hdf5 file containing the input dataset information, along with settings from the datasets.
        convolution_configuration: settings for the convolution
        cosmology_configuration; settings for the cosmology: SFR etc

    TODO: add a logging functionality. pass along with the convolution config
    """

    #
    convolution_configuration["logger"].debug("Started convolution")

    # Some checks
    if convolution_configuration["time_type"] == "lookback":
        raise ValueError("sampling in lookback time is not allowed currently")
    if any(
        convolution_configuration["time_centers"]
        > convolution_configuration["max_interpolation_redshift"]
    ):
        raise ValueError("Chosen redshifts exceed maximum redshift")

    # Copy input file to output file
    if os.path.isfile(output_filename):
        os.remove(output_filename)
    shutil.copy(input_filename, output_filename)

    #######################
    # Read out the combined dataset dataframe and get some metallicity info
    combined_df = pd.read_hdf(output_filename, "/data/combined_dataframes")
    sorted_log10metallicity_values = np.array(
        sorted(np.log10(combined_df["metallicity"].unique()))
    )
    sorted_log10metallicity_bins = create_bins_from_centers(
        sorted_log10metallicity_values
    )

    sorted_metallicities = np.array(sorted(combined_df["metallicity"].unique()))
    sorted_metallicity_bins = 10**sorted_log10metallicity_bins

    # Load the dict for dict with the interpolators
    convolution_configuration["interpolators"] = load_interpolation_data(
        convolution_configuration
    )

    ###########
    # Set up the output file and structure

    # Create the output directory
    if os.path.dirname(output_filename):
        os.makedirs(os.path.dirname(output_filename), exist_ok=True)

    # create the main HDF5 file
    hdf5_file = h5py.File(output_filename, "a")

    settings_grp = hdf5_file["settings"]

    # Read out dataset dict
    dataset_dict = json.loads(settings_grp["dataset_dict"][()])

    # Create subgroups
    convolution_configuration["logger"].info("creating hdf5 groups")
    sfr_grp = hdf5_file.create_group("sfr_data")
    shell_volumes_grp = hdf5_file.create_group("shell_volumes")

    # Settings we used in the convolution: resolution, redshift or lookback, log spaced etc
    settings_grp.create_dataset(
        "convolution_configuration",
        data=json.dumps(convolution_configuration, cls=JsonCustomEncoder),
    )

    # Settings for the cosmology, SFR and metallicity distributions
    settings_grp.create_dataset(
        "cosmology_configuration",
        data=json.dumps(cosmology_configuration, cls=JsonCustomEncoder),
    )

    ##################
    # Generate metallicity_weighted_starformation_array and store the info
    convolution_configuration["logger"].info(
        "Calculating the metallicity weighted starformation array"
    )
    start_sfr = time.time()

    (
        metallicity_weighted_starformation_array,
        metallicity_fraction_array,
        starformation_array,
    ) = generate_metallicity_sfr_array(
        cosmology_configuration=cosmology_configuration,
        time_centers=convolution_configuration["time_centers"],
        sorted_metallicity_log10values=np.array(sorted_log10metallicity_values),
    )
    metallicity_weighted_starformation_array = (
        metallicity_weighted_starformation_array.T
    )

    # Write to output file
    sfr_grp.create_dataset(
        "metallicity_weighted_starformation_array",
        data=metallicity_weighted_starformation_array,
    )
    sfr_grp.create_dataset(
        "metallicity_fraction_array", data=metallicity_fraction_array
    )
    sfr_grp.create_dataset("starformation_array", data=starformation_array)

    sfr_grp.create_dataset("metallicity_bin_centers", data=sorted_metallicities)
    sfr_grp.create_dataset("metallicity_bin_edges", data=sorted_metallicity_bins)
    sfr_grp.create_dataset(
        "redshift_bin_centers", data=convolution_configuration["time_centers"]
    )
    sfr_grp.create_dataset(
        "redshift_bin_edges", data=convolution_configuration["time_bins"]
    )
    convolution_configuration["logger"].info(
        "\ttook {:.2e}s".format(time.time() - start_sfr)
    )

    ##################
    # Create shell volume dict
    redshift_shell_volume_dict = create_shell_volume_dict(
        convolution_configuration["time_centers"]
    )
    shell_volumes_grp.create_dataset(
        "redshift_shell_volume_dict",
        data=json.dumps(redshift_shell_volume_dict, cls=JsonCustomEncoder),
    )

    #######################
    # Close hdf5 file to be able to write the pandas stuff in there
    hdf5_file.close()

    ##################
    # Loop over the rate types
    if convolution_configuration["do_convolution"]:
        ##################
        # Create the named numpy arrays that we will pass through the functions to calculate the rates
        #   This information is the only information that we need for the actual convolution (for now)

        data_columns = convolution_configuration["combined_data_array_columns"]
        if convolution_configuration["include_formation_rates"]:
            data_columns.append("formation_time_values_in_years")
        if convolution_configuration["include_merger_rates"]:
            data_columns.append("merger_time_values_in_years")

        print(combined_df.columns)

        # write as records
        records = combined_df[data_columns].to_records(index=False)
        records = list(records)

        # Create combined data array
        combined_data_array = np.array(
            records, dtype=[(col, np.float64) for col in data_columns]
        )
        print()
        # Run via multiprocessing or sequential
        convolution_configuration["logger"].info(
            "Starting multiprocessing convolution with a total of {} systems using {} cores".format(
                len(combined_data_array[combined_data_array.dtype.names[0]]),
                convolution_configuration["num_cores"],
            )
        )
        start_convolution = time.time()
        convolution_multiprocessing(
            convolution_configuration=convolution_configuration,
            output_filename=output_filename,
            metallicity_weighted_starformation_array=metallicity_weighted_starformation_array,
            combined_data_array=combined_data_array,
        )
        convolution_configuration["logger"].info(
            "\ttook {:.2e}s".format(time.time() - start_convolution)
        )

    #
    convolution_configuration["logger"].info(
        "Finished convolution for {}. Wrote results to {}".format(
            dataset_dict["sim_name"], output_filename
        )
    )
