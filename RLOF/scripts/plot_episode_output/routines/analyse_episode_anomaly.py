"""
Function to calculate the fractions of things we are interesting in based on the data of the episodes

TODO: make a deeper analysis on the RLOF episode. Look at the donors and accretors and what phase they're in. Could do it as a print now.
"""

import json
import pandas as pd
from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict

from grav_waves.settings import convolution_settings

import warnings
from pandas.core.common import SettingWithCopyWarning

warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)


def calculate_fractions_dict(dataset_dict, base_query=None):
    """
    Function to calculate fractions of interest.
    """

    #
    treshold_rlof_number = 50

    # Read out data and create dataframe
    df = pd.read_csv(dataset_dict["filename"], sep="\s+", header=0)

    # import numpy as np
    # bins = np.arange(0, 1e4, treshold_rlof_number)
    # binsizes = np.diff(bins)

    # hist_count = np.histogram(df['episode_number'], bins=bins)
    # hist_weight = np.histogram(df['episode_number'], bins=bins, weights=df['probability'])

    #
    many_episodes = df[df.episode_number > treshold_rlof_number]
    skip_columns = [
        "number_per_solar_mass",
        "unique_identifier",
        "probability",
        "episode_number",
        "final_time",
        "initial_time",
        "random_seed",
        "time",
    ]
    # Go through columns and print their mean and stddev
    for column in many_episodes.columns:
        if not column in skip_columns:
            mean = many_episodes[column].mean()
            std = many_episodes[column].std()
            print("Column {}:\n\tmean {}\n\tstddev {}".format(column, mean, std))

    # write html to file
    grouped_df = many_episodes.groupby("random_seed")
    first_values = grouped_df.first()
    first_values = first_values.reset_index()

    html = first_values.to_html()
    with open("html_table.html", "w") as f:
        f.write(html)

    # Get initial values of the systems that have this
    initial_values_df = many_episodes[
        [
            "zams_mass_1",
            "zams_mass_2",
            "zams_orbital_period",
            "random_seed",
            "episode_number",
        ]
    ]
    print(initial_values_df)

    #
    reduced_initial_values_df = initial_values_df.sort_values(
        "episode_number"
    ).drop_duplicates(
        ["zams_mass_1", "zams_mass_2", "zams_orbital_period", "random_seed"],
        keep="last",
    )
    print(reduced_initial_values_df)

    output_initial_values = reduced_initial_values_df[
        ["zams_mass_1", "zams_mass_2", "zams_orbital_period", "random_seed"]
    ]
    output_initial_values = output_initial_values.rename(
        columns={
            "zams_mass_1": "M_1",
            "zams_mass_2": "M_2",
            "zams_orbital_period": "orbital_period",
        }
    )
    # output_initial_values['orbital_period'] = output_initial_values['orbital_period']/365
    output_initial_values["metallicity"] = dataset_dict["metallicity"]

    dict_output_initial_values = output_initial_values.to_dict(orient="records")
    print(dict_output_initial_values)

    # Write to file:
    output_file = "many_episode_systems.txt"
    with open(output_file, "w") as f:
        for el in dict_output_initial_values:
            #
            call = (
                "binary_c "
                + " ".join(["{} {}".format(key, el[key]) for key in el.keys()])
                + " ensemble 1 ensemble_filters_off 1"
            )
            print(call)
            f.write(call + "\n")


# simulation_dir = '/home/david/projects/binary_c_root/results/RLOF/LOW_RES_RLOF_SANA_2021_ENSEMBLE_fractions'
simulation_dir = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions"
rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(simulation_dir)
dataset_dict = rlof_episode_dataset_dict["datasets"]

base_query = "initial_stability == 0 & initial_stellar_type_accretor == 1"
calculate_fractions_dict(dataset_dict["0.02"], base_query=base_query)
