"""
Script to plot the evolution of a system and zoom in on the RLOF episodes

TODO: remove all x axis labels
TODO: place the

"""

import os
import time

import pandas as pd
import numpy as np

import matplotlib.pyplot as plt

#
import astropy.units as u

#
from binarycpython.utils.run_system_wrapper import run_system

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


# Run system. all arguments can be given as optional arguments.
system_dict = {
    "M_1": 2,
    "M_2": 1,
    "separation": 0,
    "orbital_period": 100,
}


output = run_system(**system_dict, david_logging_function=12)

df = parse_function(output)

# plot_entire_evolution_binary_system(
#     df,
#     plot_settings={'show_plot': True}
# )

#
generate_plots(df)
