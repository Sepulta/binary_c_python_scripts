"""
routine to generate figure 1 from neijssel19
"""

import os
import numpy as np
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

from grav_waves.gw_analysis.laptop_datasets import dataset_dict

from grav_waves.gw_analysis.functions.functions import (
    make_df,
    load_info_dict,
    combine_queries,
    calculate_numbers,
)

from grav_waves.settings import (
    resolution_settings,
    AGE_UNIVERSE_IN_YEAR,
    mass_in_stars,
    config_dict_cosmology,
    coens_channels_query_dict,
    liekes_channels_query_dict,
    ppisn_query_dict,
    convolution_settings,
)

from grav_waves.gw_analysis.functions.plot_functions import (
    plot_merger_rate_by_metallicity_by_total_mass,
)

from grav_waves.convolution.functions.convolution_functions import (
    get_dataset_metallicity_info,
)

MAIN_RESULT_DIR = "/home/david/projects/binary_c_root/results/GRAV_WAVES/"
MAIN_PLOT_DIR = "plots"

# List to loop over
DATASET_KEY_LIST = [
    "LOW_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_ON",
]
PLOT_TYPE_KEY_LIST = [
    "bhbh",
]

key = DATASET_KEY_LIST[0]
type_key = PLOT_TYPE_KEY_LIST[0]
sim_name = dataset_dict[key]["sim_name"]

result_dir = os.path.join(MAIN_RESULT_DIR, dataset_dict[key]["result_dir"])
plot_dir = os.path.join(MAIN_PLOT_DIR, dataset_dict[key]["plot_dir"])

# Load dataset
info_dict = load_info_dict(result_dir, rebuild=False)

#

from grav_waves.gw_analysis.functions.plot_functions import (
    plot_neijsel_fig1_channels_lieke,
)

plot_neijsel_fig1_channels_lieke(
    info_dict, convolution_settings, plot_settings={"show_plot": True}
)
