"""
Script to do the following:

Sample the mass ratio and period for a given mass.

- Readout the RLOF information: save the first line.
- To the line add a clear count of the episode
- To the line add clearly which is the accretor and which is the donor

- Run the masses
X    [0.5, 1]
X    [2, 5]
    [7, 14]
"""


import shutil
import os
import json
import time
import pickle
import sys
import secrets

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import (
    get_help_all,
    get_help,
    create_hdf5,
    output_lines,
)

from david_phd_functions.binaryc.personal_defaults import personal_defaults


def parse_function(self, output):
    """
    Parse function for the RLOFing systems:

    will read out the output, get the rlof_counter value and write each time to a new file.

    - i will also write those that have nr=0, but those should be ignored in any calculations.
    -
    """

    # extract info from the population instance
    # TODO: think about whether this is smart. Passing around this object might be an overkill

    # Get some information from the
    data_dir = self.custom_options["data_dir"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # Create filename
    parameters = [
        "time",  # 1
        "mass_1",
        "zams_mass_1",
        "mass_2",
        "zams_mass_2",  # 2-5
        "now_rlof_1",
        "was_rlof_1",
        "now_rlof_2",
        "was_rlof_2",  # 6-9
        "stellar_type_1",
        "prev_stellar_type_1",
        "stellar_type_2",
        "prev_stellar_type_2",  # 10-13
        "metallicity",
        "probability",  # 14-15
        "separation",
        "eccentricity",
        "period",
        "zams_period",  # 16-19
        "rlof_counter",
        "disk",
        "rlof_type",
        "random_seed_extra",  # 20-23
        "radius_1",
        "radius_2",
        "orbital_angular_momentum",
        "angular_momentum_1",  # 24-27
        "angular_momentum_2",
        "v_eq_1",
        "v_eq_ratio_1",
        "v_eq_2",  # 28-31
        "v_eq_ratio_2",
        "rlof_stability",
        "donor",
        "accretor",  # 32-35
        "lum_1",
        "lum_2",
        "prev_eccentricity",
        "dm_dt_rlof_donor",  # 36-39
        "teff_1",
        "teff_2",
        "random_seed",
        "cumulative_angmom_back_to_orbit",  # 40-43
        "minimum_mass_for_cumulative_angmom_back_to_orbit",
        "total_transferred",
        "total_accreted",
        "total_non_conservative_mass_loss",  # 44-47
        "total_non_conservative_angmom_loss",
        "total_angmom_orbit_loss",
        "total_angmom_orbit_gain",
        "isotropic_angmom_loss_with_minimum_mass_for_cumulative_angmom_back_to_orbit",  # 48-51
        "cumulative_time_disk_masstransfer",
        "cumulative_mass_transfered_through_disk",
        "cumulative_angmom_accreted_through_disk",  # 52-54
        "unique_nr",  # added in python
    ]

    rlof_counter_index = parameters.index("rlof_counter")

    # Get indices of initial values
    mass_1_index = parameters.index("zams_mass_1")
    mass_2_index = parameters.index("zams_mass_2")
    orbital_period_index = parameters.index("zams_period")
    metallicity_index = parameters.index("metallicity")

    separator = "\t"

    # Create files containing the first line of the RLOF episode and the last line.
    first_line_file = os.path.join(data_dir, "start_rlof_lines.dat")
    last_line_file = os.path.join(data_dir, "last_rlof_lines.dat")

    if not os.path.exists(first_line_file):
        with open(first_line_file, "w") as first_f:
            first_f.write(separator.join(parameters) + "\n")

    if not os.path.exists(last_line_file):
        with open(last_line_file, "w") as f:
            f.write(separator.join(parameters) + "\n")

    # Set up dict
    RLOF_dict = {}

    # TODO: fix that the individual episodes are split... shit
    RLOFING_lines = [
        line for line in output_lines(output) if line.startswith("DAVID_RLOFING")
    ]

    # Split all the output into the different episodes
    for line in RLOFING_lines:
        values = line.split()[1:]

        if not len(values) == len(parameters) - 1:
            print("Length of readout vaues is not equal to length of parameters")
            raise ValueError

        rlof_counter = int(values[rlof_counter_index])

        rlof_episode_list = RLOF_dict.get(rlof_counter, [])
        rlof_episode_list.append(values)
        RLOF_dict[rlof_counter] = rlof_episode_list

    for episode in RLOF_dict.keys():
        # Get values again
        rlof_values = RLOF_dict[episode]

        # Create unique number
        unique_nr = str(secrets.randbits(48))

        # Write first line of whole sequence to custom file
        with open(first_line_file, "a") as first_f:
            first_f.write(separator.join(rlof_values[0] + [unique_nr]) + "\n")

        # # Write last line of whole sequence to different file
        # with open(last_line_file, 'a') as last_f:
        #     last_f.write(separator.join(rlof_values[-1] + [unique_nr])+'\n')


## Make population and set value
test_pop = Population()
test_pop.set(verbosity=1)

# Set grid variables
# resolution = {'M_1': 40, 'q': 40, 'per': 80}
resolution = {"M_1": 2, "q": 100, "per": 100}

test_pop.add_grid_variable(
    name="lnm1",
    longname="Primary mass",
    valuerange=[7, 14],
    resolution="{}".format(resolution["M_1"]),
    spacingfunc="const(math.log(7), math.log(14), {})".format(resolution["M_1"]),
    precode="M_1=math.exp(lnm1)",
    probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 101, -1.3, -2.3, -2.3)*M_1",
    dphasevol="dlnm1",
    parameter_name="M_1",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
)

test_pop.add_grid_variable(
    name="q",
    longname="Mass ratio",
    valuerange=["0.1", 1],
    resolution="{}".format(resolution["q"]),
    spacingfunc="const(0.1, 1, {})".format(resolution["q"]),
    probdist="1",
    dphasevol="dq",
    precode="M_2 = q * M_1",
    parameter_name="M_2",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
)

test_pop.add_grid_variable(
    name="log10per",  # in days
    longname="log10(Orbital_Period)",
    valuerange=[0.15, 5.5],
    resolution="{}".format(resolution["per"]),
    spacingfunc="const(0.15, 5.5, {})".format(resolution["per"]),
    precode="""orbital_period = 10** log10per
sep = calc_sep_from_period(M_1, M_2, orbital_period)
sep_min = calc_sep_from_period(M_1, M_2, 10**0.15)
sep_max = calc_sep_from_period(M_1, M_2, 10**5.5)""",
    probdist="1",
    parameter_name="orbital_period",
    dphasevol="dlog10per",
)

test_pop.set(**personal_defaults)

test_pop.set(
    david_logging_function=10,
    binary=1,
    amt_cores=2,
    verbosity=4,
    orbital_period=1200,
    M_1=1,
    M_2=8,
    parse_function=parse_function,
)

metallicity_values = [0.02]
for metallicity in metallicity_values:
    test_pop.set(
        metallicity=metallicity,
        parse_function=parse_function,
        # data_dir=os.path.join(os.environ['BINARYC_DATA_ROOT'], 'RLOF_SANA_2021_01', 'RLOFING_systems_z{}'.format(metallicity)),
        data_dir=os.path.join(
            os.getcwd(),
            "results",
            "sana_overview",
            "RLOFING_systems_z{}".format(metallicity),
        ),
    )

    if os.path.isdir(test_pop.custom_options["data_dir"]):
        shutil.rmtree(test_pop.custom_options["data_dir"])

    # create local tmp_dir
    test_pop.set(
        tmp_dir=os.path.join(test_pop.custom_options["data_dir"], "local_tmp_dir")
    )
    os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

    # Export settings:
    test_pop.export_all_info(use_datadir=True)

    # Evolve grid
    test_pop.evolve()

    # Get ensemble output and write it to file
    ensemble_output = test_pop.grid_ensemble_results
    # with open('ensemble_output.json', 'w') as f:
    #     json.dump(ensemble_output, f, indent=4)
    with open(
        os.path.join(test_pop.custom_options["data_dir"], "ensemble_output.json"), "w"
    ) as f:
        json.dump(ensemble_output, f, indent=4)
