"""
Routine to plot the fraction of initial orbital angular momentum transferred back from stream excess for stable mt onto ms
"""

import os

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

#
from grav_waves.settings import convolution_settings

#
from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict
from RLOF.paper_scripts.functions.episode_plot_functions.utility import filter_df_all

#
from david_phd_functions.plotting.custom_mpl_settings import LINESTYLE_TUPLE
from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

#
def plot_fraction_stream_torque_orbit(dataset_dict, add_cdf=False, plot_settings={}):
    """
    Function to plot the fraction of initial orbital angular momentum transferred back from stream excess for stable mt onto ms for all metallicities provided in the dataset_dict

    Args:
        dataset_dict: dictionary containing info for multiple metallicities
        plot_settings: dict controlling the plotting
    """

    min_ratio = 1
    max_ratio = 0
    fontsize = 30

    #
    ratio_dict = {}

    # Loop over metallicities
    for metallicity in dataset_dict:
        # Read out data and create dataframe
        df = pd.read_csv(dataset_dict[metallicity]["filename"], sep="\s+", header=0)

        # Filter the df
        stable_mt_ms, disk_df, total_prob = filter_df_all(df, convolution_settings)

        # Normalise to total probability of all stable MT onto MS
        disk_df.loc[:, "number_per_solar_mass"] = (
            disk_df["number_per_solar_mass"] / total_prob
        )
        # Add ratio angmom returned vs initial orbital ang mom
        disk_df.loc[:, "ratio"] = disk_df.loc[
            :, "total_orbital_angular_momentum_stream_torque_with_orbit"
        ] / (disk_df.loc[:, "initial_orbital_angular_momentum"])
        ratio_dict[metallicity] = {
            "ratio": disk_df["ratio"],
            "number_per_solar_mass": disk_df["number_per_solar_mass"],
        }

        ########
        # check if we find systems with ratio > 1
        if not disk_df[disk_df.ratio > 1].empty:
            print(
                "plot_fraction_stream_torque_orbit: systems with ratio > 1 in {}".format(
                    dataset_dict[metallicity]["filename"]
                )
            )

        # Find min and max ratio
        if disk_df["ratio"].min() < min_ratio:
            min_ratio = disk_df["ratio"].min()

        if disk_df["ratio"].max() > max_ratio:
            max_ratio = disk_df["ratio"].max()

    ######
    # Plot
    fig = plt.figure(figsize=(16, 16))

    #
    gs = fig.add_gridspec(nrows=10, ncols=1)

    #
    if not add_cdf:
        ax = fig.add_subplot(gs[:, :])
    else:
        ax = fig.add_subplot(gs[:7, :])
        ax_cdf = fig.add_subplot(gs[7:, :])

    # Determine bins
    min_abs_log = 3
    steps = 20
    bins_pos = 10 ** np.linspace(-min_abs_log, 1.0, steps)
    bins_neg = -(10 ** np.linspace(1.0, -min_abs_log, steps))
    bins = np.concatenate([bins_neg, bins_pos])

    #
    for i, metallicity in enumerate(sorted(ratio_dict)):
        # the histogram of the data
        n, bins, _ = ax.hist(
            ratio_dict[metallicity]["ratio"].to_numpy(),
            bins=bins,
            weights=ratio_dict[metallicity]["number_per_solar_mass"].to_numpy(),
            alpha=0.75,
            label="Z = {} ({:.2f})".format(
                metallicity,
                ratio_dict[metallicity]["number_per_solar_mass"].to_numpy().sum(),
            ),
            histtype="step",
            linestyle=LINESTYLE_TUPLE[i][1] if not i == 0 else "-",
            linewidth=4,
        )

        if add_cdf:
            cdf_array = np.cumsum(n) / np.sum(n)
            bincenters = (bins[1:] + bins[:-1]) / 2

            ax_cdf.plot(
                bincenters,
                cdf_array,
                alpha=0.75,
                label="Z = {}".format(metallicity),
                linestyle=LINESTYLE_TUPLE[i][1] if not i == 0 else "-",
                linewidth=4,
            )

    ###
    # make up

    # Set xlim
    # ax.set_xlim([1e-3, 1])
    # ax.set_ylim([1e-3, 1])

    # set legend
    ax.legend(loc=1, fontsize=fontsize)

    # Set labels
    ax.set_ylabel("Number of systems per formed solarmass", fontsize=fontsize)
    ax.set_xlabel(
        "Fraction of initial orbital angular momentum transferred\n back to orbit due to stream torque",
        fontsize=fontsize,
    )
    ax.set_title(
        "Fraction of initial orbital angular momentum transferred \n back to orbit due to stream torque",
        fontsize=fontsize,
    )

    # Set scales
    ax.set_yscale("log")
    ax.set_xscale("symlog", linthresh=10**-min_abs_log)

    #
    if add_cdf:
        ax_cdf.set_ylim([0, 1.1])
        # ax_cdf.set_xlim(ax.get_xlim())
        ax_cdf.set_xscale(ax.get_xscale())
        ax_cdf.set_xlabel(ax.get_xlabel())
        ax.set_xlabel("")
        ax.set_xticklabels([])
    #
    fig.tight_layout()

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    ########
    # Set up data
    simulation_dir = (
        "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_SANA/"
    )

    rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(simulation_dir)
    dataset_dict = rlof_episode_dataset_dict["datasets"]

    ########
    # Set filename
    target_dir = "/home/david/Dropbox/Academic/PHD/papers/paper_disks/paper_tex/figures/episode_plots"
    # target_dir = "plots"

    #
    basename = "fraction_stream_torque_orbit.pdf"

    #
    plot_fraction_stream_torque_orbit(
        dataset_dict,
        add_cdf=False,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(target_dir, basename),
        },
    )
