"""
Function to run all the relevant plots
"""

import os
import json
import time
import h5py

from PyPDF2 import PdfFileMerger
import fpdf

import numpy as np
import astropy.units as u

from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.plot_initial_final_mass_grid import (
    plot_initial_final_mass_grid,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.plot_initial_final_mass_grid_fraction import (
    plot_initial_final_mass_grid_fraction,
)

from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.plot_fallback_grid import (
    plot_fallback_grid,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.plot_min_max_first_bh_mass import (
    plot_min_max_first_bh_mass,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.plot_mapping_results import (
    generate_mass_mapping_plots,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.plot_edge_results import (
    plot_ppisn_edge_info,
)

from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.plot_mass_lost_supernova import (
    plot_mass_lost_supernova,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.plot_mass_lost_supernova_fraction import (
    plot_mass_lost_supernova_fraction,
)

from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.plot_mass_lost_winds import (
    plot_mass_lost_winds,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.plot_mass_lost_winds_fraction import (
    plot_mass_lost_winds_fraction,
)

#
from david_phd_functions.plotting.pdf_functions import (
    add_pdf_and_bookmark,
    generate_chapterpage_pdf,
)
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

import matplotlib

matplotlib.use("agg")  # THis solves the issues of the memory


def general_plot_routine(
    input_dataset,
    output_dir,
    combined_output_filename,
    sim_name,
    mass_scale="linear",
    edge_datafile=None,
    include_initial_final_grid_plot=True,
    include_initial_final_grid_fraction_plot=True,
    include_mass_lost_supernova_plot=True,
    include_mass_lost_supernova_fraction_plot=True,
    include_mass_lost_wind_plot=True,
    include_mass_lost_wind_fraction_plot=True,
    include_fallback_grid_plot=True,
    include_min_max_first_bh_mass_plot=True,
    include_mapping_plot=True,
    include_edge_plot=True,
):
    """
    Function to run all the plots for the
    """

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    #
    print("Generating plots for dataset in {}".format(input_dataset))

    #
    pdf_output_dir = os.path.join(output_dir, "pdf")
    os.makedirs(pdf_output_dir, exist_ok=True)

    # ####################################
    # # Details about the redshift interpolator
    # interpolation_analysis_chapter_filename = os.path.join(plot_dir, type_key, 'pdf', 'interpolation_analysis_chapter.pdf')
    # generate_chapterpage_pdf("Redshift interpolation error chapter", interpolation_analysis_chapter_filename)
    # merger, pdf_page_number = add_pdf_and_bookmark(merger, interpolation_analysis_chapter_filename, pdf_page_number, bookmark_text="Redshift interpolation error chapter")

    ##########
    # Initial final mass grid
    custom_colorlimits = [0, 55]
    if include_initial_final_grid_plot:
        plot_initial_final_mass_grid_filename = os.path.join(
            pdf_output_dir, "plot_initial_final_mass_grid.pdf"
        )
        print("\tPlotting {}".format(plot_initial_final_mass_grid_filename))
        plot_initial_final_mass_grid(
            datafile=input_dataset,
            custom_colorlimits=custom_colorlimits,
            plot_settings={
                "show_plot": False,
                "output_name": plot_initial_final_mass_grid_filename,
                "simulation_name": sim_name,
                "antialiased": True,
                "rasterized": True,
                "runname": "plot_initial_final_mass_grid",
                "mass_scale": mass_scale,
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_initial_final_mass_grid_filename,
            pdf_page_number,
            bookmark_text="initial final mass grid",
        )

    ##########
    # Initial final mass grid fraction
    if include_initial_final_grid_fraction_plot:
        plot_initial_final_mass_grid_fraction_filename = os.path.join(
            pdf_output_dir, "plot_initial_final_mass_grid_fraction.pdf"
        )
        print("\tPlotting {}".format(plot_initial_final_mass_grid_fraction_filename))
        plot_initial_final_mass_grid_fraction(
            datafile=input_dataset,
            custom_colorlimits=[0, 1],
            plot_settings={
                "show_plot": False,
                "output_name": plot_initial_final_mass_grid_fraction_filename,
                "simulation_name": sim_name,
                "antialiased": True,
                "rasterized": True,
                "runname": "plot_initial_final_mass_grid_fraction",
                "mass_scale": mass_scale,
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_initial_final_mass_grid_fraction_filename,
            pdf_page_number,
            bookmark_text="initial final mass grid fraction",
        )

    ########
    # Fallback grid
    if include_fallback_grid_plot:
        plot_initial_final_mass_grid_filename = os.path.join(
            pdf_output_dir, "plot_fallback_grid.pdf"
        )
        print("\tPlotting {}".format(plot_initial_final_mass_grid_filename))
        plot_fallback_grid(
            datafile=input_dataset,
            plot_settings={
                "show_plot": False,
                "output_name": plot_initial_final_mass_grid_filename,
                "simulation_name": sim_name,
                "antialiased": True,
                "rasterized": True,
                "runname": "plot_fallback_grid",
                "mass_scale": mass_scale,
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_initial_final_mass_grid_filename,
            pdf_page_number,
            bookmark_text="fallback fraction grid",
        )

    ##########
    # mass lost in supernova
    if include_mass_lost_supernova_plot:
        plot_mass_lost_supernova_filename = os.path.join(
            pdf_output_dir, "plot_mass_lost_supernova.pdf"
        )
        print("\tPlotting {}".format(plot_mass_lost_supernova_filename))
        plot_mass_lost_supernova(
            datafile=input_dataset,
            plot_settings={
                "show_plot": False,
                "output_name": plot_mass_lost_supernova_filename,
                "simulation_name": sim_name,
                "antialiased": True,
                "rasterized": True,
                "runname": "plot_mass_lost_supernova",
                "mass_scale": mass_scale,
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_mass_lost_supernova_filename,
            pdf_page_number,
            bookmark_text="mass lost supernova",
        )

    ##########
    # mass lost in supernova fraction
    if include_mass_lost_supernova_fraction_plot:
        plot_mass_lost_supernova_fraction_filename = os.path.join(
            pdf_output_dir, "plot_mass_lost_supernova_fraction.pdf"
        )
        print("\tPlotting {}".format(plot_mass_lost_supernova_fraction_filename))
        plot_mass_lost_supernova_fraction(
            datafile=input_dataset,
            custom_colorlimits=[0, 1],
            plot_settings={
                "show_plot": False,
                "output_name": plot_mass_lost_supernova_fraction_filename,
                "simulation_name": sim_name,
                "antialiased": True,
                "rasterized": True,
                "runname": "plot_mass_lost_supernova",
                "mass_scale": mass_scale,
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_mass_lost_supernova_fraction_filename,
            pdf_page_number,
            bookmark_text="mass lost supernova fraction",
        )

    ##########
    # mass lost in wind
    if include_mass_lost_wind_plot:
        plot_mass_lost_wind_filename = os.path.join(
            pdf_output_dir, "plot_mass_lost_wind.pdf"
        )
        print("\tPlotting {}".format(plot_mass_lost_wind_filename))
        plot_mass_lost_winds(
            datafile=input_dataset,
            plot_settings={
                "show_plot": False,
                "output_name": plot_mass_lost_wind_filename,
                "simulation_name": sim_name,
                "antialiased": True,
                "rasterized": True,
                "runname": "plot_mass_lost_wind",
                "mass_scale": mass_scale,
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_mass_lost_wind_filename,
            pdf_page_number,
            bookmark_text="mass lost wind",
        )

    ##########
    # mass lost in wind fraction
    if include_mass_lost_wind_fraction_plot:
        plot_mass_lost_wind_fraction_filename = os.path.join(
            pdf_output_dir, "plot_mass_lost_wind_fraction.pdf"
        )
        print("\tPlotting {}".format(plot_mass_lost_wind_fraction_filename))
        plot_mass_lost_winds_fraction(
            datafile=input_dataset,
            custom_colorlimits=[0, 1],
            plot_settings={
                "show_plot": False,
                "output_name": plot_mass_lost_wind_fraction_filename,
                "simulation_name": sim_name,
                "antialiased": True,
                "rasterized": True,
                "runname": "plot_mass_lost_wind",
                "mass_scale": mass_scale,
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_mass_lost_wind_fraction_filename,
            pdf_page_number,
            bookmark_text="mass lost wind fraction",
        )

    ########
    # first and max mass bh
    if include_min_max_first_bh_mass_plot:
        plot_min_max_first_bh_mass_filename = os.path.join(
            pdf_output_dir, "plot_min_max_first_bh_mass.pdf"
        )
        print("\tPlotting {}".format(plot_min_max_first_bh_mass_filename))
        plot_min_max_first_bh_mass(
            datafile=input_dataset,
            plot_settings={
                "show_plot": False,
                "output_name": plot_min_max_first_bh_mass_filename,
                "simulation_name": sim_name,
                "antialiased": True,
                "rasterized": True,
                "runname": "plot_min_max_first_bh_mass",
                "mass_scale": mass_scale,
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_min_max_first_bh_mass_filename,
            pdf_page_number,
            bookmark_text="First and max bh mass",
        )

    ########
    # Mapping plots
    if include_mapping_plot:
        generate_mass_mapping_plots_filename = os.path.join(
            pdf_output_dir, "mass_mapping_plots.pdf"
        )
        print(
            "\tCreating mapping plots pdf {}".format(
                generate_mass_mapping_plots_filename
            )
        )
        generate_mass_mapping_plots(
            datafile=input_dataset,
            combined_output_filename=generate_mass_mapping_plots_filename,
            output_dir=os.path.join(pdf_output_dir, "mapping_plots"),
            plot_settings={
                "show_plot": False,
                "simulation_name": sim_name,
                "antialiased": True,
                "rasterized": True,
                "runname": "plot_min_max_first_bh_mass",
                "mass_scale": mass_scale,
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            generate_mass_mapping_plots_filename,
            pdf_page_number,
            bookmark_text="Mass mapping plots",
            add_source_bookmarks=True,
        )

    ########
    # Edge plot:
    if include_edge_plot:
        plot_ppisn_edge_info_filename = os.path.join(
            pdf_output_dir, "plot_ppisn_edge_info.pdf"
        )
        print("plotting: {}".format(plot_ppisn_edge_info_filename))
        plot_ppisn_edge_info(
            result_filename=edge_datafile,
            plot_settings={
                "show_plot": False,
                "output_name": plot_ppisn_edge_info_filename,
                "simulation_name": sim_name,
                "antialiased": True,
                "rasterized": True,
                "runname": "plot_ppisn_edge_info",
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_ppisn_edge_info_filename,
            pdf_page_number,
            bookmark_text="PPISN edge location and jump size",
        )

    # wrap up the pdf
    merger.write(combined_output_filename)
    merger.close()

    print(
        "Finished generating plots for dataset {}. Wrote results to {}".format(
            input_dataset, combined_output_filename
        )
    )
