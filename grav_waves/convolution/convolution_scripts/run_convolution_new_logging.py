"""
Function to run the convolution (including building the combined dco dataset, filtering and the actual convolution)
"""

import os
import logging

import numpy as np

from grav_waves.settings import convolution_settings, config_dict_cosmology
from grav_waves.gw_analysis.functions.functions import make_dataset_dict
from grav_waves.convolution.scripts.convolve_with_new_logging.functions import (
    create_combined_dco_dataset,
    filter_combined_dco_dataset_function,
)
from grav_waves.convolution.functions.convolution_functions import convolution_main

#
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def run_convolution_new_function(
    main_dir,
    convolution_output_dir,
    convolution_settings,
    cosmology_settings,
    run_combine_datasets=False,
    run_filter_combined_dataset=False,
    filter_combined_dataset_function=None,
    run_convolution=False,
    verbosity=1,
):
    """
    Function to run the convolution and handle the all the things
    """

    # Get the correct directories
    dataset_dict_populations = make_dataset_dict(main_dir)
    os.makedirs(convolution_output_dir, exist_ok=True)

    # Create names for the output files
    combined_dco_dataset_filename = os.path.join(
        convolution_output_dir, "combined_dco_dataset.h5"
    )
    filtered_combined_dco_dataset_filename = os.path.join(
        convolution_output_dir, "filtered_combined_dco_dataset.h5"
    )
    redshift_dco_convolution_filename = os.path.join(
        convolution_output_dir, "dco_convolution_results.h5"
    )

    ##############
    # Build input dataset
    used_combined_dco_dataset_filename = combined_dco_dataset_filename
    if run_combine_datasets:
        create_combined_dco_dataset(
            dataset_dict=dataset_dict_populations,
            convolution_configuration=convolution_settings,
            cosmology_configuration=cosmology_settings,
            output_filename=combined_dco_dataset_filename,
            dco_type=convolution_settings["dco_type"],
        )

    ##############
    # Build combined dataset merging systems
    if run_filter_combined_dataset:
        filter_combined_dataset_function(
            input_hdf5file=combined_dco_dataset_filename,
            output_hdf5file=filtered_combined_dco_dataset_filename,
        )
        used_combined_dco_dataset_filename = filtered_combined_dco_dataset_filename

    ##############
    # Run convolution
    if run_convolution:
        convolution_main(
            input_filename=used_combined_dco_dataset_filename,
            convolution_configuration=convolution_settings,
            cosmology_configuration=cosmology_settings,
            output_filename=redshift_dco_convolution_filename,
            verbosity=verbosity,
        )


#
if __name__ == "__main__":
    output_root = os.path.join(this_file_dir)

    #######################
    # Select simulation

    # Fiducial
    main_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED"
    convolution_output_dir = os.path.join(main_dir, "dco_convolution_results/")

    #
    convolution_stepsize = 0.1
    convolution_settings["dco_type"] = "combined_dco"
    convolution_settings["stepsize"] = convolution_stepsize
    convolution_settings["time_bins"] = np.arange(
        1e-6 - 0.5 * convolution_stepsize,
        (convolution_settings["max_interpolation_redshift"] - 1)
        + 0.5 * convolution_stepsize,
        convolution_stepsize,
    )
    convolution_settings["time_centers"] = (
        convolution_settings["time_bins"][1:] + convolution_settings["time_bins"][:-1]
    ) / 2
    convolution_settings["amt_cores"] = 1
    convolution_settings["convolution_tmp_dir"] = os.path.join(
        convolution_output_dir, "tmp"
    )
    convolution_settings["include_birth_rates"] = False
    convolution_settings["include_formation_rates"] = False
    convolution_settings["include_merger_rates"] = True
    convolution_settings["include_detection_rates"] = False
    convolution_settings["combined_data_array_columns"] = [
        "number_per_solar_mass_values",
        "mass_1",
        "mass_2",
        "metallicity",
    ]
    convolution_settings["logger"].setLevel(logging.DEBUG)
    # convolution_settings['global_query'] = '(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0)'

    ##############
    #
    run_convolution_new_function(
        main_dir,
        convolution_output_dir,
        convolution_settings=convolution_settings,
        cosmology_settings=config_dict_cosmology,
        run_combine_datasets=True,
        run_filter_combined_dataset=True,
        filter_combined_dataset_function=filter_combined_dco_dataset_function,
        run_convolution=True,
        verbosity=0,
    )
