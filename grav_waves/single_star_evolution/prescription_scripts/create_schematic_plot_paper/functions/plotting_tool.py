"""
Function to make the plotting tool
"""

import numpy as np
import matplotlib.pyplot as plt


def add_bracket(fig, axis, endpoint_1, endpoint_2, startpoint, width, **kwargs):
    """
    requires 3 locations and a width:
    - endpoint 1 and 2: locations of the endpoints
    - startpoint: location where the line stars
    - width: distance between the end points and its corner points
    """

    vec_endpoint_1 = np.array(endpoint_1)
    vec_endpoint_2 = np.array(endpoint_2)

    line_between = vec_endpoint_2 - vec_endpoint_1

    perp_vector = np.array([line_between[1], line_between[0]])
    perp_unit_vector = perp_vector / np.linalg.norm(perp_vector)

    cornerpoint_shift = perp_unit_vector * width

    vec_cornerpoint_1 = vec_endpoint_1 + cornerpoint_shift
    vec_cornerpoint_2 = vec_endpoint_2 + cornerpoint_shift

    #
    vec_midpoint = (vec_cornerpoint_1 + vec_cornerpoint_2) / 2

    # vec_startpoint
    vec_startpoint = np.array(startpoint)

    # # Plot endpoints
    # endpoint_list = [vec_endpoint_1, vec_endpoint_2]
    # x, y = zip(*endpoint_list)
    # plt.scatter(x, y, label='endpoints')

    # # Plot cornerpoints:
    # cornerpoint_list = [vec_cornerpoint_1, vec_cornerpoint_2]
    # x, y = zip(*cornerpoint_list)
    # plt.scatter(x, y, label='cornerpoints')

    # # plot dots at midpoint
    # plt.scatter(vec_midpoint[0], vec_midpoint[1], label='midpoint')

    # # Plot dot at startpoint
    # plt.scatter(vec_startpoint[0], vec_startpoint[1])
    # plt.legend()

    # Plot line between endpoints and cornerpoints
    axis.plot(
        [vec_endpoint_1[0], vec_cornerpoint_1[0]],
        [vec_endpoint_1[1], vec_cornerpoint_1[1]],
        **kwargs,
    )
    axis.plot(
        [vec_endpoint_2[0], vec_cornerpoint_2[0]],
        [vec_endpoint_2[1], vec_cornerpoint_2[1]],
        **kwargs,
    )

    # Cornerpoints to midpoints
    axis.plot(
        [vec_cornerpoint_1[0], vec_midpoint[0]],
        [vec_cornerpoint_1[1], vec_midpoint[1]],
        **kwargs,
    )
    axis.plot(
        [vec_cornerpoint_2[0], vec_midpoint[0]],
        [vec_cornerpoint_2[1], vec_midpoint[1]],
        **kwargs,
    )

    # Midpoint to starting point
    axis.plot(
        [vec_midpoint[0], vec_startpoint[0]],
        [vec_midpoint[1], vec_startpoint[1]],
        **kwargs,
    )

    return fig, axis


def example_plot():
    """ """

    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(10, 10))

    fig, axes = add_bracket(
        fig,
        axes,
        (1, 1),
        (3, 3),
        (10, 2),
        -1.5,
        linestyle="--",
        color="black",
        linewidth=5,
    )

    plt.show()


example_plot()
