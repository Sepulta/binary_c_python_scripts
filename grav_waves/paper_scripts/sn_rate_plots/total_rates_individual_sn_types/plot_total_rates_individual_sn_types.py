"""
plot sn rate output
"""

import os
import h5py
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt


from binarycpython.utils.dicts import merge_dicts

from grav_waves.paper_scripts.sn_rate_plots.total_rates_individual_sn_types.functions import (
    readout_binary_dco_rates,
    readout_sn_rates,
    calculate_min_max_values,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.linestyle_hatch_colorlist import linestyle_list

custom_mpl_settings.load_mpl_rc()


color_list = ["blue", "orange", "green", "yellow", "pink", "red"]
hatch_alpha = 0.1


def merge_dicts(dict_1, dict_2):
    """
    Function for merging of dicts
    TODO: this can be written better
    """

    new_dict = {}
    for SN_type in dict_1.keys():

        new_dict[SN_type] = dict_1[SN_type]

        if SN_type in dict_2:
            new_dict[SN_type]["rates"] += dict_2[SN_type]["rates"]

    return new_dict


def plot_total_rates_individual_sn_types(
    dataset_dict, include_single, event_based, filter_fb, plot_settings={}
):
    """
    function to plot total rate over redshift for individual SN types
    """

    #
    alpha_fiducial = 0.25
    color_fiducial_dco = "k"

    #
    all_rates_dict = {}

    #
    num_datasets = len(list(dataset_dict.keys()))

    # Loop over the datasets
    for dataset_i, dataset in enumerate(dataset_dict):
        print("Calculating rates for {}".format(dataset))

        # Only read out DCO data for fiducial
        if dataset_i == 0:
            # Binary DCO
            binary_dco_rates = readout_binary_dco_rates(
                dataset_dict[dataset]["binary_dco_filename"]
            )

        # Binary SN
        binary_sn_rates = readout_sn_rates(
            dataset_dict[dataset]["binary_sn_filename"],
            event_based=event_based,
            filter_fb=filter_fb
            # filter_function=binary_sn_filter_function
        )

        # Combine the SN rates
        sn_rates = binary_sn_rates

        if include_single:
            # print(sn_rates)
            if "single_sn_filename" in dataset_dict[dataset]:
                # Single SN rates
                single_sn_rates = readout_sn_rates(
                    dataset_dict[dataset]["single_sn_filename"],
                    event_based=event_based,
                    filter_fb=filter_fb
                    # filter_function=single_sn_filter_function
                )

                # combine the SN rates
                sn_rates = merge_dicts(sn_rates, single_sn_rates)

        # Store
        all_rates_dict[dataset] = {
            "dco_rates": binary_dco_rates,
            "sn_rates": sn_rates,
            "name": dataset_dict[dataset]["name"],
        }

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(18, 9))
    gs = fig.add_gridspec(nrows=1, ncols=9)

    ax = fig.add_subplot(gs[:, :])

    ##########################
    # Plot fiducial DCO
    ax.plot(
        all_rates_dict["fiducial"]["dco_rates"]["bhbh"]["redshifts"],
        all_rates_dict["fiducial"]["dco_rates"]["bhbh"]["rates"],
        label=all_rates_dict["fiducial"]["dco_rates"]["bhbh"]["label"],
        linestyle=linestyle_list[1],
        color=color_fiducial_dco,
        alpha=alpha_fiducial,
    )
    ax.plot(
        all_rates_dict["fiducial"]["dco_rates"]["bhns"]["redshifts"],
        all_rates_dict["fiducial"]["dco_rates"]["bhns"]["rates"],
        label=all_rates_dict["fiducial"]["dco_rates"]["bhns"]["label"],
        linestyle=linestyle_list[2],
        color=color_fiducial_dco,
        alpha=alpha_fiducial,
    )
    ax.plot(
        all_rates_dict["fiducial"]["dco_rates"]["nsns"]["redshifts"],
        all_rates_dict["fiducial"]["dco_rates"]["nsns"]["rates"],
        label=all_rates_dict["fiducial"]["dco_rates"]["nsns"]["label"],
        linestyle=linestyle_list[3],
        color=color_fiducial_dco,
        alpha=alpha_fiducial,
    )

    ##########################
    # Plot fiducial SN rates
    ax.plot(
        all_rates_dict["fiducial"]["sn_rates"]["CC"]["redshifts"],
        all_rates_dict["fiducial"]["sn_rates"]["CC"]["rates"],
        label=all_rates_dict["fiducial"]["name"]
        + " "
        + all_rates_dict["fiducial"]["sn_rates"]["CC"]["label"],
        alpha=alpha_fiducial,
        color=color_list[0],
    )
    ax.plot(
        all_rates_dict["fiducial"]["sn_rates"]["PPISN"]["redshifts"],
        all_rates_dict["fiducial"]["sn_rates"]["PPISN"]["rates"],
        label=all_rates_dict["fiducial"]["name"]
        + " "
        + all_rates_dict["fiducial"]["sn_rates"]["PPISN"]["label"],
        alpha=alpha_fiducial,
        color=color_list[1],
    )
    ax.plot(
        all_rates_dict["fiducial"]["sn_rates"]["PISN"]["redshifts"],
        all_rates_dict["fiducial"]["sn_rates"]["PISN"]["rates"],
        label=all_rates_dict["fiducial"]["name"]
        + " "
        + all_rates_dict["fiducial"]["sn_rates"]["PISN"]["label"],
        alpha=alpha_fiducial,
        color=color_list[2],
    )

    ################
    # Plot core mass variation SN rates
    ax.plot(
        all_rates_dict["variation"]["sn_rates"]["CC"]["redshifts"],
        all_rates_dict["variation"]["sn_rates"]["CC"]["rates"],
        label=all_rates_dict["variation"]["name"]
        + " "
        + all_rates_dict["variation"]["sn_rates"]["CC"]["label"],
        color=color_list[0],
        linestyle="--",
    )
    ax.plot(
        all_rates_dict["variation"]["sn_rates"]["PPISN"]["redshifts"],
        all_rates_dict["variation"]["sn_rates"]["PPISN"]["rates"],
        label=all_rates_dict["variation"]["name"]
        + " "
        + all_rates_dict["variation"]["sn_rates"]["PPISN"]["label"],
        color=color_list[1],
        linestyle="--",
    )
    ax.plot(
        all_rates_dict["variation"]["sn_rates"]["PISN"]["redshifts"],
        all_rates_dict["variation"]["sn_rates"]["PISN"]["rates"],
        label=all_rates_dict["variation"]["name"]
        + " "
        + all_rates_dict["variation"]["sn_rates"]["PISN"]["label"],
        color=color_list[2],
        linestyle="--",
    )

    ############
    # Add fill-between for the (P)PISNe
    ax.fill_between(
        all_rates_dict["fiducial"]["sn_rates"]["PPISN"]["redshifts"],
        all_rates_dict["fiducial"]["sn_rates"]["PPISN"]["rates"],
        all_rates_dict["variation"]["sn_rates"]["PPISN"]["rates"],
        fc=color_list[1],
        hatch="//",
        alpha=hatch_alpha,
    )

    ax.fill_between(
        all_rates_dict["fiducial"]["sn_rates"]["PISN"]["redshifts"],
        all_rates_dict["fiducial"]["sn_rates"]["PISN"]["rates"],
        all_rates_dict["variation"]["sn_rates"]["PISN"]["rates"],
        fc=color_list[2],
        hatch="\\\\",
        alpha=hatch_alpha,
    )

    #############
    # Make up
    ax.set_xlim([-0.4, 8])
    ax.set_ylim([1e-1, 10**6])
    ax.set_yscale("log")
    ax.legend(ncol=(num_datasets + 1))

    #
    # ax.set_title("Intrinsic event rate density")
    ax.set_ylabel(
        r"$\mathcal{R}_{event}(\it{z})\ \left[\frac{d^{2}\,\it{N}_{event}}{d\it{t}\,d\it{V}_{\mathrm{c}}}\right]\ \left(\frac{1}{yr^{-1}\,Gpc^{-3}}\right)$",
        fontsize=plot_settings.get("axislabel_fontsize", 26),
    )
    ax.set_xlabel(
        r"Event redshift [$\it{z}$]",
        fontsize=plot_settings.get("axislabel_fontsize", 26),
    )

    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    include_single = True
    event_based = True
    filter_fb = True
    add_observational_rates = False

    ######
    # result root
    result_root = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results"
    result_root = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"), "GRAV_WAVES/server_results/"
    )

    # ######
    # # Define the dataset
    # dataset_dict = {
    #     "fiducial": {
    #         "binary_dco_filename": os.path.join(
    #             result_root,
    #             "NEW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/dco_convolution_results.h5",
    #         ),
    #         "binary_sn_filename": os.path.join(
    #             result_root,
    #             "NEW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/sn_convolution_results/sn_convolution_results.h5",
    #         ),
    #         "single_sn_filename": os.path.join(
    #             result_root,
    #             "NEW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/single_sn_convolution_results/single_sn_convolution_results.h5",
    #         ),
    #         "name": "fiducial",
    #     },
    #     "core_mass_shift": {
    #         "binary_sn_filename": os.path.join(
    #             result_root,
    #             "NEW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/sn_convolution_results/sn_convolution_results.h5",
    #         ),
    #         "binary_dco_filename": os.path.join(
    #             result_root,
    #             "NEW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/dco_convolution_results/dco_convolution_results.h5",
    #         ),
    #         "name": "$\Delta M_{\mathrm{CO,\ PPI}} = -15$",
    #     },
    # }

    # #
    # base_name = "sn_rates_over_redshift_include_single_{}.pdf".format(include_single)
    # output_dir = "/home/david/Dropbox/Academic/PHD/papers/paper_gw/paper_tex/figures/fiducial_sn_over_redshift"
    # output_dir = "plots/"

    # plot_total_rates_individual_sn_types(
    #     dataset_dict=dataset_dict,
    #     include_single=include_single,
    #     plot_settings={
    #         "output_name": os.path.join(output_dir, base_name),
    #         "axislabel_fontsize": 32,
    #     },
    # )

    ######
    # Define the dataset
    dataset_dict = {
        "fiducial": {
            "binary_dco_filename": os.path.join(
                result_root,
                "LOW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
            ),
            "binary_sn_filename": os.path.join(
                result_root,
                "LOW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/sn_convolution_results/convolved_sn_dataset.h5",
            ),
            "single_sn_filename": os.path.join(
                result_root,
                "LOW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/single_sn_convolution_results/convolved_sn_dataset.h5",
            ),
            "name": "Fid.",
        },
        "variation": {
            "binary_dco_filename": os.path.join(
                result_root,
                "LOW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/dco_convolution_results/convolution_results.h5",
            ),
            "binary_sn_filename": os.path.join(
                result_root,
                "LOW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/sn_convolution_results/convolved_sn_dataset.h5",
            ),
            "single_sn_filename": os.path.join(
                result_root,
                "LOW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-15_PPISN_core_mass_range_shift/single_sn_convolution_results/convolved_sn_dataset.h5",
            ),
            "name": "$\Delta M_{\mathrm{CO,\ PPI}} = -15$",
        },
    }

    #
    base_name = "sn_rates_over_redshift_include_single_{}_event_based_{}_filter_fb_{}.pdf".format(
        include_single, event_based, filter_fb
    )
    output_dir = "/home/david/Dropbox/Academic/PHD/papers/paper_gw/paper_tex/figures/fiducial_sn_over_redshift"
    output_dir = "plots/"

    plot_total_rates_individual_sn_types(
        dataset_dict=dataset_dict,
        include_single=include_single,
        event_based=event_based,
        filter_fb=filter_fb,
        plot_settings={
            "output_name": os.path.join(output_dir, base_name),
            "axislabel_fontsize": 32,
        },
    )
