import os
import json
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

#
from grav_waves.settings import convolution_settings
from binarycpython.utils.dicts import multiply_values_dict

#
from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict
from RLOF.paper_scripts.functions.episode_plot_functions.utility import filter_df_all

#
from david_phd_functions.plotting.custom_mpl_settings import LINESTYLE_TUPLE
from david_phd_functions.plotting.utils import show_and_save_plot, add_plot_info
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

from grav_waves.settings import convolution_settings


from ensemble_functions.ensemble_functions import (
    get_recursive_sum,
    merge_all_subdicts,
    return_keys_bins_and_bincenters,
    flatten_data_ensemble2d,
    query_dict,
    recursively_merge_all_subdicts,
    merge_recursive_until_stop,
    get_recursive_sum_for_subkeys_1d,
)


def get_ensemble_data(dataset, testing, probability_to_yield_conversion_factor=1):
    """
    Function to get the ensemble data for testing or production purposes
    """

    # Select filename
    ensemble_filename = dataset["filename"]
    if testing:
        if "filename_test" in dataset.keys():
            ensemble_filename = dataset["filename_test"]

    #
    ensemble_data = json.load(open(ensemble_filename, "r"))

    #
    if not testing:
        ensemble_data = ensemble_data["ensemble"]

    if probability_to_yield_conversion_factor != 1:
        ensemble_data = multiply_values_dict(
            ensemble_data, probability_to_yield_conversion_factor
        )

    return ensemble_data


# result_dir = "/home/david/projects/binary_c_root/results/RLOF/TEST_RES_SANA/population_results/Z0.02/"
result_dir = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_LOW_RES_SANA/population_results/Z0.02/"
ensemble_filename = os.path.join(result_dir, "ensemble_output.json")
episode_filename = os.path.join(result_dir, "RLOF_episode_data.dat")


df = pd.read_csv(episode_filename, sep="\s+", header=0)

# print(df.columns)
# print(df['total_orbital_angular_momentum_stream_torque_with_orbit'])
# print(df['total_orbital_angular_momentum_stream_excess_returned_to_orbit'])


# print(df['time_weighted_sum_synchronicity_factor'])
# print(df['mass_weighted_sum_synchronicity_factor'])

# quit()


ensemble_dict = {"filename": ensemble_filename}
probability_to_yield_conversion_factor = (
    convolution_settings["binary_fraction"]
    * convolution_settings["average_mass_system"]
)
print("loading")
ensemble_data = get_ensemble_data(
    ensemble_dict,
    False,
    probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
)["RLOF"]["fraction_separation_massratio"]["mass"]

print("querying")
stable_subensemble = query_dict(ensemble_data, "stable", [-0.5, 0.5])
ms_stable_subensemble = query_dict(stable_subensemble, "st_acc", [0.5, 1.5])
ms_disk_mass_stable_ensemble = query_dict(ms_stable_subensemble, "disk", [0.5, 1.5])
print("merging subdicts")
merged_disk_ms_stable_subensemble = merge_recursive_until_stop(
    ms_disk_mass_stable_ensemble, "frac_sync_donor"
)
print("calculating sum for subdicts")
result_dict = get_recursive_sum_for_subkeys_1d(
    merged_disk_ms_stable_subensemble, "frac_sync_donor"
)
print(result_dict)
