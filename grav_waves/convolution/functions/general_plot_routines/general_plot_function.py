"""
General plotting routines for the convolution plotting

Tasks:
    TODO: add information of how much time is spent in each function
    TODO: properly pass through the verbosity

Plots:
    TODO: add any mass vs redshift
    TODO: add massratio vs metallicity at z=0
"""

import os
import json
import time
import h5py

from PyPDF2 import PdfFileMerger
import fpdf

import numpy as np
import astropy.units as u

from grav_waves.convolution.functions.convolution_general_functions import (
    custom_json_serializer,
)

#
from grav_waves.convolution.functions.compas_metallicity_distribution import (
    find_metallicity_distribution,
)
from grav_waves.gw_analysis.functions.cosmology_functions import (
    starformation_rate,
    metallicity_distribution,
)

#
from grav_waves.convolution.functions.general_plot_function_metallicity_sfr_distribution import (
    general_plot_function as general_plot_function_metallicity_sfr_distribution,
)

# Normal convolution plots
from grav_waves.convolution.functions.plot_routines.plot_rate_density_at_zero_redshift import (
    plot_rate_density_at_zero_redshift,
)
from grav_waves.convolution.functions.plot_routines.plot_birthrate import plot_birthrate
from grav_waves.convolution.functions.plot_routines.plot_total_intrinsic_rate_density_at_all_lookbacks import (
    plot_total_intrinsic_rate_density_at_all_lookbacks,
)
from grav_waves.convolution.functions.plot_routines.plot_all_rates_and_metallicity_distribution import (
    plot_all_rates_and_metallicity_distribution,
)
from grav_waves.convolution.functions.plot_routines.plot_convolved_rate_density_per_mass_quantity import (
    plot_convolved_rate_density_per_mass_quantity,
)
from grav_waves.convolution.functions.plot_routines.plot_massratio_density_at_zero_redshift import (
    plot_massratio_density_at_zero_redshift,
)
from grav_waves.convolution.functions.plot_routines.plot_convolved_rate_density_per_mass_ratio import (
    plot_convolved_rate_density_per_mass_ratio,
)
from grav_waves.convolution.functions.plot_routines.plot_convolved_rate_density_per_metallicity import (
    plot_convolved_rate_density_per_metallicity,
)
from grav_waves.convolution.functions.plot_routines.plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity import (
    plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity,
)
from grav_waves.convolution.functions.plot_routines.plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity import (
    plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity,
)
from grav_waves.convolution.functions.plot_routines.plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_metallicity import (
    plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_metallicity,
)
from grav_waves.convolution.functions.plot_routines.plot_primary_vs_secondary_at_specific_redshift import (
    plot_primary_vs_secondary_at_specific_redshift,
)
from grav_waves.convolution.functions.plot_routines.plot_any_mass_at_zero_redshift import (
    plot_any_mass_at_zero_redshift,
)

#
from grav_waves.convolution.functions.plot_routines.plot_volume_convolved_rate import (
    plot_volume_convolved_rate,
)
from grav_waves.convolution.functions.plot_routines.plot_total_observable_rates import (
    plot_total_observable_rates,
)
from grav_waves.convolution.functions.plot_routines.plot_comoving_shell_volumes import (
    plot_comoving_shell_volumes,
)
from grav_waves.convolution.functions.plot_routines.plot_interpolation_datasets import (
    plot_interpolation_datasets,
)

#
from david_phd_functions.plotting.pdf_functions import (
    add_pdf_and_bookmark,
    generate_chapterpage_pdf,
)
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def fix_dict(input_dict):
    """
    Recursive function that removes the quantity stuff in the dictionary
    """

    new_dict = {}

    for key in input_dict:
        if isinstance(input_dict[key], dict):
            new_dict[key] = fix_dict(input_dict[key])
        elif isinstance(input_dict[key], u.Quantity):
            new_dict[key] = str(input_dict[key])
        elif isinstance(input_dict[key], np.ndarray):
            with np.printoptions(precision=2, suppress=True, threshold=5):
                new_dict[key] = str(input_dict[key])
        else:
            new_dict[key] = input_dict[key]

    return new_dict


def generate_frontpage_pdf_sfr_convolution(
    dataset_dict, cosmology_configuration, convolution_configuration, output_name
):
    """
    Function to generate a pdf containing some information about the dataset
    """

    sim_name = dataset_dict["sim_name"]

    # Write settings to pdf
    pdf = fpdf.FPDF(format="letter")
    pdf.add_page()
    pdf.set_font("Arial", size=24)
    pdf.write(10, "{} plots".format(sim_name))
    pdf.ln()
    pdf.set_font("Arial", size=12)
    pdf.ln()
    pdf.write(
        5,
        "pdf generated on: {}".format(
            time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        ),
    )
    pdf.ln()
    pdf.ln()
    pdf.write(10, "SETTINGS")
    pdf.ln()
    pdf.ln()
    pdf.write(10, "Cosmology settings")
    pdf.write(
        5,
        json.dumps(
            fix_dict(cosmology_configuration), indent=4, default=custom_json_serializer
        ),
    )
    pdf.ln()
    pdf.ln()
    pdf.write(10, "Convolution settings")
    pdf.write(
        5,
        json.dumps(
            fix_dict(convolution_configuration),
            indent=4,
            default=custom_json_serializer,
        ),
    )
    pdf.ln()
    pdf.output(output_name)


def general_plot_routine(
    convolution_dataset_hdf5_filename,
    main_plot_dir,
    output_pdf_filename,
    divide_by_binsize=False,
    dco_type="bhbh",
    querylist=None,
    add_cdf=True,
    add_ratio=True,
    num_procs=1,
    num_procs_bootstrap=1,
    bootstraps=0,
    include_starformation_rate_plots=True,
    include_plot_birthrate=True,
    include_merger_and_formation_rate_densities_over_redshift_plots=True,
    include_merger_and_formation_rate_densities_at_redshift_zero_plots=True,
    include_plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity=True,
    include_plot_primary_vs_secondary_at_specific_redshift=True,
    include_plot_any_mass_at_zero_redshift=True,
    include_plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity=True,
    include_plot_convolved_rate_density_per_metallicity=True,
    include_plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_metallicity=True,
    include_massratio_density_at_zero_redshift=True,
    include_convolved_rate_density_per_mass_ratio=True,
    include_total_intrinsic_merger_rates_over_redshift_plots=True,
    include_interpolation_error_plot=True,
):
    """
    Function to plot all the convolved quantities

    plots all above plots multiple times with different probability floors
    """

    probability_floors = [3]
    format_types = ["pdf"]
    contourlevels = [1e-1, 1e0, 1e1, 1e2]
    linestyles_contourlevels = ["solid", "dashed", "-.", ":"]

    # Open hdf5
    hdf5_file = h5py.File(convolution_dataset_hdf5_filename, "r")

    settings = hdf5_file["settings"]
    config_dict_convolution = json.loads(settings["convolution_configuration"][()])
    cosmology_configuration = json.loads(settings["cosmology_configuration"][()])
    dataset_dict = json.loads(settings["dataset_dict"][()])
    metallicity_dict = json.loads(settings["metallicity_settings"][()])

    # Close hdf5 again cause we need to open it in each routine
    hdf5_file.close()

    # mass bins
    mass_bins = {
        "chirpmass": np.arange(0, 60, 1),
        "total_mass": np.arange(0, 140, 1),
        "primary_mass": np.arange(0, 70, 1),
        "secondary_mass": np.arange(0, 70, 1),
        "any_mass": np.arange(0, 70, 1),
    }

    mass_types = ["chirpmass", "total_mass", "primary_mass", "secondary_mass"]

    querylist_any_mass = [
        {"name": "No PPISN", "query": "(undergone_ppisn==0)"},
        {"name": "PPISN", "query": "(undergone_ppisn==1)"},
    ]

    #
    sim_name = dataset_dict["sim_name"]

    # Set rate types
    rate_types = ["merger_rate"]
    if config_dict_convolution["include_formation_rates"]:
        rate_types.append("formation_rate")

    #
    plot_dir = os.path.abspath(main_plot_dir)  # Just use the passed in plot dir.

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    #
    print("Generating plots for {}".format(sim_name))

    #
    os.makedirs(os.path.join(plot_dir, "pdf"), exist_ok=True)
    os.makedirs(os.path.join(plot_dir, dco_type, "pdf"), exist_ok=True)

    # Generate frontpage pdf
    print("\tGenerating frontpage")
    frontpage_name = os.path.join(plot_dir, "pdf", "frontpage.pdf")
    generate_frontpage_pdf_sfr_convolution(
        dataset_dict, cosmology_configuration, config_dict_convolution, frontpage_name
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger, frontpage_name, pdf_page_number, bookmark_text="Frontpage"
    )

    ###########################################################################
    # Starformation rate, metallicity distribution and combination plot and all the plots
    starformation_rates_and_metallicity_distribution_chapter_filename = os.path.join(
        plot_dir,
        dco_type,
        "pdf",
        "starformation_rates_and_metallicity_distribution_chapter.pdf",
    )
    generate_chapterpage_pdf(
        "Starformation rates and metallicity distribution",
        starformation_rates_and_metallicity_distribution_chapter_filename,
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        starformation_rates_and_metallicity_distribution_chapter_filename,
        pdf_page_number,
        bookmark_text="Starformation rates and metallicity distribution chapter",
    )

    #
    if include_starformation_rate_plots:
        print("\tPlotting pdf for starformation rates")
        combined_sfr_plots_output_name = os.path.join(
            plot_dir, dco_type, "combined_sfr_plots.pdf"
        )
        combined_sfr_plots_output_dir = os.path.join(plot_dir, dco_type, "sfr_plots/")

        ############
        # Recalculate some values
        sampled_metallicities = 10 ** np.array(
            metallicity_dict["sorted_log10metallicity_values"]
        )

        log10_metallicities = np.log10(sampled_metallicities)
        log_metallicities = np.log(sampled_metallicities)

        max_log_z = log_metallicities.max()
        min_log_z = log_metallicities.min()

        # Get sfr value
        sfr_values = starformation_rate(
            np.array(config_dict_convolution["time_centers"]),
            cosmology_configuration=cosmology_configuration,
            verbosity=1,
        )

        if cosmology_configuration["metallicity_distribution_function"] in [
            "vanSon21",
            "neijssel19",
            "COMPAS",
        ]:
            # Calculate the metallicity distribution
            dPdlogZ, metallicities, p_draw_metallicity = find_metallicity_distribution(
                np.array(config_dict_convolution["time_centers"]),
                min_log_z,
                max_log_z,
                **cosmology_configuration["metallicity_distribution_args"],
            )
        else:
            raise ValueError("unknown distribution")

        #####
        # SFR and metallicity distribution plots: sequence of plots to show the starformation rate and all the mutations on this
        general_plot_function_metallicity_sfr_distribution(
            output_pdf_dir=combined_sfr_plots_output_dir,
            combined_pdf_filename=combined_sfr_plots_output_name,
            redshifts=config_dict_convolution["time_centers"],
            sfr_values=sfr_values,
            dPdlogZ=dPdlogZ,
            metallicities=metallicities,
            sampled_metallicities=sampled_metallicities,
            p_draw_metallicity=p_draw_metallicity,
            distribution=cosmology_configuration["metallicity_distribution_function"],
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            combined_sfr_plots_output_name,
            pdf_page_number,
            bookmark_text="SFR plots",
            add_source_bookmarks=True,
        )

    ######
    # Plot the birth rate: 2-d plot with redshift on x-axis and metallicity on y-axis, showing the birth rate of systems at that z,Z location
    if include_plot_birthrate:
        print("\tPlotting plot_birthrate detailed")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            plot_birthrate_filename = os.path.join(
                plot_dir, dco_type, format_type, "plot_birthrate.{}".format(format_type)
            )
            plot_birthrate(
                convolution_dataset_hdf5_filename,
                plot_settings={
                    "output_name": plot_birthrate_filename,
                    "block": False,
                    "show_plot": False,
                    "simulation_name": sim_name,
                    "antialiased": True,
                    "rasterized": True,
                    "runname": "plot_birthrate detailed",
                },
            )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_birthrate_filename,
            pdf_page_number,
            bookmark_text="plot_birthrate",
        )

    ###########################################################################
    # Intrinsic merger and formation rate densities at all times and at z=0
    merger_rate_mass_quantity_chapter_filename = os.path.join(
        plot_dir, dco_type, "pdf", "merger_rate_mass_quantity_chapter.pdf"
    )
    generate_chapterpage_pdf(
        "Merger rates per mass quantity plots",
        merger_rate_mass_quantity_chapter_filename,
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        merger_rate_mass_quantity_chapter_filename,
        pdf_page_number,
        bookmark_text="Merger rates per mass quantity chapter",
    )

    for rate_type in rate_types:
        for mass_type in mass_types:
            ########
            # Mass vs redshift: This plots a 2-d plot with redshift on the x axis,
            # and the mass type on the y axis, and the color indicates the rate. There is no bootstrapping.
            if include_merger_and_formation_rate_densities_over_redshift_plots:
                print("\tPlotting {} over all time for {}".format(rate_type, mass_type))
                for probability_floor in probability_floors:
                    print("\t\tUsing probability floor {}".format(probability_floor))
                    for format_type in format_types:
                        print("\t\tSaving as {}".format(format_type))
                        plot_convolved_rate_density_per_mass_quantity_filename = (
                            os.path.join(
                                plot_dir,
                                dco_type,
                                format_type,
                                "plot_convolved_{}_{}_{}floor.{}".format(
                                    rate_type, mass_type, probability_floor, format_type
                                ),
                            )
                        )
                        plot_convolved_rate_density_per_mass_quantity(
                            convolution_dataset_hdf5_filename,
                            dco_type=dco_type,
                            rate_type=rate_type,
                            mass_type=mass_type,
                            mass_bins=mass_bins[mass_type],
                            divide_by_binsize=divide_by_binsize,
                            add_ratio=add_ratio,
                            querylist=querylist,
                            contourlevels=contourlevels,
                            linestyles_contourlevels=linestyles_contourlevels,
                            num_procs=num_procs,
                            plot_settings={
                                "show_plot": False,
                                "probability_floor": probability_floor,
                                "output_name": plot_convolved_rate_density_per_mass_quantity_filename,
                                "simulation_name": sim_name,
                                "antialiased": True,
                                "rasterized": True,
                                "runname": "plot_convolved_rate_density_per_mass_quantity: mass quantity: {} rate type: {} dco_type: {}".format(
                                    mass_type, rate_type, dco_type
                                ),
                            },
                        )
                    merger, pdf_page_number = add_pdf_and_bookmark(
                        merger,
                        plot_convolved_rate_density_per_mass_quantity_filename,
                        pdf_page_number,
                        bookmark_text="Intrinstic {} density for {}: floor={}".format(
                            rate_type, mass_type, probability_floor
                        ),
                    )

            ########
            # Mass at redshift 0: This plots a 1-d plot with mass on the x axis,
            # and the merger rate on the y axis. Here we do bootstrapping and KDE plotting
            if include_merger_and_formation_rate_densities_at_redshift_zero_plots:
                print("\tPlotting merger rate density at z=0 for {}".format(mass_type))
                for format_type in format_types:
                    print("\t\tSaving as {}".format(format_type))
                    plot_rate_density_at_zero_redshift_filename = os.path.join(
                        plot_dir,
                        dco_type,
                        format_type,
                        "{}_{}_lookback_zero.{}".format(
                            rate_type, mass_type, format_type
                        ),
                    )
                    plot_rate_density_at_zero_redshift(
                        convolution_dataset_hdf5_filename,
                        dco_type=dco_type,
                        mass_quantity=mass_type,
                        mass_bins=mass_bins[mass_type],
                        divide_by_binsize=divide_by_binsize,
                        rate_type=rate_type,
                        add_cdf=add_cdf,
                        add_ratio=add_ratio,
                        querylist=querylist,
                        num_procs=num_procs_bootstrap,
                        bootstraps=bootstraps,
                        plot_xlim=[
                            mass_bins[mass_type].min(),
                            mass_bins[mass_type].max(),
                        ],
                        plot_settings={
                            "show_plot": False,
                            "output_name": plot_rate_density_at_zero_redshift_filename,
                            "simulation_name": sim_name,
                            "antialiased": True,
                            "rasterized": True,
                            "runname": "plot_rate_density_at_zero_redshift: mass quantity: {} rate type: {} dco_type: {}".format(
                                mass_type, rate_type, dco_type
                            ),
                        },
                    )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    plot_rate_density_at_zero_redshift_filename,
                    pdf_page_number,
                    bookmark_text="{} at 0 lookback".format(mass_type),
                )

            ########
            # Birth redshift per mass: this plots a 2-d plot with, for system merging at z=0,
            # birth redshift on the x axis and the mass quantity on the y-axis, and the color indicates the rate. There is no bootstrapping.
            if include_plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity:
                print(
                    "Plotting plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity for {}, {}".format(
                        rate_type, mass_type
                    )
                )
                for format_type in format_types:
                    print("\t\tSaving as {}".format(format_type))
                    contourlevels = [1e-1, 1e0, 1e1, 1e2]
                    linestyles_contourlevels = ["solid", "dashed", "-.", ":"]
                    plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity_filename = os.path.join(
                        plot_dir,
                        dco_type,
                        format_type,
                        "{}_{}_plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity.{}".format(
                            rate_type, mass_type, format_type
                        ),
                    )
                    plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity(
                        convolution_dataset_hdf5_filename,
                        dco_type=dco_type,
                        mass_quantity=mass_type,
                        mass_bins=mass_bins[mass_type],
                        divide_by_binsize=False,
                        rate_type=rate_type,
                        add_ratio=add_ratio,
                        querylist=querylist,
                        plot_settings={
                            "show_plot": False,
                            "output_name": plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity_filename,
                            "simulation_name": sim_name,
                            "antialiased": True,
                            "rasterized": True,
                            "runname": "plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity: mass quantity: {} rate type: {} dco_type: {}".format(
                                mass_type, rate_type, dco_type
                            ),
                        },
                    )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity_filename,
                    pdf_page_number,
                    bookmark_text="plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity {} {}".format(
                        rate_type, mass_type
                    ),
                )

            ########
            # Plot rate at z=0 per metallicity: 2-d plot with mass quantity on x-axis, metallicity on y-axis, for systems merging at z=0.
            # There is no bootstrapping
            if include_plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity:
                print(
                    "\tplot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity for {}, {}".format(
                        rate_type, mass_type
                    )
                )
                for format_type in format_types:
                    print("\t\tSaving as {}".format(format_type))
                    contourlevels = [1e-1, 1e0, 1e1, 1e2]
                    linestyles_contourlevels = ["solid", "dashed", "-.", ":"]
                    plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity_filename = os.path.join(
                        plot_dir,
                        dco_type,
                        format_type,
                        "{}_{}_plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity.{}".format(
                            rate_type, mass_type, format_type
                        ),
                    )
                    plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity(
                        convolution_dataset_hdf5_filename,
                        dco_type=dco_type,
                        rate_type=rate_type,
                        querylist=querylist,
                        add_ratio=add_ratio,
                        mass_type=mass_type,
                        mass_bins=mass_bins[mass_type],
                        plot_settings={
                            "output_name": plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity_filename,
                            "show_plot": False,
                            "simulation_name": sim_name,
                            "antialiased": True,
                            "rasterized": True,
                            "runname": "plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity: mass quantity: {} rate type: {} dco_type: {}".format(
                                mass_type, rate_type, dco_type
                            ),
                        },
                        contourlevels=contourlevels,
                        linestyles_contourlevels=linestyles_contourlevels,
                    )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity_filename,
                    pdf_page_number,
                    bookmark_text="plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity_filename {} {}".format(
                        rate_type, mass_type
                    ),
                )

        #####
        # Rate density evolution per metallicity: 2-d plot with redshift on x-axis and metallicity on y-axis.
        # There is no bootstrapping
        if include_plot_convolved_rate_density_per_metallicity:
            print(
                "\tplot_convolved_rate_density_per_metallicity for {}".format(rate_type)
            )
            for format_type in format_types:
                print("\t\tSaving as {}".format(format_type))
                contourlevels = [1e-1, 1e0, 1e1, 1e2]
                linestyles_contourlevels = ["solid", "dashed", "-.", ":"]
                plot_convolved_rate_density_per_metallicity_filename = os.path.join(
                    plot_dir,
                    dco_type,
                    format_type,
                    "{}_plot_convolved_rate_density_per_metallicity.{}".format(
                        rate_type, format_type
                    ),
                )
                plot_convolved_rate_density_per_metallicity(
                    convolution_dataset_hdf5_filename,
                    dco_type=dco_type,
                    rate_type=rate_type,
                    querylist=querylist,
                    add_ratio=add_ratio,
                    num_procs=num_procs,
                    plot_settings={
                        "output_name": plot_convolved_rate_density_per_metallicity_filename,
                        "show_plot": False,
                        "simulation_name": sim_name,
                        "antialiased": True,
                        "rasterized": True,
                        "runname": "plot_convolved_rate_density_per_metallicity: mass quantity: {} rate type: {} dco_type: {}".format(
                            mass_type, rate_type, dco_type
                        ),
                    },
                    contourlevels=contourlevels,
                    linestyles_contourlevels=linestyles_contourlevels,
                )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    plot_convolved_rate_density_per_metallicity_filename,
                    pdf_page_number,
                    bookmark_text="plot_convolved_rate_density_per_metallicity {}".format(
                        rate_type
                    ),
                )

        #####
        # Birth location for mergers/formers at 0 redshift per redshift per metallicity: 2-d plot for systems merging at redshift 0,
        # with birth redshift on x-axis and metallicity on y-axis
        if include_plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_metallicity:
            print(
                "\tplot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_metallicity for {}".format(
                    rate_type
                )
            )
            for format_type in format_types:
                print("\t\tSaving as {}".format(format_type))
                contourlevels = [1e-1, 1e0, 1e1, 1e2]
                linestyles_contourlevels = ["solid", "dashed", "-.", ":"]

                plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_metallicity_filename = os.path.join(
                    plot_dir,
                    dco_type,
                    format_type,
                    "{}_plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_metallicity.{}".format(
                        rate_type, format_type
                    ),
                )
                plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_metallicity(
                    convolution_dataset_hdf5_filename,
                    dco_type=dco_type,
                    rate_type=rate_type,
                    add_ratio=add_ratio,
                    querylist=querylist,
                    plot_settings={
                        "show_plot": False,
                        "output_name": plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_metallicity_filename,
                        "simulation_name": sim_name,
                        "antialiased": True,
                        "rasterized": True,
                        "runname": "plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_metallicity: rate type: {} dco_type: {}".format(
                            rate_type, dco_type
                        ),
                    },
                )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_metallicity_filename,
                    pdf_page_number,
                    bookmark_text="plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_metallicity {}".format(
                        rate_type
                    ),
                )

        #####
        # any mass at specific redshift: same as the 'mass at redshift 0' but then with any mass, including extra plotting with the querysets
        if include_plot_any_mass_at_zero_redshift:
            # here we first do the plot without the base query
            for format_type in format_types:
                plot_any_mass_at_zero_redshift_filename = os.path.join(
                    plot_dir,
                    dco_type,
                    format_type,
                    "{}_plot_any_mass_at_zero_redshift.{}".format(
                        rate_type, format_type
                    ),
                )
                print("\tplotting {}".format(plot_any_mass_at_zero_redshift_filename))

                plot_any_mass_at_zero_redshift(
                    convolution_dataset_hdf5_filename,
                    dco_type=dco_type,
                    divide_by_binsize=divide_by_binsize,
                    rate_type=rate_type,
                    any_mass_bins=mass_bins["any_mass"],
                    num_procs=num_procs_bootstrap,
                    plot_xlim=[
                        mass_bins["any_mass"].min(),
                        mass_bins["any_mass"].max(),
                    ],
                    # add_ratio=add_ratio,
                    querylist=querylist_any_mass,
                    plot_settings={
                        "show_plot": False,
                        "output_name": plot_any_mass_at_zero_redshift_filename,
                        "simulation_name": sim_name,
                        "antialiased": True,
                        "rasterized": True,
                        "runname": "plot_any_mass_at_zero_redshift: mass quantity: any mass rate type: {} dco_type: {}".format(
                            rate_type, dco_type
                        ),
                    },
                )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    plot_any_mass_at_zero_redshift_filename,
                    pdf_page_number,
                    bookmark_text="plot_any_mass_at_zero_redshift {} without basequery".format(
                        rate_type
                    ),
                )

                # And then for any normal query we have, we act as if that is a new base query
                if querylist:
                    for querydict in querylist:
                        # here we first do the plot without the base query
                        plot_any_mass_at_zero_redshift_filename = os.path.join(
                            plot_dir,
                            dco_type,
                            format_type,
                            "{}_plot_any_mass_at_zero_redshift_{}.{}".format(
                                rate_type,
                                querydict["name"].replace(" ", "_"),
                                format_type,
                            ),
                        )
                        print(
                            "\tplotting {}".format(
                                plot_any_mass_at_zero_redshift_filename
                            )
                        )
                        plot_any_mass_at_zero_redshift(
                            convolution_dataset_hdf5_filename,
                            divide_by_binsize=divide_by_binsize,
                            rate_type=rate_type,
                            dco_type=dco_type,
                            any_mass_bins=mass_bins["any_mass"],
                            num_procs=num_procs_bootstrap,
                            base_querydict=querydict,
                            # add_ratio=add_ratio,
                            querylist=querylist_any_mass,
                            plot_settings={
                                "show_plot": False,
                                "output_name": plot_any_mass_at_zero_redshift_filename,
                                "simulation_name": sim_name,
                                "antialiased": True,
                                "rasterized": True,
                                "runname": "plot_any_mass_at_zero_redshift {}".format(
                                    querydict["name"]
                                ),
                            },
                        )
                        merger, pdf_page_number = add_pdf_and_bookmark(
                            merger,
                            plot_any_mass_at_zero_redshift_filename,
                            pdf_page_number,
                            bookmark_text="plot_any_mass_at_zero_redshift {} for query {}".format(
                                rate_type, querydict["name"]
                            ),
                        )

        # TODO: Any mass over redshift

        #####
        # primary mass vs secondary mass at zero redshift: 2-d plot for systems merging at redshift 0,
        # with on the x-axis the primary mass and on y-axis the secondary mass
        if include_plot_primary_vs_secondary_at_specific_redshift:
            print(
                "\tplot_primary_vs_secondary_at_specific_redshift for {}".format(
                    rate_type
                )
            )
            for format_type in format_types:
                print("\t\tSaving as {}".format(format_type))
                contourlevels = [1e-1, 1e0, 1e1, 1e2]
                linestyles_contourlevels = ["solid", "dashed", "-.", ":"]

                plot_primary_vs_secondary_at_specific_redshift_filename = os.path.join(
                    plot_dir,
                    dco_type,
                    format_type,
                    "{}_plot_primary_vs_secondary_at_specific_redshift.{}".format(
                        rate_type, format_type
                    ),
                )
                plot_primary_vs_secondary_at_specific_redshift(
                    convolution_dataset_hdf5_filename,
                    dco_type=dco_type,
                    rate_type=rate_type,
                    add_ratio=add_ratio,
                    querylist=querylist,
                    primary_mass_bins=mass_bins["primary_mass"],
                    secondary_mass_bins=mass_bins["secondary_mass"],
                    plot_settings={
                        "show_plot": False,
                        "output_name": plot_primary_vs_secondary_at_specific_redshift_filename,
                        "simulation_name": sim_name,
                        "antialiased": True,
                        "rasterized": True,
                        "runname": "plot_primary_vs_secondary_at_specific_redshift: mass quantity: primary vs secondary rate type: {} dco_type: {}".format(
                            rate_type, dco_type
                        ),
                    },
                )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    plot_primary_vs_secondary_at_specific_redshift_filename,
                    pdf_page_number,
                    bookmark_text="plot_primary_vs_secondary_at_specific_redshift {}".format(
                        rate_type
                    ),
                )

    ####
    # Add massratio plot
    for rate_type in rate_types:

        #####
        # rate density per mass ratio per redshift: 2-d plot with redshift on x-axis and mass ratio on y-axis
        if include_convolved_rate_density_per_mass_ratio:
            print("\tPlotting plot_convolved_rate_density_per_mass_ratio")
            for format_type in format_types:
                print("\t\tSaving as {}".format(format_type))
                for probability_floor in probability_floors:
                    print("\t\tUsing probability floor {}".format(probability_floor))
                    plot_convolved_rate_density_per_mass_ratio_filename = os.path.join(
                        plot_dir,
                        dco_type,
                        format_type,
                        "plot_convolved_{}_{}_{}floor.{}".format(
                            rate_type, "mass_ratio", probability_floor, format_type
                        ),
                    )
                    plot_convolved_rate_density_per_mass_ratio(
                        convolution_dataset_hdf5_filename,
                        dco_type=dco_type,
                        rate_type=rate_type,
                        divide_by_binsize=divide_by_binsize,
                        add_ratio=add_ratio,
                        querylist=querylist,
                        contourlevels=contourlevels,
                        linestyles_contourlevels=linestyles_contourlevels,
                        num_procs=num_procs,
                        plot_settings={
                            "show_plot": False,
                            "probability_floor": probability_floor,
                            "output_name": plot_convolved_rate_density_per_mass_ratio_filename,
                            "simulation_name": sim_name,
                            "antialiased": True,
                            "rasterized": True,
                            "runname": "plot_convolved_rate_density_per_mass_ratio: mass quantity: {} rate type: {} dco_type floor: {}".format(
                                "mass_ratio", rate_type, dco_type
                            ),
                        },
                    )
                    merger, pdf_page_number = add_pdf_and_bookmark(
                        merger,
                        plot_convolved_rate_density_per_mass_ratio_filename,
                        pdf_page_number,
                        bookmark_text="Intrinstic {} density for {}: floor={}".format(
                            rate_type, "mass_ratio", probability_floor
                        ),
                    )

        #####
        # mass ratio at zero redshift: 1-d plot for systems merging at z=0
        # with mass ratio on the x-axis and rate density on y-axis
        # TODO: add bootstrapping here
        if include_massratio_density_at_zero_redshift:
            print("\tPlotting plot_massratio_density_at_zero_redshift")
            for format_type in format_types:
                print("\t\tSaving as {}".format(format_type))
                plot_massratio_density_at_zero_redshift_filename = os.path.join(
                    plot_dir,
                    dco_type,
                    format_type,
                    "{}_lookback_zero.{}".format("mass_ratio", format_type),
                )
                plot_massratio_density_at_zero_redshift(
                    convolution_dataset_hdf5_filename,
                    dco_type=dco_type,
                    divide_by_binsize=divide_by_binsize,
                    rate_type=rate_type,
                    add_cdf=add_cdf,
                    add_ratio=add_ratio,
                    querylist=querylist,
                    num_procs=num_procs_bootstrap,
                    plot_xlim=[0, 1.1],
                    plot_settings={
                        "show_plot": False,
                        "output_name": plot_massratio_density_at_zero_redshift_filename,
                        "simulation_name": sim_name,
                        "antialiased": True,
                        "rasterized": True,
                        "runname": "plot_massratio_density_at_zero_redshift: mass quantity: {} rate type: {} dco_type: {}".format(
                            "mass_ratio", rate_type, dco_type
                        ),
                    },
                )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    plot_massratio_density_at_zero_redshift_filename,
                    pdf_page_number,
                    bookmark_text="{} at 0 lookback".format("mass_ratio"),
                )

    ####################################
    # Total intrinsic rate at all lookbacks
    total_intrinsic_rate_over_redshift_chapter_filename = os.path.join(
        plot_dir, dco_type, "pdf", "total_intrinsic_rate_over_redshift_chapter.pdf"
    )
    generate_chapterpage_pdf(
        "Total intrinsic rates plots",
        total_intrinsic_rate_over_redshift_chapter_filename,
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        total_intrinsic_rate_over_redshift_chapter_filename,
        pdf_page_number,
        bookmark_text="Total intrinsic rates over redshift chapter",
    )

    if include_total_intrinsic_merger_rates_over_redshift_plots:
        for rate_type in rate_types:
            print(
                "\tPlotting plot_total_intrinsic_merger_rate_density_at_all_lookbacks with {}".format(
                    rate_type
                )
            )
            for format_type in format_types:
                print("\t\tSaving as {}".format(format_type))
                plot_total_intrinsic_rate_density_at_all_lookbacks_filename = (
                    os.path.join(
                        plot_dir,
                        dco_type,
                        format_type,
                        "intrinsic_{}_density_at_all_lookbacks.{}".format(
                            rate_type, format_type
                        ),
                    )
                )
                plot_total_intrinsic_rate_density_at_all_lookbacks(
                    convolution_dataset_hdf5_filename,
                    dco_type=dco_type,
                    querylist=querylist,
                    add_cdf=add_cdf,
                    add_ratio=add_ratio,
                    rate_type=rate_type,
                    plot_settings={
                        "output_name": plot_total_intrinsic_rate_density_at_all_lookbacks_filename,
                        "block": False,
                        "show_plot": False,
                        "simulation_name": sim_name,
                        "antialiased": True,
                        "rasterized": True,
                        "runname": "plot_total_intrinsic_rate_density_at_all_lookbacks: rate_type: {} dco_type: {}".format(
                            rate_type, dco_type
                        ),
                    },
                )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                plot_total_intrinsic_rate_density_at_all_lookbacks_filename,
                pdf_page_number,
                bookmark_text="Intrinsic {} at all lookbacks".format(rate_type),
            )

    ####################################
    # Details about the redshift interpolator
    interpolation_analysis_chapter_filename = os.path.join(
        plot_dir, dco_type, "pdf", "interpolation_analysis_chapter.pdf"
    )
    generate_chapterpage_pdf(
        "Redshift interpolation error chapter", interpolation_analysis_chapter_filename
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        interpolation_analysis_chapter_filename,
        pdf_page_number,
        bookmark_text="Redshift interpolation error chapter",
    )

    #####
    # redshift analysis: panel showing the accuracy of the redshift interpolation function.
    if include_interpolation_error_plot:
        print("\tPlotting redshift interpolation error analysis")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            plot_interpolation_datasets_filename = os.path.join(
                plot_dir,
                dco_type,
                format_type,
                "redshift_interpolation_error_analysis.{}".format(format_type),
            )
            plot_interpolation_datasets(
                config_dict_convolution,
                size_test_samples=200,
                plot_log=True,
                test_log=True,
                plot_settings={
                    "show_plot": False,
                    "output_name": plot_interpolation_datasets_filename,
                    "simulation_name": sim_name,
                    "antialiased": True,
                    "rasterized": True,
                    "runname": "plot_interpolation_datasets",
                },
            )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_interpolation_datasets_filename,
            pdf_page_number,
            bookmark_text="redshift interpolation error analysis",
        )

    # wrap up the pdf
    merger.write(output_pdf_filename)
    merger.close()

    print(
        "Finished generating plots for dataset {}. Wrote results to {}".format(
            dataset_dict["sim_name"], os.path.abspath(output_pdf_filename)
        )
    )
