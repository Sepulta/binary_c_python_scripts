"""
Function to plot the birthrate
"""

import numpy as np

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.pdf_functions import (
    add_pdf_and_bookmark,
    generate_chapterpage_pdf,
)
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

import json
import h5py
import astropy.units as u


def plot_birthrate(convolution_dataset_hdf5_filename, plot_settings=None):
    """
    Function to plot the birthrate for range of metallicity and redshift
    """

    #
    hdf5_file = h5py.File(convolution_dataset_hdf5_filename, "r")

    settings = hdf5_file["settings"]
    convolution_configuration = json.loads(settings["convolution_configuration"][()])
    metallicity_data = json.loads(settings["metallicity_settings"][()])

    # Read out the data
    birth_rate_dataset = json.loads(hdf5_file["data"]["birth_rate_data"][()])

    birth_rate_data = birth_rate_dataset["birthrate_data"]["value"] * u.Unit(
        birth_rate_dataset["birthrate_data"]["unit"]
    )
    birth_rate_data = birth_rate_data[:, 1:-1]

    max_value = birth_rate_data.value.max()
    custom_min_value = 10 ** (np.log10(max_value) - 4)

    # Set up axes
    fig, axes = plt.subplots(ncols=1, nrows=1, figsize=(40, 40))

    metallicity_centers = np.array(
        sorted(
            sorted(
                [
                    float(el[1:].split("_")[0])
                    for el in birth_rate_dataset["metallicity_centers"]
                ]
            )
        )
    )
    X_values = np.array(birth_rate_dataset["time_centers"])
    Y_values = np.log10(metallicity_centers)

    # Plot the results
    pcm = axes.pcolormesh(
        X_values,
        Y_values,
        birth_rate_data.value,
        norm=colors.LogNorm(vmin=custom_min_value, vmax=max_value),
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    cb = fig.colorbar(pcm, ax=axes, extend="min")
    cb.ax.set_ylabel(
        "Birth rate density"
        + r" [{}]".format(birth_rate_data.unit.to_string("latex_inline"))
    )

    # Set contourlevens and lines on the colorbar
    contourlevels = [1e1, 1e2, 0.5 * 1e3]
    linestyles_contourlevels = ["solid", "dashed", "-."]

    if (
        (not contourlevels is None)
        and (not linestyles_contourlevels is None)
        and (len(contourlevels) == len(linestyles_contourlevels))
    ):
        _ = axes.contour(
            X_values,
            Y_values,
            birth_rate_data.value,
            levels=contourlevels,
            colors="k",
            linestyles=linestyles_contourlevels,
        )

        # Set lines on the colorbar
        for contour_i, _ in enumerate(contourlevels):
            cb.ax.plot(
                [cb.ax.get_xlim()[0], cb.ax.get_xlim()[1]],
                [contourlevels[contour_i]] * 2,
                "black",
                linestyle=linestyles_contourlevels[contour_i],
            )

    axes.set_ylabel("log10(Metallicity)")
    axes.set_xlabel("Redshift")

    axes.set_title("Birth rate density")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
