"""
Script that runs a full population of binary stars and outputs ensemble information that registers the following:

We run two different distribution types:
- normal distribution
- moe & distefano distribution
"""

import os

from RLOF.run_populations.ensemble_population_and_rlof_episodes.run_all import run_all

#############################################
# Settings for the script
run_populations = True
generate_plots = False
backup_if_data_exists = True
inflate_ensembles = False
root_result_dir = os.path.join(os.environ["BINARYC_DATA_ROOT"], "RLOF")
disk_photometry_num_rings = 10
disk_photometry_disk_discretization_method = 0
disk_photometry_boundary_layer_size_fraction = 0.01

# ###########
# # high res settings
# num_cores = 48
# metallicity_values = [0.02, 0.004, 0.0008]
# resolution = {'M_1': 100, 'q': 100, 'per': 100, "e": 0}
# simname_base = "HIGH_RES_HRD"

# ###########
# # mid res settings
# num_cores = 20
# metallicity_values = [0.02, 0.004, 0.0008]
# resolution = {'M_1': 40, 'q': 40, 'per': 80, "e": 0}
# simname_base = "MID_RES_HRD"

###########
# low res settings
num_cores = 8
metallicity_values = [0.002]
resolution = {"M_1": 15, "q": 15, "per": 20, "e": 0}
simname_base = "LOW_RES_HRD"

###########
# test res settings
num_cores = 4
metallicity_values = [0.001]
resolution = {"M_1": 5, "q": 5, "per": 5, "e": 0}
simname_base = "TEST_RES_HRD"

##############
# Settings for the population object
local_population_settings = {
    "ensemble":                                     1,
    "ensemble_defer":                               1,
    "ensemble_filters_off":                         1,
    "ensemble_filter_HRD":                          1,
    "combine_ensemble_with_thread_joining":         True,
    "david_rlof_episode_logging":                   0,
    "david_rlof_system_ensemble_logging":           0,
    "david_ensemble_ballistic_exploration_logging": 0,
    "david_open_cluster_system_ensemble_logging":   0,
    "david_disk_photometry_ensemble_logging":       1,
    "disk_photometry_num_rings":                    disk_photometry_num_rings,
    "disk_photometry_disk_discretization_method":   disk_photometry_disk_discretization_method,
    "CHE_enabled":                                  0,
    "CHE_determination_prescription":               "CHE_DETERMINATION_PRESCRIPTION_MANDEL2016_1",
    "CHE_enable_ensemble_logging":                  0,
    "num_cores":                                    num_cores,
    "multiplicity":                                 2,
}

##############
# Arguments for the run_all function
run_all_extra_arguments = {
    "run_populations": run_populations,
    "backup_if_data_exists": backup_if_data_exists,
    "generate_plots": generate_plots,
    "resolution": resolution,
    "root_result_dir": root_result_dir,
    "simname_base": simname_base,
    "metallicity_values": metallicity_values,
    "local_population_settings": local_population_settings,
    "inflate_ensembles": inflate_ensembles,
}

# fiducial_settings
variation_dict_fiducial = {
    "distributions": "normal",  # Choose 'normal' or 'ms'
}

#####
# Fiducial run
variation_dict = variation_dict_fiducial
run_all(variation_dict=variation_dict, **run_all_extra_arguments)
