"""
Functions related to the arrays we store the
"""

import time
import copy
import numpy as np
import astropy.units as u

from grav_waves.settings import cosmo

from COMPAS.utils.PythonScripts.CosmicIntegration.selection_effects import (
    detection_probability,
)

from grav_waves.convolution.functions.convolution_general_functions import vb
from grav_waves.gw_analysis.functions.cosmology_functions import (
    redshift_to_lookback_time,
)

####
# Array functions
def calculate_birth_redshift_array(
    time_values_in_years_key,
    convolution_configuration,
    redshift_value,
    combined_data_array,
):
    """
    Function to calculate the birth redshift array for the merging/formation events based on
    the current redshift and the merger/formation time of the system.

    We do this by calculating the lookback time (which is age_of_universe(z=0) - age_of_universe(z))
    adding the merger time of the system to the lookback time, getting the birth time
    and then calculating the birth redshift from the birth time
    """

    #####################################################################################
    start_array_method = time.time()

    # With the current redshift, we calculate the lookback time, subtract the merger time and formation time and turn back into
    current_lookback_value = redshift_to_lookback_time(redshift_value)

    # Calculate lookback time of first starformation (which is the same as the upper redshift of the interpolator)
    lookback_time_of_first_starformation = redshift_to_lookback_time(
        convolution_configuration["max_interpolation_redshift"]
    )

    # Calculate the lookback time of the event, given the merger/formation time and the current time
    birth_lookback_time_values_in_yr = (
        current_lookback_value.to(u.yr).value
        + combined_data_array[time_values_in_years_key]
    )
    birth_lookback_time_values_in_gyr = birth_lookback_time_values_in_yr * (
        u.yr.to(u.Gyr)
    )

    # Get the indices where the event falls inside the correct starformation time range
    indices_within_first_starformation = (
        birth_lookback_time_values_in_gyr < lookback_time_of_first_starformation.value
    )
    indices_outside_first_starformation = (
        birth_lookback_time_values_in_gyr >= lookback_time_of_first_starformation.value
    )

    # Create redshift values array of the event
    birth_redshift_values = np.ones(combined_data_array[time_values_in_years_key].shape)
    birth_redshift_values[
        indices_within_first_starformation
    ] = convolution_configuration["interpolators"][
        "lookback_time_to_redshift_interpolator"
    ](
        birth_lookback_time_values_in_gyr[indices_within_first_starformation]
    )
    birth_redshift_values[indices_outside_first_starformation] = -1

    end_array_method = time.time()
    convolution_configuration["logger"].debug(
        "calculate_birth_redshift_array: took {}s".format(
            end_array_method - start_array_method
        )
    )

    #
    return birth_redshift_values
