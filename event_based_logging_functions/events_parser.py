"""
General parsing function for the event output. Rather simple but centralized
"""
import os

from binarycpython.utils.functions import output_lines
from event_based_logging_functions.event_headers import event_headers_dict


def events_parser(self, separator, data_dir, output):
    """
    Function to handle parsing the events output
    """

    # Set event outfile name
    event_outfilename = os.path.join(data_dir, "events-{}.dat".format(self.process_ID))

    # Touch the event output file
    if not os.path.exists(event_outfilename):
        with open(event_outfilename, "w") as _:
            pass

    # Loop over the output
    for line in output_lines(output):
        # Handle the events line
        if line.startswith("EVENT"):
            events_values = line.split()[1:]

            # Check for correct length
            event_type = events_values[1]
            if not len(events_values) == len(event_headers_dict[event_type]):
                print(
                    "Length of readout values ({}) is not equal to length of parameters ({}) for event type: {}".format(
                        len(events_values),
                        len(event_headers_dict[event_type]),
                        event_type,
                    )
                )
                raise ValueError

            with open(event_outfilename, "a") as events_f:
                events_f.write(separator.join(events_values[:]) + "\n")
