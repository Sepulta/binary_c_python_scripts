"""
Script containing functions to plot the grid of results of the integration
"""

import os

import numpy as np

import matplotlib.pyplot as plt

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

from ballistic_integration_code.functions.plot_functions import (
    ulrich_kolb,
    fkr,
)
from ballistic_integration_code.scripts.L1_stream_sepinsky.functions import (
    readout_grid_result,
)

custom_mpl_settings.load_mpl_rc()


def plot_grid(result_dir, plot_settings={}):
    """
    function to plot the circularisation radius for systems with ballistic non-synch results
    """

    # Read out result directory and generate dataframe containing the summarised results
    result_df = readout_grid_result(result_dir)

    #
    unique_synchronicity_factors = sorted(result_df["synchronicity_factor"].unique())

    #
    mass_accretor_array = 10 ** np.linspace(-2, 2, 100)
    mass_donor_array = np.ones(mass_accretor_array.shape)

    # get results # TODO: check if these are correct
    rcirc_ulrich = ulrich_kolb(mass_donor_array, mass_accretor_array)
    rcirc_fkr = fkr(mass_donor_array, mass_accretor_array)

    # Calculte mass ratio
    q_acc_don = mass_accretor_array / mass_donor_array
    result_df["rcirc"] = result_df["min_distance_to_accretor"] * 1.7

    #
    fig, axes = plt.subplots(figsize=(16, 16))

    # Plot the results for different prescriptions in literature
    axes.plot(q_acc_don, rcirc_ulrich, "--", label="Ulrich & Kolb")
    axes.plot(q_acc_don, rcirc_fkr, "--", label="Frank King & Raine")

    # Plot results for each synchronicity factor
    cmap = plt.cm.get_cmap("viridis", len(unique_synchronicity_factors))
    cmaplist = [cmap(i) for i in range(cmap.N)]
    for i, synchronicity_factor in enumerate(unique_synchronicity_factors):
        filtered_df = result_df[result_df.synchronicity_factor == synchronicity_factor]
        axes.plot(
            filtered_df["massratio_accretor_donor"],
            filtered_df["rcirc"],
            color=cmaplist[i],
            label="f = {}".format(synchronicity_factor),
        )

    ######
    # Make up
    axes.legend()
    axes.set_xscale("log")
    axes.set_yscale("log")
    axes.set_ylabel(r"Circularisation radius stream $R_{\mathrm{circ}}/a$ [a]")
    axes.set_xlabel(
        r"$q_{\mathrm{accretor}} = M_{\mathrm{accretor}}/M_{\mathrm{donor}}$"
    )

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    ##
    #
    result_dir = (
        "/home/david/projects/binary_c_root/results/BALLISTIC/L1_stream/sepinsky_grid"
    )

    #
    plot_dir = "/home/david/Dropbox/Academic/PHD/papers/paper_disks/paper_tex/figures/ballistic"
    # plot_dir = "plots/"

    basename = "comparison_methods_rcirc.pdf"

    #
    plot_grid(
        result_dir,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(plot_dir, basename),
        },
    )
