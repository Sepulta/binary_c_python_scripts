"""
Function to plot the ZAMS mass schematic information. Generalised for either variation
"""

import matplotlib as mpl
import matplotlib.pyplot as plt

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.add_zams_mass_schematic_to_axis import (
    add_zams_mass_schematic_to_axis,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def plot_zams_mass_schematic_generalised(
    result_directory_variation,
    old_prescription_result_directory_root,
    no_prescription_result_directory_root=None,
    variation_name="mass_removal",
    add_preSN_mass_mass_removal=True,
    add_no_prescription_line=False,
    add_old_prescription_line=False,
    add_fiducial_line=True,
    add_variation_line=False,
    add_bracket_extra_mass_loss=False,
    create_shaded_regions=True,
    create_supernova_lines=True,
    dataset_used_for_regions="fiducial",
    plot_settings={},
):
    """
    Function to plot the ZAMS mass to remnant mass
    """

    # Set up figure
    fig = plt.figure(figsize=(20, 12))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=8, ncols=1)
    ax = fig.add_subplot(gs[:, 0])

    # Call function to add the plot to the axis
    fig, ax = add_zams_mass_schematic_to_axis(
        fig,
        ax=ax,
        result_directory_variation=result_directory_variation,
        old_prescription_result_directory_root=old_prescription_result_directory_root,
        no_prescription_result_directory_root=no_prescription_result_directory_root,
        variation_name=variation_name,
        add_old_prescription_line=add_old_prescription_line,
        add_no_prescription_line=add_no_prescription_line,
        add_variation_line=add_variation_line,
        add_preSN_mass_mass_removal=add_preSN_mass_mass_removal,
        add_fiducial_line=add_fiducial_line,
        add_bracket_extra_mass_loss=add_bracket_extra_mass_loss,
        create_shaded_regions=create_shaded_regions,
        create_supernova_lines=create_supernova_lines,
        dataset_used_for_regions=dataset_used_for_regions,
    )

    # Override the limits if we have input:

    # Handle the plotting
    show_and_save_plot(fig, plot_settings)
