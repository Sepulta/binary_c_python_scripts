"""
functions to plot data
"""

import numpy as np

import matplotlib.pyplot as plt
from custom_mpl_settings import load_mpl_rc

from ensemble_functions.ensemble_functions import (
    inflate_ensemble,
    get_recursive_count,
    flatten_data_ensemble1d,
)

load_mpl_rc()


def plot_yield_per_channel(yield_data):
    """
    Function to plot total yield per channel
    """

    per_source_per_time_dict = {}

    for time in yield_data["time"]:
        current_time = yield_data["time"][time]

        for source in current_time["source"].keys():
            if not source in per_source_per_time_dict:
                per_source_per_time_dict[source] = {}
            per_source_per_time_dict[source][float(time)] = get_recursive_count(
                current_time["source"][source]
            )

    # Plot
    fig = plt.figure(figsize=(20, 20))

    # Loop over the sources and plot
    for source in per_source_per_time_dict:
        new_lis = list(per_source_per_time_dict[source].items())
        con_arr = np.array(new_lis)

        plt.plot(con_arr[:, 0], con_arr[:, 1], label=source)

    plt.ylabel(r"Yield [1/M$_{\odot}$]")
    plt.xlabel("log(time) [Myr]")
    plt.legend()
    plt.show()
