"""
Function to plot the yield distribution of the TZO formation over all metallicities
"""

import numpy as np

import matplotlib.pyplot as plt

from grav_waves.convolution.functions.convolution_general_functions import (
    create_bins_from_centers,
)

from TZO.functions.functions import load_combined_dataframe

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def plot_yield_distribution_over_metallicity(combined_df, plot_settings={}):
    """
    Function to plot the yield distribution over metallicity
    """

    log10bincenters = np.sort(
        np.unique(np.log10(combined_df["metallicity"].to_numpy()))
    )
    log10bins = create_bins_from_centers(log10bincenters)
    bins = 10**log10bins
    bincenters = 10**log10bincenters

    hist = np.histogram(
        combined_df["metallicity"],
        bins=bins,
        weights=combined_df["number_per_solar_mass"],
    )

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(20, 10))
    gs = fig.add_gridspec(nrows=1, ncols=11)

    ax = fig.add_subplot(gs[:, :])

    ax.plot(bincenters, hist[0])
    ax.set_xscale("log")
    ax.set_yscale("log")

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    combined_df = load_combined_dataframe(
        "/home/david/projects/binary_c_root/results/TZO/server_results/MID_RES_TESTING/population_results/"
    )

    plot_yield_distribution_over_metallicity(
        combined_df, plot_settings={"show_plot": True}
    )
