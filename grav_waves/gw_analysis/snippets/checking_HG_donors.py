import os

from grav_waves.gw_analysis.functions.functions import load_info_dict, make_df

MAIN_RESULT_DIR = os.path.abspath(
    "/home/david/projects/binary_c_root/results/GRAV_WAVES"
)

# List to loop over
DATASET_KEY_LIST = [
    "MID_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW",
]

dataset_root = os.path.join(MAIN_RESULT_DIR, DATASET_KEY_LIST[0])
print(os.listdir(dataset_root))

for subdir in os.listdir(dataset_root):
    if subdir.startswith("Z"):
        full_path_subdir = os.path.join(dataset_root, subdir)

        all_dataset_path = os.path.join(full_path_subdir, "total_compact_objects.dat")
        df = make_df(all_dataset_path)

        print(
            df[df.undergone_CE_with_HG_donor == 1][
                ["undergone_CE_with_HG_donor", "stellar_type_1", "stellar_type_2"]
            ]
        )
