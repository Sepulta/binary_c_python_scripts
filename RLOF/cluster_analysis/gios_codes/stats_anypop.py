# This file contains the stats pipeline for cluster age
# Usage: python stats_anypop.py <cluster shorthand name> (TBC)

import random
import json
import numpy as np
import matplotlib.pyplot as plt
import KS2D
import ndtest
import statistics

######################################################################"
def read_simplified(file_name):
    with open(file_name, "r") as f:
        x = f.readlines()

    logP = []
    e = []
    for item in x:
        logP.append(float(item.split()[0]))
        e.append(float(item.split()[1]))
    print(logP)
    print(e)

    return logP, e, e.__len__()


######################################################################"
def apply_cutoff(logPearray, cutoff):
    obs_cut = []
    for star in logPearray:
        if star[0] < cutoff[0] and star[1] < cutoff[1]:
            obs_cut.append(star)
    return np.array(obs_cut)


######################################################################"
def observations(file_name, cutoff):
    """
    This routine redirects the code based on the cluster
    to read the data tables and retrieve (logP, e)
    """
    logP, e, num_stars = read_simplified(file_name)

    # Turn into a 2D table as needed later in code
    obs = []
    for i in range(num_stars):
        obs.append([logP[i], e[i]])
    print(obs)
    obs = apply_cutoff(obs, cutoff)

    return np.array(obs)


######################################################################"
# Bootstrapping
def bootstrap_syrte(myobs, pop_list, pop_weights):
    self_p2 = []
    self_kss2 = []

    comp_p2 = []
    comp_kss2 = []

    myobs_x, myobs_y = myobs.T

    for i in range(1000):
        outpop1 = np.array(
            random.choices(pop_list, weights=pop_weights, k=myobs.__len__())
        )
        outpop1_x, outpop1_y = outpop1.T
        outpop2 = np.array(
            random.choices(pop_list, weights=pop_weights, k=myobs.__len__())
        )
        outpop2_x, outpop2_y = outpop2.T

        # Test self-match between two identical samples
        ks_test1 = ndtest.ks2d2s(outpop1_x, outpop1_y, outpop1_x, outpop1_y, extra=True)
        #        print('Test pop1 vs pop1 = {}'.format( float(ks_test1[0])) )

        # Compute distance between two samples from the same pop
        #        print('Testing with syrte/ndtest twodim ks')
        ks_self = ndtest.ks2d2s(outpop1_x, outpop1_y, outpop2_x, outpop2_y, extra=True)
        #        print('Test pop1 v pop2 = {}'.format( float(ks_self[0])) )
        self_p2.append(ks_self[0])
        self_kss2.append(ks_self[1])

        # Compute distance between pop sample and observations
        ks_comp = ndtest.ks2d2s(myobs_x, myobs_y, outpop1_x, outpop1_y, extra=True)
        #        print('Test obs v bse1 = {}'.format( float(ks_comp[0])) )
        comp_p2.append(ks_comp[0])
        comp_kss2.append(ks_comp[1])

    # We want to output self_kss2 and comp_kss2 for the plots

    # We also want to compute the statistically relevant quantities
    self_kss2_mean = statistics.mean(self_kss2)
    self_kss2_sigma = statistics.stdev(self_kss2)

    comp_kss2_mean = statistics.mean(comp_kss2)
    comp_kss2_sigma = statistics.stdev(comp_kss2)

    print("self bootstrap, mean={}, sigma={}".format(self_kss2_mean, self_kss2_sigma))
    print("comp bootstrap, mean={}, sigma={}".format(comp_kss2_mean, comp_kss2_sigma))

    return (
        self_kss2_mean,
        self_kss2_sigma,
        comp_kss2_mean,
        comp_kss2_sigma,
        np.array(self_kss2),
        np.array(comp_kss2),
    )


######################################################################"
def read_population_basic(data, cutoff):

    x = []
    y = []
    z = []

    for p in data["ensemble"]["distributions"]["P vs e MSMS binaries const SFR"][
        "log orbital period"
    ]:
        for q in data["ensemble"]["distributions"]["P vs e MSMS binaries const SFR"][
            "log orbital period"
        ][p]["eccentricity"]:
            if (
                float(p) + 2.56259778679 < cutoff[0] and float(q) < cutoff[1]
            ):  # keep only data for P<50days and e<0.6 for obs bi    as
                x.append(
                    float(p) + 2.56259778679
                )  # add log10(365..) to convert from log(yr) to log(d)
                y.append(float(q))
                z.append(
                    float(
                        data["ensemble"]["distributions"][
                            "P vs e MSMS binaries const SFR"
                        ]["log orbital period"][p]["eccentricity"][q]
                    )
                )

    print(x.__len__())
    print(y.__len__())
    print(z.__len__())

    return x, y, z


######################################################################"
def read_population_agebinned(data, age, cutoff):

    x = []
    y = []
    z = []

    for p in data["ensemble"]["distributions"]["P vs e(t) MSMS binaries"]["age"][age][
        "log orbital period"
    ]:
        for q in data["ensemble"]["distributions"]["P vs e(t) MSMS binaries"]["age"][
            age
        ]["log orbital period"][p]["eccentricity"]:
            if (
                float(p) + 2.56259778679 < cutoff[0] and float(q) < cutoff[1]
            ):  # keep only data for P<50days and e<0.6 for obs bias
                x.append(
                    float(p) + 2.56259778679
                )  # add log10(365..) to convert from log(yr) to log(d)
                y.append(float(q))
                z.append(
                    float(
                        data["ensemble"]["distributions"][
                            "P vs e MSMS binaries const SFR"
                        ]["log orbital period"][p]["eccentricity"][q]
                    )
                )

    print(x.__len__())
    print(y.__len__())
    print(z.__len__())

    return x, y, z


######################################################################"
######################################################################"
##   MAIN   ##
######################################################################"
######################################################################"
# store_arrays = []
import stats_dic
import cluster_settings
import sys

cluster_name = sys.argv[1]
# cluster_name = 'M35'

# STATS WITH NO CUTOFF
cluster = cluster_settings.values[cluster_name]

dic = stats_dic.runs[cluster_name]["distributions"]
cutoff = [100, 100]
myobs = observations(cluster["obs_file"], cutoff)
print("Observations", myobs)

f = open("{}_nocutoff.dat".format(stats_dic.runs[cluster_name]["output"]), "w")
f.write("# file  self_mean  self_sigma  comp_mean  comp_sigma\n")
ft = open("{}_nocutoff.tex".format(stats_dic.runs[cluster_name]["output"]), "w")
ft.write(
    "Distributions  & Tides & Distance to self   & Distance to obs   \\\\\hline \n"
)

for tide_presc in dic:
    for dist_presc in dic[tide_presc]:
        print(tide_presc, dist_presc)
        pop_file = dic[tide_presc][dist_presc]["datafile"]

        # Read and prepare population data
        with open(pop_file) as json_file:
            data = json.load(json_file)

        # x,y,z = read_population_basic(data,cutoff)
        x, y, z = read_population_agebinned(
            data, cluster["age_stat"], cutoff
        )  # age has to be a string!
        # Format data for bootstrapping
        pop_list = []
        for i in range(x.__len__()):
            pop_list.append([x[i], y[i]])
        pop_weights = z
        (
            self_mean,
            self_sigma,
            comp_mean,
            comp_sigma,
            self_kss_array,
            comp_kss_array,
        ) = bootstrap_syrte(myobs, pop_list, pop_weights)

        # fill dictionary
        dic[tide_presc][dist_presc]["self_mean"] = self_mean
        dic[tide_presc][dist_presc]["self_sigma"] = self_sigma
        dic[tide_presc][dist_presc]["comp_mean"] = comp_mean
        dic[tide_presc][dist_presc]["comp_sigma"] = comp_sigma
        dic[tide_presc][dist_presc]["self_kss"] = self_kss_array
        dic[tide_presc][dist_presc]["comp_kss"] = comp_kss_array

        f.write(
            "{id} {self_mean:.4f} {self_sigma:.4f} {comp_mean:.4f} {comp_sigma:.4f}\n".format(
                **dic[tide_presc][dist_presc]
            )
        )
        print(
            "{} {} {} {} {}\n".format(
                pop_file, self_mean, self_sigma, comp_mean, comp_sigma
            )
        )

        if tide_presc == "BSE" and dist_presc == "Gauss":
            ft.write(
                "Gaussians  & \\bse & ${self_mean:.3f} \\pm {self_sigma:.3f}$ & ${comp_mean:.3f} \\pm {comp_sigma:.3f}$\\\\ \n".format(
                    **dic[tide_presc][dist_presc]
                )
            )
        if tide_presc == "BSE" and dist_presc == "MS":
            ft.write(
                "Moe \& di Stefano  & \\bse & ${self_mean:.3f} \\pm {self_sigma:.3f}$ & ${comp_mean:.3f} \\pm {comp_sigma:.3f}$\\\\ \n".format(
                    **dic[tide_presc][dist_presc]
                )
            )
        if tide_presc == "MINT" and dist_presc == "Gauss":
            ft.write(
                "Gaussians  & \\mint & ${self_mean:.3f} \\pm {self_sigma:.3f}$ & ${comp_mean:.3f} \\pm {comp_sigma:.3f}$\\\\ \n".format(
                    **dic[tide_presc][dist_presc]
                )
            )
        if tide_presc == "MINT" and dist_presc == "MS":
            ft.write(
                "Moe \& di Stefano  & \\mint & ${self_mean:.3f} \\pm {self_sigma:.3f}$ & ${comp_mean:.3f} \\pm {comp_sigma:.3f}$\\\\ \\hline \n".format(
                    **dic[tide_presc][dist_presc]
                )
            )

######################################################################"
nbins = myobs.__len__()
SMALL_SIZE = 10
MEDIUM_SIZE = 12
BIGGER_SIZE = 14

plt.rc("font", size=SMALL_SIZE)  # controls default text sizes
plt.rc("axes", titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc("legend", fontsize=SMALL_SIZE)  # legend fontsize
plt.rc("figure", titlesize=BIGGER_SIZE)  # fontsize of the figure title


for tide_presc in dic:
    for dist_presc in dic[tide_presc]:
        print(tide_presc, dist_presc)
        pop_file = dic[tide_presc][dist_presc]["datafile"]

        hist = np.histogram(
            dic[tide_presc][dist_presc]["comp_kss"], bins=np.linspace(0.0, 1.0, nbins)
        )
        plt.plot(
            hist[1][:-1],
            hist[0] / max(hist[0]),
            color=dic[tide_presc][dist_presc]["color"],
            linewidth=2,
            label=dic[tide_presc][dist_presc]["id"],
            linestyle=dic[tide_presc][dist_presc]["style"],
        )

hist = np.histogram(
    dic[tide_presc][dist_presc]["self_kss"], bins=np.linspace(0.0, 1.0, nbins)
)
plt.plot(hist[1][:-1], hist[0] / max(hist[0]), color="black", linewidth=2, label="Self")

# plt.title(cluster['cluster name'])
# Display binwidth in tr corner
plt.plot(
    [0.97 - 1.0 / nbins, 0.97], [1.05, 1.05], color="k", linestyle="-", linewidth=2
)
plt.text(
    0.97 - 1.0 / nbins - 0.01,
    1.05,
    "bin width :",
    verticalalignment="center",
    horizontalalignment="right",
)

plt.ylim((0, 1.1))
plt.xlim((0.0, 1.0))
plt.xlabel("KS statistical distance")
plt.ylabel("Normalised count")
plt.legend()
plt.savefig(
    "{}_nocutoff.pdf".format(stats_dic.runs[cluster_name]["output"]),
    bbox_inches="tight",
)
plt.close()
######################################################################"

dic = stats_dic.runs[cluster_name]["distributions"]
cutoff = stats_dic.runs[cluster_name]["cutoff"]
myobs = observations(cluster["obs_file"], cutoff)
print("Observations", myobs)

f = open(
    "{}_{}cutoff.dat".format(stats_dic.runs[cluster_name]["output"], cutoff[0]), "w"
)
f.write("# file  self_mean  self_sigma  comp_mean  comp_sigma\n")
ft = open(
    "{}_{}cutoff.tex".format(stats_dic.runs[cluster_name]["output"], cutoff[0]), "w"
)
ft.write(
    "Distributions  & Tides & Distance to self   & Distance to obs   \\\\\hline \n"
)

for tide_presc in dic:
    for dist_presc in dic[tide_presc]:
        print(tide_presc, dist_presc)
        pop_file = dic[tide_presc][dist_presc]["datafile"]

        # Read and prepare population data
        with open(pop_file) as json_file:
            data = json.load(json_file)

        # x,y,z = read_population_basic(data,cutoff)
        x, y, z = read_population_agebinned(
            data, cluster["age_stat"], cutoff
        )  # age has to be a string!
        # Format data for bootstrapping
        pop_list = []
        for i in range(x.__len__()):
            pop_list.append([x[i], y[i]])
        pop_weights = z
        (
            self_mean,
            self_sigma,
            comp_mean,
            comp_sigma,
            self_kss_array,
            comp_kss_array,
        ) = bootstrap_syrte(myobs, pop_list, pop_weights)

        # fill dictionary
        dic[tide_presc][dist_presc]["self_mean"] = self_mean
        dic[tide_presc][dist_presc]["self_sigma"] = self_sigma
        dic[tide_presc][dist_presc]["comp_mean"] = comp_mean
        dic[tide_presc][dist_presc]["comp_sigma"] = comp_sigma
        dic[tide_presc][dist_presc]["self_kss"] = self_kss_array
        dic[tide_presc][dist_presc]["comp_kss"] = comp_kss_array

        f.write(
            "{id} {self_mean:.4f} {self_sigma:.4f} {comp_mean:.4f} {comp_sigma:.4f}\n".format(
                **dic[tide_presc][dist_presc]
            )
        )
        print(
            "{} {} {} {} {}\n".format(
                pop_file, self_mean, self_sigma, comp_mean, comp_sigma
            )
        )

        if tide_presc == "BSE" and dist_presc == "Gauss":
            ft.write(
                "Gaussians  & \\bse & ${self_mean:.3f} \\pm {self_sigma:.3f}$ & ${comp_mean:.3f} \\pm {comp_sigma:.3f}$\\\\ \n".format(
                    **dic[tide_presc][dist_presc]
                )
            )
        if tide_presc == "BSE" and dist_presc == "MS":
            ft.write(
                "Moe \& di Stefano  & \\bse & ${self_mean:.3f} \\pm {self_sigma:.3f}$ & ${comp_mean:.3f} \\pm {comp_sigma:.3f}$\\\\ \n".format(
                    **dic[tide_presc][dist_presc]
                )
            )
        if tide_presc == "MINT" and dist_presc == "Gauss":
            ft.write(
                "Gaussians  & \\mint & ${self_mean:.3f} \\pm {self_sigma:.3f}$ & ${comp_mean:.3f} \\pm {comp_sigma:.3f}$\\\\ \n".format(
                    **dic[tide_presc][dist_presc]
                )
            )
        if tide_presc == "MINT" and dist_presc == "MS":
            ft.write(
                "Moe \& di Stefano  & \\mint & ${self_mean:.3f} \\pm {self_sigma:.3f}$ & ${comp_mean:.3f} \\pm {comp_sigma:.3f}$\\\\ \\hline \n".format(
                    **dic[tide_presc][dist_presc]
                )
            )

######################################################################"
nbins = myobs.__len__()
SMALL_SIZE = 10
MEDIUM_SIZE = 12
BIGGER_SIZE = 14

plt.rc("font", size=SMALL_SIZE)  # controls default text sizes
plt.rc("axes", titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc("legend", fontsize=SMALL_SIZE)  # legend fontsize
plt.rc("figure", titlesize=BIGGER_SIZE)  # fontsize of the figure title


for tide_presc in dic:
    for dist_presc in dic[tide_presc]:
        print(tide_presc, dist_presc)
        pop_file = dic[tide_presc][dist_presc]["datafile"]

        hist = np.histogram(
            dic[tide_presc][dist_presc]["comp_kss"], bins=np.linspace(0.0, 1.0, nbins)
        )
        plt.plot(
            hist[1][:-1],
            hist[0] / max(hist[0]),
            color=dic[tide_presc][dist_presc]["color"],
            linewidth=2,
            label=dic[tide_presc][dist_presc]["id"],
            linestyle=dic[tide_presc][dist_presc]["style"],
        )

hist = np.histogram(
    dic[tide_presc][dist_presc]["self_kss"], bins=np.linspace(0.0, 1.0, nbins)
)
plt.plot(hist[1][:-1], hist[0] / max(hist[0]), color="black", linewidth=2, label="Self")

# plt.title(cluster['cluster name'])
# plt.plot([0.97-1./nbins, 0.97], [1.05, 1.05], color='k', linestyle='-', linewidth=2)
plt.plot(
    [0.97 - 1.0 / nbins, 0.97], [1.05, 1.05], color="k", linestyle="-", linewidth=2
)
plt.text(
    0.97 - 1.0 / nbins - 0.01,
    1.05,
    "bin width :",
    verticalalignment="center",
    horizontalalignment="right",
)
plt.ylim((0, 1.1))
plt.xlim((0.0, 1.0))
plt.xlabel("KS statistical distance")
plt.ylabel("Normalised count")
plt.legend()
plt.savefig(
    "{}_cutoff.pdf".format(stats_dic.runs[cluster_name]["output"]), bbox_inches="tight"
)
plt.close()

######################################################################"
