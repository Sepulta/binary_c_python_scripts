"""
Function to plot the distribution of yield per metallicity
"""

import os

import h5py
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

#
from grav_waves.convolution.functions.convolution_general_functions import (
    create_bins_from_centers,
)
from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    generate_mask,
    add_columns_to_df,
    quantity_name_dict,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def readout_gw_events():
    """ """
    pass


def readout_sn_events_binary():
    """ """
    pass


def readout_sn_events_single():
    """ """
    pass


def plot_yield_vs_metallicity(dataset_dict, scale, plot_settings={}):
    """
    Function to plot the merger time vs metallicity for source distributions
    """

    # Read out info
    hdf5file = h5py.File(dataset_filename)
    metallicity_bin_centers = np.array(hdf5file["sfr_data/metallicity_bin_centers"][()])
    log10metallicity_values = np.array(sorted(np.log10(metallicity_bin_centers)))
    log10metallicity_bins = create_bins_from_centers(log10metallicity_values)
    metallicity_bins = 10**log10metallicity_bins
    hdf5file.close()

    # Read out combined df
    combined_df = pd.read_hdf(dataset_filename, "/data/combined_dataframes")
    combined_df = add_columns_to_df(combined_df, columns=["primary_mass"])

    # Create mask
    mask = generate_mask(combined_df, general_query=None, dco_type="bhbh")

    # Get the data from the dataframe
    number_per_formed_solarmass = combined_df[
        "number_per_solar_mass_values"
    ].to_numpy()[mask]
    metallicity = combined_df["metallicity"].to_numpy()[mask]

    #
    hist = np.histogram(
        metallicity, bins=metallicity_bins, weights=number_per_formed_solarmass
    )

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=11)

    ax = fig.add_subplot(gs[:, :])

    # ##################
    # # Plot the data

    ##################
    # Plot the MSSFR results
    _ = ax.plot(
        metallicity_bin_centers,
        hist[0] / 1e-5,
    )

    #
    ax.set_xscale("log")
    ax.set_yscale(scale)
    ax.set_ylabel(
        r"Yield (d$N_{\mathrm{BBH\ }t_{\mathrm{merge}} < t_{\mathrm{Hubble}}}}$/d$M_{\mathrm{SFR}}$) [10$^{-5}$ M$_{\odot}^{-1}]$"
    )
    ax.set_xlabel("Metallicity [Z]")
    ax.set_title("Yield (number per formed solar mass) per metallicity", fontsize=28)

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    # Get dataset
    result_root = (
        "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/"
    )
    simname = "NEW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED"

    dataset_dict = {
        "binary_dco_rates": os.path.join(
            result_root, simname, "dco_convolution_results/dco_convolution_results.h5"
        ),
        "binary_sn_rates": os.path.join(
            result_root, simname, "sn_convolution_results/sn_convolution_results.h5"
        ),
        "single_sn_rates": os.path.join(
            result_root,
            simname,
            "single_sn_convolution_results/single_sn_convolution_results.h5",
        ),
        "name": "fiducial",
    }

    #
    base_output_name = "yield_vs_metallicity.pdf"

    #
    output_dir = (
        "/home/david/papers/paper_gw/paper_tex/figures/fiducial_combined_dataframe/"
    )
    output_dir = "plots/"

    #
    plot_yield_vs_metallicity(
        dataset_dict=dataset_dict,
        scale="linear",
        plot_settings={
            "show_plot": True,
            "output_name": os.path.join(output_dir, base_output_name),
        },
    )
