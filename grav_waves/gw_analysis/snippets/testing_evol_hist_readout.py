from settings import *
import settings

datafile = "/home/david/projects/binary_c_root/results/TESTING_EVOLHIST/HIGH_RES_SCHNEIDER_MASS_PPISN_ON/Z0.0002/total_compact_objects.dat"

df = pd.read_table(datafile, sep="\s+")
searchfor = "CE"
df["TrueFalse"] = df["evol_hist"].str.contains(searchfor)
