"""
Script containing the big general plot routine
"""

import os
import json
import time

from plot_functions import (
    plot_general_probability_fractions_accretors_and_donors,
    plot_general_probability_fractions_accretors,
    plot_general_probability_fractions_donors,
    plot_log_massfraction_separation_fraction,
    plot_specific_angmom_ratio_distribution,
    plot_specific_angmom_ratio_fraction_accretor_separation_distribution,
)
from functions import (
    generate_frontpage_pdf_fraction_ensemble,
    getFromDict,
    get_recursive_sum,
)

from PyPDF2 import PdfFileMerger
import fpdf
from david_phd_functions.plotting.utils import add_pdf_and_bookmark


def general_plot_function(ensemble_data, plot_dir, output_filename_pdf):
    """
    Function to glue together all the plots into a pdf

    TODO: add functionality to kwargs to disable plots
    TODO: add functionality to change the plot dirs
    """

    sim_name = ensemble_data["metadata"]["simname"]

    weight_types = ["time", "mass"]
    bin_types = ["linear", "log10"]
    format_types = ["png", "pdf"]
    # format_types = ['pdf']
    sub_ensemble = ["ensemble", "RLOF", "fraction_separation_massratio"]

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_list = []
    pdf_page_number = 0

    # Generate frontpage pdf
    os.makedirs(os.path.join(plot_dir, "pdf"), exist_ok=True)
    frontpage_name = os.path.join(plot_dir, "pdf", "frontpage.pdf")
    generate_frontpage_pdf_fraction_ensemble(ensemble_data, frontpage_name)
    pdf_list.append(frontpage_name)
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger, pdf_list[-1], pdf_page_number, bookmark_text="Frontpage"
    )

    # To compare
    linear_results = get_recursive_sum(
        getFromDict(ensemble_data, sub_ensemble + ["time", "linear"])
    )
    log10_results = get_recursive_sum(
        getFromDict(ensemble_data, sub_ensemble + ["time", "log10"])
    )

    assert (
        linear_results - log10_results
    ) / linear_results < 1e-8  # Check if the results are correct

    # Plot plot_general_probability_fractions_accretors
    print("Plotting plot_general_probability_fractions_accretors:")
    for weight_type in weight_types:
        for bin_type in ["linear"]:
            for format_type in format_types:
                print(
                    "\t\tweight type: {} bin type: {} format type: {}".format(
                        weight_type, bin_type, format_type
                    )
                )
                plot_general_probability_fractions_accretors(
                    ensemble_data,
                    sub_ensemble,
                    weight_type=weight_type,
                    bin_type=bin_type,
                    plot_settings={
                        "show_plot": False,
                        "output_name": os.path.join(
                            plot_dir,
                            format_type,
                            "plot_general_probability_fractions_accretors_weighttype_{}_bintype_{}.{}".format(
                                weight_type, bin_type, format_type
                            ),
                        ),
                        "simulation_name": sim_name,
                        "antialiased": True,
                        "rasterized": True,
                        "runname": "plot_general_probability_fractions_accretors: weight_type: {} bin_type: {}".format(
                            weight_type, bin_type
                        ),
                    },
                )
            pdf_list.append(
                os.path.join(
                    plot_dir,
                    "pdf",
                    "plot_general_probability_fractions_accretors_weighttype_{}_bintype_{}.pdf".format(
                        weight_type, bin_type
                    ),
                )
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                pdf_list[-1],
                pdf_page_number,
                bookmark_text="plot_general_probability_fractions_accretors weighttype: {} bintype: {}".format(
                    weight_type, bin_type
                ),
            )

    # Plot plot_general_probability_fractions_donors
    print("Plotting plot_general_probability_fractions_donors:")
    for weight_type in weight_types:
        for bin_type in ["linear"]:
            for format_type in format_types:
                print(
                    "\t\tweight type: {} bin type: {} format type: {}".format(
                        weight_type, bin_type, format_type
                    )
                )
                plot_general_probability_fractions_donors(
                    ensemble_data,
                    sub_ensemble,
                    weight_type=weight_type,
                    bin_type=bin_type,
                    plot_settings={
                        "show_plot": False,
                        "output_name": os.path.join(
                            plot_dir,
                            format_type,
                            "plot_general_probability_fractions_donors_weighttype_{}_bintype_{}.{}".format(
                                weight_type, bin_type, format_type
                            ),
                        ),
                        "simulation_name": sim_name,
                        "antialiased": True,
                        "rasterized": True,
                        "runname": "plot_general_probability_fractions_donors: weight_type: {} bin_type: {}".format(
                            weight_type, bin_type
                        ),
                    },
                )
            pdf_list.append(
                os.path.join(
                    plot_dir,
                    "pdf",
                    "plot_general_probability_fractions_donors_weighttype_{}_bintype_{}.pdf".format(
                        weight_type, bin_type
                    ),
                )
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                pdf_list[-1],
                pdf_page_number,
                bookmark_text="plot_general_probability_fractions_donors weighttype: {} bintype: {}".format(
                    weight_type, bin_type
                ),
            )

    # Plot plot_general_probability_fractions_accretors_and_donors
    print("Plotting plot_general_probability_fractions_accretors_and_donors:")
    for weight_type in weight_types:
        for bin_type in ["linear"]:
            for format_type in format_types:
                print(
                    "\t\tweight type: {} bin type: {} format type: {}".format(
                        weight_type, bin_type, format_type
                    )
                )
                plot_general_probability_fractions_accretors_and_donors(
                    ensemble_data,
                    sub_ensemble,
                    weight_type=weight_type,
                    bin_type=bin_type,
                    plot_settings={
                        "show_plot": False,
                        "output_name": os.path.join(
                            plot_dir,
                            format_type,
                            "plot_general_probability_fractions_accretors_and_donors_weighttype_{}_bintype_{}.{}".format(
                                weight_type, bin_type, format_type
                            ),
                        ),
                        "simulation_name": sim_name,
                        "antialiased": True,
                        "rasterized": True,
                        "runname": "plot_general_probability_fractions_accretors_and_donors: weight_type: {} bin_type: {}".format(
                            weight_type, bin_type
                        ),
                    },
                )
            pdf_list.append(
                os.path.join(
                    plot_dir,
                    "pdf",
                    "plot_general_probability_fractions_accretors_and_donors_weighttype_{}_bintype_{}.pdf".format(
                        weight_type, bin_type
                    ),
                )
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                pdf_list[-1],
                pdf_page_number,
                bookmark_text="plot_general_probability_fractions_accretors_and_donors weighttype: {} bintype: {}".format(
                    weight_type, bin_type
                ),
            )

    # Plot plot_log_massfraction_separation_fraction
    print("Plotting plot_log_massfraction_separation_fraction:")
    for weight_type in weight_types:
        for bin_type in bin_types:
            for format_type in format_types:
                print(
                    "\t\tweight type: {} bin type: {} format type: {}".format(
                        weight_type, bin_type, format_type
                    )
                )
                plot_log_massfraction_separation_fraction(
                    ensemble_data,
                    sub_ensemble,
                    weight_type=weight_type,
                    bin_type=bin_type,
                    plot_settings={
                        "show_plot": False,
                        "output_name": os.path.join(
                            plot_dir,
                            format_type,
                            "plot_log_massfraction_separation_fraction_weighttype_{}_bintype_{}.{}".format(
                                weight_type, bin_type, format_type
                            ),
                        ),
                        "simulation_name": sim_name,
                        "antialiased": True,
                        "rasterized": True,
                        "runname": "plot_log_massfraction_separation_fraction: weight_type: {} bin_type: {}".format(
                            weight_type, bin_type
                        ),
                    },
                )
            pdf_list.append(
                os.path.join(
                    plot_dir,
                    "pdf",
                    "plot_log_massfraction_separation_fraction_weighttype_{}_bintype_{}.pdf".format(
                        weight_type, bin_type
                    ),
                )
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                pdf_list[-1],
                pdf_page_number,
                bookmark_text="plot_log_massfraction_separation_fraction weighttype: {} bintype: {}".format(
                    weight_type, bin_type
                ),
            )

    # Plot plot_log_massfraction_separation_fraction
    print("Plotting plot_log_massfraction_separation_fraction:")
    for accretor_n in [1]:
        for weight_type in weight_types:
            for bin_type in bin_types:
                for format_type in format_types:
                    print(
                        "\t\tweight type: {} bin type: {} format type: {} accretor type: {}".format(
                            weight_type, bin_type, format_type, accretor_n
                        )
                    )
                    plot_log_massfraction_separation_fraction(
                        ensemble_data,
                        sub_ensemble,
                        weight_type=weight_type,
                        bin_type=bin_type,
                        accretor_stellar_type=accretor_n,
                        plot_settings={
                            "show_plot": False,
                            "output_name": os.path.join(
                                plot_dir,
                                format_type,
                                "plot_log_massfraction_separation_fraction_weighttype_{}_bintype_{}_accretor_{}.{}".format(
                                    weight_type, bin_type, accretor_n, format_type
                                ),
                            ),
                            "simulation_name": sim_name,
                            "antialiased": True,
                            "rasterized": True,
                            "runname": "plot_log_massfraction_separation_fraction: weight_type: {} bin_type: {} accretor: {}".format(
                                weight_type, bin_type, accretor_n
                            ),
                        },
                    )
                pdf_list.append(
                    os.path.join(
                        plot_dir,
                        "pdf",
                        "plot_log_massfraction_separation_fraction_weighttype_{}_bintype_{}_accretor_{}.pdf".format(
                            weight_type, bin_type, accretor_n
                        ),
                    )
                )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    pdf_list[-1],
                    pdf_page_number,
                    bookmark_text="plot_log_massfraction_separation_fraction weighttype: {} bintype: {} accretor: {}".format(
                        weight_type, bin_type, accretor_n
                    ),
                )

    # Plot plot_specific_angmom_ratio_distribution
    print("Plotting plot_specific_angmom_ratio_distribution:")
    for accretor_n in [1]:
        for weight_type in weight_types:
            for bin_type in ["log10"]:
                for format_type in format_types:
                    print(
                        "\t\tweight type: {} bin type: {} format type: {} accretor type: {}".format(
                            weight_type, bin_type, format_type, accretor_n
                        )
                    )
                    plot_specific_angmom_ratio_distribution(
                        ensemble_data,
                        sub_ensemble,
                        weight_type=weight_type,
                        bin_type=bin_type,
                        accretor_stellar_type=accretor_n,
                        plot_settings={
                            "show_plot": False,
                            "output_name": os.path.join(
                                plot_dir,
                                format_type,
                                "plot_specific_angmom_ratio_distribution_weighttype_{}_bintype_{}_accretor_{}.{}".format(
                                    weight_type, bin_type, accretor_n, format_type
                                ),
                            ),
                            "simulation_name": sim_name,
                            "antialiased": True,
                            "rasterized": True,
                            "runname": "plot_specific_angmom_ratio_distribution: weight_type: {} bin_type: {} accretor: {}".format(
                                weight_type, bin_type, accretor_n
                            ),
                        },
                    )
                pdf_list.append(
                    os.path.join(
                        plot_dir,
                        "pdf",
                        "plot_specific_angmom_ratio_distribution_weighttype_{}_bintype_{}_accretor_{}.pdf".format(
                            weight_type, bin_type, accretor_n
                        ),
                    )
                )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    pdf_list[-1],
                    pdf_page_number,
                    bookmark_text="plot_specific_angmom_ratio_distribution weighttype: {} bintype: {} accretor: {}".format(
                        weight_type, bin_type, accretor_n
                    ),
                )

    # Plot plot_specific_angmom_ratio_fraction_accretor_separation_distribution
    print(
        "Plotting plot_specific_angmom_ratio_fraction_accretor_separation_distribution:"
    )
    for accretor_n in [1]:
        for weight_type in weight_types:
            for bin_type in ["log10"]:
                for format_type in format_types:
                    print(
                        "\t\tweight type: {} bin type: {} format type: {} accretor type: {}".format(
                            weight_type, bin_type, format_type, accretor_n
                        )
                    )
                    plot_specific_angmom_ratio_fraction_accretor_separation_distribution(
                        ensemble_data,
                        sub_ensemble,
                        weight_type=weight_type,
                        bin_type=bin_type,
                        accretor_stellar_type=accretor_n,
                        plot_settings={
                            "show_plot": False,
                            "output_name": os.path.join(
                                plot_dir,
                                format_type,
                                "plot_specific_angmom_ratio_fraction_accretor_separation_distribution_weighttype_{}_bintype_{}_accretor_{}.{}".format(
                                    weight_type, bin_type, accretor_n, format_type
                                ),
                            ),
                            "simulation_name": sim_name,
                            "antialiased": True,
                            "rasterized": True,
                            "runname": "plot_specific_angmom_ratio_fraction_accretor_separation_distribution: weight_type: {} bin_type: {} accretor: {}".format(
                                weight_type, bin_type, accretor_n
                            ),
                        },
                    )
                pdf_list.append(
                    os.path.join(
                        plot_dir,
                        "pdf",
                        "plot_specific_angmom_ratio_fraction_accretor_separation_distribution_weighttype_{}_bintype_{}_accretor_{}.pdf".format(
                            weight_type, bin_type, accretor_n
                        ),
                    )
                )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    pdf_list[-1],
                    pdf_page_number,
                    bookmark_text="plot_specific_angmom_ratio_fraction_accretor_separation_distribution weighttype: {} bintype: {} accretor: {}".format(
                        weight_type, bin_type, accretor_n
                    ),
                )

    # wrap up the pdf
    merger.write(output_filename_pdf)
    merger.close()

    print(
        "Finished generating all the ensemble plots. Wrote results to {}".format(
            output_filename_pdf
        )
    )


###########################
# local result plots
local_ensemble_files = []
local_ensemble_files.append(
    "/home/david/projects/binary_c_root/results/RLOF/RLOF_SANA_2021_ENSEMBLE_fractions/RLOFING_systems_z0.02/ensemble_output.json",
)
# quit()

# for ensemble_file in local_ensemble_files:
#     # Load ensemble file
#     with open(ensemble_file, 'r') as f:
#         ensemble_data = json.loads(f.read())

#     simname = ensemble_data['metadata']['simname']

#     #
#     general_plot_function(
#         ensemble_data,
#         plot_dir='plots/local/{}'.format(simname),
#         output_filename_pdf='plots/local/{}/all_plots.pdf'.format(simname)
#     )

###########################
# plot server results
server_ensemble_files = [
    # sana set
    "/home/david/projects/binary_c_root/results/RLOF/server_results/RLOF_SANA_2021_ENSEMBLE_fractions/RLOF_SANA_2021_ENSEMBLE_fractions_Z0.02/ensemble_output.json",
    "/home/david/projects/binary_c_root/results/RLOF/server_results/RLOF_SANA_2021_ENSEMBLE_fractions/RLOF_SANA_2021_ENSEMBLE_fractions_Z0.01/ensemble_output.json",
    "/home/david/projects/binary_c_root/results/RLOF/server_results/RLOF_SANA_2021_ENSEMBLE_fractions/RLOF_SANA_2021_ENSEMBLE_fractions_Z0.002/ensemble_output.json",
    # M&S set
    "/home/david/projects/binary_c_root/results/RLOF/server_results/RLOF_SANA_2021_ENSEMBLE_fractions/RLOF_SANA_2021_ENSEMBLE_fractions_Z0.02/ensemble_output.json",
    "/home/david/projects/binary_c_root/results/RLOF/server_results/RLOF_SANA_2021_ENSEMBLE_fractions/RLOF_SANA_2021_ENSEMBLE_fractions_Z0.01/ensemble_output.json",
    "/home/david/projects/binary_c_root/results/RLOF/server_results/RLOF_SANA_2021_ENSEMBLE_fractions/RLOF_SANA_2021_ENSEMBLE_fractions_Z0.002/ensemble_output.json",
]

for ensemble_file in server_ensemble_files:
    # Load ensemble file
    with open(ensemble_file, "r") as f:
        ensemble_data = json.loads(f.read())

    simname = ensemble_data["metadata"]["simname"]

    #
    general_plot_function(
        ensemble_data,
        plot_dir="plots/server/{}".format(simname),
        output_filename_pdf="plots/server/{}/all_plots.pdf".format(simname),
    )
