"""
Script to run the general plotting routine of the theory plots and save them locally
"""

import os

from RLOF.scripts.theory_plots.general_theory_plot_function import general_plot_routine

##############
# Generate all and copy to paper
##############

# # Set the directories of the paper
directory_paper_disks = os.path.expanduser("~/PHD/papers/paper_disks/paper_tex")
directory_paper_disks_figures = os.path.join(directory_paper_disks, "figures/theory")


combined_output_pdf_filename = "test.pdf"
rebuild_results = False

#
general_plot_routine(
    "output",
    output_pdf_filename=combined_output_pdf_filename,
    rebuild_results=rebuild_results,
    verbose=1,
)
