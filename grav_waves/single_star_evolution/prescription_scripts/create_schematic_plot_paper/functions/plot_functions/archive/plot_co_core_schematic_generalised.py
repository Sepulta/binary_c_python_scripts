"""
Generalised function to plot the CO core to remantn mass plots
"""

import matplotlib as mpl
import matplotlib.pyplot as plt

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.add_co_core_schematic_to_axis import (
    add_co_core_schematic_to_axis,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def plot_co_core_schematic_generalised(
    result_directory_variation,
    old_prescription_result_directory_root,
    no_prescription_result_directory_root=None,
    variation_name="mass_removal",
    add_no_prescription_line=False,
    add_old_prescription_line=False,
    add_fiducial_line=True,
    add_variation_line=False,
    add_bracket_extra_mass_loss=False,
    create_shaded_regions=True,
    create_supernova_lines=True,
    dataset_used_for_regions="fiducial",
    plot_settings=None,
):
    """
    Function to plot the ZAMS mass to remnant mass with extra ppisn mass removal
    """

    # Set up figure
    fig = plt.figure(figsize=(20, 12))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=8, ncols=1)

    ax = fig.add_subplot(gs[:, 0])

    # Call function to add the plot to the axis
    fig, ax = add_co_core_schematic_to_axis(
        fig=fig,
        ax=ax,
        result_directory_variation=result_directory_variation,
        old_prescription_result_directory_root=old_prescription_result_directory_root,
        no_prescription_result_directory_root=no_prescription_result_directory_root,
        variation_name=variation_name,
        add_no_prescription_line=add_no_prescription_line,
        add_old_prescription_line=add_old_prescription_line,
        add_fiducial_line=add_fiducial_line,
        add_variation_line=add_variation_line,
        add_bracket_extra_mass_loss=add_bracket_extra_mass_loss,
        create_shaded_regions=create_shaded_regions,
        create_supernova_lines=create_supernova_lines,
        dataset_used_for_regions=dataset_used_for_regions,
    )

    # Override the limits if we have input:
    if plot_settings.get("min_xlim", None) is not None:
        ax.set_xlim([plot_settings["min_xlim"], ax.get_xlim()[1]])
    if plot_settings.get("max_xlim", None) is not None:
        ax.set_xlim([ax.get_xlim()[0], plot_settings["max_xlim"]])
    if plot_settings.get("min_ylim", None) is not None:
        ax.set_ylim([plot_settings["min_ylim"], ax.get_ylim()[1]])
    if plot_settings.get("max_ylim", None) is not None:
        ax.set_ylim([ax.get_ylim()[0], plot_settings["max_ylim"]])

    # Handle the plotting
    show_and_save_plot(fig, plot_settings)
