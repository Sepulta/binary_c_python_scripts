"""
Script containin the plot function

TODO: use the binsize calculator of the ensembles
"""

import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from ensemble_functions.ensemble_functions import generate_bins_from_keys

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def plot_min_max_first_bh_mass(datafile, plot_settings=None):
    """
    Function to plot the grid in metallicity and mass
    """

    # Open the data as a pandas dataframe
    df = pd.read_csv(datafile, header=0, sep="\s+")
    df["log10metallicity"] = np.log10(df["metallicity"])

    # Select bh only
    df = df[df.stellar_type == 14]

    # Filter out effective_metallicity == 0:
    df = df[df.effective_metallicity > 0]

    other_columns = [
        col for col in df.columns if not col in ["zams_mass", "metallicity"]
    ]

    # make plot
    unique_zams_mass_values = df["zams_mass"].sort_values().unique()
    unique_metallicity_values = df["metallicity"].sort_values().unique()
    unique_log10unique_metallicity_values = np.log10(unique_metallicity_values)

    # Check if there are metallicity values for which not all of the zams masses are registered/successfully evolved
    for met in unique_metallicity_values:
        # Find missing zams values
        cur_zams_values = set(df[df.metallicity == met]["zams_mass"])
        missing_zams_values = set(unique_zams_mass_values) - cur_zams_values

        # Create placeholder data
        if missing_zams_values:
            dummy_lines = []
            for zams_value in missing_zams_values:
                dummy_line = {
                    "metallicity": met,
                    "zams_mass": zams_value,
                    **{col: np.nan for col in other_columns},
                }
                dummy_lines.append(dummy_line)

            # Create dataframe
            placeholder_df = pd.DataFrame(dummy_lines)

            # Add dataframe to total dataframe
            df = pd.concat([df, placeholder_df], ignore_index=True)

    # It is possible that there are duplicate lines in the data. Somehow, not sure how, but apparently so.
    df = df.drop_duplicates()

    # Make sure the data is sorted again
    df = df.sort_values(by=["zams_mass", "log10metallicity"])

    # Get all max bh masses for each metallicity
    max_masses = df.groupby(["metallicity"])["mass"].max()
    first_masses = df.groupby(["metallicity"])["mass"].first()

    ####
    #
    fig = plt.figure(figsize=(20, 20))

    #
    gs = fig.add_gridspec(nrows=1, ncols=12)

    # create axes
    ax = fig.add_subplot(gs[:, :])

    # #
    ax.plot(max_masses.index, max_masses.values, label="Max BH mass")
    ax.plot(first_masses.index, first_masses.values, label="First BH mass")

    # Some make up
    ax.legend()
    ax.set_xscale("log")
    ax.set_title("Max and first bh mass over metallicity")
    ax.set_xlabel("log10 Metallicity (Z)")
    ax.set_ylabel(r"Mass [M$_{\odot}$]")

    # Handle saving
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #
    plot_min_max_first_bh_mass(
        "../../output/output_vanilla/data/output.dat", plot_settings={"show_plot": True}
    )
