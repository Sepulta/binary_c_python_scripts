"""
Function to compare the total intrinsic rate for the systems
"""

import h5py

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors


import numpy as np

from david_phd_functions.plotting.utils import (
    add_plot_info,
    show_and_save_plot,
    align_axes,
)
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

#
from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.plot_functions.plot_mass_quantity_over_redshift import (
    get_data_plot_mass_quantity_over_redshift as get_data_compas,
)
from grav_waves.convolution.functions.plot_routines.plot_convolved_rate_density_per_mass_quantity import (
    get_data as get_data_custom,
)

from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    linestyle_list,
)

matplotlib.use("tkAgg")


def compare_total_intrinsic_rate_density_over_redshift(
    compas_filename,
    custom_filename,
    mass_quantity,
    bins,
    include_compas_plot=True,
    include_custom_plot=True,
    plot_settings={},
):
    """
    Function to compare the mass quantity over redshift for the two different convolution methods
    """

    DIFF_MAX_THRESHOLD = 3

    #
    add_contours = False

    #######
    # Set up figure
    fig = plt.figure(figsize=(30, 20))

    #
    gs = fig.add_gridspec(nrows=2, ncols=11)

    # Create axes
    ax_compas = fig.add_subplot(gs[0, 0:10])
    ax_custom = fig.add_subplot(gs[1, 0:10])

    #####################
    # COMPAS
    if include_compas_plot:
        #####################
        # Get compas data
        x_data, y_data, x_bins, weights = get_data_compas(
            compas_filename, mass_quantity
        )

        # calculate the 2-d histogram and find the max value
        pre_hist = np.histogram2d(x_data, y_data, bins=[x_bins, bins], weights=weights)

        bincenters = (x_bins[1:] + x_bins[:-1]) / 2
        total_intrinsic_rate = np.sum(pre_hist[0], axis=1)

        # Plot the data
        ax_compas.plot(bincenters, total_intrinsic_rate)
        ax_compas.set_yscale("log")
        ax_compas.set_xlim(0, 8)
        ax_compas.set_ylim(
            [
                total_intrinsic_rate[bincenters <= 8].min(),
                total_intrinsic_rate[bincenters <= 8].max(),
            ]
        )

    #####################
    # CUSTOM
    if include_custom_plot:
        #####################
        # Get custom data
        results_dict = {}
        results_dict["all"] = get_data_custom(
            hdf5_filename=custom_filename,
            dco_type="bhbh",
            mass_bins=bins,
            mass_quantity=mass_quantity,
            rate_key_in_df="merger_rates",
            rate_type_hdf5_key="merger_rate_data",
            divide_by_binsize=False,
            num_procs=8,
        )

        # total intrinsic rate
        total_intrinsic_rate = np.sum(results_dict["all"]["total_rate_array"], axis=1)
        redshifts = results_dict["all"]["total_redshift_array"]

        # Plot the data
        ax_custom.plot(redshifts, total_intrinsic_rate)
        ax_custom.set_yscale("log")
        ax_custom.set_xlim(0, 8)

        ax_custom.set_ylim(
            [
                total_intrinsic_rate[redshifts <= 8].min(),
                total_intrinsic_rate[redshifts <= 8].max(),
            ]
        )

    ##########
    # Make up

    # Align axes
    align_axes(fig=fig, axes_list=[ax_compas, ax_custom], which_axis="x")

    # Add labels
    ax_compas.set_ylabel(r"rate Intrinsic [dN/dGpc3/dYr]")
    ax_custom.set_ylabel(r"rate Intrinsic [dN/dGpc3/dYr]")

    ax_custom.set_xlabel("Redshift [z]")

    #
    ax_compas.set_title("COMPAS convolution")
    ax_custom.set_title("CUSTOM convolution")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    ## Setting
    sim_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/"
    dco_type = "combined_dco"

    # compas convolution
    compas_dir_convolution = os.path.join(
        sim_dir, "convolution_results_compas_structure"
    )
    compas_filename_convolution = os.path.join(
        compas_dir_convolution, "convolved_{}_compas_structure.hdf5".format(dco_type)
    )

    # custom convolution
    custom_filename_convolution = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/without_detection_probability/rebinned_convolution_results.h5"

    #
    show_plots = False
    include_custom_plots = True

    #
    compare_total_intrinsic_rate_density_over_redshift(
        compas_filename_convolution,
        custom_filename_convolution,
        mass_quantity="primary_mass",
        bins=np.arange(0, 70, 1),
        include_custom_plot=include_custom_plots,
        plot_settings={
            "show_plot": show_plots,
            "hspace": 0.5,
            "runname": "with probability weighting in compas",
        },
    )

else:
    matplotlib.use("Agg")
