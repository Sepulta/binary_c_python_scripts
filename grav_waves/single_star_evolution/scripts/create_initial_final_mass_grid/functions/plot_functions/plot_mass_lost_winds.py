"""
Script containin the plot function

TODO: use the binsize calculator of the ensembles
"""

import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from ensemble_functions.ensemble_functions import generate_bins_from_keys

from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.utility_functions import (
    fill_missing_values,
    SN_type_dict,
)

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def plot_mass_lost_winds(datafile, custom_colorlimits=None, plot_settings=None):
    """
    Function to plot the grid in metallicity and mass
    """

    # contourlevels = [22]
    # linestyles = ['solid', 'dashed', '-.']

    # Open the data as a pandas dataframe
    df = pd.read_csv(datafile, header=0, sep="\s+")
    df["log10metallicity"] = np.log10(df["metallicity"])
    df = df.sort_values(by=["zams_mass", "log10metallicity"])
    other_columns = [
        col for col in df.columns if not col in ["zams_mass", "metallicity"]
    ]

    # Filter out effective_metallicity == 0:
    df = df[df.effective_metallicity > 0]

    # Fill missing values
    df = fill_missing_values(df, other_columns)

    unique_zams_mass_values = df["zams_mass"].sort_values().unique()
    unique_metallicity_values = df["metallicity"].sort_values().unique()
    unique_log10unique_metallicity_values = np.log10(unique_metallicity_values)

    df["mass_lost_wind"] = df["zams_mass"] - df["pre_sn_mass"]

    z_vals = (
        df["mass_lost_wind"]
        .to_numpy()
        .reshape(
            len(unique_zams_mass_values), len(unique_log10unique_metallicity_values)
        )
    )

    #
    if not custom_colorlimits is None:
        norm = colors.Normalize(vmin=custom_colorlimits[0], vmax=custom_colorlimits[1])
    else:
        norm = colors.Normalize(
            vmin=z_vals[~np.isnan(z_vals)].min(), vmax=z_vals[~np.isnan(z_vals)].max()
        )

    ####
    #
    fig = plt.figure(figsize=(20, 20))

    #
    gs = fig.add_gridspec(nrows=1, ncols=12)

    # create axes
    ax = fig.add_subplot(gs[:, :10])
    ax_cb = fig.add_subplot(gs[:, 11])

    # Set up the colormesh plot
    colormesh = ax.pcolormesh(
        unique_metallicity_values,
        unique_zams_mass_values,
        z_vals,
        norm=norm,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        norm=norm,
    )
    cbar.ax.set_ylabel(r"Fraction of ZAMS mass")

    #
    sn_type_vals = (
        df["sn_type"]
        .to_numpy()
        .reshape(
            len(unique_zams_mass_values), len(unique_log10unique_metallicity_values)
        )
    )

    #
    cs = ax.contour(
        unique_metallicity_values,
        unique_zams_mass_values,
        sn_type_vals,
        levels=[20, 21, 22],
        colors="red",
        linewidths=0.5,
    )

    # #
    # contourlevels = [0.1, 0.2, 0.3, 0.4]
    # linestyles = list(matplotlib.lines.lineStyles.keys())
    # _ = ax.contour(
    #     unique_metallicity_values,
    #     unique_zams_mass_values,
    #     z_vals,
    #     levels=contourlevels,
    #     linestyles=linestyles,
    #     colors='k',
    # )
    # for contour_i, _ in enumerate(contourlevels):
    #     cbar.ax.plot([cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]], [contourlevels[contour_i]]*2, 'black', linestyle=linestyles[contour_i])

    # Set x-scale to log
    ax.set_xscale("log")
    ax.set_yscale(plot_settings["mass_scale"])

    ax.set_xlabel("log10 Metallicity (Z)")
    ax.set_ylabel(r"ZAMS mass [M$_{\odot}$]")

    ax.set_title("Mass lost through winds")

    # Handle saving
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #
    plot_mass_lost_winds(
        "../../output/output_vanilla/data/output.dat", plot_settings={"show_plot": True}
    )
