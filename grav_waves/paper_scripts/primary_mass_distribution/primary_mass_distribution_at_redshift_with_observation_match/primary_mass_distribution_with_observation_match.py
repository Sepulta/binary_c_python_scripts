import os
import numpy as np

from grav_waves.paper_scripts.primary_mass_distribution.main_plot_function import (
    generate_primary_mass_at_redshift_zero_plot,
)


if __name__ == "__main__":
    #########
    #
    data_root = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"), "GRAV_WAVES/server_results/"
    )

    #########
    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"),
        "paper_gw/paper_tex/figures/primary_mass_distribution_at_redshift_zero_with_extra_2d_plot",
    )
    plot_dir = "plots"

    # configuration:
    primary_mass_bins = np.arange(0, 100, 2)
    divide_by_binsize = True
    add_rates_plots = True
    add_kde = False
    add_comparison_to_observations = True
    bootstraps = 2
    show_plot = False
    general_query = None

    #######
    # Fiducial

    #
    convolved_datasets = {
        # Fiducial and variations
        "Fiducial (SEMI RES)": os.path.join(
            data_root,
            "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
        ),
        "Fiducial (SEMI mid RES)": os.path.join(
            data_root,
            "EVENTS_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
        ),
    }

    #
    basename = "test.pdf"
    generate_primary_mass_at_redshift_zero_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_comparison_to_observations=add_comparison_to_observations,
        bootstraps=bootstraps,
        general_query=general_query,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 32,
        },
    )
    quit()

    #######
    # Fiducial

    #
    convolved_datasets = {
        # Fiducial and variations
        "Fiducial (SEMI RES)": os.path.join(
            data_root,
            "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
        ),
        "Fiducial (SEMI mid RES)": os.path.join(
            data_root,
            "EVENTS_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
        ),
    }

    #
    basename = "fiducial_with_eccentricity_second_axis.pdf"
    general_query = None
    # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    generate_primary_mass_at_redshift_zero_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_comparison_to_observations=add_comparison_to_observations,
        bootstraps=bootstraps,
        general_query=general_query,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 32,
        },
    )
