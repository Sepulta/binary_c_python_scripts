"""
Function to run the convolution (including building the combined_dataset, filtering the dataset and running the convolution and rebinning)
"""

import os
import numpy as np

from grav_waves.settings import convolution_settings, config_dict_cosmology
from grav_waves.gw_analysis.functions.functions import make_dataset_dict

from grav_waves.convolution.functions.convolution_functions import convolution_main
from grav_waves.convolution.functions.convolution_create_combined_dataset import (
    create_combined_dataset,
)
from grav_waves.convolution.functions.convolution_rebin_redshift_functions import (
    convolution_rebin_redshift,
)
from grav_waves.convolution.functions.convolution_filter_combined_dataset_on_merging_systems import (
    filter_combined_dataset_on_merging_systems,
)

#
this_file = os.path.abspath(__file__)


def run_convolution_function(
    main_dir,
    convolution_output_dir,
    convolution_settings,
    cosmology_settings,
    run_combine_datasets=False,
    run_filter_combined_dataset_on_merging_systems=True,
    run_convolution=True,
    run_rebinning=True,
    remove_original_after_rebinning=False,
    rebinned_stepsize=0.05,
    verbosity=1,
):
    """
    Function to run the convolution and handle the all the things
    """

    # Get the correct directories
    dataset_dict_populations = make_dataset_dict(main_dir)
    os.makedirs(convolution_output_dir, exist_ok=True)

    # Create names for the output files
    combined_dataset_filename = os.path.join(
        convolution_output_dir, "combined_dataset.h5"
    )
    combined_dataset_merging_systems_filename = os.path.join(
        convolution_output_dir, "combined_dataset_merging_systems.h5"
    )
    redshift_convolution_filename = os.path.join(
        convolution_output_dir, "convolution_results.h5"
    )
    rebinned_redshift_convolution_filename = os.path.join(
        convolution_output_dir, "rebinned_convolution_results.h5"
    )

    ##############
    # Build input dataset
    if run_combine_datasets:
        create_combined_dataset(
            dataset_dict=dataset_dict_populations,
            convolution_configuration=convolution_settings,
            cosmology_configuration=cosmology_settings,
            output_filename=combined_dataset_filename,
            dco_type=convolution_settings["dco_type"],
            limit_merging_time=False,
        )

    ##############
    # Build combined dataset merging systems
    if run_filter_combined_dataset_on_merging_systems:
        filter_combined_dataset_on_merging_systems(
            input_hdf5file=combined_dataset_filename,
            output_hdf5file=combined_dataset_merging_systems_filename,
        )

    ##############
    # Run convolution
    if run_convolution:
        convolution_main(
            input_filename=combined_dataset_merging_systems_filename,
            convolution_configuration=convolution_settings,
            cosmology_configuration=cosmology_settings,
            output_filename=redshift_convolution_filename,
            verbosity=verbosity,
        )

    ##############
    # Do the re-binning
    if run_rebinning:
        # Set new bins
        new_bins = np.arange(
            0 - 0.5 * rebinned_stepsize,
            (convolution_settings["max_interpolation_redshift"] - 1)
            + 0.5 * rebinned_stepsize,
            rebinned_stepsize,
        )

        #
        convolution_rebin_redshift(
            input_dataset_filename=redshift_convolution_filename,
            output_dataset_filename=rebinned_redshift_convolution_filename,
            new_redshift_bins=new_bins,
        )

        #
        if remove_original_after_rebinning:
            os.remove(redshift_convolution_filename)


#
if __name__ == "__main__":
    this_file = os.path.abspath(__file__)
    this_file_dir = os.path.dirname(this_file)

    ##
    #
    output_root = os.path.join(this_file_dir)

    #######################
    # Select simulation

    # Fiducial
    main_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED"
    convolution_output_dir = os.path.join(
        output_root, "output/output_high_res/convolution/"
    )
    convolution_output_dir = os.path.join(output_root, "test/")

    # # 10 extra mass loss
    # main_dir = '/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_10_EXTRA_MASSLOSS_PPISN'
    # convolution_output_dir = os.path.join(output_root, 'output/output_high_res_ten_massloss/convolution/')

    # # 5 core mass shift
    # main_dir = '/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-5_CORE_MASS_SHIFT'
    # convolution_output_dir = os.path.join(output_root, 'output/output_high_res_five_coreshift/convolution/')

    # # multiprocessing test
    # main_dir = '/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-5_CORE_MASS_SHIFT'
    # # main_dir = '/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/MID_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW'
    # convolution_output_dir = os.path.join(output_root, 'output/array_test/convolution/')

    #
    convolution_stepsize = 0.001
    convolution_settings["stepsize"] = convolution_stepsize
    convolution_settings["time_bins"] = np.arange(
        1e-6 - 0.5 * convolution_stepsize,
        (convolution_settings["max_interpolation_redshift"] - 1)
        + 0.5 * convolution_stepsize,
        convolution_stepsize,
    )
    convolution_settings["time_centers"] = (
        convolution_settings["time_bins"][1:] + convolution_settings["time_bins"][:-1]
    ) / 2
    convolution_settings["add_detection_probability"] = False
    convolution_settings["include_formation_rates"] = False
    convolution_settings["amt_cores"] = 1
    convolution_settings["convolution_tmp_dir"] = os.path.join(
        convolution_output_dir, "tmp"
    )
    convolution_settings[
        "global_query"
    ] = "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0)"

    #
    run_convolution_function(
        main_dir,
        convolution_output_dir,
        convolution_settings=convolution_settings,
        cosmology_settings=config_dict_cosmology,
        run_combine_datasets=True,
        run_filter_combined_dataset_on_merging_systems=True,
        run_convolution=False,
        run_rebinning=False,
        rebinned_stepsize=0.05,
        verbosity=0,
    )
