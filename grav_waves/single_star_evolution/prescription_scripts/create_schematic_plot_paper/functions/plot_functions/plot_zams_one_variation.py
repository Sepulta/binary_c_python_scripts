"""
Function to plot the zams: mass removal and core mass shift schematic
"""

import matplotlib as mpl
import matplotlib.pyplot as plt

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.add_zams_variation_to_axis import (
    add_zams_variation_to_axis,
)

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_utils import (
    add_label_subplot,
    handle_overriding_xlim,
    align_axes,
    add_ax_invisible,
    add_labels_subplots,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def plot_zams_one_variation(
    variation_name,
    result_directory_variation,
    old_prescription_result_directory_root,
    extra_variation_configuration={},
    plot_settings=None,
):
    """
    Function to plot the ZAMS mass to remnant mass with extra ppisn mass removal and min co_core mass shift
    """

    # Set up figure
    fig = plt.figure(figsize=(16, 12))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=8, ncols=1)
    ax = fig.add_subplot(gs[:, 0])

    #######
    # Add mass removal plot
    fig, ax = add_zams_variation_to_axis(
        fig=fig,
        ax=ax,
        result_directory_variation=result_directory_variation,
        old_prescription_result_directory_root=old_prescription_result_directory_root,
        #
        variation_name=variation_name,
        #
        **extra_variation_configuration,
    )

    # Updated the axes limits if user provides update
    handle_overriding_xlim(
        fig=fig,
        axes_list=[ax],
        plot_settings=plot_settings,
    )

    # Handle the plotting
    show_and_save_plot(fig, plot_settings)
