"""
Function to plot the paper plot for the primary mass at zero redshift comparison between variations

TODO: improve the re-sampling; use replacement? use weights? see https://maxhalford.github.io/blog/weighted-sampling-without-replacement/
"""

import os

import numpy as np

import pandas as pd

import matplotlib.pyplot as plt
from matplotlib import colors

from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    extract_columns_from_querylist,
    quantity_name_dict,
    quantity_unit_dict,
    rate_type_name_dict,
    rate_density_units,
    add_columns_to_df,
    dco_type_query,
    readout_rate_array_specific_keys,
    create_centers_from_bins,
    get_closest_redshift_key,
    get_histogram_data,
    get_KDE_data,
    return_1d_plot_canvas,
    plot_1d_results,
    hatch_list,
    linestyle_list,
)
from grav_waves.convolution.functions.new_plot_routines.plot_kde_and_bootstrap_functions import (
    run_bootstrap,
)

from grav_waves.scripts.add_confidence_interval_powerlaw_peak.functions import (
    add_confidence_interval_powerlaw_peak_primary_mass,
)

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

from grav_waves.paper_scripts.primary_mass_distribution.primary_mass_distribution_at_redshift_with_variations.functions import (
    handle_columns,
    get_mask,
    get_rate_data,
)


def get_median_percentiles(value_array):
    """
    Function to get the median and the percentiles from the data
    """

    result_dict = {}

    result_dict["median"] = np.percentile(value_array, [50], axis=0)

    result_dict["90%_CI"] = np.percentile(value_array, [5, 95], axis=0)

    # result_dict["1_sigma"] = np.percentile(value_array, [15.89, 84.1], axis=0)

    # result_dict["2_sigma"] = np.percentile(value_array, [2.27, 97.725], axis=0)

    return result_dict


def get_data(
    filename,
    rate_type,
    dco_type,
    quantity,
    quantity_bins,
    divide_by_binsize,
    redshift_value,
    second_quantity=None,
    second_quantity_bins=None,
    add_kde=False,
    bootstraps=0,
    num_procs=1,
    general_query=None,
    querylist=None,
):
    """
    Function to read out the data from the datafile
    """

    ####
    # Read out dataframe
    combined_df = pd.read_hdf(filename, "/data/combined_dataframes")

    # Add columns
    handle_columns(
        combined_df,
        quantity,
        querylist,
        extra_columns=["primary_undergone_ppisn", second_quantity],
    )

    # Create masks and apply, taking into account that the query and general query can be None
    mask = get_mask(combined_df, dco_type, general_query, query=None)

    ###########
    # Get the quantity data from the dataframe
    quantity_data = combined_df[quantity]

    # mask the quantity data
    masked_quantity_data = quantity_data[mask].to_numpy()

    # Get second quantity
    if second_quantity:
        # Get the quantity data from the dataframe
        second_quantity_data = combined_df[second_quantity]

        # mask the quantity data
        second_masked_quantity_data = second_quantity_data[mask].to_numpy()

    ################
    # Read out rate data for this specific redshift
    rate_array = get_rate_data(filename, rate_type, redshift_value, mask)

    ################
    # Calculate the rate histogram
    hist, bincenter, truncated_bins = get_histogram_data(
        bins=quantity_bins, data_array=masked_quantity_data, weight_array=rate_array[0]
    )

    # Divide by binsize
    if divide_by_binsize:
        hist = hist / np.diff(quantity_bins)

    #
    rates_return_dict = {
        "centers": bincenter,
        "rates": hist,
    }

    # bootstrap
    if bootstraps:
        # Get a list of indices
        indices = np.arange(len(masked_quantity_data))

        # Set up array that stores all the boostrapped results
        bootstrapped_rates_hist_vals = np.zeros(
            (bootstraps, len(rates_return_dict["centers"]))
        )  # center_bins

        #
        for bootstrap_i in range(bootstraps):
            print("Bootstrap {}".format(bootstrap_i))
            ##############################
            # Get bootstrap indices
            boot_index = np.random.choice(
                indices,
                size=len(indices),
                replace=True,
                # p=rate_array[0][indices] / np.sum(rate_array[0][indices]),
            )

            # Select the quantity values with these indices
            bootstrapped_masked_quantity_data = masked_quantity_data[boot_index]

            # Select the rate values with these indices
            bootstrapped_rate_array = rate_array[:, boot_index]

            ##############################
            # Calculate the rate histogram
            (bootstrapped_hist, _, _,) = get_histogram_data(
                bins=quantity_bins,
                data_array=bootstrapped_masked_quantity_data,
                weight_array=bootstrapped_rate_array[0],
            )

            # Store unfiltered rate in array
            bootstrapped_rates_hist_vals[bootstrap_i] = bootstrapped_hist

        ###########
        # Calculate median and percentiles
        bootstrapped_median_percentiles_dict = get_median_percentiles(
            bootstrapped_rates_hist_vals
        )
        rates_return_dict["median_percentiles"] = bootstrapped_median_percentiles_dict

    ################
    # Calculate the KDE data
    if add_kde:
        # Calculate KDE for the rates
        y_vals, center_KDEbins, kde_width = get_KDE_data(
            bins=truncated_bins,
            hist=hist,
            data_array=masked_quantity_data,
            weight_array=rate_array[0],
        )

        #
        rates_return_dict["kde_yvals"] = y_vals
        rates_return_dict["kde_bins"] = center_KDEbins

        ################
        # Calculate the bootstrap data
        if bootstraps:
            # TODO: Ask lieke why do we use x_KDE here instead of center_KDEbins
            _, median, percentiles = run_bootstrap(
                masses=masked_quantity_data,
                weights=rate_array[0],
                indices=np.arange(len(masked_quantity_data)),
                kde_width=kde_width,
                mass_bins=truncated_bins,
                center_KDEbins=center_KDEbins,
                num_procs=num_procs,
                bootstraps=bootstraps,
            )

            #
            rates_return_dict["median"] = median
            rates_return_dict["percentiles"] = percentiles

    #
    return_dict = {
        "rates_results": rates_return_dict,
    }

    ################
    # Calculate second 2d plot histogram
    if second_quantity:

        # create histogram
        (second_quantity_2d_hist, _, _) = np.histogram2d(
            masked_quantity_data,
            second_masked_quantity_data,
            bins=[quantity_bins, second_quantity_bins],
            weights=rate_array[0],
        )

        # Divide by the rates array
        second_quantity_2d_hist = (
            second_quantity_2d_hist
            / np.sum(second_quantity_2d_hist, axis=1)[:, np.newaxis]
        )

        #
        secondary_2d_plot_return_dict = {
            "rates": second_quantity_2d_hist,
        }

        #
        return_dict["secondary_2d_plot_return_dict"] = secondary_2d_plot_return_dict

    return return_dict


def generate_primary_mass_at_redshift_zero_comparison_plot(
    convolved_datasets,
    primary_mass_bins,
    divide_by_binsize,
    add_rates_plots=True,
    add_secondary_2d_plot=True,
    second_quantity=None,
    second_quantity_bins=None,
    add_kde=False,
    bootstraps=0,
    general_query=None,
    plot_settings={},
):
    """
    Function to plot the paper plot for the primary mass at zero redshift comparison between variations
    """

    cmap = plt.cm.get_cmap("viridis", len(convolved_datasets) - 1)
    color_list = [cmap(i) for i in range(cmap.N)]

    # Add other color type at the end
    color_list.append("orange")

    #####################################
    # Calculate the results
    result_dict_rates = {}
    for convolved_dataset_i, (
        convolved_dataset_key,
        convolved_dataset_filename,
    ) in enumerate(convolved_datasets.items()):

        result_dict_rates[convolved_dataset_key] = get_data(
            filename=convolved_dataset_filename,
            rate_type="merger_rate",
            dco_type="bhbh",
            quantity="primary_mass",
            quantity_bins=primary_mass_bins,
            divide_by_binsize=divide_by_binsize,
            redshift_value=0,
            second_quantity=second_quantity,
            second_quantity_bins=second_quantity_bins,
            add_kde=add_kde,
            bootstraps=bootstraps,
            general_query=general_query,
        )

    # Settings
    linewidth = 5
    fig_scale = 15
    gs_scale = 1
    levels = (2 * add_rates_plots) + (2 * add_secondary_2d_plot)
    level = 0

    # Set up figure logic
    fig = plt.figure(figsize=(1.5 * fig_scale, 1 * fig_scale))
    gs = fig.add_gridspec(nrows=levels * gs_scale, ncols=1)
    axes_list = []

    # Add rates to the plot
    if add_rates_plots:
        # set up the subplot
        rates_axis = fig.add_subplot(gs[level * gs_scale : (level + 2) * gs_scale, 0])
        level += 2
        axes_list.append(rates_axis)

        ylabel = "$\\frac{d\\mathcal{R}}{dm_{1}}$ [Gpc$^{-3}$yr$^{-1}M_{\\odot}^{-1}$]"  # ligo plot

        rates_axis.set_ylabel(
            ylabel,
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

        # Add results to plot
        for result_i, (result_name, result) in enumerate(result_dict_rates.items()):
            if (not add_kde) and (not bootstraps):
                # Plot histograms
                rates_axis.plot(
                    result["rates_results"]["centers"],
                    result["rates_results"]["rates"],
                    lw=linewidth,
                    c=color_list[result_i],
                    zorder=200,
                    alpha=1,
                    linestyle=linestyle_list[result_i],
                    label=result_name,
                )

            # Plot KDEs
            if add_kde:
                rates_axis.plot(
                    result["rates_results"]["kde_bins"],
                    result["rates_results"]["kde_yvals"],
                    lw=linewidth,
                    c=color_list[result_i],
                    zorder=13,
                )

                # Plot median and bootstrap
                rates_axis.plot(
                    result["rates_results"]["centers"],
                    result["rates_results"]["median_percentiles"]["median"][0],
                    lw=linewidth,
                    c=color_list[result_i],
                    zorder=13,
                )

                rates_axis.fill_between(
                    result["rates_results"]["centers"],
                    result["rates_results"]["median_percentiles"]["90%_CI"][0],
                    result["rates_results"]["median_percentiles"]["90%_CI"][1],
                    alpha=0.4,
                    zorder=11,
                    color=color_list[result_i],
                )  # 90%-CI

            else:
                if bootstraps:
                    # Plot median and bootstrap
                    rates_axis.plot(
                        result["rates_results"]["centers"],
                        result["rates_results"]["median_percentiles"]["median"][0],
                        lw=linewidth,
                        c=color_list[result_i],
                        zorder=13,
                        linestyle=linestyle_list[result_i],
                        label=result_name,
                    )

                    #
                    rates_axis.fill_between(
                        result["rates_results"]["centers"],
                        result["rates_results"]["median_percentiles"]["90%_CI"][0],
                        result["rates_results"]["median_percentiles"]["90%_CI"][1],
                        alpha=0.4,
                        zorder=11,
                        color=color_list[result_i],
                    )  # 1-sigma

        # set ylims
        rates_axis.set_ylim([1e-3, 1e1])

        # Make up
        rates_axis.set_yscale("log")

        # Add fraction of primary underwent PPISN
        if add_secondary_2d_plot:
            secondary_2d_plot_axis = fig.add_subplot(
                gs[level * gs_scale : (level + 1) * gs_scale, 0]
            )
            level += 1
            axes_list.append(secondary_2d_plot_axis)

            #
            secondary_2d_plot_axis.set_ylabel(
                "{} [{}]".format(
                    quantity_name_dict[second_quantity],
                    quantity_unit_dict[second_quantity].to_string("latex"),
                ),
                fontsize=plot_settings.get("axislabel_fontsize", 26),
            )

            secondary_2d_plot_data = result["secondary_2d_plot_return_dict"]["rates"]

            norm = colors.LogNorm(
                vmin=1e-2,
                vmax=1e3,
            )

            norm = colors.Normalize(vmin=0, vmax=1)

            quantity_bins_centers = result["rates_results"]["centers"]
            second_quantity_bins_centers = (
                second_quantity_bins[1:] + second_quantity_bins[:-1]
            ) / 2

            quantity_bins_centers_X, second_quantity_bins_centers_Y = np.meshgrid(
                quantity_bins_centers, second_quantity_bins_centers
            )

            _ = secondary_2d_plot_axis.pcolormesh(
                quantity_bins_centers_X,
                second_quantity_bins_centers_Y,
                secondary_2d_plot_data.T,
                norm=norm,
                shading="auto",
                antialiased=plot_settings.get("antialiased", True),
                rasterized=plot_settings.get("rasterized", True),
            )

    ####
    # Add confidence interval
    fig, axes_list[0] = add_confidence_interval_powerlaw_peak_primary_mass(
        fig=fig,
        ax=axes_list[0],
        data_root=os.path.join(os.environ["DATAFILES_ROOT"], "GW"),
    )

    # Remove x-ticklabels
    for axes in axes_list[:-1]:
        axes.set_xticklabels([])

    # Extra makeup
    axes_list[-1].set_xlabel(
        "{} [{}]".format(
            quantity_name_dict["primary_mass"],
            quantity_unit_dict["primary_mass"].to_string("latex"),
        ),
        fontsize=plot_settings.get("axislabel_fontsize", 26),
    )
    axes_list[0].legend(fontsize=plot_settings.get("legend_fontsize", 26))

    for axes in axes_list:
        axes.set_xlim(2, 100)

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #########
    #
    data_root = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"), "GRAV_WAVES/server_results/"
    )

    #########
    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"),
        "paper_gw/paper_tex/figures/primary_mass_distribution_at_redshift_zero_with_extra_2d_plot",
    )
    plot_dir = "plots"

    # configuration:
    primary_mass_bins = np.arange(0, 100, 2)
    divide_by_binsize = True
    add_rates_plots = True
    add_secondary_2d_plot = True
    add_kde = False
    bootstraps = 50
    show_plot = False
    second_quantity = "eccentricity"
    second_quantity_bins_stepsize = 0.05
    second_quantity_bins = np.arange(
        0, 1 + second_quantity_bins_stepsize, second_quantity_bins_stepsize
    )

    # #######
    # # Fiducial

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_eccentricity_second_axis.pdf"
    # general_query = None
    # # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Without CE

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial without CE systems (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_eccentricity_second_axis_without_CE.pdf"
    # general_query = None
    # general_query = "comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # CE only

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial CE only (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_eccentricity_second_axis_CE_only.pdf"
    # general_query = None
    # general_query = "comenv_counter > 0 & stable_rlof_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Without stable MT

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial without stable MT systems (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_eccentricity_second_axis_without_stable_MT.pdf"
    # general_query = None
    # general_query = "stable_rlof_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Stable MT only

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial stable MT only (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_eccentricity_second_axis_stable_MT_only.pdf"
    # general_query = None
    # general_query = "stable_rlof_counter > 0 & comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Without any MT

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial without any MT systems (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_eccentricity_second_axis_without_any_MT.pdf"
    # general_query = None
    # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    #######
    # lower res with 0 kick variation

    #
    convolved_datasets = {
        # Fiducial and variations
        # "Fiducial max mass=150(SEMI RES)": os.path.join(
        #     data_root,
        #     "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
        # ),
        r"Fiducial max mass=150 $\sigma_{\mathrm{kick}} = 0$ (MID SEMI RES)": os.path.join(
            data_root,
            "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_sn_kick_dispersion_II_0sn_kick_dispersion_IBC_0/dco_convolution_results/convolution_results.h5",
        ),
    }

    #
    general_query = None
    basename = "low_res_zero_kick_variation_eccentricity_second_axis.pdf"
    # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    generate_primary_mass_at_redshift_zero_comparison_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_secondary_2d_plot=add_secondary_2d_plot,
        second_quantity=second_quantity,
        second_quantity_bins=second_quantity_bins,
        add_kde=add_kde,
        bootstraps=bootstraps,
        general_query=general_query,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 24,
        },
    )

    #######
    # Fiducial

    #
    convolved_datasets = {
        # Fiducial and variations
        "Fiducial (HIGH RES)": os.path.join(
            data_root,
            "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
        ),
    }

    #
    basename = "low_res_fiducial_with_eccentricity_second_axis.pdf"
    general_query = None
    # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    generate_primary_mass_at_redshift_zero_comparison_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_secondary_2d_plot=add_secondary_2d_plot,
        second_quantity=second_quantity,
        second_quantity_bins=second_quantity_bins,
        add_kde=add_kde,
        bootstraps=bootstraps,
        general_query=general_query,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 32,
        },
    )

    ############################
    #
    second_quantity = "mass_ratio"
    second_quantity_bins_stepsize = 0.05
    second_quantity_bins = np.arange(
        0, 1 + second_quantity_bins_stepsize, second_quantity_bins_stepsize
    )

    # #######
    # # Fiducial

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_massratio_second_axis.pdf"
    # general_query = None
    # # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Without CE

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial without CE systems (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_massratio_second_axis_without_CE.pdf"
    # general_query = None
    # general_query = "comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # CE only

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial CE only (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_massratio_second_axis_CE_only.pdf"
    # general_query = None
    # general_query = "comenv_counter > 0 & stable_rlof_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Without stable MT

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial without stable MT systems (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_massratio_second_axis_without_stable_MT.pdf"
    # general_query = None
    # general_query = "stable_rlof_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Stable MT only

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial stable MT systems only (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_massratio_second_axis_stable_MT_only.pdf"
    # general_query = None
    # general_query = "stable_rlof_counter > 0 & comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    # #######
    # # Without any MT

    # #
    # convolved_datasets = {
    #     # Fiducial and variations
    #     # "Fiducial (SEMI RES)": os.path.join(
    #     #     data_root,
    #     #     "SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
    #     # ),
    #     "Fiducial without any MT systems (HIGH RES)": os.path.join(
    #         data_root,
    #         "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
    #     ),
    # }

    # #
    # basename = "fiducial_with_massratio_second_axis_without_any_MT.pdf"
    # general_query = None
    # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    # generate_primary_mass_at_redshift_zero_comparison_plot(
    #     convolved_datasets=convolved_datasets,
    #     primary_mass_bins=primary_mass_bins,
    #     divide_by_binsize=divide_by_binsize,
    #     add_rates_plots=add_rates_plots,
    #     add_secondary_2d_plot=add_secondary_2d_plot,
    #     second_quantity=second_quantity,
    #     second_quantity_bins=second_quantity_bins,
    #     add_kde=add_kde,
    #     bootstraps=bootstraps,
    #     general_query=general_query,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "hspace": 0,
    #         "axislabel_fontsize": 32,
    #         "legend_fontsize": 32,
    #     },
    # )

    #######
    # lower res with 0 kick variation

    #
    convolved_datasets = {
        # Fiducial and variations
        # "Fiducial max mass=150(SEMI RES)": os.path.join(
        #     data_root,
        #     "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
        # ),
        r"Fiducial max mass=150 $\sigma_{\mathrm{kick}} = 0$ (MID SEMI RES)": os.path.join(
            data_root,
            "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150_sn_kick_dispersion_II_0sn_kick_dispersion_IBC_0/dco_convolution_results/convolution_results.h5",
        ),
    }

    #
    basename = "low_res_zero_kick_variation_massratio_second_axis.pdf"
    # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    generate_primary_mass_at_redshift_zero_comparison_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_secondary_2d_plot=add_secondary_2d_plot,
        second_quantity=second_quantity,
        second_quantity_bins=second_quantity_bins,
        add_kde=add_kde,
        bootstraps=bootstraps,
        # general_query=general_query,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 24,
        },
    )

    #######
    # Fiducial

    #
    convolved_datasets = {
        # Fiducial and variations
        "Fiducial (HIGH RES)": os.path.join(
            data_root,
            "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
        ),
    }

    #
    basename = "low_res_fiducial_with_massratio_second_axis.pdf"
    general_query = None
    # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    generate_primary_mass_at_redshift_zero_comparison_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_secondary_2d_plot=add_secondary_2d_plot,
        second_quantity=second_quantity,
        second_quantity_bins=second_quantity_bins,
        add_kde=add_kde,
        bootstraps=bootstraps,
        general_query=general_query,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 32,
        },
    )
