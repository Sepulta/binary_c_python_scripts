import os

import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

from binarycpython.utils.grid import Population

from ensemble_functions.ensemble_functions import (
    return_float_list_of_dictkeys,
    generate_bins_from_keys,
)

"""
TODO: clean up the function to read out the dataframe and the columns
TODO: automate selecting 2 columns, grouping things, creating bins/bincenters, generate histogram (! excluding the ones that have 0 probability)
"""

run_normal_distributions = False
run_ms_distributions = True

# ###########
# # high res settings
# num_cores = 20
# metallicity_values = [0.02, 0.01, 0.002]
# resolution = {'M_1': 100, 'q': 100, 'per': 100, "e": 0}
# res_string = "HIGH_RES"

# ###########
# # mid res settings
# num_cores = 20
# metallicity_values = [0.02, 0.01, 0.002]
# resolution = {'M_1': 40, 'q': 40, 'per': 80, "e": 0}
# res_string = "MID_RES"

###########
# low res settings
num_cores = 8
metallicity_values = [0.02, 0.01, 0.002]
resolution = {"M_1": 25, "q": 25, "per": 50, "e": 0}
res_string = "LOW_RES"

# ###########
# # test res settings
# resolution = {'M_1': 5, 'q': 5, 'per': 5, "e": 0}
# res_string = "TEST_RES"

############
# Set up the normal population that uses a manual grid
if run_normal_distributions:
    ## Make population and set value
    normal_pop = Population(verbosity=1)

    ######
    # Set distributions
    normal_pop.add_grid_variable(
        name="lnm1",
        longname="Primary mass",
        valuerange=[0.1, 100],
        samplerfunc="const(math.log(0.1), math.log(100), {})".format(resolution["M_1"]),
        precode="M_1=math.exp(lnm1)",
        probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 101, -1.3, -2.3, -2.3)*M_1",
        dphasevol="dlnm1",
        parameter_name="M_1",
        condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
    )

    normal_pop.add_grid_variable(
        name="q",
        longname="Mass ratio",
        valuerange=["0.1/M_1", 1],
        samplerfunc="const(0.1/M_1, 1, {})".format(resolution["q"]),
        probdist="flatsections(q, [{'min': 0.1/M_1, 'max': 1.0, 'height': 1}])",
        dphasevol="dq",
        precode="M_2 = q * M_1",
        parameter_name="M_2",
        condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
    )

    normal_pop.add_grid_variable(
        name="log10per",  # in days
        longname="log10(Orbital_Period)",
        valuerange=[0.15, 5.5],
        samplerfunc="const(0.15, 5.5, {})".format(resolution["per"]),
        precode="""orbital_period = 10** log10per
sep = calc_sep_from_period(M_1, M_2, orbital_period)
sep_min = calc_sep_from_period(M_1, M_2, 10**0.15)
sep_max = calc_sep_from_period(M_1, M_2, 10**5.5)""",
        probdist="sana12(M_1, M_2, sep, orbital_period, sep_min, sep_max, math.log10(10**0.15), math.log10(10**5.5), -0.55)",
        parameter_name="orbital_period",
        dphasevol="dlog10per",
    )

    normal_pop.write_binary_c_calls_to_file(
        "distributions", "normal_distributions_raw.dat"
    )

    # open the file, read out the content, store the
    headers = []
    data = []
    with open(os.path.join("distributions", "normal_distributions_raw.dat"), "r") as f:
        for line in f:
            stripped_line = line.strip().replace("binary_c ", "")

            # split
            split_line = stripped_line.split()

            # select the headers
            headers = split_line[::2]

            # select the values
            values = [float(el) for el in split_line[1::2]]

            #
            data.append(values)

    # Put in dataframe
    df = pd.DataFrame(data, columns=headers)

    #
    df["q"] = df["M_2"] / df["M_1"]
    df["log10M_1"] = np.log10(df["M_1"])
    df["log10per"] = np.log10(df["orbital_period"])
    del df["M_1"]
    del df["M_2"]
    del df["orbital_period"]

    df = df.reindex(columns=["log10M_1", "q", "log10per", "probability"])

    grouped = df.groupby(["log10M_1", "log10per"], as_index=False)["probability"].sum()

    key_list_m1 = sorted(grouped["log10M_1"].unique())
    bins_m1 = generate_bins_from_keys(np.array(key_list_m1))
    bincenters_m1 = (bins_m1[1:] + bins_m1[:-1]) / 2

    key_list_per = sorted(grouped["log10per"].unique())
    bins_per = generate_bins_from_keys(np.array(key_list_per))
    bincenters_per = (bins_per[1:] + bins_per[:-1]) / 2

    #
    hist, _, _ = np.histogram2d(
        grouped["log10M_1"].to_numpy(),
        grouped["log10per"].to_numpy(),
        bins=[bins_m1, bins_per],
        weights=grouped["probability"].to_numpy(),
    )
    min_val = hist.min()
    max_val = hist.max()

    fig, axes = plt.subplots(nrows=1, ncols=1)

    # Set up the colormesh plot
    X, Y = np.meshgrid(bincenters_m1, bincenters_per)
    hist_disk = axes.pcolormesh(
        X,
        Y,
        hist.T,
        norm=colors.LogNorm(vmin=min_val, vmax=max_val),
        shading="auto",
        antialiased=True,
        rasterized=True,
    )
    plt.show()

#############
# Set up M&S pop

if run_ms_distributions:
    # ## Make population and set value
    # ms_pop = Population(verbosity=1)

    # # Set distributions
    # ms_pop.Moe_di_Stefano_2017(
    #     options={
    #         "multiplicity_modulator": [0, 1, 0, 0],
    #         "resolutions":
    #             {
    #                 "M": [resolution['M_1'], resolution['q'], 0, 0],
    #                 "logP": [resolution['per'], 0, 0],
    #                 "ecc": [resolution['e'], 0, 0]
    #             },
    #     }
    # )

    # ms_pop.write_binary_c_calls_to_file(
    #     'distributions',
    #     'ms_distributions.dat')

    # open the file, read out the content, store the
    headers = []
    data = []
    with open(os.path.join("distributions", "ms_distributions.dat"), "r") as f:
        for line in f:
            stripped_line = line.strip().replace("binary_c ", "")

            # split
            split_line = stripped_line.split()

            # select the headers
            headers = split_line[::2]

            # select the values
            values = [float(el) for el in split_line[1::2]]
            print(values)
            #
            data.append(values)

    # Put in dataframe
    df = pd.DataFrame(data, columns=headers)

    #
    df["q"] = df["M_2"] / df["M_1"]
    df["log10M_1"] = np.log10(df["M_1"])
    df["log10per"] = np.log10(df["orbital_period"])
    del df["M_1"]
    del df["M_2"]
    del df["orbital_period"]

    df = df.reindex(columns=["log10M_1", "q", "log10per", "probability"])

    grouped = df.groupby(["log10M_1", "log10per"], as_index=False)["probability"].sum()
    grouped = df.groupby(["log10M_1"], as_index=False)["probability"].sum()
    print(grouped)
    quit()

    key_list_m1 = sorted(grouped["log10M_1"].unique())
    bins_m1 = generate_bins_from_keys(np.array(key_list_m1))
    bincenters_m1 = (bins_m1[1:] + bins_m1[:-1]) / 2

    key_list_per = sorted(grouped["log10per"].unique())
    bins_per = generate_bins_from_keys(np.array(key_list_per))
    bincenters_per = (bins_per[1:] + bins_per[:-1]) / 2

    #
    hist, _, _ = np.histogram2d(
        grouped["log10M_1"].to_numpy(),
        grouped["log10per"].to_numpy(),
        bins=[bins_m1, bins_per],
        weights=grouped["probability"].to_numpy(),
    )
    min_val = hist.min()
    max_val = hist.max()

    fig, axes = plt.subplots(nrows=1, ncols=1)

    # Set up the colormesh plot
    X, Y = np.meshgrid(bincenters_m1, bincenters_per)
    hist_disk = axes.pcolormesh(
        X,
        Y,
        hist.T,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )
    plt.show()
