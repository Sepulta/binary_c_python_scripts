"""
Function to inflate an ensemble (without the )

TODO: check why the final columns is not filled
"""

import os
import time
import json
import pandas as pd

from grav_waves.settings import convolution_settings
from RLOF.scripts.plot_ensemble_output.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)
from RLOF.run_populations.ensemble_population_and_rlof_episodes.functions import (
    split_off_ensemble_projects,
    convert_ensemble_to_dataframe,
)


simulation_dir = (
    "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_SANA"
)
testing = False

# ensemble_output_name = 'results/output_ensemble.json'
# inflated_ensemble_output_name = 'results/inflated_output_ensemble.json'

#
probability_to_yield_conversion_factor = (
    convolution_settings["binary_fraction"]
    * convolution_settings["average_mass_system"]
)


f = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_SANA/population_results/Z0.02/ensemble_output_e464b411fa8d4a2f8eb69c5755152ab5_0.json"
project_outputnames = split_off_ensemble_projects(f)

for project_outputname in project_outputnames:
    convert_ensemble_to_dataframe(project_outputname)


#     dirname = os.path.dirname(ensemble_filename)
#     basename = os.path.basename(ensemble_filename)

#     # Open ensemble file
#     ensemble_data = json.load(open(ensemble_filename, 'r'))

#     # Convert to dataframe
#     print("Converting ensemble data to dataframe"); start=time.time()
#     columnames = find_columnames_recursively(ensemble_data)
#     data_list = inflate_ensemble_with_lists(ensemble_data)

#     df = pd.DataFrame(data_list)
#     df = df.transpose()
#     df.columns = columnames + ['probability']
#     df['number_per_formed_solarmass'] = df['probability'] * probability_to_yield_conversion_factor
#     print("\tTook {}s".format(time.time()-start))

#     # write to file
#     print("Writing dataframe to file"); start=time.time()
#     df.to_csv(inflated_ensemble_output_name, sep='\t', header=True, index=False)
#     print("\tTook {}s".format(time.time()-start))

#     # reloading for timing purposes
#     print("reloading dataframe"); start=time.time()
#     df = pd.read_csv(inflated_ensemble_output_name, header=0, sep='\s+')
#     print("\tTook {}s".format(time.time()-start))


#     # reloading for timing purposes
#     print("reloading ensemble"); start=time.time()
#     with open(ensemble_output_name, 'r') as f:
#         ensemble_data = json.loads(f.read())
#     print("\tTook {}s".format(time.time()-start))
