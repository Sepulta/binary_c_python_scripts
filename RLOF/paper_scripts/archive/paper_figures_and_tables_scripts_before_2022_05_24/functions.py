"""
Some functionality to deal with getting the correct datasets
"""

import os
import sys
import json
import shutil
import tempfile
import subprocess

import psutil

from binarycpython.utils.dicts import multiply_values_dict


def copy_to_paper_directory_function(
    local_directory, target_directory, base_filename, verbose=0
):
    """
    Function to copy file to paper directory
    """

    source_filepath = os.path.join(local_directory, base_filename)
    target_filepath = os.path.join(target_directory, base_filename)

    if not os.path.isdir(target_directory):
        if verbose:
            print(
                "\tTarget filepath {} does not exist. Creating now".format(
                    target_directory
                )
            )
        os.makedirs(target_directory, exist_ok=True)

    #
    shutil.copy(source_filepath, target_filepath)
    if verbose:
        print("\tCopied {} to {}".format(source_filepath, target_filepath))


def get_username():
    return psutil.Process().username()


def create_tmp_dir_table_compilation():
    """
    Function to create a tmp directory for the table compilation

    returns the path to the tmp dir
    """

    username = get_username()
    root_tmp_dir = os.path.join(
        tempfile.gettempdir(), "python-{}-RLOF-pdfs".format(username)
    )
    os.makedirs(root_tmp_dir, exist_ok=True)
    current_path = tempfile.mkdtemp(dir=root_tmp_dir)

    return current_path


def wrap_table_tex(input_file, output_file):
    """
    function to wrap the contents of the input file with a standard tex documentclass wrapper
    """

    wrapper_tex = """\documentclass{{article}}
\\usepackage{{booktabs}}
\\begin{{document}}

{CONTENT_INPUT_FILE}

\end{{document}}
"""

    content_input_file = open(input_file, "r").read()

    wrapped_tex_string = wrapper_tex.format(CONTENT_INPUT_FILE=content_input_file)

    with open(output_file, "w") as f:
        f.write(wrapped_tex_string)

    return output_file


def create_table_pdf(full_path_table):
    """
    Function to take a .tex table and compile it into a pdf and return the full path of the pdf
    """

    # Get info of the current table
    full_path_tex = os.path.abspath(full_path_table)
    basename_tex = os.path.basename(full_path_tex)

    # Create tmp paths
    tmp_dir = create_tmp_dir_table_compilation()
    tmp_path_tex = os.path.join(tmp_dir, basename_tex)
    tmp_path_main_tex = os.path.join(tmp_dir, "main.tex")

    # copy the table to a tmp path
    shutil.copy(full_path_tex, tmp_path_tex)

    # Wrap the table in documentclass etc
    wrap_table_tex(tmp_path_tex, tmp_path_main_tex)

    script_workdir = os.getcwd()

    # Compile with latex
    os.chdir(tmp_dir)

    p = subprocess.run(
        ["pdflatex", "{}".format(tmp_path_main_tex)],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout = p.stdout  # stdout = normal output
    stderr = p.stderr  # stderr = error output

    if p.returncode != 0:
        print("Something went wrong when compiling the table:")
        print(stderr.decode("utf-8"))
        print("Aborting")
        sys.exit(-1)

    os.chdir(script_workdir)

    # Check if the pdf is created
    pdfname = tmp_path_main_tex.replace(".tex", ".pdf")
    if os.path.isfile(pdfname):
        print("Succesfully created table pdf")
    else:
        raise ValueError("PDF not created")

    # Copy pdf back to the directory
    full_path_target_pdfname = full_path_tex.replace(".tex", ".pdf")
    shutil.copy(pdfname, full_path_target_pdfname)

    if os.path.isfile(full_path_target_pdfname):
        print("Succesfully copied pdf")
    else:
        raise ValueError("PDF not created")

    return full_path_target_pdfname
