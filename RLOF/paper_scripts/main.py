"""
Main function to generate the plots for the disks paper
"""

import os

from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict

# Episode plot functions
from RLOF.paper_scripts.functions.episode_plot_functions.plot_fraction_stream_excess_disk_angmom_initial_angmom_stable_mt_onto_ms import (
    plot_fraction_stream_excess_disk_angmom_initial_angmom_stable_mt_onto_ms,
)


def generate_paper_plots_and_tables(output_root, verbose=0):
    """
    Function to generate the plots and tables
    """

    ####
    # Set up episodal data
    simulation_dir = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions"
    rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(simulation_dir)
    episodal_dataset_dict = rlof_episode_dataset_dict["datasets"]

    # Call functions to generate plots with the episode data

    # Plt with stream excess transferred back to orbit
    base_name_fraction_stream_excess_disk_angmom_initial_angmom_stable_mt_onto_ms = (
        "fraction_stream_excess_disk_angmom_initial_angmom_stable_mt_onto_ms.pdf"
    )
    plot_fraction_stream_excess_disk_angmom_initial_angmom_stable_mt_onto_ms(
        episodal_dataset_dict,
        add_cdf=True,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                output_root,
                "episode_plots",
                base_name_fraction_stream_excess_disk_angmom_initial_angmom_stable_mt_onto_ms,
            ),
        },
    )


if __name__ == "__main__":
    plot_output_root = os.path.join(
        "/home/david/papers/paper_disks/paper_tex/figures_new"
    )
    plot_output_root = os.path.join(os.getcwd(), "output/")

    generate_paper_plots_and_tables(output_root=plot_output_root)
