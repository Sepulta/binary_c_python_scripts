"""
General function to plot datasets manually
"""

import pickle
from general_plot_function import general_plot_routine
from plot_functions import plot_all_rates_and_metallicity_distribution

#
pickled_result_dict = pickle.load(open("test.p", "rb"))
metallicity_weighted_starformation_array = pickled_result_dict[
    "metallicity_weighted_starformation_array"
].T

# plot_all_rates_and_metallicity_distribution(pickled_result_dict, detailed_plot=True, plot_settings={})
# plot_all_rates_and_metallicity_distribution(pickled_result_dict, detailed_plot=False, plot_settings={'show_plot': True})

general_plot_routine(pickled_result_dict, main_plot_dir="plots/test/new_convolution")


# # ################
# # # Local results

# local_result_files = [
#     # "results/local/sfr_convolution_result_server_data.p"
#     'results/local/sfr_convolution_comparison_dummy.p'
# ]

# #
# for result_file in local_result_files:
#     pickled_result_dict = pickle.load(open(result_file, "rb" ))

#     general_plot_routine(pickled_result_dict, main_plot_dir='plots/local/comparison_dummy')

# # # ################
# # # Server results
# # server_result_files = [
# #     "results/server/sfr_convolution_result_server_data.p"
# # ]

# # #
# # for result_file in server_result_files:
# #     pickled_result_dict = pickle.load(open(result_file, "rb" ))

# #     general_plot_routine(pickled_result_dict, main_plot_dir='plots/server/')
