"""
Function to test the general plot routine with new plots
"""

import os

from grav_waves.convolution.functions.general_plot_function import (
    general_plot_routine as general_sfr_convolution_plot_function,
)


ppisn_query_dict = {
    "no_ppisn": "undergone_ppisn_1==0 & undergone_ppisn_2==0",
    "one_ppisn": "(undergone_ppisn_1==0 & undergone_ppisn_2==1) | (undergone_ppisn_1==1 & undergone_ppisn_2==0)",
    "two_ppisn": "undergone_ppisn_1==1 & undergone_ppisn_2==1",
    "any_ppisn": "undergone_ppisn_1==1 | undergone_ppisn_2==1",
}


output_dataset_filename = "../hdf5_output_rewrite/output/convolution_without_detector_probability_results/rebinned_convolution_without_detection_probability.h5"
convolution_plot_dir = "output/"
divide_by_binsize = False
querylist = [
    {
        "name": "CE channel",
        "query": "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter >= 1)",
    },
    {
        "name": "No CE channel",
        "query": "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter==0)",
    },
]
# querylist = [
#     {'name': 'No PPISN', 'query': '(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & (undergone_ppisn_1==0 & undergone_ppisn_2==0))'},
#     {'name': 'One PPISN', 'query': '(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & ((undergone_ppisn_1==0 & undergone_ppisn_2==1) | (undergone_ppisn_1==1 & undergone_ppisn_2==0)))'},
#     {'name': 'Two PPISN', 'query': '(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & (undergone_ppisn_1==1 & undergone_ppisn_2==1))'}
# ]
convolution_without_detector_plot_output_pdf = "dataset_plots.pdf"

add_ratio = True

#
general_sfr_convolution_plot_function(
    output_dataset_filename,
    divide_by_binsize=divide_by_binsize,
    output_pdf_filename=convolution_without_detector_plot_output_pdf,
    main_plot_dir=os.path.join(convolution_plot_dir),
    querylist=querylist,
    add_ratio=add_ratio,
    include_starformation_rate_plots=False,
    include_plot_birthrate=False,
    include_merger_and_formation_rate_densities_over_redshift_plots=True,
    include_merger_and_formation_rate_densities_at_redshift_zero_plots=False,
    include_massratio_density_at_zero_redshift=True,
    include_convolved_rate_density_per_mass_ratio=True,
    include_total_intrinsic_merger_rates_over_redshift_plots=False,
    include_interpolation_error_plot=False,
)
