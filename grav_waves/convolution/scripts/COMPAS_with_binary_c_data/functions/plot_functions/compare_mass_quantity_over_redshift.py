"""
Function to compare the mass quantity over redshift
"""

import os
import h5py

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors


import numpy as np

from david_phd_functions.plotting.utils import (
    add_plot_info,
    show_and_save_plot,
    align_axes,
)
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

#
from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.plot_functions.plot_mass_quantity_over_redshift import (
    get_data_plot_mass_quantity_over_redshift as get_data_compas,
)
from grav_waves.convolution.functions.plot_routines.plot_convolved_rate_density_per_mass_quantity import (
    get_data as get_data_custom,
)

from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    linestyle_list,
)

matplotlib.use("tkAgg")


def compare_mass_quantity_over_redshift(
    compas_filename,
    custom_filename,
    mass_quantity,
    bins,
    include_compas_plot=True,
    include_custom_plot=True,
    plot_settings={},
):
    """
    Function to compare the mass quantity over redshift for the two different convolution methods
    """

    DIFF_MAX_THRESHOLD = 3

    #
    add_contours = True

    #######
    # Set up figure
    fig = plt.figure(figsize=(30, 20))

    #
    gs = fig.add_gridspec(nrows=2, ncols=11)

    # Create axes
    ax_compas = fig.add_subplot(gs[0, 0:10])
    ax_compas_cb = fig.add_subplot(gs[0, 10])

    ax_custom = fig.add_subplot(gs[1, 0:10])
    ax_custom_cb = fig.add_subplot(gs[1, 10])

    #####################
    # COMPAS
    if include_compas_plot:
        #####################
        # Get compas data
        x_data, y_data, x_bins, weights = get_data_compas(
            compas_filename, mass_quantity
        )

        # calculate the 2-d histogram and find the max value
        pre_hist = np.histogram2d(x_data, y_data, bins=[x_bins, bins], weights=weights)
        global_max_val = np.max(pre_hist[0])

        # Determine the norm
        norm = matplotlib.colors.LogNorm(
            vmin=10 ** (np.log10(global_max_val) - DIFF_MAX_THRESHOLD),
            vmax=global_max_val,
        )

        #####################
        # plot compas data

        # Plot density
        _ = ax_compas.hist2d(
            x_data, y_data, bins=[x_bins, bins], weights=weights, norm=norm
        )

        # make colorbar
        cbar = matplotlib.colorbar.ColorbarBase(
            ax_compas_cb, cmap=matplotlib.cm.viridis, norm=norm, extend="min"
        )
        cbar.ax.set_ylabel("rate Intrinsic [dN/dGpc3/dYr]")

        # Add contourlevels
        if add_contours:
            contourlevels = [
                10 ** (np.log10(global_max_val) - 3),
                10 ** (np.log10(global_max_val) - 2),
                10 ** (np.log10(global_max_val) - 1),
            ]

            x_bins_centers = (x_bins[1:] + x_bins[:-1]) / 2
            bins_centers = (bins[1:] + bins[:-1]) / 2

            _ = ax_compas.contour(
                x_bins_centers,
                bins_centers,
                pre_hist[0].T,
                levels=contourlevels,
                colors="k",
                linestyles=linestyle_list,
            )

            # Set lines on the colorbar
            for contour_i, _ in enumerate(contourlevels):
                cbar.ax.plot(
                    [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
                    [contourlevels[contour_i]] * 2,
                    "black",
                    linestyle=linestyle_list[contour_i],
                )

    #####################
    # CUSTOM
    if include_custom_plot:
        #####################
        # Get custom data
        results_dict = {}
        results_dict["all"] = get_data_custom(
            hdf5_filename=custom_filename,
            dco_type="bhbh",
            mass_bins=bins,
            mass_quantity=mass_quantity,
            rate_key_in_df="merger_rates",
            rate_type_hdf5_key="merger_rate_data",
            divide_by_binsize=False,
            num_procs=8,
        )

        #####################
        # plot custom data
        mass_centers = (bins[1:] + bins[:-1]) / 2
        X, Y = np.meshgrid(results_dict["all"]["total_redshift_array"], mass_centers)

        #
        max_val = results_dict["all"]["total_rate_array"].max()
        custom_min_val = 10 ** (np.log10(max_val) - DIFF_MAX_THRESHOLD)

        norm = colors.LogNorm(vmin=custom_min_val, vmax=max_val)

        # Plot the results
        ax_custom.pcolormesh(
            X,
            Y,
            results_dict["all"]["total_rate_array"].T,
            norm=norm,
            shading="auto",
            antialiased=plot_settings.get("antialiased", False),
            rasterized=plot_settings.get("rasterized", False),
        )

        # make colorbar
        cb = matplotlib.colorbar.ColorbarBase(ax_custom_cb, norm=norm, extend="min")

        # add contourlevels
        if add_contours:
            contourlevels = [
                10 ** (np.log10(max_val) - 3),
                10 ** (np.log10(max_val) - 2),
                10 ** (np.log10(max_val) - 1),
            ]
            _ = ax_custom.contour(
                X,
                Y,
                results_dict["all"]["total_rate_array"].T,
                levels=contourlevels,
                colors="k",
                linestyles=linestyle_list,
            )

            # Set lines on the colorbar
            for contour_i, _ in enumerate(contourlevels):
                cb.ax.plot(
                    [cb.ax.get_xlim()[0], cb.ax.get_xlim()[1]],
                    [contourlevels[contour_i]] * 2,
                    "black",
                    linestyle=linestyle_list[contour_i],
                )

    ##########
    # Make up

    # Align axes
    align_axes(fig=fig, axes_list=[ax_compas, ax_custom], which_axis="x")
    align_axes(fig=fig, axes_list=[ax_compas, ax_custom], which_axis="y")

    # Add labels
    ax_compas.set_ylabel(r"Primary mass [M$_{\odot}$]")
    ax_custom.set_ylabel(r"Primary mass [M$_{\odot}$]")

    ax_custom.set_xlabel("Redshift [z]")

    #
    ax_compas.set_title("COMPAS convolution")
    ax_custom.set_title("CUSTOM convolution")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    ## Setting
    sim_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/"
    dco_type = "combined_dco"

    # compas convolution
    compas_dir_convolution = os.path.join(
        sim_dir, "convolution_results_compas_structure"
    )
    compas_filename_convolution = os.path.join(
        compas_dir_convolution, "convolved_{}_compas_structure.hdf5".format(dco_type)
    )

    # custom convolution
    custom_filename_convolution = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/without_detection_probability/rebinned_convolution_results.h5"

    #
    show_plots = False
    include_custom_plots = True

    #
    compare_mass_quantity_over_redshift(
        compas_filename_convolution,
        custom_filename_convolution,
        mass_quantity="primary_mass",
        bins=np.arange(0, 70, 1),
        include_custom_plot=include_custom_plots,
        plot_settings={
            "show_plot": show_plots,
            "hspace": 0.5,
            "runname": "with probability weighting in compas",
        },
    )
else:
    matplotlib.use("Agg")
