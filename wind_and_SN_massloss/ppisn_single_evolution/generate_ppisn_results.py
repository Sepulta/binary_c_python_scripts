"""
Function that generates the output from binary_c to analyze where all the mass goes. it's a combination of some older scripts

Plots
- pre-supernova stellar type, post-sn stellar type,
"""

import os

import numpy as np

from binarycpython.utils.grid import Population

from david_phd_functions.binaryc.personal_defaults import personal_defaults

from custom_logging_string import custom_logging_string
from parse_function import parse_function

from plot import plot_whole_sequence

# Set up population
test_pop = Population()
test_pop.set(C_logging_code=custom_logging_string)

metallicity = 0.0002

# Load personal defaults
test_pop.set(**personal_defaults)

# Set resolution
resolution = {"M_1": 300}

# Add the mass in logspace
test_pop.add_grid_variable(
    name="lnm1",
    longname="Primary mass",
    valuerange=[0.8, 300],
    resolution="{}".format(resolution["M_1"]),
    spacingfunc="const(math.log(0.8), math.log(300), {})".format(resolution["M_1"]),
    precode="M_1=math.exp(lnm1)",
    probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 301, -1.3, -2.3, -2.3)*M_1",
    dphasevol="dlnm1",
    parameter_name="M_1",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
)

# # Add the mass: Zoomed in
# test_pop.add_grid_variable(
#     name="lnm1",
#     longname="Primary mass",
#     valuerange=[100, 150],
#     resolution="{}".format(resolution["M_1"]),
#     spacingfunc="const(math.log(100), math.log(150), {})".format(resolution["M_1"]),
#     precode="M_1=math.exp(lnm1)",
#     probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 301, -1.3, -2.3, -2.3)*M_1",
#     dphasevol="dlnm1",
#     parameter_name="M_1",
#     condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
# )

# Add situational settings
test_pop.set(
    separation=1000000000,
    orbital_period=400000000,
    M_2=0.08,
    metallicity=metallicity,
    verbosity=2,
    amt_cores=2,
    parse_function=parse_function,
    BH_prescription=4,
    wind_mass_loss=2,  # 0 for no wind mass loss. 1 for hurley 2002, 2 for schneider 2018. 3 for binaryc  2021
    save_pre_events_stardata=1,  # To catch the pre-event stuff
)

# Get version info of the binary_c build
version_info = test_pop.return_binary_c_version_info(parsed=True)

# Stop the script if the configuration is wrong
if version_info["macros"]["MINT"] == "on":
    print("Please disable MINT")
    quit()
if version_info["macros"]["NUCSYN"] == "on":
    print("Please disable NUCSYN")
    quit()

metallicity_values = 10 ** np.linspace(-4, -1.4, 14)[::-1]

# metallicity_values = [0.0001]

wind_mass_loss_dict = {
    1: "WIND_ALGORITHM_HURLEY2002",
    2: "WIND_ALGORITHM_SCHNEIDER2018",
    3: "WIND_ALGORITHM_BINARY_C_2020",
}
ppisn_dict = {
    # 0: "OFF",
    1: "FARMER19",
}

result_dir = os.path.abspath("results")
plot_dir = os.path.abspath("plots")

# Loop over metallicities
for metallicity in metallicity_values:
    wind_mass_loss_setting = 2
    ppisn_prescription = 1

    test_pop.set(wind_mass_loss=2)  # Set correct wind
    test_pop.set(PPISN_prescription=ppisn_prescription)

    simname = "{}_PPISN_{}".format(
        wind_mass_loss_dict[wind_mass_loss_setting], ppisn_dict[ppisn_prescription]
    )

    test_pop.set(
        metallicity=metallicity,
        data_dir=os.path.join(result_dir, simname, "Z{}".format(metallicity)),
        base_filename="output.dat",
    )

    # create local tmp_dir
    test_pop.set(
        tmp_dir=os.path.join(test_pop.custom_options["data_dir"], "local_tmp_dir")
    )
    os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

    # Remove pre-existing file
    filename = os.path.join(
        test_pop.custom_options["data_dir"], test_pop.custom_options["base_filename"]
    )
    if os.path.isfile(filename):
        os.remove(filename)

    # Export settings:
    test_pop.export_all_info(use_datadir=True)

    # Evolve grid
    test_pop.evolve()

# Plot all the stuff
plot_whole_sequence(
    result_dir=result_dir,
    plot_dir=plot_dir,
    sim_name=simname,
    create_pdf=True,
    display_log_metallicity=False,
)
