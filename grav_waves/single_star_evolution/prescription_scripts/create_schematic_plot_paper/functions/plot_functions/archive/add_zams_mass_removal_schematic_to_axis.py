import os
import numpy as np
import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.extra_plot_functions import (
    place_arrows,
    add_bracket,
)

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_utils import (
    add_pre_SN_mass_plot,
    add_no_prescription_plot,
    add_old_prescription_plot,
    add_fiducial_plot,
    add_mass_removal_plot,
    add_core_mass_shift_plot,
    get_df_bh,
    calculate_min_max_zams_ppisn_masses,
    add_makeup_plot,
    get_dataframes,
    add_supernova_lines,
    add_shaded_regions_plot,
    add_bracket_extra_mass_loss_plot,
)

from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def add_zams_mass_removal_schematic_to_axis(
    fig,
    ax,
    result_directory_variation,
    old_prescription_result_directory_root,
    no_prescription_result_directory_root=None,
    add_preSN_mass_mass_removal=True,
    add_no_prescription_line=False,
    add_old_prescription_line=True,
    add_fiducial_line=True,
    add_mass_removed_mass_removal=True,
    add_bracket_extra_mass_loss=True,
    create_shaded_regions=True,
    create_supernova_lines=True,
    dataset_used_for_regions="fiducial",
):
    """
    Function to add the ZAMS mass to remnant mass with extra ppisn mass removal plot to an axis
    """

    # Settings
    min_zams_mass = 0
    max_zams_mass = 250

    ########################
    # Get metallicity values
    metallicity_value = float(
        os.path.basename(result_directory_variation).split("_Z")[-1]
    )

    ########################
    # dataframe of different result sets

    ########################
    # Get the dataframes
    (
        old_prescription_df,
        fiducial_df,
        no_prescription_df,
        variation_df,
    ) = get_dataframes(
        old_prescription_result_directory_root,
        result_directory_variation,
        no_prescription_result_directory_root,
    )

    ########################
    # Get some masses
    (
        min_ppisn_zams,
        max_ppisn_zams,
        zams_mass_for_max_ppisn,
        fiducial_max_mass_ppisn,
        variation_max_mass_ppisn,
    ) = calculate_min_max_zams_ppisn_masses(fiducial_df, variation_df)

    #############################################
    # Plot masses

    ################
    # Pre-SN mass plot
    if add_preSN_mass_mass_removal:
        add_pre_SN_mass_plot(ax, fiducial_df)

    ################
    # No ppisn prescription plot
    if add_no_prescription_line:
        add_no_prescription_plot(ax, no_prescription_df)

    ################
    # old prescription mass plot
    if add_old_prescription_line:
        add_old_prescription_plot(ax, old_prescription_df)

    ################
    # fiducial mass plot
    if add_fiducial_line:
        add_fiducial_plot(ax, fiducial_df)

    ################
    # Mass removal plot
    if add_mass_removed_mass_removal:
        add_mass_removal_plot(ax, variation_df)

        ################
        # Add bracket indicating the extra PPISN loss
        if add_bracket_extra_mass_loss:
            add_bracket_extra_mass_loss_plot(
                fig,
                ax,
                zams_mass_for_max_ppisn,
                fiducial_max_mass_ppisn,
                variation_max_mass_ppisn,
            )

    ################
    # Create shaded region under the main line that indicates which prescription
    if create_shaded_regions:
        add_shaded_regions_plot(
            ax=ax,
            dataset_used_for_regions=dataset_used_for_regions,
            max_zams_mass=max_zams_mass,
            df_dict=df_dict,
        )

    ################
    # Add lines on the top to indicate which supernova type
    if create_supernova_lines:
        add_supernova_lines(fig, ax, fiducial_max_mass_ppisn, dataset_used_for_regions)

    # Add makeup to plot
    add_makeup_plot(
        ax,
        min_zams_mass,
        max_zams_mass,
        fiducial_max_mass_ppisn,
        metallicity_value,
    )

    return fig, ax
