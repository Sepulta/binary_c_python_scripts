"""
Function to plot the confidence interval of the GWTC03b detection for power law + peak model

Make sure the correct datasets are in place.
"""

import os

import numpy as np
import deepdish as dd

import matplotlib.pyplot as plt


def add_confidence_interval_powerlaw_peak_mass_ratio(
    fig, ax, data_root, label=None, fill_between_kwargs={}
):
    """
    Function to add the confidence interval for the powerlaw + peak model of the GWTC03b data release for the mass ratio distribution

    data_root has to contain the file "o1o2o3_mass_c_iid_mag_iid_tilt_powerlaw_redshift_mass_data.h5"
    """

    # Get file and set limits
    mass_PP_path = os.path.join(
        data_root, "o1o2o3_mass_c_iid_mag_iid_tilt_powerlaw_redshift_mass_data.h5"
    )
    limits = [5, 95]

    # Create mass grid
    mass_1 = np.linspace(2, 100, 1000)
    mass_ratio = np.linspace(0.1, 1, 500)

    # load in the traces.
    # Each entry in lines is p(m1 | Lambda_i) or p(q | Lambda_i)
    # where Lambda_i is a single draw from the hyperposterior
    # The ppd is a 2D object defined in m1 and q
    with open(mass_PP_path, "r") as _data:
        _data = dd.io.load(mass_PP_path)
        lines = _data["lines"]
        ppd = _data["ppd"]

    # marginalize over m1 to get the ppd in terms of q only

    mass_ratio_ppd = np.trapz(ppd, mass_1, axis=-1)

    ax.semilogy(
        mass_ratio,
        mass_ratio_ppd,
        label=label,
        alpha=0.75,
        **fill_between_kwargs,
    )
    ax.fill_between(
        mass_ratio,
        np.percentile(lines["mass_ratio"], limits[0], axis=0),
        np.percentile(lines["mass_ratio"], limits[1], axis=0),
        alpha=0.5,
        label=label,
        **fill_between_kwargs,
    )

    return fig, ax


def get_data_powerlaw_peak_primary_mass(data_root):
    """
    Routine to get the data for the powerlaw peak estimates
    """

    # Get file and set limits
    mass_PP_path = os.path.join(
        data_root, "o1o2o3_mass_c_iid_mag_iid_tilt_powerlaw_redshift_mass_data.h5"
    )
    limits = [5, 95]

    # Create mass grid
    mass_1 = np.linspace(2, 100, 1000)
    mass_ratio = np.linspace(0.1, 1, 500)

    # load in the traces.
    # Each entry in lines is p(m1 | Lambda_i) or p(q | Lambda_i)
    # where Lambda_i is a single draw from the hyperposterior
    # The ppd is a 2D object defined in m1 and q
    with open(mass_PP_path, "r") as _data:
        _data = dd.io.load(mass_PP_path)
        lines = _data["lines"]
        ppd = _data["ppd"]

    # marginalize over q to get the ppd in terms of m1 only
    mass_1_ppd = np.trapz(ppd, mass_ratio, axis=0)

    observation_1sigma = [
        np.percentile(lines["mass_1"], 16, axis=0),
        np.percentile(lines["mass_1"], 84, axis=0),
    ]

    return_dict = {
        "mass_1_centers": mass_1,
        "mass_1_mean_rate": mass_1_ppd,
        "mass_1": lines["mass_1"],
        "observation_1sigma": observation_1sigma,
    }

    return return_dict


def add_confidence_interval_powerlaw_peak_primary_mass(
    fig, ax, data_root, label=None, fill_between_kwargs={}
):
    """
    Function to add the confidence interval for the powerlaw + peak model of the GWTC03b data release for the primary mass distribution

    data_root has to contain the file "o1o2o3_mass_c_iid_mag_iid_tilt_powerlaw_redshift_mass_data.h5"
    """

    # Get file and set limits
    mass_PP_path = os.path.join(
        data_root, "o1o2o3_mass_c_iid_mag_iid_tilt_powerlaw_redshift_mass_data.h5"
    )
    limits = [5, 95]

    # Create mass grid
    mass_1 = np.linspace(2, 100, 1000)
    mass_ratio = np.linspace(0.1, 1, 500)

    # load in the traces.
    # Each entry in lines is p(m1 | Lambda_i) or p(q | Lambda_i)
    # where Lambda_i is a single draw from the hyperposterior
    # The ppd is a 2D object defined in m1 and q
    with open(mass_PP_path, "r") as _data:
        _data = dd.io.load(mass_PP_path)
        lines = _data["lines"]
        ppd = _data["ppd"]

    # marginalize over q to get the ppd in terms of m1 only
    mass_1_ppd = np.trapz(ppd, mass_ratio, axis=0)

    # plot the PPD as a solid line
    ax.semilogy(
        mass_1,
        mass_1_ppd,
        # label=label,
        alpha=0.75,
        **fill_between_kwargs,
    )

    # plot the CIs as a filled interval
    ax.fill_between(
        mass_1,
        np.percentile(lines["mass_1"], limits[0], axis=0),
        np.percentile(lines["mass_1"], limits[1], axis=0),
        alpha=0.5,
        # label=label,
        **fill_between_kwargs,
    )

    return fig, ax


if __name__ == "__main__":
    data_root = os.path.join(os.environ["DATAFILES_ROOT"], "GW")

    # ##################
    # # Plot primary mass distribution + CI

    # # Set up figure logic
    # fig = plt.figure(figsize=(18, 9))
    # gs = fig.add_gridspec(nrows=1, ncols=9)
    # ax = fig.add_subplot(gs[:, :])
    # fig, ax = add_confidence_interval_powerlaw_peak_primary_mass(
    #     fig,
    #     ax,
    #     data_root=data_root,
    #     label=None,
    #     fill_between_kwargs={"color": "#B8B8B8"},
    # )
    # ax.set_ylim(1e-3, 10)
    # ax.set_yscale("log")
    # ax.legend()
    # plt.show()

    ##################
    # Plot mass ratio + CI

    # Set up figure logic
    fig = plt.figure(figsize=(18, 9))
    gs = fig.add_gridspec(nrows=1, ncols=9)
    ax = fig.add_subplot(gs[:, :])
    fig, ax = add_confidence_interval_powerlaw_peak_mass_ratio(
        fig,
        ax,
        data_root=data_root,
        label=None,
        fill_between_kwargs={"color": "#B8B8B8"},
    )
    ax.set_ylim(1e-3, 10)
    ax.set_yscale("log")
    ax.legend()
    plt.show()
