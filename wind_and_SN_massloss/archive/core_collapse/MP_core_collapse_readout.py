import h5py
import json
import pandas as pd
import numpy as np
import os


import matplotlib.pyplot as plt
import seaborn as sns

# file_name = '/home/david/projects/binary_c_root/results/testing_python/supernova_test_mass/test_sn_matt_z0002.hdf5'

result_dir = os.environ["BINARYC_DATA_ROOT"]

#
file_name_001 = os.path.join(
    result_dir,
    "testing_python",
    "multiprocessing",
    "core_collapse_0.01",
    "cc_test.hdf5",
)
file_name_002 = os.path.join(
    result_dir,
    "testing_python",
    "multiprocessing",
    "core_collapse_0.02",
    "cc_test.hdf5",
)
file_name_0002 = os.path.join(
    result_dir,
    "testing_python",
    "multiprocessing",
    "core_collapse_0.002",
    "cc_test.hdf5",
)
file_name_0001 = os.path.join(
    result_dir,
    "testing_python",
    "multiprocessing",
    "core_collapse_0.001",
    "cc_test.hdf5",
)
file_name_00002 = os.path.join(
    result_dir,
    "testing_python",
    "multiprocessing",
    "core_collapse_0.0002",
    "cc_test.hdf5",
)
file_name_00001 = os.path.join(
    result_dir,
    "testing_python",
    "multiprocessing",
    "core_collapse_0.0001",
    "cc_test.hdf5",
)

#
def load_df(file_name):
    """
    function to load filename
    """

    hdf5_file = h5py.File(file_name, "r")
    data = hdf5_file["data"]["cc_test"][()]
    df = pd.DataFrame(data)
    df_headers = [
        header.decode("utf-8") for header in hdf5_file["data"]["cc_test_header"][()]
    ]
    df.columns = df_headers
    df["mass_lost_during_evolution"] = df["zams_mass_1"] - df["prev_mass_1"]
    df["mass_lost_during_supernova"] = df["prev_mass_1"] - df["mass_1"]
    hdf5_file.close()

    return df


df_002 = load_df(file_name_002)
df_001 = load_df(file_name_001)

df_0002 = load_df(file_name_0002)
df_0001 = load_df(file_name_0001)

print(df_0002.columns)

print(df_0002["stellar_type"])


# plt.plot(df_0002['zams_mass_1'], np.repeat(0.002, df_0002['zams_mass_1'].count()))


# https://seaborn.pydata.org/generated/seaborn.scatterplot.html
# https://tryolabs.com/blog/2017/03/16/pandas-seaborn-a-guide-to-handle-visualize-data-elegantly/
# https://www.datacamp.com/community/tutorials/seaborn-python-tutorial#show
# ax = sns.scatterplot(x="zams_mass_1", y=np.repeat(0.02, df_002['zams_mass_1'].count()), hue="stellar_type", data=df_002, edgecolor=None)
# ax2 = sns.scatterplot(x="zams_mass_1", y=np.repeat(0.01, df_001['zams_mass_1'].count()), hue="stellar_type", data=df_001, edgecolor=None)
# ax3 = sns.scatterplot(x="zams_mass_1", y=np.repeat(0.002, df_0002['zams_mass_1'].count()), hue="stellar_type", data=df_0002, edgecolor=None)
# ax4 = sns.scatterplot(x="zams_mass_1", y=np.repeat(0.001, df_0001['zams_mass_1'].count()), hue="stellar_type", data=df_0001, edgecolor=None)

# plt.show()


quit()

# Create a stack plot of where what mass is lost
x = df["zams_mass_1"]
y1 = df["mass_1"]
y2 = df["mass_lost_during_supernova"]
y3 = df["mass_lost_during_evolution"]
# y = np.vstack([y1, y2, y3])
labels = ["remnant mass ", "Mass lost during SN", "Mass lost during evolution"]
fig, ax = plt.subplots()
ax.stackplot(x, y1, y2, y3, labels=labels)
ax.legend(loc="upper left")
plt.show()


# Create plot showing when different kinds of supernova go off.
# Select differnt systems
df_sn2 = df[df["SN_type"] == 12]
df_sn1bc = df[df["SN_type"] == 11]

#
plt.plot(df_sn2["zams_mass_1"], df_sn2["time"], "bo", label="SN type 2")
plt.plot(df_sn1bc["zams_mass_1"], df_sn1bc["time"], "r-", label="SN type 1bc")
plt.title("Lifetime of star until supernova per mass at {}".format(metallicity))
plt.yscale("log")
plt.xlabel("Mass (solarmass)")
plt.ylabel("Time (Myr)")
plt.legend()
plt.show()
