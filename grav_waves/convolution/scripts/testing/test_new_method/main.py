"""
Convolution which is now based on the advice Lieke gave me; to do redshift smapling, move to lookabck time, subtract and the do back
"""

import os
import copy
import pickle
import numpy as np

from grav_waves.settings import config_dict_cosmology, convolution_settings
from grav_waves.convolution.functions.convolution_functions import new_convolution_main
from grav_waves.convolution.functions.general_plot_function import general_plot_routine

########
# Configure the settings for the datasets
# Load dataset
# from grav_waves.gw_analysis.server_datasets import dataset_dict
from grav_waves.gw_analysis.laptop_datasets import dataset_dict

# data info
data_info = {
    "dataset_dict": dataset_dict,
    "dataset_name": "HIGH_RES_SCHNEIDER_MASS_LOSS_PPISN_ON",
    "type_key": "bhbh",
    "main_result_dir": os.path.abspath("../../../gw_analysis/results"),
    "main_plot_dir": os.path.abspath("plots/"),
}

####################
# Configure settings for the cosmology settings we use
# Load the default cosmology configuration
cosmology_configuration = copy.deepcopy(config_dict_cosmology)
cosmology_configuration["metallicity_distribution_function"] = "coen19"

#########################
# Configure settings for the convolution settings we use
# Load the default convolution configuration
convolution_configuration = copy.deepcopy(convolution_settings)
convolution_configuration["amt_cores"] = 4

#
time_stepsize = 0.5

# do redshift loop
redshift_max_time = 9
convolution_configuration["time_type"] = "redshift"

# Set the time stuff in the convolution config
convolution_configuration["stepsize"] = time_stepsize
convolution_configuration["time_bins"] = np.arange(
    0.0001 - 0.5 * time_stepsize, redshift_max_time + 0.5 * time_stepsize, time_stepsize
)
convolution_configuration["time_centers"] = (
    convolution_configuration["time_bins"][1:]
    + convolution_configuration["time_bins"][:-1]
) / 2

convolution_configuration["min_loop_time"] = 0
convolution_configuration["max_loop_time"] = redshift_max_time

lookback_redshift_output_filename = "results/new_convolution_lookback_redshift.p"
new_convolution_main(
    data_info,
    convolution_configuration,
    cosmology_configuration,
    lookback_redshift_output_filename,
    # save_time_bins=save_time_bins,
    store_each_timestep=False,
)

# # Plot the results of the
pickled_result_dict_redshift = pickle.load(
    open(lookback_redshift_output_filename, "rb")
)
general_plot_routine(pickled_result_dict_redshift, main_plot_dir="plots/redshift")
