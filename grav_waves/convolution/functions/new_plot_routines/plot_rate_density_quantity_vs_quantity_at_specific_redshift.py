"""
Function to plot quantity vs quantity at specific redshift

2-d plot
"""

import h5py
import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    get_num_subsets,
    extract_columns_from_querylist,
    quantity_name_dict,
    quantity_unit_dict,
    rate_type_name_dict,
    rate_density_units,
    add_columns_to_df,
    dco_type_query,
    readout_rate_array_specific_keys,
    create_centers_from_bins,
    get_closest_redshift_key,
    plot_2d_results,
    plot_contourlevels,
    linestyle_list,
)
from grav_waves.convolution.functions.new_plot_routines.plot_kde_and_bootstrap_functions import (
    run_bootstrap,
)
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.canvas_functions import return_canvas_with_subsets
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def readout_rate_data_run_bootstraps(
    combined_df,
    rate_type,
    dco_type,
    x_quantity,
    x_quantity_bins,
    y_quantity,
    y_quantity_bins,
    redshift_value,
    general_query=None,
    query=None,
):
    """
    Function to read out the rate data and get the histogram data, the KDE data and the bootstrapped data
    """

    # Get the quantity data from the dataframe
    x_quantity_data = combined_df[x_quantity].to_numpy()
    y_quantity_data = combined_df[y_quantity].to_numpy()

    ################
    # Create masks and apply, taking into account that the query and general query can be None
    dco_mask = combined_df.eval(dco_type_query(dco_type)).to_numpy()
    general_query_mask = (
        combined_df.eval(general_query)
        if general_query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    query_mask = (
        combined_df.eval(query)
        if query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    mask = dco_mask * general_query_mask * query_mask

    # mask the quantity data
    masked_x_quantity_data = x_quantity_data[mask]
    masked_y_quantity_data = y_quantity_data[mask]

    ################
    # Read out rate data for this specific redshift
    datafile = h5py.File(filename)
    all_redshift_keys = sorted(list(datafile["data/{}".format(rate_type)].keys()))
    closest_redshift_key = get_closest_redshift_key(redshift_value, all_redshift_keys)
    rate_array, redshifts = readout_rate_array_specific_keys(
        datafile=datafile,
        rate_key=rate_type,
        mask=mask,
        redshift_keys=[closest_redshift_key],
    )
    datafile.close()

    ################
    # Create the 2-d hist
    hist, _, _ = np.histogram2d(
        masked_x_quantity_data,
        masked_y_quantity_data,
        bins=[x_quantity_bins, y_quantity_bins],
        weights=rate_array[0],
    )

    #
    return hist.T


def get_data(
    filename,
    rate_type,
    dco_type,
    x_quantity,
    x_quantity_bins,
    y_quantity,
    y_quantity_bins,
    divide_by_binsize,
    redshift_value,
    general_query=None,
    querylist=None,
):
    """
    Function to read out the data from the datafile
    """

    ####
    # Read out dataframe
    combined_df = pd.read_hdf(filename, "/data/combined_dataframes")

    # Add columns
    used_columns = [x_quantity, y_quantity] + extract_columns_from_querylist(
        querylist
    )  # TODO: add general query in here
    columns_to_add = [
        column for column in used_columns if not column in combined_df.columns
    ]
    combined_df = add_columns_to_df(combined_df, columns_to_add)

    # Create result dict
    result_dict = {}

    #
    result_dict["all"] = readout_rate_data_run_bootstraps(
        combined_df=combined_df,
        rate_type=rate_type,
        dco_type=dco_type,
        x_quantity=x_quantity,
        x_quantity_bins=x_quantity_bins,
        y_quantity=y_quantity,
        y_quantity_bins=y_quantity_bins,
        redshift_value=redshift_value,
        general_query=general_query,
        query=None,
    )

    # Loop over queries
    if querylist is not None:
        for query_dict in querylist:
            result_dict[query_dict["name"]] = readout_rate_data_run_bootstraps(
                combined_df=combined_df,
                rate_type=rate_type,
                dco_type=dco_type,
                x_quantity=x_quantity,
                x_quantity_bins=x_quantity_bins,
                y_quantity=y_quantity,
                y_quantity_bins=y_quantity_bins,
                redshift_value=redshift_value,
                general_query=general_query,
                query=query_dict["query"],
            )

    return result_dict


def plot_rate_density_quantity_at_specific_redshift(
    filename,
    rate_type,
    dco_type,
    x_quantity,
    x_quantity_bins,
    y_quantity,
    y_quantity_bins,
    redshift_value,
    querylist=None,
    x_scale="linear",
    y_scale="linear",
    verbose=0,
    base_querydict=None,
    add_ratio=False,
    plot_settings={},
):
    """
    Function to plot quantity vs redshift
    """

    #
    x_label = r"{} [{}]".format(
        quantity_name_dict[x_quantity],
        quantity_unit_dict[x_quantity].to_string("latex"),
    )
    y_label = r"{} [{}]".format(
        quantity_name_dict[y_quantity],
        quantity_unit_dict[y_quantity].to_string("latex"),
    )

    #
    contourlevels = [1e-1, 1e0, 1e1]
    ratio_contourlevels = [0.1, 0.5, 0.9]

    #
    x_quantity_bincenters = create_centers_from_bins(x_quantity_bins)
    y_quantity_bincenters = create_centers_from_bins(y_quantity_bins)

    #####################
    # Get results
    results_dict = get_data(
        filename=filename,
        rate_type=rate_type,
        dco_type=dco_type,
        x_quantity=x_quantity,
        x_quantity_bins=x_quantity_bins,
        y_quantity=y_quantity,
        y_quantity_bins=y_quantity_bins,
        redshift_value=redshift_value,
        divide_by_binsize=False,
        general_query=base_querydict["query"] if base_querydict is not None else None,
        querylist=querylist,
    )

    #
    max_val = results_dict["all"].max()
    custom_min_val = 10 ** (
        np.log10(max_val) - plot_settings.get("probability_floor", 4)
    )

    norm = colors.LogNorm(vmin=custom_min_val, vmax=max_val)

    # ####################
    # Plot data

    ##
    # Figure out the number of subplots
    num_subsets = get_num_subsets(querylist)

    ##
    # Set up axes
    fig = plt.figure(figsize=(40, 40))
    fig, _, axes_dict = return_canvas_with_subsets(
        num_subsets, fig=fig, add_ratio_axes=add_ratio
    )

    ##
    # Plot 2-d results
    X, Y = np.meshgrid(x_quantity_bincenters, y_quantity_bincenters)
    fig, axes_dict, cb, cb_ratio = plot_2d_results(
        fig=fig,
        axes_dict=axes_dict,
        X=X,
        Y=Y,
        x_label=x_label,
        y_label=y_label,
        add_ratio=add_ratio,
        results_dict=results_dict,
        querylist=querylist,
        plot_settings=plot_settings,
        x_scale=x_scale,
        y_scale=y_scale,
        norm=norm,
    )

    ##
    # Add contour levels
    fig, axes_dict = plot_contourlevels(
        results_dict=results_dict,
        X=X,
        Y=Y,
        fig=fig,
        axes_dict=axes_dict,
        cb=cb,
        cb_ratio=cb_ratio,
        contourlevels=contourlevels,
        ratio_contourlevels=ratio_contourlevels,
        linestyle_list=linestyle_list,
        querylist=querylist,
        add_ratio=add_ratio,
    )

    ###
    # Make up
    axes_dict["all_axis"].set_xlabel(x_label)
    axes_dict["all_axis"].set_ylabel(y_label)
    axes_dict["colorbar_axis"].set_ylabel(
        r"{} density [{}]".format(
            rate_type_name_dict[rate_type], rate_density_units.to_string("latex")
        )
    )

    #
    title_text = (
        "Intrinsic {} density of {} systems for {} vs {} at redshift {}".format(
            rate_type_name_dict[rate_type].lower(),
            dco_type.upper(),
            quantity_name_dict[x_quantity].lower(),
            quantity_name_dict[y_quantity].lower(),
            redshift_value,
        )
    )
    axes_dict["all_axis"].set_title(title_text, fontsize=26)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #
    filename = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/rebinned_convolution_results.h5"

    querylist = [
        {"name": "CE channel", "query": "(comenv_counter >= 1)"},
        {"name": "No CE channel", "query": "(comenv_counter == 0)"},
    ]

    querylist = [
        {"name": "Primary ppisn", "query": "(primary_undergone_ppisn == 1)"},
        {"name": "No Primary ppisn", "query": "(primary_undergone_ppisn == 0)"},
    ]

    #
    plot_rate_density_quantity_at_specific_redshift(
        filename=filename,
        rate_type="merger_rate",
        dco_type="bhbh",
        redshift_value=0,
        querylist=querylist,
        x_quantity="primary_mass",
        x_quantity_bins=np.arange(0, 70, 1),
        y_quantity="secondary_mass",
        y_quantity_bins=np.arange(0, 70, 1),
        x_scale="linear",
        y_scale="linear",
        verbose=0,
        add_ratio=False,
        plot_settings={"show_plot": True, "probability_floor": 3},
    )

else:
    matplotlib.use("Agg")
