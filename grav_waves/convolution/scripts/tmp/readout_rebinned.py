"""
Function to check the structure of the rebinned file
"""

import h5py
import pandas as pd

filename = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/rebinned_convolution_results.h5"


file = h5py.File(filename)

print(file.keys())

print(file["data"].keys())

print(file["data/merger_rate"].keys())


first = list(file["data/merger_rate"].keys())[0]
first_merger_rate_data = file["data/merger_rate/{}".format(first)][()]


file.close()

combined_df = pd.read_hdf(filename, "/data/combined_dataframes")
