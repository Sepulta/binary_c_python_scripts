"""
Parse function methods for the continous RLOF logging
"""

import os
import secrets

from binarycpython.utils.functions import output_lines


parameters_continous_RLOF = [
    # 1:        time
    "time",
    ## ZAMS values
    # 2-4:                        ZAMS values
    "zams_mass_1",
    "zams_mass_2",
    "zams_orbital_period",
    ## Initial RLOF episode values
    # 5-6:                        masses
    "initial_mass_accretor",
    "initial_mass_donor",
    # 7-8:                        radii
    "initial_radius_accretor",
    "initial_radius_donor",
    # 9-10:                       separation and orbital period
    "initial_separation",
    "initial_orbital_period",
    # 11-12:                      stellar types
    "initial_stellar_type_accretor",
    "initial_stellar_type_donor",
    # 13-14:                      orbital angular momentum and RLOF stability
    "initial_orbital_angular_momentum",
    "initial_stability",
    # 15-16:                      star numbers
    "initial_starnum_accretor",
    "initial_starnum_donor",
    # 17-18:                      time and disk accretion
    "initial_time",
    "initial_disk",
    ## Cumulative total values RLOF episode
    # 19-23:                      masses
    "total_mass_lost_from_accretor",
    "total_mass_lost_from_disk",
    "total_mass_lost",
    "total_mass_accreted",
    "total_mass_transferred",
    # 24-29:                      angular momenta
    "total_orbital_angular_momentum_lost_through_mass_loss_from_star",
    "total_orbital_angular_momentum_lost_through_mass_loss_from_disk",
    "total_orbital_angular_momentum_lost_through_mass_loss",
    "total_orbital_angular_momentum_stream_excess_returned_to_orbit",
    "total_orbital_angular_momentum_stellar_overspin_returned_to_orbit",
    "total_orbital_angular_momentum_stream_torque_with_orbit",
    # 30:                         total mass transferred through disk
    "total_mass_transferred_through_disk",
    # 31-35:                      total mass transferred through disk per category
    "total_mass_transferred_through_disk_MT_category_COLD",
    "total_mass_transferred_through_disk_MT_category_UNSTABLE",
    "total_mass_transferred_through_disk_MT_category_HOT",
    "total_mass_transferred_through_disk_MT_category_PARTIALLY_THICK",
    "total_mass_transferred_through_disk_MT_category_THICK",
    # 36:                         total time spent disk MT
    "total_time_spent_disk_masstransfer",
    # 37-41:                      total time spent disk MT per category
    "total_time_spent_disk_masstransfer_MT_category_COLD",
    "total_time_spent_disk_masstransfer_MT_category_UNSTABLE",
    "total_time_spent_disk_masstransfer_MT_category_HOT",
    "total_time_spent_disk_masstransfer_MT_category_PARTIALLY_THICK",
    "total_time_spent_disk_masstransfer_MT_category_THICK",
    # 42-44:                       time and mass weighted synchronicity factors. Use these and the total mass transferred to calculated the weighted averages
    "time_weighted_sum_synchronicity_factor",
    "mass_weighted_sum_synchronicity_factor",
    "synchronicity_factor",
    # 45-47: episode_number and probability and random seed
    "episode_number",
    "probability",
    "random_seed",
    ## Added by python:
    # 48: Unique identifier
    "unique_identifier",
]


def RLOF_parsing_continuous_to_file(
    line, parameters_RLOF, outfilename_RLOF, separator, unique_identifier
):
    """
    Function to handle the RLOF parsing
    """

    if line.startswith("RLOF_EPISODE"):
        values = line.split()[1:]

        # Check if the length of the output is the same as we expect
        if not len(values) == len(parameters_RLOF) - 1:
            print(
                "Length of readout values ({}) is not equal to length of parameters ({})".format(
                    len(values), len(parameters_RLOF) - 1
                )
            )
            raise ValueError

        # Write to file
        with open(outfilename_RLOF, "a") as output_filehandle:
            output_filehandle.write(separator.join(values + [unique_identifier]) + "\n")


def parse_function_continuous_to_file(self, output):
    """
    Function to read out the RLOF episode lines that contain a summary of the RLOF episode
    """

    # Separator
    separator = "\t"

    # Get some information from the
    data_dir = self.custom_options["data_dir"]
    output_file = os.path.join(data_dir, "RLOF_episode_data.dat")

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # Create unique number
    unique_identifier = str(secrets.randbits(50))

    # check if output file exists, if not, write it and create headers
    if not os.path.exists(output_file):
        with open(output_file, "w") as output_filehandle:
            output_filehandle.write(separator.join(parameters_continous_RLOF) + "\n")

    # Loop over the output
    for line in output_lines(output):
        RLOF_parsing_continuous_to_file(
            line=line,
            parameters_RLOF=parameters_continous_RLOF,
            outfilename_RLOF=output_file,
            separator=separator,
            unique_identifier=unique_identifier,
        )


def RLOF_parsing_continuous_to_df(
    output, parameters_RLOF, separator, unique_identifier
):
    """
    Function to handle the RLOF parsing
    """

    value_list = []

    # loop over output
    for line in output_lines(output):
        if line.startswith("RLOF_EPISODE"):
            values = line.split()[1:]

            # Check if the length of the output is the same as we expect
            if not len(values) == len(parameters_RLOF) - 1:
                print(
                    "Length of readout values ({}) is not equal to length of parameters ({})".format(
                        len(values), len(parameters_RLOF) - 1
                    )
                )
                raise ValueError

            value_list.append(values)

    # create dataframe with correct column names
    df = pd.Dataframe(value_list, columns=parameters_continous_RLOF)

    return df


def parse_function_continuous_to_df(self, output):
    """
    Function to read out the RLOF episode lines that contain a summary of the RLOF episode
    """

    # Separator
    separator = "\t"

    # Get some information from the
    data_dir = self.custom_options["data_dir"]
    output_file = os.path.join(data_dir, "RLOF_episode_data.dat")

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # Create unique number
    unique_identifier = str(secrets.randbits(50))

    # get df
    continous_RLOF_df = RLOF_parsing_continuous_to_df(
        output=output,
        parameters_RLOF=parameters_continous_RLOF,
        outfilename_RLOF=output_file,
        unique_identifier=unique_identifier,
    )
