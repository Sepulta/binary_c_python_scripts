import os
import json
import time
import pickle
import sys

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import get_help_all, get_help, create_hdf5

## Quick script to get some output about which stars go supernova when.
def output_lines(output):
    """
    Function that outputs the lines that were recieved from the binary_c run.
    """
    return output.splitlines()


def parse_function(self, output):
    # extract info from the population instance
    # TODO: think about whether this is smart. Passing around this object might be an overkill

    # Get some information from the
    data_dir = self.custom_options["data_dir"]
    base_filename = self.custom_options["base_filename"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    seperator = " "

    # Create filename
    outfilename = os.path.join(data_dir, base_filename)

    result_header = [
        "zams_mass",
        "st_0",
        "st_1",
        "st_2",
        "st_3",
        "st_4",
        "st_5",
        "st_6",
        "st_7",
        "st_8",
        "st_9",
        "st_10",
        "st_11",
        "st_12",
        "st_13",
        "st_14",
        "st_15",
    ]

    mass_lost_dict = {}
    for i in range(16):
        mass_lost_dict["{}".format(i)] = 0

    # Go over the output.
    for el in output_lines(output):
        headerline = el.split()[0]

        # Check the header and act accordingly
        if headerline == "DAVID_MASSLOSS":
            parameters = [
                "time",
                "mass_1",
                "prev_mass_1",
                "zams_mass_1",
                "stellar_type",
                "probability",
            ]
            values = el.split()[1:]

            if not float(values[0]) == 0.0:
                mass_lost = float(values[2]) - float(values[1])
                mass_lost_dict[values[4]] += mass_lost

                initial_mass = values[3]

    result_list = [initial_mass]
    for key in mass_lost_dict.keys():
        result_list.append(str(mass_lost_dict[key]))

    # print(mass_lost_dict)
    # print(result_list)

    # print(len(result_header))
    # print(len(result_list))

    if not os.path.exists(outfilename):
        with open(outfilename, "w") as f:
            f.write(seperator.join(result_header) + "\n")

    with open(outfilename, "a") as f:
        f.write(seperator.join(result_list) + "\n")


## Set values
test_pop = Population()
# test_pop.set(
#     C_logging_code="""
# Printf("DAVID_MASSLOSS %30.12e %g %g %g %d %g\\n",
#     //
#     stardata->model.time, // 1
#     stardata->star[0].mass, //2
#     stardata->previous_stardata->star[0].mass, //3
#     stardata->star[0].pms_mass, //4
#     stardata->star[0].stellar_type, //5
#     stardata->model.probability //6
#     );
# """)


metallicity = 0.02
test_pop.set(
    separation=1000000000,
    orbital_period=400000000,
    metallicity=metallicity,
    data_dir="",
    # data_dir=os.path.join(os.environ['BINARYC_DATA_ROOT'], 'testing_python', 'massloss_test', 'massloss_{}'.format(metallicity)),
    base_filename="massloss_{}.dat".format(metallicity),
    verbose=1,
    amt_cores=2,
    binary=0,
    parse_function=parse_function,
)

# Add grid variables
resolution = {"M_1": 10}

test_pop.add_grid_variable(
    name="M_1",
    longname="Primary mass",
    valuerange=[1, 300],
    resolution="{}".format(resolution["M_1"]),
    spacingfunc="const(1, 300, {})".format(resolution["M_1"]),
    probdist="Kroupa2001(M_1)",
    dphasevol="dm1",
    parameter_name="M_1",
)


start = time.time()
# quit()

# Export settings:
# test_pop.export_all_info(use_datadir=True)

# Evolve grid
test_pop.evolve()

# Put data (and settings) in hdf5
# create_hdf5(test_pop.custom_options['data_dir'], 'massloss_{}.hdf5'.format(metallicity))

stop = time.time()
