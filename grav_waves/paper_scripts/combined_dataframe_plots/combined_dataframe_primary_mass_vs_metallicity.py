"""
Function to plot the primary mass vs metallicity

This plot does the following:
- [X] Read out combined dataframe information and plot the primary mass vs metallicity
"""

import os

import h5py
import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

#
from grav_waves.convolution.functions.convolution_general_functions import (
    create_bins_from_centers,
)
from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    generate_mask,
    add_columns_to_df,
    quantity_name_dict,
    quantity_unit_dict,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def plot_primary_mass_vs_metallicity(dataset_filename, scale, plot_settings={}):
    """
    Function to plot the merger time vs metallicity for source distributions
    """

    # Read out info
    hdf5file = h5py.File(dataset_filename)

    metallicity_bin_centers = np.array(hdf5file["sfr_data/metallicity_bin_centers"][()])
    log10metallicity_values = np.array(sorted(np.log10(metallicity_bin_centers)))
    log10metallicity_bins = create_bins_from_centers(log10metallicity_values)
    metallicity_bins = 10**log10metallicity_bins

    hdf5file.close()

    # Read out combined df
    combined_df = pd.read_hdf(dataset_filename, "/data/combined_dataframes")
    combined_df = add_columns_to_df(combined_df, columns=["primary_mass"])

    # Create mask
    mask = generate_mask(combined_df, general_query=None, dco_type="bhbh")

    # Get the data from the dataframe
    primary_mass_values = combined_df["primary_mass"].to_numpy()[mask]
    metallicity_values = combined_df["metallicity"].to_numpy()[mask]
    number_per_formed_solarmass = combined_df[
        "number_per_solar_mass_values"
    ].to_numpy()[mask]

    #
    metallicity_centers = (metallicity_bins[1:] + metallicity_bins[:-1]) / 2

    primary_mass_bins = np.arange(0, 70, 1)
    primary_mass_centers = (primary_mass_bins[1:] + primary_mass_bins[:-1]) / 2

    hist = np.histogram2d(
        primary_mass_values,
        metallicity_values,
        bins=[primary_mass_bins, metallicity_bins],
        weights=number_per_formed_solarmass,
    )

    X, Y = np.meshgrid(primary_mass_centers, metallicity_centers)

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=11)

    ax = fig.add_subplot(gs[:, :-2])
    ax_cb = fig.add_subplot(gs[:, -1])

    ##################
    # Plot the data

    # Get the normalisation
    if scale == "linear":
        norm = colors.Normalize(vmin=hist[0].min(), vmax=hist[0].max())
    elif scale == "log":
        norm = colors.LogNorm(
            vmin=10 ** (np.log10(hist[0].max()) - 3),
            vmax=hist[0].max(),
        )

    ##################
    # Plot the MSSFR results
    _ = ax.pcolormesh(
        X,
        Y,
        hist[0].T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # make colorbar
    cb = matplotlib.colorbar.ColorbarBase(
        ax_cb, norm=norm, extend="min" if scale == "log" else None
    )
    cb.ax.set_ylabel("Number per formed solar mass")

    #
    ax.set_yscale("log")

    ax.set_xlabel(
        "{} [{}]".format(
            quantity_name_dict["primary_mass"],
            quantity_unit_dict["primary_mass"].to_string("latex"),
        )
    )
    ax.set_ylabel("Metallicity [Z]")

    ax.set_title(
        "{} vs metallicity distribution of source populations".format(
            quantity_name_dict["primary_mass"]
        ),
        fontsize=28,
    )

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #
    fiducial_filename = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/rebinned_convolution_results.h5"
    base_output_name = "primary_mass_vs_metallicity.pdf"

    #
    plot_primary_mass_vs_metallicity(
        dataset_filename=fiducial_filename,
        scale="log",
        plot_settings={
            "show_plot": True,
            # 'output_name': base_output_name
            "output_name": os.path.join(
                "/home/david/papers/paper_gw/paper_tex/figures/fiducial_combined_dataframe/",
                base_output_name,
            ),
        },
    )
