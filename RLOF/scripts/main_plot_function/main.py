"""
Function to run the routine to run all plot routines
"""

from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
)

from RLOF.scripts.main_plot_function.main_plot_function import main_plot_function


simulation_dir = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions"

# Get dataset
population_datasets_sana = generate_ensemble_dataset_dict(simulation_dir)

combined_pdf_filename = "test.pdf"
rebuild_results = True
testing = True

#
main_plot_function(
    population_datasets_sana["simulation_path"],
    combined_pdf_filename=combined_pdf_filename,
    verbose=1,
    testing=testing,
    rebuild_results=rebuild_results,
    do_theory_plots=True,
    do_ensemble_plots=True,
    do_episode_plots=True,
)
