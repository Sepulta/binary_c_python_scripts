"""
Function to plot the total intrinsic rate density over redshift

TODO: add total rate prediction for each variation
"""

import os

import h5py
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

#
from grav_waves.convolution.functions.convolution_general_functions import (
    create_bins_from_centers,
)
from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    generate_mask,
    add_columns_to_df,
    readout_rate_array,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def get_total_intrinsic_rate_data(dataset_filename):
    """
    Function to get the total intrinsic rate data
    """

    ################
    # Read out info
    hdf5file = h5py.File(dataset_filename)

    # Get metallicity info
    metallicity_bin_centers = np.array(hdf5file["sfr_data/metallicity_bin_centers"][()])
    log10metallicity_values = np.array(sorted(np.log10(metallicity_bin_centers)))
    log10metallicity_bins = create_bins_from_centers(log10metallicity_values)

    # Get redshift bin data
    redshifts = [float(el) for el in list(hdf5file["data/merger_rate"].keys())]

    hdf5file.close()

    #################
    # Read out combined df
    combined_df = pd.read_hdf(dataset_filename, "/data/combined_dataframes")
    combined_df = add_columns_to_df(combined_df, columns=["primary_mass"])

    # Create mask and filter shit
    mask = generate_mask(combined_df, general_query=None, dco_type="bhbh")

    #######################
    # Read out rate data
    datafile = h5py.File(dataset_filename)
    rate_array, redshifts = readout_rate_array(
        datafile=datafile, rate_key="merger_rate", mask=mask, max_redshift=10
    )
    datafile.close()

    # calculate total intrinsic rates
    total_intrinsic_rates = np.sum(rate_array, axis=1)

    return {"redshifts": redshifts, "total_intrinsic_rates": total_intrinsic_rates}


def plot_total_intrinsic_rate_density_over_redshift(dataset_dict, plot_settings={}):
    """
    Function to read out the data for total rate density evolution
    """

    result_dict = {}
    for dataset in dataset_dict:
        result_dict[dataset] = get_total_intrinsic_rate_data(dataset_dict[dataset])

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(18, 9))
    gs = fig.add_gridspec(nrows=1, ncols=9)

    ax = fig.add_subplot(gs[:, :])

    for dataset in result_dict:
        ax.plot(
            result_dict[dataset]["redshifts"],
            result_dict[dataset]["total_intrinsic_rates"],
            label=dataset,
        )

    ax.set_ylim([1e0, 2 * ax.get_ylim()[-1]])
    ax.set_xlim([ax.get_xlim()[0], 8])

    ax.legend()
    #
    ax.set_ylabel(
        r"Merger rate density $\mathcal{R}_{merger}(z) \left[\frac{d^{2}N_{merger}}{dt dV_{\mathrm{c}}}\right]$",
        fontsize=plot_settings.get("axislabel_fontsize", 26),
    )
    ax.set_xlabel(
        "Merger redshift [z]", fontsize=plot_settings.get("axislabel_fontsize", 26)
    )
    ax.set_yscale("log")

    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    data_root = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/"

    convolved_datasets = {
        "fiducial": os.path.join(
            data_root,
            "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
        ),
        "Farmer19": os.path.join(
            data_root,
            "HIGH_RES_SCHNEIDER_WIND_PPISN_FARMER19_FRYER_DELAYED//convolution_results/convolution_results.h5",
        ),
        "ten_extra_massloss": os.path.join(
            data_root,
            "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_10_PPISN_additional_massloss/convolution_results/convolution_results.h5",
        ),
        "twenty_extra_massloss": os.path.join(
            data_root,
            "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_20_PPISN_additional_massloss/convolution_results/convolution_results.h5",
        ),
        "five_core_mass_shift": os.path.join(
            data_root,
            "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-5_PPISN_core_mass_range_shift/convolution_results/convolution_results.h5",
        ),
        "ten_core_mass_shift": os.path.join(
            data_root,
            "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-10_PPISN_core_mass_range_shift/convolution_results/convolution_results.h5",
        ),
    }

    #
    base_name = "total_intrinsic_rate_density_over_redshift.pdf"
    plot_total_intrinsic_rate_density_over_redshift(
        dataset_dict=convolved_datasets,
        plot_settings={
            # 'show_plot': False,
            # 'output_name': base_name,
            "output_name": os.path.join(
                "/home/david/Dropbox/Academic/PHD/papers/paper_gw/paper_tex/figures/fiducial_over_redshift",
                base_name,
            ),
            "axislabel_fontsize": 32,
        },
    )
