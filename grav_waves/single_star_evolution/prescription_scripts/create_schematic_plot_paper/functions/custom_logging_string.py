mass_removal_custom_logging_string = """
if(stardata->star[0].SN_type != SN_NONE)
{
    if (stardata->model.time < stardata->model.max_evolution_time)
    {
        if(stardata->pre_events_stardata != NULL)
        {
            Printf("DAVID_SN %30.12e " // 1
                "%g %g %g %d " // 2-5
                "%d %d %g %g " // 6-9
                "%g %g %g %g\\n", // 10-13

                //
                stardata->model.time, // 1

                stardata->star[0].mass, //2
                stardata->pre_events_stardata->star[0].mass, //3
                stardata->common.zero_age.mass[0], //4
                stardata->star[0].SN_type, //5

                stardata->star[0].stellar_type, //6
                stardata->pre_events_stardata->star[0].stellar_type, //7
                stardata->model.probability, //8
                stardata->pre_events_stardata->star[0].core_mass[ID_core(stardata->pre_events_stardata->star[0].stellar_type)],           // 9

                stardata->pre_events_stardata->star[0].core_mass[CORE_CO],     // 10
                stardata->pre_events_stardata->star[0].core_mass[CORE_He],    // 11
                stardata->star[0].fallback, // 12
                stardata->star[0].fallback_mass // 13
            );
        }
    };
    /* Kill the simulation to save time */
    stardata->model.max_evolution_time = stardata->model.time - stardata->model.dtm;
};
"""
