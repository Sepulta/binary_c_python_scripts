"""
Script to generate the RLOFing systems, and log them accordingly.

Several steps need to be taken:

- execute system and get output
- split the system up into the different mass transfer episode counters
- write each of these sets to different files.

- if nr = 0 then ignore
- dont make an hdf5file
"""

import os
import json
import time
import pickle
import sys
import secrets

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import (
    get_help_all,
    get_help,
    create_hdf5,
    output_lines,
)

from david_phd_functions.binaryc.personal_defaults import personal_defaults
from RLOF.settings import project_specific_binary_c_results


def parse_function(self, output):
    """
    Parse function for the RLOFing systems:

    will read out the output, get the rlof_counter value and write each time to a new file.

    - i will also write those that have nr=0, but those should be ignored in any calculations.
    -
    """

    # extract info from the population instance
    # TODO: think about whether this is smart. Passing around this object might be an overkill

    # Get some information from the
    data_dir = self.custom_options["data_dir"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # Create filename
    parameters = [
        "time",  # 1
        "mass_1",
        "zams_mass_1",
        "mass_2",
        "zams_mass_2",  # 2-5
        "now_rlof_1",
        "was_rlof_1",
        "now_rlof_2",
        "was_rlof_2",  # 6-9
        "stellar_type_1",
        "prev_stellar_type_1",
        "stellar_type_2",
        "prev_stellar_type_2",  # 10-13
        "metallicity",
        "probability",  # 14-15
        "separation",
        "eccentricity",
        "period",
        "zams_period",  # 16-19
        "rlof_counter",
        "disk",
        "rlof_type",
        "random_seed_extra",  # 20-23
        "radius_1",
        "radius_2",
        "orbital_angular_momentum",
        "angular_momentum_1",  # 24-27
        "angular_momentum_2",
        "v_eq_1",
        "v_eq_ratio_1",
        "v_eq_2",  # 28-31
        "v_eq_ratio_2",
        "rlof_stability",
        "donor",
        "accretor",  # 32-35
        "lum_1",
        "lum_2",
        "prev_eccentricity",
        "dm_dt_rlof_donor",  # 36-39
        "teff_1",
        "teff_2",
        "random_seed",
        "cumulative_angmom_back_to_orbit",  # 40-43
        "minimum_mass_for_cumulative_angmom_back_to_orbit",
        "total_transferred",
        "total_accreted",
        "total_non_conservative_mass_loss",  # 44-47
        "total_non_conservative_angmom_loss",
        "total_angmom_orbit_loss",
        "total_angmom_orbit_gain",
        "isotropic_angmom_loss_with_minimum_mass_for_cumulative_angmom_back_to_orbit",  # 48-51
        "cumulative_time_disk_masstransfer",
        "cumulative_mass_transfered_through_disk",
        "cumulative_angmom_accreted_through_disk",  # 52-54
        "unique_nr",  # added in python
    ]

    rlof_counter_index = parameters.index("rlof_counter")

    # Get indices of initial values
    mass_1_index = parameters.index("zams_mass_1")
    mass_2_index = parameters.index("zams_mass_2")
    orbital_period_index = parameters.index("zams_period")
    metallicity_index = parameters.index("metallicity")

    separator = "\t"

    # Create files containing the first line of the RLOF episode and the last line.
    first_line_file = os.path.join(data_dir, "start_rlof_lines.dat")
    last_line_file = os.path.join(data_dir, "last_rlof_lines.dat")

    if not os.path.exists(first_line_file):
        with open(first_line_file, "w") as first_f:
            first_f.write(separator.join(parameters) + "\n")

    if not os.path.exists(last_line_file):
        with open(last_line_file, "w") as f:
            f.write(separator.join(parameters) + "\n")

    # Set up dict
    RLOF_dict = {}

    # TODO: fix that the individual episodes are split... shit
    RLOFING_lines = [
        line for line in output_lines(output) if line.startswith("DAVID_RLOFING")
    ]

    # Split all the output into the different episodes
    for line in RLOFING_lines:
        values = line.split()[1:]

        if not len(values) == len(parameters) - 1:
            print("Length of readout vaues is not equal to length of parameters")
            raise ValueError

        rlof_counter = int(values[rlof_counter_index])

        rlof_episode_list = RLOF_dict.get(rlof_counter, [])
        rlof_episode_list.append(values)
        RLOF_dict[rlof_counter] = rlof_episode_list

    for episode in RLOF_dict.keys():
        # Get values again
        rlof_values = RLOF_dict[episode]
        # print("\n".join(rlof_values))

        # Create unique number
        unique_nr = str(secrets.randbits(48))

        # Write first line of whole sequence to custom file
        with open(first_line_file, "a") as first_f:
            first_f.write(separator.join(rlof_values[0] + [unique_nr]) + "\n")

        # Write last line of whole sequence to different file
        with open(last_line_file, "a") as last_f:
            last_f.write(separator.join(rlof_values[-1] + [unique_nr]) + "\n")

    # if RLOFING_lines:
    #     # Write first line of whole sequence to custom file
    #     with open(first_line_file, 'a') as first_f:
    #         first_f.write(separator.join(RLOFING_lines[0].split()[1:] + [unique_nr])+'\n')

    #     # Write last line of whole sequence to different file
    #     with open(last_line_file, 'a') as last_f:
    #         last_f.write(separator.join(RLOFING_lines[-1].split()[1:] + [unique_nr])+'\n')

    # Loop over all the lines and put it in seperate files
    # for line in RLOFING_lines:
    #     values = line.split()[1:]
    #     rlof_counter = int(values[rlof_counter_index])
    #     base_filename = "m1={}_m2={}_per={}_z={}_nr={}.dat".format(
    #         values[mass_1_index],
    #         values[mass_2_index],
    #         values[orbital_period_index],
    #         values[metallicity_index],
    #         rlof_counter)

    #     outfilename = os.path.join(data_dir, base_filename)

    #     if not os.path.exists(outfilename):
    #         with open(outfilename, 'w') as f:
    #             f.write(separator.join(parameters)+'\n')

    #     with open(outfilename, 'a') as f:
    #         f.write(separator.join(values)+'\n')


## Make population and set value
test_pop = Population()
test_pop.set(verbosity=1)

# Set grid variables
# resolution = {'M_1': 40, 'q': 40, 'per': 80}
resolution = {"M_1": 10, "q": 10, "per": 10}

test_pop.add_grid_variable(
    name="lnm1",
    longname="Primary mass",
    valuerange=[1, 100],
    resolution="{}".format(resolution["M_1"]),
    spacingfunc="const(math.log(1), math.log(100), {})".format(resolution["M_1"]),
    precode="M_1=math.exp(lnm1)",
    probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 101, -1.3, -2.3, -2.3)*M_1",
    dphasevol="dlnm1",
    parameter_name="M_1",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
)

test_pop.add_grid_variable(
    name="q",
    longname="Mass ratio",
    valuerange=["0.1/M_1", 1],
    resolution="{}".format(resolution["q"]),
    spacingfunc="const(0.1/M_1, 1, {})".format(resolution["q"]),
    probdist="flatsections(q, [{'min': 0.1/M_1, 'max': 1.0, 'height': 1}])",
    dphasevol="dq",
    precode="M_2 = q * M_1",
    parameter_name="M_2",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
)

test_pop.add_grid_variable(
    name="log10per",  # in days
    longname="log10(Orbital_Period)",
    valuerange=[0.15, 5.5],
    resolution="{}".format(resolution["per"]),
    spacingfunc="const(0.15, 5.5, {})".format(resolution["per"]),
    precode="""orbital_period = 10** log10per
sep = calc_sep_from_period(M_1, M_2, orbital_period)
sep_min = calc_sep_from_period(M_1, M_2, 10**0.15)
sep_max = calc_sep_from_period(M_1, M_2, 10**5.5)""",
    probdist="sana12(M_1, M_2, sep, orbital_period, sep_min, sep_max, math.log10(10**0.15), math.log10(10**5.5), -0.55)",
    parameter_name="orbital_period",
    dphasevol="dlog10per",
)

# test_pop.add_grid_variable(
#     name="logper",
#     longname="log(Orbital_Period)",
#     valuerange=[-2, 12],
#     resolution="{}".format(resolution["per"]),
#     spacingfunc="np.linspace(-2, 12, {})".format(resolution["per"]),
#     precode="orbital_period = 10** logper\n",
#     probdist="gaussian(logper,4.8, 2.3, -2.0, 12.0)",
#     parameter_name="orbital_period",
#     dphasevol="dln10per",
# )

test_pop.set(**personal_defaults)

test_pop.set(
    david_logging_function=10,
    binary=1,
    # amt_cores=24,
    amt_cores=2,
    verbosity=4,
    # # Activate ensemble
    ensemble=1,
    ensemble_defer=1,
    ensemble_filters_off=1,
    ensemble_filter_RLOF=1,
    combine_ensemble_with_thread_joining=True,
    # log_runtime_systems=0
    orbital_period=1200,
    M_1=20,
    M_2=8,
    parse_function=parse_function,
    # metallicity=0.0002,
    # data_dir=os.path.join(os.environ['BINARYC_DATA_ROOT'], 'TESTING_OUTPUT2', 'RLOFING_systems_z{}'.format(0.02)),
)

# test_pop.evolve_single()
# quit()

# Make metallicity loop.
# metallicity_values = [0.02, 0.01, 0.002, 0.001, 0.0002]
# metallicity_values = [0.02, 0.01, 0.002]
metallicity_values = [0.02]
for metallicity in metallicity_values:
    test_pop.set(
        metallicity=metallicity,
        parse_function=parse_function,
        # data_dir=os.path.join(os.environ['BINARYC_DATA_ROOT'], 'RLOF_SANA_2021_01', 'RLOFING_systems_z{}'.format(metallicity)),
        data_dir=os.path.join(
            os.environ["BINARYC_DATA_ROOT"],
            "TESTING",
            "RLOF_SANA_2021_01_ENSEMBLE_presentation",
            "RLOFING_systems_z{}".format(metallicity),
        ),
    )

    # create local tmp_dir
    test_pop.set(
        tmp_dir=os.path.join(test_pop.custom_options["data_dir"], "local_tmp_dir")
    )
    os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

    # Export settings:
    test_pop.export_all_info(use_datadir=True)

    # Evolve grid
    test_pop.evolve()

    # Get ensemble output and write it to file
    ensemble_output = test_pop.grid_ensemble_results

    with open(
        os.path.join(test_pop.custom_options["data_dir"], "ensemble_output.json"), "w"
    ) as f:
        json.dump(ensemble_output, f, indent=4)
