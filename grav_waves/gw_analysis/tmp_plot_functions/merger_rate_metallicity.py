from settings import *
import settings
import matplotlib.colors
import matplotlib.gridspec as gridspec

from laptop_datasets import dataset_dict

import astropy
import argparse  # https://docs.python.org/3/library/argparse.html
from grav_waves.settings import cosmo
import astropy.units as u


MAIN_RESULT_DIR = "results"
MAIN_PLOT_DIR = "plots"

key = "HIGH_RES_SCHNEIDER_MASS_LOSS_PPISN_ON"
# key = 'HIGH_RES_SCHNEIDER_MASS_LOSS_PPISN_OFF'
type_key = "bhbh"

rebuild = dataset_dict[key]["rebuild"]
sim_name = dataset_dict[key]["sim_name"]
result_dir = os.path.join(MAIN_RESULT_DIR, dataset_dict[key]["result_dir"])
plot_dir = os.path.join(MAIN_PLOT_DIR, dataset_dict[key]["plot_dir"])

pdf_list = []

# Load dataset
info_dict = load_info_dict(result_dir, rebuild=rebuild)

plot_merger_rate_by_metallicity_by_merger_time(
    info_dict, type_key, plot_settings={"show_plot": True}
)
