"""
Script containing the functions to generate the stability table for all the metallicities passed in the dict

This is taken  from the start_rlof subdict

This contains
- Stable MT onto MS via disk
- Stable MT onto MS via direct
- Unstable MT onto MS via disk
- Unstable MT onto MS via direct
"""

import json
import pandas as pd

from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)
from ensemble_functions.ensemble_functions import (
    get_recursive_sum,
    merge_all_subdicts,
    query_dict,
)
from grav_waves.settings import convolution_settings


def generate_table_stable_fractions(
    dataset_dict,
    output_filename,
    testing,
    caption="",
    label="",
    global_query_dict=None,
    verbose=0,
):
    """
    Function to generate the table with stability fractions on disk and no disk.
    """

    if verbose:
        print(
            "Running generate_table_stable_fractions.\n\tOutput: {}".format(
                output_filename
            )
        )

        if global_query_dict:
            print(
                "\twith global query: {}".format(global_query_dict["global_query_name"])
            )
            print(
                "\t\tqueries: {}".format(
                    ", ".join(
                        [
                            "{} between {}".format(
                                query["parameter_key"], query["value_range"]
                            )
                            for query in global_query_dict["query_list"]
                        ]
                    )
                )
            )

    #
    result_dict = {}

    # Calculate this for all the metallicities
    for key in dataset_dict.keys():
        if verbose:
            print("\tZ = {}".format(key))
        current_dataset = dataset_dict[key]

        # Get the correct ensemble data from the dict
        if verbose:
            print("\t\tLoading data")
        probability_to_yield_conversion_factor = (
            convolution_settings["binary_fraction"]
            * convolution_settings["mass_in_stars"]
        )
        ensemble_data = get_ensemble_data(
            current_dataset,
            testing,
            probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
        )["RLOF"]["starting_rlof"]

        if global_query_dict:
            if verbose:
                print("\tPerforming queries")
            for query in global_query_dict["query_list"]:
                if verbose:
                    print(
                        "\t\tPerforming query {}".format(
                            "{} between {}".format(
                                query["parameter_key"], query["value_range"]
                            )
                        )
                    )
                ensemble_data = query_dict(
                    ensemble_data, query["parameter_key"], query["value_range"]
                )

        # Select MT onto MS
        if verbose:
            print("\t\tSelecting MS accretors")
        ms_ensemble_data = query_dict(ensemble_data, "st_acc", [0.5, 1.5])
        total_ms_ensemble_data = get_recursive_sum(ms_ensemble_data)

        # Select stability
        if verbose:
            print("\t\tSelecting stable and unstable")
        stable_ms_ensemble_data = query_dict(ms_ensemble_data, "stable", [-0.5, 0.5])
        unstable_ms_ensemble_data = query_dict(ms_ensemble_data, "stable", [0.5, 100])

        # Select disk or direct
        if verbose:
            print("\t\tSelecting disk and direct")
        disk_stable_ms_ensemble_data = query_dict(
            stable_ms_ensemble_data, "disk", [0.5, 1.5]
        )
        direct_stable_ms_ensemble_data = query_dict(
            stable_ms_ensemble_data, "disk", [-0.5, 0.5]
        )

        disk_unstable_ms_ensemble_data = query_dict(
            unstable_ms_ensemble_data, "disk", [0.5, 1.5]
        )
        direct_unstable_ms_ensemble_data = query_dict(
            unstable_ms_ensemble_data, "disk", [-0.5, 0.5]
        )

        # Get totals
        if verbose:
            print("\t\tGetting totals")
        total_disk_stable_ms_ensemble_data = get_recursive_sum(
            disk_stable_ms_ensemble_data
        )
        total_direct_stable_ms_ensemble_data = get_recursive_sum(
            direct_stable_ms_ensemble_data
        )

        total_disk_unstable_ms_ensemble_data = get_recursive_sum(
            disk_unstable_ms_ensemble_data
        )
        total_direct_unstable_ms_ensemble_data = get_recursive_sum(
            direct_unstable_ms_ensemble_data
        )

        # Get fractions
        if verbose:
            print("\t\tGetting fractions")
        fraction_disk_stable_ms_ensemble_data = (
            total_disk_stable_ms_ensemble_data / total_ms_ensemble_data
            if total_ms_ensemble_data != 0
            else 0
        )
        fraction_direct_stable_ms_ensemble_data = (
            total_direct_stable_ms_ensemble_data / total_ms_ensemble_data
            if total_ms_ensemble_data != 0
            else 0
        )

        fraction_disk_unstable_ms_ensemble_data = (
            total_disk_unstable_ms_ensemble_data / total_ms_ensemble_data
            if total_ms_ensemble_data != 0
            else 0
        )
        fraction_direct_unstable_ms_ensemble_data = (
            total_direct_unstable_ms_ensemble_data / total_ms_ensemble_data
            if total_ms_ensemble_data != 0
            else 0
        )

        # Store results
        if verbose:
            print("\t\tStoring results")
        result_dict["Z = {}".format(key)] = [
            fraction_disk_stable_ms_ensemble_data,
            fraction_direct_stable_ms_ensemble_data,
            fraction_disk_unstable_ms_ensemble_data,
            fraction_direct_unstable_ms_ensemble_data,
        ]

    #
    column_names = [
        "Fraction stable MT via disk",
        "Fraction stable MT direct",
        "Fraction unstable MT via disk",
        "Fraction unstable MT direct",
    ]

    # put in dataframe:
    result_df = pd.DataFrame.from_dict(
        result_dict, columns=column_names, orient="index"
    )
    result_df = result_df.transpose()

    # Create latex string
    latex_string = result_df.to_latex(
        index=True, label=label, caption=caption, float_format="%.2f"
    )
    # print(latex_string)

    # Export
    with open(output_filename, "w") as output_filehandle:
        output_filehandle.write(latex_string)


# #
# population_datasets_sana = generate_ensemble_dataset_dict('/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions/')
# generate_table_stable_fractions(population_datasets_sana, 'test.tex', testing=True, label='', caption='', verbose=1)
