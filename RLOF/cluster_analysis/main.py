"""
Main cluster readout and analysis script
"""

import pandas as pd

from RLOF.cluster_analysis.plot_functions import (
    plot_cluster_and_data,
    plot_similarity_statistics,
)
from RLOF.cluster_analysis.functions import normalize_df, get_statistics

from open_cluster_data.main import get_cluster_data

#
data_file = "/home/david/data_projects/binary_c_data/RLOF/TEST_RES_OPEN_CLUSTER_M35_SANA/population_results/Z0.02/subproject_cluster_ensemble_data.csv"
data_file = "/home/david/data_projects/binary_c_data/RLOF/LOW_RES_OPEN_CLUSTER_M35_SANA/population_results/Z0.02/subproject_cluster_ensemble_data.csv"
data_file = "/home/david/data_projects/binary_c_data/RLOF/LOW_RES_OPEN_CLUSTER_M35_MOE_DISTEFANO/population_results/Z0.02/subproject_cluster_ensemble_data.csv"
# data_file = "/home/david/data_projects/binary_c_data/RLOF/TEST_RES_OPEN_CLUSTER_M35_MOE_DISTEFANO/population_results/Z0.02/subproject_cluster_ensemble_data.csv"


M35_data = get_cluster_data("M35")

# Open with pandas
df = pd.read_csv(data_file, sep="\s+")

# Convert columns to observational
df.loc[:, "orbital_period_in_years"] = 10 ** df["log10orbital_period"]
df.loc[:, "orbital_period_in_days"] = df["orbital_period_in_years"] * 365

M35_sample_stats = get_statistics(df, cluster_observations=M35_data, n_bootstraps=100)
# plot_similarity_statistics(M35_sample_stats, plot_settings={"show_plot": True})

# select only the MS+MS systems. This includes the lower mass main sequence
ms_df = df.query("st_1 < 2 & st_2 < 2")

orbital_period_upper_bound_cutoff = 50
orbital_eccentricity_upper_bound_cutoff = 60

# Normalize the df
normalize_df(ms_df)

# Do cut-off for observational bias
ms_df = ms_df.query(
    "orbital_period_in_days < {}".format(orbital_period_upper_bound_cutoff)
)
ms_df = ms_df.query(
    "orbital_eccentricity < {}".format(orbital_eccentricity_upper_bound_cutoff)
)

# plot_cluster_and_data(ms_df, plot_settings={'show_plot': True})
plot_cluster_and_data(
    df, cluster_observations=M35_data, plot_settings={"show_plot": True}
)
