"""
Functions to handle the rebinning of the results
"""

import os
import time
import json
import warnings
import shutil
import multiprocessing
import pickle

import h5py
import pandas as pd
import numpy as np
import setproctitle
from tables import NaturalNameWarning

from grav_waves.convolution.functions.volume_convolution_functions import (
    create_shell_volume_dict,
)
from grav_waves.convolution.functions.convolution_general_functions import (
    JsonCustomEncoder,
)
from grav_waves.convolution.functions.convolution_sfr_functions import (
    generate_metallicity_sfr_array,
)
from grav_waves.convolution.functions.convolution_calculate_output_functions import (
    create_birthrate_array,
)

from grav_waves.settings import logger

warnings.filterwarnings("ignore", category=NaturalNameWarning)


def create_new_redshift_dict_with_old_centers(
    new_redshift_centers, new_redshift_bins, old_redshift_shell_volume_dict
):
    """
    Create dictionary that stores the info of the new redshift centers, bins, and the old centers that fit in there
    """

    # Set up a new redshift dictionary
    new_redshift_dict_with_old_centers = {}
    for redshift_i, redshift in enumerate(sorted(new_redshift_centers)):
        new_redshift_dict_with_old_centers[redshift] = {
            "edges": {
                "lower_edge": new_redshift_bins[redshift_i],
                "upper_edge": new_redshift_bins[redshift_i + 1],
            }
        }

    # Find all the old redshift centers fall in each of the current redshift bins
    old_redshift_data = list(old_redshift_shell_volume_dict.keys())
    for redshift_i, redshift in enumerate(sorted(new_redshift_dict_with_old_centers)):
        old_redshifts_in_current_redshift_bin = []

        #
        lower_edge = new_redshift_dict_with_old_centers[redshift]["edges"]["lower_edge"]
        upper_edge = new_redshift_dict_with_old_centers[redshift]["edges"]["upper_edge"]

        for old_redshift_key in old_redshift_data:
            if lower_edge <= float(old_redshift_key) < upper_edge:
                old_redshifts_in_current_redshift_bin.append(old_redshift_key)

        new_redshift_dict_with_old_centers[redshift][
            "old_keys"
        ] = old_redshifts_in_current_redshift_bin

    #
    return new_redshift_dict_with_old_centers


def rebin_convolution_job_worker(job_queue, worker_ID):
    """
    Function that handles running the job
    """

    #
    setproctitle.setproctitle("convolution rebin worker process {}".format(worker_ID))

    # Get items from the job_queue
    for job_dict in iter(job_queue.get, "STOP"):
        if job_dict == "STOP":
            print("\t\tWorker {}: stopping".format(worker_ID))
        else:
            ##########################
            # Unpack job dict
            redshift = job_dict["redshift"]
            old_redshift_shell_volume_dict = job_dict["old_redshift_shell_volume_dict"]
            new_redshift_shell_volume_dict = job_dict["new_redshift_shell_volume_dict"]
            new_redshift_dict_with_old_centers = job_dict[
                "new_redshift_dict_with_old_centers"
            ]
            output_redshifts_dir = job_dict["output_redshifts_dir"]
            input_redshifts_dir = job_dict["input_redshifts_dir"]
            convolution_configuration = job_dict["convolution_configuration"]

            ##########################
            logger.debug("Worker {} handling redshift {}".format(worker_ID, redshift))

            # Use the current volume dict
            current_volume_dict = new_redshift_shell_volume_dict[redshift]

            # Create new output dict
            new_output_dict = {}

            ##########################
            # Loop over the old bins
            for _, old_key in enumerate(
                new_redshift_dict_with_old_centers[redshift]["old_keys"]
            ):
                logger.debug(
                    "Worker {} adding old redshift {} to new redshift {}".format(
                        worker_ID, old_key, redshift
                    )
                )

                ##########################
                # Get volume of current sub-redshift shell
                old_volume_dict = old_redshift_shell_volume_dict[str(old_key)]
                old_volume = float(old_volume_dict["shell_volume"]["value"])

                ##########################
                # Open current redshift pickle file
                full_path = os.path.join(input_redshifts_dir, "{}.p".format(old_key))
                with open(full_path, "rb") as f:
                    redshift_result_dict = pickle.load(f)

                # Loop over the data types
                for rate_data_key in redshift_result_dict.keys():

                    # Get rate data from old pickle file
                    rate_data_dict = redshift_result_dict[rate_data_key]

                    # Check if the current rate type is present in the new dict, if not; provide structure and create empty rate data
                    if not rate_data_key in new_output_dict:
                        new_output_dict[rate_data_key] = {
                            "time_value": redshift,
                            "rate_type": rate_data_dict["rate_type"],
                            "rate_array": np.zeros(rate_data_dict["rate_array"].shape),
                        }

                    # Add old rates to new array
                    new_output_dict[rate_data_key]["rate_array"] += (
                        rate_data_dict["rate_array"] * old_volume
                    )

                # remove the old pickled file
                if convolution_configuration["remove_pickle_files_after_rebinning"]:
                    os.remove(full_path)

            # Divide by the new shell volume for all rate types
            for rate_data_key in new_output_dict.keys():
                new_output_dict[rate_data_key]["rate_array"] = (
                    new_output_dict[rate_data_key]["rate_array"]
                    / current_volume_dict["shell_volume"].value
                )

            # Write to pickle file
            with open(
                os.path.join(output_redshifts_dir, "{}.p".format(redshift)), "wb"
            ) as f:
                pickle.dump(new_output_dict, f)


def rebin_convolution_queue_filler(
    job_queue,
    num_cores,
    new_redshift_centers,
    new_redshift_shell_volume_dict,
    old_redshift_shell_volume_dict,
    new_redshift_dict_with_old_centers,
    input_redshifts_dir,
    output_redshifts_dir,
    convolution_configuration,
):
    """
    Function to handle filling the queue for rebinning.
    """

    # Loop over all the new redshift centers
    for redshift_i, redshift in enumerate(sorted(new_redshift_centers)):
        # Use the current volume dict
        current_volume_dict = new_redshift_shell_volume_dict[redshift]

        # Set up job dict
        job_dict = {
            "redshift": redshift,
            "redshift_i": redshift_i,
            "current_volume_dict": current_volume_dict,
            "input_redshifts_dir": input_redshifts_dir,
            "output_redshifts_dir": output_redshifts_dir,
            "old_redshift_shell_volume_dict": old_redshift_shell_volume_dict,
            "new_redshift_shell_volume_dict": new_redshift_shell_volume_dict,
            "new_redshift_dict_with_old_centers": new_redshift_dict_with_old_centers,
            "convolution_configuration": convolution_configuration,
        }

        # Put job in queue
        job_queue.put(job_dict)

    # Signal stop to workers
    for _ in range(num_cores):
        job_queue.put("STOP")


def rebin_multiprocessing(
    convolution_configuration,
    output_dataset_filename,
    new_redshift_centers,
    new_redshift_shell_volume_dict,
    old_redshift_shell_volume_dict,
    new_redshift_dict_with_old_centers,
):
    """
    Main process to handle the multiprocessing
    """

    # check for tmp dir
    if convolution_configuration["convolution_tmp_dir"] is None:
        raise ValueError(
            "Please set the convolution_tmp_dir to do the convolution with multiprocessing"
        )

    #
    tmp_input_dir = os.path.join(convolution_configuration["convolution_tmp_dir"])
    tmp_output_dir = os.path.join(
        convolution_configuration["convolution_tmp_dir"], "rebinned"
    )

    #
    if os.path.isdir(tmp_output_dir):
        shutil.rmtree(tmp_output_dir)
    os.makedirs(tmp_output_dir, exist_ok=True)

    ########################
    # Rebin using multiprocessing
    convolution_configuration["logger"].info(
        "Running the rebinning with multiprocessing using {} cores".format(
            convolution_configuration["num_cores"]
        )
    )
    start = time.time()

    # Set process name
    setproctitle.setproctitle("rebin multiprocessing parent process")

    # Set up the manager object that can share info between processes
    manager = multiprocessing.Manager()
    job_queue = manager.Queue(convolution_configuration["max_job_queue_size"])

    # Create process instances
    processes = []
    for worker_ID in range(convolution_configuration["num_cores"]):
        processes.append(
            multiprocessing.Process(
                target=rebin_convolution_job_worker,
                args=(job_queue, worker_ID),
            )
        )

    # Activate the processes
    for p in processes:
        p.start()

    # Set up the system_queue
    rebin_convolution_queue_filler(
        job_queue=job_queue,
        new_redshift_centers=new_redshift_centers,
        new_redshift_shell_volume_dict=new_redshift_shell_volume_dict,
        old_redshift_shell_volume_dict=old_redshift_shell_volume_dict,
        new_redshift_dict_with_old_centers=new_redshift_dict_with_old_centers,
        num_cores=convolution_configuration["num_cores"],
        input_redshifts_dir=tmp_input_dir,
        output_redshifts_dir=tmp_output_dir,
        convolution_configuration=convolution_configuration,
    )

    # Join the processes
    for p in processes:
        p.join()
    convolution_configuration["logger"].info(
        "Generated pickle files for each redshift. This took {}".format(
            time.time() - start
        )
    )

    ###################
    # Write redshift info to hdf5 and remove files if necessary
    pickle_to_hdf5_start = time.time()
    if convolution_configuration["write_to_hdf5"]:
        convolution_configuration["logger"].info(
            "Writing to HDF5 file".format(time.time() - start)
        )

        # Open file
        output_hdf5file = h5py.File(output_dataset_filename, "a")

        # Loop over files in the pickle dir
        for file in sorted(os.listdir(tmp_output_dir)):
            full_path = os.path.join(tmp_output_dir, file)

            # Open file
            with open(full_path, "rb") as f:
                # read out data
                redshift_dict = pickle.load(f)

                # Loop over rate types
                for rate_type_key in redshift_dict.keys():
                    rate_type_dict = redshift_dict[rate_type_key]

                    # Read out the current data
                    rate_type = rate_type_dict["rate_type"]
                    redshift_value = rate_type_dict["time_value"]
                    rate_array = rate_type_dict["rate_array"]

                    # Add group if necessary
                    if not rate_type in output_hdf5file["data/"].keys():
                        output_hdf5file.create_group("data/{}".format(rate_type))

                    #
                    convolution_configuration["logger"].debug(
                        "Adding z={} rate_type={} to {}".format(
                            redshift_value, rate_type, tmp_output_dir
                        )
                    )

                    # write to hdf5
                    output_hdf5file["data/{}".format(rate_type)].create_dataset(
                        str(redshift_value), data=rate_array
                    )

            # remove the pickled file
            if convolution_configuration["remove_pickle_files_after_rebinning"]:
                os.remove(full_path)

        # Remove the directory
        if convolution_configuration["remove_pickle_files_after_rebinning"]:
            shutil.rmtree(tmp_output_dir)

    #
    convolution_configuration["logger"].info(
        "writing pickle files to the hdf5 file took {}".format(
            time.time() - pickle_to_hdf5_start
        )
    )


def convolution_rebin_redshift(
    input_dataset_filename, output_dataset_filename, new_redshift_bins
):
    """
    Function to re-bin the results of the
    """

    #
    logger.info(
        "Rebinning all the data from {}. Writing results to {}".format(
            input_dataset_filename, output_dataset_filename
        )
    )

    # Create the output directory
    if os.path.dirname(output_dataset_filename):
        os.makedirs(os.path.dirname(output_dataset_filename), exist_ok=True)

    #####################
    # Open output dataset
    logger.info("Creating new file and copying settings")

    # Open output dataset
    new_hdf5_file = h5py.File(output_dataset_filename, "w")

    # Open input dataset
    old_hdf5_file = h5py.File(input_dataset_filename, "r")

    # Create subgroups
    settings_grp = new_hdf5_file.create_group("settings")
    sfr_grp = new_hdf5_file.create_group("sfr_data")
    shell_volumes_grp = new_hdf5_file.create_group("shell_volumes")
    _ = new_hdf5_file.create_group("data")

    #####################
    # Calculate the new centers
    new_redshift_centers = (new_redshift_bins[1:] + new_redshift_bins[:-1]) / 2

    # Create new volume dict for redshift
    new_redshift_shell_volume_dict = create_shell_volume_dict(new_redshift_centers)
    old_redshift_shell_volume_dict = json.loads(
        old_hdf5_file["shell_volumes"]["redshift_shell_volume_dict"][()]
    )

    new_redshift_dict_with_old_centers = create_new_redshift_dict_with_old_centers(
        new_redshift_centers, new_redshift_bins, old_redshift_shell_volume_dict
    )

    ##################
    # Create shell volume dict
    shell_volumes_grp.create_dataset(
        "redshift_shell_volume_dict",
        data=json.dumps(new_redshift_shell_volume_dict, cls=JsonCustomEncoder),
    )

    ########################
    # Copy all the information of the previous dataset into this dataset

    # Dataset dicts
    dataset_dict = json.loads(old_hdf5_file["settings"]["dataset_dict"][()])
    settings_grp.create_dataset(
        "dataset_dict", data=json.dumps(dataset_dict, cls=JsonCustomEncoder)
    )

    # Add the input datasets dictionary
    info_dict = json.loads(old_hdf5_file["settings"]["input_datasets"][()])
    settings_grp.create_dataset(
        "input_datasets", data=json.dumps(info_dict, cls=JsonCustomEncoder)
    )

    # Add the convolution configuration
    convolution_configuration = json.loads(
        old_hdf5_file["settings"]["convolution_configuration"][()]
    )
    convolution_configuration["time_bins"] = new_redshift_bins
    convolution_configuration["time_centers"] = new_redshift_centers
    settings_grp.create_dataset(
        "convolution_configuration",
        data=json.dumps(convolution_configuration, cls=JsonCustomEncoder),
    )
    convolution_configuration["logger"] = logger

    # Add the cosmology configuration
    cosmology_configuration = json.loads(
        old_hdf5_file["settings"]["cosmology_configuration"][()]
    )
    settings_grp.create_dataset(
        "cosmology_configuration",
        data=json.dumps(cosmology_configuration, cls=JsonCustomEncoder),
    )

    # Add the metallicity settings
    metallicity_settings = json.loads(
        old_hdf5_file["settings"]["metallicity_settings"][()]
    )
    settings_grp.create_dataset(
        "metallicity_settings",
        data=json.dumps(metallicity_settings, cls=JsonCustomEncoder),
    )

    ###########
    # Recalculate the star formation stuff with the new time bins
    start_sfr = time.time()
    (
        metallicity_weighted_starformation_array,
        metallicity_fraction_array,
        starformation_array,
    ) = generate_metallicity_sfr_array(
        cosmology_configuration=cosmology_configuration,
        time_centers=convolution_configuration["time_centers"],
        sorted_metallicity_log10values=np.array(
            metallicity_settings["sorted_log10metallicity_values"]
        ),
    )
    metallicity_weighted_starformation_array = (
        metallicity_weighted_starformation_array.T
    )

    # Write to output file
    sfr_grp.create_dataset(
        "metallicity_weighted_starformation_array",
        data=metallicity_weighted_starformation_array,
    )
    sfr_grp.create_dataset(
        "metallicity_fraction_array", data=metallicity_fraction_array
    )
    sfr_grp.create_dataset("starformation_array", data=starformation_array)

    sfr_grp.create_dataset(
        "metallicity_bin_centers",
        data=old_hdf5_file["sfr_data"]["metallicity_bin_centers"][()],
    )
    sfr_grp.create_dataset(
        "metallicity_bin_edges",
        data=old_hdf5_file["sfr_data"]["metallicity_bin_edges"][()],
    )
    sfr_grp.create_dataset(
        "redshift_bin_centers", data=convolution_configuration["time_centers"]
    )
    sfr_grp.create_dataset(
        "redshift_bin_edges", data=convolution_configuration["time_bins"]
    )
    convolution_configuration["logger"].info(
        "\ttook {:.2e}s".format(time.time() - start_sfr)
    )

    ######
    # Close both files cause we can't write the hdf5 files to them
    old_hdf5_file.close()
    new_hdf5_file.close()

    # Copy the combined df
    logger.info("Copying combined dataset")

    combined_df = pd.read_hdf(input_dataset_filename, "/data/combined_dataframes")
    combined_df.to_hdf(output_dataset_filename, "data/combined_dataframes")

    #########
    # Loop over rate types
    rebin_multiprocessing(
        convolution_configuration=convolution_configuration,
        output_dataset_filename=output_dataset_filename,
        new_redshift_centers=new_redshift_centers,
        new_redshift_shell_volume_dict=new_redshift_shell_volume_dict,
        old_redshift_shell_volume_dict=old_redshift_shell_volume_dict,
        new_redshift_dict_with_old_centers=new_redshift_dict_with_old_centers,
    )

    #
    print("Finished rebining to {}".format(output_dataset_filename))
