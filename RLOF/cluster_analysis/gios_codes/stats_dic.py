# This file contains the dictionary needed for the stats calculations

runs = {
    "M35": {
        "cutoff": [1.7, 0.6],
        "output": "autostats6/M35_oct22",
        "distributions": {
            "BSE": {
                "Gauss": {
                    "id": "BSE + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_k2_fixed/M35testBG/ensemble_output.json",
                    "color": "#f0e442",
                    "style": "solid",
                },
                "MS": {
                    "id": "BSE + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_k2_fixed/M35testBMS/ensemble_output.json",
                    "color": "#cc79a7",
                    "style": "dashed",
                },
            },
            "MINT": {
                "Gauss": {
                    "id": "MINT + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_renvfix/M35testMG_vrot0_0/ensemble_output.json",
                    "color": "#0072b2",
                    "style": "solid",
                },
                "MS": {
                    "id": "MINT + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_renvfix/M35testMMS_vrot0_0/ensemble_output.json",
                    "color": "#009e73",
                    "style": "dashed",
                },
            },
        },
    },
    "Pl": {
        "cutoff": [1.5, 0.7],
        "output": "autostats6/Pl_oct22",
        "distributions": {
            "BSE": {
                "Gauss": {
                    "id": "BSE + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_k2_fixed/PleiadestestBG_vrot0_0/ensemble_output.json",
                    "color": "#f0e442",
                    "style": "solid",
                },
                "MS": {
                    "id": "BSE + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_k2_fixed/PleiadestestBMS_vrot0_0/ensemble_output.json",
                    "color": "#cc79a7",
                    "style": "dashed",
                },
            },
            "MINT": {
                "Gauss": {
                    "id": "MINT + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_renvfix/PleiadestestMG_vrot0_0/ensemble_output.json",
                    "color": "#0072b2",
                    "style": "solid",
                },
                "MS": {
                    "id": "MINT + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_renvfix/PleiadestestMMS_vrot0_0/ensemble_output.json",
                    "color": "#009e73",
                    "style": "dashed",
                },
            },
        },
    },
    "HyPr": {
        "cutoff": [1.4, 0.7],
        "output": "autostats6/HyPr_oct22",
        "distributions": {
            "BSE": {
                "Gauss": {
                    "id": "BSE + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_e0/HyPrtestBG/ensemble_output.json",
                    "color": "#f0e442",
                    "style": "solid",
                },
                "MS": {
                    "id": "BSE + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_e0/HyPrtestBMS/ensemble_output.json",
                    "color": "#cc79a7",
                    "style": "dashed",
                },
            },
            "MINT": {
                "Gauss": {
                    "id": "MINT + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_renvfix/HyPrtestMG_vrot0_0/ensemble_output.json",
                    "color": "#0072b2",
                    "style": "solid",
                },
                "MS": {
                    "id": "MINT + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_renvfix/HyPrtestMMS_vrot0_0/ensemble_output.json",
                    "color": "#009e73",
                    "style": "dashed",
                },
            },
        },
    },
    #####################################################################################################################################
    "M67": {
        "cutoff": [1.8, 0.7],
        "output": "autostats6/M67_oct22",
        "distributions": {
            "BSE": {
                "Gauss": {
                    "id": "BSE + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_resub/M67testBG_vrot0_0/ensemble_output.json",
                    "color": "#f0e442",
                    "style": "solid",
                },
                "MS": {
                    "id": "BSE + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_resub/M67testBMS_vrot0_0/ensemble_output.json",
                    "color": "#cc79a7",
                    "style": "dashed",
                },
            },
            "MINT": {
                "Gauss": {
                    "id": "MINT + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_resub/M67testMG_vrot0_0/ensemble_output.json",
                    "color": "#0072b2",
                    "style": "solid",
                },
                "MS": {
                    "id": "MINT + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_resub/M67testMMS_vrot0_0/ensemble_output.json",
                    "color": "#009e73",
                    "style": "dashed",
                },
            },
        },
    },
    "NGC6819": {
        "cutoff": [1.5, 0.7],
        "output": "autostats6/NGC6819_oct22",
        "distributions": {
            "BSE": {
                "Gauss": {
                    "id": "BSE + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_resub/NGC6819testBG_vrot0_0/ensemble_output.json",
                    "color": "#f0e442",
                    "style": "solid",
                },
                "MS": {
                    "id": "BSE + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_resub/NGC6819testBMS_vrot0_0/ensemble_output.json",
                    "color": "#cc79a7",
                    "style": "dashed",
                },
            },
            "MINT": {
                "Gauss": {
                    "id": "MINT + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_resub/NGC6819testMG_vrot0_0/ensemble_output.json",
                    "color": "#0072b2",
                    "style": "solid",
                },
                "MS": {
                    "id": "MINT + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_resub/NGC6819testMMS_vrot0_0/ensemble_output.json",
                    "color": "#009e73",
                    "style": "dashed",
                },
            },
        },
    },
    "NGC188": {
        "cutoff": [1.7, 0.5],
        "output": "autostats6/NGC188_oct22",
        "distributions": {
            "BSE": {
                "Gauss": {
                    "id": "BSE + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_resub/NGC188testBG_vrot0_0/ensemble_output.json",
                    "color": "#f0e442",
                    "style": "solid",
                },
                "MS": {
                    "id": "BSE + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_resub/NGC188testBMS_vrot0_0/ensemble_output.json",
                    "color": "#cc79a7",
                    "style": "dashed",
                },
            },
            "MINT": {
                "Gauss": {
                    "id": "MINT + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_resub/NGC188testMG_vrot0_0/ensemble_output.json",
                    "color": "#0072b2",
                    "style": "solid",
                },
                "MS": {
                    "id": "MINT + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_resub/NGC188testMMS_vrot0_0/ensemble_output.json",
                    "color": "#009e73",
                    "style": "dashed",
                },
            },
        },
    },
    #####################################################################################################################################
    "NGC7789": {
        "cutoff": [100, 100],
        "output": "autostats6/NGC7789_oct22",
        "distributions": {
            "BSE": {
                "Gauss": {
                    "id": "BSE + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_last/NGC7789testBG_vrot0_0/ensemble_output.json",
                    "color": "#f0e442",
                    "style": "solid",
                },
                "MS": {
                    "id": "BSE + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_last/NGC7789testBMS_vrot0_0/ensemble_output.json",
                    "color": "#cc79a7",
                    "style": "dashed",
                },
            },
            "MINT": {
                "Gauss": {
                    "id": "MINT + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_last/NGC7789testMG_vrot0_0/ensemble_output.json",
                    "color": "#0072b2",
                    "style": "solid",
                },
                "MS": {
                    "id": "MINT + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_last/NGC7789testMMS_vrot0_0/ensemble_output.json",
                    "color": "#009e73",
                    "style": "dashed",
                },
            },
        },
    },
    "Tar": {
        "cutoff": [3, 1],
        "output": "autostats6/Tar_oct22",
        "distributions": {
            "BSE": {
                "Gauss": {
                    "id": "BSE + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_k2_fixed/TartestBG/ensemble_output.json",
                    "color": "#f0e442",
                    "style": "solid",
                },
                "MS": {
                    "id": "BSE + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_k2_fixed/TartestBMS/ensemble_output.json",
                    "color": "#cc79a7",
                    "style": "dashed",
                },
            },
            "MINT": {
                "Gauss": {
                    "id": "MINT + Gauss",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_renvfix/TartestMG_vrot0_0/ensemble_output.json",
                    "color": "#0072b2",
                    "style": "solid",
                },
                "MS": {
                    "id": "MINT + M&S",
                    "datafile": "/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_renvfix/TartestMMS_vrot0_0/ensemble_output.json",
                    "color": "#009e73",
                    "style": "dashed",
                },
            },
        },
    },
}


# runs={
#  "BSE":  { "Gauss": {
#                      "id"       : 'BSE + Gauss',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires/PleiadestestBG/ensemble_output.json',
#                      "color"    : '#f0e442',
#                      "style"    : 'solid'
#                    },
#           "MS" :   {
#                      "id"       : 'BSE + M&S  ',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires/PleiadestestBMS/ensemble_output.json',
#                      "color"    : '#cc79a7',
#                      "style"    : 'dashed'
#                    }
#           },
#  "MINT": { "Gauss": {
#                      "id"       : 'MINT + Gauss',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires/PleiadestestMG/ensemble_output.json',
#                      "color"    : '#0072b2',
#                      "style"    : 'solid'
#                    },
#           "MS" :   {
#                      "id"       : 'MINT + M&S ',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires/PleiadestestMMS/ensemble_output.json',
#                      "color"    : '#009e73',
#                      "style"    : 'dashed'
#                    }
#         }
#     }


# runs={
#  "BSE":  { "Gauss": {
#                      "id"       : 'BSE + Gauss',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires/HyPrtestBG/ensemble_output.json',
#                      "color"    : '#f0e442',
#                      "style"    : 'solid'
#                    },
##           "MS" :   {
#                      "id"       : 'BSE + M&S  ',
##                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires/HyPrtestBMS/ensemble_output.json',
#                      "color"    : '#cc79a7',
#                      "style"    : 'dashed'
#                    }
#           },
#  "MINT": { "Gauss": {
#                      "id"       : 'MINT + Gauss',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires/HyPrtestMG/ensemble_output.json',
#                      "color"    : '#0072b2',
##                      "style"    : 'solid'
#                    },
#           "MS" :   {
#                      "id"       : 'MINT + M&S ',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires/HyPrtestMMS/ensemble_output.json',
#                      "color"    : '#009e73',
#                      "style"    : 'dashed'
#                    }
#         }
#     }
#

# runs={
#  "BSE":  { "Gauss": {
#                      "id"       : 'BSE + Gauss',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_e0/HyPrtestBG/ensemble_output.json',
#                      "color"    : '#f0e442',
#                      "style"    : 'solid'
##                    },
#           "MS" :   {
#                      "id"       : 'BSE + M&S  ',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_e0/HyPrtestBMS/ensemble_output.json',
#                      "color"    : '#cc79a7',
#                      "style"    : 'dashed'
#                    }
#           },
#  "MINT": { "Gauss": {
#                      "id"       : 'MINT + Gauss',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_e0/HyPrtestMG/ensemble_output.json',
#                      "color"    : '#0072b2',
#                      "style"    : 'solid'
#                   },
#           "MS" :   {
#                      "id"       : 'MINT + M&S ',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_e0/HyPrtestMMS/ensemble_output.json',
#                      "color"    : '#009e73',
#                      "style"    : 'dashed'
#                    }
#         }
#     }
#

# runs={
#  "BSE":  { "Gauss": {
#                      "id"       : 'BSE + Gauss',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_e0/TartestBG/ensemble_output.json',
#                      "color"    : '#f0e442',
#                      "style"    : 'solid'
#                    },
#           "MS" :   {
#                      "id"       : 'BSE + M&S  ',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_e0/TartestBMS/ensemble_output.json',
#                      "color"    : '#cc79a7',
#                      "style"    : 'dashed'
#                    }
#           },
#  "MINT": { "Gauss": {
#                      "id"       : 'MINT + Gauss',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_e0/TartestMG/ensemble_output.json',
#                      "color"    : '#0072b2',
#                      "style"    : 'solid'
#                   },
#           "MS" :   {
#                      "id"       : 'MINT + M&S ',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_e0/TartestMMS/ensemble_output.json',
#                      "color"    : '#009e73',
#                      "style"    : 'dashed'
#                    }
#         }
#     }


# runs={
#  "BSE":  { "Gauss": {
#                      "id"       : 'BSE + Gauss',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_e0/NGC7789testBG/ensemble_output.json',
#                      "color"    : '#f0e442',
#                      "style"    : 'solid'
#                    },
#           "MS" :   {
#                      "id"       : 'BSE + M&S  ',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_e0/NGC7789testBMS/ensemble_output.json',
#                      "color"    : '#cc79a7',
#                      "style"    : 'dashed'
#                    }
#           },
#  "MINT": { "Gauss": {
#                      "id"       : 'MINT + Gauss',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_e0/NGC7789testMG/ensemble_output.json',
#                      "color"    : '#0072b2',
#                      "style"    : 'solid'
#                   },
#           "MS" :   {
#                      "id"       : 'MINT + M&S ',
#                      "datafile" : '/home/gio/Work/TIDES/populations_aug21/PARAMETERISED/hires_e0/NGC7789testMMS/ensemble_output.json',
#                      "color"    : '#009e73',
#                      "style"    : 'dashed'
#                    }
#         }
#  }
