"""
Function to test the general plot routine with new plots
https://stackoverflow.com/questions/22487296/multiprocessing-in-python-sharing-large-object-e-g-pandas-dataframe-between
https://dref360.github.io/managers/
"""

import os

from grav_waves.convolution.functions.general_plot_function import (
    general_plot_routine as general_sfr_convolution_plot_function,
)


def plot_convolution_results(
    convolution_plot_dir,
    results_filename,
    bootstraps=0,
    plot_without_channels=True,
    plot_channels_lieke=True,
    plot_channels_ppisn=True,
    plot_routine_kwargs={},
):
    """
    Function to plot the convolution results
    """

    # settings
    divide_by_binsize = False

    num_procs = 4
    num_procs_bootstrap = 8

    add_ratio = True
    add_cdf = True

    #
    os.makedirs(convolution_plot_dir, exist_ok=True)

    #######################
    # Run the plotter without any channels
    if plot_without_channels:
        no_channels_convolution_dir = os.path.join(convolution_plot_dir, "no_channels")

        general_output_pdf = os.path.join(
            no_channels_convolution_dir, "dataset_plots.pdf"
        )

        general_sfr_convolution_plot_function(
            results_filename,
            divide_by_binsize=divide_by_binsize,
            output_pdf_filename=general_output_pdf,
            main_plot_dir=no_channels_convolution_dir,
            add_ratio=False,
            add_cdf=False,
            bootstraps=bootstraps,
            num_procs=num_procs,
            num_procs_bootstrap=num_procs_bootstrap,
            **plot_routine_kwargs,
        )

    ######################
    # Run the plotter with liekes channels
    if plot_channels_lieke:
        liekes_channels = os.path.join(convolution_plot_dir, "channels_lieke")

        querylist_lieke = [
            {"name": "CE channel", "query": "(comenv_counter >= 1)"},
            {"name": "No CE channel", "query": "(comenv_counter==0)"},
        ]

        channels_lieke_convolution_without_detector_plot_output_pdf = os.path.join(
            liekes_channels, "dataset_plots_channels_lieke.pdf"
        )
        general_sfr_convolution_plot_function(
            results_filename,
            divide_by_binsize=divide_by_binsize,
            output_pdf_filename=channels_lieke_convolution_without_detector_plot_output_pdf,
            main_plot_dir=liekes_channels,
            querylist=querylist_lieke,
            add_ratio=add_ratio,
            add_cdf=add_cdf,
            bootstraps=bootstraps,
            num_procs=num_procs,
            num_procs_bootstrap=num_procs_bootstrap,
            **plot_routine_kwargs,
        )

    #######################
    # Run the plotter with ppisn channels
    if plot_channels_ppisn:
        ppisn_channels = os.path.join(convolution_plot_dir, "channels_ppisn")

        querylist_ppisn = [
            {
                "name": "No PPISN",
                "query": "((undergone_ppisn_1==0 & undergone_ppisn_2==0))",
            },
            {
                "name": "One PPISN",
                "query": "(((undergone_ppisn_1==0 & undergone_ppisn_2==1) | (undergone_ppisn_1==1 & undergone_ppisn_2==0)))",
            },
            {
                "name": "Two PPISN",
                "query": "((undergone_ppisn_1==1 & undergone_ppisn_2==1))",
            },
        ]

        channels_ppisn_convolution_plot_output_pdf = os.path.join(
            ppisn_channels, "dataset_plots_channels_ppisn.pdf"
        )

        #
        general_sfr_convolution_plot_function(
            results_filename,
            divide_by_binsize=divide_by_binsize,
            output_pdf_filename=channels_ppisn_convolution_plot_output_pdf,
            main_plot_dir=ppisn_channels,
            querylist=querylist_ppisn,
            add_ratio=add_ratio,
            add_cdf=add_cdf,
            bootstraps=bootstraps,
            num_procs=num_procs,
            num_procs_bootstrap=num_procs_bootstrap,
            **plot_routine_kwargs,
        )


if __name__ == "__main__":

    ## Select file
    # fiducial lower res
    rebinned_results_filename = "output/output_high_res/convolution/rebinned_convolution_without_detection_probability.h5"
    convolution_plot_dir = "output/output_high_res/convolution_plots/"

    # fiducial
    rebinned_results_filename = "output/output_high_res/convolution/rebinned_convolution_without_detection_probability.h5"
    convolution_plot_dir = "output/output_high_res/convolution_plots/"

    # # 10 extra mass loss
    # rebinned_results_filename = 'output/output_high_res_ten_massloss/convolution/rebinned_convolution_without_detection_probability.h5'
    # convolution_plot_dir = 'output/output_high_res_ten_massloss/convolution_plots/'

    # # Five core mass shift
    # rebinned_results_filename = 'output/output_high_res_five_coreshift/convolution/rebinned_convolution_without_detection_probability.h5'
    # convolution_plot_dir = 'output/output_high_res_five_coreshift/convolution_plots/'

    # fiducial lower res
    # rebinned_results_filename = '/home/david/projects/binary_c_root/results/GRAV_WAVES/multiprocessing_test2/convolution/without_detection_probability/rebinned_convolution_results.h5'
    # convolution_plot_dir = '/home/david/projects/binary_c_root/results/GRAV_WAVES/MID_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-5_CORE_MASS_SHIFT/convolution_plots/test/'

    rebinned_results_filename = "/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/convolution/scripts/new_main/output/array_test/convolution/without_detection_probability/rebinned_convolution_results.h5"
    convolution_plot_dir = "/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/convolution/scripts/new_main/output/array_test/convolution/without_detection_probability/plots/"

    plot_convolution_results(
        convolution_plot_dir=convolution_plot_dir,
        results_filename=rebinned_results_filename,
        bootstraps=5,
        plot_without_channels=True,
        plot_channels_lieke=False,
        plot_channels_ppisn=False,
        plot_routine_kwargs={
            # SFR and birth rate
            "include_starformation_rate_plots": False,
            "include_plot_birthrate": False,
            # (1d) Plot at 0 redshift: mass and mass ratio
            "include_plot_any_mass_at_zero_redshift": True,
            "include_massratio_density_at_zero_redshift": True,
            "include_merger_and_formation_rate_densities_at_redshift_zero_plots": True,
            # (2d) Plot at 0 redshift varying other values
            "include_plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_metallicity": False,
            # Mass and mass ratio and rate over entire redshift
            "include_merger_and_formation_rate_densities_over_redshift_plots": False,
            #
            "include_plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity": True,
            "include_plot_primary_vs_secondary_at_specific_redshift": False,
            "include_plot_convolved_rate_density_at_specific_redshift_for_mass_quantity_per_metallicity": True,
            "include_plot_convolved_rate_density_per_metallicity": False,
            "include_convolved_rate_density_per_mass_ratio": False,
            "include_total_intrinsic_merger_rates_over_redshift_plots": False,
            # Interpolation analysis
            "include_interpolation_error_plot": False,
        },
    )
