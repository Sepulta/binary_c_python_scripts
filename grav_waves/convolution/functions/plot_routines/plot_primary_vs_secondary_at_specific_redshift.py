"""
Plot for birth redshift per metallicity
"""

import json
import h5py
import numpy as np
import pandas as pd

import astropy.units as u

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors

from david_phd_functions.grav_waves.dataframe_functions import add_chirpmass_column
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.pdf_functions import (
    add_pdf_and_bookmark,
    generate_chapterpage_pdf,
)
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()
from grav_waves.gw_analysis.functions.cosmology_functions import (
    redshift_to_lookback_time,
)
from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    filter_dco_type,
)
from grav_waves.convolution.functions.redshift_interpolation_functions import (
    load_interpolation_data,
)
from david_phd_functions.plotting.canvas_functions import return_canvas_with_subsets


def get_data(
    hdf5_filename,
    dco_type,
    convolution_configuration,
    redshift_key,
    primary_mass_bins,
    secondary_mass_bins,
    rate_key_in_df,
    rate_type_hdf5_key,
    rate_time_key_in_df,
    divide_by_mass_bins,
    birth_redshift_bins=None,
    query=None,
):
    """
    Function to get the data out of the hdf5 file and query if necessary

    if birth_redshift_bins is None we use the data to dictate the bins
    """

    #
    redshift_interpolators = load_interpolation_data(convolution_configuration)

    # calculat ecurrent lookback time
    current_lookback_value = redshift_to_lookback_time(float(redshift_key))

    # Get combined df
    combined_df = pd.read_hdf(hdf5_filename, "/data/combined_dataframes")

    # Add columns
    combined_df = add_chirpmass_column(combined_df, m1_name="mass_1", m2_name="mass_2")
    combined_df["total_mass"] = combined_df["mass_1"] + combined_df["mass_2"]
    combined_df["primary_mass"] = combined_df[["mass_1", "mass_2"]].max(axis=1)
    combined_df["secondary_mass"] = combined_df[["mass_1", "mass_2"]].min(axis=1)

    # readout the dataframe and query if needed
    index_df = pd.read_hdf(
        hdf5_filename, "/data/{}/{}".format(rate_type_hdf5_key, str(redshift_key))
    )
    joined_df = index_df.join(combined_df, on="local_index")

    # Filter the dco type:
    joined_df = filter_dco_type(joined_df, dco_type)

    if not query is None:
        joined_df = joined_df.query(query)

    #
    hist = np.histogram2d(
        joined_df["primary_mass"],
        joined_df["secondary_mass"],
        bins=[primary_mass_bins, secondary_mass_bins],
        weights=joined_df[rate_key_in_df],
    )

    return hist[0]


def plot_primary_vs_secondary_at_specific_redshift(
    convolution_dataset_hdf5_filename,
    dco_type,
    rate_type,
    primary_mass_bins,
    secondary_mass_bins,
    querylist=None,
    divide_by_binsize=True,
    add_ratio=False,
    add_cdf=False,
    plot_settings=None,
):
    """
    Test function to plot things with the new datastructure
    """

    contourlevels = [1e-1, 1e0, 1e1]
    linestyles_contourlevels = ["solid", "--", "-."]

    ratio_contourlevels = [1e1, 1e2, 1e3]
    linestyles_ratio_contourlevels = ["solid", "--", "-."]

    norm_ratio = colors.Normalize(vmin=0, vmax=1)
    ratio_contourlevels = [0.1, 0.5, 0.90]

    # Select correct rate type:
    if rate_type == "merger_rate":
        rate_key_in_dict = "merger_rate_data"
        rate_key_in_df = "merger_rates"
        long_name_rate_type = "Merger rate"
        rate_type_hdf5_key = "merger_rate_data"
        rate_time_key_in_df = "merger_time_values_in_years"
    elif rate_type == "formation_rate":
        rate_key_in_dict = "formation_rate_data"
        rate_key_in_df = "formation_rates"
        long_name_rate_type = "Formation rate"
        rate_type_hdf5_key = "formation_rate_data"
        rate_time_key_in_df = "formation_time_values_in_years"

    #
    hdf5_file = h5py.File(convolution_dataset_hdf5_filename, "r")

    settings = hdf5_file["settings"]
    convolution_configuration = json.loads(settings["convolution_configuration"][()])

    # Determine mass bins
    primary_mass_binsize = np.diff(primary_mass_bins)
    primary_mass_bincenter = (primary_mass_bins[1:] + primary_mass_bins[:-1]) / 2

    secondary_mass_binsize = np.diff(secondary_mass_bins)
    secondary_mass_bincenter = (secondary_mass_bins[1:] + secondary_mass_bins[:-1]) / 2

    #
    zero_key = str(
        min([float(el) for el in list(hdf5_file["data"][rate_type_hdf5_key].keys())])
    )

    hdf5_file.close()

    # Get the unit correct
    number_density_per_year = 1 / u.Gpc**3 / u.yr
    plotting_unit = number_density_per_year

    if divide_by_binsize:
        plotting_unit = plotting_unit / u.Msun

    # Calculate the data:
    results_dict = {}
    results_dict["all"] = get_data(
        convolution_dataset_hdf5_filename,
        dco_type=dco_type,
        convolution_configuration=convolution_configuration,
        redshift_key=zero_key,
        primary_mass_bins=primary_mass_bins,
        secondary_mass_bins=secondary_mass_bins,
        rate_key_in_df=rate_key_in_df,
        rate_type_hdf5_key=rate_type_hdf5_key,
        rate_time_key_in_df=rate_time_key_in_df,
        divide_by_mass_bins=divide_by_binsize,
    )

    if not querylist is None:
        for query_dict in querylist:
            results_dict[query_dict["name"]] = get_data(
                convolution_dataset_hdf5_filename,
                dco_type=dco_type,
                convolution_configuration=convolution_configuration,
                redshift_key=zero_key,
                primary_mass_bins=primary_mass_bins,
                secondary_mass_bins=secondary_mass_bins,
                rate_key_in_df=rate_key_in_df,
                rate_type_hdf5_key=rate_type_hdf5_key,
                rate_time_key_in_df=rate_time_key_in_df,
                divide_by_mass_bins=divide_by_binsize,
                query=query_dict["query"],
            )

    ####
    # Set up figure and plots
    if querylist is None:
        num_subsets = 0
    else:
        num_subsets = len(querylist)

    #
    fig, gs, axes_dict = return_canvas_with_subsets(
        num_subsets, add_ratio_axes=add_ratio
    )

    X, Y = np.meshgrid(primary_mass_bincenter, secondary_mass_bincenter)

    norm = colors.LogNorm(
        vmin=10
        ** np.floor(
            np.log10(results_dict["all"].max())
            - plot_settings.get("probability_floor", 4)
        ),
        vmax=results_dict["all"].max(),
    )

    # Plot the results
    pcm = axes_dict["all_axis"].pcolormesh(
        X,
        Y,
        results_dict["all"].T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )

    # Plot the subsets
    if not querylist is None:
        for query_i, query_dict in enumerate(querylist):
            X, Y = np.meshgrid(primary_mass_bincenter, secondary_mass_bincenter)
            pcm = axes_dict["subset_axes"][query_i].pcolormesh(
                X,
                Y,
                results_dict[query_dict["name"]].T,
                norm=norm,
                shading="auto",
                antialiased=plot_settings.get("antialiased", False),
                rasterized=plot_settings.get("rasterized", False),
            )

            axes_dict["subset_axes"][query_i].set_title(
                "{}: {}".format(query_dict["name"], long_name_rate_type), fontsize=18
            )
            axes_dict["subset_axes"][0].set_ylabel("Metallicity [Z]")

            if query_i > 0:
                axes_dict["subset_axes"][query_i].set_yticklabels([])

            # Add ratio plots
            if add_ratio:
                ratio_to_total = np.divide(
                    results_dict[query_dict["name"]].T,
                    results_dict["all"].T,
                    out=np.zeros_like(results_dict[query_dict["name"]].T),
                    where=results_dict["all"].T != 0,
                )  # only divide nonzeros else 1

                ratio_to_total[ratio_to_total == 0] = np.nan
                pcm_ratio = axes_dict["ratio_axes"][query_i].pcolormesh(
                    X,
                    Y,
                    ratio_to_total,
                    norm=norm_ratio,
                    shading="auto",
                    antialiased=plot_settings.get("antialiased", False),
                    rasterized=plot_settings.get("rasterized", False),
                )

                axes_dict["ratio_axes"][query_i].set_title(
                    "{}: Ratio".format(query_dict["name"]), fontsize=18
                )
                axes_dict["ratio_axes"][0].set_ylabel("Metallicity [Z]")
                if query_i > 0:
                    axes_dict["ratio_axes"][query_i].set_yticklabels([])

    # make colorbar
    cb = matplotlib.colorbar.ColorbarBase(
        axes_dict["colorbar_axis"], norm=norm, extend="min"
    )
    # cb.ax.set_ylabel("Rate density [{}]".format(main_result_units.unit.to_string('latex_inline')))

    if add_ratio:
        # Consider using a logscale for the ratio
        cb_ratio = matplotlib.colorbar.ColorbarBase(
            axes_dict["colorbar_axis_ratio"],
            norm=norm_ratio,
        )
        cb_ratio.ax.set_ylabel("Ratio to total")

    # Set contourlevels and lines on the colorbar
    if (
        (not contourlevels is None)
        and (not linestyles_contourlevels is None)
        and (len(contourlevels) == len(linestyles_contourlevels))
    ):
        _ = axes_dict["all_axis"].contour(
            X,
            Y,
            results_dict["all"].T,
            levels=contourlevels,
            colors="k",
            linestyles=linestyles_contourlevels,
        )

        # Set lines on the colorbar
        for contour_i, _ in enumerate(contourlevels):
            cb.ax.plot(
                [cb.ax.get_xlim()[0], cb.ax.get_xlim()[1]],
                [contourlevels[contour_i]] * 2,
                "black",
                linestyle=linestyles_contourlevels[contour_i],
            )

        # Add contours on the subplots
        if not querylist is None:
            for query_i, query_dict in enumerate(querylist):
                _ = axes_dict["subset_axes"][query_i].contour(
                    X,
                    Y,
                    results_dict[query_dict["name"]].T,
                    levels=contourlevels,
                    colors="k",
                    linestyles=linestyles_contourlevels,
                )

                if add_ratio:
                    ratio_to_total = np.divide(
                        results_dict[query_dict["name"]].T,
                        results_dict["all"].T,
                        out=np.zeros_like(results_dict[query_dict["name"]].T),
                        where=results_dict["all"].T != 0,
                    )  # only divide nonzeros else 1

                    # Add contourlines
                    if (
                        (not ratio_contourlevels is None)
                        and (not linestyles_ratio_contourlevels is None)
                        and (
                            len(ratio_contourlevels)
                            == len(linestyles_ratio_contourlevels)
                        )
                    ):
                        _ = axes_dict["ratio_axes"][query_i].contour(
                            X,
                            Y,
                            ratio_to_total,
                            levels=ratio_contourlevels,
                            colors="k",
                            linestyles=linestyles_ratio_contourlevels,
                        )

                    # Set lines on the colorbar
                    for contour_i, _ in enumerate(ratio_contourlevels):
                        cb_ratio.ax.plot(
                            [cb_ratio.ax.get_xlim()[0], cb_ratio.ax.get_xlim()[1]],
                            [ratio_contourlevels[contour_i]] * 2,
                            "black",
                            linestyle=linestyles_ratio_contourlevels[contour_i],
                        )

    ###
    #
    axes_dict["all_axis"].set_ylabel(r"Secondary mass [M$_{\odot}$]")
    axes_dict["all_axis"].set_xlabel(r"Primary mass [M$_{\odot}$]")

    #
    title_text = "{} density of BHBH systems primary vs secondary mass".format(
        long_name_rate_type.lower()
    )
    axes_dict["all_axis"].set_title(title_text, fontsize=26)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    fig.subplots_adjust(hspace=0.5)
    show_and_save_plot(fig, plot_settings)


matplotlib.use("Agg")

# # Global settings
# rebinned_results_filename = '/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/convolution/scripts/new_main/output/high_rebinned_convolution_without_detection_probability.h5'

# convolution_plot_dir = 'output/'
# divide_by_binsize = False

# querylist = [
#     {'name': 'CE channel', 'query': '(comenv_counter >= 1)'},
#     {'name': 'No CE channel', 'query': '(comenv_counter==0)'}
# ]
# # querylist = [
# #     {'name': 'No PPISN', 'query': '((undergone_ppisn_1==0 & undergone_ppisn_2==0))'},
# #     {'name': 'One PPISN', 'query': '(((undergone_ppisn_1==0 & undergone_ppisn_2==1) | (undergone_ppisn_1==1 & undergone_ppisn_2==0)))'},
# #     {'name': 'Two PPISN', 'query': '((undergone_ppisn_1==1 & undergone_ppisn_2==1))'}
# # ]


# plot_primary_vs_secondary_at_specific_redshift(
#     rebinned_results_filename,
#     divide_by_binsize=False,
#     rate_type='merger_rate',

#     primary_mass_bins=np.arange(0, 70, 1),
#     secondary_mass_bins=np.arange(0, 60, 1),

#     add_ratio=True,
#     querylist=querylist,

#     plot_settings={
#         'show_plot': True,
#         'output_name': 'output/plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity.pdf',
#         'simulation_name': 'test',
#         'antialiased': True,
#         'rasterized': True,
#         'runname': 'plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity',
#     }
# )
