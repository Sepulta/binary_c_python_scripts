"""
Function to plot some stuff with the new structure
TODO: fix that the numbers that we plot match the total intrinsic rate numbers
https://en.wikipedia.org/wiki/Kernel_density_estimation
"""

import os
import json

import h5py
import numpy as np
import pandas as pd
import astropy.units as u

import matplotlib
import matplotlib.pyplot as plt

from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    filter_dco_type,
    long_name_dict,
    get_rate_type_names,
    linestyle_list,
    add_contour_levels,
    plot_2d_results,
    get_num_subsets,
)

from grav_waves.gw_analysis.functions.cosmology_functions import (
    redshift_to_lookback_time,
)

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.canvas_functions import return_canvas_with_subsets
from david_phd_functions.grav_waves.dataframe_functions import add_chirpmass_column

custom_mpl_settings.load_mpl_rc()


def plot_zams_mass_any_merging_at_specific_redshift():
    """
    Function that plots mass of any of the components merging system on the x-axis, and the corresponding ZAMS mass on the y-axis
    """

    pass


def plot_zams_mass_primary_secondary_at_specific_redshift():
    """
    Function that plots ZAMS mass of primary on the x-axis and ZAMS mass of secondary on the y-axis.
    """

    pass


def get_data(
    hdf5_filename,
    dco_type,
    convolution_configuration,
    redshift_key,
    mass_bins,
    mass_quantity,
    zams_mass_bins,
    zams_mass_quantity,
    rate_key_in_df,
    rate_type_hdf5_key,
    rate_time_key_in_df,
    divide_by_mass_bins,
    birth_redshift_bins=None,
    query=None,
):
    """
    Function to get the data out of the hdf5 file and query if necessary

    if birth_redshift_bins is None we use the data to dictate the bins
    """

    # calculat ecurrent lookback time
    current_lookback_value = redshift_to_lookback_time(float(redshift_key))

    # Get combined df
    combined_df = pd.read_hdf(hdf5_filename, "/data/combined_dataframes")

    # Add columns for current mass quantities
    combined_df = add_chirpmass_column(
        combined_df, m1_name="mass_1", m2_name="mass_2", chirpmass_name="chirpmass"
    )
    combined_df["total_mass"] = combined_df["mass_1"] + combined_df["mass_2"]
    combined_df["primary_mass"] = combined_df[["mass_1", "mass_2"]].max(axis=1)
    combined_df["secondary_mass"] = combined_df[["mass_1", "mass_2"]].min(axis=1)

    # Add columns for zams mass quantities
    combined_df = add_chirpmass_column(
        combined_df,
        m1_name="zams_mass_1",
        m2_name="zams_mass_2",
        chirpmass_name="zams_chirpmass",
    )
    combined_df["zams_total_mass"] = (
        combined_df["zams_mass_1"] + combined_df["zams_mass_2"]
    )

    indices_primary = np.where(
        combined_df["mass_1"] >= combined_df["mass_2"], True, False
    )
    indices_secondary = np.where(
        combined_df["mass_1"] >= combined_df["mass_2"], False, True
    )

    # Construct primary masses
    combined_df["zams_primary_mass"] = 0
    combined_df["zams_primary_mass"][indices_primary] = combined_df["zams_mass_1"][
        indices_primary
    ]
    combined_df["zams_primary_mass"][indices_secondary] = combined_df["zams_mass_2"][
        indices_secondary
    ]

    #
    combined_df["zams_secondary_mass"] = 0
    combined_df["zams_secondary_mass"][indices_primary] = combined_df["zams_mass_2"][
        indices_primary
    ]
    combined_df["zams_secondary_mass"][indices_secondary] = combined_df["zams_mass_1"][
        indices_secondary
    ]

    # readout the dataframe and query if needed
    index_df = pd.read_hdf(
        hdf5_filename, "/data/{}/{}".format(rate_type_hdf5_key, str(redshift_key))
    )
    joined_df = index_df.join(combined_df, on="local_index")

    # Filter the dco type:
    joined_df = filter_dco_type(joined_df, dco_type)

    if not query is None:
        joined_df = joined_df.query(query)

    hist = np.histogram2d(
        joined_df[zams_mass_quantity],
        joined_df[mass_quantity],
        bins=[zams_mass_bins, mass_bins],
        weights=joined_df[rate_key_in_df],
    )

    return hist[0]


def plot_zams_mass_mass_quantity_merging_at_specific_redshift(
    convolution_dataset_hdf5_filename,
    dco_type,
    mass_quantity,
    mass_bins,
    zams_mass_quantity,
    zams_mass_bins,
    rate_type,
    querylist=None,
    divide_by_binsize=True,
    add_ratio=False,
    add_cdf=False,
    plot_settings=None,
):
    """
    Test function to plot things with the new datastructure
    """

    contourlevels = [1e-1, 1e0, 1e1]
    ratio_contourlevels = [1e1, 1e2, 1e3]
    long_name_quantity = long_name_dict[mass_quantity]
    (
        _,
        rate_key_in_df,
        long_name_rate_type,
        rate_type_hdf5_key,
        rate_time_key_in_df,
    ) = get_rate_type_names(rate_type)

    #
    hdf5_file = h5py.File(convolution_dataset_hdf5_filename, "r")
    settings = hdf5_file["settings"]
    convolution_configuration = json.loads(settings["convolution_configuration"][()])
    zero_key = str(
        min([float(el) for el in list(hdf5_file["data"][rate_type_hdf5_key].keys())])
    )
    hdf5_file.close()

    # Determine mass bins
    mass_bincenter = (mass_bins[1:] + mass_bins[:-1]) / 2
    zams_mass_bincenter = (zams_mass_bins[1:] + zams_mass_bins[:-1]) / 2

    # Get the unit correct
    number_density_per_year = 1 / u.Gpc**3 / u.yr
    plotting_unit = number_density_per_year

    if divide_by_binsize:
        plotting_unit = plotting_unit / u.Msun

    # Calculate the data:
    results_dict = {}
    results_dict["all"] = get_data(
        convolution_dataset_hdf5_filename,
        dco_type=dco_type,
        convolution_configuration=convolution_configuration,
        redshift_key=zero_key,
        mass_bins=mass_bins,
        mass_quantity=mass_quantity,
        zams_mass_bins=zams_mass_bins,
        zams_mass_quantity=zams_mass_quantity,
        rate_key_in_df=rate_key_in_df,
        rate_type_hdf5_key=rate_type_hdf5_key,
        rate_time_key_in_df=rate_time_key_in_df,
        divide_by_mass_bins=divide_by_binsize,
    )

    if not querylist is None:
        for query_dict in querylist:
            results_dict[query_dict["name"]] = get_data(
                convolution_dataset_hdf5_filename,
                dco_type=dco_type,
                convolution_configuration=convolution_configuration,
                redshift_key=zero_key,
                mass_bins=mass_bins,
                mass_quantity=mass_quantity,
                rate_key_in_df=rate_key_in_df,
                rate_type_hdf5_key=rate_type_hdf5_key,
                rate_time_key_in_df=rate_time_key_in_df,
                divide_by_mass_bins=divide_by_binsize,
                zams_mass_bins=zams_mass_bins,
                zams_mass_quantity=zams_mass_quantity,
                query=query_dict["query"],
            )

    ####
    # Set up figure and plots
    num_subsets = get_num_subsets(querylist)

    fig = plt.figure(figsize=(40, 40))
    fig, _, axes_dict = return_canvas_with_subsets(
        num_subsets, fig=fig, add_ratio_axes=add_ratio
    )

    X, Y = np.meshgrid(mass_bincenter, zams_mass_bincenter)

    ##
    # Plot 2-d results
    fig, axes_dict, cb, cb_ratio = plot_2d_results(
        fig=fig,
        axes_dict=axes_dict,
        X=X,
        Y=Y,
        long_name=long_name_quantity,
        add_ratio=add_ratio,
        results_dict=results_dict,
        querylist=querylist,
        plot_settings=plot_settings,
        x_scale="linear",
    )

    ##
    # Add contour levels
    fig, axes_dict = add_contour_levels(
        results_dict=results_dict,
        X=X,
        Y=Y,
        fig=fig,
        axes_dict=axes_dict,
        cb=cb,
        cb_ratio=cb_ratio,
        contourlevels=contourlevels,
        ratio_contourlevels=ratio_contourlevels,
        linestyle_list=linestyle_list,
        querylist=querylist,
        add_ratio=add_ratio,
    )

    ###
    #
    axes_dict["all_axis"].set_ylabel(r"%s [M$_{\odot}$]" % long_name_quantity)

    #
    title_text = "{} density of merging BHBH systems per {} per birth redshift".format(
        long_name_rate_type.lower(), long_name_quantity.lower()
    )
    axes_dict["all_axis"].set_title(title_text, fontsize=26)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    # Global settings
    rebinned_results_filename = "/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/convolution/scripts/new_main/output/array_test/convolution/without_detection_probability/rebinned_convolution_results.h5"
    convolution_plot_dir = "output/test/"
    divide_by_binsize = False

    querylist_lieke = [
        {"name": "CE channel", "query": "(comenv_counter >= 1)"},
        {
            "name": "No CE channel",
            "query": "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter==0)",
        },
    ]

    #
    plot_zams_mass_mass_quantity_merging_at_specific_redshift(
        rebinned_results_filename,
        dco_type="bhbh",
        mass_quantity="total_mass",
        mass_bins=np.arange(0, 120, 1),
        zams_mass_quantity="zams_total_mass",
        zams_mass_bins=np.arange(0, 300, 5),
        add_ratio=True,
        divide_by_binsize=False,
        rate_type="merger_rate",
        querylist=querylist_lieke,
        plot_settings={
            "show_plot": True,
            "output_name": os.path.join(
                convolution_plot_dir,
                "plot_zams_mass_mass_quantity_merging_at_specific_redshift.pdf",
            ),
            "simulation_name": "test",
            "antialiased": True,
            "rasterized": True,
            "runname": "plot_zams_mass_mass_quantity_merging_at_specific_redshift",
        },
    )
else:
    matplotlib.use("Agg")
