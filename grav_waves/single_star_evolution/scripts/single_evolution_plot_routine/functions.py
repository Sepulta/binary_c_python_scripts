import os
import numpy as np
import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams["axes.facecolor"] = ".9"
import proplot as pplt

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

from david_phd_functions.binaryc.personal_defaults import personal_defaults

from binarycpython.utils.grid import Population

from binarycpython.utils import stellar_types

import astropy.units as u
import astropy.constants as const

from david_phd_functions.plotting.pdf_functions import (
    add_pdf_and_bookmark,
    generate_chapterpage_pdf,
)
from PyPDF2 import PdfFileMerger


defaults_settings = {
    "wind_mass_loss": 2,
    "PPISN_prescription": 2,
    "metallicity": 0.002,
    "M_1": 10,
}


def luminosity_from_temperature_for_given_radius(temperature, radius):
    """
    Function to convert the temperature and radius to luminosity

    input without units but they are assumed to be kelvin and solar radius
    """

    luminosity = (
        4
        * np.pi
        * np.power(radius * u.Rsun, 2)
        * const.sigma_sb
        * np.power(temperature * u.K, 4)
    )

    return luminosity.to(u.Lsun).value


def temperature_from_luminosity_for_given_radius(luminosity, radius):
    """
    Function to convert the temperature and radius to luminosity

    input without units but they are assumed to be kelvin and solar radius
    """

    temperature = np.power(
        (luminosity * u.Lsun)
        / (4 * np.pi * np.power(radius * u.Rsun, 2) * const.sigma_sb),
        1 / 4,
    )

    return temperature.to(u.K).value


custom_logging_string_detailed_evolution = """
Printf("DAVID_DETAILED_EVOLUTION %30.12e " // 1
    "%g %g %d %g " // 2-5
    "%g %g %g %g " // 6-9
    "%g %g\\n", // 10-11

    //
    stardata->model.time, // 1

    stardata->star[0].mass, // 2
    stardata->common.zero_age.mass[0], // 3
    stardata->star[0].stellar_type, // 4
    stardata->star[0].luminosity, // 5

    Teff(0), // 6
    stardata->star[0].radius, // 7
    stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS], // 8
    stardata->star[0].core_mass[ID_core(stardata->star[0].stellar_type)],           // 9

    stardata->star[0].core_mass[CORE_CO],     // 10
    stardata->star[0].core_mass[CORE_He]    // 11
);


if(stardata->star[0].SN_type != SN_NONE)
{
    if (stardata->model.time < stardata->model.max_evolution_time)
    {
        if(stardata->pre_events_stardata != NULL)
        {
            Printf("DAVID_SN %30.12e " // 1
                "%g %g %g %d " // 2-5
                "%d %d %g %g " // 6-9
                "%g %g %g\\n", // 10-14

                //
                stardata->model.time, // 1

                stardata->star[0].mass, //2
                stardata->pre_events_stardata->star[0].mass, //3
                stardata->common.zero_age.mass[0], //4
                stardata->star[0].SN_type, //5

                stardata->star[0].stellar_type, //6
                stardata->pre_events_stardata->star[0].stellar_type, //7
                stardata->model.probability, //8
                stardata->pre_events_stardata->star[0].core_mass[ID_core(stardata->pre_events_stardata->star[0].stellar_type)],           // 9

                stardata->pre_events_stardata->star[0].core_mass[CORE_CO],     // 10
                stardata->pre_events_stardata->star[0].core_mass[CORE_He],    // 11
                stardata->common.metallicity //14
            );
        }
    };
    /* Kill the simulation to save time */
    stardata->model.max_evolution_time = stardata->model.time - stardata->model.dtm;
};
"""


def parse_function(self, output):
    """ """

    result_list = []
    supernova_result_list = []

    params = [
        "time",
        "mass",
        "zams_mass",
        "stellar_type",
        "luminosity",
        "Teff",
        "radius",
        "wind_mass_loss_rate",
        "core_mass",
        "co_core_mass",
        "he_core_mass",
    ]

    # params
    supernova_params = [
        "time",
        "mass",
        "pre_sn_mass",
        "zams_mass",
        "sn_type",
        "stellar_type",
        "pre_sn_stellar_type",
        "probability",
        "pre_sn_core_mass",
        "pre_sn_co_core_mass",
        "pre_sn_he_core_mass",
        "metallicity",
    ]

    # Go over output
    if output:
        for line in output.splitlines():
            if line.startswith("DAVID_DETAILED_EVOLUTION"):
                values = line.split()[1:]

                if not len(values) == len(params):
                    raise ValueError("Not the same number of values as params")

                result_list.append([float(value) for value in values])

            if line.startswith("DAVID_SN"):
                supernova_values = line.split()[1:]

                if not len(supernova_values) == len(supernova_params):
                    raise ValueError("Not the same number of values as params")

                supernova_result_list.append(
                    {
                        supernova_params[i]: float(supernova_values[i])
                        for i in range(len(supernova_values))
                    }
                )

    #
    evolution_dataframe = pd.DataFrame(result_list, columns=params)

    # Limit the output to the first step of the last stellar type
    last_type = sorted(evolution_dataframe["stellar_type"].unique())[-1]

    #
    first_time_last_type = evolution_dataframe[
        evolution_dataframe.stellar_type == last_type
    ]["time"].iloc[0]

    return (
        supernova_result_list,
        evolution_dataframe[evolution_dataframe.time < first_time_last_type],
    )
    # return supernova_result_list, evolution_dataframe[evolution_dataframe.time <= first_time_last_type]


def generate_evolution_data(**kwargs):
    """
    Function to generate the evolution data
    """

    # Set up population
    test_pop = Population()

    # Load personal defaults
    test_pop.set(**personal_defaults)

    # Add situational settings
    test_pop.set(
        C_logging_code=custom_logging_string_detailed_evolution,
        multiplicity=1,
        verbosity=1,
        amt_cores=1,
        parse_function=parse_function,
        BH_prescription=4,
        save_pre_events_stardata=1,  # To catch the pre-event stuff,
        minimum_timestep=1e-8,
    )

    # Set with kwargs
    test_pop.set(
        wind_mass_loss=kwargs.get(
            "wind_mass_loss", defaults_settings["wind_mass_loss"]
        ),
        PPISN_prescription=kwargs.get(
            "PPISN_prescription", defaults_settings["PPISN_prescription"]
        ),
        metallicity=kwargs.get("metallicity", defaults_settings["metallicity"]),
        M_1=kwargs.get("M_1", defaults_settings["M_1"]),
    )

    # Get version info of the binary_c build
    version_info = test_pop.return_binary_c_version_info(parsed=True)

    # create local tmp_dir
    test_pop.set(tmp_dir=os.path.join(os.path.dirname(__file__), "local_tmp_dir"))
    os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

    # Stop the script if the configuration is wrong
    if version_info["macros"]["MINT"] == "on":
        print("Please disable MINT")
        quit()
    if version_info["macros"]["NUCSYN"] == "on":
        print("Please disable NUCSYN")
        quit()

    evolution_dataframe = test_pop.evolve_single()

    return evolution_dataframe
