"""
Main point for running all the routines in a sequence
"""
import os
import copy
import numpy as np

from grav_waves.settings import (
    convolution_settings,
    config_dict_cosmology,
    liekes_channels_query_dict,
    ppisn_query_dict,
)
from generate_whole_sequence import generate_whole_sequence
from general_sfr_convolution_plot_function_with_subsets import (
    general_sfr_convolution_plot_function_with_subsets,
)

########
# Configure the settings for the datasets
# Load dataset
# from grav_waves.gw_analysis.server_datasets import dataset_dict
from grav_waves.gw_analysis.laptop_datasets import dataset_dict

MAIN_RESULT_DIR = os.path.join(os.getenv("BINARYC_DATA_ROOT"), "GRAV_WAVES")

quit()
# List to loop over
DATASET_KEY_LIST = [
    "MID_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW",
]

PLOT_TYPE_KEY_LIST = [
    "bhbh",
]

convolution_output_dir = os.path.join(os.path.abspath("results"))
main_plot_output_dir = os.path.abspath("plots")

# data info
data_info = {
    # Input
    "dataset_dict": dataset_dict,
    "dataset_name": DATASET_KEY_LIST[0],
    "type_key": PLOT_TYPE_KEY_LIST[0],
    "main_result_dir": MAIN_RESULT_DIR,
    # output
    "main_plot_output_dir": main_plot_output_dir,
    "main_convolution_output_dir": convolution_output_dir,
}

####################
# Configure settings for the cosmology settings we use
# Load the default cosmology configuration
cosmology_configuration = copy.deepcopy(config_dict_cosmology)
cosmology_configuration["metallicity_distribution_function"] = "coen19"

#########################
# Configure settings for the convolution settings we use
# Load the default convolution configuration
convolution_configuration = copy.deepcopy(convolution_settings)
convolution_configuration["amt_cores"] = 4

#
time_stepsize = 0.05

# do redshift loop
redshift_max_time = 9
convolution_configuration["time_type"] = "redshift"

# Set the time stuff in the convolution config
convolution_configuration["stepsize"] = time_stepsize
convolution_configuration["time_bins"] = np.arange(
    0.0001 - 0.5 * time_stepsize, redshift_max_time + 0.5 * time_stepsize, time_stepsize
)
convolution_configuration["time_centers"] = (
    convolution_configuration["time_bins"][1:]
    + convolution_configuration["time_bins"][:-1]
) / 2

convolution_configuration["min_loop_time"] = 0
convolution_configuration["max_loop_time"] = redshift_max_time

###########
# Set up the queries that we'll use
# queries = {
#     'all': None,
#     'CE_channel': liekes_channels_query_dict['channel_1'],
#     'STABLE_channel': liekes_channels_query_dict['channel_2']
# }

queries = {
    "all": None,
    "no_ppi": ppisn_query_dict["no_ppisn"],
    "one_ppi": ppisn_query_dict["one_ppisn"],
    "two_ppi": ppisn_query_dict["two_ppisn"],
}

# # Do all the convolution
# for query_name in queries.keys():
#     convolution_configuration['global_query'] = queries[query_name]

#     # We set the name of the convolution to make it easier to separate the results of the convolution
#     convolution_name = query_name
#     convolution_configuration['convolution_name'] = convolution_name

#     #
#     generate_whole_sequence(
#         data_info,
#         cosmology_configuration,
#         convolution_configuration,

#         generate_individual_dataset_plots=False,
#         do_convolution_without_detector_probability=True,
#         do_convolution_with_detector_probability=False,
#         do_volume_convolution=False
#     )

# Do SFR convolution plotting with subsets
general_sfr_convolution_plot_function_with_subsets(
    queries, data_info, convolution_configuration
)

# Copy all this to a new dictionary to store everything
