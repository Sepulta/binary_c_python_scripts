"""
Script containing the plotting routine for the mass loss and final fate of stars
"""

import os
import time
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from PyPDF2 import PdfFileMerger

from binarycpython.utils import stellar_types

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

filename = "results/massloss_0.02.dat"

SN_type_dict = {
    0: "None",
    1: "IA_He",
    2: "IA_ELD",
    3: "IA_CHAND",
    4: "AIC",
    5: "ECAP",
    6: "IA_He_Coal",
    7: "IA_CHAND_Coal",
    8: "NS_NS",
    9: "GRB_COLLAPSAR",
    10: "HeStarIa",
    11: "IBC",
    12: "II",
    13: "IIa",
    14: "WDKICK",
    15: "TZ",
    16: "AIC_BH",
    17: "BH_BH",
    18: "BH_NS",
    19: "IA_Hybrid_HeCOWD",
    20: "IA_Hybrid_HeCOWD_subluminous",
    21: "PPISN",
    22: "PISN",
    23: "PHDIS",
}


def plot_mass_canvas(filename, metallicity, plot_settings=None):
    """
    TODO: Remove the stellar types that arent in the unique_st list
    """

    if not plot_settings:
        plot_settings = {}

    #
    STELLAR_TYPE_PLOT_INDEX = 0
    MASS_FRACTIONS_PLOT_INDEX = 1
    WIND_LOSS_PLOT_INDEX = 2
    REMNANT_COMPONENT_PLOT_INDEX = 3
    SUPERNOVA_TYPE_PLOT_INDEX = 4
    FALLBACK_PLOT_INDEX = 5

    alpha_val = 0.15

    # Modify data and sort etc
    df = pd.read_table(filename, sep="\s+")
    df.loc[df.SN_MASSLOSS == -1, "SN_MASSLOSS"] = 0
    df = df.sort_values(by="zams_mass")

    df["fraction_wind_mass_loss"] = df["WIND_MASSLOSS"] / df["zams_mass"]
    df["fraction_SN_mass_loss"] = df["SN_MASSLOSS"] / df["zams_mass"]
    df["remnant_mass"] = (
        df["zams_mass"] - df["SN_MASSLOSS"] - df["WIND_MASSLOSS"]
    ) / df["zams_mass"]
    df["pre_sn_mass"] = (df["zams_mass"] - df["WIND_MASSLOSS"]) / df["zams_mass"]

    ################################################################################
    # Plotting

    # set up canvas
    fig, axes = plt.subplots(nrows=6, ncols=1, sharex=True, figsize=(20, 20))

    ################################################################################
    ## PRE_SN and REMNANT stellar types
    # Add the lines
    for st in df[df.STELLAR_TYPE_REMNANT != -1]["STELLAR_TYPE_REMNANT"].unique():
        axes[STELLAR_TYPE_PLOT_INDEX].hlines(
            st,
            df["zams_mass"].iloc[0],
            df["zams_mass"].iloc[-1],
            edgecolor="m",
            linestyle="--",
            zorder=-1,
            linewidth=2,
            alpha=0.5,
        )

    # Add the lines
    for st in df[df.STELLAR_TYPE_REMNANT != -1]["STELLAR_TYPE_PRE_SN"].unique():
        axes[STELLAR_TYPE_PLOT_INDEX].hlines(
            st,
            df["zams_mass"].iloc[0],
            df["zams_mass"].iloc[-1],
            edgecolor="r",
            linestyle="--",
            zorder=-1,
            linewidth=2,
            alpha=0.5,
        )

    # add second y axis stellar types
    min_st = df[df.STELLAR_TYPE_REMNANT != -1]["STELLAR_TYPE_PRE_SN"].unique().min()
    max_st = df[df.STELLAR_TYPE_REMNANT != -1]["STELLAR_TYPE_REMNANT"].unique().max()

    unique_st_1 = df[df.STELLAR_TYPE_REMNANT != -1]["STELLAR_TYPE_PRE_SN"].unique()
    unique_st_2 = df[df.STELLAR_TYPE_REMNANT != -1]["STELLAR_TYPE_REMNANT"].unique()

    unique_sts = list(unique_st_1) + list(unique_st_2)

    y_2_range_st = range(min_st, max_st + 1)
    st_names = [
        stellar_types.STELLAR_TYPE_DICT_SHORT[el] if el in unique_sts else ""
        for el in y_2_range_st
    ]

    axes_STELLAR_TYPE_PLOT_INDEX_twin_y = axes[STELLAR_TYPE_PLOT_INDEX].twinx()
    axes_STELLAR_TYPE_PLOT_INDEX_twin_y.set_ylim(
        axes[STELLAR_TYPE_PLOT_INDEX].get_ylim()
    )  # same limits
    axes_STELLAR_TYPE_PLOT_INDEX_twin_y.set_yticks(list(y_2_range_st))
    axes_STELLAR_TYPE_PLOT_INDEX_twin_y.set_yticklabels(st_names, fontsize=15)

    # Pre-SN and post SN stellar types
    axes[STELLAR_TYPE_PLOT_INDEX].scatter(
        df[df.STELLAR_TYPE_REMNANT != -1]["zams_mass"],
        df[df.STELLAR_TYPE_REMNANT != -1]["STELLAR_TYPE_REMNANT"],
        label="Remnant type",
    )
    axes[STELLAR_TYPE_PLOT_INDEX].scatter(
        df[df.STELLAR_TYPE_REMNANT != -1]["zams_mass"],
        df[df.STELLAR_TYPE_REMNANT != -1]["STELLAR_TYPE_PRE_SN"],
        label="Pre-SN type",
    )

    axes[STELLAR_TYPE_PLOT_INDEX].legend(bbox_to_anchor=(1.275, 1), fontsize=13)
    axes[STELLAR_TYPE_PLOT_INDEX].set_ylabel("Stellar type", fontsize=16)

    ################################################################################
    ## Fractions of mass lost in winds, supernova and what stays in the remnant
    # Total mass loss for winds and supernovae
    axes[MASS_FRACTIONS_PLOT_INDEX].fill_between(
        df["zams_mass"], 0, df["fraction_wind_mass_loss"], label="Mass loss\nby winds"
    )
    axes[MASS_FRACTIONS_PLOT_INDEX].fill_between(
        df["zams_mass"],
        df["fraction_wind_mass_loss"],
        df["fraction_wind_mass_loss"] + df["fraction_SN_mass_loss"],
        label="Mass loss\nby supernova",
    )
    axes[MASS_FRACTIONS_PLOT_INDEX].fill_between(
        df["zams_mass"],
        df["fraction_wind_mass_loss"] + df["fraction_SN_mass_loss"],
        df["fraction_wind_mass_loss"]
        + df["fraction_SN_mass_loss"]
        + df["remnant_mass"],
        label="Remnant mass",
    )

    axes[MASS_FRACTIONS_PLOT_INDEX].set_ylim(0, 1)
    axes[MASS_FRACTIONS_PLOT_INDEX].set_xlim(
        df["zams_mass"].min(), df["zams_mass"].max()
    )
    axes[MASS_FRACTIONS_PLOT_INDEX].legend(bbox_to_anchor=(1.275, 1), fontsize=13)
    axes[MASS_FRACTIONS_PLOT_INDEX].set_ylabel("Fraction of\nZAMS mass", fontsize=16)
    axes[MASS_FRACTIONS_PLOT_INDEX].set_title(
        "Fraction of mass losses for wind and supernova", fontsize=18
    )

    ################################################################################
    ## Cumulative wind mass loss per stellar type
    prev_array = np.zeros(len(df["zams_mass"].tolist()))
    for el in range(15):
        if df["st_{}".format(el)].any():
            axes[2].fill_between(
                df["zams_mass"],
                prev_array,
                prev_array
                + (df["st_{}".format(el)] / df["WIND_MASSLOSS"]).sort_index(),
                label=stellar_types.STELLAR_TYPE_DICT_SHORT[el],
            )
            prev_array += (
                (df["st_{}".format(el)] / df["WIND_MASSLOSS"]).sort_index().to_list()
            )
    axes[WIND_LOSS_PLOT_INDEX].legend(bbox_to_anchor=(1.375, 1), fontsize=11, ncol=2)
    axes[WIND_LOSS_PLOT_INDEX].set_ylim(0, 1)
    axes[WIND_LOSS_PLOT_INDEX].set_ylabel("Fraction of\nwind mass loss", fontsize=16)
    axes[WIND_LOSS_PLOT_INDEX].set_title(
        "Fraction of total wind mass loss per stellar type", fontsize=18
    )

    ################################################################################
    ## Remnant mass components pre and post sn
    axes[REMNANT_COMPONENT_PLOT_INDEX].plot(
        df["zams_mass"], df["remnant_mass"] * df["zams_mass"], label="Remnant mass"
    )
    axes[REMNANT_COMPONENT_PLOT_INDEX].plot(
        df["zams_mass"], df["PREV_CORE_MASS"], label="Prev core mass"
    )
    axes[REMNANT_COMPONENT_PLOT_INDEX].plot(
        df["zams_mass"],
        df["PREV_CO_CORE_MASS"],
        "-.",
        alpha=0.5,
        label="prev CO core mass",
    )
    axes[REMNANT_COMPONENT_PLOT_INDEX].plot(
        df["zams_mass"],
        df["PREV_HE_CORE_MASS"],
        ":",
        alpha=0.5,
        label="prev He core mass",
    )
    axes[REMNANT_COMPONENT_PLOT_INDEX].plot(
        df["zams_mass"],
        df["pre_sn_mass"] * df["zams_mass"],
        "--",
        alpha=0.5,
        label="pre-SN mass",
    )
    axes[REMNANT_COMPONENT_PLOT_INDEX].legend(bbox_to_anchor=(1.375, 1), fontsize=11)

    axes_REMNANT_COMPONENT_PLOT_INDEX_twin_y = axes[
        REMNANT_COMPONENT_PLOT_INDEX
    ].twinx()
    axes_REMNANT_COMPONENT_PLOT_INDEX_twin_y.set_ylim(
        axes[REMNANT_COMPONENT_PLOT_INDEX].get_ylim()
    )
    # same limits

    ################################################################################
    # Supernova type output
    for st in df[df.SN_TYPE > 0]["SN_TYPE"].unique():
        axes[SUPERNOVA_TYPE_PLOT_INDEX].hlines(
            st,
            df["zams_mass"].iloc[0],
            df["zams_mass"].iloc[-1],
            edgecolor="m",
            linestyle="--",
            zorder=-1,
            linewidth=2,
            alpha=0.5,
        )

    unique_sns = df[df.SN_TYPE > 0]["SN_TYPE"].unique()
    min_sn = np.min(unique_sns)
    max_sn = np.max(unique_sns)

    y_2_range_sn = range(min_sn, max_sn + 1)
    sn_names = [SN_type_dict[el] if el in unique_sns else "" for el in y_2_range_sn]

    axes_SUPERNOVA_TYPE_PLOT_INDEX_twin_y = axes[SUPERNOVA_TYPE_PLOT_INDEX].twinx()
    axes_SUPERNOVA_TYPE_PLOT_INDEX_twin_y.set_ylim(
        axes[SUPERNOVA_TYPE_PLOT_INDEX].get_ylim()
    )  # same limits
    axes_SUPERNOVA_TYPE_PLOT_INDEX_twin_y.set_yticks(list(y_2_range_sn))
    axes_SUPERNOVA_TYPE_PLOT_INDEX_twin_y.set_yticklabels(sn_names, fontsize=15)

    # Supernova type type
    axes[SUPERNOVA_TYPE_PLOT_INDEX].scatter(
        df[df.SN_TYPE > 0]["zams_mass"],
        df[df.SN_TYPE > 0]["SN_TYPE"],
        label="Supernova type",
    )

    axes[SUPERNOVA_TYPE_PLOT_INDEX].legend(bbox_to_anchor=(1.275, 1), fontsize=13)
    axes[SUPERNOVA_TYPE_PLOT_INDEX].set_ylabel("Supernova type type", fontsize=16)

    axes[SUPERNOVA_TYPE_PLOT_INDEX].fill_between(
        df[df.SN_TYPE == 22]["zams_mass"],
        min_sn,
        max_sn,
        color="red",
        alpha=alpha_val,
        hatch="\\",
    )
    axes[SUPERNOVA_TYPE_PLOT_INDEX].fill_between(
        df[df.SN_TYPE == 21]["zams_mass"],
        min_sn,
        max_sn,
        color="green",
        alpha=alpha_val,
        hatch="/",
    )
    axes[SUPERNOVA_TYPE_PLOT_INDEX].fill_between(
        df[df.SN_TYPE == 23]["zams_mass"],
        min_sn,
        max_sn,
        color="yellow",
        alpha=alpha_val,
        hatch="/",
    )
    axes[SUPERNOVA_TYPE_PLOT_INDEX].set_title("Supernova types", fontsize=18)

    ################################################################################
    # Fallback plot
    axes[FALLBACK_PLOT_INDEX].plot(
        df[df.FALLBACK >= 0]["zams_mass"],
        df[df.FALLBACK >= 0]["FALLBACK"],
        label="Fallback fraction",
    )
    axes[FALLBACK_PLOT_INDEX].plot(
        df[df.FALLBACK >= 0]["zams_mass"],
        df[df.FALLBACK >= 0]["FALLBACK_MASS"] / df[df.FALLBACK >= 0]["zams_mass"],
        label="Fallback mass\n(as fraction of zams mass)",
    )
    axes[FALLBACK_PLOT_INDEX].legend(bbox_to_anchor=(1.275, 1), fontsize=13)
    axes[SUPERNOVA_TYPE_PLOT_INDEX].set_ylabel("Fractions", fontsize=16)
    axes[SUPERNOVA_TYPE_PLOT_INDEX].set_title(
        "Fallback fraction and mass fraction", fontsize=18
    )

    # add some info to the last
    axes[-1].set_xlabel(r"ZAMS mass [M$_{\odot}$]", fontsize=18)
    axes[-1].set_xscale("log")

    print(df)

    #################
    # Make up of the plot
    if not plot_settings.get("display_log_metallicity", False):
        plt.suptitle(
            "Overview of mass loss and remnant mass for Z = {}".format(metallicity)
        )
    else:
        plt.suptitle(
            "Overview of mass loss and remnant mass for log10(Z) = {}".format(
                metallicity
            )
        )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_cc_ppisn_jump(result_dir, sim_name, plot_settings=None):
    """
    Function that checks all metallicities for the edge of the cc and ppisn,

    copies the zams mass, the last cc mass, the first ppisn mass and the difference between them

    then plots the results
    """

    if not plot_settings:
        plot_settings = {}

    # Filter out the zero and sort the values by metallicity
    metallicity_dirs = [
        file
        for file in os.listdir(os.path.join(result_dir, sim_name))
        if not file.endswith("ZERO")
    ]
    sorted_metallicity_dirs = sorted(
        metallicity_dirs, key=lambda metallicity: float(metallicity[1:])
    )[::-1]

    zero_metallicity_dirs = [
        file
        for file in os.listdir(os.path.join(result_dir, sim_name))
        if file.endswith("ZERO")
    ]

    metallicities_with_ppisn = []
    zams_mass_first_ppisn = []
    mass_first_ppisn = []
    mass_last_pre_ppisn = []
    difference_last_pre_ppisn_first_pisn = []

    all_z_values = []
    #
    for metallicity_dir in sorted_metallicity_dirs:
        filename_data = os.path.join(
            result_dir, sim_name, metallicity_dir, "output.dat"
        )
        Z_val = float(metallicity_dir[1:])
        all_z_values.append(Z_val)

        # Modify data and sort etc
        df = pd.read_table(filename_data, sep="\s+")
        df.loc[df.SN_MASSLOSS == -1, "SN_MASSLOSS"] = 0
        df = df.sort_values(by="zams_mass")

        df["remnant_mass"] = df["zams_mass"] - df["SN_MASSLOSS"] - df["WIND_MASSLOSS"]

        if 21 in df["SN_TYPE"].unique():

            # Register the first ppisn mass
            first_ppisn_zams_mass = df[df.SN_TYPE == 21].iloc[0]["zams_mass"]
            first_ppisn_mass = df[df.SN_TYPE == 21].iloc[0]["remnant_mass"]
            last_pre_ppisn_mass = df[df.zams_mass < first_ppisn_zams_mass].iloc[-1][
                "remnant_mass"
            ]

            metallicities_with_ppisn.append(Z_val)
            zams_mass_first_ppisn.append(first_ppisn_zams_mass)
            mass_first_ppisn.append(first_ppisn_mass)
            mass_last_pre_ppisn.append(last_pre_ppisn_mass)
            difference_last_pre_ppisn_first_pisn.append(
                last_pre_ppisn_mass - first_ppisn_mass
            )

    # Plot
    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(15, 15))

    # Plot the min and max diff lines
    axes.plot(
        [min(all_z_values), max(all_z_values)],
        [
            min(difference_last_pre_ppisn_first_pisn)
            for _ in [min(all_z_values), max(all_z_values)]
        ],
        linestyle="--",
        label="Minimum difference",
        zorder=-1,
    )
    axes.plot(
        [min(all_z_values), max(all_z_values)],
        [
            max(difference_last_pre_ppisn_first_pisn)
            for _ in [min(all_z_values), max(all_z_values)]
        ],
        linestyle="--",
        label="Maximum difference",
        zorder=-1,
    )

    # scatterplot with the calculated metallicities
    axes.scatter(
        metallicities_with_ppisn,
        zams_mass_first_ppisn,
        marker="s",
        label="ZAMS first ppisn",
    )
    axes.scatter(
        metallicities_with_ppisn,
        mass_first_ppisn,
        marker="d",
        label="Remnant mass first ppisn",
    )
    axes.scatter(
        metallicities_with_ppisn,
        mass_last_pre_ppisn,
        label="Remnant mass last pre-ppisn",
    )
    scatterplot_diff = axes.scatter(
        metallicities_with_ppisn,
        difference_last_pre_ppisn_first_pisn,
        label="diff last pre-ppisn first ppisn",
    )
    axes.plot(
        metallicities_with_ppisn,
        difference_last_pre_ppisn_first_pisn,
        color=scatterplot_diff.get_facecolor()[-1],
        alpha=0.5,
        label="diff last pre-ppisn first ppisn",
    )

    # Copy the axis and plot the exact value
    axes_twin = axes.twinx()

    # Plot the min and max diff lines
    axes_twin.plot(
        [min(all_z_values), max(all_z_values)],
        [
            min(difference_last_pre_ppisn_first_pisn)
            for _ in [min(all_z_values), max(all_z_values)]
        ],
        linestyle="--",
        label="Minimum difference",
        zorder=-1,
    )
    axes_twin.plot(
        [min(all_z_values), max(all_z_values)],
        [
            max(difference_last_pre_ppisn_first_pisn)
            for _ in [min(all_z_values), max(all_z_values)]
        ],
        linestyle="--",
        label="Maximum difference",
        zorder=-1,
    )

    axes_twin.set_ylabel(r"Mass [M$_{\odot}$")
    axes_twin.set_yscale("log")

    # makeup
    axes.set_ylabel(r"Mass [M$_{\odot}$")
    axes.set_xlabel("log10 metallicity")
    axes.set_xscale("log")
    axes.set_yscale("log")
    axes.set_ylim(1, max(zams_mass_first_ppisn))
    axes_twin.set_ylim(axes.get_ylim())

    axes.legend(framealpha=0.5)
    axes.set_xlim(min(all_z_values), max(all_z_values))

    axes_twin.set_yticks(
        [
            min(difference_last_pre_ppisn_first_pisn),
            max(difference_last_pre_ppisn_first_pisn),
        ]
    )
    axes_twin.set_yticklabels(
        [
            "{:f}".format(min(difference_last_pre_ppisn_first_pisn)),
            "{:f}".format(max(difference_last_pre_ppisn_first_pisn)),
        ]
    )

    #
    plt.suptitle(
        "Location of edge and size of discontinuity at lower edge ppisn\nfor Fryer+12 and Farmer+19"
    )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_whole_sequence(
    result_dir, plot_dir, sim_name, create_pdf, display_log_metallicity=False
):
    """
    Create a sequence of plots from the contents of a given result dir.
    """

    # Filter out the zero and sort the values by metallicity
    metallicity_dirs = [
        file
        for file in os.listdir(os.path.join(result_dir, sim_name))
        if not file.endswith("ZERO")
    ]
    sorted_metallicity_dirs = sorted(
        metallicity_dirs, key=lambda metallicity: float(metallicity[1:])
    )[::-1]

    zero_metallicity_dirs = [
        file
        for file in os.listdir(os.path.join(result_dir, sim_name))
        if file.endswith("ZERO")
    ]

    pdf_output_files = []

    #
    for metallicity_dir in sorted_metallicity_dirs:
        filename_data = os.path.join(
            result_dir, sim_name, metallicity_dir, "output.dat"
        )
        Z_val = float(metallicity_dir[1:])
        png_plot_name = os.path.join(
            plot_dir, sim_name, "png", "plot_{}.png".format(Z_val)
        )

        print("Plotting data from {}".format(filename_data))

        metallicity_string = Z_val if not display_log_metallicity else np.log10(Z_val)

        # Plot png
        plot_mass_canvas(
            filename_data,
            metallicity="{}".format(metallicity_string),
            plot_settings={
                "runname": "Z={}".format(Z_val),
                "simulation_name": "{}".format(sim_name),
                "show_plot": False,
                "output_name": png_plot_name,
                "display_log_metallicity": display_log_metallicity,
            },
        )

        # Handle pdf plotting
        if create_pdf:
            pdf_plot_name = os.path.join(
                plot_dir, sim_name, "pdf", "plot_{}.pdf".format(Z_val)
            )

            # Plot pdf
            plot_mass_canvas(
                filename_data,
                metallicity="{}".format(metallicity_string),
                plot_settings={
                    "runname": "Z={}".format(Z_val),
                    "simulation_name": "{}".format(sim_name),
                    "show_plot": False,
                    "output_name": pdf_plot_name,
                    "display_log_metallicity": display_log_metallicity,
                },
            )
            pdf_output_files.append(pdf_plot_name)

    for zero_metallicity_dir in zero_metallicity_dirs:
        filename_data = os.path.join(
            result_dir, sim_name, zero_metallicity_dir, "output.dat"
        )
        Z_val = float(metallicity_dir.replace("_ZERO", "")[1:])
        png_plot_name = os.path.join(
            plot_dir, sim_name, "png", "plot_{}.png".format(zero_metallicity_dir)
        )

        print("Plotting data from {}".format(filename_data))

        metallicity_string = 0 if not display_log_metallicity else "log10(ZERO)"

        # Plot png
        plot_mass_canvas(
            filename_data,
            metallicity="{}".format(metallicity_string),
            plot_settings={
                "runname": "Z={}".format(Z_val),
                "simulation_name": "{}".format(sim_name),
                "show_plot": False,
                "output_name": png_plot_name,
                "display_log_metallicity": display_log_metallicity,
            },
        )

        # Handle pdf plotting
        if create_pdf:
            pdf_plot_name = os.path.join(
                plot_dir, sim_name, "pdf", "plot_{}.pdf".format(Z_val)
            )

            # Plot pdf
            plot_mass_canvas(
                filename_data,
                metallicity="{}".format(metallicity_string),
                plot_settings={
                    "runname": "Z={}".format(Z_val),
                    "simulation_name": "{}".format(sim_name),
                    "show_plot": False,
                    "output_name": pdf_plot_name,
                    "display_log_metallicity": display_log_metallicity,
                },
            )
            pdf_output_files.append(pdf_plot_name)

    # plot the cc ppisn jump
    for file_type in ["png", "pdf"]:
        plot_cc_ppisn_jump(
            result_dir,
            sim_name,
            plot_settings={
                "runname": "Fryer+12 vs Farmer+19",
                "simulation_name": "{}".format(sim_name),
                "show_plot": False,
                "output_name": os.path.join(
                    plot_dir,
                    sim_name,
                    file_type,
                    "plot_cc_ppisn_jump.{}".format(file_type),
                ),
            },
        )
    pdf_output_files.append(
        os.path.join(
            plot_dir, sim_name, file_type, "plot_cc_ppisn_jump.{}".format("pdf")
        )
    )

    # Handle pdf combining
    if create_pdf:
        # After making the pdfs: join them into a big pdf
        merger = PdfFileMerger()

        for pdf in pdf_output_files:
            merger.append(pdf)

        merger.write(os.path.join(plot_dir, sim_name, "{}_plots.pdf".format(sim_name)))
        merger.close()

    # for Z in os.listdir(os.path.join(result_dir, sim_name)):
    #     if not Z == 'Z0.0002_ZERO':
    #         filename_data = os.path.join(result_dir, sim_name, Z, 'output.dat')
    #         Z_val = float(Z[1:])
    #         plot_name = os.path.join(plot_dir, sim_name, 'plot_{}_log.png'.format(Z_val))

    #         plot_mass_canvas(filename_data,
    #             metallicity='{}'.format(Z_val),
    #             output_name=plot_name,
    #             extra_settings={
    #                 "runname": "Z={}".format(Z_val),
    #                 "simulation_name": "{}".format(sim_name),
    #                 "show_plot": False
    #             }
    #         )

    #     else:
    #         # Zero metallicity:
    #         filename_data_zero = os.path.join('results/{}'.format(sim_name), 'Z0.0002_ZERO', 'output.dat')
    #         filename_data_zero = os.path.join(result_dir, sim_name, 'Z_ZERO', 'output.dat')

    #         plot_mass_canvas(filename_data_zero,
    #             metallicity='{}'.format(0),
    #             output_name='plots/{}/plot_{}_log.png'.format(sim_name, str('Z0.0002_ZERO').replace('.', '')),
    #             extra_settings={
    #                 "runname": "Z={}".format('Z0.0002_ZERO'),
    #                 "simulation_name": "{} log spaced mass sampling".format(sim_name),
    #                 "show_plot": False
    #             }
    #         )


# sim = 'WIND_ALGORITHM_SCHNEIDER2018_PPISN_FARMER19_ZOOMED'

# plot_whole_sequence(result_dir='results', plot_dir='plots', sim_name=sim, create_pdf=True, display_log_metallicity=True)
