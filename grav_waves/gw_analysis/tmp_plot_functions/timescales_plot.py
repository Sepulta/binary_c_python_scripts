import os
import time

from grav_waves.gw_analysis.settings import resolution_settings
from grav_waves.gw_analysis.laptop_datasets import dataset_dict

import matplotlib.pyplot as plt

import numpy as np

from grav_waves.gw_analysis.functions.functions import (
    make_df,
    load_info_dict,
)

from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

from grav_waves.gw_analysis.functions.plot_functions import plot_timescales_plot

MAIN_RESULT_DIR = "../results"
MAIN_PLOT_DIR = "../plots"

#
key = "HIGH_RES_SCHNEIDER_MASS_LOSS_PPISN_ON"
type_key = "bhbh"

rebuild = dataset_dict[key]["rebuild"]
sim_name = dataset_dict[key]["sim_name"]
result_dir = os.path.join(MAIN_RESULT_DIR, dataset_dict[key]["result_dir"])
plot_dir = os.path.join(MAIN_PLOT_DIR, dataset_dict[key]["plot_dir"])

pdf_list = []

# Load dataset
info_dict = load_info_dict(result_dir, rebuild=rebuild)


plot_timescales_plot(
    info_dict, type_key, display_log_string=True, plot_settings={"show_plot": True}
)
