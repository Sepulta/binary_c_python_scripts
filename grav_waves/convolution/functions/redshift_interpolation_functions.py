"""
Script to create the redshift interpolation

Consists of two interpolators:
- redshift to time interpolation
- time to redshift interpolation
"""

import os
import pickle
import time
import numpy as np
from scipy import interpolate

from grav_waves.settings import cosmo

from grav_waves.gw_analysis.functions.cosmology_functions import (
    age_of_universe_to_redshift,
    lookback_time_to_redshift,
    redshift_to_lookback_time,
    redshift_to_age_of_universe,
)

##################
# Functions to create the datasets to interpolate on


def create_interpolation_datasets(convolution_dict):
    """
    Function that generates two datasets:
    - First a redshift to lookback time dataset
    - Then we turn it around and use the result to set up a lookback time to redshift dataset (this is much faster, we can increase the resolution while still being off faster)
    """

    min_redshift = convolution_dict["min_interpolation_redshift"]

    print("Generating redshift range")
    if convolution_dict["log_redshift_interpolation"]:
        if min_redshift == 0:
            min_redshift += convolution_dict["min_redshift_change_if_log_sampling"]
        redshift_range = 10 ** np.arange(
            np.log10(min_redshift),
            np.log10(
                convolution_dict["max_interpolation_redshift"]
                + convolution_dict["redshift_interpolation_stepsize"]
            ),
            convolution_dict["redshift_interpolation_stepsize"],
        )
    else:
        redshift_range = np.arange(
            min_redshift,
            convolution_dict["max_interpolation_redshift"]
            + convolution_dict["redshift_interpolation_stepsize"],
            convolution_dict["redshift_interpolation_stepsize"],
        )
    # print(redshift_range)

    print("Turning redshifts into lookbacks")
    redshift_to_time_array = cosmo.age(0) - cosmo.age(redshift_range)
    # print(redshift_to_time_array)

    # Generate dictionary
    interpolation_data_dict = {
        "redshift_data": redshift_range,
        "lookback_time_data": redshift_to_time_array,
        "min_redshift": convolution_dict["min_interpolation_redshift"],
        "max_redshift": convolution_dict["max_interpolation_redshift"],
        "redshift_stepsize": convolution_dict["redshift_interpolation_stepsize"],
        "interpolate_log": convolution_dict["log_redshift_interpolation"],
    }

    # Write to file
    pickle.dump(
        interpolation_data_dict,
        open(convolution_dict["interpolator_data_output_filename"], "wb"),
    )


def load_interpolation_data(convolution_dict):
    """
    Function to load the interpolation dataset and return the loaded interpolators

    if rebuild_when_settings_not_match: we build the dataset based on the settings we passed

    returns:
        - redshift_to_lookback_time interpolator
        - lookback_time_to_redshift interpolator
    """

    if not os.path.isfile(convolution_dict["interpolator_data_output_filename"]):
        print(
            "Creating new interpolation dict, since it was not found in the place it should be"
        )

        # Rebuild data
        create_interpolation_datasets(convolution_dict)

    # load data
    interpolation_data_dict = pickle.load(
        open(convolution_dict["interpolator_data_output_filename"], "rb")
    )

    # Check if we need to rebuild
    rebuild_interpolation_data = False
    if convolution_dict["rebuild_interpolation_data_when_settings_not_match"]:
        if (
            interpolation_data_dict["min_redshift"]
            != convolution_dict["min_interpolation_redshift"]
        ):
            rebuild_interpolation_data = True
        if (
            interpolation_data_dict["max_redshift"]
            != convolution_dict["max_interpolation_redshift"]
        ):
            rebuild_interpolation_data = True
        if (
            interpolation_data_dict["redshift_stepsize"]
            != convolution_dict["redshift_interpolation_stepsize"]
        ):
            rebuild_interpolation_data = True
        if (
            interpolation_data_dict["interpolate_log"]
            != convolution_dict["log_redshift_interpolation"]
        ):
            rebuild_interpolation_data = True

    # Or if we override it
    if convolution_dict["rebuild_interpolation_data"]:
        rebuild_interpolation_data = True

    #
    if rebuild_interpolation_data:
        print(
            "Loaded interpolation configuration doesn't match the provided settings. Rebuilding the interpolation dataset"
        )

        # Rebuild data
        create_interpolation_datasets(convolution_dict)

        # Reload dict
        interpolation_data_dict = pickle.load(
            open(convolution_dict["interpolator_data_output_filename"], "rb")
        )

    # Create the interpolators
    redshift_to_lookback_time_interpolator = interpolate.interp1d(
        interpolation_data_dict["redshift_data"],
        interpolation_data_dict["lookback_time_data"],
        bounds_error=False,
        fill_value=0,
    )

    lookback_time_to_redshift_interpolator = interpolate.interp1d(
        interpolation_data_dict["lookback_time_data"],
        interpolation_data_dict["redshift_data"],
        bounds_error=False,
        fill_value=0,
    )

    return {
        "redshift_to_lookback_time_interpolator": redshift_to_lookback_time_interpolator,
        "lookback_time_to_redshift_interpolator": lookback_time_to_redshift_interpolator,
    }


##################
# Functions to test the interpolation
def test_redshift_to_lookback_time(convolution_dict, size_test_sample, test_log=False):
    """
    Redshift to lookback time interpolation test
    """

    # Load the interpolators:
    interpolators_dict = load_interpolation_data(convolution_dict)

    # Set up the interpolator
    redshift_to_lookback_time_interpolator = interpolators_dict[
        "redshift_to_lookback_time_interpolator"
    ]

    # Create test sample redshift
    if test_log:
        random_redshift_sample = 10 ** np.random.uniform(
            np.log10(
                convolution_dict["min_interpolation_redshift"]
                + convolution_dict["min_redshift_change_if_log_sampling"]
            ),
            np.log10(convolution_dict["max_interpolation_redshift"]),
            size_test_sample,
        )
    else:
        random_redshift_sample = np.random.uniform(
            convolution_dict["min_interpolation_redshift"],
            convolution_dict["max_interpolation_redshift"],
            size_test_sample,
        )
    # print(random_redshift_sample)

    # Transform the redshift to time to compare to
    start_real_time = time.time()
    true_lookback_time_results = (
        cosmo.age(0).value - cosmo.age(random_redshift_sample).value
    )
    # print(true_lookback_time_results)
    stop_real_time = time.time()
    print(
        "Took {} seconds to generate the real data for {} samples".format(
            stop_real_time - start_real_time, size_test_sample
        )
    )

    # Create time values by interpolation
    start_interpolation_time = time.time()
    interpolated_lookback_time_results = redshift_to_lookback_time_interpolator(
        random_redshift_sample
    )
    # print(redshift_to_time_interpolated_time_sample)
    stop_interpolation_time = time.time()
    print(
        "Took {} seconds to interpolate the data for {} samples".format(
            stop_interpolation_time - start_interpolation_time, size_test_sample
        )
    )
    print(
        "Generating redshift to lookback time via interpolation is {:.2e} times faster than normal function".format(
            (stop_real_time - start_real_time)
            / (stop_interpolation_time - start_interpolation_time)
        )
    )

    # Calculat the abs fractional errors
    fractional_error = np.abs(
        (true_lookback_time_results - interpolated_lookback_time_results)
        / true_lookback_time_results
    )
    # print(fractional_error)

    result_dict = {
        "redshift_sample": random_redshift_sample,
        "true_lookback_time_results": true_lookback_time_results,
        "interpolated_lookback_time_results": interpolated_lookback_time_results,
        "fractional_error": fractional_error,
        "sample_size": size_test_sample,
    }

    return result_dict


def test_lookback_time_to_redshift(convolution_dict, size_test_sample, test_log=False):
    """
    Lookback time to redshift test
    """

    #
    min_lookback_time = redshift_to_lookback_time(
        convolution_dict["min_interpolation_redshift"]
    )
    max_lookback_time = redshift_to_lookback_time(
        convolution_dict["max_interpolation_redshift"]
    )

    # Load the interpolators:
    interpolators_dict = load_interpolation_data(convolution_dict)

    # Set up the interpolator
    lookback_time_to_redshift_interpolator = interpolators_dict[
        "lookback_time_to_redshift_interpolator"
    ]

    # Create test sample of lookback times
    if test_log:
        if min_lookback_time == 0:
            min_lookback_time += redshift_to_lookback_time(
                convolution_dict["min_redshift_change_if_log_sampling"]
            )
        random_lookback_times_sample = 10 ** np.random.uniform(
            np.log10(min_lookback_time.value),
            np.log10(max_lookback_time.value),
            size_test_sample,
        )
    else:
        random_lookback_times_sample = np.random.uniform(
            min_lookback_time.value, max_lookback_time.value, size_test_sample
        )
    # print(random_lookback_times_sample)

    # Transform the redshift to time to compare to
    start_real_time = time.time()
    true_redshift_results = np.array(
        [
            lookback_time_to_redshift(lookback_time)
            for lookback_time in random_lookback_times_sample
        ]
    )
    # print(true_redshift_results)
    stop_real_time = time.time()
    print(
        "Took {} seconds to generate the real data for {} samples".format(
            stop_real_time - start_real_time, size_test_sample
        )
    )

    # Create redshift values by interpolation
    start_interpolation_time = time.time()
    interpolated_redshift_results = lookback_time_to_redshift_interpolator(
        random_lookback_times_sample
    )
    # print(interpolated_redshift_results)
    stop_interpolation_time = time.time()
    print(
        "Took {} seconds to interpolate the data for {} samples".format(
            stop_interpolation_time - start_interpolation_time, size_test_sample
        )
    )
    print(
        "Generating lookback time to redshift via interpolation is {:.2e} times faster than normal function".format(
            (stop_real_time - start_real_time)
            / (stop_interpolation_time - start_interpolation_time)
        )
    )

    # Calculate the abs fractional errors
    fractional_error = np.abs(
        (true_redshift_results - interpolated_redshift_results) / true_redshift_results
    )
    # print(fractional_error)

    result_dict = {
        "lookback_times_sample": random_lookback_times_sample,
        "true_redshift_results": true_redshift_results,
        "interpolated_redshift_results": interpolated_redshift_results,
        "fractional_error": fractional_error,
        "sample_size": size_test_sample,
    }

    return result_dict
