import os
import json
import time
import pickle
import sys
import secrets
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import (
    get_help_all,
    get_help,
    create_hdf5,
    output_lines,
)
from binarycpython.utils.functions import merge_dicts
from binarycpython.utils.spacing_functions import const

from david_phd_functions.binaryc.personal_defaults import personal_defaults

from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot

load_mpl_rc()

from df_functions import *


def stability_types():
    stringo = """
    #define RLOF_INSTABILITY_BLOCKED -2
    #define RLOF_IMPOSSIBLE -1
    #define RLOF_STABLE 0
    #define RLOF_UNSTABLE 1
    #define RLOF_UNSTABLE_LOW_MASS_MS_COMENV 2
    #define RLOF_UNSTABLE_GIANT_COMENV 3
    #define RLOF_UNSTABLE_WD_COMENV 4
    #define RLOF_UNSTABLE_NS 5
    #define RLOF_UNSTABLE_BH 6
    #define RLOF_UNSTABLE_MS_MERGER 7
    #define RLOF_UNSTABLE_VERY_LARGE_DONOR 8
    """

    cleaned = stringo.strip().splitlines()

    stability_dict = {}
    for line in cleaned:
        splitted = line.split("#define")[-1].strip().split()
        stability_dict[int(splitted[-1])] = splitted[0]

    return stability_dict


def plot_overview(result_dir, index_mass, plot_settings=None):
    """
    Function to plot several things in for the stability etc
    """

    if not plot_settings:
        plot_settings = {}

    df = pd.read_csv(os.path.join(result_dir, "start_rlof_lines.dat"), sep="\t")

    print(df["zams_mass_1"].unique())

    filtered_df = df[df.zams_mass_1 == df["zams_mass_1"].unique()[index_mass]]
    filtered_df = filtered_df.reset_index(drop=True)

    filtered_df = filtered_df[filtered_df.rlof_counter == 1]  # Filter on rlof counter
    filtered_df["q_21"] = (
        filtered_df["zams_mass_2"] / filtered_df["zams_mass_1"]
    )  # add q12

    # Add more info to the grid
    filtered_df = add_information(filtered_df)

    filtered_df = filtered_df[filtered_df.stellar_type_accretor == 1]

    stability_dict = stability_types()

    simulation_filename = [
        el for el in os.listdir(result_dir) if el.startswith("simulation_")
    ][0]
    with open(os.path.join(result_dir, simulation_filename)) as f:
        simulation_settings = json.loads(f.read())

    period_resolution = int(
        simulation_settings["population_settings"]["grid_options"]["_grid_variables"][
            "log10per"
        ]["resolution"]
    )
    q_resolution = int(
        simulation_settings["population_settings"]["grid_options"]["_grid_variables"][
            "q"
        ]["resolution"]
    )

    filtered_df["stable"] = np.where(filtered_df["rlof_stability"] == 0, 1, 0)
    filtered_df["unstable"] = np.where(filtered_df["rlof_stability"] == 0, 0, 1)

    # filter on onto MS
    input_log10_zams_periods = []
    input_zams_periods = []

    steps_period = period_resolution
    spacing = const(0.15, 5.5, steps_period)
    spacing_step = spacing[1] - spacing[0]

    for i in range(steps_period):
        log10per = 0.15 + i * spacing_step
        orbital_period = 10**log10per

        input_zams_periods.append(orbital_period)
        input_log10_zams_periods.append(log10per)

    plt.tricontourf(
        np.log10(filtered_df["zams_period"]),
        filtered_df["q_21"],
        filtered_df["stable"],
        N=1,
        levels=0,
    )
    # plt.tricontourf(np.log10(filtered_df['zams_period']), filtered_df['q_21'], filtered_df['unstable'], N=1,levels=0, color='red')

    # Plot the input values
    x, y = np.meshgrid(input_log10_zams_periods, sorted(filtered_df["q_21"].unique()))
    plt.scatter(x, y, alpha=0.1)

    # Plot the different types of cases
    plt.scatter(
        np.log10(filtered_df[filtered_df.case == "a"]["zams_period"]),
        filtered_df[filtered_df.case == "a"]["q_21"],
    )
    plt.scatter(
        np.log10(filtered_df[filtered_df.case == "b"]["zams_period"]),
        filtered_df[filtered_df.case == "b"]["q_21"],
    )
    plt.scatter(
        np.log10(filtered_df[filtered_df.case == "c"]["zams_period"]),
        filtered_df[filtered_df.case == "c"]["q_21"],
    )

    plt.scatter(
        np.log10(filtered_df[filtered_df.disk == 0]["zams_period"]),
        filtered_df[filtered_df.disk == 0]["q_21"],
        marker="x",
        s=20,
        alpha=0.5,
        edgecolors="black",
    )
    plt.scatter(
        np.log10(filtered_df[filtered_df.disk == 1]["zams_period"]),
        filtered_df[filtered_df.disk == 1]["q_21"],
        marker="h",
        s=120,
        alpha=0.5,
        edgecolors="blue",
    )

    plt.xlabel("log10Period (days)")
    plt.ylabel("Mass ratio")
    plt.title(
        r"Overview of cases and stability for M=%s [M$_{\odot}$]"
        % df["zams_mass_1"].unique()[index_mass]
    )

    plt.xlim(0, 5.5)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


result_dir = "results/sana_overview/RLOFING_systems_z0.02_M_7_14/"
result_file = os.path.join(result_dir, "start_rlof_lines.dat")
df = pd.read_csv(result_file, sep="\t")

plot_overview(
    result_dir,
    1,
    plot_settings={
        "simulation_name": "SANA Z=0.02",
        "output_name": "plots/sana_002_m=14.png",
    },
)
