"""
Function to generate the table containing the fractions of all RLOF cases, where MS is stable or unstable and whether its via a disk or not.
    Normalized by all MT onto MS
For 1 metallicity
"""

import os
import json
import pandas as pd

from ensemble_functions.ensemble_functions import (
    get_recursive_sum,
    merge_all_subdicts,
    query_dict,
)
from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)
from RLOF.scripts.plot_ensemble_output.settings import (
    rlof_a_stellar_type_range,
    rlof_b_stellar_type_range,
    rlof_c_stellar_type_range,
)

from grav_waves.settings import convolution_settings


def generate_table_fraction_all_rlof_cases_stable_unstable_disk_nodisk(
    dataset,
    output_filename,
    testing,
    caption="",
    label="",
    global_query_dict=None,
    verbose=0,
):
    """
    Function to create the table to shows the fraction of MS to all MT
    """

    if verbose:
        print(
            "Running generate_table_fraction_all_rlof_cases_stable_unstable_disk_nodisk for Z = {}\n\toutput: {}".format(
                dataset["metallicity"], os.path.abspath(output_filename)
            )
        )

        if global_query_dict:
            print(
                "\twith global query: {}".format(global_query_dict["global_query_name"])
            )
            print(
                "\t\tqueries: {}".format(
                    ", ".join(
                        [
                            "{} between {}".format(
                                query["parameter_key"], query["value_range"]
                            )
                            for query in global_query_dict["query_list"]
                        ]
                    )
                )
            )

    #
    result_dict = {}

    # Get the correct ensemble data from the dict
    if verbose:
        print("\t\tLoading data")
    probability_to_yield_conversion_factor = (
        convolution_settings["binary_fraction"] * convolution_settings["mass_in_stars"]
    )
    ensemble_data = get_ensemble_data(
        dataset,
        testing,
        probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
    )["RLOF"]["starting_rlof"]

    if global_query_dict:
        if verbose:
            print("\tPerforming queries")
        for query in global_query_dict["query_list"]:
            if verbose:
                print(
                    "\t\tPerforming query {}".format(
                        "{} between {}".format(
                            query["parameter_key"], query["value_range"]
                        )
                    )
                )
            ensemble_data = query_dict(
                ensemble_data, query["parameter_key"], query["value_range"]
            )

    # Select onto MS
    if verbose:
        print("\t\tSelecting mass transfer on MS data")
    ms_ensemble = query_dict(ensemble_data, "st_acc", [0.5, 1.5])
    total_ms_ensemble = get_recursive_sum(ms_ensemble)

    # Get stable unstable
    if verbose:
        print("\t\tSelecting stable and unstable transfer")
    stable_ms_ensemble = query_dict(ms_ensemble, "stable", [-0.5, 0.5])
    unstable_ms_ensemble = query_dict(ms_ensemble, "stable", [0.5, 10])

    # Get disk and direct
    if verbose:
        print("\t\tSelecting MT via disk and direct impact")
    disk_stable_ms_ensemble = query_dict(stable_ms_ensemble, "disk", [0.5, 1.5])
    direct_stable_ms_ensemble = query_dict(stable_ms_ensemble, "disk", [-0.5, 0.5])

    disk_unstable_ms_ensemble = query_dict(unstable_ms_ensemble, "disk", [0.5, 1.5])
    direct_unstable_ms_ensemble = query_dict(unstable_ms_ensemble, "disk", [-0.5, 0.5])

    # Select cases
    if verbose:
        print("\t\tSelecting RLOF cases")
    rlof_a_disk_stable_ms_ensemble = query_dict(
        disk_stable_ms_ensemble, "st_don", rlof_a_stellar_type_range
    )
    rlof_a_direct_stable_ms_ensemble = query_dict(
        direct_stable_ms_ensemble, "st_don", rlof_a_stellar_type_range
    )
    rlof_a_disk_unstable_ms_ensemble = query_dict(
        disk_unstable_ms_ensemble, "st_don", rlof_a_stellar_type_range
    )
    rlof_a_direct_unstable_ms_ensemble = query_dict(
        direct_unstable_ms_ensemble, "st_don", rlof_a_stellar_type_range
    )

    rlof_b_disk_stable_ms_ensemble = query_dict(
        disk_stable_ms_ensemble, "st_don", rlof_b_stellar_type_range
    )
    rlof_b_direct_stable_ms_ensemble = query_dict(
        direct_stable_ms_ensemble, "st_don", rlof_b_stellar_type_range
    )
    rlof_b_disk_unstable_ms_ensemble = query_dict(
        disk_unstable_ms_ensemble, "st_don", rlof_b_stellar_type_range
    )
    rlof_b_direct_unstable_ms_ensemble = query_dict(
        direct_unstable_ms_ensemble, "st_don", rlof_b_stellar_type_range
    )

    rlof_c_disk_stable_ms_ensemble = query_dict(
        disk_stable_ms_ensemble, "st_don", rlof_c_stellar_type_range
    )
    rlof_c_direct_stable_ms_ensemble = query_dict(
        direct_stable_ms_ensemble, "st_don", rlof_c_stellar_type_range
    )
    rlof_c_disk_unstable_ms_ensemble = query_dict(
        disk_unstable_ms_ensemble, "st_don", rlof_c_stellar_type_range
    )
    rlof_c_direct_unstable_ms_ensemble = query_dict(
        direct_unstable_ms_ensemble, "st_don", rlof_c_stellar_type_range
    )

    # Get totals
    if verbose:
        print("\t\tCalculating totals")
    total_rlof_a_disk_stable_ms_ensemble = get_recursive_sum(
        rlof_a_disk_stable_ms_ensemble
    )
    total_rlof_a_direct_stable_ms_ensemble = get_recursive_sum(
        rlof_a_direct_stable_ms_ensemble
    )
    total_rlof_a_disk_unstable_ms_ensemble = get_recursive_sum(
        rlof_a_disk_unstable_ms_ensemble
    )
    total_rlof_a_direct_unstable_ms_ensemble = get_recursive_sum(
        rlof_a_direct_unstable_ms_ensemble
    )

    total_rlof_b_disk_stable_ms_ensemble = get_recursive_sum(
        rlof_b_disk_stable_ms_ensemble
    )
    total_rlof_b_direct_stable_ms_ensemble = get_recursive_sum(
        rlof_b_direct_stable_ms_ensemble
    )
    total_rlof_b_disk_unstable_ms_ensemble = get_recursive_sum(
        rlof_b_disk_unstable_ms_ensemble
    )
    total_rlof_b_direct_unstable_ms_ensemble = get_recursive_sum(
        rlof_b_direct_unstable_ms_ensemble
    )

    total_rlof_c_disk_stable_ms_ensemble = get_recursive_sum(
        rlof_c_disk_stable_ms_ensemble
    )
    total_rlof_c_direct_stable_ms_ensemble = get_recursive_sum(
        rlof_c_direct_stable_ms_ensemble
    )
    total_rlof_c_disk_unstable_ms_ensemble = get_recursive_sum(
        rlof_c_disk_unstable_ms_ensemble
    )
    total_rlof_c_direct_unstable_ms_ensemble = get_recursive_sum(
        rlof_c_direct_unstable_ms_ensemble
    )

    if verbose:
        print("\t\tCalculating fractions")
    fraction_rlof_a_disk_stable_ms_ensemble = (
        total_rlof_a_disk_stable_ms_ensemble / total_ms_ensemble
    )
    fraction_rlof_a_direct_stable_ms_ensemble = (
        total_rlof_a_direct_stable_ms_ensemble / total_ms_ensemble
    )
    fraction_rlof_a_disk_unstable_ms_ensemble = (
        total_rlof_a_disk_unstable_ms_ensemble / total_ms_ensemble
    )
    fraction_rlof_a_direct_unstable_ms_ensemble = (
        total_rlof_a_direct_unstable_ms_ensemble / total_ms_ensemble
    )

    fraction_rlof_b_disk_stable_ms_ensemble = (
        total_rlof_b_disk_stable_ms_ensemble / total_ms_ensemble
    )
    fraction_rlof_b_direct_stable_ms_ensemble = (
        total_rlof_b_direct_stable_ms_ensemble / total_ms_ensemble
    )
    fraction_rlof_b_disk_unstable_ms_ensemble = (
        total_rlof_b_disk_unstable_ms_ensemble / total_ms_ensemble
    )
    fraction_rlof_b_direct_unstable_ms_ensemble = (
        total_rlof_b_direct_unstable_ms_ensemble / total_ms_ensemble
    )

    fraction_rlof_c_disk_stable_ms_ensemble = (
        total_rlof_c_disk_stable_ms_ensemble / total_ms_ensemble
    )
    fraction_rlof_c_direct_stable_ms_ensemble = (
        total_rlof_c_direct_stable_ms_ensemble / total_ms_ensemble
    )
    fraction_rlof_c_disk_unstable_ms_ensemble = (
        total_rlof_c_disk_unstable_ms_ensemble / total_ms_ensemble
    )
    fraction_rlof_c_direct_unstable_ms_ensemble = (
        total_rlof_c_direct_unstable_ms_ensemble / total_ms_ensemble
    )

    # Sanity check: this has to add up
    # print(sum([
    #     fraction_rlof_a_disk_stable_ms_ensemble, fraction_rlof_a_direct_stable_ms_ensemble, fraction_rlof_a_disk_unstable_ms_ensemble, fraction_rlof_a_direct_unstable_ms_ensemble,
    #     fraction_rlof_b_disk_stable_ms_ensemble, fraction_rlof_b_direct_stable_ms_ensemble, fraction_rlof_b_disk_unstable_ms_ensemble, fraction_rlof_b_direct_unstable_ms_ensemble,
    #     fraction_rlof_c_disk_stable_ms_ensemble, fraction_rlof_c_direct_stable_ms_ensemble, fraction_rlof_c_disk_unstable_ms_ensemble, fraction_rlof_c_direct_unstable_ms_ensemble,
    # ]))

    # Store in dict
    if verbose:
        print("\t\tStoring in dict")
    result_dict["RLOF A"] = [
        fraction_rlof_a_disk_stable_ms_ensemble,
        fraction_rlof_a_direct_stable_ms_ensemble,
        fraction_rlof_a_disk_unstable_ms_ensemble,
        fraction_rlof_a_direct_unstable_ms_ensemble,
    ]

    result_dict["RLOF B"] = [
        fraction_rlof_b_disk_stable_ms_ensemble,
        fraction_rlof_b_direct_stable_ms_ensemble,
        fraction_rlof_b_disk_unstable_ms_ensemble,
        fraction_rlof_b_direct_unstable_ms_ensemble,
    ]

    result_dict["RLOF C"] = [
        fraction_rlof_c_disk_stable_ms_ensemble,
        fraction_rlof_c_direct_stable_ms_ensemble,
        fraction_rlof_c_disk_unstable_ms_ensemble,
        fraction_rlof_c_direct_unstable_ms_ensemble,
    ]

    #
    column_names = [
        "Stable, disk",
        "Stable, direct",
        "Unstable, disk",
        "Unstable, direct",
    ]

    # put in dataframe:
    result_df = pd.DataFrame.from_dict(
        result_dict, columns=column_names, orient="index"
    )
    result_df = result_df.transpose()

    # Create latex string
    latex_string = result_df.to_latex(
        index=True, label=label, caption=caption, float_format="%.2f"
    )
    # print(latex_string)

    # Export
    with open(output_filename, "w") as output_filehandle:
        output_filehandle.write(latex_string)


# # to test
# population_datasets_sana = generate_ensemble_dataset_dict('/home/david/projects/binary_c_root/results/RLOF/LOW_RES_RLOF_SANA_2021_ENSEMBLE_fractions/')
# print(population_datasets_sana)
# generate_table_fraction_all_rlof_cases_stable_unstable_disk_nodisk(population_datasets_sana['datasets']['0.02'], '/tmp/test.tex', testing=False, caption='', label='', global_query_dict=None, verbose=1)
