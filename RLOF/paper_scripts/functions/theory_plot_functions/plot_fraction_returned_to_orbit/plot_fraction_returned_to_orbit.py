"""
Script containing the plotting routine for the fraction of transferred angular momentum return to the orbit
"""

import os

import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from RLOF.scripts.theory_plots.functions import (
    nans,
    rmin_ulrich_burger_binaryc_version,
    calc_fraction_returned_conservative,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


# Plot the fraction of angular momentum of the mass stream that is returned to the orbit
def plot_fraction_returned(
    min_log10_q_acc_don=-2,
    max_log10_q_acc_don=2,
    min_log10_racc_sep=-4,
    max_log10_racc_sep=0,
    plot_settings={},
):
    """
    Function to plot the fraction of angular momentum of the stream that is returned to the orbit
    """

    contour_levels = [0.25, 0.33, 0.5, 0.9]
    linestyles = ["solid", "dashed", "-.", ":"]

    # Set bounds and stepsize
    log10q_acc_don_stepsize = 0.005
    log10_racc_sep_stepsize = 0.005

    # Set up arrays
    q_accretor_donor_array = 10 ** np.arange(
        min_log10_q_acc_don, max_log10_q_acc_don, log10q_acc_don_stepsize
    )
    racc_sep_array = 10 ** np.arange(
        min_log10_racc_sep, max_log10_racc_sep, log10_racc_sep_stepsize
    )

    # Set up meshgrid
    X_racc_sep_array, Y_q_accretor_donor_array = np.meshgrid(
        racc_sep_array, q_accretor_donor_array
    )
    X_racc_sep_array_flat = np.ravel(X_racc_sep_array)
    Y_q_accretor_donor_array_flat = np.ravel(Y_q_accretor_donor_array)

    # Create beta_array
    # beta_min_array = np.zeros(Y_q_accretor_donor_array.shape)
    fraction_returned_array = nans(Y_q_accretor_donor_array.shape)
    fraction_returned_array_flat = np.ravel(fraction_returned_array)

    # TODO: improve this
    # Loop over the flattened arrays and impose limits
    # The requirements are: r accretor < rmin
    for i in range(len(Y_q_accretor_donor_array_flat)):
        mass_accretor = 1
        mass_donor = 1 / Y_q_accretor_donor_array_flat[i]

        rmin = rmin_ulrich_burger_binaryc_version(mass_donor, mass_accretor)

        if X_racc_sep_array_flat[i] < rmin:
            fraction_returned = calc_fraction_returned_conservative(
                mass_donor, mass_accretor, X_racc_sep_array_flat[i]
            )
            fraction_returned_array_flat[i] = fraction_returned

    # reshape the array
    fraction_returned_array = np.reshape(
        fraction_returned_array_flat, fraction_returned_array.shape
    )

    min_plot_value = 0
    max_plot_value = 1

    ################
    # Plotting
    fig = plt.figure(figsize=(16, 10))

    #
    gs = fig.add_gridspec(nrows=1, ncols=11)

    # create axes
    ax_plot = fig.add_subplot(gs[0, 0:9])

    # Create colorbar
    ax_cb = fig.add_subplot(gs[0, 10])

    # plot values
    ax_plot.pcolormesh(
        X_racc_sep_array,
        Y_q_accretor_donor_array,
        fraction_returned_array,
        cmap=matplotlib.cm.viridis,
        vmin=min_plot_value,
        vmax=max_plot_value,
        antialiased=True,
        rasterized=True,
    )

    # Create contour
    CS_mt_region_1 = ax_plot.contour(
        X_racc_sep_array,
        Y_q_accretor_donor_array,
        fraction_returned_array,
        levels=contour_levels,
        colors="k",
        linestyles=linestyles,
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        cmap=matplotlib.cm.viridis,
        norm=matplotlib.colors.Normalize(vmin=min_plot_value, vmax=max_plot_value),
    )
    cbar.ax.set_ylabel(r"Fraction of $\dot{J}_{\mathrm{stream}}$ returned to the orbit")

    for contour_i in range(len(contour_levels)):
        cbar.ax.plot(
            [0, 1],
            [contour_levels[contour_i]] * 2,
            "black",
            linestyle=linestyles[contour_i],
        )

    # Some make up
    ax_plot.set_yscale("log")
    ax_plot.set_xscale("log")

    ax_plot.set_ylabel(
        r"$q_{\mathrm{acc}}$", fontsize=plot_settings.get("fontsize_labels", 26)
    )
    ax_plot.set_xlabel(
        r"$r_{\mathrm{acc}}/a$", fontsize=plot_settings.get("fontsize_labels", 26)
    )

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":

    #########
    #
    target_dir = (
        "/home/david/Dropbox/Academic/PHD/papers/paper_disks/paper_tex/figures/theory/"
    )
    target_dir = "plots"

    #
    base_name = "birth_redshift_vs_birth_metallicity_at_redshift_zero.pdf"
    plot_fraction_returned(
        min_log10_q_acc_don=-2,
        max_log10_q_acc_don=2,
        min_log10_racc_sep=-4,
        max_log10_racc_sep=0,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(target_dir, base_name),
            "fontsize_labels": 42,
        },
    )
