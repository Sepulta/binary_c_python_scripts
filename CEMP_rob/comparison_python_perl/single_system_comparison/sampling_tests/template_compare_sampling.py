"""
Script to compare the sampling files
"""

import os
import pandas as pd
import numpy as np

result_dir = "template_results"

# Set filenames
python_sampling_file = os.path.join(result_dir, "python", "results", "sampling.txt")
perl_sampling_file = os.path.join(result_dir, "perl", "results", "sampling.txt")

# Make
python_masses = []
python_probabilities = []

perl_masses = []
perl_probabilities = []

# Load files
with open(python_sampling_file, "r") as pythonfile:
    pythonfile.readline()

    for line in pythonfile:
        split_line = line.strip().split()

        python_masses.append(float(split_line[0]))
        python_probabilities.append(float(split_line[1]))

with open(perl_sampling_file, "r") as perlfile:
    perlfile.readline()

    for line in perlfile:
        split_line = line.strip().split()

        perl_masses.append(float(split_line[0]))
        perl_probabilities.append(float(split_line[1]))

#
assert len(python_masses) == len(perl_masses)

# Create dataframe
df = pd.DataFrame(
    np.array([python_masses, python_probabilities, perl_masses, perl_probabilities]).T,
    columns=[
        "python_masses",
        "python_probabilities",
        "perl_masses",
        "perl_probabilities",
    ],
)
df["mass_differences"] = df["python_masses"] - df["perl_masses"]
df["probability_differences"] = df["python_probabilities"] - df["perl_probabilities"]

print(df["mass_differences"].unique())
print(df["probability_differences"].unique())

if not df["mass_differences"].unique() == [0.0]:
    print("error. there are sampling differences")

if not df["probability_differences"].unique() == [0.0]:
    print("error. there are probability differences")
