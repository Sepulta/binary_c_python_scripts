"""
Script containing function to generate the data
"""

import os
import json
import shutil

from binarycpython.utils.grid import Population
from binarycpython.utils.ensemble import binaryc_json_serializer

from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.custom_system_generator import (
    custom_system_generator,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.custom_logging_function import (
    custom_logging_string,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.parse_function import (
    parse_function,
)


def remove_contents_of_dir(target_dir):
    """
    remove files in target dir
    """

    with os.scandir(target_dir) as entries:
        for entry in entries:
            if entry.is_dir() and not entry.is_symlink():
                shutil.rmtree(entry.path)
            else:
                os.remove(entry.path)


def generate_data_function(
    output_dir, system_generator_settings, grid_settings, physics_settings
):
    """
    function to generate the data
    """

    # Load system generator
    system_gen_obj = custom_system_generator(
        system_generator_settings, physics_settings
    )

    # Set up population
    test_pop = Population(verbosity=1)

    # Load settings
    test_pop.set(**grid_settings)

    # Add situational settings
    test_pop.set(
        C_logging_code=custom_logging_string,
        parse_function=parse_function,
        evolution_type="custom_generator",
        custom_generator=system_gen_obj,
    )

    test_pop.set(data_dir=output_dir, base_filename="output.dat")

    # Get version info of the binary_c build
    version_info = test_pop.return_binary_c_version_info(parsed=True)

    # create local tmp_dir
    test_pop.set(
        tmp_dir=os.path.join(test_pop.custom_options["data_dir"], "local_tmp_dir")
    )
    test_pop.set(log_args_dir=test_pop.grid_options["tmp_dir"])
    os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

    # Stop the script if the configuration is wrong
    if version_info["macros"]["NUCSYN"] == "on":
        print("Please disable NUCSYN")
        quit()

    # Clean files in output dir
    if os.path.isdir(test_pop.custom_options["data_dir"]):
        remove_contents_of_dir(test_pop.custom_options["data_dir"])

    # Evolve
    test_pop.evolve()

    # Handle the ensemble
    ensemble_output = test_pop.grid_ensemble_results

    # Write the metadata
    with open(
        os.path.join(test_pop.custom_options["data_dir"], "metadata.json"), "w"
    ) as f:
        json.dump(ensemble_output["metadata"], f, default=binaryc_json_serializer)

    # Write all info
    with open(
        os.path.join(test_pop.custom_options["data_dir"], "ensemble_output.json"), "w"
    ) as f:
        json.dump(ensemble_output, f, default=binaryc_json_serializer)
