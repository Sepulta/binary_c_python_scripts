"""
Function to plot the three-panel for the star formation but unconvolved
"""

import os
import json
import h5py
import numpy as np
import pandas as pd

import astropy.units as u

import matplotlib
import matplotlib.pyplot as plt
import matplotlib as mpl

from grav_waves.settings import AGE_UNIVERSE_IN_YEAR
from grav_waves.gw_analysis.functions.cosmology_functions import (
    redshift_to_lookback_time,
)
from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    filter_dco_type,
    get_rate_type_names,
    linestyle_list,
    add_contour_levels,
    get_num_subsets,
    plot_2d_results,
)
from grav_waves.convolution.functions.redshift_interpolation_functions import (
    load_interpolation_data,
)

from david_phd_functions.grav_waves.dataframe_functions import add_chirpmass_column
from david_phd_functions.plotting.canvas_functions import return_canvas_with_subsets
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

from grav_waves.gw_analysis.functions.cosmology_functions import (
    starformation_rate,
    metallicity_distribution,
)
from grav_waves.convolution.functions.compas_metallicity_distribution import (
    find_metallicity_distribution,
)


def get_mass_quantity_info():
    """
    Fucntion to get the mass quantity info
    """

    #######
    # Read out the dataframe and query
    # Get combined df
    combined_df = pd.read_hdf(hdf5_filename, "/data/combined_dataframes")

    # Add columns
    combined_df = add_chirpmass_column(combined_df, m1_name="mass_1", m2_name="mass_2")
    combined_df["total_mass"] = combined_df["mass_1"] + combined_df["mass_2"]
    combined_df["primary_mass"] = combined_df[["mass_1", "mass_2"]].max(axis=1)
    combined_df["secondary_mass"] = combined_df[["mass_1", "mass_2"]].min(axis=1)

    #
    index_df = pd.read_hdf(
        hdf5_filename, "/data/{}/{}".format(rate_type_hdf5_key, str(redshift_key))
    )
    joined_df = index_df.join(combined_df, on="local_index")

    # Filter the dco type:
    joined_df = filter_dco_type(joined_df, dco_type)

    if not query is None:
        joined_df = joined_df.query(query)

    #
    data_array = joined_df[mass_quantity]
    weight_array = joined_df[rate_key_in_df]

    # Determine the mass bins
    bin_size = np.diff(bins)
    bincenter = (bins[1:] + bins[:-1]) / 2

    # bin and take into account the divison by mass
    hist = np.histogram(data_array, bins=bins, weights=weight_array)[0]
    if divide_by_mass_bins:
        hist = hist / mass_bin_size

    # Select the non-zero bins and split off the empty ones
    # NOTE: without this, toms method does not work
    non_zero_bins_indices = np.nonzero(hist)[0]
    bins = bins[non_zero_bins_indices.min() : non_zero_bins_indices.max() + 1]

    #
    return_dict = {
        "mass_centers": bincenter,
        "rates": hist,
    }

    ########################
    # KDE
    x_KDE_width = 0.1
    kde_width = 0.1 * bin_size[0]

    x_KDE = np.arange(bins.min(), bins.max() + x_KDE_width, x_KDE_width)
    center_KDEbins = (x_KDE[:-1] + x_KDE[1:]) / 2.0

    # calculate_kde_values
    y_vals = calculate_kde_values(
        data=data_array,
        weights=weight_array,
        lower_bound=data_array.min(),
        upper_bound=data_array.max(),
        bw_method=kde_width,
        center_KDEbins=center_KDEbins,
        hist=hist,
    )

    #
    return_dict["kde_yvals"] = y_vals
    return_dict["center_KDEbins"] = center_KDEbins

    ########################
    # Bootstrap
    if bootstraps:

        # TODO: Ask lieke why do we use x_KDE here instead of center_KDEbins
        _, median, percentiles = run_bootstrap(
            masses=data_array,
            weights=weight_array,
            indices=data_array.index,
            kde_width=kde_width,
            mass_bins=bins,
            center_KDEbins=center_KDEbins,
            num_procs=num_procs,
            bootstraps=bootstraps,
        )

        #
        return_dict["median"] = median
        return_dict["percentiles"] = percentiles

    #
    return return_dict


def get_sfr_info(config_dict_convolution, metallicity_dict, cosmology_configuration):
    """
    Function to get the sfr info
    """

    ############
    # Recalculate some values
    redshifts = config_dict_convolution["time_centers"]
    sampled_metallicities = 10 ** np.array(
        metallicity_dict["sorted_log10metallicity_values"]
    )

    log10_metallicities = np.log10(sampled_metallicities)
    log_metallicities = np.log(sampled_metallicities)

    max_log_z = log_metallicities.max()
    min_log_z = log_metallicities.min()

    # Get sfr value
    sfr_values = starformation_rate(
        np.array(config_dict_convolution["time_centers"]),
        cosmology_configuration=cosmology_configuration,
        verbosity=1,
    )
    if cosmology_configuration["metallicity_distribution_function"] in [
        "vanSon21",
        "neijssel19",
        "COMPAS",
    ]:
        # Calculate the metallicity distribution
        dPdlogZ, metallicities, p_draw_metallicity = find_metallicity_distribution(
            np.array(config_dict_convolution["time_centers"]),
            min_log_z,
            max_log_z,
            **cosmology_configuration["metallicity_distribution_args"],
        )
    else:
        raise ValueError("unknown distribution")

    # Plot the SFR * dP/dlogZ * dlogZ
    log_metallicities = np.log(metallicities)

    extended_log_metallicities = np.append(
        np.array(log_metallicities[0] - np.diff(log_metallicities)[0]),
        log_metallicities,
    )
    dlogZ = np.diff(extended_log_metallicities)

    # Calculate full probability
    P = dPdlogZ * dlogZ

    # Multiply by sfr:
    SFR_P = (sfr_values * P.T).T

    return redshifts, metallicities, SFR_P


def align_axes(fig, axes_list, which_axis="x"):
    """
    Function to align the x or y axis of a list of axes
    """

    if which_axis == "x":
        getter = "get_xlim"
        setter = "set_xlim"
    elif which_axis == "y":
        getter = "get_ylim"
        setter = "set_ylim"
    else:
        raise ValueError("not implemented yet")

    min_val = 1e9
    max_val = 1e-9

    # Find the min and max
    for axis in axes_list:
        lims = axis.__getattribute__(getter)()

        min_val = np.min([min_val, lims[0]])
        max_val = np.max([max_val, lims[1]])

    # Set the min and max
    for axis in axes_list:
        axis.__getattribute__(setter)([min_val, max_val])


def plot_sfr_unconvolved(dataset_filename, rate_type, dco_type, plot_settings):
    """ """

    # Get configuration and interpolators
    hdf5_file = h5py.File(dataset_filename, "r")

    settings = hdf5_file["settings"]
    convolution_configuration = json.loads(settings["convolution_configuration"][()])
    cosmology_configuration = json.loads(settings["cosmology_configuration"][()])
    dataset_dict = json.loads(settings["dataset_dict"][()])
    metallicity_dict = json.loads(settings["metallicity_settings"][()])
    hdf5_file.close()

    #
    convolution_configuration[
        "interpolator_data_output_filename"
    ] = "/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/interpolator_data_dict.p"
    redshift_interpolators = load_interpolation_data(convolution_configuration)

    # Read the datasets
    combined_df = pd.read_hdf(dataset_filename, "/data/combined_dataframes")
    combined_df = filter_dco_type(combined_df, dco_type)

    # Calculate birth redshift
    max_lookback_time = redshift_interpolators[
        "redshift_to_lookback_time_interpolator"
    ](9)
    combined_df["birth_lookback_time_in_years"] = combined_df[
        "merger_time_values_in_years"
    ] * (u.yr.to(u.Gyr))
    combined_df = combined_df[
        combined_df.birth_lookback_time_in_years < max_lookback_time
    ]
    combined_df["birth_redshift"] = redshift_interpolators[
        "lookback_time_to_redshift_interpolator"
    ](combined_df["birth_lookback_time_in_years"])

    redshifts, metallicities, SFR_P = get_sfr_info(
        config_dict_convolution=convolution_configuration,
        metallicity_dict=metallicity_dict,
        cosmology_configuration=cosmology_configuration,
    )

    log10metallicities = np.log10(metallicities)
    diff_metallicities = np.diff(log10metallicities)

    combined_df["log10metallicities"] = np.log10(combined_df["metallicity"])
    num = len(combined_df["log10metallicities"])
    noise = (np.random.random(num) - 0.5) * 0.016
    combined_df["log10metallicities_randomised"] = (
        combined_df["log10metallicities"] + noise
    )
    combined_df["metallicities_randomised"] = (
        10 ** combined_df["log10metallicities_randomised"]
    )

    #
    # Set up norm
    norm = mpl.colors.Normalize(vmin=np.min(SFR_P).value, vmax=np.max(SFR_P).value)

    ##
    # Set up figure
    fig = plt.figure(figsize=(40, 20))
    gs = matplotlib.gridspec.GridSpec(nrows=1, ncols=2, figure=fig)

    #
    axis_merger_redshifts = fig.add_subplot(gs[0, 0])
    axis_sfr = fig.add_subplot(gs[0, 1])

    redshift_bins = np.arange(0, 9, 0.5)
    metallicity_bins = 10 ** np.arange(-5, -1, 0.25)

    #
    axis_merger_redshifts.hist2d(
        combined_df["birth_redshift"],
        combined_df["metallicities_randomised"],
        bins=[redshift_bins, metallicity_bins],
        norm=mpl.colors.LogNorm(),
    )
    # axis_merger_redshifts.scatter(combined_df['birth_redshift'], combined_df['metallicity'])
    axis_merger_redshifts.set_xlabel("redshift")
    axis_merger_redshifts.set_yscale("log")
    axis_merger_redshifts.set_ylabel("metallicity")

    #
    axis_sfr.pcolormesh(
        redshifts,
        metallicities,
        SFR_P.T.value,
        norm=norm,
        antialiased=True,
        rasterized=True,
    )
    axis_sfr.set_xlabel("redshift")
    axis_sfr.set_yscale("log")

    #
    align_axes(fig, [axis_merger_redshifts, axis_sfr], which_axis="y")

    #
    combined_df["primary_mass"] = combined_df[["mass_1", "mass_2"]].max(axis=1)
    # axis_primary_mass_distribution.

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    # Global settings
    fiducial_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/without_detection_probability/rebinned_convolution_results.h5"

    #
    plot_sfr_unconvolved(
        dataset_filename=fiducial_dataset,
        rate_type="merger_rate",
        dco_type="bhbh",
        plot_settings={
            "show_plot": False,
            "output_name": "output/plot_sfr_unconvolved.pdf",
            "simulation_name": "test",
            "antialiased": True,
            "rasterized": True,
            "runname": "plot_sfr_unconvolved",
        },
    )
else:
    matplotlib.use("Agg")
