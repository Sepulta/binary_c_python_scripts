"""
File containing functions to convolve the SN results based on new event based structure
"""

import os
import copy
import time
import h5py
import logging
import json
import numpy as np
import pandas as pd
import astropy.units as u
from grav_waves.convolution.functions.convolution_general_functions import (
    JsonCustomEncoder,
)
from grav_waves.convolution.functions.convolution_add_population_settings_to_hdf5file import (
    add_population_settings_to_hdf5file,
)
import shutil
from grav_waves.convolution.functions.convolution_functions import convolution_main
from grav_waves.gw_analysis.functions.functions import make_dataset_dict
from grav_waves.settings import convolution_settings, config_dict_cosmology

t_hubble = 13.7 * 1e9 * u.yr


def add_number_per_formed_solar_mass(df, binary_fraction_factor, average_mass_system):
    """
    function to add the number per formed solarmass to the df
    """

    # Multiply the probability by a binary fraction
    df["probability"] *= binary_fraction_factor

    # Multiply the probability by a conversion factor to get the number per solar mass
    df["number_per_solar_mass"] = df["probability"] / average_mass_system

    return df


def create_combined_sn_dataset(
    dataset_dict,
    convolution_configuration,
    cosmology_configuration,
    output_filename,
    total_sn_event_filename,
):
    """
    Function to create the combined SN hdf5 dataset

    input:
        convolution_configuration: settings for the convolution
        cosmology_configuration; settings for the cosmology: SFR etc
        output_filename: target filename for the hdf5 file containing the combined data
    """

    simname_dir = dataset_dict["main_dir"]
    print("simname_dir", simname_dir)

    ##
    convolution_configuration["logger"].info("Started combining the datasets")

    ###########
    # Set up the output file and groups

    # Create the output directory
    if os.path.dirname(output_filename):
        os.makedirs(os.path.dirname(output_filename), exist_ok=True)

    # create the main HDF5 file
    hdf5_file = h5py.File(output_filename, "w")

    # Create groups
    settings_grp = hdf5_file.create_group("settings")
    _ = hdf5_file.create_group("data")

    settings_grp.create_dataset(
        "dataset_dict", data=json.dumps(dataset_dict, cls=JsonCustomEncoder)
    )

    # ##################
    # # Load dataset info and write all the settings to the output hdf5
    # # TODO: fix the dataset dict
    # info_dict = load_info_dict(dataset_dict['population_result_dir'], rebuild=dataset_dict['rebuild'])
    # settings_grp.create_dataset(
    #     "input_datasets", data=json.dumps(
    #         info_dict,
    #         cls=JsonCustomEncoder
    #     )
    # )

    ##################
    # Load dataset population settings and binary_c settings etc and add to settings group
    population_result_dir = os.path.join(simname_dir, "population_results")
    metallicity_dir = os.path.join(
        population_result_dir, sorted(os.listdir(population_result_dir))[0]
    )
    population_settings_filename = os.path.join(
        metallicity_dir,
        [el for el in os.listdir(metallicity_dir) if el.endswith("_settings.json")][0],
    )

    add_population_settings_to_hdf5file(
        convolution_filehandle=hdf5_file,
        population_settings_filename=population_settings_filename,
    )

    #######################
    # Close hdf5 file to be able to write the pandas stuff in there
    hdf5_file.close()

    ##################
    # Create a combined dataframe of all datasets and add to data group
    convolution_configuration["logger"].info("Combining dataframes and writing to hdf5")
    start_combining = time.time()

    ########
    # TODO: get total SN event things into dataframe
    total_sn_dataframe = pd.read_csv(total_sn_event_filename, sep="\s+")

    # Add formation time:
    total_sn_dataframe["formation_time_values_in_years"] = (
        total_sn_dataframe["time"] * 1e6
    )

    # Query by forming in hubble time
    total_sn_dataframe = total_sn_dataframe[
        total_sn_dataframe.formation_time_values_in_years < t_hubble.value
    ]

    #
    total_sn_dataframe = add_number_per_formed_solar_mass(
        total_sn_dataframe,
        binary_fraction_factor=convolution_configuration["binary_fraction"],
        average_mass_system=convolution_configuration["average_mass_system"],
    )
    total_sn_dataframe["number_per_solar_mass_values"] = total_sn_dataframe[
        "number_per_solar_mass"
    ]

    # Create combined dataframe from all individual dataframes
    total_sn_dataframe.to_hdf(output_filename, "data/combined_dataframes", complevel=0)

    #
    convolution_configuration["logger"].info(
        "Finished combining dataframes and writing to hdf5. Took {}s".format(
            time.time() - start_combining
        )
    )
    convolution_configuration["logger"].info(
        "Finished building the combined sn dataset for:\n\t{}.\nWrote results to:\n\t{}".format(
            simname_dir, output_filename
        )
    )


def filter_combined_sn_dataset_function(input_hdf5file, output_hdf5file):
    """
    Function to filter the combined sn dataset

    TODO: overwriting the group is better thna deleting and then overwriting
    TODO: https://nl.mathworks.com/matlabcentral/answers/395920-how-can-i-delete-a-dataset-completely-from-a-group-in-a-hdf5-file
    """

    # Copy input file to output file
    if os.path.isfile(output_hdf5file):
        os.remove(output_hdf5file)
    shutil.copy(input_hdf5file, output_hdf5file)

    # Filter the dataset
    combined_sn_df = pd.read_hdf(input_hdf5file, "/data/combined_dataframes")
    filtered_combined_sn_df = combined_sn_df.copy(deep=True)
    filtered_combined_sn_df = filtered_combined_sn_df.query(
        "SN_type in [11, 12, 13, 21, 22, 23]"
    )
    filtered_combined_sn_df.reset_index(inplace=True, drop=True)

    # Write the dataset to the output_filename
    filtered_combined_sn_df.to_hdf(
        output_hdf5file, "/data/combined_dataframes", complevel=0
    )


def run_convolution_sn(
    main_dir,
    sn_convolution_output_dir,
    convolution_settings,
    cosmology_settings,
    total_SN_event_outputfile,
    run_combine_datasets=False,
    run_filter_combined_dataset=True,
    run_convolution=True,
    verbosity=1,
):
    """
    Function to handle all the steps for the convolution of the SN datasets
    """

    # Get the correct directories
    dataset_dict = make_dataset_dict(main_dir)
    os.makedirs(sn_convolution_output_dir, exist_ok=True)

    # Create names for the output files
    combined_sn_dataset_filename = os.path.join(
        sn_convolution_output_dir, "combined_sn_dataset.h5"
    )
    filtered_sn_dataset_filename = os.path.join(
        sn_convolution_output_dir, "filtered_sn_dataset.h5"
    )
    redshift_convolution_sn_dataset_filename = os.path.join(
        sn_convolution_output_dir, "convolved_sn_dataset.h5"
    )

    ##############
    # Build input dataset
    used_sn_dataset_filename = combined_sn_dataset_filename
    if run_combine_datasets:
        print("create_combined_sn_dataset:", combined_sn_dataset_filename)
        create_combined_sn_dataset(
            dataset_dict=dataset_dict,
            convolution_configuration=convolution_settings,
            cosmology_configuration=config_dict_cosmology,
            output_filename=combined_sn_dataset_filename,
            total_sn_event_filename=total_SN_event_outputfile,
        )

    ##############
    # Build combined dataset merging systems
    if run_filter_combined_dataset:
        filter_combined_sn_dataset_function(
            input_hdf5file=combined_sn_dataset_filename,
            output_hdf5file=filtered_sn_dataset_filename,
        )
        used_sn_dataset_filename = filtered_sn_dataset_filename

    ##############
    # Run convolution
    if run_convolution:
        convolution_main(
            input_filename=used_sn_dataset_filename,
            convolution_configuration=convolution_settings,
            cosmology_configuration=config_dict_cosmology,
            output_filename=redshift_convolution_sn_dataset_filename,
            verbosity=1,
        )


if __name__ == "__main__":
    simname_dir = "/home/david/data_projects/binary_c_data/GRAV_WAVES/TEST_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED"
    total_SN_outputfile = os.path.join(
        simname_dir, "dco_convolution_results/combined_total_SN_BINARY_events.dat"
    )
    sn_convolution_output_dir = os.path.join(simname_dir, "sn_convolution")
    population_dir = os.path.join(simname_dir, "population_results")

    #############################
    # Convolution settings
    convolution_stepsize = 0.025
    convolution_settings["stepsize"] = convolution_stepsize
    convolution_settings["time_bins"] = np.arange(
        1e-6 - 0.5 * convolution_stepsize,
        (convolution_settings["max_interpolation_redshift"] - 1)
        + 0.5 * convolution_stepsize,
        convolution_stepsize,
    )
    convolution_settings["time_centers"] = (
        convolution_settings["time_bins"][1:] + convolution_settings["time_bins"][:-1]
    ) / 2
    convolution_settings["include_formation_rates"] = False
    convolution_settings[
        "global_query"
    ] = "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0)"
    convolution_settings["clean_dco_type_files_and_info_dict"] = False
    convolution_settings["num_cores"] = 4
    convolution_settings["dco_type"] = "combined_dco"
    convolution_settings["remove_pickle_files"] = False
    convolution_settings["logger"].setLevel(logging.DEBUG)
    convolution_settings["convolution_tmp_dir"] = os.path.join(
        sn_convolution_output_dir, "tmp"
    )

    #############################
    # SN convolution settings
    sn_convolution_settings = copy.deepcopy(convolution_settings)
    sn_convolution_settings[
        "global_query"
    ] = "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0)"

    sn_convolution_settings["include_birth_rates"] = False
    sn_convolution_settings["include_formation_rates"] = True
    sn_convolution_settings["include_merger_rates"] = False
    sn_convolution_settings["include_detection_rates"] = False

    sn_convolution_settings["combined_data_array_columns"] = [
        "number_per_solar_mass_values",
        "metallicity",
    ]

    run_convolution_sn(
        main_dir=simname_dir,
        sn_convolution_output_dir=sn_convolution_output_dir,
        convolution_settings=sn_convolution_settings,
        cosmology_settings=config_dict_cosmology,
        total_SN_event_outputfile=total_SN_outputfile,
        run_combine_datasets=True,
        run_filter_combined_dataset=True,
        filter_combined_dataset_function=filter_combined_sn_dataset_function,
        run_convolution=True,
        verbosity=0,
    )
