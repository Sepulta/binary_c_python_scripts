"""
General plotting routines for the convolution plotting
"""

import os
import json
import time
import h5py

from PyPDF2 import PdfFileMerger
import fpdf

import numpy as np
import astropy.units as u

from grav_waves.convolution.functions.convolution_general_functions import (
    custom_json_serializer,
)

#
from grav_waves.convolution.functions.compas_metallicity_distribution import (
    find_metallicity_distribution,
)
from grav_waves.gw_analysis.functions.cosmology_functions import (
    starformation_rate,
    metallicity_distribution,
)

#
from grav_waves.convolution.functions.new_plot_routines.plot_rate_density_quantity_vs_redshift import (
    plot_quantity_vs_redshift,
)
from grav_waves.convolution.functions.new_plot_routines.plot_interpolation_datasets import (
    plot_interpolation_datasets,
)

#
# from grav_waves.convolution.functions.new_general_plot_routines.general_plot_function_metallicity_sfr_distribution import general_plot_function_metallicity_sfr_distribution

#
from david_phd_functions.plotting.pdf_functions import (
    add_pdf_and_bookmark,
    generate_chapterpage_pdf,
)
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def fix_dict(input_dict):
    """
    Recursive function that removes the quantity stuff in the dictionary
    """

    new_dict = {}

    for key in input_dict:
        if isinstance(input_dict[key], dict):
            new_dict[key] = fix_dict(input_dict[key])
        elif isinstance(input_dict[key], u.Quantity):
            new_dict[key] = str(input_dict[key])
        elif isinstance(input_dict[key], np.ndarray):
            with np.printoptions(precision=2, suppress=True, threshold=5):
                new_dict[key] = str(input_dict[key])
        else:
            new_dict[key] = input_dict[key]

    return new_dict


def generate_frontpage_pdf_sfr_convolution(
    dataset_dict, cosmology_configuration, convolution_configuration, output_name
):
    """
    Function to generate a pdf containing some information about the dataset
    """

    sim_name = dataset_dict["sim_name"]

    # Write settings to pdf
    pdf = fpdf.FPDF(format="letter")
    pdf.add_page()
    pdf.set_font("Arial", size=24)
    pdf.write(10, "{} plots".format(sim_name))
    pdf.ln()
    pdf.set_font("Arial", size=12)
    pdf.ln()
    pdf.write(
        5,
        "pdf generated on: {}".format(
            time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        ),
    )
    pdf.ln()
    pdf.ln()
    pdf.write(10, "SETTINGS")
    pdf.ln()
    pdf.ln()
    pdf.write(10, "Cosmology settings")
    pdf.write(
        5,
        json.dumps(
            fix_dict(cosmology_configuration), indent=4, default=custom_json_serializer
        ),
    )
    pdf.ln()
    pdf.ln()
    pdf.write(10, "Convolution settings")
    pdf.write(
        5,
        json.dumps(
            fix_dict(convolution_configuration),
            indent=4,
            default=custom_json_serializer,
        ),
    )
    pdf.ln()
    pdf.output(output_name)


def general_plot_routine(
    convolution_dataset_hdf5_filename,
    main_plot_dir,
    output_pdf_filename,
    dco_type="bhbh",
    rate_type="merger_rate",
    querylist=None,
    add_cdf=True,
    add_ratio=True,
    num_procs=1,
    num_procs_bootstrap=1,
    bootstraps=0,
    quantity_vs_redshift_quantities=[
        "primary_mass",
        "mass_ratio",
        "total_mass",
        "chirp_mass",
    ],
    include_starformation_rate_plots=True,
    include_plot_birthrate=True,
    include_quantity_vs_redshift_plots=True,
    include_interpolation_error_plot=True,
):
    """
    Function to plot all the convolved quantities

    plots all above plots multiple times with different probability floors
    """

    #########################
    # settings
    probability_floors = [3]
    format_types = ["pdf"]
    contourlevels = [1e-1, 1e0, 1e1, 1e2]
    linestyles_contourlevels = ["solid", "dashed", "-.", ":"]

    # bins
    bins = {
        "chirp_mass": np.arange(0, 60, 1),
        "total_mass": np.arange(0, 140, 1),
        "primary_mass": np.arange(0, 70, 1),
        "secondary_mass": np.arange(0, 70, 1),
        "any_mass": np.arange(0, 70, 1),
        "mass_ratio": np.arange(0, 1, 0.01),
    }

    # Querylist for any mass plots
    querylist_any_mass = [
        {"name": "No PPISN", "query": "(undergone_ppisn==0)"},
        {"name": "PPISN", "query": "(undergone_ppisn==1)"},
    ]

    ################
    # Read out some info from the hdf5 file
    convolution_dataset_hdf5_file = h5py.File(convolution_dataset_hdf5_filename)
    dataset_dict = json.loads(
        convolution_dataset_hdf5_file["settings/dataset_dict"][()]
    )
    sim_name = dataset_dict["sim_name"]
    cosmology_configuration = json.loads(
        convolution_dataset_hdf5_file["settings/cosmology_configuration"][()]
    )
    config_dict_convolution = json.loads(
        convolution_dataset_hdf5_file["settings/convolution_configuration"][()]
    )
    convolution_dataset_hdf5_file.close()

    #
    print("Generating plots for {}".format(sim_name))

    #########################
    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    #
    plot_dir = os.path.abspath(main_plot_dir)  # Just use the passed in plot dir.
    os.makedirs(os.path.join(plot_dir, "pdf"), exist_ok=True)
    os.makedirs(os.path.join(plot_dir, dco_type, "pdf"), exist_ok=True)

    # Generate frontpage pdf
    print("\tGenerating frontpage")
    frontpage_name = os.path.join(plot_dir, "pdf", "frontpage.pdf")
    generate_frontpage_pdf_sfr_convolution(
        dataset_dict, cosmology_configuration, config_dict_convolution, frontpage_name
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger, frontpage_name, pdf_page_number, bookmark_text="Frontpage"
    )

    ###########################################################################
    # Starformation rate, metallicity distribution and combination plot and all the plots
    starformation_rates_and_metallicity_distribution_chapter_filename = os.path.join(
        plot_dir,
        dco_type,
        "pdf",
        "starformation_rates_and_metallicity_distribution_chapter.pdf",
    )
    generate_chapterpage_pdf(
        "Starformation rates and metallicity distribution",
        starformation_rates_and_metallicity_distribution_chapter_filename,
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        starformation_rates_and_metallicity_distribution_chapter_filename,
        pdf_page_number,
        bookmark_text="Starformation rates and metallicity distribution chapter",
    )

    ######
    # Star formation rate plots
    if include_starformation_rate_plots:
        print("\tPlotting pdf for starformation rates")
        combined_sfr_plots_output_name = os.path.join(
            plot_dir, dco_type, "combined_sfr_plots.pdf"
        )
        combined_sfr_plots_output_dir = os.path.join(plot_dir, dco_type, "sfr_plots/")

        ############
        # Recalculate some values
        sampled_metallicities = 10 ** np.array(
            metallicity_dict["sorted_log10metallicity_values"]
        )

        log10_metallicities = np.log10(sampled_metallicities)
        log_metallicities = np.log(sampled_metallicities)

        max_log_z = log_metallicities.max()
        min_log_z = log_metallicities.min()

        # Get sfr value
        sfr_values = starformation_rate(
            np.array(config_dict_convolution["time_centers"]),
            cosmology_configuration=cosmology_configuration,
            verbosity=1,
        )

        if cosmology_configuration["metallicity_distribution_function"] in [
            "vanSon21",
            "neijssel19",
            "COMPAS",
        ]:
            # Calculate the metallicity distribution
            dPdlogZ, metallicities, p_draw_metallicity = find_metallicity_distribution(
                np.array(config_dict_convolution["time_centers"]),
                min_log_z,
                max_log_z,
                **cosmology_configuration["metallicity_distribution_args"],
            )
        else:
            raise ValueError("unknown distribution")

        #####
        # SFR and metallicity distribution plots: sequence of plots to show the starformation rate and all the mutations on this
        general_plot_function_metallicity_sfr_distribution(
            output_pdf_dir=combined_sfr_plots_output_dir,
            combined_pdf_filename=combined_sfr_plots_output_name,
            redshifts=config_dict_convolution["time_centers"],
            sfr_values=sfr_values,
            dPdlogZ=dPdlogZ,
            metallicities=metallicities,
            sampled_metallicities=sampled_metallicities,
            p_draw_metallicity=p_draw_metallicity,
            distribution=cosmology_configuration["metallicity_distribution_function"],
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            combined_sfr_plots_output_name,
            pdf_page_number,
            bookmark_text="SFR plots",
            add_source_bookmarks=True,
        )

    ######
    # Plot the birth rate: 2-d plot with redshift on x-axis and metallicity on y-axis, showing the birth rate of systems at that z,Z location
    if include_plot_birthrate:
        print("\tPlotting plot_birthrate detailed")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            plot_birthrate_filename = os.path.join(
                plot_dir, dco_type, format_type, "plot_birthrate.{}".format(format_type)
            )
            plot_birthrate(
                convolution_dataset_hdf5_filename,
                plot_settings={
                    "output_name": plot_birthrate_filename,
                    "block": False,
                    "show_plot": False,
                    "simulation_name": sim_name,
                    "antialiased": True,
                    "rasterized": True,
                    "runname": "plot_birthrate detailed",
                },
            )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_birthrate_filename,
            pdf_page_number,
            bookmark_text="plot_birthrate",
        )

    ###########################################################################
    # Intrinsic rate densities quantity vs redshift: 2-d
    merger_rate_mass_quantity_chapter_filename = os.path.join(
        plot_dir, dco_type, "pdf", "merger_rate_mass_quantity_chapter.pdf"
    )
    generate_chapterpage_pdf(
        "Merger rates per mass quantity plots",
        merger_rate_mass_quantity_chapter_filename,
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        merger_rate_mass_quantity_chapter_filename,
        pdf_page_number,
        bookmark_text="Merger rates per mass quantity chapter",
    )

    #####
    # Quantity vs redshift plots
    if include_quantity_vs_redshift_plots:
        for quantity in quantity_vs_redshift_quantities:
            for format_type in format_types:
                plot_quantity_vs_redshift_filename = os.path.join(
                    plot_dir,
                    dco_type,
                    format_type,
                    "plot_{}_vs_redshift_filename.{}".format(quantity, format_type),
                )
                print(
                    "Plotting {}".format(
                        os.path.basename(plot_quantity_vs_redshift_filename)
                    )
                )
                plot_quantity_vs_redshift(
                    filename=convolution_dataset_hdf5_filename,
                    rate_type=rate_type,
                    dco_type=dco_type,
                    quantity=quantity,
                    quantity_bins=bins[quantity],
                    general_query=None,
                    querylist=querylist,
                    add_ratio=add_ratio,
                    x_scale="linear",
                    y_scale="linear",
                    verbose=1,
                    plot_settings={
                        "output_name": plot_quantity_vs_redshift_filename,
                        "block": False,
                        "show_plot": False,
                        "simulation_name": sim_name,
                        "antialiased": True,
                        "rasterized": True,
                        "runname": "plot_birthrate detailed",
                    },
                )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                plot_quantity_vs_redshift_filename,
                pdf_page_number,
                bookmark_text="plot_{}_vs_redshift_filename.{}".format(
                    quantity, format_type
                ),
            )

    ###########################################################################
    # Total intrinsic rate at all lookbacks
    total_intrinsic_rate_over_redshift_chapter_filename = os.path.join(
        plot_dir, dco_type, "pdf", "total_intrinsic_rate_over_redshift_chapter.pdf"
    )
    generate_chapterpage_pdf(
        "Total intrinsic rates plots",
        total_intrinsic_rate_over_redshift_chapter_filename,
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        total_intrinsic_rate_over_redshift_chapter_filename,
        pdf_page_number,
        bookmark_text="Total intrinsic rates over redshift chapter",
    )

    ###########################################################################
    # Details about the redshift interpolator
    interpolation_analysis_chapter_filename = os.path.join(
        plot_dir, dco_type, "pdf", "interpolation_analysis_chapter.pdf"
    )
    generate_chapterpage_pdf(
        "Redshift interpolation error chapter", interpolation_analysis_chapter_filename
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        interpolation_analysis_chapter_filename,
        pdf_page_number,
        bookmark_text="Redshift interpolation error chapter",
    )

    #####
    # redshift analysis: panel showing the accuracy of the redshift interpolation function.
    if include_interpolation_error_plot:
        print("\tPlotting redshift interpolation error analysis")
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            plot_interpolation_datasets_filename = os.path.join(
                plot_dir,
                dco_type,
                format_type,
                "redshift_interpolation_error_analysis.{}".format(format_type),
            )
            plot_interpolation_datasets(
                config_dict_convolution,
                size_test_samples=200,
                plot_log=True,
                test_log=True,
                plot_settings={
                    "show_plot": False,
                    "output_name": plot_interpolation_datasets_filename,
                    "simulation_name": sim_name,
                    "antialiased": True,
                    "rasterized": True,
                    "runname": "plot_interpolation_datasets",
                },
            )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_interpolation_datasets_filename,
            pdf_page_number,
            bookmark_text="redshift interpolation error analysis",
        )

    # wrap up the pdf
    merger.write(output_pdf_filename)
    merger.close()

    print(
        "Finished generating plots for dataset {}. Wrote results to {}".format(
            dataset_dict["sim_name"], os.path.abspath(output_pdf_filename)
        )
    )


if __name__ == "__main__":

    #
    convolution_dataset_hdf5_filename = "/home/david/projects/binary_c_root/results/GRAV_WAVES/convolution_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/rebinned_convolution_results.h5"
    main_plot_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "plots/")
    output_pdf_filename = "test.pdf"

    querylist = [
        {"name": "CE channel", "query": "(comenv_counter >= 1)"},
        {"name": "No CE channel", "query": "(comenv_counter == 0)"},
    ]

    general_plot_routine(
        convolution_dataset_hdf5_filename=convolution_dataset_hdf5_filename,
        main_plot_dir=main_plot_dir,
        output_pdf_filename=output_pdf_filename,
        dco_type="bhbh",
        rate_type="merger_rate",
        querylist=querylist,
        quantity_vs_redshift_quantities=[
            "mass_ratio",
            "primary_mass",
            "total_mass",
            "chirp_mass",
        ],
        add_ratio=True,
        num_procs=1,
        num_procs_bootstrap=1,
        bootstraps=0,
        include_starformation_rate_plots=False,
        include_plot_birthrate=False,
        include_quantity_vs_redshift_plots=True,
        include_interpolation_error_plot=False,
    )
