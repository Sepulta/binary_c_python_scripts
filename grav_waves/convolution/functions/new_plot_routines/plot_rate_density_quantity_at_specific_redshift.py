"""
Function to plot a quantity at specific redshift
"""

import h5py
import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    extract_columns_from_querylist,
    quantity_name_dict,
    quantity_unit_dict,
    rate_type_name_dict,
    rate_density_units,
    add_columns_to_df,
    dco_type_query,
    readout_rate_array_specific_keys,
    create_centers_from_bins,
    get_closest_redshift_key,
    get_histogram_data,
    get_KDE_data,
    return_1d_plot_canvas,
    plot_1d_results,
    color_list,
    hatch_list,
)
from grav_waves.convolution.functions.new_plot_routines.plot_kde_and_bootstrap_functions import (
    run_bootstrap,
)
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.canvas_functions import return_canvas_with_subsets
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def readout_rate_data_run_bootstraps(
    combined_df,
    rate_type,
    dco_type,
    quantity,
    quantity_bins,
    redshift_value,
    add_kde,
    bootstraps=0,
    num_procs=1,
    general_query=None,
    query=None,
):
    """
    Function to read out the rate data and get the histogram data, the KDE data and the bootstrapped data
    """

    # Get the quantity data from the dataframe
    quantity_data = combined_df[quantity]

    ################
    # Create masks and apply, taking into account that the query and general query can be None
    dco_mask = combined_df.eval(dco_type_query(dco_type)).to_numpy()
    general_query_mask = (
        combined_df.eval(general_query)
        if general_query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    query_mask = (
        combined_df.eval(query)
        if query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    mask = dco_mask * general_query_mask * query_mask

    # mask the quantity data
    masked_quantity_data = quantity_data[mask].to_numpy()

    ################
    # Read out rate data for this specific redshift
    datafile = h5py.File(filename)
    all_redshift_keys = sorted(list(datafile["data/{}".format(rate_type)].keys()))
    closest_redshift_key = get_closest_redshift_key(redshift_value, all_redshift_keys)
    rate_array, redshifts = readout_rate_array_specific_keys(
        datafile=datafile,
        rate_key=rate_type,
        mask=mask,
        redshift_keys=[closest_redshift_key],
    )
    datafile.close()

    ################
    # Calculate the histogram data
    hist, bincenter, truncated_bins = get_histogram_data(
        bins=quantity_bins, data_array=masked_quantity_data, weight_array=rate_array[0]
    )

    #
    return_dict = {
        "centers": bincenter,
        "rates": hist,
    }

    ################
    # Calculate the KDE data
    if add_kde:
        y_vals, center_KDEbins, kde_width = get_KDE_data(
            bins=truncated_bins,
            hist=hist,
            data_array=masked_quantity_data,
            weight_array=rate_array[0],
        )

        #
        return_dict["kde_yvals"] = y_vals
        return_dict["center_KDEbins"] = center_KDEbins

        ################
        # Calculate the bootstrap data
        if bootstraps:
            # TODO: Ask lieke why do we use x_KDE here instead of center_KDEbins
            _, median, percentiles = run_bootstrap(
                masses=masked_quantity_data,
                weights=rate_array[0],
                indices=np.arange(len(masked_quantity_data)),
                kde_width=kde_width,
                mass_bins=truncated_bins,
                center_KDEbins=center_KDEbins,
                num_procs=num_procs,
                bootstraps=bootstraps,
            )

            #
            return_dict["median"] = median
            return_dict["percentiles"] = percentiles

    return return_dict


def get_data(
    filename,
    rate_type,
    dco_type,
    quantity,
    quantity_bins,
    divide_by_binsize,
    redshift_value,
    add_kde=False,
    bootstraps=0,
    num_procs=1,
    general_query=None,
    querylist=None,
):
    """
    Function to read out the data from the datafile
    """

    ####
    # Read out dataframe
    combined_df = pd.read_hdf(filename, "/data/combined_dataframes")

    # Add columns
    used_columns = [quantity] + extract_columns_from_querylist(querylist)
    columns_to_add = [
        column for column in used_columns if not column in combined_df.columns
    ]
    combined_df = add_columns_to_df(combined_df, columns_to_add)

    # Create result dict
    result_dict = {}

    #
    result_dict["all"] = readout_rate_data_run_bootstraps(
        combined_df=combined_df,
        rate_type=rate_type,
        dco_type=dco_type,
        quantity=quantity,
        quantity_bins=quantity_bins,
        redshift_value=redshift_value,
        general_query=general_query,
        add_kde=add_kde,
        bootstraps=bootstraps,
        num_procs=num_procs,
        query=None,
    )

    # Loop over queries
    if querylist is not None:
        for query_dict in querylist:
            result_dict[query_dict["name"]], _ = readout_rate_data_run_bootstraps(
                combined_df=combined_df,
                rate_type=rate_type,
                dco_type=dco_type,
                quantity=quantity,
                quantity_bins=quantity_bins,
                redshift_value=redshift_value,
                general_query=general_query,
                add_kde=add_kde,
                bootstraps=bootstraps,
                num_procs=num_procs,
                query=query_dict["query"],
            )

    return result_dict


def plot_rate_density_quantity_at_specific_redshift(
    filename,
    rate_type,
    dco_type,
    quantity,
    quantity_bins,
    redshift_value,
    add_kde=False,
    bootstraps=0,
    num_procs=1,
    base_querydict=None,
    querylist=None,
    add_ratio=False,
    add_cdf=False,
    x_scale="linear",
    y_scale="linear",
    verbose=0,
    plot_settings={},
):
    """
    Function to plot quantity vs redshift
    """

    #
    x_label = r"{} [{}]".format(
        quantity_name_dict[quantity], quantity_unit_dict[quantity].to_string("latex")
    )

    #
    contourlevels = [1e-1, 1e0, 1e1]
    ratio_contourlevels = [0.1, 0.5, 0.9]

    #
    quantity_bincenters = create_centers_from_bins(quantity_bins)

    #####################
    # Get results
    results_dict = get_data(
        filename=filename,
        rate_type=rate_type,
        dco_type=dco_type,
        quantity=quantity,
        quantity_bins=quantity_bins,
        redshift_value=redshift_value,
        divide_by_binsize=False,
        general_query=base_querydict["query"] if base_querydict is not None else None,
        querylist=querylist,
        add_kde=add_kde,
        bootstraps=bootstraps,
        num_procs=num_procs,
    )

    # ####################
    # Plot data

    ##
    # Set up axes
    fig = plt.figure(figsize=(40, 40))
    gs = fig.add_gridspec(nrows=12, ncols=1)

    fig, axes_dict = return_1d_plot_canvas(
        fig=fig,
        gs=gs,
        xlabel_text=x_label,
        add_ratio=add_ratio,
        add_cdf=add_cdf,
    )

    plot_1d_results(
        results_dict=results_dict,
        axes_dict=axes_dict,
        bins=quantity_bins,
        colors=color_list,
        hatch_list=hatch_list,
        querylist=querylist,
        base_querydict=base_querydict,
        add_kde=add_kde,
        add_cdf=add_cdf,
        add_ratio=add_ratio,
        bootstraps=bootstraps,
    )

    ###
    # Make up
    axes_dict["ax"].set_ylabel(
        r"{} density [{}]".format(
            rate_type_name_dict[rate_type], rate_density_units.to_string("latex")
        )
    )

    # Set limits and scale
    max_yval = results_dict["all"]["rates"].max()
    min_yval = 10 ** (np.log10(max_yval) - plot_settings.get("probability_floor", 3))

    axes_dict["ax"].set_ylim([min_yval, max_yval])
    axes_dict["ax"].set_yscale("log")

    #
    title_text = "Intrinsic {} density of {} systems for {} at redshift {}".format(
        rate_type_name_dict[rate_type].lower(),
        dco_type.upper(),
        quantity_name_dict[quantity].lower(),
        redshift_value,
    )
    axes_dict["ax"].set_title(title_text, fontsize=26)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #
    filename = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/rebinned_convolution_results.h5"

    querylist_lieke = [
        {"name": "CE channel", "query": "(comenv_counter >= 1)"},
        {"name": "No CE channel", "query": "(comenv_counter == 0)"},
    ]

    #
    plot_rate_density_quantity_at_specific_redshift(
        filename=filename,
        rate_type="merger_rate",
        dco_type="bhbh",
        quantity="primary_mass",
        quantity_bins=np.arange(0, 100, 1),
        redshift_value=0,
        add_kde=True,
        bootstraps=10,
        num_procs=4,
        # querylist=querylist_lieke,
        # add_ratio=True,
        add_cdf=False,
        add_ratio=False,
        x_scale="linear",
        y_scale="linear",
        verbose=0,
        plot_settings={"show_plot": True, "probability_floor": 3},
    )

else:
    matplotlib.use("Agg")
