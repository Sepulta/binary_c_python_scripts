"""
Support functions for the plotting of ensemble data
"""

import os
import time
import numpy as np
import json
from matplotlib import colors

from RLOF.scripts.plot_ensemble_output.settings import bins_integer_parametertypes

from ensemble_functions.ensemble_functions import (
    return_keys_bins_and_bincenters,
)
from ensemble_functions.ensemble_functions import (
    query_dict,
)
from binarycpython.utils.dicts import multiply_values_dict


def generate_ensemble_dataset_dict(simulation_path):
    """
    Function to return the dictionary containing the datasets and metallicity for a given simname root path

    looks for all the subdirectories in the main dir. Then extracts the metallicity key from the subdir and creates per-metallicity dictionaries containing metallicity info and dataset info

    It will also try to find ensemble chunks and add the first non-zero chunk for us to use in test cases
    """

    ensemble_dataset_dict = {}

    simulation_path = os.path.abspath(simulation_path)
    simname = os.path.basename(simulation_path)

    ensemble_dataset_dict["simname"] = simname
    ensemble_dataset_dict["simulation_path"] = simulation_path

    #
    dataset_dict = {}

    #
    subkeys = sorted(
        os.listdir(os.path.join(simulation_path, "population_results")), reverse=True
    )

    #
    for subkey in subkeys:
        if subkey.startswith("Z"):
            subkey_path = os.path.join(simulation_path, "population_results", subkey)

            # Select metallicity string
            metallicity_string = subkey.split("_")[-1][1:]

            #
            metallicity_dict = {}
            metallicity_dict["metallicity"] = metallicity_string

            full_dataset_filename = os.path.join(subkey_path, "ensemble_output.json")

            #
            if not os.path.exists(full_dataset_filename):
                raise ValueError(
                    "full ensemble json doesnt exist: {}".format(full_dataset_filename)
                )

            metallicity_dict["filename"] = full_dataset_filename

            # Select the first chunk that has non-zero size
            subkey_content = os.listdir(subkey_path)
            for chunk_filename in [
                el for el in subkey_content if el.startswith("ensemble_output_")
            ]:
                full_chunk_filename = os.path.join(subkey_path, chunk_filename)
                if not os.path.getsize(os.path.join(subkey_path, chunk_filename)) == 0:
                    metallicity_dict["filename_test"] = os.path.join(
                        subkey_path, chunk_filename
                    )
                    break

            #
            dataset_dict[metallicity_string] = metallicity_dict

    ensemble_dataset_dict["datasets"] = dataset_dict

    #
    return ensemble_dataset_dict


def get_ensemble_data(dataset, testing, probability_to_yield_conversion_factor=1):
    """
    Function to get the ensemble data for testing or production purposes
    """

    # Select filename
    ensemble_filename = dataset["filename"]
    if testing:
        if "filename_test" in dataset.keys():
            ensemble_filename = dataset["filename_test"]

    #
    ensemble_data = json.load(open(ensemble_filename, "r"))

    #
    if not testing:
        ensemble_data = ensemble_data["ensemble"]

    if probability_to_yield_conversion_factor != 1:
        ensemble_data = multiply_values_dict(
            ensemble_data, probability_to_yield_conversion_factor
        )

    return ensemble_data


def return_string_query(query_dict):
    """
    Function to return a string representing a query

    Input:
        query dict

    Output:
        string representing the query dict
    """

    return "{}: {}".format(query_dict["keyname"], query_dict["valuerange"])


def return_string_querylist(querylist):
    """
    Function to return a string representing a querylist

    Input:
        query list

    Output:
        string representing the query dict
    """

    return ", ".join([return_string_query(query) for query in querylist])


def get_bins_bincenters(data, parameter):
    """
    Function to get the bins and the bincenters of the selected data
    """

    if parameter in bins_integer_parametertypes.keys():
        parameter_total_bins = bins_integer_parametertypes[parameter]
        parameter_total_bincenter = (
            parameter_total_bins[1:] + parameter_total_bins[:-1]
        ) / 2
    else:
        # Get the bins and bincenters based on the total
        (
            _,
            parameter_total_bins,
            parameter_total_bincenter,
        ) = return_keys_bins_and_bincenters(data, parameter)

    return parameter_total_bins, parameter_total_bincenter


def handle_querylist(data, querylist, name, verbose=0):
    """
    Function that handles running the querylist on the dataset
    """

    if verbose:
        print("running {}".format(name))
        start_base_query = time.time()
    for query in querylist:
        data = query_dict(data, query["keyname"], query["valuerange"])

        if len(data) == 0:
            raise ValueError("Running base-query resulted in an empty dataset")
    if verbose:
        print("\ttook {}s".format(time.time() - start_base_query))

    return data


def add_2d_density_plot_to_axis(
    fig,
    axis,
    data,
    x_axis_bins,
    y_axis_bins,
    min_val,
    max_val,
    contourlevels,
    contour_linestyles,
):
    """
    Function to add a general density plot to an axis.

    Input:
        fig: figure object
        axis: axis object
        data: array with 3 elements: x-values, y-values, weight/probability values
        x_axis_bins: bins for the x- parameter
        y_axis_bins: bins for the y- parameter
    """

    #
    norm = colors.LogNorm(vmin=min_val, vmax=max_val)

    # Do numpy hist first
    hist, _, _ = np.histogram2d(
        data[0], data[1], bins=[x_axis_bins, y_axis_bins], weights=data[2]
    )

    # Set up the colormesh plot
    x_axis_bincenters = (x_axis_bins[1:] + x_axis_bins[:-1]) / 2
    y_axis_bincenters = (y_axis_bins[1:] + y_axis_bins[:-1]) / 2

    X, Y = np.meshgrid(x_axis_bincenters, y_axis_bincenters)

    _ = axis.pcolormesh(
        X,
        Y,
        hist.T,
        norm=norm,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    #
    if contourlevels is not None:
        #
        _ = axis.contour(
            X,
            Y,
            hist.T,
            levels=contourlevels,
            colors="k",
            linestyles=contour_linestyles,
        )

    return fig, axis
