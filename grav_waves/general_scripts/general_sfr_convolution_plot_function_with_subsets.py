"""
Function to plot the queries with subset plots
"""

import os
import pickle

from grav_waves.general_scripts.plot_with_subset_functions import (
    plot_convolved_rate_density_per_mass_quantity_with_subsets,
    plot_total_intrinsic_rate_density_at_all_lookback_with_subsets,
)

from PyPDF2 import PdfFileMerger
import fpdf

from david_phd_functions.plotting.pdf_functions import (
    add_pdf_and_bookmark,
    generate_chapterpage_pdf,
)


def general_sfr_convolution_plot_function_with_subsets(
    dataset_dict,
    query_dataset_dict,
    plot_dir,
    output_pdf_filename,
    convolution_configuration,
    dco_type="bhbh",
    include_plot_convolved_rate_density_per_mass_quantity_with_subsets=True,
    include_plot_total_intrinsic_rate_density_at_all_lookback_with_subsets=True,
):
    """
    Function to run the plotting routines for the subset of queries
    """

    #
    probability_floors = [4, 6]
    format_types = ["png", "pdf"]

    #
    simname = dataset_dict["sim_name"]

    #
    os.makedirs(plot_dir, exist_ok=True)
    os.makedirs(os.path.join(plot_dir, "pdf"), exist_ok=True)
    os.makedirs(os.path.join(plot_dir, dco_type, "pdf"), exist_ok=True)

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_list = []
    pdf_page_number = 0

    # Set rate types
    rate_types = ["merger_rate"]
    if convolution_configuration["include_formation_rates"]:
        rate_types.append("formation_rate")

    #
    print("Generating plots for {}".format(simname))

    convolution_dataset_dict = {}
    for query in query_dataset_dict.keys():
        convolution_dataset_dict[query] = pickle.load(
            open(query_dataset_dict[query]["result_filename"], "rb")
        )

    # Plot
    for mass_type in ["chirpmass", "total_mass", "primary_mass", "any_mass"]:
        for rate_type in rate_types:
            print(
                "\tPlotting plot_convolved_rate_density_per_mass_quantity_with_subsets with {} over all time for {}".format(
                    rate_type, mass_type
                )
            )
            if include_plot_convolved_rate_density_per_mass_quantity_with_subsets:
                try:
                    for probability_floor in probability_floors:
                        print(
                            "\t\tUsing probability floor {}".format(probability_floor)
                        )
                        for format_type in format_types:
                            print("\t\t\tSaving as {}".format(format_type))
                            plot_convolved_rate_density_per_mass_quantity_with_subsets(
                                convolution_dataset_dict,
                                rate_type=rate_type,
                                mass_type=mass_type,
                                plot_settings={
                                    "show_plot": False,
                                    "probability_floor": probability_floor,
                                    "output_name": os.path.join(
                                        plot_dir,
                                        dco_type,
                                        format_type,
                                        "plot_convolved_rate_density_per_mass_quantity_with_subsets_{}_{}_{}floor.{}".format(
                                            rate_type,
                                            mass_type,
                                            probability_floor,
                                            format_type,
                                        ),
                                    ),
                                    "simulation_name": simname,
                                    "antialiased": True,
                                    "rasterized": True,
                                    "runname": "plot_convolved_rate_density_per_mass_quantity_with_subsets: mass quantity: {} rate type: {} probability floor: {}".format(
                                        mass_type, rate_type, probability_floor
                                    ),
                                },
                            )
                        pdf_list.append(
                            os.path.join(
                                plot_dir,
                                dco_type,
                                "pdf",
                                "plot_convolved_rate_density_per_mass_quantity_with_subsets_{}_{}_{}floor.pdf".format(
                                    rate_type, mass_type, probability_floor
                                ),
                            )
                        )
                        merger, pdf_page_number = add_pdf_and_bookmark(
                            merger,
                            pdf_list[-1],
                            pdf_page_number,
                            bookmark_text="Intrinstic {} density for {}: floor={}".format(
                                rate_type, mass_type, probability_floor
                            ),
                        )
                except ValueError:
                    print(
                        "Could not properly generate the plot for {} over all time for {}".format(
                            rate_type, mass_type
                        )
                    )

    # plot_total_intrinsic_rate_density_at_all_lookback_with_subsets
    for rate_type in rate_types:
        print(
            "\tPlotting plot_total_intrinsic_rate_density_at_all_lookback_with_subsets {} over all time for {}".format(
                rate_type, mass_type
            )
        )
        if include_plot_total_intrinsic_rate_density_at_all_lookback_with_subsets:
            try:
                for format_type in format_types:
                    print("\t\t\tSaving as {}".format(format_type))
                    plot_total_intrinsic_rate_density_at_all_lookback_with_subsets(
                        convolution_dataset_dict,
                        rate_type=rate_type,
                        plot_settings={
                            "show_plot": False,
                            "probability_floor": probability_floor,
                            "output_name": os.path.join(
                                plot_dir,
                                dco_type,
                                format_type,
                                "plot_total_intrinsic_rate_density_at_all_lookback_with_subsets_{}_{}.{}".format(
                                    rate_type, mass_type, format_type
                                ),
                            ),
                            "simulation_name": simname,
                            "antialiased": True,
                            "rasterized": True,
                            "runname": "plot_convolved_rate_density_per_mass_quantity_with_subsets: mass quantity: {} rate type: {}".format(
                                mass_type, rate_type
                            ),
                        },
                    )
                pdf_list.append(
                    os.path.join(
                        plot_dir,
                        dco_type,
                        "pdf",
                        "plot_total_intrinsic_rate_density_at_all_lookback_with_subsets_{}_{}.pdf".format(
                            rate_type, mass_type
                        ),
                    )
                )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    pdf_list[-1],
                    pdf_page_number,
                    bookmark_text="Total Intrinstic {} density for {}".format(
                        rate_type, mass_type
                    ),
                )
            except ValueError:
                print(
                    "Could not properly generate the plot for {} over all time for {}".format(
                        rate_type, mass_type
                    )
                )

    # wrap up the pdf
    merger.write(output_pdf_filename)
    merger.close()

    print(
        "Finished generating plots for dataset {}. Wrote results to {}".format(
            simname, output_pdf_filename
        )
    )
