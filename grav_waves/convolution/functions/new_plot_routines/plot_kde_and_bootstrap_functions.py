"""
Bootstrap and KDE functions
"""

from multiprocessing import Pool

import numpy as np

from scipy import stats
from scipy.stats import gaussian_kde


class MirroredKDE(gaussian_kde):
    """KDE class that mirrors data at boundaries to account for bounded support. Copied from Toms notebook"""

    def __init__(
        self,
        data,
        weights=None,
        lower_bound=None,
        upper_bound=None,
        bw_method=None,
        bw_adjust=None,
    ):
        """instantiate class in similar way to scipy but with some additions"""
        super().__init__(data, weights=weights, bw_method=bw_method)

        # also store the lower and upper bounds
        self._lower_bound = lower_bound
        self._upper_bound = upper_bound

        # allow adjustment of the default bandwidth similar to seaborn
        if bw_adjust is not None:
            self.set_bandwidth(self.factor * bw_adjust)

    def evaluate(self, x_vals=None, x_min=None, x_max=None, x_count=200):
        """evaluate the kde taking into account the boundaries"""

        # only return x_vals when they aren't supplied
        return_x_vals = x_vals is None

        if x_vals is None:
            if x_min is None:
                x_min = np.min(self.dataset)
            if x_max is None:
                x_max = np.max(self.dataset)
            x_vals = np.linspace(x_min, x_max, x_count)

        # make a copy of the data before I mirror anything
        unmirrored_x_vals = np.copy(x_vals)

        # evaluate the kde at the original x values
        kde_vals = super().evaluate(x_vals)

        # if either bound is present then mirror the data and
        # add the evaluated kde for the mirrored data to the original
        if self._lower_bound is not None:
            x_vals = 2.0 * self._lower_bound - x_vals
            kde_vals += super().evaluate(x_vals)
            x_vals = unmirrored_x_vals

        if self._upper_bound is not None:
            x_vals = 2.0 * self._upper_bound - x_vals
            kde_vals += super().evaluate(x_vals)
            x_vals = unmirrored_x_vals

        if return_x_vals:
            return x_vals, kde_vals
        else:
            return kde_vals


def calculate_kde_values(
    data,
    weights,
    center_KDEbins,
    bw_method,
    hist,
    lower_bound=None,
    upper_bound=None,
    use_mirrored_kde=False,
):
    """
    Function to calculate the KDE values.

    We can calculate the KDE via the mirrored kde from Tom if use_mirrored_kde=True
    """

    # Toms method
    if use_mirrored_kde:
        kde = MirroredKDE(
            data,
            weights=weights,
            lower_bound=lower_bound,
            upper_bound=upper_bound,
            bw_method=bw_method,
        )
        y_vals = kde.evaluate(center_KDEbins) * sum(hist)
    else:
        kernel = stats.gaussian_kde(
            data,
            weights=weights,
            bw_method=bw_method,
        )
        y_vals = kernel(center_KDEbins) * sum(
            hist
        )  # /binwidth) * (np.diff(x_KDE))#re-normalize the KDE

    return y_vals


def run_bootstrap_worker_function(task_list):
    """
    Worker function to handle the bootstrapping
    """

    task_ID = task_list[0]
    masses = task_list[1]
    weights = task_list[2]
    indices = task_list[3]
    kde_width = task_list[4]
    mass_bins = task_list[5]
    center_KDEbins = task_list[6]

    # TODO: ask lieke, should we not use the weights of the systems here to properly replace?
    boot_index = np.random.choice(indices, size=len(indices), replace=True)

    # Set up the kernel and make the histogram
    kernel = stats.gaussian_kde(
        masses[boot_index], bw_method=kde_width, weights=weights[boot_index]
    )
    Hist, _ = np.histogram(
        masses[boot_index], bins=mass_bins, weights=weights[boot_index], density=False
    )

    #
    res = kernel(center_KDEbins) * sum(Hist)
    print("Finished bootstrap {}".format(task_ID))

    return res


def run_bootstrap(
    masses,
    weights,
    indices,
    kde_width,
    mass_bins,
    center_KDEbins,
    num_procs=1,
    bootstraps=50,
):
    """
    Function to multiprocess the bootstrapping
    """

    print("running {} bootstrap with {} processes".format(bootstraps, num_procs))

    #
    hist_vals = np.zeros((bootstraps, len(center_KDEbins)))  # center_bins

    # Set up pool an create result
    with Pool(processes=num_procs) as pool:

        #
        bootstrapped_results = pool.map(
            run_bootstrap_worker_function,
            [
                (
                    task_ID,
                    masses,
                    weights,
                    indices,
                    kde_width,
                    mass_bins,
                    center_KDEbins,
                )
                for task_ID in range(bootstraps)
            ],
        )

    # Put in the correct form
    for i, bootstrap_sample in enumerate(bootstrapped_results):
        hist_vals[i] = bootstrap_sample

    # Calculate median and percentiles
    # calculate 1- and 2- sigma percentiles
    median = np.percentile(hist_vals, [50], axis=0)
    percentiles = np.percentile(hist_vals, [15.89, 84.1, 2.27, 97.725], axis=0)

    return hist_vals, median, percentiles
