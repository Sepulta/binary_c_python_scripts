"""
Script containing the parse_function to read out RLOF episode lines
"""

import os

from event_based_logging_functions.events_parser import events_parser


parameters_RLOF = [
    # 1:        time
    "time",
    ## ZAMS values
    # 2-4:                        ZAMS values
    "zams_mass_1",
    "zams_mass_2",
    "zams_orbital_period",
    ## Initial RLOF episode values
    # 5-6:                        masses
    "initial_mass_accretor",
    "initial_mass_donor",
    # 7-8:                        radii
    "initial_radius_accretor",
    "initial_radius_donor",
    # 9-10:                       separation and orbital period
    "initial_separation",
    "initial_orbital_period",
    # 11-12:                      stellar types
    "initial_stellar_type_accretor",
    "initial_stellar_type_donor",
    # 13-14:                      orbital angular momentum and RLOF stability
    "initial_orbital_angular_momentum",
    "initial_stability",
    # 15-16:                      star numbers
    "initial_starnum_accretor",
    "initial_starnum_donor",
    # 17-18:                      time and disk accretion
    "initial_time",
    "initial_disk",
    ## final values RLOF episode
    # 19-20:                      masses
    "final_mass_accretor",
    "final_mass_donor",
    # 21-22:                      radii
    "final_radius_accretor",
    "final_radius_donor",
    # 23-24:                      separation and orbital period
    "final_separation",
    "final_orbital_period",
    # 25-26:                      stellar types
    "final_stellar_type_accretor",
    "final_stellar_type_donor",
    # 27-28:                      orbital angular momentum and RLOF stability
    "final_orbital_angular_momentum",
    "final_stability",
    # 29-30:                      star numbers
    "final_starnum_accretor",
    "final_starnum_donor",
    # 31-32:                      time and disk accretion
    "final_time",
    "final_disk",
    ## Cumulative total values RLOF episode
    # 33                          time spent
    "total_time_spent_masstransfer",
    # 34-38:                      masses
    "total_mass_lost_from_accretor",
    "total_mass_lost_from_disk",
    "total_mass_lost",
    "total_mass_accreted",
    "total_mass_transferred",
    # 39-44:                      angular momenta
    "total_orbital_angular_momentum_lost_through_mass_loss_from_star",
    "total_orbital_angular_momentum_lost_through_mass_loss_from_disk",
    "total_orbital_angular_momentum_lost_through_mass_loss",
    "total_orbital_angular_momentum_stream_excess_returned_to_orbit",
    "total_orbital_angular_momentum_stellar_overspin_returned_to_orbit",
    "total_orbital_angular_momentum_stream_torque_with_orbit",
    # 45:                         total mass transferred through disk
    "total_mass_transferred_through_disk",
    # 46-50:                      total mass transferred through disk per category
    "total_mass_transferred_through_disk_MT_category_COLD",
    "total_mass_transferred_through_disk_MT_category_UNSTABLE",
    "total_mass_transferred_through_disk_MT_category_HOT",
    "total_mass_transferred_through_disk_MT_category_PARTIALLY_THICK",
    "total_mass_transferred_through_disk_MT_category_THICK",
    # 51:                         total time spent disk MT
    "total_time_spent_disk_masstransfer",
    # 52-56:                      total time spent disk MT per category
    "total_time_spent_disk_masstransfer_MT_category_COLD",
    "total_time_spent_disk_masstransfer_MT_category_UNSTABLE",
    "total_time_spent_disk_masstransfer_MT_category_HOT",
    "total_time_spent_disk_masstransfer_MT_category_PARTIALLY_THICK",
    "total_time_spent_disk_masstransfer_MT_category_THICK",
    # 57-58:                       time and mass weighted synchronicity factors. Use these and the total mass transferred to calculated the weighted averages
    "time_weighted_sum_synchronicity_factor",
    "mass_weighted_sum_synchronicity_factor",
    # 58-60: episode_number and probability and random seed
    "episode_number",
    "probability",
    "random_seed",
    # 61-64: whether stars should have undergone CHE
    "initial_accretor_should_have_undergone_CHE",
    "initial_donor_should_have_undergone_CHE",
    "final_accretor_should_have_undergone_CHE",
    "final_donor_should_have_undergone_CHE",
    ## Added by python:
    # 61: Unique identifier
    "unique_identifier",
]


# Create filename
def RLOF_parsing(line, parameters_RLOF, outfilename_RLOF, separator, unique_identifier):
    """
    Function to handle the RLOF parsing
    """

    if line.startswith("RLOF_EPISODE"):
        values = line.split()[1:]

        # Check if the length of the output is the same as we expect
        if not len(values) == len(parameters_RLOF) - 1:
            print(
                "Length of readout values ({}) is not equal to length of parameters ({})".format(
                    len(values), len(parameters_RLOF) - 1
                )
            )
            raise ValueError

        # Write to file
        with open(outfilename_RLOF, "a") as output_filehandle:
            output_filehandle.write(separator.join(values + [unique_identifier]) + "\n")


def parse_function(self, output):
    """
    Function to read out the RLOF episode lines that contain a summary of the RLOF episode
    """

    # Separator
    separator = "\t"

    # Get some information from the
    data_dir = self.custom_options["data_dir"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)
    # output_file = os.path.join(data_dir, "RLOF_episode_data.dat")

    # # Create unique number
    # unique_identifier = str(secrets.randbits(50))

    # # check if output file exists, if not, write it and create headers
    # if not os.path.exists(output_file):
    #     with open(output_file, "w") as output_filehandle:
    #         output_filehandle.write(separator.join(parameters_RLOF) + "\n")

    # # Loop over the output
    # for line in output_lines(output):
    #     RLOF_parsing(
    #         line=line,
    #         parameters_RLOF=parameters_RLOF,
    #         outfilename_RLOF=output_file,
    #         separator=separator,
    #         unique_identifier=unique_identifier,
    #     )

    ####################
    # Handle the events output
    events_parser(self, separator, data_dir, output)
