"""
Script containing the main function to generate all the tables and figures for the thesis
"""

import os


def generate_thesis_figures_and_tables(thesis_root):
    """
    function to generate all the tables and figures for the thesis
    """

    ##########
    # Set up specific directories
    figures_root = os.path.join(thesis_root, "Figures")
    tables_root = os.path.join(thesis_root, "Tables")

    os.makedirs(figures_root, exist_ok=True)
    os.makedirs(tables_root, exist_ok=True)

    print(figures_root)
    print(tables_root)


if __name__ == "__main__":

    thesis_root = "/home/david/papers/thesis_dd_hendriks/thesis_tex"
    thesis_root = "figures_and_tables"

    generate_thesis_figures_and_tables(thesis_root)
