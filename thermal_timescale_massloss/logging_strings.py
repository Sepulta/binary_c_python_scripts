single_logging_string = """
// make sure he core mass and CO core mass are set properly.

if(stardata->star[0].stellar_type>=NS)
{
    if (stardata->model.time < stardata->model.max_evolution_time)
    {
        Printf("DAVID_PPISN_SINGLE %30.12e "
            "%g %g "
            "%d %d "
            "%g %g "
            "%g %g %g %g %g %d\\n",
            //
            stardata->model.time,                                     // 1

            // Some masses
            stardata->star[0].mass,                                   // 2
            stardata->star[0].pms_mass,                               // 3

            // Stellar typ info
            stardata->star[0].stellar_type,                           // 6
            stardata->previous_stardata->star[0].stellar_type,        // 7

            // model stuff
            stardata->common.metallicity,                             // 10
            stardata->model.probability,                              // 11

            // Some info about the SN
            stardata->previous_stardata->star[0].mass,                // 21
            stardata->previous_stardata->star[0].core_mass,           // 22
            stardata->previous_stardata->star[0].he_core_mass,        // 22
            stardata->previous_stardata->star[0].CO_core_mass,        // 22
            stardata->star[0].metal_core_mass,                        //
            stardata->star[0].SN_type                                 // 23
        );
    }
    /* Kill the simulation to save time */
    stardata->model.max_evolution_time = stardata->model.time - stardata->model.dtm;
}
"""
