import os
import json
import time
import pickle
import sys
import shutil

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import (
    get_help_all,
    get_help,
    create_hdf5,
    output_lines,
)

from david_phd_functions.binaryc.personal_defaults import personal_defaults

# from functions import *

"""
Script to analyse the runtime for the threads by using a representable script.
"""


def parse_function(self, output):
    """
    Parse function for binary compact objects
    """

    # Get some information from the
    data_dir = self.custom_options["data_dir"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # Set outfile name
    outfilename = os.path.join(
        data_dir, "compact_objects-{}.dat".format(self.process_ID)
    )

    # Create filename
    parameters = [
        "time",  # 1
        "mass_1",
        "zams_mass_1",
        "mass_2",
        "zams_mass_2",  # 2-5
        "stellar_type_1",
        "prev_stellar_type_1",
        "stellar_type_2",
        "prev_stellar_type_2",  # 6-9
        "metallicity",
        "probability",  # 10-11
        "separation",
        "eccentricity",
        "period",
        "merger_time",  # 12-15
        "zams_period",
        "zams_separation",
        "zams_eccentricity",
        "prev_eccentricity",  # 16-19
        "prev_period",
        "prev_separation",
        "prev_mass_1",
        "prev_core_mass_1",  # 20-23
        "prev_CO_core_mass_1",
        "prev_He_core_mass_1",
        "prev_mass_2",
        "prev_core_mass-2",  # 24-27
        "prev_CO_core_mass_2",
        "prev_He_core_mass_2",
        "fallback_1",
        "fallback_2",  # 28-31
        "fallback_mass_1",
        "fallback_mass_2",
        "random_seed",
        "undergone_ppisn_1",  # 32-35
        "undergone_ppisn_2",  # 36
    ]

    separator = "\t"

    for line in output_lines(output):
        if line.startswith("DAVID_DCO"):
            values = line.split()[1:]

            # Check whether the amount of parameters is the same as amount of values
            if not len(values) == len(parameters):
                print("Length of readout vaues is not equal to length of parameters")
                raise ValueError

            if not os.path.exists(outfilename):
                with open(outfilename, "w") as f:
                    f.write(separator.join(parameters) + "\n")

            with open(outfilename, "a") as f:
                f.write(separator.join(values) + "\n")


## Make population and set value
test_pop = Population()
test_pop.set(verbosity=10)

# Set grid variables
# resolution = {'M_1': 50, 'q': 50, 'per': 100}
# resolution = {'M_1': 100, 'q': 100, 'per': 200}
resolution = {"M_1": 10, "q": 5, "per": 5}

test_pop.add_grid_variable(
    name="lnm1",
    longname="Primary mass",
    valuerange=[7.5, 150],
    resolution="{}".format(resolution["M_1"]),
    spacingfunc="const(math.log(7.5), math.log(300), {})".format(resolution["M_1"]),
    precode="M_1=math.exp(lnm1)",
    probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 301, -1.3, -2.3, -2.3)*M_1",
    dphasevol="dlnm1",
    parameter_name="M_1",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
)

test_pop.add_grid_variable(
    name="q",
    longname="Mass ratio",
    valuerange=["0.1/M_1", 1],
    resolution="{}".format(resolution["q"]),
    spacingfunc="const(0.1/M_1, 1, {})".format(resolution["q"]),
    probdist="flatsections(q, [{'min': 0.1/M_1, 'max': 1.0, 'height': 1}])",
    dphasevol="dq",
    precode="M_2 = q * M_1",
    parameter_name="M_2",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
)

test_pop.add_grid_variable(
    name="log10per",  # in days
    longname="log10(Orbital_Period)",
    valuerange=[0.15, 5.5],
    resolution="{}".format(resolution["per"]),
    spacingfunc="const(0.15, 5.5, {})".format(resolution["per"]),
    precode="""orbital_period = 10** log10per
sep = calc_sep_from_period(M_1, M_2, orbital_period)
sep_min = calc_sep_from_period(M_1, M_2, 10**0.15)
sep_max = calc_sep_from_period(M_1, M_2, 10**5.5)""",
    probdist="sana12(M_1, M_2, sep, orbital_period, sep_min, sep_max, math.log10(10**0.15), math.log10(10**5.5), -0.55)",
    parameter_name="orbital_period",
    dphasevol="dlog10per",
)

test_pop.set(**personal_defaults)

test_pop.set(
    david_logging_function=8,
    binary=1,
    amt_cores=2,
    # amt_cores=24,
    orbital_period=400,
    M_1=5,
    M_2=3,
    wind_mass_loss=2,
    BH_prescription=4,
    PPISN_prescription=1,
    log_runtime_systems=1,
    max_queue_size=100,
)

metallicity_values = [
    0.02,
    0.01,
    0.002,
    0.001,
    0.0002,
    0.0001,
    0.00002,
    0.00001,
    0.000002,
    0.000001,
]

# metallicity_values = [0.02, 0.01, 0.002]
metallicity_values = [0.02]

for metallicity in metallicity_values:
    test_pop.set(
        metallicity=metallicity,
        parse_function=parse_function,
        # data_dir=os.path.join(os.environ['BINARYC_DATA_ROOT'], 'RUNTIME_TESTING', 'HIGH_RES_SCHNEIDER_MASS_LOSS_COMBINE', 'Z{}'.format(metallicity)),
        # data_dir=os.path.join(os.environ['BINARYC_DATA_ROOT'], 'GRAV_WAVES', 'HIGH_RES_SCHNEIDER_MASS_LOSS', 'Z{}'.format(metallicity)),
        data_dir=os.path.join(
            "results",
            "RUNTIME_TESTING",
            "HIGH_RES_SCHNEIDER_MASS_LOSS_COMBINE",
            "Z{}".format(metallicity),
        ),
    )

    # create local tmp_dir
    test_pop.set(
        tmp_dir=os.path.join(test_pop.custom_options["data_dir"], "local_tmp_dir")
    )

    if os.path.isdir(test_pop.grid_options["tmp_dir"]):
        shutil.rmtree(test_pop.grid_options["tmp_dir"])
    os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

    # Export settings:
    test_pop.export_all_info(use_datadir=True)

    # Evolve grid
    test_pop.evolve()

    # combine_resultfiles(test_pop.custom_options['data_dir'], 'compact_objects', 'total_compact_objects.dat', check_duplicates_and_all_present=True)
