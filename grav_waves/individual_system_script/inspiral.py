######################################
## Imports and definitions
import numpy as np
import h5py as h5

from astropy.table import Table, Column
import astropy.units as u
from astropy import constants as const
from matplotlib import ticker, cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

import matplotlib.pyplot as plt
import seaborn as sns


# Assumes you provide the initial separation and m1 with units
# I'm writing m2 in terms of Q_BBH = MBH2/MBH1 <--> m2 = m1 * Q_BBH
def GW_inspiral_time_circular(a0, m1, Q_BBH):
    # Equations 5.9 and 5.10 From peters 64
    a0 = a0.to(u.m)
    m1 = m1.to(u.kg)
    m2 = m1 * Q_BBH
    beta_peters = 64 / 5 * const.G**3 / const.c**5 * m1 * m2 * (m1 + m2)
    t_merge = a0**4 / (4 * beta_peters)
    t_merge = t_merge.to(u.yr)
    print("t_merge", t_merge)
    return t_merge


def keppler(Mtot, P):
    P = P.to(u.s)
    Mtot = Mtot.to(u.kg)
    a = (const.G * Mtot) ** (1.0 / 3.0) * (P / (2 * np.pi)) ** (2.0 / 3)
    print(a, "= ", a.to(u.Rsun))
    a = a.to(u.Rsun)
    return a


# GW_inspiral_time_circular(50*u.Rsun, 30*u.Msun, 0.7)

M_1 = 20.4084 * u.Msun
M_2 = 7.88612 * u.Msun
period = 21.441 * u.yr

q = M_2 / M_1
M_tot = M_1 + M_2
separation = keppler(M_tot, period)
print(separation)
# State at DCO formation:
#     M_1: 20.4084 Msun
#     M_2: 7.88612 Msun
#     Period: 21.441 yr
#     Time: 4386820.0 yr
#     Inspiral time: 6660693180.0 yr


# # Assuming a primary AND secondary mass of 100 --> Mtot = 200 (absolute maximum)
# M1     = 100*u.Msun
# qfinal = 1#0.5
# separation = keppler(M1 + qfinal*M1, 1e4*u.day)


# t_insp = GW_inspiral_time_circular(separation, M1, qfinal)
t_insp = GW_inspiral_time_circular(separation, M_1, q)
print("t_insp", np.round(t_insp.to(u.Gyr), 3))  # *1e-9)
quit()

t_hubble = 13.7 * 1e9 * u.yr

print("t_insp/t_hubble", t_insp / t_hubble)
