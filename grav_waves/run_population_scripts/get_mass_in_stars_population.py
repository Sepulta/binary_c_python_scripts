"""
Script to calculate the mass in stars

TODO: fix the issue as to why single stars produce average 0.671192434196028 and binary stars produce 1.0567821287924928 for average mass
"""

from binarycpython.utils.grid import Population


def parse_function(self, output):
    """
    Parse function for binary compact objects
    """

    pass


def generate_average_mass_in_single_systems():
    """
    Function to calculate the average mass in single star systems
    """

    ## Make population and set value
    test_pop = Population()
    test_pop.set(verbosity=1)

    # Set grid variables
    resolution = {"M_1": 100}

    # Count total number of systems to run
    total_no = 1
    for el in resolution:
        total_no *= resolution[el]

    test_pop.add_grid_variable(
        name="lnm1",
        longname="Primary mass",
        valuerange=[0.1, 300],
        samplerfunc="const(math.log(0.1), math.log(300), {})".format(resolution["M_1"]),
        precode="M_1=math.exp(lnm1)",
        probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 301, -1.3, -2.3, -2.3)*M_1",
        dphasevol="dlnm1",
        parameter_name="M_1",
        condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
    )

    test_pop.set(
        num_cores=4,
        multiplicity=1,
        _actually_evolve_system=False,
        do_dry_run=False,
        max_queue_size=total_no,
    )

    # Evolve grid
    analytics_dict = test_pop.evolve()

    return analytics_dict


def generate_average_mass_in_binary_systems():
    """
    Function to calculate the average mass in binary star systems.
    """

    ## Make population and set value
    test_pop = Population()
    test_pop.set(verbosity=1)

    # Set grid variables
    resolution = {"M_1": 100, "q": 100}

    # Count total number of systems to run
    total_no = 1
    for el in resolution:
        total_no *= resolution[el]

    test_pop.add_grid_variable(
        name="lnm1",
        longname="Primary mass",
        valuerange=[0.1, 300],
        samplerfunc="const(math.log(0.1), math.log(300), {})".format(resolution["M_1"]),
        precode="M_1=math.exp(lnm1)",
        probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 301, -1.3, -2.3, -2.3)*M_1",
        dphasevol="dlnm1",
        parameter_name="M_1",
        condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
    )

    test_pop.add_grid_variable(
        name="q",
        longname="Mass ratio",
        valuerange=["0.1/M_1", 1],
        samplerfunc="const(0.1/M_1, 1, {})".format(resolution["q"]),
        probdist="flatsections(q, [{'min': 0.1/M_1, 'max': 1.0, 'height': 1}])",
        dphasevol="dq",
        precode="M_2 = q * M_1",
        parameter_name="M_2",
        condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
    )

    test_pop.set(
        num_cores=4,
        multiplicity=2,
        #
        _actually_evolve_system=False,
        do_dry_run=False,
        max_queue_size=total_no,
    )

    # Evolve grid
    analytics_dict = test_pop.evolve()

    return analytics_dict


if __name__ == "__main__":
    # # Get single system average mass
    # single_analytics_dict = generate_average_mass_in_single_systems()
    # print(single_analytics_dict)

    # Get binary system average mass
    binary_analytics_dict = generate_average_mass_in_binary_systems()
    print(binary_analytics_dict)
