import os
import matplotlib.colors
import matplotlib.gridspec as gridspec

from laptop_datasets import dataset_dict

MAIN_RESULT_DIR = "results"
MAIN_PLOT_DIR = "plots"

key = "HIGH_RES_SCHNEIDER_MASS_LOSS_PPISN_ON"
key = "HIGH_RES_SCHNEIDER_MASS_LOSS_PPISN_OFF"
type_key = "bhbh"

rebuild = dataset_dict[key]["rebuild"]
sim_name = dataset_dict[key]["sim_name"]
result_dir = os.path.join(MAIN_RESULT_DIR, dataset_dict[key]["result_dir"])
plot_dir = os.path.join(MAIN_PLOT_DIR, dataset_dict[key]["plot_dir"])

pdf_list = []

# Load dataset
info_dict = load_info_dict(result_dir, rebuild=rebuild)

plot_any_mass_plot(info_dict, type_key, plot_settings={"show_plot": True})
