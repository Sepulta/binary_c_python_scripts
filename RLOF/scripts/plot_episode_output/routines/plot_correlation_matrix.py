"""
Function to plot the correlation matrix of the episode data

TODO: Go through https://www.geeksforgeeks.org/sort-correlation-matrix-in-python/ to improve the statistics here
TODO: add optionality to include weights to calculate the correlations
TODO: find a method to remove the empty columns and rows from the plot
TODO: Add spearman and pearson correlation

https://github.com/matthijsz/weightedcorr
"""

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

import pandas as pd
import seaborn as sn

from RLOF.scripts.plot_episode_output.routines.weighted_corr_class import WeightedCorr

#

from david_phd_functions.plotting.utils import show_and_save_plot, add_plot_info
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict


def plot_correlation_matrix_for_specific_metallicity(
    metallicity_dataset_dict,
    plot_settings=None,
    disk_only_systems=False,
    use_weighted_correlation=False,
    weighted_correlation_type="pearson",
):
    """
    Function to plot the correlation matrix for a specific metallicity dataset

    We select only the systems of interest: stable mass transfer onto MS stars

    Input:
        metallicity_dataset_dict: dataset dict for specific metallicity. Contains 'metallicity' and 'filename'
    """

    #
    if not plot_settings:
        plot_settings = {}

    # Read out data and create dataframe
    df = pd.read_csv(metallicity_dataset_dict["filename"], sep="\s+", header=0)

    # Filter dataset for the systems of our interest
    df = df[df.initial_stability == 0]  # Stable mass transfer
    df = df[df.initial_stellar_type_accretor == 1]  # Accretion onto main sequence

    # Add columns
    df["time_spent_mt"] = df["final_time"] - df["initial_time"]
    df["ratio_angular_momentum_stream_excess_returned_to_orbit"] = df[
        "total_angular_momentum_stream_excess_returned_to_orbit"
    ] / (df["initial_orbital_angular_momentum"])
    df["ratio_angular_momentum_stellar_overspin_returned_to_orbit"] = df[
        "total_angular_momentum_stellar_overspin_returned_to_orbit"
    ] / (df["initial_orbital_angular_momentum"])
    df["ratio_time_spent_disk_masstransfer"] = (
        df["total_time_spent_disk_masstransfer"] / df["time_spent_mt"]
    )
    df["ratio_mass_transferred_through_disk"] = df[
        "total_mass_transferred_through_disk"
    ] / (df["total_mass_lost"] + df["total_mass_accreted"])

    # get disk mt
    if disk_only_systems:
        df = df[df.total_time_spent_disk_masstransfer > 0]

    # Specify the columns we want to include in the correlation
    exclude_columns = [
        "unique_identifier",
        "initial_starnum_accretor",
        "initial_starnum_donor",
        "initial_stellar_type_accretor",
        "initial_stellar_type_donor",
        "final_starnum_accretor",
        "final_starnum_donor",
        "final_stellar_type_accretor",
        "final_stellar_type_donor",
        "time",
        "episode_number",
        "initial_time",
        "final_time",
        "random_seed",
        "initial_stability",
        # 'probability',
    ]
    # include_columns = [column for column in df.columns if not column in exclude_columns]

    # Drop columns
    dropped_df = df[df.columns.drop(exclude_columns)]

    # create the correlation matrix and filter out stuff that is not relevant
    if not use_weighted_correlation:
        corrMatrix = dropped_df.corr()
    else:
        corrMatrix = WeightedCorr(df=dropped_df, wcol="probability")(
            method=weighted_correlation_type
        )

    filteredCorrMatrix = corrMatrix[
        ((corrMatrix >= 0.5) | (corrMatrix <= -0.5)) & (corrMatrix != 1.000)
    ]

    ####
    # Plot
    fig, ax = plt.subplots(figsize=(50, 50))
    sn.heatmap(
        filteredCorrMatrix,
        annot=True,
        linewidths=0.5,
        ax=ax,
        cmap=cm.get_cmap("viridis"),
    )

    ax.set_title(
        "Correlation matrix for stable MS RLOF episodes" + "(disk accretion only)"
        if disk_only_systems
        else ""
    )

    #######################
    # Save and finish
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


# simulation_dir = '/home/david/projects/binary_c_root/results/RLOF/LOW_RES_RLOF_SANA_2021_ENSEMBLE_fractions'
simulation_dir = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions"
rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(simulation_dir)
dataset_dict = rlof_episode_dataset_dict["datasets"]
show_plot = False

#
plot_correlation_matrix_for_specific_metallicity(
    dataset_dict["0.002"],
    disk_only_systems=True,
    use_weighted_correlation=False,
    plot_settings={"show_plot": show_plot, "output_name": "output/unweighted.pdf"},
)

#
plot_correlation_matrix_for_specific_metallicity(
    dataset_dict["0.002"],
    disk_only_systems=True,
    use_weighted_correlation=True,
    weighted_correlation_type="pearson",
    plot_settings={
        "show_plot": show_plot,
        "output_name": "output/weighted_pearson.pdf",
    },
)

#
plot_correlation_matrix_for_specific_metallicity(
    dataset_dict["0.002"],
    disk_only_systems=True,
    weighted_correlation_type="spearman",
    plot_settings={
        "show_plot": show_plot,
        "output_name": "output/weighted_spearman.pdf",
    },
)
