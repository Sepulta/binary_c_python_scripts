"""
Routine to plot the fraction of initial orbital angular momentum transferred back from overspin excess for stable mt onto ms
"""

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

#
from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict

from grav_waves.settings import convolution_settings

#
from david_phd_functions.plotting.custom_mpl_settings import LINESTYLE_TUPLE

from david_phd_functions.plotting.utils import show_and_save_plot, add_plot_info
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def plot_fraction_overspin_excess_disk_angmom_initial_angmom_stable_mt_onto_ms(
    dataset_dict, add_cdf=False, plot_settings=None
):
    """
    Function to plot the fraction of initial orbital angular momentum transferred back from overspin excess for stable mt onto ms for all metallicities provided in the dataset_dict

    Args:
        dataset_dict: dictionary containing info for multiple metallicities
        plot_settings: dict controlling the plotting
    """

    #
    if not plot_settings:
        plot_settings = {}

    min_ratio = 1
    max_ratio = 0

    ratio_dict = {}

    for metallicity in dataset_dict:
        # Read out data and create dataframe
        df = pd.read_csv(dataset_dict[metallicity]["filename"], sep="\s+", header=0)

        # Query the datafame
        df = df[df.initial_stability == 0]  # Stable mass transfer
        df = df[df.initial_stellar_type_accretor == 1]  # Accretion onto main sequence

        # Multiply the probability by a binary fraction
        df["probability"] *= convolution_settings["binary_fraction"]

        # Multiply the probability by a conversion factor to get the number per solar mass
        df["number_per_solar_mass"] = (
            df["probability"] * convolution_settings["mass_in_stars"]
        )

        # Get total probability of ALL systems that transferring mass onto MS
        total_prob = df["number_per_solar_mass"].sum()

        # get disk mt
        disk_mt = df[df.total_time_spent_disk_masstransfer > 0]

        disk_mt["ratio"] = disk_mt[
            "total_angular_momentum_stellar_overspin_returned_to_orbit"
        ] / (disk_mt["initial_orbital_angular_momentum"])

        # check if we find systems with ratio > 1
        if not disk_mt[disk_mt.ratio > 1].empty:
            print(
                "plot_fraction_excess_disk_angmom_initial_angmom: systems with ratio > 1 in {}".format(
                    dataset_dict[metallicity]["filename"]
                )
            )

        # # remove the ratio > 1:
        # disk_mt = disk_mt[disk_mt.ratio <= 1]

        # normalise:
        disk_mt["number_per_solar_mass"] = disk_mt["number_per_solar_mass"] / total_prob

        ratio_dict[metallicity] = {
            "ratio": disk_mt["ratio"],
            "number_per_solar_mass": disk_mt["number_per_solar_mass"],
        }

        if disk_mt["ratio"].min() < min_ratio:
            min_ratio = disk_mt["ratio"].min()

        if disk_mt["ratio"].max() > max_ratio:
            max_ratio = disk_mt["ratio"].max()

    ######
    # Plot
    fig = plt.figure(figsize=(20, 20))

    #
    gs = fig.add_gridspec(nrows=10, ncols=1)

    #
    if not add_cdf:
        ax = fig.add_subplot(gs[:, :])
    else:
        ax = fig.add_subplot(gs[:7, :])
        ax_cdf = fig.add_subplot(gs[7:, :])

    #
    bins = 10 ** np.linspace(np.max([np.log10(min_ratio), -12]), 0.5, 40)

    for i, metallicity in enumerate(sorted(ratio_dict)):
        # the histogram of the data
        n, bins, _ = ax.hist(
            ratio_dict[metallicity]["ratio"].to_numpy(),
            bins=bins,
            weights=ratio_dict[metallicity]["number_per_solar_mass"].to_numpy(),
            alpha=0.75,
            label="Z = {} ({:.2f})".format(
                metallicity,
                ratio_dict[metallicity]["number_per_solar_mass"].to_numpy().sum(),
            ),
            histtype="step",
            linestyle=LINESTYLE_TUPLE[i][1] if not i == 0 else "-",
            linewidth=4,
        )

        if add_cdf:
            cdf_array = np.cumsum(n) / np.sum(n)
            bincenters = (bins[1:] + bins[:-1]) / 2
            ax_cdf.plot(
                bincenters,
                cdf_array,
                alpha=0.75,
                label="Z = {}".format(metallicity),
                linestyle=LINESTYLE_TUPLE[i][1] if not i == 0 else "-",
                linewidth=4,
            )

    ###
    # make up

    # set legend
    ax.legend(loc=2)

    # # Set labels
    ax.set_ylabel("Normalized number of systems")
    ax.set_xlabel(
        "Fraction of initial orbital angular momentum transferred\nback to orbit due to overspin excess"
    )

    ax.set_title(
        "Fraction of initial orbital angular momentum transferred back to orbit due to overspin excess\nFor systems with initially stable RLOF onto MS. Normalized by all stable MT onto on MS",
        fontsize=24,
    )

    # Set scales
    ax.set_xscale("log")
    ax.set_yscale("log")

    if add_cdf:
        ax_cdf.set_ylim([0, 1.1])
        ax_cdf.set_xscale("log")
        ax_cdf.set_xlabel(ax.get_xlabel())
        ax.set_xlabel("")
        ax_cdf.set_xlim(ax.get_xlim())

    #######################
    # Save and finish
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


# # simulation_dir = '/home/david/projects/binary_c_root/results/RLOF/LOW_RES_RLOF_SANA_2021_ENSEMBLE_fractions'
# simulation_dir = '/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions'
# rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(simulation_dir)
# dataset_dict = rlof_episode_dataset_dict['datasets']
# plot_fraction_overspin_excess_disk_angmom_initial_angmom_stable_mt_onto_ms(dataset_dict, add_cdf=True, plot_settings={'show_plot': True})
