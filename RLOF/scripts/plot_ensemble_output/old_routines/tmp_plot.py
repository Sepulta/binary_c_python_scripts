"""
File to test out some plots
"""


from plot_functions import *
from tmp_plotfuncs import *

import json

ensemble_file = "/home/david/projects/binary_c_root/results/RLOF/RLOF_SANA_2021_ENSEMBLE_fractions/RLOFING_systems_z0.02/ensemble_output.json"

# Load ensemble file
with open(ensemble_file, "r") as f:
    ensemble_data = json.loads(f.read())

# plot_general_probability_fractions_accretors(
#     ensemble_data,
#     ['ensemble', 'RLOF', 'fraction_separation_massratio'],
#     weight_type='mass',
#     bin_type='linear',
#     plot_settings={'show_plot': True}
# )

# plot_general_probability_fractions_accretors(
#     ensemble_data,
#     ['ensemble', 'RLOF', 'fraction_separation_massratio'],
#     weight_type='time',
#     bin_type='linear',
#     plot_settings={'show_plot': True}
# )

# plot_general_probability_fractions_donors(
#     ensemble_data,
#     ['ensemble', 'RLOF', 'fraction_separation_massratio'],
#     weight_type='time',
#     bin_type='linear',
#     plot_settings={'show_plot': True}
# )

# plot_general_probability_fractions_donors(
#     ensemble_data,
#     ['ensemble', 'RLOF', 'fraction_separation_massratio'],
#     weight_type='mass',
#     bin_type='linear',
#     plot_settings={'show_plot': True}
# )


# plot_general_probability_fractions_accretors_and_donors(
#     ensemble_data,
#     ['ensemble', 'RLOF', 'fraction_separation_massratio'],
#     weight_type='time',
#     bin_type='linear',
#     plot_settings={'show_plot': True, 'output_name': 'plots/time.png'}
# )

# plot_general_probability_fractions_accretors_and_donors(
#     ensemble_data,
#     ['ensemble', 'RLOF', 'fraction_separation_massratio'],
#     weight_type='mass',
#     bin_type='linear',
#     plot_settings={'show_plot': True, 'output_name': 'plots/mass.png'}
# )

# plot_log_massfraction_separation_fraction(
#     ensemble_data,
#     ['ensemble', 'RLOF', 'fraction_separation_massratio'],
#     weight_type='time',
#     bin_type='log10',
#     accretor_stellar_type=1,
#     plot_settings={'show_plot': True}
# )

# plot_log_massfraction_separation_fraction(
#     ensemble_data,
#     ['ensemble', 'RLOF', 'fraction_separation_massratio'],
#     weight_type='mass',
#     bin_type='log10',
#     accretor_stellar_type=13,
#     plot_settings={'show_plot': True, 'block': False}
# )

# plot_log_massfraction_separation_fraction(
#     ensemble_data,
#     ['ensemble', 'RLOF', 'fraction_separation_massratio'],
#     weight_type='time',
#     bin_type='log10',
#     accretor_stellar_type=13,
#     plot_settings={'show_plot': True}
# )

# plot_massratio_histogram_with_specific_angmom_increase(
#     ensemble_data,
#     ['ensemble', 'RLOF', 'fraction_separation_massratio'],
#     weight_type='mass',
#     bin_type='linear',
#     accretor_stellar_type=14,
#     fraction_separation_value=0.01,
#     plot_settings={'show_plot': True},
# )

# calculate_all_angmom_fractions(
#     ensemble_data,
#     ['ensemble', 'RLOF', 'fraction_separation_massratio'],
#     weight_type='mass',
#     bin_type='linear',
#     accretor_stellar_type=1,
# )


# plot_specific_angmom_ratio_distribution(
#     ensemble_data,
#     ['ensemble', 'RLOF', 'fraction_separation_massratio'],
#     weight_type='mass',
#     bin_type='linear',
#     accretor_stellar_type=1,
#     plot_settings={'show_plot': True},
# )


plot_specific_angmom_ratio_fraction_accretor_separation_distribution(
    ensemble_data,
    ["ensemble", "RLOF", "fraction_separation_massratio"],
    weight_type="mass",
    bin_type="log10",
    accretor_stellar_type=1,
    plot_settings={"show_plot": True},
)
