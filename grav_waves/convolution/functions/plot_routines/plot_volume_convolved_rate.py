"""
Function  to plot the rates with the volume convolution
"""

import copy
import pickle
import json
import h5py
import numpy as np
import pandas as pd

import astropy.units as u

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors

from grav_waves.convolution.functions.convolution_functions import (
    generate_metallicity_sfr_array,
)
from grav_waves.convolution.functions.redshift_interpolation_functions import (
    test_redshift_to_lookback_time,
    test_lookback_time_to_redshift,
    create_interpolation_datasets,
)
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot

#
# matplotlib.use('Agg')

#
number_per_gigaparsec_cubed_per_year = 1 / u.Gpc**3 / u.yr
sfr_unit = u.Msun / u.Gpc**3 / u.yr


def plot_volume_convolved_rate(
    convolution_dataset, mass_type, time_bin_key=None, plot_settings=None
):
    """
    Plot for the volume convolved rate. CAn only be run if the time type is redshift

    Args:
        - convolution_dataset: dataset containing all the information about the simulation, results and initial settings
        - mass_type: option to plot which mass

        - time_bin_key (Optional): If passed then we plot only the results of that certain time bin
    """

    if not plot_settings:
        plot_settings = {}

    # unpack the convolution dataset
    convolution_settings = convolution_dataset["convolution_configuration"]

    #
    rate_key = "merger_rates"
    long_name_rate_type = "Merger rate"

    #
    rate_result_dict = convolution_dataset["redshift_volume_convolution_results"][
        "merger_rates"
    ]

    # Get correct mass keyname
    if mass_type == "chirpmass":
        mass_bins = convolution_settings["chirpmass_bins"]
        mass_key = "result_array_chirpmass"
        long_name_mass = "Chirp mass"
    elif mass_type == "total_mass":
        mass_bins = convolution_settings["total_mass_bins"]
        mass_key = "result_array_total_mass"
        long_name_mass = "Total mass"
    elif mass_type == "primary_mass":
        mass_bins = convolution_settings["primary_mass_bins"]
        mass_key = "result_array_primary_mass"
        long_name_mass = "Primary mass"
    elif mass_type == "any_mass":
        mass_bins = convolution_settings["any_mass_bins"]
        mass_key = "result_array_any_mass"
        long_name_mass = "Any mass"

    #
    main_results = rate_result_dict[mass_key]

    #
    timebins = convolution_settings["time_bins"]

    # make dummy hist
    _, xedges, yedges = np.histogram2d(
        np.array([]), np.array([]), bins=[timebins, mass_bins]
    )

    # Rotate dataset and set up meshgrid
    main_results = main_results.T
    X, Y = np.meshgrid(xedges, yedges)

    # Set up axes
    fig, axes = plt.subplots(ncols=1, nrows=1, figsize=(20, 20))

    # # Decide which results will be plotted
    plot_results = main_results
    if not time_bin_key is None:
        if time_bin_key in convolution_dataset["time_binned"]["results"].keys():
            # if we do want to plot the time slice we need to select that data again:
            sliced_results = convolution_dataset["time_binned"]["results"][
                time_bin_key
            ][rate_key][mass_key]

            #
            sliced_results = sliced_results.T

            #
            plot_results = sliced_results
        else:
            msg = "Couldnt find the passed time_bin_key in the known keys. Aborting"
            raise KeyError(msg)

    # Plot the results
    pcm = axes.pcolormesh(
        X,
        Y,
        plot_results.value,
        norm=colors.LogNorm(
            vmin=10
            ** np.floor(
                np.log10(main_results.value.max())
                - plot_settings.get("probability_floor", 4)
            ),
            vmax=main_results.value.max(),
        ),
        shading="auto",
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )

    cb = fig.colorbar(pcm, ax=axes, extend="min")

    # TODO: Check the actual units that are contained in the main results
    if convolution_settings[
        "convert_probability_with_binary_fraction_and_mass_in_stars"
    ]:
        cb.ax.set_label("Rate [{}]".format(u.Unit(plot_results.unit)))
    else:
        cb.ax.set_label("probability Rate [{}]".format(u.Unit(plot_results.unit)))

    # Limit range of time that we plot
    axes.set_xlim(
        plot_settings.get("min_plot_time", axes.get_xlim()[0]),
        plot_settings.get("max_plot_time", axes.get_xlim()[1]),
    )

    #
    axes.set_xlabel(r"Source redshift")
    axes.set_ylabel(r"%s [M$_{\odot}$]" % long_name_mass)

    title_text = (
        "Redshift shell volume weighted observable merger rates and source redshifts"
    )
    if not time_bin_key is None:
        bin_edges = convolution_dataset["time_binned"]["bins_edges"][time_bin_key]
        title_text += "\n with the rate resulting from stars formed between redshift {} and {}".format(
            bin_edges[0], bin_edges[1]
        )

    #
    plt.suptitle(title_text, y=0.9, fontsize=20)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
