"""
Function to plot either the mass weighted or time weighted distribution of racc and qacc for stable mass transfer onto MS via disks.

The results are normalised by the total amount of stable MT onto MS stars

TODO: make this cleaner and add disk information here
"""

import os
import json
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

from ensemble_functions.ensemble_functions import (
    get_recursive_sum,
    merge_all_subdicts,
    return_keys_bins_and_bincenters,
    flatten_data_ensemble2d,
    query_dict,
    recursively_merge_all_subdicts,
    merge_recursive_until_stop,
    get_recursive_sum_for_subkeys_1d,
)
from RLOF.scripts.plot_ensemble_output.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)

from scipy.ndimage import gaussian_filter
from grav_waves.settings import convolution_settings


def plot_time_weighted_synchronicity_factor_ensemble(
    dataset_dict,
    testing,
    weight_type,
    verbose=0,
    global_query_dict=None,
    plot_settings={},
):
    """
    Function to plot the mass weighted and time weighted distribution of racc and qacc for stable mass transfer onto MS via disks.

    The results are normalised by the total amount of stable MT onto MS stars
    """

    DIFF_MAX_THRESHOLD = 3
    contour_levels = [1e-5, 1e-4, 0.5 * 1e-3]

    #########
    # Check and give some info

    #
    if len(list(dataset_dict.keys())) > 3:
        raise ValueError("Number of metallicities cannot exceed 3")

    #
    if not weight_type in ["mass", "time"]:
        raise ValueError("Chosen weight type not available")

    if verbose:
        print(
            "Running plot_massratio_separation_histogram_one_quantity_three_metallicities\n\toutput: {}".format(
                os.path.abspath(plot_settings["output_name"])
            )
        )

        if global_query_dict:
            print(
                "\twith global query: {}".format(global_query_dict["global_query_name"])
            )
            print(
                "\t\tqueries: {}".format(
                    ", ".join(
                        [
                            "{} between {}".format(
                                query["parameter_key"], query["value_range"]
                            )
                            for query in global_query_dict["query_list"]
                        ]
                    )
                )
            )

    probability_to_yield_conversion_factor = (
        convolution_settings["binary_fraction"]
        * convolution_settings["average_mass_system"]
    )

    # Loop over keys first time to get the data and
    subensemble_dict = {}
    for key in dataset_dict.keys():
        if verbose:
            print("\tZ = {}".format(key))
        current_dataset = dataset_dict[key]

        # load ensemble data
        # if verbose: print("\t\tLoading data")
        ensemble_data = get_ensemble_data(
            current_dataset,
            testing,
            probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
        )["RLOF"]["fraction_separation_massratio"][weight_type]

        #######
        # Querying
        # TODO: fix the query here

        # # Run base-query on the data
        # # Do base query
        # if not base_querylist is None:
        #     ensemble_data = handle_querylist(ensemble_data, base_querylist, 'base-query')

        # # Run query on the data
        # if not querylist is None:
        #     queried_data = ensemble_data
        #     queried_data = handle_querylist(queried_data, querylist, 'query')

        # Query all the
        stable_subensemble = query_dict(ensemble_data, "stable", [-0.5, 0.5])
        ms_stable_subensemble = query_dict(stable_subensemble, "st_acc", [0.5, 1.5])
        # ms_disk_mass_stable_ensemble = query_dict(ms_stable_subensemble, 'disk', [0.5, 1.5])

        # Merge all the ensembles
        merged_disk_ms_stable_subensemble = merge_recursive_until_stop(
            ms_stable_subensemble, "frac_sync_donor"
        )

        # Calculate total sum of the probabilityies
        result_dict = get_recursive_sum_for_subkeys_1d(
            merged_disk_ms_stable_subensemble, "frac_sync_donor"
        )

        # Store in results
        subensemble_dict[key] = result_dict

    ########
    # Do plotting
    fig = plt.figure(figsize=(20, 10))

    #
    gs = fig.add_gridspec(nrows=1, ncols=10)

    #
    ax = fig.add_subplot(gs[:, :-2])
    title_axis = ax
    labelpad = 0

    for key in subensemble_dict.keys():
        result_dict = subensemble_dict[key]

        #
        keys, vals = [], []
        for key in result_dict["frac_sync_donor"]:
            val = result_dict["frac_sync_donor"][key]
            keys.append(float(key))
            vals.append(val)

        keys = np.array(keys)
        vals = np.array(vals)

        ax.plot(keys, vals, "o")

    #####
    # Make up
    ax.set_yscale("log")

    # # Axes labels
    # ax.set_xlabel(r"{}".format(parameter_name_dict[first_parameter]))
    # ax.set_ylabel(r"{}".format(parameter_name_dict[second_parameter]))

    # if not querylist is None:
    #     ax_query.set_xlabel(r"{}".format(parameter_name_dict[first_parameter]))
    #     ax_query.set_yticklabels([])

    # # Set title
    # title_axis.set_title("Probability distribution of {} and {}{}{}{}".format(
    #     parameter_name_dict[first_parameter],
    #     parameter_name_dict[second_parameter],

    #     "\n" if (base_querylist is not None) or (querylist is not None) else '',
    #     " with base query {}".format(return_string_querylist(base_querylist)) if base_querylist is not None else '',
    #     " with query {}".format(return_string_querylist(querylist)) if querylist is not None else ''
    #     ), fontsize=24,
    #     pad=labelpad
    # )

    # Round off
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    ########
    # Set up data
    simulation_dir = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions"
    simulation_dir = (
        "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_SANA/"
    )

    ensemble_dataset_dict = generate_ensemble_dataset_dict(simulation_dir)
    dataset_dict = ensemble_dataset_dict["datasets"]

    ########
    # Set filename
    target_dir = "/home/david/Dropbox/Academic/PHD/papers/paper_disks/paper_tex/figures/episode_plots"
    target_dir = "plots"

    #
    basename = "fraction_stellar_overspin_excess.pdf"

    #
    testing = True
    weight_type = "mass"

    #
    plot_time_weighted_synchronicity_factor_ensemble(
        dataset_dict,
        testing=testing,
        weight_type=weight_type,
        global_query_dict={},
        # add_cdf=True,
        plot_settings={
            "show_plot": True,
            "output_name": os.path.join(target_dir, basename),
        },
    )
