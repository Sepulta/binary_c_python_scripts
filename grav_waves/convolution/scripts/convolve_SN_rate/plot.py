"""
plot sn rate output

TODO: add DCO rate data as comparison
"""

import h5py
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def get_redshift_and_values(rate_dict):
    """
    Function to turn dict into redshift and values
    """

    rate_dict_keys = np.array(sorted(list(rate_dict.keys()), key=float))
    rate_dict_redshift = [float(el) for el in rate_dict_keys]
    rate_dict_values = [rate_dict[el] for el in rate_dict_keys]

    return rate_dict_redshift, rate_dict_values


SN_type_dict = {
    0: "None",
    1: "IA_He",
    2: "IA_ELD",
    3: "IA_CHAND",
    4: "AIC",
    5: "ECAP",
    6: "IA_He_Coal",
    7: "IA_CHAND_Coal",
    8: "NS_NS",
    9: "GRB_COLLAPSAR",
    10: "HeStarIa",
    11: "IBC",
    12: "II",
    13: "IIa",
    14: "WDKICK",
    15: "TZ",
    16: "AIC_BH",
    17: "BH_BH",
    18: "BH_NS",
    19: "IA_Hybrid_HeCOWD",
    20: "IA_Hybrid_HeCOWD_subluminous",
    21: "PPISN",
    22: "PISN",
    23: "PHDIS",
}
inverse_SN_type_dict = {value: key for key, value in SN_type_dict.items()}


###########
# DCO
dco_convolution_filename = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/NEW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/dco_convolution_results.h5"

# Get dataframe
dco_convolution_dataframe = pd.read_hdf(
    dco_convolution_filename, "/data/combined_dataframes"
)

# Get BHBH DCO
BHBH_indices = dco_convolution_dataframe.eval(
    "stellar_type_1 == 14 & stellar_type_2 == 14"
).to_numpy()

total_BHBH_rate_dict = {}

dco_convolution_datafile = h5py.File(dco_convolution_filename)

redshifts = np.array(
    sorted(list(dco_convolution_datafile["data/formation_rate"].keys()), key=float)
)

for redshift_key in redshifts:
    redshift = float(redshift_key)

    # Readout rate data
    rate_data = dco_convolution_datafile["data/formation_rate/{}".format(redshift_key)][
        ()
    ]

    # get BHBH rate
    BHBH_rate = rate_data[BHBH_indices]
    total_BHBH_rate_dict[redshift] = np.sum(BHBH_rate)

# get BHBH rate data in correct shape
total_BHBH_rate_dict_redshift, total_BHBH_rate_dict_values = get_redshift_and_values(
    total_BHBH_rate_dict
)

###########
# SN
sn_convolution_filename = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/NEW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/sn_convolution_results/sn_convolution_results.h5"

# Get dataframe
sn_convolution_dataframe = pd.read_hdf(
    sn_convolution_filename, "/data/combined_dataframes"
)

# get IBC SN
IBC_indices = sn_convolution_dataframe.eval(
    "post_SN_SN_type == {}".format(inverse_SN_type_dict["IBC"])
).to_numpy()
# print(np.count_nonzero(II_indices))

# get II SN
II_indices = sn_convolution_dataframe.eval(
    "post_SN_SN_type == {}".format(inverse_SN_type_dict["II"])
).to_numpy()
# print(np.count_nonzero(II_indices))

# get IIa SN
IIa_indices = sn_convolution_dataframe.eval(
    "post_SN_SN_type == {}".format(inverse_SN_type_dict["IIa"])
).to_numpy()
# print(np.count_nonzero(II_indices))

# get PPISN SN
PPISN_indices = sn_convolution_dataframe.eval(
    "post_SN_SN_type == {}".format(inverse_SN_type_dict["PPISN"])
).to_numpy()
# print(np.count_nonzero(PPISN_indices))

# get PISN SN
PISN_indices = sn_convolution_dataframe.eval(
    "post_SN_SN_type == {}".format(inverse_SN_type_dict["PISN"])
).to_numpy()
# print(np.count_nonzero(pisn_indices))

# get PHDIS SN
PHDIS_indices = sn_convolution_dataframe.eval(
    "post_SN_SN_type == {}".format(inverse_SN_type_dict["PHDIS"])
).to_numpy()
# print(np.count_nonzero(PHDIS_indices))

total_IBC_rate_dict = {}
total_II_rate_dict = {}
total_IIa_rate_dict = {}
total_PPISN_rate_dict = {}
total_PISN_rate_dict = {}
total_PHDIS_rate_dict = {}

# Open HDF5 file again
sn_convolution_datafile = h5py.File(sn_convolution_filename)


redshifts = np.array(
    sorted(list(sn_convolution_datafile["data/formation_rate"].keys()), key=float)
)

for redshift_key in redshifts:
    redshift = float(redshift_key)

    # Readout rate data
    rate_data = sn_convolution_datafile["data/formation_rate/{}".format(redshift_key)][
        ()
    ]

    # get IBC rate
    IBC_rate = rate_data[IBC_indices]
    total_IBC_rate_dict[redshift] = np.sum(IBC_rate)

    # get II rate
    II_rate = rate_data[II_indices]
    total_II_rate_dict[redshift] = np.sum(II_rate)

    # get IIa rate
    IIa_rate = rate_data[IIa_indices]
    total_IIa_rate_dict[redshift] = np.sum(IIa_rate)

    # get PPISN rate
    PPISN_rate = rate_data[PPISN_indices]
    total_PPISN_rate_dict[redshift] = np.sum(PPISN_rate)

    # get PISN rate
    PISN_rate = rate_data[PISN_indices]
    total_PISN_rate_dict[redshift] = np.sum(PISN_rate)

    # get PHDIS rate
    PHDIS_rate = rate_data[PHDIS_indices]
    total_PHDIS_rate_dict[redshift] = np.sum(PHDIS_rate)


# get IBC rate data in correct shape
total_IBC_rate_dict_redshift, total_IBC_rate_dict_values = get_redshift_and_values(
    total_IBC_rate_dict
)

# get II rate data in correct shape
total_II_rate_dict_redshift, total_II_rate_dict_values = get_redshift_and_values(
    total_II_rate_dict
)

# get IIa rate data in correct shape
total_IIa_rate_dict_redshift, total_IIa_rate_dict_values = get_redshift_and_values(
    total_IIa_rate_dict
)

# get PPISN rate data in correct shape
total_PPISN_rate_dict_redshift, total_PPISN_rate_dict_values = get_redshift_and_values(
    total_PPISN_rate_dict
)

# get PPISN rate data in correct shape
total_PISN_rate_dict_redshift, total_PISN_rate_dict_values = get_redshift_and_values(
    total_PISN_rate_dict
)

# get PHDIS rate data in correct shape
total_PHDIS_rate_dict_redshift, total_PHDIS_rate_dict_values = get_redshift_and_values(
    total_PHDIS_rate_dict
)


#
plt.plot(
    total_BHBH_rate_dict_redshift,
    total_BHBH_rate_dict_values,
    label="BHBH",
    linestyle="--",
)
plt.plot(total_IBC_rate_dict_redshift, total_IBC_rate_dict_values, label="IBC")
plt.plot(total_II_rate_dict_redshift, total_II_rate_dict_values, label="II")
plt.plot(total_IIa_rate_dict_redshift, total_IIa_rate_dict_values, label="IIa")
plt.plot(total_PPISN_rate_dict_redshift, total_PPISN_rate_dict_values, label="PPISN")
plt.plot(total_PISN_rate_dict_redshift, total_PISN_rate_dict_values, label="PISN")
plt.plot(total_PHDIS_rate_dict_redshift, total_PHDIS_rate_dict_values, label="PHDIS")

plt.legend()
plt.yscale("log")
# plt.ylim([1, 2*1e3])
plt.xlim([-0.4, 8])
plt.show()
