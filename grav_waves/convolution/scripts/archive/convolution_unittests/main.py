"""
Script to test the convolution. We need to see if the rate that is predicted at z=0 aligns with what we expect.

TODO: set up the convolution in a way that we control
TODO: implement the testing method.
"""

import os
import json
import shutil
import numpy as np

import astropy.units as u

#
from grav_waves.settings import (
    config_dict_cosmology,
    convolution_settings,
)
from grav_waves.gw_analysis.functions.cosmology_functions import (
    lookback_time_to_redshift,
    redshift_to_lookback_time,
)
from grav_waves.gw_analysis.functions.functions import make_dataset_dict
from grav_waves.convolution.functions.convolution_functions import (
    new_convolution_main as convolution_main,
)


#
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)

###
# Configure the data
output_dir = os.path.join(this_file_dir, "output/TESTING_CONVOLUTION_OUTPUT_DIR")
if os.path.isdir(output_dir):
    shutil.rmtree(output_dir)
os.makedirs(output_dir, exist_ok=True)

###
# Configure the cosmology dict:
config_dict_cosmology["star_formation_rate_function"] = "dummy"
config_dict_cosmology["metallicity_distribution_function"] = "dummy"

# all these below should be updated if something with the time changes
stepsize = 1
convolution_settings["time_type"] = "redshift"

convolution_settings["stepsize"] = stepsize
convolution_settings["time_bins"] = np.arange(
    0.0001 - 0.5 * stepsize,
    (convolution_settings["max_interpolation_redshift"] - 1) + 0.5 * stepsize,
    stepsize,
)
convolution_settings["time_centers"] = (
    convolution_settings["time_bins"][1:] + convolution_settings["time_bins"][:-1]
) / 2
convolution_settings["min_loop_time"] = 1e-6
convolution_settings["max_loop_time"] = (
    convolution_settings["max_interpolation_redshift"] - 1
)
convolution_settings["add_detection_probability"] = False
convolution_settings[
    "convert_probability_with_binary_fraction_and_mass_in_stars"
] = False
convolution_settings["testing_mode"] = True
config_dict_cosmology["testing_mode"] = True
