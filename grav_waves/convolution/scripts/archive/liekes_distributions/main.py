"""
Implementation / copy of compas metallicity distribution function
"""

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

from grav_waves.convolution.functions.compas_metallicity_distribution import (
    find_metallicity_distribution,
)
from grav_waves.settings import convolution_settings, config_dict_cosmology
from grav_waves.gw_analysis.functions.cosmology_functions import (
    starformation_rate,
    metallicity_distribution,
    normalize_metallicities_distribution,
)

#
redshifts = np.arange(1e-5, 10, 0.01)
sampled_metallicities = 10 ** np.linspace(-4, -1.6, 12)

log10_metallicities = np.log10(sampled_metallicities)
log_metallicities = np.log(sampled_metallicities)

max_log_z = log_metallicities.max()
min_log_z = log_metallicities.min()

# Get sfr value
sfr_values = starformation_rate(
    redshifts, cosmology_configuration=config_dict_cosmology, verbosity=1
)

# distribution = "neijssel19"
distribution = "vanSon21"

if distribution == "neijssel19":
    metallicity_distribution_settings = {"mu0": 0.035, "muz": -0.23, "sigma_0": 0.39}

    # Calculate the metallicity distribution
    dPdlogZ, metallicities, p_draw_metallicity = find_metallicity_distribution(
        redshifts, min_log_z, max_log_z, **metallicity_distribution_settings
    )
elif distribution == "vanSon21":
    metallicity_distribution_settings = {
        "mu0": 0.025,
        "muz": -0.048,
        "sigma_0": 1.125,
        "sigma_z": 0.048,
        "alpha": 0,
    }

    #
    dPdlogZ, metallicities, p_draw_metallicity = find_metallicity_distribution(
        redshifts, min_log_z, max_log_z, **metallicity_distribution_settings
    )
else:
    raise ValueError("Unknown setting")

from general_plot_function_metallicity_sfr_distribution import general_plot_function

general_plot_function(
    output_pdf_dir="output/",
    combined_pdf_filename="combined.pdf",
    redshifts=redshifts,
    sfr_values=sfr_values,
    dPdlogZ=dPdlogZ,
    metallicities=metallicities,
    sampled_metallicities=sampled_metallicities,
    p_draw_metallicity=p_draw_metallicity,
    distribution=distribution,
)
