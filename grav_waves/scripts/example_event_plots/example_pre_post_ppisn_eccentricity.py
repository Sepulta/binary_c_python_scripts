"""
Plot routine to plot the pre and post eccentricity of merging systems that have undergone a PPISN supernova
"""
import numpy as np
import pandas as pd
from grav_waves.functions.events import merge_with_event_dataframe, print_rows_and_cols


import matplotlib.pyplot as plt
from matplotlib import colors
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

import matplotlib
from plot_utils import get_vmin


def example_pre_post_ppisn_eccentricity(
    combined_dataframes, rate_array, sn_event_dataframe
):
    """
    Example function to combine the DCO data with supernova events and
    """

    #############
    # Handle the data
    # Join the dataframes
    merged_sn_df = merge_with_event_dataframe(
        main_dataframe=combined_dataframes, event_dataframe=sn_event_dataframe
    )
    print_rows_and_cols(merged_sn_df, "merged_sn_df")

    # Set the rate values
    merged_sn_df["rates"] = rate_array[merged_sn_df["initial_indices"].to_numpy()]

    # Query
    ppisn_SN = merged_sn_df.query("SN_type == 21")

    ###########
    # Handle the plot
    # Set bins
    numbins = 20
    x_bins = np.linspace(0, 1, numbins)
    y_bins = np.linspace(0, 1, numbins)

    #
    hist, _, _ = np.histogram2d(
        ppisn_SN["pre_SN_ecc"],
        ppisn_SN["post_SN_ecc"],
        bins=[x_bins, y_bins],
        weights=ppisn_SN["rates"],
    )

    # Set norm
    norm = colors.LogNorm(
        vmin=get_vmin(hist, 1e-6),
        vmax=np.max(hist),
    )

    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=5)
    rates_axis = fig.add_subplot(gs[:, :-1])
    ax_cb = fig.add_subplot(gs[:, -1:])

    plot_settings = {}
    # make bincenters
    x_bincenters = (x_bins[1:] + x_bins[:-1]) / 2
    y_bincenters = (y_bins[1:] + y_bins[:-1]) / 2

    X, Y = np.meshgrid(x_bincenters, y_bincenters)

    _ = rates_axis.pcolormesh(
        X,
        Y,
        hist.T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # rates_axis.set_yscale('log')
    # rates_axis.set_xscale('log')
    rates_axis.set_xlabel("Pre-SN eccentricity")
    rates_axis.set_ylabel("Post-SN eccentricity")

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        norm=norm,
    )
    cbar.ax.set_ylabel(r"{} Rate [{}]".format(1, 2))

    plt.show()


if __name__ == "__main__":

    ##############
    # Get the dataframes
    # file = "/home/david/data_projects/binary_c_data/GRAV_WAVES/server_results/EVENTS_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5"
    file = "/home/david/data_projects/binary_c_data/GRAV_WAVES/server_results/EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5"

    # Set up dataframe
    combined_dataframes = pd.read_hdf(file, key="data/combined_dataframes")

    # Calculate rate array
    rate_array = get_rate_data(
        file,
        rate_type="merger_rate",
        redshift_value=0,
        mask=np.ones(len(combined_dataframes.index), dtype=bool),
    )[0]

    # # Filter
    # combined_dataframes = combined_dataframes[combined_dataframes.mass_1 < 8]

    # Select dataframe of supernovae
    sn_event_dataframe = pd.read_hdf(file, key="data/events/SN_BINARY")
