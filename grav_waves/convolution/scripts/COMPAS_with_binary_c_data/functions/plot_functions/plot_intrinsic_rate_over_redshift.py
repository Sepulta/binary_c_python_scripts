"""
Function to plot the total intrinsic rate over redshift
"""

import h5py

import matplotlib.pyplot as plt

import numpy as np

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def plot_intrinsic_rate_over_redshift(target_filename_convolution, plot_settings={}):
    """
    Function to plot the intrinsic rate over redshift
    """

    # Open file
    target_h5file = h5py.File(target_filename_convolution, "r")

    intrinsic_merger_rate_per_system_per_redshift = target_h5file[
        "convolution_data/merger_rate"
    ][()]
    redshifts = target_h5file["convolution_settings/redshift"][()]

    # Now use numpy to just collapse all values
    rate = np.sum(intrinsic_merger_rate_per_system_per_redshift, axis=0)
    fig, axes = plt.subplots(1, 1)

    axes.plot(redshifts, rate)
    axes.set_xlabel("redshift")
    axes.set_ylabel("rate Intrinsic [dN/dGpc3/dYr]")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
