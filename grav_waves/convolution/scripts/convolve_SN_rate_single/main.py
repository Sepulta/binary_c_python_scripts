"""
Main function to handle convolution of single star systems
"""

import json
import os
import shutil
import logging
import time
import h5py
import pandas as pd
from grav_waves.convolution.functions.convolution_add_population_settings_to_hdf5file import (
    add_population_settings_to_hdf5file,
)

from grav_waves.gw_analysis.functions.functions import make_dataset_dict
from grav_waves.convolution.functions.convolution_general_functions import (
    JsonCustomEncoder,
)


from grav_waves.convolution.scripts.convolve_SN_rate_single.functions import (
    add_number_per_formed_solar_mass,
    handle_columns,
    return_single_sn_dataframe,
    combine_single_sn_dataframes,
    create_combined_single_sn_dataset,
    filter_combined_single_sn_dataset_function,
)

from grav_waves.settings import convolution_settings, config_dict_cosmology


if __name__ == "__main__":

    simname_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/NEW_LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED"
    convolution_settings["logger"].setLevel(logging.DEBUG)

    # Get the correct directories
    dataset_dict_single_sn = make_dataset_dict(simname_dir)

    create_combined_single_sn_dataset(
        dataset_dict=dataset_dict_single_sn,
        convolution_configuration=convolution_settings,
        cosmology_configuration=config_dict_cosmology,
        output_filename="output/test.hdf5",
    )

    filter_combined_single_sn_dataset_function(
        input_hdf5file="output/test.hdf5", output_hdf5file="output/filtered_test.hdf5"
    )

    # combine_single_sn_dataframes(simname_dir, convolution_settings)

    # filename = "/home/david/projects/binary_c_root/results/GRAV_WAVES/NEW_LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/single_population_results/Z0.00012589254117941674/total_sn_events.dat"

    # df = return_single_sn_dataframe(filename, convolution_settings)
