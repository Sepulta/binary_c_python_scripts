"""
Some tests to readout the hdf5 files
"""

import pandas as pd
import h5py

filename = "output/convolution_without_detector_probability_results/convolution_without_detection_probability.h5"
print(filename)

hdf5_file = h5py.File(filename, "r")
rate_data = hdf5_file["data"]

#
merger_rate_data = rate_data["merger_rate_data"]
# print(merger_rate_data.keys())

df1 = pd.read_hdf(filename, "/data/merger_rate_data/0.0501")
df2 = pd.read_hdf(filename, "/data/merger_rate_data/0.10010000000000001")
# print(df1)
# print(df2)

combined_df = pd.DataFrame()

combined_df = pd.concat([combined_df, df1])
combined_df = combined_df.sort_values("local_index")

combined_df = pd.concat([combined_df, df2])
combined_df = combined_df.sort_values("local_index")

# The aggregate dict determines how we aggregate the values. For all columns we just take the value of the first row of the group, because the values should all be the same.
# For the merger rate however we sum the values
aggregate_dict = {el: "first" for el in combined_df.columns}
aggregate_dict["merger_rates"] = "sum"

combined_df = (
    combined_df.groupby("local_index", as_index=False)
    .aggregate(aggregate_dict)
    .reindex(columns=combined_df.columns)
)
print(combined_df[len(combined_df) - 1 :])
