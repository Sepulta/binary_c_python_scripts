"""
Main function to start running the convolution through COMPAS.

The functions are based on the tutorials from compas
"""

import os

from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.convert_binary_c_structure_to_compas_structure import (
    convert_binaryc_data_to_compas_data,
)
from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.run_convolution_compas import (
    run_convolution_compas,
)
from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.run_convolution_fastintegrator import (
    run_convolution_fastintegrator,
)

## Setting
sim_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED"
dco_type = "combined_dco"

# Targets
target_dir_restructure = os.path.join(sim_dir, "population_results_compas_structure")
target_filename_restructure = os.path.join(
    target_dir_restructure, "{}_compas_structure.hdf5".format(dco_type)
)

target_dir_convolution = os.path.join(sim_dir, "convolution_results_compas_structure")
target_filename_convolution = os.path.join(
    target_dir_convolution,
    "convolved_{}_compas_structure_coens_method.hdf5".format(dco_type),
)

print(target_filename_convolution)
quit()
# Create directories
os.makedirs(target_dir_restructure, exist_ok=True)
os.makedirs(target_dir_convolution, exist_ok=True)

#
convert_data_to_compas_structure = False
run_compas_convolution = True
run_fast_cosmic_integration = False

# Take original data and write to new structure
if convert_data_to_compas_structure:
    convert_binaryc_data_to_compas_data(target_filename=target_filename_restructure)

# Take the new structure dataset and run the convolution
if run_compas_convolution:
    run_convolution_compas(
        source_filename=target_filename_restructure,
        target_filename=target_filename_convolution,
    )

if run_fast_cosmic_integration:
    run_convolution_fastintegrator(
        source_filename=target_filename_restructure,
        target_filename=target_filename_convolution,
    )
