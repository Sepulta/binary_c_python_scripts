"""
Utility functions for the plotting
"""

import numpy as np
import h5py


from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    extract_columns_from_querylist,
    quantity_name_dict,
    quantity_unit_dict,
    rate_type_name_dict,
    rate_density_units,
    add_columns_to_df,
    dco_type_query,
    readout_rate_array_specific_keys,
    create_centers_from_bins,
    get_closest_redshift_key,
    get_histogram_data,
    get_KDE_data,
    return_1d_plot_canvas,
    plot_1d_results,
    hatch_list,
    linestyle_list,
)


def likelihood(f_mod, f_obs, f_obs_error):
    """
    Function to return the likelihood that this model at the this point fits the observation
    """

    L = (1 / (f_obs_error * np.sqrt(2 * np.pi))) * np.exp(
        -((f_mod - f_obs) ** 2) / 2 * f_obs_error**2
    )

    return L


def handle_comparison_to_observations(
    indices, centers, rates, data_powerlaw_peak_primary_mass
):
    """
    Function to handle the comparison to the observations
    """

    # Get matching values observations
    observation_bin_indices = np.digitize(
        centers,
        bins=data_powerlaw_peak_primary_mass["mass_1_centers"],
        right=False,
    )
    observation_values = data_powerlaw_peak_primary_mass["mass_1_mean_rate"][
        observation_bin_indices
    ]

    # Create the comparison
    rates_comparison_values = likelihood(
        f_mod=rates,
        f_obs=observation_values,
        f_obs_error=data_powerlaw_peak_primary_mass["observation_1sigma"][0][
            observation_bin_indices
        ],
    )

    return rates_comparison_values


def get_median_percentiles(value_array):
    """
    Function to get the median and the percentiles from the data
    """

    result_dict = {}

    result_dict["median"] = np.percentile(value_array, [50], axis=0)

    result_dict["90%_CI"] = np.percentile(value_array, [5, 95], axis=0)

    # result_dict["1_sigma"] = np.percentile(value_array, [15.89, 84.1], axis=0)

    # result_dict["2_sigma"] = np.percentile(value_array, [2.27, 97.725], axis=0)

    return result_dict


def handle_columns(combined_df, quantity, querylist, extra_columns=[]):
    """
    Function to handle adding the columns
    """

    used_columns = (
        [quantity] + extract_columns_from_querylist(querylist) + extra_columns
    )
    columns_to_add = [
        column for column in used_columns if not column in combined_df.columns
    ]
    combined_df = add_columns_to_df(combined_df, columns_to_add)

    return combined_df


def get_mask(combined_df, dco_type, general_query, query):
    """
    function to make the mask
    """

    dco_mask = combined_df.eval(dco_type_query(dco_type)).to_numpy()
    general_query_mask = (
        combined_df.eval(general_query)
        if general_query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    query_mask = (
        combined_df.eval(query)
        if query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    mask = dco_mask * general_query_mask * query_mask

    return mask


def get_rate_data(filename, rate_type, redshift_value, mask):
    """
    Function to get rate data
    """

    datafile = h5py.File(filename)
    all_redshift_keys = sorted(list(datafile["data/{}".format(rate_type)].keys()))
    closest_redshift_key = get_closest_redshift_key(redshift_value, all_redshift_keys)
    rate_array, _ = readout_rate_array_specific_keys(
        datafile=datafile,
        rate_key=rate_type,
        mask=mask,
        redshift_keys=[closest_redshift_key],
    )
    datafile.close()

    return rate_array
