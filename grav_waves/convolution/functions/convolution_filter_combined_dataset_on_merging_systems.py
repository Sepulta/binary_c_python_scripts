"""
Function to filter the combined_dataset on merging systems
"""

import os
import shutil

import h5py
import pandas as pd

from grav_waves.settings import AGE_UNIVERSE_IN_YEAR


def filter_combined_dataset_on_merging_systems(input_hdf5file, output_hdf5file):
    """
    Function to filter the combined_dataset on merging systems

    This function is intended to be used with binary_c gravitational wave datasets

    Steps:
    - Copies the input_hdf5file to the output_hdf5file
    - deletes the combined_dataset in the output_hdf5file
    - loads the combined_dataset of the input_hdf5file
    - filters the combined_dataset
    - writes the combined_dataset_merging_systems to output_hdf5file
    """

    # Copy input file to output file
    if os.path.isfile(output_hdf5file):
        os.remove(output_hdf5file)
    shutil.copy(input_hdf5file, output_hdf5file)

    # Delete combined dataframe in data/combined_dataframes
    with h5py.File(output_hdf5file, "a") as f:
        del f["/data/combined_dataframes"]

    # Read out the combined_df from the input_hdf5file
    combined_df = pd.read_hdf(input_hdf5file, "/data/combined_dataframes")

    # Filter the dataset
    combined_dataset_merging_systems = combined_df[
        combined_df.merger_time_values_in_years < AGE_UNIVERSE_IN_YEAR
    ]

    # Write the dataset to the output_filename
    combined_dataset_merging_systems.to_hdf(
        output_hdf5file, "/data/combined_dataframes"
    )
