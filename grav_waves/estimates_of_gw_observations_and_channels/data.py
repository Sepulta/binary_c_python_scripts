"""
File holding the data for the estimates of intrinsic merger rate density from observations and predictions of the contributions of individual channels of binary black hole mergers

The collection of data comes from the supplementary materials form the

https://github.com/FloorBroekgaarden/Rates_of_Compact_Object_Coalescence

well floor did that already so lets not re-do that work
"""


observation_estimates_bbh_mergers = {
    "abbott_2016": {
        "url": "https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.116.061102",
        "mean": 200,
        "upper": 400,
        "lower": 2,
        "comment": "page 8",
        "date": "2016-02-12",
        "type": "mean",
    }
}


# combine
data_dict = {
    "observation_estimates_bbh_mergers": observation_estimates_bbh_mergers,
}
