"""
Routine to plot the disk mt time categories
"""

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

#
from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict

from grav_waves.settings import convolution_settings

#
from david_phd_functions.plotting.custom_mpl_settings import LINESTYLE_TUPLE

from david_phd_functions.plotting.utils import show_and_save_plot, add_plot_info
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def plot_disk_MT_mass_categories(dataset_dict, add_cdf=False, plot_settings=None):
    """
    Function to plot the fraction of mass transfered through disk when the MT is initially stable for all metallicities provided in the dataset_dict

    Args:
        dataset_dict: dictionary containing info for multiple metallicities
        plot_settings: dict controlling the plotting
    """

    #
    if not plot_settings:
        plot_settings = {}

    min_ratio = 1
    max_ratio = 0

    ratio_dict = {}

    for metallicity in dataset_dict:
        # Read out data and create dataframe
        df = pd.read_csv(dataset_dict[metallicity]["filename"], sep="\s+", header=0)

        # Query the datafame
        df = df[df.initial_stability == 0]  # Stable mass transfer
        df = df[df.initial_stellar_type_accretor == 1]  # Accretion onto main sequence

        # Multiply the probability by a binary fraction
        df["probability"] *= convolution_settings["binary_fraction"]

        # Multiply the probability by a conversion factor to get the number per solar mass
        df["number_per_solar_mass"] = (
            df["probability"] * convolution_settings["mass_in_stars"]
        )

        # Get total probability of ALL systems that transferring mass onto MS
        total_prob = df["number_per_solar_mass"].sum()

        # get disk mt
        disk_mt = df[df.total_mass_transferred_through_disk > 0]

        # normalise:
        disk_mt["number_per_solar_mass"] = disk_mt["number_per_solar_mass"] / total_prob

        # Calculate fractions of total time spent in each category
        disk_mt["fraction_mass_transferred_through_disk_category_0"] = (
            disk_mt["total_mass_transferred_through_disk_MT_category_0"]
            / disk_mt["total_mass_transferred_through_disk"]
        )
        disk_mt["fraction_mass_transferred_through_disk_category_1"] = (
            disk_mt["total_mass_transferred_through_disk_MT_category_1"]
            / disk_mt["total_mass_transferred_through_disk"]
        )
        disk_mt["fraction_mass_transferred_through_disk_category_2"] = (
            disk_mt["total_mass_transferred_through_disk_MT_category_2"]
            / disk_mt["total_mass_transferred_through_disk"]
        )
        disk_mt["fraction_mass_transferred_through_disk_category_3"] = (
            disk_mt["total_mass_transferred_through_disk_MT_category_3"]
            / disk_mt["total_mass_transferred_through_disk"]
        )
        disk_mt["fraction_mass_transferred_through_disk_category_4"] = (
            disk_mt["total_mass_transferred_through_disk_MT_category_4"]
            / disk_mt["total_mass_transferred_through_disk"]
        )
        disk_mt["fraction_mass_transferred_through_disk_category_5"] = (
            disk_mt["total_mass_transferred_through_disk_MT_category_5"]
            / disk_mt["total_mass_transferred_through_disk"]
        )

        # Calculate error
        disk_mt["fractional_error_mass_transferred_through_disk_categories"] = 1 - (
            disk_mt["fraction_mass_transferred_through_disk_category_0"]
            + disk_mt["fraction_mass_transferred_through_disk_category_1"]
            + disk_mt["fraction_mass_transferred_through_disk_category_2"]
            + disk_mt["fraction_mass_transferred_through_disk_category_3"]
            + disk_mt["fraction_mass_transferred_through_disk_category_4"]
            + disk_mt["fraction_mass_transferred_through_disk_category_5"]
        )
        disk_mt["absolute_error_mass_transferred_through_disk_categories"] = (
            disk_mt["fractional_error_mass_transferred_through_disk_categories"]
            * disk_mt["total_mass_transferred_through_disk"]
        )
        print(
            "Maximum fractional error mass categories: {}\nMaximum absolute error mass categories: {}".format(
                disk_mt[
                    "fractional_error_mass_transferred_through_disk_categories"
                ].max(),
                disk_mt[
                    "absolute_error_mass_transferred_through_disk_categories"
                ].max(),
            )
        )

        bin_size = 0.1
        bins = np.arange(0, 1 + bin_size, bin_size)
        bincenters = (bins[1:] + bins[:-1]) / 2

        ######
        # Plot
        fig = plt.figure(figsize=(20, 20))

        #
        gs = fig.add_gridspec(nrows=10, ncols=1)
        ax = fig.add_subplot(gs[:, :])

        stacked_histograms = np.zeros(bincenters.shape[0])

        for i in range(6):
            # Calculate the histograms
            hist = np.histogram(
                disk_mt["fraction_mass_transferred_through_disk_category_{}".format(i)],
                bins=bins,
                weights=disk_mt["number_per_solar_mass"],
                density=True,
            )

            #
            ax.bar(
                bincenters,
                hist[0],
                bin_size,
                label="category {}".format(i),
                bottom=stacked_histograms,
            )

            stacked_histograms += hist[0]

            # ax.bar(
            #         ,
            #     array_total,
            #     binsize,
            #     label='total{}'.format(' (base_query: {})'.format(return_string_querylist(base_querylist)) if base_querylist is not None else ''),
            #     color='blue',
            #     alpha=0.5,
            #     hatch='/',
            #     antialiased=True,
            #     rasterized=True,
            # )
        plt.show()

    # # set legend
    # ax.legend(loc=2)

    # # Set labels
    # ax.set_ylabel("Normalized number of systems")

    # ax.set_xlabel("Fraction of mass transferred through disk during RLOF episode")

    # ax.set_title("Fraction of total mass transferred through a disk during RLOF episode\nFor systems with initially stable RLOF onto MS. Normalized by all stable MT onto on MS", fontsize=24)

    # # Set scales
    # ax.set_xscale('log')
    # ax.set_yscale('log')

    # if add_cdf:
    #     ax_cdf.set_ylim([0, 1.1])
    #     ax_cdf.set_xscale('log')
    #     ax_cdf.set_xlabel(ax.get_xlabel())
    #     ax.set_xlabel('')
    #     ax_cdf.set_xlim(ax.get_xlim())

    # #######################
    # # Save and finish
    # fig = add_plot_info(fig, plot_settings)
    # show_and_save_plot(fig, plot_settings)


# simulation_dir = '/home/david/projects/binary_c_root/results/RLOF/LOW_RES_RLOF_SANA_2021_ENSEMBLE_fractions'
simulation_dir = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions"
rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(simulation_dir)
dataset_dict = rlof_episode_dataset_dict["datasets"]

print(dataset_dict)
quit()

plot_disk_MT_mass_categories(
    dataset_dict, add_cdf=True, plot_settings={"show_plot": True}
)
