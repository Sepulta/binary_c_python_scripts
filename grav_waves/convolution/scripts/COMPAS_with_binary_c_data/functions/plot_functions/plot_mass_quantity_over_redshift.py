"""
Function to plot the total intrinsic rate over redshift
"""

import h5py

import matplotlib
import matplotlib.pyplot as plt

import numpy as np

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def generate_bins_from_keys(input_array):
    """
    Function to generate the bins that the array of keys represents
    """

    stepsize = np.around(min(input_array[1:] - input_array[:-1]), decimals=10)
    bins = np.arange(
        input_array.min() - 0.5 * stepsize, input_array.max() + 1.5 * stepsize, stepsize
    )

    return bins


def get_data_plot_mass_quantity_over_redshift(
    target_filename_convolution, mass_quantity
):
    """
    Function to get the data for the plot
    """

    # Open file
    target_h5file = h5py.File(target_filename_convolution, "r")

    intrinsic_merger_rate_per_system_per_redshift = target_h5file[
        "convolution_data/merger_rate"
    ][()]
    flat_intrinsic_merger_rates = (
        intrinsic_merger_rate_per_system_per_redshift.flatten()
    )

    try:
        redshifts = target_h5file["convolution_settings/redshift"][()]
        redshift_bins = generate_bins_from_keys(redshifts)
    except:
        redshift_bins = np.linspace(0, 10, 101)
        redshifts = (redshift_bins[1:] + redshift_bins[:-1]) / 2

    redshift_array = (
        np.ones(intrinsic_merger_rate_per_system_per_redshift.shape)
        * redshifts[np.newaxis, :].T
    )
    flat_redshift_array = redshift_array.flatten()

    mask = target_h5file["convolution_settings/DCOmask"][()]

    # Read out mass
    mass_1 = target_h5file["BSE_Double_Compact_Objects/Mass(1)"][()][mask]
    mass_2 = target_h5file["BSE_Double_Compact_Objects/Mass(2)"][()][mask]

    secondary_mass = np.min([mass_1, mass_2], axis=0)
    chirpmass = np.power(mass_1 * mass_2, 3.0 / 5.0) / np.power(
        mass_1 + mass_2, 1.0 / 5.0
    )

    # Handle the choice of mass quantity
    if mass_quantity == "primary_mass":
        primary_mass = np.max([mass_1, mass_2], axis=0)
        mass_array = (
            np.ones(intrinsic_merger_rate_per_system_per_redshift.shape)
            * primary_mass[:, np.newaxis].T
        )
        flat_mass_array = mass_array.flatten()
    if mass_quantity == "total_mass":
        total_mass = np.add(mass_1, mass_2)
        mass_array = (
            np.ones(intrinsic_merger_rate_per_system_per_redshift.shape)
            * total_mass[:, np.newaxis]
        )
        flat_mass_array = mass_array.flatten()

    return (
        flat_redshift_array,
        flat_mass_array,
        redshift_bins,
        flat_intrinsic_merger_rates,
    )


def plot_mass_quantity_over_redshift(
    target_filename_convolution, mass_quantity, bins, plot_settings={}
):
    """
    Function to plot the intrinsic rate over redshift
    """

    # Settings
    DIFF_MAX_THRESHOLD = 3

    x_data, y_data, x_bins, weights = get_data_plot_mass_quantity_over_redshift(
        target_filename_convolution, mass_quantity
    )

    # calculate the 2-d histogram and find the max value
    pre_hist = np.histogram2d(x_data, y_data, bins=[x_bins, bins], weights=weights)
    global_max_val = np.max(pre_hist[0])

    # Determine the norm
    norm = matplotlib.colors.LogNorm(
        vmin=10 ** (np.log10(global_max_val) - DIFF_MAX_THRESHOLD), vmax=global_max_val
    )

    #####
    # Plot

    #
    fig = plt.figure(figsize=(30, 20))

    #
    gs = fig.add_gridspec(nrows=1, ncols=11)

    # Create axes
    ax_1 = fig.add_subplot(gs[0, 0:10])
    ax_cb = fig.add_subplot(gs[0, 10])

    # Plot density
    _ = ax_1.hist2d(x_data, y_data, bins=[x_bins, bins], weights=weights, norm=norm)

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb, cmap=matplotlib.cm.viridis, norm=norm, extend="min"
    )
    cbar.ax.set_ylabel("rate Intrinsic [dN/dGpc3/dYr]")

    #
    ax_1.set_xlabel("redshift")
    ax_1.set_ylabel("{}".format(mass_quantity))

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
