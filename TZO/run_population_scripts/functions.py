"""
Extra function for the population
"""


def add_distribution_grid_variables(population_object, resolution):
    """
    Function to add the distribution grid variables
    """

    population_object.add_grid_variable(
        name="lnm1",
        longname="Primary mass",
        valuerange=[3, 200],
        samplerfunc="const(math.log(3), math.log(200), {})".format(resolution["M_1"]),
        precode="M_1=math.exp(lnm1)",
        probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 201, -1.3, -2.3, -2.3)*M_1",
        dphasevol="dlnm1",
        parameter_name="M_1",
        condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
    )
    population_object.add_grid_variable(
        name="q",
        longname="Mass ratio",
        valuerange=["0.1/M_1", 1],
        samplerfunc="const(0.1/M_1, 1, {})".format(resolution["q"]),
        probdist="flatsections(q, [{'min': 0.1/M_1, 'max': 1.0, 'height': 1}])",
        dphasevol="dq",
        precode="M_2 = q * M_1",
        parameter_name="M_2",
        condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
    )
    population_object.add_grid_variable(
        name="log10per",  # in days
        longname="log10(Orbital_Period)",
        valuerange=[0.15, 5.5],
        samplerfunc="const(0.15, 5.5, {})".format(resolution["per"]),
        precode="""orbital_period = 10** log10per
sep = calc_sep_from_period(M_1, M_2, orbital_period)
sep_min = calc_sep_from_period(M_1, M_2, 10**0.15)
sep_max = calc_sep_from_period(M_1, M_2, 10**5.5)""",
        probdist="sana12(M_1, M_2, sep, orbital_period, sep_min, sep_max, math.log10(10**0.15), math.log10(10**5.5), -0.55)",
        parameter_name="orbital_period",
        dphasevol="dlog10per",
    )

    return population_object
