import time
import os

import numpy as np

import astropy.units as u

import matplotlib.pyplot as plt
import matplotlib.colors as colors

from david_phd_functions.masstransfer.functions import *
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot


G = 1


def iso(md, ma, a):
    val = ((md / (md + ma)) ** 2) * np.sqrt(G * (ma + md) * a)

    return val


def jeans(md, ma, a):
    val = ((ma / (md + ma)) ** 2) * np.sqrt(G * (ma + md) * a)

    return val


def disk(ma, rloss, inner_disk_radius=0):
    val = np.sqrt(G * ma * rloss) - np.sqrt(G * ma * inner_disk_radius)

    return val


def circumbinary_ring(md, ma, aring):
    hring = np.sqrt(G * (md + ma) * aring)

    return hring


def ratio_disk_iso(ma, md, a, rloss, inner_disk_radius=0, include_iso=False):
    """
    Function to return the ratio of specific angmom in disk at rloss vs that at the center of the accretor via isotropic reeeemission
    """

    # circ_radius = circularisation_radius(mass_donor=mass_donor, mass_accretor=mass_accretor)

    if include_iso:
        return (
            disk(ma=ma, rloss=rloss, inner_disk_radius=inner_disk_radius)
            + iso(md=md, ma=ma, a=a)
        ) / iso(md=md, ma=ma, a=a)
    else:
        return disk(ma=ma, rloss=rloss) / iso(md=md, ma=ma, a=a)


def ratio_disk_circumbinary(ma, md, a, aring, rloss, include_iso=False):
    """
    Function to return the ratio of specific angmom in disk at rloss vs that at the center of the accretor via isotropic reeeemission
    """

    if include_iso:
        return (disk(ma=ma, rloss=rloss) + iso(md=md, ma=ma, a=a)) / circumbinary_ring(
            md=md, ma=ma, aring=aring
        )
    else:
        return disk(ma=ma, rloss=rloss) / circumbinary_ring(md=md, ma=ma, aring=aring)


def ratio_disk_jeans(ma, md, a, rloss, include_iso=False):
    """
    Function to return the ratio of specific angmom in disk at rloss vs that at the center of the accretor via isotropic reeeemission
    """

    if include_iso:
        return (disk(ma=ma, rloss=rloss) + iso(md=md, ma=ma, a=a)) / jeans(
            md=md, ma=ma, a=a
        )
    else:
        return disk(ma=ma, rloss=rloss) / jeans(md=md, ma=ma, a=a)


def plot_disk_vs_isotropic(
    r_loss_array,
    q_array,
    factor_outer_edge_rl_accretor,
    inner_disk_radius=0,
    include_iso=False,
    include_rmin=True,
    include_rcirc=True,
    plot_log=False,
    plot_settings=None,
):
    if not plot_settings:
        plot_settings = {}

    Maccretor = 1

    #####
    # Create meshgrid and calculate ratio
    X, Y = np.meshgrid(r_loss_array, q_array)
    Z = ratio_disk_iso(
        Maccretor,
        Maccretor * Y,
        1,
        X,
        inner_disk_radius=inner_disk_radius,
        include_iso=include_iso,
    )

    # Make sure Z is not smaller than 1:
    Z[Z < 1] = 1

    #####
    # Plot the figure
    fig, ax = plt.subplots(figsize=(16, 16))

    # Colormesh and contourplot
    c = ax.pcolormesh(
        X, Y, Z, cmap="RdBu", norm=colors.LogNorm(vmin=Z.min(), vmax=Z.max())
    )
    CS = ax.contour(
        X,
        Y,
        Z,
        levels=[0.1, 0.5, 1, 1.5, 2, 5, 25, 50, 100],
        colors="k",
    )

    # Add the rl_radii and some
    rl_radii = factor_outer_edge_rl_accretor * rochelobe_radius_accretor(
        mass_donor=Maccretor * q_array, mass_accretor=Maccretor
    )
    ax.plot(rl_radii, q_array)
    ax.fill_betweenx(
        q_array,
        rl_radii,
        x2=r_loss_array.max(),
        interpolate=True,
        hatch="x",
        color="#b8b894",
        zorder=5,
        alpha=0.5,
        label="R > {} * RL".format(factor_outer_edge_rl_accretor),
    )

    # # circ radius
    if include_rcirc:
        r_circs = circularisation_radius(q_array, np.ones(q_array.shape[0]))
        ax.plot(r_circs, q_array, label=r"R$_{circ}$", zorder=5)

    if include_rmin:
        r_mins = minimum_radius(q_array, np.ones(q_array.shape[0]))
        ax.plot(r_mins, q_array, label=r"R$_{min}$", zorder=5)

    # If there is an inner disk radius:
    if inner_disk_radius:
        ax.axvline(inner_disk_radius, linestyle="--", label=r"R$_{acc}$", zorder=5)

        ax.fill_betweenx(
            q_array,
            x1=r_loss_array.min(),
            x2=inner_disk_radius,
            interpolate=True,
            hatch="//",
            color="#b8b894",
            zorder=5,
            alpha=0.5,
            label="accretor (R+",
        )

    ax.set_xlim(np.min(r_loss_array), 1)

    # Set up title etc
    ax.clabel(CS, inline=1, fontsize=10)
    ax.set_title(r"Ratio h$_{disk(r loss)}$ vs h$_{isotropic}$")
    ax.set_xlabel("Radius mass loss [a (separation)]")
    ax.set_ylabel(r"Mass ratio (M$_{accretor}$/M$_{donor}$)")

    # Set colorbar
    fig.colorbar(c, ax=ax, label=r"h$_{disk(r loss)}$/h$_{isotropic}$")

    if plot_log:
        ax.set_xscale("log")

    #
    plt.legend().set_zorder(10)

    plt.yscale("log")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_disk_vs_circumbinary(
    r_loss_array,
    q_array,
    factor_outer_edge_rl_accretor,
    include_iso=False,
    plot_settings=None,
):
    Maccretor = 1

    if not plot_settings:
        plot_settings = None

    #####
    # Create meshgrid and calculate ratio
    X, Y = np.meshgrid(r_loss_array, q_array)
    Z = ratio_disk_circumbinary(Maccretor, Maccretor * Y, 1, np.sqrt(2), X, include_iso)

    #####
    # Plot the figure
    fig, ax = plt.subplots(figsize=(16, 16))

    # Colormesh and contourplot
    c = ax.pcolormesh(
        X, Y, Z, cmap="RdBu", norm=colors.LogNorm(vmin=Z.min(), vmax=Z.max())
    )
    CS = ax.contour(
        X,
        Y,
        Z,
        levels=[0.1, 0.5, 0.8, 1],
        colors="k",
    )

    # Add the rl_radii and some
    rl_radii = factor_outer_edge_rl_accretor * rochelobe_radius_accretor(
        mass_donor=Maccretor * q_array, mass_accretor=Maccretor
    )
    ax.plot(rl_radii, q_array)
    ax.fill_betweenx(
        q_array,
        rl_radii,
        x2=r_loss_array.max(),
        interpolate=True,
        hatch="x",
        color="#b8b894",
        zorder=5,
        alpha=0.5,
        label="R > {} * RL".format(factor_outer_edge_rl_accretor),
    )

    # Set up title etc
    ax.clabel(CS, inline=1, fontsize=10)
    ax.set_title(r"Ratio h$_{disk(r loss)}$ vs h$_{circumbinary}$")
    ax.set_xlabel("Radius mass loss [a (separation)]")
    ax.set_ylabel(r"Mass ratio (M$_{accretor}$/M$_{donor}$)")

    # Set colorbar
    fig.colorbar(c, ax=ax, label=r"h$_{disk(r loss)}$/h$_{circumbinary}$")

    #
    plt.legend().set_zorder(10)

    plt.yscale("log")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_disk_vs_jeans(
    r_loss_array,
    q_array,
    factor_outer_edge_rl_accretor,
    include_iso=False,
    plot_settings=None,
):
    if not plot_settings:
        plot_settings = {}

    Maccretor = 1

    #####
    # Create meshgrid and calculate ratio
    X, Y = np.meshgrid(r_loss_array, q_array)
    Z = ratio_disk_jeans(Maccretor, Maccretor * Y, 1, X, include_iso)

    #####
    # Plot the figure
    fig, ax = plt.subplots(figsize=(16, 16))

    # Colormesh and contourplot
    c = ax.pcolormesh(
        X, Y, Z, cmap="RdBu", norm=colors.LogNorm(vmin=Z.min(), vmax=Z.max())
    )
    CS = ax.contour(
        X,
        Y,
        Z,
        levels=[0.1, 0.5, 1, 5, 10, 50, 100],
        colors="k",
    )

    # Add the rl_radii and some
    rl_radii = factor_outer_edge_rl_accretor * rochelobe_radius_accretor(
        mass_donor=Maccretor * q_array, mass_accretor=Maccretor
    )
    ax.plot(rl_radii, q_array)
    ax.fill_betweenx(
        q_array,
        rl_radii,
        x2=r_loss_array.max(),
        interpolate=True,
        hatch="x",
        color="#b8b894",
        zorder=5,
        alpha=0.5,
        label="R > {} * RL".format(factor_outer_edge_rl_accretor),
    )

    # Set up title etc
    ax.clabel(CS, inline=1, fontsize=10)
    ax.set_title(r"Ratio h$_{disk(r loss)}$ vs h$_{jeans}$")
    ax.set_xlabel("Radius mass loss [a (separation)]")
    ax.set_ylabel(r"Mass ratio (M$_{accretor}$/M$_{donor}$)")

    # Set colorbar
    fig.colorbar(c, ax=ax, label=r"h$_{disk(r loss)}$/h$_{jeans}$")

    #
    plt.legend().set_zorder(10)
    plt.yscale("log")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
