from binarycpython.utils.grid import Population


def parse_function(self, output):
    # print(repr(output))

    return output.splitlines()


test_pop = Population()

test_pop.set(verbosity=2)
test_pop.set(parse_function=parse_function)

test_pop.set(
    metallicity=0.02,
    max_stellar_type_1=1,
    max_stellar_type_2=1,
    MINT_Kippenhahn=0,
    MINT_nuclear_burning=0,
)

# Wrap in loop
test_pop.set(
    M_1=1.2,
    M_2=0.36,
    eccentricity=0.4,
    orbital_period=2,
    E2_prescription="E2_MINT",
    tides_convective_damping="TIDES_ZAHN1989",
)
output_1 = test_pop.evolve_single()


# test_pop.set(M_1=1.2, M_2=0.36, eccentricity=0.4, orbital_period=2, E2_prescription="E2_IZZARD", tides_convective_damping="TIDES_HURLEY2002")
# output_2 = test_pop.evolve_single()

# test_pop.set(M_1=1.2, M_2=0.36, eccentricity=0.4, orbital_period=2, E2_prescription="E2_HURLEY_2002", tides_convective_damping="TIDES_HURLEY2002")
# output_3 = test_pop.evolve_single(parse_function=parse_function)

print(output_1)
