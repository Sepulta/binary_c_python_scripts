#!/usr/bin/env perl
use strict; # highly recommended
use 5.16.0; # highly recommended
use distribution_functions;
use binary_grid2; # required
use binary_grid::C; # required (uses C backend )
use rob_misc qw/ncpus/;

#
my $nthreads = 1;

# make a new stellar population
my $population = binary_grid2->new(
    # options can be given now ...
    metallicity => 0.02, # mass fraction of "metals"
    max_evolution_time => 15000, # Myr
    nthreads=>$nthreads, # number of threads,
    check_args=>1, # check that args exist
    vb=>1, # turn on verbose logging (can be 0,1,2...)
    return_array_refs=>1, # quicker data parsing mode
    log_args=>1,
    sort_args=>1,
    save_args=>1,
    log_args_dir=>'template_results/perl/tmp_dir',
    minimum_timestep=>1e-6,
);

$population->set(
   C_logging_code => '
if (stardata->model.time > 0)
{
    Printf("DAVID_CENTER_TEST %30.12e " // 1
        "%g %g\\n", // 2-5

        //
        stardata->model.time, // 1
        stardata->common.zero_age.mass[0], // 2
        stardata->model.probability // 3
    );
    /* Kill the simulation to save time */
    stardata->model.max_evolution_time = stardata->model.time - stardata->model.dtm;
};',
);

# check structures are up to date
exit if($population->check_structure_sizes());

############################################################
#
# Now we set up the grid of stars.
#
# duplicity is 0 for single stars, 1 for binary stars
# and 2 for a mixed population sampled at equal times
my $duplicity = 0;
$population->{_grid_options}{multiplicity} =
    $duplicity == 0 ? 1 : 2;

# make a grid of $nstars single stars, log-spaced,
# with masses between $mmin and $mmax
my $nstars = 100;
my $mmin = 0.1;
my $mmax = 80.0;

$population->add_grid_variable(
    'name'       =>'lnm1',
    'longname'   =>'Primary mass',
    'range'      =>[log($mmin),log($mmax)],
    'resolution' => $nstars, # just a counter for the grid
    'spacingfunc'=>"const(log($mmin),log($mmax),$nstars)",
    'precode'    =>'$m1=exp($lnm1);',
    'probdist'   =>'Kroupa2001($m1)*$m1',
    'method'     =>'grid',
    'gridtype'   =>'centred',
);
$population->set(
    multiplicity=>1
);

############################################################
# link population to custom data parser function
$population->set(
    parse_bse_function_pointer => \&main::parse_data
    );

my %init = $population->initial_abundance_hash('Karakas2002',0.02);
my %isotope_hash = $population->isotope_hash();
my @isotope_list = $population->isotope_list();
my %nuclear_mass_hash = $population->nuclear_mass_hash();
my @nuclear_mass_list = $population->nuclear_mass_list();
my @sources = $population->source_list();
my @ensemble = $population->ensemble_list();

# you can use Data::Dumper to see the contents
# of the above lists and hashes
if(0){
    print Data::Dumper->Dump([
        #\%init,
        #\%isotope_hash,
        #\@isotope_list,
        #\%nuclear_mass_hash,
        \@nuclear_mass_list,
        #\@sources,
        #\@ensemble
                         ]);
}


# Check if file exists
my $filename = 'template_results/perl/results/sampling.txt';
if (-e $filename) {
    print "file exits. Removing file";
    unlink $filename;
}

# evolution the stellar population (this takes some time)
$population->evolve();

# done : exit
exit;

############################################################
# subroutines
############################################################

sub parse_data
{
    my ($population, $results) = @_;

    #
    my $filename = 'template_results/perl/results/sampling.txt';
    unless (-e $filename) {
        open(FH, '>', $filename) or die $!;
        print FH "mass\tprobability\n";
        close(FH);
    }

    while(1)
    {
        # subsequent calls to tbse_line contain
        # (references to) arrays of data
        my $la = $population->tbse_line();

        # first element is the "header" line
        my $header = shift @$la;

        # break out of the loop if this is 'fin'
        last if ($header eq 'fin');

        # check if $header matches one of your
        # expected data lines, if so, act
        if($header eq 'DAVID_CENTER_TEST')
        {
            # matched MY_STELLAR_DATA header
            #
            # get time, mass, probability etc. as specified above
            #
            # (note that $probability is possibly not the same as
            #  the progenitor's probability!)

            my $time = $la->[0];
            my $mass = $la->[1];
            my $probability = $la->[2];

            print "$mass $probability\n";

            open(FH, '>>', $filename) or die $!;
            print FH "$mass\t$probability\n";
            close(FH);
        }
    }
}
