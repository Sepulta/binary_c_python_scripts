"""
Functions for the ensemble plotting
"""

import copy
import time
import operator

import collections
from collections import OrderedDict

from functools import reduce  # forward compatibility for Python 3

import numpy as np

import fpdf
import matplotlib.pyplot as plt

import astropy.units as u
from binarycpython.utils.dicts import AutoVivificationDict

ITERATOR_SYMBOLS = ["i", "j", "k", "l", "m", "n", "o"]


def check_value(parameter, iterator_n):
    # check for disk type:
    if parameter == "a":
        parameter_value_string = "\\forall"
    elif parameter == "i":
        parameter_value_string = ITERATOR_SYMBOLS[iterator_n]
        iterator_n += 1
    else:
        parameter_value_string = str(parameter)

    return parameter_value_string, iterator_n


def return_string(
    weight_type,
    bin_type,
    disk_type="a",
    accretor_type="a",
    donor_type="a",
    fraction_racc_separation="a",
    log10_massratio_value="a",
    remove_end=True,
):
    """
    Function to return the symbol that represents the current ensemble selection
    """

    #
    keys = [
        "weight_type",
        "bin_type",
        "disk_type",
        "accretor_type",
        "donor_type",
        "fraction_racc_separation",
        "log10_massratiodon",
    ]
    keynames = ["weight", "bin", "disk", "acc", "don", "frac", "log10q"]

    #
    values = []
    values.append(weight_type)
    values.append(bin_type)
    values.append(disk_type)
    values.append(accretor_type)
    values.append(donor_type)
    values.append(fraction_racc_separation)
    values.append(log10_massratio_value)

    # iterators
    iterator_n = 0

    # Create the string values
    values_string = []

    # Add weight and bin type
    values_string.append(weight_type)
    values_string.append(bin_type)

    disk_type_value_string, iterator_n = check_value(disk_type, iterator_n)
    values_string.append(disk_type_value_string)

    accretor_type_value_string, iterator_n = check_value(accretor_type, iterator_n)
    values_string.append(accretor_type_value_string)

    donor_type_value_string, iterator_n = check_value(donor_type, iterator_n)
    values_string.append(donor_type_value_string)

    fraction_racc_separation_value_string, iterator_n = check_value(
        fraction_racc_separation, iterator_n
    )
    values_string.append(fraction_racc_separation_value_string)

    log10_massratio_value_string, iterator_n = check_value(
        log10_massratio_value, iterator_n
    )
    values_string.append(log10_massratio_value_string)

    # zip and remove the end
    zipped_dings = list(zip(keys, keynames, values, values_string))

    if remove_end:
        while zipped_dings[-1][2] == "a":
            zipped_dings.pop(-1)

    # Combine global keys
    combined_global_keys = ", ".join(
        ["{}={}".format(entry[1], entry[3]) for entry in zipped_dings[:2]]
    )

    # Combine local keys
    combined_local_keys = ", ".join(
        ["{}={}".format(entry[1], entry[3]) for entry in zipped_dings[2:]]
    )

    # Build the whole string
    final_string = r"A$^{%s}_{%s}$" % (combined_global_keys, combined_local_keys)

    return final_string


def generate_frontpage_pdf_fraction_ensemble(ensemble, output_name):
    """
    Function to generate a pdf containing some information about the dataset
    """

    # Write settings to pdf
    pdf = fpdf.FPDF(format="letter")
    pdf.add_page()
    pdf.set_font("Arial", size=24)
    pdf.write(10, "{} plots".format(ensemble["metadata"]["simname"]))
    pdf.ln()
    pdf.set_font("Arial", size=12)
    pdf.write(
        5,
        "pdf generated on: {}".format(
            time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        ),
    )
    pdf.ln()
    pdf.ln()
    for key in [
        "simname",
        "population_id",
        "population_name",
        "failed_count",
        "failed_prob",
        "total_probability",
        "total_count",
        "start_timestamp",
        "end_timestamp",
        "total_mass_run",
        "total_probability_weighted_mass_run",
    ]:
        pdf.write(5, "{}:\n    {}".format(key, ensemble["metadata"][key]))
        pdf.ln()
    pdf.output(output_name)


"""
script containing the function that can query a ensemble dict (i.e. nested dict that has probabilities as the final entry)
"""


def combine_or_filter_stellar_types(
    input_ensemble, accretor_or_donor_key, stellar_type_value
):
    """
    Function to decide whether the input ensemble is filtered by a given
    <accretor/donor> stellar type, or that all of them are just folded
    """

    if not stellar_type_value == -1:
        if str(stellar_type_value) in input_ensemble[accretor_or_donor_key].keys():
            return input_ensemble[accretor_or_donor_key][str(stellar_type_value)]
        else:
            return {}
    else:
        if accretor_or_donor_key in input_ensemble.keys():
            return merge_all_subdicts(input_ensemble[accretor_or_donor_key])
        else:
            return {}
