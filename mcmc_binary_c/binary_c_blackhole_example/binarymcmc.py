import numpy as np
import matplotlib.pyplot as plt

import emcee
import corner
import pandas as pd

import binarycpython
from binarycpython.utils.grid import Population
from binarycpython.utils.functions import output_lines

from david_phd_functions.binaryc.personal_defaults import personal_defaults

# https://stackoverflow.com/questions/49810234/using-emcee-with-gaussian-priors


print(X, Y)


quit()


def parse_function(self, output):
    # Function to extract the data from the output, and
    # extract info from the population instance

    matched_lines = []

    # print("TYYYYYY", output)

    for i, el in enumerate(output_lines(output)):
        headerline = el.split()[0]

        # Check the header and act accordingly
        if headerline == "DAVID_SCO":
            parameters = [
                "time",
                "mass_1",
                "zams_mass_1",
                "stellar_type",
                "prev_stellar_type",
                "metallicity",
                "probability",
                "prev_mass",
                "core_mass",
                "co_core_mass",
                "he_core_mass",
            ]
            values = el.split()[1:]
            values = [float(i) for i in values]

            # Check whether the amount of parameters is the same as amount of values
            if not len(values) == len(parameters):
                print("Length of readout vaues is not equal to length of parameters")
                raise ValueError

            # Add matched lines to list
            matched_lines.append(values)

    return matched_lines


### Goal of this script:
# Set up method to run mcmc model with binary_c:
#
# Steps:
# - [x] Make sure the single_system works well with capsules AND mcmc at the same time.
# - [ ] Create a simple model: vary mass and metallicity to find the highest mass black hole. I.e. theta consists of M1 and metallicity.
# - [ ] Give the model a certain weight score based on the total mass of the black hole


# Create a store memory capsule
store_mem_caps = binarycpython._binary_c_bindings.return_store_memaddr()

# Add situational settings
mcmc_pop = Population()

# Load personal defaults
mcmc_pop.set(**personal_defaults)

mcmc_pop.set(
    M_1=100,
    max_evolution_time=15000,
    separation=1000000000,
    orbital_period=400000000,
    M_2=0.08,
    metallicity=0.02,
    # data_dir='results',
    data_dir="",
    # base_filename="massloss_{}.dat".format(metallicity),
    verbosity=0,
    amt_cores=1,
    binary=0,
    parse_function=parse_function,
    _store_memaddr=store_mem_caps,
    david_logging_function=7,
)

#

# # Delete the memory adress
# binarycpython._binary_c_bindings.free_store_memaddr(store_mem_caps)
# mcmc_pop.set(_store_memaddr=-1)
# del store_mem_caps


def model(theta):
    M_1, metallicity = theta
    mcmc_pop.set(M_1=M_1, metallicity=metallicity)
    output = mcmc_pop.evolve_single(clean_up_custom_logging_files=False)

    print(theta)
    # if not output:
    #     print("Empty output!")

    return output


def lnlike(theta):
    # The further apart the numbers are, the more negative they are. the closer they get to eachother, the closer to 0 we get.
    model_output = model(theta)

    if model_output:
        # print(model_output)
        bh_mass = 0
        for line in model_output:
            if int(line[3]) == 14:
                bh_mass = line[1]
        if bh_mass == 0:
            return -10
        else:
            return -0.5 * (1 / bh_mass)
    else:
        return -10


def lnprior(theta):
    M_1, metallicity = theta

    if (0 < M_1 < 300) and (0.0002 < metallicity < 0.02):
        return 0.0
    else:
        return -np.inf


def lnprob(theta):
    lp = lnprior(theta)
    if not np.isfinite(lp):
        return -np.inf
    return lp + lnlike(theta)


nwalkers = 6
niter = 10
initial = np.array([18.0, 0.01])
ndim = len(initial)
p0 = [np.array(initial) + 1e-3 * np.random.randn(ndim) for i in range(nwalkers)]


def main(p0, nwalkers, niter, ndim, lnprob):
    sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob, threads=1)

    print("Running burn-in...")
    p0, _, _ = sampler.run_mcmc(p0, 100)
    sampler.reset()

    print("Running production...")
    pos, prob, state = sampler.run_mcmc(p0, niter)

    return sampler, pos, prob, state


sampler, pos, prob, state = main(p0, nwalkers, niter, ndim, lnprob)

samples = sampler.flatchain

labels = ["M_1", "metallicity"]
fig = corner.corner(
    samples,
    show_titles=True,
    labels=labels,
    plot_datapoints=True,
    quantiles=[0.16, 0.5, 0.84],
)
fig.show()

quit()
