"""
Script to generate the PPISN edge data
"""

import json

import numpy as np

from binarycpython.utils.grid import Population

from david_phd_functions.misc.edge_finder.functions import edge_finder

from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.parse_function import (
    parse_function_ppisn_edge_general,
    parse_function_ppisn_edge,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.custom_logging_function import (
    custom_logging_string,
)


def generate_edge_data(
    output_file, bse_settings, grid_settings, system_generator_settings
):
    """
    Function to find the PPISN for a range of metallicities
    """

    #
    left_initial_mass = 20
    right_initial_mass = 300
    edge_diff_threshold = 0.05

    #
    log10metallicity_array = np.linspace(
        system_generator_settings["min_log10_metallicity"],
        system_generator_settings["max_log10_metallicity"],
        system_generator_settings["log10metallicity_resolution"],
    )
    edge_metallicities = 10**log10metallicity_array

    ##################
    # Set up population
    test_pop = Population(verbosity=1)

    def run_and_check_edge(mass):
        """
        Function to run the
        """

        test_pop.set(M_1=mass)
        output = test_pop.evolve_single(clean_up_custom_logging_files=False)

        return output

    # Load settings
    test_pop.set(**grid_settings)
    test_pop.set(verbosity=grid_settings["verbosity"] - 1)

    test_pop.set(**bse_settings)

    # Add situational settings
    test_pop.set(
        C_logging_code=custom_logging_string,
        parse_function=parse_function_ppisn_edge,
    )

    ##################
    # Edge finder
    ##################
    # Load previously found results
    edge_finder_outputs = {}

    # Loop over metallicities
    for i, metallicity in enumerate(edge_metallicities):
        if str(metallicity) in edge_finder_outputs.keys():
            print("Already have results for this metallicity")
            continue

        #
        test_pop.set(metallicity=metallicity)

        #
        try:
            status_dict = edge_finder(
                [left_initial_mass, right_initial_mass],
                run_and_check_edge,
                diff_threshold=edge_diff_threshold,
            )
            status_dict["metallicity"] = metallicity
            status_dict["found_edge"] = True
            edge_finder_outputs[metallicity] = status_dict
            print(
                "metallicity [{}/{}]: Adding results for metallicity: {}".format(
                    i, len(edge_metallicities), metallicity
                )
            )
        except ValueError:
            print(
                "metallicity [{}/{}]: Could not find a correct edge for metallicity: {}".format(
                    i, len(edge_metallicities), metallicity
                )
            )

            # Still add it to the output
            status_dict = {}
            status_dict["found_edge"] = False
            status_dict["metallicity"] = metallicity
            edge_finder_outputs[metallicity] = status_dict

    # Write to json output
    with open(output_file, "w") as f:
        f.write(json.dumps(edge_finder_outputs))

    # Load the results again and add the results of the
    with open(output_file, "r") as f:
        edge_finder_outputs = json.loads(f.read())

    # Set the new parse function, go over all the edge points and fill the dict further
    test_pop.set(
        parse_function=parse_function_ppisn_edge_general,
    )
    for metallicity in edge_finder_outputs.keys():
        status_dict = edge_finder_outputs[metallicity]

        test_pop.set(metallicity=float(metallicity))

        if status_dict.get("found_edge", False):
            if not status_dict.get("left_guess_results", {}):
                # Run with the left edge and find the values
                test_pop.set(M_1=status_dict["left_guess"])

                evol_output = test_pop.evolve_single(
                    clean_up_custom_logging_files=False
                )

                print(
                    "Adding output for the left guess at metallicity {}".format(
                        metallicity
                    )
                )
                status_dict["left_guess_results"] = evol_output
            else:
                print("Already have results for the left guess")

            if not status_dict.get("right_guess_results", {}):
                # Run with the left edge and find the values
                test_pop.set(M_1=status_dict["right_guess"])

                evol_output = test_pop.evolve_single(
                    clean_up_custom_logging_files=False
                )

                print(
                    "Adding output for the right guess at metallicity {}".format(
                        metallicity
                    )
                )
                status_dict["right_guess_results"] = evol_output
            else:
                print("Already have results for the right guess")

    # Write to json output
    with open(output_file, "w") as f:
        f.write(json.dumps(edge_finder_outputs))
