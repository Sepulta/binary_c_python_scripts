"""
Function to plot the initial mass of both components
"""

import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    get_num_subsets,
    plot_2d_results,
    plot_contourlevels,
    linestyle_list,
    create_centers_from_bins,
)
from TZO.functions.plot_functions.plot_utility_functions import (
    quantity_name_dict,
    quantity_unit_dict,
)

from david_phd_functions.plotting.utils import show_and_save_plot, add_plot_info
from david_phd_functions.plotting.canvas_functions import return_canvas_with_subsets
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def plot_2d_quantity(
    dataframe,
    x_quantity,
    x_quantity_bins,
    x_scale,
    x_label,
    y_quantity,
    y_quantity_bins,
    y_scale,
    y_label,
    base_query=None,
    querylist=None,
    add_ratio=False,
    weights_column="probability",
    all_metallicity_mode=False,
    plot_settings={},
):
    """
    Function to plot 2d quantity
    """

    #
    contourlevels = [1e-1, 1e0, 1e1]
    ratio_contourlevels = [0.1, 0.5, 0.9]

    #################################
    # get the data

    # perform base query
    if base_query is not None:
        dataframe = dataframe.query(base_query)

    #
    results_dict = {}

    print(dataframe[x_quantity].max())
    print(dataframe[x_quantity].min())

    #
    results_dict["all"] = np.histogram2d(
        dataframe[x_quantity],
        dataframe[y_quantity],
        bins=[x_quantity_bins, y_quantity_bins],
        weights=dataframe[weights_column],
    )[0].T
    if all_metallicity_mode:
        results_dict["all"] = results_dict["all"] / np.sum(results_dict["all"])

    if querylist is not None:
        for querydict in querylist:
            queried_df = dataframe.query(querydict["query"])
            results_dict[querydict["name"]] = np.histogram2d(
                queried_df[x_quantity],
                queried_df[y_quantity],
                bins=[x_quantity_bins, y_quantity_bins],
                weights=queried_df[weights_column],
            )[0].T
        if all_metallicity_mode:
            results_dict[querydict["name"]] = results_dict[querydict["name"]] / np.sum(
                results_dict[querydict["name"]]
            )

    #################################
    # Plotting

    #
    x_quantity_bincenters = create_centers_from_bins(x_quantity_bins)
    y_quantity_bincenters = create_centers_from_bins(y_quantity_bins)

    # set up norm
    max_val = results_dict["all"].max()
    custom_min_val = 10 ** (
        np.log10(max_val) - plot_settings.get("probability_floor", 4)
    )

    norm = colors.LogNorm(vmin=custom_min_val, vmax=max_val)

    ##
    # Figure out the number of subplots
    num_subsets = get_num_subsets(querylist)

    ##
    # Set up axes
    fig = plt.figure(figsize=(20, 20))
    fig, _, axes_dict = return_canvas_with_subsets(
        num_subsets, fig=fig, add_ratio_axes=add_ratio
    )

    ##
    # Plot 2-d results
    X, Y = np.meshgrid(x_quantity_bincenters, y_quantity_bincenters)
    fig, axes_dict, cb, cb_ratio = plot_2d_results(
        fig=fig,
        axes_dict=axes_dict,
        X=X,
        Y=Y,
        x_label=x_label,
        y_label=y_label,
        add_ratio=add_ratio,
        results_dict=results_dict,
        querylist=querylist,
        plot_settings=plot_settings,
        x_scale=x_scale,
        y_scale=y_scale,
        norm=norm,
    )

    #
    if all_metallicity_mode:
        cb.ax.set_ylabel("Number per formed solar mass")
    else:
        cb.ax.set_ylabel("Normalised number per formed solar mass")

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


def plot_initial_mass_comparison_no_query(
    df, all_metallicity_mode=False, plot_settings={}
):
    """
    Wrapper function to plot the initial masses without any query
    """

    # querylist = [
    #     {'name': 'main_sequence', 'query': 'prev_stellar_type_2 == 1', },
    #     {'name': 'Non MS', 'query': 'prev_stellar_type_2 != 1', }
    # ]
    querylist = None

    #
    plot_2d_quantity(
        df,
        x_quantity="zams_mass_1",
        x_quantity_bins=10 ** np.arange(0.5, 2, 0.1),
        x_scale="log",
        x_label="Initial mass NS",
        y_quantity="zams_mass_2",
        y_quantity_bins=10 ** np.arange(-1, 2, 0.1),
        y_scale="log",
        y_label="Initial mass companion",
        base_query=None,
        querylist=querylist,
        add_ratio=False,
        weights_column="probability",
        all_metallicity_mode=all_metallicity_mode,
        plot_settings=plot_settings,
    )


if __name__ == "__main__":
    #
    file = "/home/david/projects/binary_c_root/results/TZO/server_results/MID_RES_TESTING/population_results/Z0.005/all_tzo_events.dat"
    df = pd.read_csv(file, sep="\s+")

    plot_initial_mass_comparison_no_query(
        df, all_metallicity_mode=False, plot_settings={"show_plot": True}
    )
