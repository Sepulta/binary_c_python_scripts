"""
Script to test the new dataframe method
"""

import os
import logging

import numpy as np

from grav_waves.settings import convolution_settings, config_dict_cosmology
from grav_waves.gw_analysis.functions.functions import make_dataset_dict

from grav_waves.convolution.functions.convolution_functions import convolution_main
from grav_waves.convolution.functions.convolution_create_combined_dataset import (
    create_combined_dataset,
)
from grav_waves.convolution.functions.convolution_rebin_redshift_functions import (
    convolution_rebin_redshift,
)

#
this_file = os.path.abspath(__file__)


def run_convolution_function(
    main_dir,
    convolution_output_dir,
    convolution_settings,
    cosmology_settings,
    run_combine_datasets=False,
    run_convolution=True,
    run_rebinning=True,
    remove_original_after_rebinning=False,
    rebinned_stepsize=0.05,
    verbosity=1,
):
    """
    Function to run the convolution and handle the all the things

    TODO: Add options to run the volumetric convolution
    TODO: Add options to run the convolution with the detector sensitivity in there
    """

    # Get the correct directories
    dataset_dict_populations = make_dataset_dict(main_dir)
    os.makedirs(convolution_output_dir, exist_ok=True)

    # Create names for the output files
    combined_dataset_filename = os.path.join(
        convolution_output_dir, "combined_dataset.h5"
    )
    redshift_convolution_filename = os.path.join(
        convolution_output_dir, "convolution_results.h5"
    )
    rebinned_redshift_convolution_filename = os.path.join(
        convolution_output_dir, "rebinned_convolution_results.h5"
    )

    ##############
    # Build input dataset
    if run_combine_datasets:
        create_combined_dataset(
            dataset_dict=dataset_dict_populations,
            convolution_configuration=convolution_settings,
            cosmology_configuration=cosmology_settings,
            output_filename=combined_dataset_filename,
            dco_type=convolution_settings["dco_type"],
        )

    ##############
    # Run convolution
    if run_convolution:
        convolution_main(
            input_filename=combined_dataset_filename,
            convolution_configuration=convolution_settings,
            cosmology_configuration=cosmology_settings,
            output_filename=redshift_convolution_filename,
            verbosity=verbosity,
        )

    ##############
    # Do the re-binning
    if run_rebinning:
        # Set new bins
        new_bins = np.arange(
            0 - 0.5 * rebinned_stepsize,
            (convolution_settings["max_interpolation_redshift"] - 1)
            + 0.5 * rebinned_stepsize,
            rebinned_stepsize,
        )

        #
        convolution_rebin_redshift(
            input_dataset_filename=redshift_convolution_filename,
            output_dataset_filename=rebinned_redshift_convolution_filename,
            new_redshift_bins=new_bins,
        )

        #
        if remove_original_after_rebinning:
            os.remove(redshift_convolution_filename)


def run_convolution_binaryc(main_dir):
    """
    Function to run the convolution
    """

    #
    convolution_output_dir = os.path.join(main_dir, "convolution_results/")

    #
    convolution_stepsize = 0.01
    convolution_settings["stepsize"] = convolution_stepsize
    convolution_settings["time_bins"] = np.arange(
        0, 10 + convolution_stepsize, convolution_stepsize
    )
    convolution_settings["time_centers"] = (
        convolution_settings["time_bins"][1:] + convolution_settings["time_bins"][:-1]
    ) / 2
    convolution_settings["add_detection_probability"] = False
    convolution_settings["include_formation_rates"] = False
    convolution_settings["num_cores"] = 1
    convolution_settings["convolution_tmp_dir"] = os.path.join(
        convolution_output_dir, "tmp"
    )
    convolution_settings["dco_type"] = "combined_dco"
    convolution_settings["do_convolution"] = True
    convolution_settings[
        "global_query"
    ] = "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0)"
    convolution_settings["remove_pickle_files"] = False
    convolution_settings["logger"].setLevel(logging.DEBUG)

    #
    run_convolution_function(
        main_dir,
        convolution_output_dir,
        convolution_settings=convolution_settings,
        cosmology_settings=config_dict_cosmology,
        run_combine_datasets=True,
        run_convolution=True,
        run_rebinning=True,
        rebinned_stepsize=0.05,
        verbosity=1,
        remove_original_after_rebinning=True,
    )


#
if __name__ == "__main__":
    # multiprocessing test
    main_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED"
    main_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-5_CORE_MASS_SHIFT"

    # loop over datasets
    main_dirs = [
        "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED",
        "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-10_CORE_MASS_SHIFT",
        "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_10_EXTRA_MASSLOSS_PPISN",
        "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_20_EXTRA_MASSLOSS_PPISN",
        "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-5_CORE_MASS_SHIFT",
    ]

    for main_dir in main_dirs:
        run_convolution_binaryc(main_dir=main_dir)
