"""
Function to generate the table to display:
- Fraction of stable MT that starts with disk / normalised by total stable MT
- Fraction of all mass weighted MT that goes through disk / normalised by total stable MT mass weighted
- Fraction of all time weighted MT that goes through disk / normalised by total stable MT time weighted
"""

import json
import pandas as pd

from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)
from ensemble_functions.ensemble_functions import (
    get_recursive_sum,
    merge_all_subdicts,
    query_dict,
)
from grav_waves.settings import convolution_settings


def generate_table_disk_fractions(
    dataset_dict,
    output_filename,
    testing,
    caption="",
    label="",
    global_query_dict=None,
    verbose=0,
):
    """
    Function to generate the stable MT onto MS with fraction that starts with a disk. Fraction of all MT going through disk mass weighted, and time weighted
    """

    if verbose:
        print(
            "Running generate_table_stable_fractions.\n\tOutput: {}".format(
                output_filename
            )
        )

        if global_query_dict:
            print(
                "\twith global query: {}".format(global_query_dict["global_query_name"])
            )
            print(
                "\t\tqueries: {}".format(
                    ", ".join(
                        [
                            "{} between {}".format(
                                query["parameter_key"], query["value_range"]
                            )
                            for query in global_query_dict["query_list"]
                        ]
                    )
                )
            )

    #
    result_dict = {}

    # Calculate this for all the metallicities
    for key in dataset_dict.keys():
        if verbose:
            print("\tZ = {}".format(key))
        current_dataset = dataset_dict[key]

        # Get the correct ensemble data from the dict
        if verbose:
            print("\t\tLoading data")
        probability_to_yield_conversion_factor = (
            convolution_settings["binary_fraction"]
            * convolution_settings["mass_in_stars"]
        )
        ensemble_data = get_ensemble_data(
            current_dataset,
            testing,
            probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
        )["RLOF"]

        start_ensemble_data = ensemble_data["starting_rlof"]
        mass_ensemble_data = ensemble_data["fraction_separation_massratio"]["mass"]
        time_ensemble_data = ensemble_data["fraction_separation_massratio"]["time"]

        if global_query_dict:
            if verbose:
                print("\tPerforming queries")
            for query in global_query_dict["query_list"]:
                if verbose:
                    print(
                        "\t\tPerforming query {}".format(
                            "{} between {}".format(
                                query["parameter_key"], query["value_range"]
                            )
                        )
                    )
                start_ensemble_data = query_dict(
                    start_ensemble_data, query["parameter_key"], query["value_range"]
                )
                mass_ensemble_data = query_dict(
                    mass_ensemble_data, query["parameter_key"], query["value_range"]
                )
                time_ensemble_data = query_dict(
                    time_ensemble_data, query["parameter_key"], query["value_range"]
                )

        # Select the stable MT
        if verbose:
            print("\t\tSelecting Stable MT")
        stable_start_ensemble_data = query_dict(
            start_ensemble_data, "stable", [-0.5, 0.5]
        )
        stable_mass_ensemble_data = query_dict(
            mass_ensemble_data, "stable", [-0.5, 0.5]
        )
        stable_time_ensemble_data = query_dict(
            time_ensemble_data, "stable", [-0.5, 0.5]
        )

        # Select MT onto MS
        if verbose:
            print("\t\tSelecting MS accretors")
        ms_stable_start_ensemble_data = query_dict(
            stable_start_ensemble_data, "st_acc", [0.5, 1.5]
        )
        ms_stable_mass_ensemble_data = query_dict(
            stable_mass_ensemble_data, "st_acc", [0.5, 1.5]
        )
        ms_stable_time_ensemble_data = query_dict(
            stable_time_ensemble_data, "st_acc", [0.5, 1.5]
        )

        total_ms_stable_start_ensemble_data = get_recursive_sum(
            ms_stable_start_ensemble_data
        )
        total_ms_stable_mass_ensemble_data = get_recursive_sum(
            ms_stable_mass_ensemble_data
        )
        total_ms_stable_time_ensemble_data = get_recursive_sum(
            ms_stable_time_ensemble_data
        )

        # Select MT onto MS via disks
        if verbose:
            print("\t\tSelecting disk accretion")
        disk_ms_stable_start_ensemble_data = query_dict(
            ms_stable_start_ensemble_data, "disk", [0.5, 1.5]
        )
        disk_ms_stable_mass_ensemble_data = query_dict(
            ms_stable_mass_ensemble_data, "disk", [0.5, 1.5]
        )
        disk_ms_stable_time_ensemble_data = query_dict(
            ms_stable_time_ensemble_data, "disk", [0.5, 1.5]
        )

        # Get totals
        if verbose:
            print("\t\tGetting totals")
        total_disk_ms_stable_start_ensemble_data = get_recursive_sum(
            disk_ms_stable_start_ensemble_data
        )
        total_disk_ms_stable_mass_ensemble_data = get_recursive_sum(
            disk_ms_stable_mass_ensemble_data
        )
        total_disk_ms_stable_time_ensemble_data = get_recursive_sum(
            disk_ms_stable_time_ensemble_data
        )

        # Get fractions
        if verbose:
            print("\t\tGetting fractions")
        fraction_disk_ms_stable_start_ensemble_data = (
            total_disk_ms_stable_start_ensemble_data
            / total_ms_stable_start_ensemble_data
            if total_ms_stable_start_ensemble_data != 0
            else 0
        )
        fraction_disk_ms_stable_mass_ensemble_data = (
            total_disk_ms_stable_mass_ensemble_data / total_ms_stable_mass_ensemble_data
            if total_ms_stable_mass_ensemble_data != 0
            else 0
        )
        fraction_disk_ms_stable_time_ensemble_data = (
            total_disk_ms_stable_time_ensemble_data / total_ms_stable_time_ensemble_data
            if total_ms_stable_time_ensemble_data != 0
            else 0
        )

        # Store results
        if verbose:
            print("\t\tStoring results")
        result_dict["Z = {}".format(key)] = [
            fraction_disk_ms_stable_start_ensemble_data,
            fraction_disk_ms_stable_mass_ensemble_data,
            fraction_disk_ms_stable_time_ensemble_data,
        ]

    #
    column_names = [
        "Fraction disk at onset",
        "Fraction disk time weighted",
        "Fraction disk mass weighted",
    ]

    # put in dataframe:
    result_df = pd.DataFrame.from_dict(
        result_dict, columns=column_names, orient="index"
    )
    result_df = result_df.transpose()

    # #
    # caption = 'Fractions of stable disk mass transfer onto main-sequence stars for the fiducial runs for a range of metallicities (Z). Fraction MT disk at start and end are the fraction of all the stable mass transfer episodes onto main sequence stars weighted by their system probability. Fraction time MT through disk is the total probability weighted time spent transferring mass onto MS stars through a disk, normalized by the total onto MS via disk and direct accretion. Fraction mass MT through disk is the probability weighted mass transferred onto MS stars through a disk, normalized by the total onto MS via disk and direct accretion.'
    # label='table:disk_fractions_normal'

    #
    caption = caption
    label = label

    # Create latex string
    latex_string = result_df.to_latex(
        index=True, label=label, caption=caption, float_format="%.2f"
    )
    # print(latex_string)

    # Export
    with open(output_filename, "w") as output_filehandle:
        output_filehandle.write(latex_string)


# #
# population_datasets_sana = generate_ensemble_dataset_dict('/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions/')
# generate_table_disk_fractions(population_datasets_sana, 'test.tex', testing=True, label='', caption='', verbose=1)
