"""
Function to readout population settings and create a dictionary of settings for a single system
"""

import os
import json


def readout_settings_file_and_return_bse_options(file):
    """
    Function to readout a settings file and copy the used population settings as a dictionary to the new system
    """

    with open(file, "r") as f:
        settings = json.loads(f.read())

    return settings["population_settings"]["bse_options"]


if __name__ == "__main__":
    settings_file = "example_settings.json"
    readout_settings_file_and_return_bse_options(settings_file)
