"""
settings of the TZO project
"""

from grav_waves.settings import project_specific_settings

project_specific_settings["PPISN_prescription"] = 2
project_specific_settings["PPISN_additional_massloss"] = 0
project_specific_settings["PPISN_core_mass_range_shift"] = 0
project_specific_settings["david_tzo_logging"] = 1


LAMBDA_CE_prescription_dict = {
    -1: "LAMBDA_CE_DEWI_TAURIS",
    -2: "LAMBDA_CE_WANG_2016",
    -3: "LAMBDA_CE_POLYTROPE",
    -4: "LAMBDA_CE_KLENCKI_2020",
}
