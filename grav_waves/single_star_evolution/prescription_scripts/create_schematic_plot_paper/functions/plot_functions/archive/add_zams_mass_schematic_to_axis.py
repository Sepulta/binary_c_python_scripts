"""
Function to add ZAMS mass vs remnant mass, generalised to handle either variation
"""

import os
import numpy as np

import matplotlib as mpl

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.extra_plot_functions import (
    place_arrows,
    add_bracket,
)

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_utils import (
    get_df,
)

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_utils import (
    add_pre_SN_mass_plot,
    add_no_prescription_plot,
    add_old_prescription_plot,
    add_fiducial_plot,
    add_mass_removal_plot,
    add_core_mass_shift_plot,
    add_makeup_plot,
    calculate_min_max_zams_ppisn_masses,
    get_dataframes,
)

from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def add_zams_mass_schematic_to_axis(
    fig,
    ax,
    result_directory_variation,
    old_prescription_result_directory_root,
    no_prescription_result_directory_root=None,
    variation_name="mass_removal",
    add_preSN_mass_mass_removal=True,
    add_no_prescription_line=False,
    add_old_prescription_line=True,
    add_fiducial_line=True,
    add_variation_line=True,
    add_bracket_extra_mass_loss=True,
    create_shaded_regions=True,
    create_supernova_lines=True,
    dataset_used_for_regions="fiducial",
):
    """
    Function to add the ZAMS mass to remnant mass with extra ppisn mass removal plot to an axis
    """

    # Settings
    min_zams_mass = 0
    max_zams_mass = 250
    alpha_regions = 0.2

    ########################
    # Get metallicity values
    metallicity_value = float(
        os.path.basename(result_directory_variation).split("_Z")[-1]
    )

    ########################
    # Get the dataframes
    (
        old_prescription_df,
        fiducial_df,
        no_prescription_df,
        variation_df,
    ) = get_dataframes(
        old_prescription_result_directory_root,
        result_directory_variation,
        no_prescription_result_directory_root,
    )

    ########################
    # Get some masses
    (
        min_ppisn_zams,
        max_ppisn_zams,
        zams_mass_for_max_ppisn,
        fiducial_max_mass_ppisn,
        variation_max_mass_ppisn,
    ) = calculate_min_max_zams_ppisn_masses(fiducial_df, variation_df)

    #############################################
    # Plot masses

    ################
    # Pre-SN mass plot
    if add_preSN_mass_mass_removal:
        add_pre_SN_mass_plot(ax, fiducial_df)

    ################
    # No ppisn prescription plot
    if add_no_prescription_line:
        add_no_prescription_plot(ax, no_prescription_df)

    ################
    # old prescription mass plot
    if add_old_prescription_line:
        add_old_prescription_plot(ax, old_prescription_df)

    ################
    # fiducial mass plot
    if add_fiducial_line:
        add_fiducial_plot(ax, fiducial_df)

    # ################
    # # Mass removal plot
    # if add_mass_removed_mass_removal:
    #     add_mass_removal_plot(ax, variation_df)

    # ################
    # # Core mass shift plot
    # if add_mass_removed_mass_removal:
    #     add_core_mass_shift_plot(ax, variation_df)

    ################
    # Mass removal plot
    if add_variation_line:
        if variation_name == "mass_removal":
            label = r"New prescription - $\Delta$ $M_{\mathrm{Extra\ mass\ loss}}$"
        if variation_name == "core_mass_shift":
            label = r"New prescription with $M_{CO\ PPISN\ shifted}$"
        else:
            label = "New prescription with {} variation".format(variation_name)

        #
        ax.plot(
            variation_df["zams_mass"],
            variation_df["mass"],
            linestyle="--",
            alpha=0.8,
            label=label,
        )

        ################
        # Add bracket indicating the extra PPISN loss
        if add_bracket_extra_mass_loss:
            if variation_name == "mass_removal":
                # Get location for the brackets
                location_max_fiducial_mass = (
                    zams_mass_for_max_ppisn,
                    fiducial_max_mass_ppisn,
                )
                location_max_mass_removal_mass = (
                    zams_mass_for_max_ppisn,
                    variation_max_mass_ppisn,
                )
                location_startpoint = (
                    zams_mass_for_max_ppisn + 25,
                    (fiducial_max_mass_ppisn + variation_max_mass_ppisn) / 2,
                )

                # Plot the brackets
                fig, ax = add_bracket(
                    fig,
                    ax,
                    location_max_fiducial_mass,
                    location_max_mass_removal_mass,
                    location_startpoint,
                    -2,
                    linestyle="solid",
                    color="black",
                    linewidth=5,
                )

                # Add text
                ax.text(
                    location_startpoint[0] + 5,
                    location_startpoint[1] - 1,
                    s=r"$\Delta$ $M_{\mathrm{Extra\ mass\ loss}}$",
                    fontsize=20,
                )

    ################
    # Create shaded region under the main line that indicates which prescription
    if create_shaded_regions:
        if dataset_used_for_regions == "no_prescription":
            region_dataset = no_prescription_df
        if dataset_used_for_regions == "fiducial":
            region_dataset = fiducial_df
        if dataset_used_for_regions == "old_prescription":
            region_dataset = old_prescription_df
        if dataset_used_for_regions == "variation":
            region_dataset = variation_df

        # Get mass first ppisn
        first_ppisn_mass = max_zams_mass
        if dataset_used_for_regions != "no_prescription":
            first_ppisn_mass = region_dataset[region_dataset.sn_type == 21].iloc[0][
                "zams_mass"
            ]

        # Get the region for CC
        fryer_area_df = region_dataset[
            region_dataset.zams_mass < first_ppisn_mass
        ].copy(deep=True)

        #
        ax.fill_between(
            fryer_area_df["zams_mass"],
            fryer_area_df["mass"],
            y2=0,
            color="yellow",
            alpha=alpha_regions,
        )

        # Plot region of PPISN
        if dataset_used_for_regions != "no_prescription":
            ax.fill_between(
                region_dataset[region_dataset.zams_mass >= first_ppisn_mass][
                    "zams_mass"
                ],
                region_dataset[region_dataset.zams_mass >= first_ppisn_mass]["mass"],
                y2=0,
                color="pink",
                alpha=alpha_regions,
            )

        ################
        # Add lines on the top to indicate which supernova type
        if create_supernova_lines:
            y_value_sn_mechanism_line = fiducial_max_mass_ppisn + 2.5

            #################
            # Plot the CC SN
            # below_ppisn_area = region_dataset[region_dataset.zams_mass<min_ppisn_zams]

            min_below_ppisn_zams = fryer_area_df["zams_mass"].to_numpy().min()
            max_below_ppisn_zams = fryer_area_df["zams_mass"].to_numpy().max()

            size_between_below_ppisn = max_below_ppisn_zams - min_below_ppisn_zams
            midpoint_below_ppisn = (max_below_ppisn_zams + min_below_ppisn_zams) / 2

            #
            fig, ax = place_arrows(
                fig,
                ax,
                midpoint_below_ppisn,
                y_value_sn_mechanism_line,
                size_between_below_ppisn,
                text="CC",
            )

            #################
            # Plot the PPI SN
            if dataset_used_for_regions != "no_prescription":
                ppisn_area = region_dataset[
                    region_dataset.zams_mass >= first_ppisn_mass
                ][region_dataset.stellar_type == 14]

                min_ppisn_zams = ppisn_area["zams_mass"].to_numpy().min()
                max_ppisn_zams = ppisn_area["zams_mass"].to_numpy().max()
                size_between_ppisn = max_ppisn_zams - min_ppisn_zams
                midpoint_ppisn = (max_ppisn_zams + min_ppisn_zams) / 2

                #
                fig, ax = place_arrows(
                    fig,
                    ax,
                    midpoint_ppisn,
                    y_value_sn_mechanism_line,
                    size_between_ppisn,
                    text="PPISN + CC",
                )

    # Add makeup to plot
    add_makeup_plot(
        ax, min_zams_mass, max_zams_mass, fiducial_max_mass_ppisn, metallicity_value
    )

    return fig, ax
