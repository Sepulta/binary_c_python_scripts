"""
Utility functions for the plotting
"""

import numpy as np
import pandas as pd


SN_type_dict = {
    0: "None",
    1: "IA_He",
    2: "IA_ELD",
    3: "IA_CHAND",
    4: "AIC",
    5: "ECAP",
    6: "IA_He_Coal",
    7: "IA_CHAND_Coal",
    8: "NS_NS",
    9: "GRB_COLLAPSAR",
    10: "HeStarIa",
    11: "IBC",
    12: "II",
    13: "IIa",
    14: "WDKICK",
    15: "TZ",
    16: "AIC_BH",
    17: "BH_BH",
    18: "BH_NS",
    19: "IA_Hybrid_HeCOWD",
    20: "IA_Hybrid_HeCOWD_subluminous",
    21: "PPISN",
    22: "PISN",
    23: "PHDIS",
}


linestyle_tuple = [
    ("loosely dotted", (0, (1, 10))),
    ("dotted", (0, (1, 1))),
    ("densely dotted", (0, (1, 1))),
    ("loosely dashed", (0, (5, 10))),
    ("dashed", (0, (5, 5))),
    ("densely dashed", (0, (5, 1))),
    ("loosely dashdotted", (0, (3, 10, 1, 10))),
    ("dashdotted", (0, (3, 5, 1, 5))),
    ("densely dashdotted", (0, (3, 1, 1, 1))),
    ("dashdotdotted", (0, (3, 5, 1, 5, 1, 5))),
    ("loosely dashdotdotted", (0, (3, 10, 1, 10, 1, 10))),
    ("densely dashdotdotted", (0, (3, 1, 1, 1, 1, 1))),
]


def fill_missing_values(df, other_columns):
    """
    Function to fill the missing values and return an updated dataframe

    In some situations the model fails, and we have a hole in our grid. We want to replace this hole by NaN values.
    """

    unique_zams_mass_values = df["zams_mass"].sort_values().unique()
    unique_metallicity_values = df["metallicity"].sort_values().unique()

    # Check if there are metallicity values for which not all of the zams masses are registered/successfully evolved
    for met in unique_metallicity_values:
        # Find missing zams values
        cur_zams_values = set(df[df.metallicity == met]["zams_mass"])
        missing_zams_values = set(unique_zams_mass_values) - cur_zams_values

        # Create placeholder data
        if missing_zams_values:
            dummy_lines = []
            for zams_value in missing_zams_values:
                dummy_line = {
                    "metallicity": met,
                    "zams_mass": zams_value,
                    "log10metallicity": np.log10(met),
                    **{
                        col: np.nan
                        for col in other_columns
                        if not col == "log10metallicity"
                    },
                }
                dummy_lines.append(dummy_line)

            # Create dataframe
            placeholder_df = pd.DataFrame(dummy_lines)

            # Add dataframe to total dataframe
            df = pd.concat([df, placeholder_df], ignore_index=True)

    # It is possible that there are duplicate lines in the data. Somehow, not sure how, but apparently so.
    df = df.drop_duplicates()

    # Make sure the data is sorted again
    df = df.sort_values(by=["zams_mass", "log10metallicity"])

    return df
