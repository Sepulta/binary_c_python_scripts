"""
Described in Appendix B: https://arxiv.org/pdf/2110.01634.pdf
"""

from scipy.special import erf
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib as mpl

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

log10metallicity_values = np.array([-3, -2.5, -2.0, -1.5, -1])

# Get center of these
bins_log10metallicity_values = (
    log10metallicity_values[1:] + log10metallicity_values[:-1]
) / 2

# Add one to the left and one to the right:
bins_log10metallicity_values = np.array(
    [
        log10metallicity_values[0]
        + (log10metallicity_values[0] - bins_log10metallicity_values[0])
    ]
    + list(bins_log10metallicity_values)
)
bins_log10metallicity_values = np.array(
    list(bins_log10metallicity_values)
    + [
        log10metallicity_values[-1]
        + (log10metallicity_values[-1] - bins_log10metallicity_values[-1])
    ]
)

stepsizes_log10metallicity_values = np.diff(bins_log10metallicity_values)
print(stepsizes_log10metallicity_values)


quit()


def metallicity_distribution_vanSon21(Z, z, alpha, mu_0, mu_z, sigma_0, sigma_z):
    """
    Metallicity probabilty distribution (dP/dZ) from van Son 2021 (https://arxiv.org/pdf/2110.01634.pdf) based on a skewed log-normal (rather than a log-normal from Neijssel+19).
    """

    # omega term
    omega_term = sigma_0 * 10 ** (sigma_z * z)

    # XI term
    mean_metallicity = mu_0 * 10 ** (mu_z * z)
    beta = alpha / (np.sqrt(1 + alpha**2))
    phi = 0.5 * (1 + erf((beta * omega_term) / np.sqrt(2)))
    xi_term = np.log(mean_metallicity / (2 * phi)) * (-(omega_term**2)) / 2

    # Calculate the assymetric term
    assymetric_term = (0.5) * (
        1 + erf((alpha / np.sqrt(2)) * ((np.log(Z) - xi_term) / (omega_term)))
    )

    # Calculate the lognormal term
    lognormal_term = (1 / (omega_term * np.sqrt(2 * np.pi))) * np.exp(
        -0.5 * ((np.log(Z) - xi_term) / (omega_term)) ** 2
    )

    #
    dPdLnZ = 2 * lognormal_term * assymetric_term

    #
    dPdZ = (1 / Z) * dPdLnZ

    #
    return dPdZ


sigma_0 = 1.125
sigma_z = 0.048
mu_0 = 0.025
mu_z = -0.048
alpha = 0.2

dpdz = metallicity_distribution_vanSon21(0.02, 0, alpha, mu_0, mu_z, sigma_0, sigma_z)

# Set up function to plot the distribution:
z_array = np.arange(0.001, 10, 0.2)
Z_array = 10 ** np.arange(-8, 0, 0.05)  # Beware, we do this in log

mesh_z_array, mesh_Z_array = np.meshgrid(z_array, Z_array)

#####
# Set up the figure
fig = plt.figure(figsize=(20, 20))

#
gs = fig.add_gridspec(nrows=1, ncols=11)

ax = fig.add_subplot(gs[:, :9])

ax_cb = fig.add_subplot(gs[:, 10])

#
_ = ax.pcolormesh(
    z_array,
    Z_array,
    dpdz_mesh,
    # norm=norm,
    shading="auto",
    antialiased=True,
    rasterized=True,
)
ax.set_yscale("log")
plt.show()
