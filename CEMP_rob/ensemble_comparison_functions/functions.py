gimport os
import time

import matplotlib.pyplot as plt

import json

from functools import reduce  # forward compatibility for Python 3
import operator
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot

# function to provide a list that can act as a sequence of key selections
def getFromDict(dataDict, mapList):
    return reduce(operator.getitem, mapList, dataDict)

def show_differences_between_dicts(robs_ensemble, davids_ensemble, subkeys, normalize=True):
    """
    Function to show the difference between two dictionaries for any given sequence of nested keys
    """

    if normalize:
        normalizing_const = davids_ensemble['metadata']['total_probability_weighted_mass']
    else:
        normalizing_const = 1

    subdict_rob = getFromDict(robs_ensemble['ensemble'], subkeys)
    subdict_david = getFromDict(davids_ensemble['ensemble'], subkeys)

    number_keys_subdict_rob = [float(key) for key in subdict_rob.keys()]
    number_keys_subdict_david = [float(key) for key in subdict_david.keys()]

    print("For subdict {} in ensemble: ".format(str(''.join(['[{}]'.format(el) for el in subkeys]))))
    print('\tRobs: ', sorted(number_keys_subdict_rob))
    print('\tDavids:', sorted(number_keys_subdict_david))
    print("\n")

    not_in_david = set(number_keys_subdict_rob)-set(number_keys_subdict_david)
    not_in_rob = set(number_keys_subdict_david)-set(number_keys_subdict_rob)
    common_keys = set(number_keys_subdict_david)&set(number_keys_subdict_rob)

    print("Keys from robs dict not in davids dict:\n\t{}".format(not_in_david))
    print("Keys from robs dict not in davids dict:\n\t{}".format(not_in_rob))
    print("Common keys:\n\t{}".format(common_keys))
    print("\n")

    for key in sorted(list(common_keys)):
        chosen_key = str(key)
        value_rob = subdict_rob[chosen_key]
        value_david = subdict_david[chosen_key]/normalizing_const
        print("\nKey: {} {}".format(chosen_key, str(''.join(['["{}"]'.format(el) for el in subkeys]))))
        print("\tValue rob: {}".format(value_rob))
        print("\tValue david: {}".format(value_david))
        print("\tRatio rob/david: {}".format(value_rob/value_david))

def plot_differences_between_dicts(robs_ensemble, davids_ensemble, subkeys, plot_settings=None, to_float=True, normalize=True, print_info=False):
    """
    Function to plot the differences for a dict
    """

    if not plot_settings:
        plot_settings = {}

    # check if we want to normalize
    if normalize:
        normalizing_const = davids_ensemble['metadata']['total_probability_weighted_mass']
    else:
        normalizing_const = 1

    # Load the specific subdict
    subdict_rob = getFromDict(robs_ensemble['ensemble'], subkeys)
    subdict_david = getFromDict(davids_ensemble['ensemble'], subkeys)

    if to_float:
        number_keys_subdict_rob = [float(key) for key in subdict_rob.keys()]
        number_keys_subdict_david = [float(key) for key in subdict_david.keys()]
    else:
        number_keys_subdict_rob = subdict_rob.keys()
        number_keys_subdict_david = subdict_david.keys()

    # look for missing keys
    not_in_david = set(number_keys_subdict_rob)-set(number_keys_subdict_david)
    not_in_rob = set(number_keys_subdict_david)-set(number_keys_subdict_rob)
    common_keys = set(number_keys_subdict_david)&set(number_keys_subdict_rob)

    # aa
    ratios_common_keys = []
    diff_common_keys = []

    # Calculate ratios
    for key in sorted(list(common_keys)):
        chosen_key = str(key)
        value_rob = subdict_rob[chosen_key]
        value_david = subdict_david[chosen_key]/normalizing_const
        ratios_common_keys.append(value_rob/value_david)
        diff_common_keys.append(value_rob-value_david)

        if print_info:
            print("\nKey: {} {}".format(chosen_key, str(''.join(['["{}"]'.format(el) for el in subkeys]))))
            print("\tValue rob: {}".format(value_rob))
            print("\tValue david: {}".format(value_david))
            print("\tRatio rob/david: {}".format(value_rob/value_david))

    # Set up plotting routine
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(8, 1)
    gs.update(hspace=1) # set the spacing between axes.

    # Set up the axes for the merging dataframes
    axes_plot = fig.add_subplot(gs[:6, 0])
    axes_ratio = fig.add_subplot(gs[6:7, 0])
    axes_diff = fig.add_subplot(gs[7:8, 0])

    # Plot keys for each of the dicts
    axes_plot.plot(
        sorted(number_keys_subdict_rob),
        [subdict_rob[str(key)] for key in sorted(number_keys_subdict_rob)],
        'ro',
        label="Perl data"
    )
    axes_plot.plot(
        sorted(number_keys_subdict_david),
        [subdict_david[str(key)]/normalizing_const for key in sorted(number_keys_subdict_david)],
        'bs',
        label="Python data"
    )

    # Plot ratios
    axes_ratio.plot(sorted(list(common_keys)),
        ratios_common_keys,
        'bo',
        label='Ratio Perl/Python'
    )
    axes_ratio.plot(sorted(list(not_in_david)),
        [1 for el in not_in_david],
        'gD',
        label='Not in Python'
    )

    axes_ratio.plot(sorted(list(not_in_rob)),
        [1 for el in not_in_rob],
        'yD',
        label='Not in Perl',
    )

    axes_diff.plot(sorted(list(common_keys)),
        diff_common_keys,
        'bo',
        label='Perl - Python'
    )

    # Makeup
    fontsize=16
    axes_plot.set_ylabel("log10(Probability)", fontsize=fontsize)
    axes_plot.set_yscale('log')

    axes_ratio.set_yscale('log')
    axes_ratio.set_ylabel("log10(Ratio Perl/Python)", fontsize=fontsize)

    axes_diff.set_yscale('log')
    axes_diff.set_ylabel("log10(Diff Perl-Python)", fontsize=fontsize)

    #
    axes_plot.legend(loc=1, framealpha=0.5)
    axes_ratio.legend(loc=1, framealpha=0.5)

    #
    axes_plot.set_title("Values for both dictionaries", fontsize=18)

    plt.suptitle(str(''.join(['["{}"]'.format(el) for el in subkeys])), fontsize=24)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
