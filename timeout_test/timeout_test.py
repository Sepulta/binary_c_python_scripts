from contextlib import contextmanager
import signal
import time


class binary_cTimeoutException(Exception):
    """Base class for other exceptions"""

    pass


@contextmanager
def timeout(duration):
    def timeout_handler(signum, frame):
        raise binary_cTimeoutException(f"block timedout after {duration} seconds")

    signal.signal(signal.SIGALRM, timeout_handler)
    signal.alarm(duration)
    yield
    signal.alarm(0)


def sleeper(duration):
    time.sleep(duration)
    print("finished")


try:
    with timeout(2):
        sleeper(3)
except binary_cTimeoutException:
    print("yo")
