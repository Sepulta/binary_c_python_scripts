import os
import json
import time
import pickle
import sys

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import get_help_all, get_help, create_hdf5

## Quick script to get some output about which stars go supernova when.
def output_lines(output):
    """
    Function that outputs the lines that were recieved from the binary_c run.
    """
    return output.splitlines()


def parse_function(self, output):
    # extract info from the population instance
    # TODO: think about whether this is smart. Passing around this object might be an overkill

    # Get some information from the
    data_dir = self.custom_options["data_dir"]
    base_filename = self.custom_options["base_filename"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    seperator = " "

    # Create filename
    outfilename = os.path.join(data_dir, base_filename)

    result_header = [
        "zams_mass",
        "st_0",
        "st_1",
        "st_2",
        "st_3",
        "st_4",
        "st_5",
        "st_6",
        "st_7",
        "st_8",
        "st_9",
        "st_10",
        "st_11",
        "st_12",
        "st_13",
        "st_14",
        "st_15",
    ]

    if not os.path.exists(outfilename):
        with open(outfilename, "w") as f:
            f.write(seperator.join(result_header) + "\n")

    with open(outfilename, "a") as f:
        f.write(seperator.join(result_list) + "\n")


################################################################
# Set values for the grid


## Set values
test_pop = Population()


metallicity = 0.02
test_pop.set(
    # Physics options
    metallicity=metallicity,
    # Grid options
    data_dir=os.path.join(
        os.environ["BINARYC_DATA_ROOT"],
        "testing_python",
        "massloss_test",
        "massloss_{}".format(metallicity),
    ),
    base_filename="massloss_{}.dat".format(metallicity),
    verbose=1,
    amt_cores=2,
    binary=1,
    # parse_function=parse_function,
)

# Optionally: Add custom logging functionality
test_pop.set(
    C_logging_code="""
Printf("DAVID_CO %30.12e %g %g %g %d %g\\n",
    //
    stardata->model.time, // 1
    stardata->star[0].mass, //2
    stardata->previous_stardata->star[0].mass, //3
    stardata->star[0].pms_mass, //4
    stardata->star[0].stellar_type, //5
    stardata->model.probability //6
    );
"""
)

# Add grid variables
resolution = {"M_1": 10, "q": 10, "per": 40}

# Mass
test_pop.add_grid_variable(
    name="lnm1",
    longname="Primary mass",
    valuerange=[2, 150],
    resolution="{}".format(resolution["M_1"]),
    spacingfunc="const(math.log(2), math.log(150), {})".format(resolution["M_1"]),
    precode="M_1=math.exp(lnm1)",
    probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 150, -1.3, -2.3, -2.3)*M_1",
    dphasevol="dlnm1",
    parameter_name="M_1",
)

# Mass ratio
test_pop.add_grid_variable(
    name="q",
    longname="Mass ratio",
    condition='self.grid_options["binary"]==1',
    valuerange=["0.1/M_1", 1],
    resolution="{}".format(resolution["q"]),
    spacingfunc="const(0.1/M_1, 1, {})".format(resolution["q"]),
    probdist="flatsections(q, [{'min':0.1/M_1, 'max': 1.0, 'height': 1.0}])",
    precode="M_2= q * M_1",
    dphasevol="dq",
    parameter_name="M_2",
)

# Period
test_pop.add_grid_variable(
    name="logper",
    longname="log(Orbital_Period)",
    valuerange=[-2.0, 12.0],
    resolution="{}".format(resolution["per"]),
    spacingfunc="const(-2.0, 12.0, {})".format(resolution["per"]),
    precode="orbital_period = 10.0 ** logper\n\
    eccentricity = 0.0\n\
    separation=calc_sep_from_period(M_1, M_2, orbital_period)",
    probdist="gaussian(logper,4.8,2.3,-2.0,12.0)",
    dphasevol="dln10per",
    parameter_name="orbital_period",
)


start = time.time()


# quit()
# Export settings:
# test_pop.export_all_info(use_datadir=True)


# Evolve grid
test_pop.evolve_population_mp_chunks()

# Put data (and settings) in hdf5
# create_hdf5(test_pop.custom_options['data_dir'], 'massloss_{}.hdf5'.format(metallicity))

stop = time.time()
