"""
Script containin the plot function

TODO: use the binsize calculator of the ensembles
"""

import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from ensemble_functions.ensemble_functions import generate_bins_from_keys

from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.utility_functions import (
    fill_missing_values,
    SN_type_dict,
    linestyle_tuple,
)

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def plot_fallback_grid(datafile, plot_settings=None):
    """
    Function to plot the grid in metallicity and mass
    """

    # Open the data as a pandas dataframe
    df = pd.read_csv(datafile, header=0, sep="\s+")
    df["log10metallicity"] = np.log10(df["metallicity"])
    df = df.sort_values(by=["zams_mass", "log10metallicity"])
    other_columns = [
        col for col in df.columns if not col in ["zams_mass", "metallicity"]
    ]

    # Filter out effective_metallicity == 0:
    df = df[df.effective_metallicity > 0]

    # Fill missing values
    df = fill_missing_values(df, other_columns)

    # make plot
    unique_zams_mass_values = df["zams_mass"].sort_values().unique()
    unique_metallicity_values = df["metallicity"].sort_values().unique()
    unique_log10unique_metallicity_values = np.log10(unique_metallicity_values)

    #
    z_vals = (
        df["fallback_fraction"]
        .to_numpy()
        .reshape(
            len(unique_zams_mass_values), len(unique_log10unique_metallicity_values)
        )
    )
    sn_types = (
        df["sn_type"]
        .to_numpy()
        .reshape(
            len(unique_zams_mass_values), len(unique_log10unique_metallicity_values)
        )
    )

    #
    norm = colors.Normalize(
        vmin=z_vals[~np.isnan(z_vals)].min(), vmax=z_vals[~np.isnan(z_vals)].max()
    )

    ####
    #
    fig = plt.figure(figsize=(20, 20))

    #
    gs = fig.add_gridspec(nrows=1, ncols=12)

    # create axes
    ax = fig.add_subplot(gs[:, :10])
    ax_cb = fig.add_subplot(gs[:, 11])

    # Set up the colormesh plot
    _ = ax.pcolormesh(
        unique_metallicity_values,
        unique_zams_mass_values,
        z_vals,
        norm=norm,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        norm=norm,
    )
    cbar.ax.set_ylabel(r"Fallback fraction")

    #
    contourlevels = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
    linestyles = [el[1] for el in linestyle_tuple]
    _ = ax.contour(
        unique_metallicity_values,
        unique_zams_mass_values,
        z_vals,
        levels=contourlevels,
        linestyles=linestyles,
        colors="k",
    )
    for contour_i, _ in enumerate(contourlevels):
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [contourlevels[contour_i]] * 2,
            "black",
            linestyle=linestyles[contour_i],
        )

    # Set x-scale to log
    ax.set_xscale("log")
    ax.set_yscale(plot_settings["mass_scale"])

    ax.set_title("Fallback fraction")
    ax.set_xlabel("log10 Metallicity (Z)")
    ax.set_ylabel(r"ZAMS mass [M$_{\odot}$]")

    # Handle saving
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #
    plot_fallback_grid(
        "../../output/output_vanilla/data/output.dat", plot_settings={"show_plot": True}
    )
