"""
General plot function for the detector probabilities

TODO: Add actual readout of the convolution
"""

import os
import numpy as np

# Pdf generating routines
from PyPDF2 import PdfFileMerger
from david_phd_functions.plotting.pdf_functions import add_pdf_and_bookmark

#
from grav_waves.convolution.scripts.detection_probability_COMPAS.plot_functions import (
    plot_z_domain,
    plot_detection_probability,
)


def general_plot_routine(
    main_plot_dir, convolution_configuration, combined_output_pdf, verbose=0
):
    """
    Function to plot all the convolved quantities

    plots all above plots multiple times with different probability floors
    """

    format_types = ["pdf"]

    sensitivity = convolution_configuration["sensitivity"]
    snr_threshold = convolution_configuration["snr_threshold"]
    redshift_array = np.arange(
        convolution_configuration["min_interpolation_redshift"],
        convolution_configuration["max_interpolation_redshift"],
        convolution_configuration["redshift_stepsize"],
    )

    # Specify plot directory with more details
    plot_dir = os.path.join(
        os.path.abspath(main_plot_dir),
        "{}_snr_threshold={}".format(sensitivity, snr_threshold),
    )

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_list = []
    pdf_page_number = 0

    #
    print("Generating plots for detection probability")

    ###########################################################################
    # Detection probabilities at different redshifts
    max_plot_redshift = 3
    modulo_plot = 2
    for redshift_value in redshift_array[redshift_array < max_plot_redshift][::2]:
        print("\tPlotting plot_detection_probability for z={}".format(redshift_value))
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))

            plot_detection_probability(
                redshift_value,
                snr_threshold,
                sensitivity,
                plot_settings={
                    "output_name": os.path.join(
                        plot_dir,
                        format_type,
                        "detection_probability_{}.{}".format(
                            redshift_value, format_type
                        ),
                    ),
                    "block": False,
                    "show_plot": False,
                    "simulation_name": "z={} snr_threshold={}".format(
                        redshift_value, snr_threshold
                    ),
                    "antialiased": True,
                    "rasterized": True,
                    "runname": "plot_detection_probability",
                },
            )
        pdf_list.append(
            os.path.join(
                plot_dir, "pdf", "detection_probability_{}.pdf".format(redshift_value)
            )
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            pdf_list[-1],
            pdf_page_number,
            bookmark_text="detection_probability z={}".format(redshift_value),
        )

    ###########################################################################
    # Z domain and comoving volume
    print("\tPlotting plot_z_domain")
    for format_type in format_types:
        print("\t\tSaving as {}".format(format_type))

        plot_z_domain(
            sensitivity,
            plot_settings={
                "output_name": os.path.join(
                    plot_dir, format_type, "plot_z_domain.{}".format(format_type)
                ),
                "block": False,
                "show_plot": False,
                "simulation_name": "snr_threshold={}".format(snr_threshold),
                "antialiased": True,
                "rasterized": True,
                "runname": "plot_z_domain",
            },
        )
    pdf_list.append(os.path.join(plot_dir, "pdf", "plot_z_domain.pdf"))
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger, pdf_list[-1], pdf_page_number, bookmark_text="plot_z_domain"
    )

    # wrap up the pdf
    merger.write(combined_output_pdf)
    merger.close()

    print(
        "Finished generating plots for the detection probabilities. Wrote results to {}".format(
            combined_output_pdf
        )
    )
