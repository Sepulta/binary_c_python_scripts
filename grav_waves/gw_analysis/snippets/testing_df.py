import pandas as pd


test_data = [
    ["zero", 0, 0],
    ["one", 1, 0],
    ["other", 0, 1],
    ["both", 1, 1],
]


df = pd.DataFrame(test_data, columns=["which", "undergone_1", "undergone_2"])


# This will select them non-exclusive
print(df.query("undergone_1==1 or undergone_2==1"))

# This will make it exclusive
print(df.query("(undergone_1==1 | undergone_2==1) and not undergone_1==undergone_2"))


print(df[(df.undergone_1 == 1) | (df.undergone_2 == 1)])
