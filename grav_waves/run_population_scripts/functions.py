"""
Utility functions for the PPISN grav wave simulations
"""

import os
import json

from grav_waves.settings import BH_prescription_name_dict, PPISN_prescription_name_dict


def generate_simname(simname_base, variation_dict):
    """
    Function to generate the simname

    TODO: based on the default values, and the macro's of each of the variables we can probably create an automatic method for this.
    TODO: we can also automate this
    """

    # Add PPISN name
    simname = simname_base + "_{}".format(
        PPISN_prescription_name_dict[variation_dict["PPISN_prescription"]]
    )

    # Add BH prescription name
    simname = simname + "_{}".format(
        BH_prescription_name_dict[variation_dict["BH_prescription"]]
    )

    # Add value of extra mass loss
    simname = simname + (
        "_{}_PPISN_additional_massloss".format(
            variation_dict["PPISN_additional_massloss"]
        )
        if variation_dict["PPISN_additional_massloss"] > 0
        else ""
    )

    # Add value of core mass shift
    simname = simname + (
        "_{}_PPISN_core_mass_range_shift".format(
            variation_dict["PPISN_core_mass_range_shift"]
        )
        if variation_dict["PPISN_core_mass_range_shift"] != 0
        else ""
    )

    # Add other variations:
    # Alpha_ce
    if "alpha_ce" in variation_dict:
        simname = simname + (
            "_alpha_ce_multiplier_{}".format(variation_dict["alpha_ce"])
            if variation_dict["alpha_ce"] != 1
            else ""
        )

    # WR wind
    if "wind_type_multiplier_4" in variation_dict:
        simname = simname + (
            "_wind_type_multiplier_WR_{}".format(
                variation_dict["wind_type_multiplier_4"]
            )
            if variation_dict["wind_type_multiplier_4"] != 1
            else ""
        )

    # max mass
    if "max_mass" in variation_dict:
        simname = simname + (
            "_max_mass_{}".format(variation_dict["max_mass"])
            if variation_dict["max_mass"] != 300
            else ""
        )

    # Thermal rate multiplier
    if "accretion_limit_thermal_multiplier" in variation_dict:
        simname = simname + (
            "_accretion_limit_thermal_multiplier_{}".format(
                variation_dict["accretion_limit_thermal_multiplier"]
            )
            if variation_dict["accretion_limit_thermal_multiplier"] != 10
            else ""
        )

    # Nonconservative gamma
    if "nonconservative_angmom_gamma" in variation_dict:
        simname = simname + (
            "_nonconservative_angmom_gamma_{}".format(
                variation_dict["nonconservative_angmom_gamma"]
            )
            if variation_dict["nonconservative_angmom_gamma"] != -2
            else ""
        )

    # Nonconservative gamma
    if "fixed_beta_mt" in variation_dict:
        simname = simname + (
            "_fixed_beta_mt_{}".format(variation_dict["fixed_beta_mt"])
            if variation_dict["fixed_beta_mt"] != -1
            else ""
        )
    # sn_kick_dispersion_II
    if "sn_kick_dispersion_II" in variation_dict:
        simname = simname + (
            "_sn_kick_dispersion_II_{}".format(variation_dict["sn_kick_dispersion_II"])
            if variation_dict["sn_kick_dispersion_II"] != 265
            else ""
        )
    # sn_kick_dispersion_IBC
    if "sn_kick_dispersion_IBC" in variation_dict:
        simname = simname + (
            "sn_kick_dispersion_IBC_{}".format(variation_dict["sn_kick_dispersion_IBC"])
            if variation_dict["sn_kick_dispersion_IBC"] != 265
            else ""
        )
    # accretion_limit_eddington_steady_multiplier
    if "accretion_limit_eddington_steady_multiplier" in variation_dict:
        simname = simname + (
            "_accretion_limit_eddington_steady_multiplier_{}".format(
                variation_dict["accretion_limit_eddington_steady_multiplier"]
            )
            if variation_dict["accretion_limit_eddington_steady_multiplier"] != 1
            else ""
        )
    # slope IMF
    if "high_end_IMF_slope" in variation_dict:
        simname = simname + (
            "_high_end_IMF_slope_{}".format(variation_dict["high_end_IMF_slope"])
            if variation_dict["high_end_IMF_slope"] != -2.3
            else ""
        )
    # max_log10_orbital_period
    if "max_log10_orbital_period" in variation_dict:
        simname = simname + (
            "_max_log10_orbital_period_{}".format(
                variation_dict["max_log10_orbital_period"]
            )
            if variation_dict["max_log10_orbital_period"] != -5.5
            else ""
        )

    #
    return simname


def add_distribution_grid_variables(
    population_object,
    resolution,
    max_mass=300,
    high_end_IMF_slope=-2.3,
    max_log10_orbital_period=5.5,
):
    """
    Function to add the distribution grid variables to the code
    """

    ############
    ## Setting grids
    population_object.add_grid_variable(
        name="lnm1",
        longname="Primary mass",
        valuerange=[7.5, max_mass],
        samplerfunc="const(math.log(7.5), math.log({}), {})".format(
            max_mass, resolution["M_1"]
        ),
        precode="M_1=math.exp(lnm1)",
        probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, {}, -1.3, -2.3, {})*M_1".format(
            max_mass + 1, high_end_IMF_slope
        ),
        dphasevol="dlnm1",
        parameter_name="M_1",
        condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
    )
    population_object.add_grid_variable(
        name="q",
        longname="Mass ratio",
        valuerange=["0.1/M_1", 1],
        samplerfunc="const(0.1/M_1, 1, {})".format(resolution["q"]),
        probdist="flatsections(q, [{'min': 0.1/M_1, 'max': 1.0, 'height': 1}])",
        dphasevol="dq",
        precode="M_2 = q * M_1",
        parameter_name="M_2",
        condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
    )
    population_object.add_grid_variable(
        name="log10per",  # in days
        longname="log10(Orbital_Period)",
        valuerange=[0.15, max_log10_orbital_period],
        samplerfunc="const(0.15, {}, {})".format(
            max_log10_orbital_period, resolution["per"]
        ),
        precode="""orbital_period = 10** log10per
sep = calc_sep_from_period(M_1, M_2, orbital_period)
sep_min = calc_sep_from_period(M_1, M_2, 10**0.15)
sep_max = calc_sep_from_period(M_1, M_2, 10**{})""".format(
            max_log10_orbital_period
        ),
        probdist="sana12(M_1, M_2, sep, orbital_period, sep_min, sep_max, math.log10(10**0.15), math.log10(10**{}), -0.55)".format(
            max_log10_orbital_period
        ),
        parameter_name="orbital_period",
        dphasevol="dlog10per",
    )

    return population_object


def add_distribution_grid_variables_single(
    population_object,
    resolution,
    resolution_multiplication,
    max_mass=300,
    high_end_IMF_slope=-2.3,
):
    """
    Function to add the distribution grid variables to the code
    """

    ############
    ## Setting grids
    population_object.add_grid_variable(
        name="lnm1",
        longname="Primary mass",
        valuerange=[7.5, max_mass],
        samplerfunc="const(math.log(7.5), math.log({}), {})".format(
            max_mass, resolution["M_1"] * resolution_multiplication
        ),
        precode="M_1=math.exp(lnm1)",
        probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, {}, -1.3, -2.3, {})*M_1".format(
            max_mass + 1, high_end_IMF_slope
        ),
        dphasevol="dlnm1",
        parameter_name="M_1",
        condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
    )

    return population_object


def read_simulation_dict(simname_dir):
    """
    Function to read out and return the contents of the simulation_dict.json file in the simname_dir
    """

    with open(os.path.join(simname_dir, "simulation_dict.json"), "r") as f:
        simulation_dict = json.loads(f.read())

    return simulation_dict


def combine_resultfiles(
    data_dir,
    basename,
    combined_name,
    check_duplicates_and_all_present=False,
    remove_individual_files=False,
):
    """
    Function to combine the individual result files
    """

    full_path_combined = os.path.join(data_dir, combined_name)

    ####################
    # Remove if it already exists
    if os.path.exists(full_path_combined):
        os.remove(full_path_combined)

    full_path_individual_list = []
    for file in os.listdir(data_dir):
        if file.startswith(basename):
            full_path_individual_list.append(os.path.join(data_dir, file))

    for full_path_individual in full_path_individual_list:
        # Loop over file and add lines together
        with open(full_path_individual) as f:
            line = f.readline()

            # Check if the file doesnt exist yet. If not them write the headerline.
            if not os.path.exists(full_path_combined):
                with open(full_path_combined, "w") as output_f:
                    output_f.write(line)

            # Write the rest of the data
            with open(full_path_combined, "a") as output_f:
                for line in f:
                    output_f.write(line)

    ####################
    # Check whether we have duplicates
    if check_duplicates_and_all_present:
        already_found = []

        # Check duplicates
        with open(full_path_combined, "r") as f:
            line = f.readline()

            for line in f:
                if not line in already_found:
                    already_found.append(line)
                else:
                    print("FOUND DUPLICATE!")

        # Check if they're all present:
        for full_path_individual in full_path_individual_list:
            with open(full_path_individual, "r") as indiv_f:
                line = indiv_f.readline()

                for line in indiv_f:
                    if not line in already_found:
                        print("CANT FIND MY LINE")

    ####################
    # Remove files
    if remove_individual_files:
        for full_path_individual in full_path_individual_list:
            os.remove(full_path_individual)
