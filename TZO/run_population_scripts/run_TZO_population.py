"""
Main script to run the TZO population

The variations that are sensible to make here are:
- mass transfer/outflow method

Things to study are:
- distribution of envelope masses
- initial distribution that leads to interesting dataset

Tasks:
- extend with ensemble?
"""

import os
import numpy as np

from TZO.run_population_scripts.run_all_function import run_all

##############################################
# Settings for the script
run_simulations = True  # Switch to stop the backup and running of the simulation
backup_if_data_exists = True
combine_all_resultfiles = True
root_result_dir = os.path.join(os.environ["BINARYC_DATA_ROOT"], "TZO")

##
# Defaults for the population object
num_cores = 8
evolution_splitting = 0
evolution_splitting_maxdepth = 0
evolution_splitting_sn_n = 0

# ###########
# # high res settings
# num_cores                           = 20
# evolution_splitting                 = 1
# evolution_splitting_sn_n            = 4
# evolution_splitting_maxdepth        = 1
# metallicity_values                  = 10**np.linspace(-4, -1.7, 8)
# resolution                          = {'M_1': 100, 'q': 100, 'per': 100}
# simname_format                      = 'HIGH_RES'

# ###########
# # mid res settings
# evolution_splitting                 = 1
# evolution_splitting_sn_n            = 10
# evolution_splitting_maxdepth        = 1
# metallicity_values                  = 10**np.linspace(-3, -1.7, 4)
# resolution                          = {'M_1': 50, 'q': 50, 'per': 50}
# simname_base                        = 'MID_RES'

# ###########
# # low res settings
# # Settings for the evolution splitting
# evolution_splitting                 = 1
# evolution_splitting_sn_n            = 2
# evolution_splitting_maxdepth        = 1
# metallicity_values                  = 10**np.linspace(-3, -1.7, 4)
# resolution                          = {'M_1': 25, 'q': 25, 'per': 40}
# simname_base                        = 'LOW_RES'

###########
# test res settings
metallicity_values = [0.01]
resolution = {"M_1": 10, "q": 10, "per": 10}
simname_base = "TEST_RES"

#############
# specific local settings
local_population_settings = {
    "num_cores": num_cores,
    "evolution_splitting": evolution_splitting,
    "evolution_splitting_sn_n": evolution_splitting_sn_n,
    "evolution_splitting_maxdepth": evolution_splitting_maxdepth,
}

##############
# Default settings
run_all_extra_arguments = {
    "run_simulations": run_simulations,
    "backup_if_data_exists": backup_if_data_exists,
    "combine_all_resultfiles": combine_all_resultfiles,
    "root_result_dir": root_result_dir,
    "simname_base": simname_base,
    "metallicity_values": metallicity_values,
    "local_population_settings": local_population_settings,
    "resolution": resolution,
}

##############
# fiducial settings
variation_dict_fiducial = {"lambda_ce": -1}

##
# Fiducial run
variation_dict = variation_dict_fiducial
run_all(variation_dict=variation_dict, **run_all_extra_arguments)
