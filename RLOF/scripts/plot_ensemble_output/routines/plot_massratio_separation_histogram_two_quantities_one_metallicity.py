"""
Function to plot the mass weighted and time weighted distribution of racc and qacc for stable mass transfer onto MS via disks.

The results are normalised by the total amount of stable MT onto MS stars
"""

import os
import json
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

from ensemble_functions.ensemble_functions import (
    get_recursive_sum,
    merge_all_subdicts,
    return_keys_bins_and_bincenters,
    flatten_data_ensemble2d,
    query_dict,
    recursively_merge_all_subdicts,
    merge_recursive_until_stop,
)
from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)

from scipy.ndimage import gaussian_filter

from grav_waves.settings import convolution_settings


def plot_massratio_separation_histogram_two_quantities_one_metallicity(
    dataset, testing, verbose=0, global_query_dict=None, plot_settings=None
):
    """
    Function to plot the mass weighted and time weighted distribution of racc and qacc for stable mass transfer onto MS via disks.

    The results are normalised by the total amount of stable MT onto MS stars
    """

    if not plot_settings:
        plot_settings = {}

    if verbose:
        print(
            "Running plot_massratio_separation_histogram_rlof_cases_one_metallicity for Z = {}\n\toutput: {}".format(
                dataset["metallicity"], os.path.abspath(plot_settings["output_name"])
            )
        )

        if global_query_dict:
            print(
                "\twith global query: {}".format(global_query_dict["global_query_name"])
            )
            print(
                "\t\tqueries: {}".format(
                    ", ".join(
                        [
                            "{} between {}".format(
                                query["parameter_key"], query["value_range"]
                            )
                            for query in global_query_dict["query_list"]
                        ]
                    )
                )
            )

    # Get the correct ensemble data from the dict
    if verbose:
        print("\t\tLoading data")
    probability_to_yield_conversion_factor = (
        convolution_settings["binary_fraction"] * convolution_settings["mass_in_stars"]
    )
    ensemble_data = get_ensemble_data(
        dataset,
        testing,
        probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
    )["RLOF"]["fraction_separation_massratio"]

    if global_query_dict:
        if verbose:
            print("\tPerforming queries")
        for query in global_query_dict["query_list"]:
            if verbose:
                print(
                    "\t\tPerforming query {}".format(
                        "{} between {}".format(
                            query["parameter_key"], query["value_range"]
                        )
                    )
                )
            ensemble_data = query_dict(
                ensemble_data, query["parameter_key"], query["value_range"]
            )

    # select mass and time weighted data
    if verbose:
        print("\t\tSelecting mass and time weighted data")
    mass_subensemble = ensemble_data["mass"]
    time_subensemble = ensemble_data["time"]

    # Get stable MT
    if verbose:
        print("\t\tSelecting stable MT")
    stable_mass_subensemble = query_dict(mass_subensemble, "stable", [-0.5, 0.5])
    stable_time_subensemble = query_dict(time_subensemble, "stable", [-0.5, 0.5])

    # Get MT onto MS
    if verbose:
        print("\t\tSelecting MT onto MS")
    ms_stable_mass_subensemble = query_dict(
        stable_mass_subensemble, "st_acc", [0.5, 1.5]
    )
    ms_stable_time_subensemble = query_dict(
        stable_time_subensemble, "st_acc", [0.5, 1.5]
    )

    total_ms_stable_mass_subensemble = get_recursive_sum(ms_stable_mass_subensemble)
    total_ms_stable_time_subensemble = get_recursive_sum(ms_stable_time_subensemble)

    # Get MS via disk
    if verbose:
        print("\t\tSelecting disk MT")
    disk_ms_stable_mass_subensemble = query_dict(
        ms_stable_mass_subensemble, "disk", [0.5, 1.5]
    )
    disk_ms_stable_time_subensemble = query_dict(
        ms_stable_time_subensemble, "disk", [0.5, 1.5]
    )

    if verbose:
        print("\t\tMerging until one-to-last parameter")
    merged_disk_ms_stable_mass_subensemble = merge_recursive_until_stop(
        disk_ms_stable_mass_subensemble, "r_acc_sep"
    )
    merged_disk_ms_stable_time_subensemble = merge_recursive_until_stop(
        disk_ms_stable_time_subensemble, "r_acc_sep"
    )

    # Get bins and stuff
    if verbose:
        print("\t\tGetting the bins")
    (
        log10_fraction_accretor_separation_keys,
        bins_log10_accretor_separation,
        bincenters_log10_accretor_separation,
    ) = return_keys_bins_and_bincenters(
        merged_disk_ms_stable_mass_subensemble, "r_acc_sep"
    )
    (
        log10_massratio_keys,
        bins_log10_massratio,
        bincenters_log10_massratio,
    ) = return_keys_bins_and_bincenters(
        merge_all_subdicts(merged_disk_ms_stable_mass_subensemble["r_acc_sep"]),
        "q_acc_don",
    )

    # Set up dict to store the huistograms in
    if verbose:
        print("\t\tFlattening data")
    flattenend_disk_ms_stable_mass_subensemble = flatten_data_ensemble2d(
        merged_disk_ms_stable_mass_subensemble, "r_acc_sep", "q_acc_don"
    )
    flattenend_disk_ms_stable_time_subensemble = flatten_data_ensemble2d(
        merged_disk_ms_stable_time_subensemble, "r_acc_sep", "q_acc_don"
    )

    # Create histograms
    if verbose:
        print("\t\tCreating histograms")
    hist_data_disk_ms_stable_mass_subensemble, _, _ = np.histogram2d(
        flattenend_disk_ms_stable_mass_subensemble[0],
        flattenend_disk_ms_stable_mass_subensemble[1],
        bins=[bins_log10_accretor_separation, bins_log10_massratio],
        weights=flattenend_disk_ms_stable_mass_subensemble[2],
    )
    hist_data_disk_ms_stable_time_subensemble, _, _ = np.histogram2d(
        flattenend_disk_ms_stable_time_subensemble[0],
        flattenend_disk_ms_stable_time_subensemble[1],
        bins=[bins_log10_accretor_separation, bins_log10_massratio],
        weights=flattenend_disk_ms_stable_time_subensemble[2],
    )

    # Normalize
    if verbose:
        print("\t\tNormalising histograms")
    normed_hist_data_disk_ms_stable_mass_subensemble = (
        hist_data_disk_ms_stable_mass_subensemble / total_ms_stable_mass_subensemble
        if total_ms_stable_mass_subensemble != 0
        else np.zeros(hist_data_disk_ms_stable_mass_subensemble.shape)
    )
    normed_hist_data_disk_ms_stable_time_subensemble = (
        hist_data_disk_ms_stable_time_subensemble / total_ms_stable_time_subensemble
        if total_ms_stable_time_subensemble != 0
        else np.zeros(hist_data_disk_ms_stable_time_subensemble.shape)
    )

    # apply gaussian blur
    if verbose:
        print("\t\tApplying gaussian blur")
    normed_hist_data_disk_ms_stable_mass_subensemble = gaussian_filter(
        normed_hist_data_disk_ms_stable_mass_subensemble, sigma=2
    )
    normed_hist_data_disk_ms_stable_time_subensemble = gaussian_filter(
        normed_hist_data_disk_ms_stable_time_subensemble, sigma=2
    )
    combined = (
        normed_hist_data_disk_ms_stable_mass_subensemble
        + normed_hist_data_disk_ms_stable_time_subensemble
    )
    global_max_val = np.max(combined)

    ######################
    # Set up figure
    if verbose:
        print("\t\tPlotting figure")
    fig = plt.figure(figsize=(24, 10))

    #
    gs = fig.add_gridspec(nrows=1, ncols=8)

    # create axes
    ax_disk_ms_stable_mass = fig.add_subplot(gs[0, 0:3])
    ax_disk_ms_stable_time = fig.add_subplot(gs[0, 3:6])

    ax_invisible = fig.add_subplot(gs[0, :6], frame_on=False)
    ax_invisible.set_xticks([])
    ax_invisible.set_yticks([])

    # Create colorbar
    ax_cb = fig.add_subplot(gs[0, 7])

    # Remove y-ticks
    ax_disk_ms_stable_time.set_yticklabels([])

    # Set titles
    ax_disk_ms_stable_mass.set_title("Mass")
    ax_disk_ms_stable_time.set_title("Time")

    #
    ax_invisible.set_xlabel("")
    ax_disk_ms_stable_mass.set_ylabel(
        r"log$_{10}$($M_{\mathrm{acc}}$/$M_{\mathrm{don}}$)"
    )
    ax_invisible.set_xlabel(r"log$_{10}$($R_{\mathrm{acc}}$ / $a$)", labelpad=50)

    # Make meshgrid of bins
    X, Y = np.meshgrid(bincenters_log10_accretor_separation, bincenters_log10_massratio)

    DIFF_MAX_THRESHOLD = 5

    ######################
    # Plot the data

    #
    hist_disk_ms_stable_mass_subensemble = ax_disk_ms_stable_mass.pcolormesh(
        X,
        Y,
        normed_hist_data_disk_ms_stable_mass_subensemble.T,
        norm=colors.LogNorm(
            vmin=10 ** (np.log10(global_max_val) - DIFF_MAX_THRESHOLD),
            vmax=global_max_val,
        ),
        shading="auto",
        antialiased=True,
        rasterized=True,
    )
    hist_disk_ms_stable_time_subensemble = ax_disk_ms_stable_time.pcolormesh(
        X,
        Y,
        normed_hist_data_disk_ms_stable_time_subensemble.T,
        norm=colors.LogNorm(
            vmin=10 ** (np.log10(global_max_val) - DIFF_MAX_THRESHOLD),
            vmax=global_max_val,
        ),
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    #
    contour_levels = [1e-5, 1e-4, 0.5 * 1e-3]
    CS_disk_ms_stable_mass = ax_disk_ms_stable_mass.contour(
        X,
        Y,
        normed_hist_data_disk_ms_stable_mass_subensemble.T,
        levels=contour_levels,
        colors="k",
        linestyles=["solid", "dashed", "-."],
    )
    CS_disk_ms_stable_time = ax_disk_ms_stable_time.contour(
        X,
        Y,
        normed_hist_data_disk_ms_stable_time_subensemble.T,
        levels=contour_levels,
        colors="k",
        linestyles=["solid", "dashed", "-."],
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        cmap=matplotlib.cm.viridis,
        norm=matplotlib.colors.LogNorm(
            vmin=10 ** (np.log10(global_max_val) - DIFF_MAX_THRESHOLD),
            vmax=global_max_val,
        ),
        extend="min",
    )
    cbar.ax.set_ylabel(
        "Fraction of all stable mass-\ntransfer onto main-sequence\nstars through disks"
    )

    fig.tight_layout()

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


# population_datasets_sana = generate_ensemble_dataset_dict('/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions/')
# plot_massratio_separation_histogram_two_quantities_one_metallicity(population_datasets_sana['0.02'], testing=True, verbose=1, plot_settings={'show_plot': True, 'output_name': 'test.pdf'})
