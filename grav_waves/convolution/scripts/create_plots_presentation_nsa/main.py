"""
Main script to create presentation plots for presentation at NSA symposium 2022-04-14

Plots included:
- [ ] plot with distribution for fiducial simulation
- [ ] plot with distribution for fiducial + mass loss variation
- [ ] plot with distribution for fiducial + core mass shift variation

- [ ] plot with 3 panels: star formation distribution, merger times at z=0 and distribution of primary masses for un-convolved systems
- [ ] plot with 3 panels: star formation distribution, merger times at z=0 and distribution of primary masses for un-convolved systems

"""

import numpy as np
from plot_functions.plot_panel_primary_mass_distribution import (
    plot_primary_mass_distribution,
)
from plot_functions.plot_panel_sfr_unconvolved import plot_sfr_unconvolved

# Define datasets
fiducial_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/without_detection_probability/rebinned_convolution_results.h5"

ten_extra_massloss_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_10_EXTRA_MASSLOSS_PPISN/convolution_results/without_detection_probability/rebinned_convolution_results.h5"
twenty_extra_massloss_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_20_EXTRA_MASSLOSS_PPISN/convolution_results/without_detection_probability/rebinned_convolution_results.h5"

five_core_mass_shift_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-5_CORE_MASS_SHIFT/convolution_results/without_detection_probability/rebinned_convolution_results.h5"
ten_core_mass_shift_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-10_CORE_MASS_SHIFT/convolution_results/without_detection_probability/rebinned_convolution_results.h5"

#### Plot primary mass fiducial only
if 0:
    dataset_file_dict = {
        "fiducial": fiducial_dataset,
    }

    alpha_dict = {
        "fiducial": 1,
    }

    #
    plot_primary_mass_distribution(
        dataset_file_dict=dataset_file_dict,
        alpha_dict=alpha_dict,
        rate_type="merger_rate",
        dco_type="bhbh",
        num_procs=5,
        bootstraps=0,
        bins=np.arange(0, 100, 1),
        plot_settings={
            "show_plot": False,
            "output_name": "output/plot_primary_mass_distribution.pdf",
            "simulation_name": "test",
            "antialiased": True,
            "rasterized": True,
            "runname": "plot_primary_mass_distribution",
        },
    )

#### Plot primary mass extra mass loss
if 0:
    dataset_file_dict = {
        "fiducial": fiducial_dataset,
        "Ten Msun extra loss": ten_extra_massloss_dataset,
        "Twenty Msun extra loss": twenty_extra_massloss_dataset,
    }

    alpha_dict = {
        "fiducial": 1,
        "Ten Msun extra loss": 1,
        "Twenty Msun extra loss": 1,
    }

    #
    plot_primary_mass_distribution(
        dataset_file_dict=dataset_file_dict,
        alpha_dict=alpha_dict,
        rate_type="merger_rate",
        dco_type="bhbh",
        num_procs=5,
        bootstraps=0,
        bins=np.arange(0, 100, 1),
        plot_settings={
            "show_plot": False,
            "output_name": "output/plot_primary_mass_distribution_extra_mass_loss.pdf",
            "simulation_name": "test",
            "antialiased": True,
            "rasterized": True,
            "runname": "plot_primary_mass_distribution with extra mass loss sets",
        },
    )

# plot extra mass loss with highlight
if 1:
    dataset_file_dict = {
        "fiducial": fiducial_dataset,
        "Ten Msun extra loss": ten_extra_massloss_dataset,
        "Twenty Msun extra loss": twenty_extra_massloss_dataset,
    }

    alpha_dict = {
        "fiducial": 0.1,
        "Ten Msun extra loss": 1,
        "Twenty Msun extra loss": 0.1,
    }

    #
    plot_primary_mass_distribution(
        dataset_file_dict=dataset_file_dict,
        alpha_dict=alpha_dict,
        rate_type="merger_rate",
        dco_type="bhbh",
        num_procs=5,
        bootstraps=0,
        bins=np.arange(0, 100, 1),
        plot_settings={
            "show_plot": False,
            "output_name": "output/plot_primary_mass_distribution_extra_mass_loss_highlight.pdf",
            "simulation_name": "test",
            "antialiased": True,
            "rasterized": True,
            "runname": "plot_primary_mass_distribution with extra mass loss sets",
        },
    )

####
# Plot the core mass shift
if 0:
    dataset_file_dict = {
        "fiducial": fiducial_dataset,
        "Five Msun core mass shift": five_core_mass_shift_dataset,
        "Ten Msun Core mass shift": ten_core_mass_shift_dataset,
    }

    alpha_dict = {
        "fiducial": 1,
        "Five Msun core mass shift": 1,
        "Ten Msun Core mass shift": 1,
    }

    #
    plot_primary_mass_distribution(
        dataset_file_dict=dataset_file_dict,
        alpha_dict=alpha_dict,
        rate_type="merger_rate",
        dco_type="bhbh",
        num_procs=5,
        bootstraps=0,
        bins=np.arange(0, 100, 1),
        plot_settings={
            "show_plot": False,
            "output_name": "output/plot_primary_mass_distribution_mass_shift.pdf",
            "simulation_name": "test",
            "antialiased": True,
            "rasterized": True,
            "runname": "plot_primary_mass_distribution with core mass shift sets",
        },
    )

# Plot core mass shift with highlight
if 0:
    alpha_dict = {
        "fiducial": 0.1,
        "Five Msun core mass shift": 0.1,
        "Ten Msun Core mass shift": 1,
    }

    #
    plot_primary_mass_distribution(
        dataset_file_dict=dataset_file_dict,
        alpha_dict=alpha_dict,
        rate_type="merger_rate",
        dco_type="bhbh",
        num_procs=5,
        bootstraps=0,
        bins=np.arange(0, 100, 1),
        plot_settings={
            "show_plot": False,
            "output_name": "output/plot_primary_mass_distribution_mass_shift_highlight.pdf",
            "simulation_name": "test",
            "antialiased": True,
            "rasterized": True,
            "runname": "plot_primary_mass_distribution with core mass shift sets",
        },
    )
