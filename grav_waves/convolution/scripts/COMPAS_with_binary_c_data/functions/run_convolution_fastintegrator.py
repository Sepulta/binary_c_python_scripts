"""
Function to run the compas convolution via the FastIntegrator
"""

import os
import sys
import pickle
import shutil
import json

import h5py
import numpy as np

import astropy.units as u
from astropy.cosmology import WMAP9 as cosmology
from scipy import stats
from scipy.optimize import newton
import warnings

# Import COMPAS root directory and set data
compasRootDir = "/home/david/projects/binary_c_root/other_codes/COMPAS/utils"
imageDir = compasRootDir + "/postProcessing/Tutorial/media/"

# Import COMPAS specific scripts
sys.path.append(compasRootDir + "/PythonScripts/CosmicIntegration/")
import ClassCOMPAS, ClassMSSFR, ClassCosmicIntegrator
import FastCosmicIntegration as FCI
import selection_effects

from grav_waves.settings import logger


def quantity_object_to_dict(quantity_object):
    """
    Function to turn astropy quantity to a dict representation
    """

    return {"value": quantity_object.value, "unit": quantity_object.unit.to_string()}


def quantity_dict_to_object(quantity_dict):
    """
    Function to turn astropy dict to a quantity object
    """

    return quantity_dict["value"] * u.Unit(quantity_dict["unit"])


def FCI_json_serializer(obj):
    """
    Custom serialiser
    """

    if isinstance(obj, u.Quantity):
        return quantity_object_to_dict(obj)

    try:
        string_version = str(obj)
        return string_version
    except:
        raise TypeError(
            "Unserializable object {} of type {}. Attempted to convert to string but that failed.".format(
                obj, type(obj)
            )
        )


def run_convolution_fastintegrator(source_filename, target_filename):
    """
    Function to run the fastintegrator of COMPAS
    """

    FastCosmicIntegrator_arguments = {
        # For what DCO would you like the rate?  options: ALL, BHBH, BHNS NSNS
        "dco_type": "BBH",  # for now lets to BBH only
        "weight_column": "Probability",  # I'll configure this to be my probability column
        #
        "merges_hubble_time": True,  # Yes lets only include merging in HT
        "pessimistic_CEE": True,  # No HG CE donors survive
        "no_RLOF_after_CEE": True,  # ?RLOF after CE is merger
        # Options for the redshift evolution
        "max_redshift": 10,  # For now lets just do to
        "max_redshift_detection": 2.0,  # idem
        "redshift_step": 0.1,  # Not too small, but small enough for now
        "z_first_SF": 10,  # first SFR redshift is 10
        # Metallicity of the Universe
        "min_logZ": -12.0,  # The grid in metallicities. This is not necesarily my grid right? TODO: ask lieke how to deal with this
        "max_logZ": 0.0,
        "step_logZ": 0.01,
        # and detector sensitivity
        "sensitivity": "O1",  # sensitivity currently does not matter really
        "snr_threshold": 8,  #
        #
        "Mc_max": 300.0,
        "Mc_step": 0.1,
        "eta_max": 0.25,
        "eta_step": 0.01,
        "snr_max": 1000.0,
        "snr_step": 0.1,
        # Parameters to calculate the representing SF mass (make sure these match YOUR simulation!)
        "m1_min": 7 * u.Msun,  # M1 was 7
        "m1_max": 300 * u.Msun,  #
        "m2_min": 0.1 * u.Msun,
        "fbin": 0.7,
        # Parameters determining dP/dZ and SFR(z), default options from Neijssel 2019
        "aSF": 0.02,
        "bSF": 1.48,
        "cSF": 4.45,
        "dSF": 5.90,
        # MSSFR settings
        "mu0": 0.025,
        "muz": -0.05,
        "sigma0": 1.125,
        "sigmaz": 0.05,
        "alpha": -1.77,
    }

    # Run the convolution
    (
        detection_rate,
        formation_rate,
        merger_rate,
        redshifts,
        COMPAS,
    ) = FCI.find_detection_rate(
        source_filename,
        # Pass all the arguments
        **FastCosmicIntegrator_arguments,
    )

    ##########
    # Write results to target file

    #
    if os.path.isfile(target_filename):
        os.remove(target_filename)
    shutil.copy(
        source_filename, target_filename
    )  # dst can be a folder; use shutil.copy2() to preserve timestamp

    # add groups to
    target_h5file = h5py.File(target_filename, "a")

    # Write the results to the file
    target_h5file.create_dataset(
        "/convolution_data/detection_rate", data=detection_rate
    )
    target_h5file.create_dataset(
        "/convolution_data/formation_rate", data=formation_rate
    )
    target_h5file.create_dataset("/convolution_data/merger_rate", data=merger_rate)
    target_h5file.create_dataset("/convolution_settings/redshift", data=redshifts)
    target_h5file.create_dataset("/convolution_settings/DCOmask", data=COMPAS.DCOmask)

    # Write the arguments to the output file
    serialised_dict = json.dumps(
        FastCosmicIntegrator_arguments, default=FCI_json_serializer
    )
    target_h5file.create_dataset("/arguments", data=serialised_dict)

    target_h5file.close()
