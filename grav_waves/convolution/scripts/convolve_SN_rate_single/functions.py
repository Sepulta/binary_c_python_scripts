"""
Extra functions for the single sn convolution
"""

import os
import time
import json
import shutil

import h5py
import pandas as pd

from grav_waves.convolution.functions.convolution_add_population_settings_to_hdf5file import (
    add_population_settings_to_hdf5file,
)
from grav_waves.convolution.functions.convolution_general_functions import (
    JsonCustomEncoder,
)


def create_combined_single_sn_dataset(
    dataset_dict, convolution_configuration, cosmology_configuration, output_filename
):
    """
    Function to create the combined SN hdf5 dataset

    input:
        convolution_configuration: settings for the convolution
        cosmology_configuration; settings for the cosmology: SFR etc
        output_filename: target filename for the hdf5 file containing the combined data
        dco_type: choice of dco type to contain in the combined dataset. choices: 'nsns', 'bhns', 'bhbh', 'combined'. Default='combined' (all dco type)
    """

    simname_dir = dataset_dict["main_dir"]

    #
    convolution_configuration["logger"].info("Started combining the datasets")

    # ###########
    # # Set up the output file and groups

    # Create the output directory
    if os.path.dirname(output_filename):
        os.makedirs(os.path.dirname(output_filename), exist_ok=True)

    # create the main HDF5 file
    hdf5_file = h5py.File(output_filename, "w")

    # Create groups
    settings_grp = hdf5_file.create_group("settings")
    _ = hdf5_file.create_group("data")

    settings_grp.create_dataset(
        "dataset_dict", data=json.dumps(dataset_dict, cls=JsonCustomEncoder)
    )

    # ##################
    # # Load dataset info and write all the settings to the output hdf5
    # # TODO: fix the dataset dict
    # info_dict = load_info_dict(dataset_dict['population_result_dir'], rebuild=dataset_dict['rebuild'])
    # settings_grp.create_dataset(
    #     "input_datasets", data=json.dumps(
    #         info_dict,
    #         cls=JsonCustomEncoder
    #     )
    # )

    ##################
    # Load dataset population settings and binary_c settings etc and add to settings group
    population_result_dir = os.path.join(simname_dir, "single_population_results")
    metallicity_dir = os.path.join(
        population_result_dir, sorted(os.listdir(population_result_dir))[0]
    )
    population_settings_filename = os.path.join(
        metallicity_dir,
        [el for el in os.listdir(metallicity_dir) if el.endswith("_settings.json")][0],
    )

    add_population_settings_to_hdf5file(
        convolution_filehandle=hdf5_file,
        population_settings_filename=population_settings_filename,
    )

    #######################
    # Close hdf5 file to be able to write the pandas stuff in there
    hdf5_file.close()

    ##################
    # Create a combined dataframe of all datasets and add to data group
    convolution_configuration["logger"].info("Combining dataframes and writing to hdf5")
    start_combining = time.time()

    # Create combined dataframe from all individual dataframes
    combined_df = combine_single_sn_dataframes(
        simname_dir=simname_dir, convolution_configuration=convolution_configuration
    )
    combined_df.to_hdf(output_filename, "data/combined_dataframes", complevel=0)

    #
    convolution_configuration["logger"].info(
        "Finished combining dataframes and writing to hdf5. Took {}s".format(
            time.time() - start_combining
        )
    )
    convolution_configuration["logger"].info(
        "Finished building the combined sn dataset for:\n\t{}.\nWrote results to:\n\t{}".format(
            simname_dir, output_filename
        )
    )


def combine_single_sn_dataframes(simname_dir, convolution_configuration):
    """
    Function to read out each metallicity and combine their individual dataframes
    """

    # readout the population dir
    population_results_dir = os.path.join(simname_dir, "single_population_results")

    # read out content of population results
    metallicity_dirs = [
        os.path.join(population_results_dir, el)
        for el in os.listdir(population_results_dir)
        if el.startswith("Z")
    ]

    # set up dataframe
    combined_df = pd.DataFrame()

    # Loop over all metallicity dirs and combine the dataframes
    for metallicity_dir in sorted(metallicity_dirs):
        total_file = os.path.join(metallicity_dir, "total_sn_events.dat")

        # If the file doesn't exist, reconstruct it:
        if not os.path.isfile(total_file):
            convolution_configuration["logger"].debug(
                "combining chunked results for metallicity {}".format(
                    os.path.basename(metallicity_dir)
                )
            )
            combine_resultfiles(
                metallicity_dir,
                "sn_events",
                "total_sn_events.dat",
                check_duplicates_and_all_present=False,
            )

        # Check if the file exists
        if os.path.isfile(total_file):
            # Create df
            convolution_configuration["logger"].debug(
                "building df for metallicity {}".format(
                    os.path.basename(metallicity_dir)
                )
            )
            df = return_single_sn_dataframe(
                filename=total_file, convolution_configuration=convolution_configuration
            )

            # Combine df
            convolution_configuration["logger"].debug(
                "combining df for metallicity {}".format(
                    os.path.basename(metallicity_dir)
                )
            )
            combined_df = pd.concat([combined_df, df], ignore_index=True)

    return combined_df


def return_single_sn_dataframe(filename, convolution_configuration):
    """
    Function to return a dataframe that has read out and filtered the data from the supernova logging of the PPISN project.

    opens the dataset, splits if up and generates a new dataset containing information about the SN and the companion

    input: filename containing the output of binary_c
    """

    # Read out df
    df = pd.read_csv(filename, header=0, sep="\s+")

    #
    df = add_number_per_formed_solar_mass(
        df,
        single_fraction_factor=convolution_configuration["single_fraction"],
        average_mass_system=convolution_configuration["average_mass_system"],
    )

    ###################
    # Handle sn 1
    handle_columns(df, 1)

    # Filter out post_SN_SN_type == 0
    df = df.query("post_SN_SN_type > 0")

    #
    df["formation_time_values_in_years"] = df["post_SN_time"] * 1e6
    df["number_per_solar_mass_values"] = df["number_per_solar_mass"]

    return df


def other(index):
    """
    Function to return the other
    """

    if index == 1:
        return 2
    if index == 2:
        return 1
    raise ValueError("index {} has no other".format(index))


def handle_columns(df, index):
    """
    Function to handle renaming etc
    """

    # Rename some columns
    df["zams_mass"] = df["zams_mass_{}".format(index)]

    del df["zams_mass_{}".format(index)]

    #
    rename_dict = {el: el[:-2] for el in df.columns if el.endswith("_{}".format(index))}
    df = df.rename(columns=rename_dict, inplace=True)

    return df


def add_number_per_formed_solar_mass(df, single_fraction_factor, average_mass_system):
    """
    function to add the number per formed solarmass to the df
    """

    # Multiply the probability by a binary fraction
    df["probability"] *= single_fraction_factor

    # Multiply the probability by a conversion factor to get the number per solar mass
    df["number_per_solar_mass"] = df["probability"] / average_mass_system

    return df
