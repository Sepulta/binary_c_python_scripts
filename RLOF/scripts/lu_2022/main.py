"""
Functions to solve Lu2022 equations

The equations are quite difficult, especially because of the non linearity and the dependence of the opacity. My idea is
"""

import numpy as np

from sympy.solvers import solve

from sympy import Symbol


def equation_16(theta, T, fl2):
    """
    non-symbolic version of equation 16
    """

    c1 = 1
    c2 = 1

    res = (c1 * T**4) * theta**3 - theta**2 + c2 * T

    return res


def return_solutions_symbolic_equation_16(theta, fl2):
    """
    Function to calculate equation 16 of the two equations to solve for theta and T
    """

    c1 = 1
    c2 = 1

    T = Symbol("T")

    res = solve((c1 * T**4) * theta**3 - theta**2 + c2 * T)

    return res


def filter_results(res):
    """
    Function to filter the results of the symbolic solver
    """

    filtered_res = []

    for el in res:
        try:
            floatval = float(el)
            if floatval > 0:
                filtered_res.append(floatval)
        except TypeError:
            pass

    return filtered_res


#
fl2 = 0

# for theta in 10**np.arange(-4, 4, 0.1):
#     #
#     fl2 = 0
#     res = return_solutions_symbolic_equation_16(theta=theta, fl2=fl2)
#     filtered_res = filter_results(res)
#     print(filtered_res)
#     for el in filtered_res:
#         print(equation_16(theta=theta, T=el, fl2=fl2))


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

#
from grav_waves.settings import convolution_settings

#
from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict
from RLOF.paper_scripts.functions.episode_plot_functions.utility import filter_df_all

#
from david_phd_functions.plotting.custom_mpl_settings import LINESTYLE_TUPLE
from david_phd_functions.plotting.utils import show_and_save_plot, add_plot_info
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

T_arr = np.arange(-10, 10, 0.001)
theta = 10

res_arr = equation_16(theta=theta, T=T_arr, fl2=fl2)
print(res_arr)

res = return_solutions_symbolic_equation_16(theta=theta, fl2=fl2)
filtered_res = filter_results(res)

print(filtered_res)
plt.plot(T_arr, res_arr)
plt.yscale("symlog")

plt.show()
