"""
Script to calculate the detection probability for a dummy system

TODO: Add colorbar
TODO: Add contour lines for linear plot
TODO: Add contour lines for log10 plot
TODO: Add plot info and export functionality
"""

import os
import numpy as np

import fpdf


# #
# snr_threshold = 7
# redshift = 3
# plot_detection_probability(redshift, snr_threshold, linear=0, plot_settings={'show_plot': True})
# plot_z_domain(plot_settings={'show_plot': True})


general_plot_routine(main_plot_dir="plots/")
