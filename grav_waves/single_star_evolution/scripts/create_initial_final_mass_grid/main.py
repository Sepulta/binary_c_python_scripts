"""
Script to run a grid of single stars in mass and metallicity and plot that

TODO: continous new prescription fryer
"""

import os

from david_phd_functions.binaryc.personal_defaults import personal_defaults
from david_phd_functions.backup_functions.functions import backup_if_exists

from grav_waves.settings import (
    project_specific_bse_settings,
    project_specific_grid_settings,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.generate_data_function import (
    generate_data_function,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.generate_edge_data_function import (
    generate_edge_data,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.general_plot_routine import (
    general_plot_routine,
)


#
this_file_dir = os.path.dirname(os.path.abspath(__file__))


def main(
    main_output_dir,
    sim_name,
    bse_settings,
    system_generator_settings,
    grid_settings,
    general_plot_config={},
    generate_data=False,
    generate_edge_data=False,
    generate_plot=False,
    generate_data_if_not_present=False,
):
    """
    main function to run a grid of stars in mass and metallicity
    """

    print("Called main function for m-Z grid.")
    print("Using physics settings:\n\t{}".format(bse_settings))
    print("Using system generator settings:\n\t{}".format(system_generator_settings))
    print("Using grid settings:\n\t{}".format(grid_settings))

    ##
    # Create names
    data_output_dir = os.path.join(main_output_dir, "data/")
    edge_data_output_dir = os.path.join(main_output_dir, "edge_data/")
    plot_output_dir = os.path.join(main_output_dir, "plots/")
    combined_plot_output_filename = os.path.join(main_output_dir, "combined.pdf")

    grid_dataset_output_filename = os.path.join(data_output_dir, "output.dat")
    edge_dataset_output_filename = os.path.join(edge_data_output_dir, "output.json")

    # Create directories
    os.makedirs(main_output_dir, exist_ok=True)
    os.makedirs(data_output_dir, exist_ok=True)
    os.makedirs(edge_data_output_dir, exist_ok=True)

    ##############################################
    # Check if the directory of the current simulation exists. If it does, then delete the old dir
    if generate_data or generate_edge_data:
        backup_if_exists(main_output_dir, remove_old_directory_after_backup=True)

    # Generate the data
    if generate_data:
        # Generate grid
        generate_data_function(
            output_dir=data_output_dir,
            system_generator_settings=system_generator_settings,
            grid_settings=grid_settings,
            physics_settings=bse_settings,
        )

    # Generate edge data
    if generate_edge_data:
        generate_edge_data(
            output_file=edge_dataset_output_filename,
            bse_settings=bse_settings,
            grid_settings=grid_settings,
            system_generator_settings=system_generator_settings,
        )

    # Generate the plots
    if generate_plot:
        general_plot_routine(
            input_dataset=grid_dataset_output_filename,
            output_dir=plot_output_dir,
            combined_output_filename=combined_plot_output_filename,
            sim_name=sim_name,
            edge_datafile=edge_dataset_output_filename,
            **general_plot_config,
        )


##
# Setting for this script
generate_data = False
generate_edge_data = False
generate_plots = True
generate_data_if_not_present = False

###
# Set up the base settings
system_generator_settings = {
    "mass_resolution": 292,
    "min_mass": 8,
    "max_mass": 300,
    "log10metallicity_resolution": 24,
    "min_log10_metallicity": -3.9,
    "max_log10_metallicity": -1.4,
    "include_zero_metallicity": False,
}

del project_specific_bse_settings["david_ppisn_logging"]
del project_specific_bse_settings["david_logging_function"]

# Create the BSE settings
bse_settings = {
    **personal_defaults,
    **project_specific_bse_settings,
    "BH_prescription": 3,
    # 'david_log_wind_ensemble': 0,
    "multiplicity": 1,
    "custom_timeout_time": 30,
    "minimum_timestep": 1e-8,
}

#
grid_settings = {**project_specific_grid_settings, "num_cores": 12, "verbosity": 1}

#
general_plot_config = {
    # 'mass_scale': 'log',
    "include_initial_final_grid_plot": True,
    "include_initial_final_grid_fraction_plot": True,
    "include_mass_lost_supernova_plot": True,
    "include_mass_lost_supernova_fraction_plot": True,
    "include_mass_lost_wind_plot": True,
    "include_mass_lost_wind_fraction_plot": True,
    "include_fallback_grid_plot": True,
    "include_min_max_first_bh_mass_plot": True,
    "include_mapping_plot": True,
    "include_edge_plot": False,
}

main_args = {
    "system_generator_settings": system_generator_settings,
    "grid_settings": grid_settings,
    "general_plot_config": general_plot_config,
    "generate_data": generate_data,
    "generate_edge_data": generate_edge_data,
    "generate_plot": generate_plots,
    "generate_data_if_not_present": generate_data_if_not_present,
}

#
main_dir = this_file_dir

##
# Vanilla/default model
main(
    main_output_dir=os.path.join(main_dir, "output/output_vanilla/"),
    sim_name="vanilla",
    bse_settings=bse_settings,
    **main_args,
)

# ##
# # Variation: Robs method
# robs_prescription_setting = {**bse_settings, 'PPISN_prescription': 1}
# main(
#     main_output_dir=os.path.join(main_dir, 'output/output_robs_prescription/'),
#     sim_name='robs_prescription',
#     bse_settings=robs_prescription_setting,
#     **main_args
# )

# ##
# # Variation: 5 core mass shift
# core_shift_settings = {**bse_settings, 'PPISN_core_mass_range_shift': -5}
# main(
#     main_output_dir=os.path.join(main_dir, 'output/output_five_core_mass_shift/'),
#     sim_name='five_core_mass_shift',
#     bse_settings=core_shift_settings,
#     **main_args
# )

# ##
# # Variation: 10 solar mass removed
# additional_massloss_settings = {**bse_settings, 'PPISN_additional_massloss': 10}
# main(
#     main_output_dir=os.path.join(main_dir, 'output/output_ten_solarmass_extra_removed/'),
#     sim_name='ten_solarmass_extra_removed',
#     bse_settings=additional_massloss_settings,
#     **main_args
# )

# ##
# # Variation: rapid
# rapid_bh_prescription_settings = {**bse_settings, 'BH_prescription': 4}
# main(
#     main_output_dir=os.path.join(main_dir, 'output/output_rapid_prescription/'),
#     sim_name='rapid_prescription',
#     bse_settings=rapid_bh_prescription_settings,
#     **main_args
# )

# ##
# # Sander & Vink wind
# vink_wind_settings = {**bse_settings, 'wind_mass_loss': 3}
# main(
#     main_output_dir=os.path.join(main_dir, 'output/output_vink_wind/'),
#     sim_name='vink_wind',
#     bse_settings=vink_wind_settings,
#     **main_args
# )


# ##
# # double PPISN massloss
# double_ppisn_massloss_settings = {**bse_settings, 'PPISN_massloss_multiplier': 2.0}
# main(
#     main_output_dir=os.path.join(main_dir, 'output/output_double_ppisn_massloss/'),
#     sim_name='double_ppisn_massloss',
#     bse_settings=double_ppisn_massloss_settings,
#     **main_args
# )

# ##
# # half PPISN massloss
# half_ppisn_massloss_settings = {**bse_settings, 'PPISN_massloss_multiplier': 0.5}
# main(
#     main_output_dir=os.path.join(main_dir, 'output/output_half_ppisn_massloss/'),
#     sim_name='half_ppisn_massloss',
#     bse_settings=half_ppisn_massloss_settings,
#     **main_args
# )
