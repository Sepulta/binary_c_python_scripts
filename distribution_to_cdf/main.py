from binarycpython.utils.distribution_functions import ktg93
import numpy as np
import matplotlib.pyplot as plt

help(ktg93)

sample_range = 10 ** np.arange(np.log10(0.1), np.log10(80), 0.01)
bins = 10 ** np.arange(np.log10(0.1), np.log10(80), 0.01)
bins_wide = 10 ** np.arange(np.log10(0.1), np.log10(80), 0.05)

ktg93_values = [ktg93(m) for m in sample_range]

plt.plot(sample_range, ktg93_values)
plt.hist(sample_range, bins=bins, weights=ktg93_values)


# plot the cumulative histogram
n, new_bins, patches = plt.hist(
    sample_range,
    bins=bins,
    density=True,
    histtype="step",
    cumulative=True,
    label="Empirical",
    weights=ktg93_values,
)

random_sample_amt = 1000000
from scipy import stats

random_sample = stats.uniform(0, 1).rvs(random_sample_amt)

# print(n)
# print(random_sample)
print(np.digitize(random_sample, n))
m_vals = [new_bins[ind] for ind in np.digitize(random_sample, n)]
plt.hist(m_vals, bins=bins_wide, density=True)

plt.xscale("log")
plt.yscale("log")
plt.savefig("dist.png")
plt.show()
