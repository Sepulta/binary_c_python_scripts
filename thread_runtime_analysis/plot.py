import os
import json
import time

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot


def plot_runtime_hists(tmp_dir, plot_settings=None):
    """
    Function to plot the histograms of runtime for systems. Requires the directory that contains the runtime dir, summary_dir
    """
    if not plot_settings:
        plot_settings = {}

    # Load directories
    summary_dir = os.path.join(tmp_dir, "process_summary")

    summary_dict = {}
    for file in os.listdir(summary_dir):
        with open(os.path.join(summary_dir, file), "r") as f:
            data = json.loads(f.read())
            summary_dict[file] = data

    runtime_dir = os.path.join(tmp_dir, "runtime_systems")

    #
    shortest = 1e20
    longest = 0
    for el in summary_dict:
        # print(summary_dict[el])
        data = summary_dict[el]
        length = data["end_process_time"] - data["start_process_time"]
        if length < shortest:
            shortest = length
        if length > longest:
            longest = length

    for file in os.listdir(runtime_dir):
        result_list = []
        with open(os.path.join(runtime_dir, file), "r") as f:
            for line in f:
                split_line = line.split()
                start_time = float(split_line[0])
                binaryc_time = float(split_line[1])
                binaryc_args = " ".join(split_line[2:])

                result_list.append([start_time, binaryc_time, binaryc_args])
        # Put in df
        df = pd.DataFrame.from_records(
            result_list,
            columns=["start_timestamp", "runtime_binary_c", "commandline_arg"],
        )

        # Calculate the time spent per system MINUS the binary_c call
        total_per_system = (
            np.array(df["start_timestamp"].to_list())[1:]
            - np.array(df["start_timestamp"].to_list())[:-1]
        )
        non_binary_c_time = total_per_system - np.array(df["runtime_binary_c"])[:-1]

        # Find max and min vals
        max_val = max(
            np.max(non_binary_c_time),
            np.max(np.array(df["runtime_binary_c"].to_list())),
        )
        max_val = np.ceil(10 * np.log10(max_val)) / 10

        min_val = min(
            np.min(non_binary_c_time),
            np.min(np.array(df["runtime_binary_c"].to_list())),
        )
        min_val = np.floor(10 * np.log10(min_val)) / 10

        # Set up figure
        fig, axes = plt.subplots(ncols=2, nrows=2, sharex=True)

        bins = 10 ** np.linspace(min_val, max_val, 100)

        res_runtime = axes[0][0].hist(
            np.array(df["runtime_binary_c"].to_list()), bins=bins
        )

        counts = res_runtime[0]
        midpoint_bins = (res_runtime[1][1:] + res_runtime[1][:-1]) / 2
        total = counts * midpoint_bins

        this_dict = summary_dict[file.replace(".txt", ".json")]
        this_length = this_dict["end_process_time"] - this_dict["start_process_time"]

        axes[0][1].plot(midpoint_bins, np.cumsum(total), label="cum sum of this thread")
        axes[0][1].axhline(
            shortest, label="shortest of the threads", alpha=0.5, linestyle="--"
        )
        axes[0][1].axhline(
            longest, label="Longest of the threads", alpha=0.5, linestyle="-."
        )
        axes[0][1].axhline(
            this_length, label="Total length current thread", alpha=0.5, linestyle=":"
        )
        axes[0][1].legend(fontsize=12, loc=2, framealpha=0.5)

        res_non_binary_c = axes[1][0].hist(non_binary_c_time, bins=bins)
        axes[1][1].plot(
            midpoint_bins,
            np.cumsum(res_non_binary_c[0] * midpoint_bins),
            label="cum sum of this thread",
        )

        axes[1][0].set_xscale("log")
        axes[1][0].set_xlabel("Log time (s)")
        axes[0][0].set_ylabel("Counts")
        axes[1][0].set_ylabel("Counts")

        axes[0][0].set_yscale("log")
        axes[1][0].set_yscale("log")
        axes[0][0].set_title("Times in binary-c")
        axes[1][0].set_title("Times not in binary-c")
        axes[0][1].set_title("Cumulative times in binary-c")
        axes[1][0].set_title("Cumulative times not in binary-c")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_barcharts(tmp_dir, plot_settings=None):
    """
    Function to plot the barcharts of runtimes of the threads
    """
    if not plot_settings:
        plot_settings = {}

    # Load directories
    summary_dir = os.path.join(tmp_dir, "process_summary")

    summary_dict = {}
    for file in os.listdir(summary_dir):
        with open(os.path.join(summary_dir, file), "r") as f:
            data = json.loads(f.read())
            summary_dict[file] = data

    runtime_dir = os.path.join(tmp_dir, "runtime_systems")

    #
    total_runtimes_dict = {}
    total_calling_binary_c_dict = {}

    for file in summary_dict.keys():
        dicto = summary_dict[file]
        total_runtimes_dict[dicto["process_id"]] = (
            dicto["end_process_time"] - dicto["start_process_time"]
        )
        total_calling_binary_c_dict[dicto["process_id"]] = dicto[
            "total_time_calling_binary_c"
        ]

    thread_numbers = sorted(total_runtimes_dict.keys())
    total_runtimes = [
        total_runtimes_dict[key] for key in sorted(total_runtimes_dict.keys())
    ]
    total_calling_binary_c = [
        total_calling_binary_c_dict[key] for key in sorted(total_runtimes_dict.keys())
    ]

    # Position of bars on x-axis
    ind = np.array(thread_numbers)

    # Width of a bar
    width = 0.3

    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(16, 16))

    # Plotting
    axes.bar(ind, total_runtimes, width, label="Total runtime thread")
    axes.bar(ind + width, total_calling_binary_c, width, label="Time spent on binary_c")

    axes.set_xlabel("Thread number")
    axes.set_ylabel("Total time")
    axes.set_title("Total runtimes per thread")

    # xticks()
    # First argument - A list of positions at which ticks should be placed
    # Second argument -  A list of labels to place at the given locations
    axes.set_xticks(ind + width / 2)
    axes.set_xticklabels(("thread_{}".format(el) for el in ind), rotation=90)

    # Finding the best position for legends and putting it
    axes.legend(loc="best")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_all_runtimes(tmp_dir, plot_settings=None):
    """
    Function to plot the cumulative sum of the runtimes for systems
    """

    # Load directories
    summary_dir = os.path.join(tmp_dir, "process_summary")

    summary_dict = {}
    for file in os.listdir(summary_dir):
        with open(os.path.join(summary_dir, file), "r") as f:
            data = json.loads(f.read())
            summary_dict[file] = data

    runtime_dir = os.path.join(tmp_dir, "runtime_systems")

    if not plot_settings:
        plot_settings = {}

    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(16, 16))

    for file in os.listdir(runtime_dir):
        result_list = []
        with open(os.path.join(runtime_dir, file), "r") as f:
            for line in f:
                split_line = line.split()
                start_time = float(split_line[0])
                binaryc_time = float(split_line[1])
                binaryc_args = " ".join(split_line[2:])

                result_list.append([start_time, binaryc_time, binaryc_args])
        # Put in df
        df = pd.DataFrame.from_records(
            result_list,
            columns=["start_timestamp", "runtime_binary_c", "commandline_arg"],
        )

        axes.plot(
            np.arange(0, len(df["runtime_binary_c"].to_list())),
            np.cumsum(df["runtime_binary_c"].to_list()),
            label="{}".format(file.replace(".txt", "")),
        )

    axes.legend(ncol=2, loc=4, framealpha=0.5, fontsize=14)

    axes.set_title("Cumulative runtime for binary_c calls")
    axes.set_ylabel("Time (s)")
    axes.set_xlabel("System")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
