"""
Function to handle the plotting of the data
"""

import os

from grav_waves.convolution.scripts.new_main.plot_convolution_results import (
    plot_convolution_results,
)
from grav_waves.gw_analysis.functions.general_plot_function import (
    general_plot_function as general_dataset_plot_function,
)
from grav_waves.gw_analysis.functions.functions import combine_queries


def run_plotting_function(
    dataset_dict,
    population_data_result_dir,
    population_data_plots_dir,
    convolution_plots_dir,
    convolution_result_dir,
    convolution_settings,
    run_dataset_plots=True,
    run_convolution_plots=True,
):
    """
    Function to handle plotting the results
    """

    #############
    # run the plotting for the population datasets:
    if run_dataset_plots:
        # TODO: put this in a function
        main_population_data_plots_dir = os.path.join(population_data_plots_dir, "all")
        main_population_data_plots_output_name = os.path.join(
            main_population_data_plots_dir, "combined_dataset_plots.pdf"
        )
        os.makedirs(main_population_data_plots_dir, exist_ok=True)

        print("Plotting dataset plots without query")
        general_dataset_plot_function(
            dataset_dict=dataset_dict,
            result_dir=population_data_result_dir,
            plot_dir=main_population_data_plots_dir,
            dco_type="bhbh",
            combined_pdf_output_name=main_population_data_plots_output_name,
            convolution_settings=convolution_settings,
            # disable some plots as they are outdated
            include_plot_MWEG_merger_rates=False,
            include_plot_RVOL_merger_rates=False,
            include_marchant_plots=False,
            include_stevenson_plots=False,
        )

        ################
        # Plotting with queries

        # register the original query:
        original_global_query = convolution_settings["global_query"]

        ####
        # Liekes channels:
        querylist_lieke = [
            {"name": "CE channel", "query": "(comenv_counter >= 1)"},
            {"name": "No CE channel", "query": "(comenv_counter==0)"},
        ]
        liekes_channels_data_plots_dir = os.path.join(
            population_data_plots_dir, "channels_lieke"
        )
        os.makedirs(liekes_channels_data_plots_dir, exist_ok=True)

        for query in querylist_lieke:
            fixed_query_name = query["name"].replace(" ", "_")
            query_plots_dir = os.path.join(
                liekes_channels_data_plots_dir, fixed_query_name
            )
            query_plots_output_name = os.path.join(
                query_plots_dir,
                "combined_dataset_plots_{}.pdf".format(fixed_query_name),
            )
            os.makedirs(query_plots_dir, exist_ok=True)

            # set query
            convolution_settings["global_query"] = combine_queries(
                original_global_query, query["query"]
            )

            # plot data
            print(
                "Plotting dataset plots with query: {}".format(
                    convolution_settings["global_query"]
                )
            )
            general_dataset_plot_function(
                dataset_dict=dataset_dict,
                result_dir=population_data_result_dir,
                plot_dir=query_plots_dir,
                dco_type="bhbh",
                combined_pdf_output_name=query_plots_output_name,
                convolution_settings=convolution_settings,
                # disable some plots as they are outdated
                include_plot_MWEG_merger_rates=False,
                include_plot_RVOL_merger_rates=False,
                include_marchant_plots=False,
                include_stevenson_plots=False,
            )

        ####
        # ppisn channels:
        querylist_ppisn = [
            {
                "name": "No PPISN",
                "query": "((undergone_ppisn_1==0 & undergone_ppisn_2==0))",
            },
            {
                "name": "One PPISN",
                "query": "(((undergone_ppisn_1==0 & undergone_ppisn_2==1) | (undergone_ppisn_1==1 & undergone_ppisn_2==0)))",
            },
            {
                "name": "Two PPISN",
                "query": "((undergone_ppisn_1==1 & undergone_ppisn_2==1))",
            },
        ]
        ppisn_channels_data_plots_dir = os.path.join(
            population_data_plots_dir, "channels_ppisn"
        )
        os.makedirs(ppisn_channels_data_plots_dir, exist_ok=True)

        for query in querylist_ppisn:
            fixed_query_name = query["name"].replace(" ", "_")
            query_plots_dir = os.path.join(
                ppisn_channels_data_plots_dir, fixed_query_name
            )
            query_plots_output_name = os.path.join(
                query_plots_dir,
                "combined_dataset_plots_{}.pdf".format(fixed_query_name),
            )
            os.makedirs(query_plots_dir, exist_ok=True)

            # set query
            convolution_settings["global_query"] = combine_queries(
                original_global_query, query["query"]
            )

            # plot data
            print(
                "Plotting dataset plots without query: {}".format(
                    convolution_settings["global_query"]
                )
            )
            general_dataset_plot_function(
                dataset_dict=dataset_dict,
                result_dir=population_data_result_dir,
                plot_dir=query_plots_dir,
                dco_type="bhbh",
                combined_pdf_output_name=query_plots_output_name,
                convolution_settings=convolution_settings,
                # disable some plots as they are outdated
                include_plot_MWEG_merger_rates=False,
                include_plot_RVOL_merger_rates=False,
                include_marchant_plots=False,
                include_stevenson_plots=False,
            )

    #############
    # run the plotting for the convolution:
    if run_convolution_plots:
        # for the convolution without detector probability:
        convolution_without_detector_probability_filename = os.path.join(
            convolution_result_dir,
            "without_detection_probability",
            "rebinned_convolution_results.h5",
        )
        print(
            "running convolution plots for {}".format(
                convolution_without_detector_probability_filename
            )
        )
        if os.path.isfile(convolution_without_detector_probability_filename):
            plot_convolution_results(
                convolution_plot_dir=convolution_plots_dir,
                results_filename=convolution_without_detector_probability_filename,
                bootstraps=50,
                plot_without_channels=True,
                plot_channels_lieke=True,
                plot_channels_ppisn=True,
            )
        else:
            print(
                "File {} not found, abort.".format(
                    convolution_without_detector_probability_filename
                )
            )
