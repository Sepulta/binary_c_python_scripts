import matplotlib.pyplot as plt
import numpy as np
import os

sourcedir = "unique_store_memaddr"

# for el in sorted(os.listdir(sourcedir)):
#     if el.endswith('runtime_systems.txt'):
#         times = []
#         with open(os.path.join(sourcedir, el), 'r') as f:
#             for line in f:
#                 split = line.split()
#                 times.append(float(split[0]))
#         rn = np.linspace(0, 15, num=75)
#         plt.hist(times, bins=rn)
#         plt.yscale('log')
#         plt.title(el)
#         plt.savefig('plots/{}_hist.png'.format(el))
#         plt.show()


for el in sorted(os.listdir(sourcedir)):
    if el.endswith("runtime_systems.txt"):
        times = []
        with open(os.path.join(sourcedir, el), "r") as f:
            for line in f:
                split = line.split()
                times.append(float(split[0]))

        lim = 2
        times = np.array(times)
        under_lim = times[times < lim]
        above_lim = times[times >= lim]

        print(
            "{}. total amt below {}s ({} systems). total amt above= {}s ({} systems".format(
                el, np.sum(under_lim), len(under_lim), np.sum(above_lim), len(above_lim)
            )
        )

        # print(under_lim)
        # print(above_lim)


# # sorted_data =

# data = np.array(times)
# # evaluate the histogram
# values, base = np.histogram(data, bins=rn)
# #evaluate the cumulative
# cumulative = np.cumsum(values)
# # plot the cumulative function
# plt.plot(base[:-1], cumulative, c='blue', marker='o')
# #plot the survival function
# # plt.plot(base[:-1], len(data)-cumulative, c='green')
# plt.xscale('log')
# plt.show()
