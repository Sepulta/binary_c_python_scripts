import os
import json
import time
import sys

import pandas as pd
import matplotlib.pyplot as plt

import numpy as np

from plot import *

from david_phd_functions.plotting.custom_mpl_settings import (
    load_mpl_rc,
    LINESTYLE_TUPLE,
)

load_mpl_rc()

main_results_directory = "results/local_tmp_dir"

runtime_dir = os.path.join(main_results_directory, "runtime_systems")

process = "process_8"

fullname_process_file = os.path.join(os.path.abspath(runtime_dir), process + ".txt")

result_list = []
with open(fullname_process_file, "r") as f:

    for line in f:
        split_line = line.split()
        start_time = float(split_line[0])
        binaryc_time = float(split_line[1])
        binaryc_args = " ".join(split_line[2:])

        result_list.append([start_time, binaryc_time, binaryc_args])
# Put in df
df = pd.DataFrame.from_records(
    result_list, columns=["start_timestamp", "runtime_binary_c", "commandline_arg"]
)

high = df[df.runtime_binary_c > 100]
print(["runtime_binary_c"])
print(high["runtime_binary_c"])
print(high.iloc[0]["commandline_arg"])
