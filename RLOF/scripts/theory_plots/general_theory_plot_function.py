"""
Function to run and store all the plots for the theory plots
"""

import os

from PyPDF2 import PdfFileMerger
from david_phd_functions.plotting.pdf_functions import add_pdf_and_bookmark

from RLOF.scripts.theory_plots.functions import (
    plot_radii,
    plot_tidal_radii,
    plot_beta_min,
    plot_fraction_returned,
    plot_pdot_p_conservative_without_disk_torque,
    plot_pdot_p_conservative,
    plot_pdot_p_conservative_disk_torque_only,
)
from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    copy_to_paper_directory_function,
)

from RLOF.scripts.theory_plots.routines.ratio_accretion_luminosity_zams_luminosity.functions import (
    generate_data_and_create_plots,
)

#
def general_plot_routine(
    output_dir,
    output_pdf_filename,
    rebuild_results=True,
    copy_to_paper_directory=False,
    paper_figure_directory=None,
    verbose=0,
    min_log10_q_acc_don=-2,
    max_log10_q_acc_don=2,
    min_log10_racc_sep=-4,
    max_log10_racc_sep=0,
):
    """
    Function to plot all the convolved quantities

    plots all above plots multiple times with different probability floors
    """

    # Make output dir if necessary:
    output_pdfs_dir = os.path.join(output_dir, "pdf")

    os.makedirs(output_dir, exist_ok=True)
    os.makedirs(output_pdfs_dir, exist_ok=True)

    # to check the input
    if copy_to_paper_directory:
        if paper_figure_directory is None:
            msg = "Configured to copy to paper directory but got a wrong paper_figure_directory path ({})".format(
                paper_figure_directory
            )
            raise ValueError(msg)

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    #
    print("Generating theory plots")

    # Plot the radii
    plot_radii_filename = os.path.join(output_pdfs_dir, "theory_plot_radii.pdf")
    plot_radii(
        plot_settings={"show_plot": False, "output_name": plot_radii_filename},
        min_log10_q_acc_don=min_log10_q_acc_don,
        max_log10_q_acc_don=max_log10_q_acc_don,
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger, plot_radii_filename, pdf_page_number, bookmark_text="Relevant Radii"
    )
    if copy_to_paper_directory:
        copy_to_paper_directory_function(
            output_pdfs_dir,
            paper_figure_directory,
            "theory_plot_radii.pdf",
            verbose=verbose,
        )

    # Plot the tidal radius (outer radius)
    plot_tidal_radii_filename = os.path.join(
        output_pdfs_dir, "theory_plot_tidal_radii.pdf"
    )
    plot_tidal_radii(
        plot_settings={"show_plot": False, "output_name": plot_tidal_radii_filename},
        min_log10_q_acc_don=min_log10_q_acc_don,
        max_log10_q_acc_don=max_log10_q_acc_don,
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger, plot_tidal_radii_filename, pdf_page_number, bookmark_text="Tidal Radii"
    )
    if copy_to_paper_directory:
        copy_to_paper_directory_function(
            output_pdfs_dir,
            paper_figure_directory,
            "theory_plot_tidal_radii.pdf",
            verbose=verbose,
        )

    # Plot the plot_beta_min
    plot_beta_min_filename = os.path.join(output_pdfs_dir, "theory_plot_beta_min.pdf")
    plot_beta_min(
        plot_settings={"show_plot": False, "output_name": plot_beta_min_filename},
        min_log10_q_acc_don=min_log10_q_acc_don,
        max_log10_q_acc_don=max_log10_q_acc_don,
        min_log10_racc_sep=min_log10_racc_sep,
        max_log10_racc_sep=max_log10_racc_sep,
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        plot_beta_min_filename,
        pdf_page_number,
        bookmark_text="Fraction to be lost at the outer edge of the disk",
    )
    if copy_to_paper_directory:
        copy_to_paper_directory_function(
            output_pdfs_dir,
            paper_figure_directory,
            "theory_plot_beta_min.pdf",
            verbose=verbose,
        )

    # Plot the fraction that is returned to the orbit
    plot_fraction_returned_to_orbit_filename = os.path.join(
        output_pdfs_dir, "theory_plot_fraction_returned.pdf"
    )
    plot_fraction_returned(
        plot_settings={
            "show_plot": False,
            "output_name": plot_fraction_returned_to_orbit_filename,
        },
        min_log10_q_acc_don=min_log10_q_acc_don,
        max_log10_q_acc_don=max_log10_q_acc_don,
        min_log10_racc_sep=min_log10_racc_sep,
        max_log10_racc_sep=max_log10_racc_sep,
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        plot_fraction_returned_to_orbit_filename,
        pdf_page_number,
        bookmark_text="Fraction returned to orbit",
    )
    if copy_to_paper_directory:
        copy_to_paper_directory_function(
            output_pdfs_dir,
            paper_figure_directory,
            "theory_plot_fraction_returned.pdf",
            verbose=verbose,
        )

    ###########################
    # pdot/p

    # Plot pdot/p for direct impact only
    plot_pdot_p_direct_impact_only_filename = os.path.join(
        output_pdfs_dir, "theory_plot_pdot_p_direct_impact_only.pdf"
    )
    plot_pdot_p_conservative_without_disk_torque(
        plot_settings={
            "show_plot": False,
            "output_name": plot_pdot_p_direct_impact_only_filename,
        },
        min_log10_q_acc_don=min_log10_q_acc_don,
        max_log10_q_acc_don=max_log10_q_acc_don,
        min_log10_racc_sep=min_log10_racc_sep,
        max_log10_racc_sep=max_log10_racc_sep,
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        plot_pdot_p_direct_impact_only_filename,
        pdf_page_number,
        bookmark_text="Pdot/P for direct impact only for conservative MT",
    )
    if copy_to_paper_directory:
        copy_to_paper_directory_function(
            output_pdfs_dir,
            paper_figure_directory,
            "theory_plot_pdot_p_direct_impact_only.pdf",
            verbose=verbose,
        )

    # Plot pdot/p general (effect of MT general + disk torque)
    plot_pdot_p_conservative_general_filename = os.path.join(
        output_pdfs_dir, "theory_plot_pdot_p_conservative_general.pdf"
    )
    plot_pdot_p_conservative(
        plot_settings={
            "show_plot": False,
            "output_name": plot_pdot_p_conservative_general_filename,
        },
        min_log10_q_acc_don=min_log10_q_acc_don,
        max_log10_q_acc_don=max_log10_q_acc_don,
        min_log10_racc_sep=min_log10_racc_sep,
        max_log10_racc_sep=max_log10_racc_sep,
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        plot_pdot_p_conservative_general_filename,
        pdf_page_number,
        bookmark_text="Pdot/P general (general MT and disk torque) for conservative MT",
    )
    if copy_to_paper_directory:
        copy_to_paper_directory_function(
            output_pdfs_dir,
            paper_figure_directory,
            "theory_plot_pdot_p_conservative_general.pdf",
            verbose=verbose,
        )

    # Plot pdot/p disk torque only (disk torque)
    plot_pdot_p_conservative_disk_torque_only_filename = os.path.join(
        output_pdfs_dir, "theory_plot_pdot_p_conservative_disk_torque_only_general.pdf"
    )
    plot_pdot_p_conservative_disk_torque_only(
        plot_settings={
            "show_plot": False,
            "output_name": plot_pdot_p_conservative_disk_torque_only_filename,
        },
        min_log10_q_acc_don=min_log10_q_acc_don,
        max_log10_q_acc_don=max_log10_q_acc_don,
        min_log10_racc_sep=min_log10_racc_sep,
        max_log10_racc_sep=max_log10_racc_sep,
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        plot_pdot_p_conservative_disk_torque_only_filename,
        pdf_page_number,
        bookmark_text="Pdot/P disk torque only for conservative MT",
    )
    if copy_to_paper_directory:
        copy_to_paper_directory_function(
            output_pdfs_dir,
            paper_figure_directory,
            "theory_plot_pdot_p_conservative_disk_torque_only_general.pdf",
            verbose=verbose,
        )

    # plot_pdot_p_conservative_fraction_disk_torque(plot_settings={'show_plot': True})

    ###################
    # Ratio of accretion luminosity etc
    accretion_luminosity_plots_combined_pdf_filename = os.path.join(
        output_pdfs_dir, "accretion_luminosity_plots.pdf"
    )
    accretion_luminosity_plots_result_output_dir = os.path.join(
        output_pdfs_dir, "accretion_luminosity_plots_result_output_dir"
    )
    accretion_luminosity_plots_plot_output_dir = os.path.join(
        output_pdfs_dir, "accretion_luminosity_plots_plot_output_dir"
    )

    os.makedirs(accretion_luminosity_plots_result_output_dir, exist_ok=True)
    os.makedirs(accretion_luminosity_plots_plot_output_dir, exist_ok=True)

    generate_data_and_create_plots(
        result_output_dir=accretion_luminosity_plots_result_output_dir,
        plot_output_dir=accretion_luminosity_plots_plot_output_dir,
        output_pdf_filename=accretion_luminosity_plots_combined_pdf_filename,
        generate_plots=True,
        generate_results=rebuild_results,
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        accretion_luminosity_plots_combined_pdf_filename,
        pdf_page_number,
        bookmark_text="Accretion luminosity plots",
        add_source_bookmarks=True,
    )

    #
    print("Creating combined pdf for theory plots")

    # wrap up the pdf
    merger.write(output_pdf_filename)
    merger.close()
    print("\t wrote combined pdf to {}".format(output_pdf_filename))

    print("Finished generating plots")
