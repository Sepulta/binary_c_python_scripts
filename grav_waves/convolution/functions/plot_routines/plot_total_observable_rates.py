"""
Function to plot the total observable rate after volume convolution
"""

import copy
import pickle
import json
import h5py
import numpy as np
import pandas as pd

import astropy.units as u

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors

from grav_waves.convolution.functions.convolution_functions import (
    generate_metallicity_sfr_array,
)
from grav_waves.convolution.functions.redshift_interpolation_functions import (
    test_redshift_to_lookback_time,
    test_lookback_time_to_redshift,
    create_interpolation_datasets,
)
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot

#
# matplotlib.use('Agg')

#
number_per_gigaparsec_cubed_per_year = 1 / u.Gpc**3 / u.yr
sfr_unit = u.Msun / u.Gpc**3 / u.yr


def plot_total_observable_rates(
    convolution_dataset, mass_type, time_bin_key=None, plot_settings=None
):
    """
    Plot for the volume convolved rate. CAn only be run if the time type is redshift

    Args:
        - convolution_dataset: dataset containing all the information about the simulation, results and initial settings
        - mass_type: option to plot which mass

        - time_bin_key (Optional): If passed then we plot only the results of that certain time bin
    """

    if not plot_settings:
        plot_settings = {}

    # unpack the convolution dataset
    convolution_settings = convolution_dataset["convolution_configuration"]

    #
    rate_key = "merger_rates"
    long_name_rate_type = "Merger rate"

    #
    rate_result_dict = convolution_dataset["redshift_volume_convolution_results"][
        "merger_rates"
    ]

    # Get correct mass keyname
    if mass_type == "chirpmass":
        mass_bins = convolution_settings["chirpmass_bins"]
        mass_key = "result_array_chirpmass"
        long_name_mass = "Chirp mass"
    elif mass_type == "total_mass":
        mass_bins = convolution_settings["total_mass_bins"]
        mass_key = "result_array_total_mass"
        long_name_mass = "Total mass"
    elif mass_type == "primary_mass":
        mass_bins = convolution_settings["primary_mass_bins"]
        mass_key = "result_array_primary_mass"
        long_name_mass = "Primary mass"
    elif mass_type == "any_mass":
        mass_bins = convolution_settings["any_mass_bins"]
        mass_key = "result_array_any_mass"
        long_name_mass = "Any mass"

    #
    main_results = rate_result_dict[mass_key]

    # Decide which results will be plotted
    plot_results = main_results
    if not time_bin_key is None:
        if time_bin_key in convolution_dataset["time_binned"]["results"].keys():
            # if we do want to plot the time slice we need to select that data again:
            sliced_results = convolution_dataset["time_binned"]["results"][
                time_bin_key
            ][rate_key][mass_key]

            #
            plot_results = sliced_results

        else:
            msg = "Couldnt find the passed time_bin_key in the known keys. Aborting"
            raise KeyError(msg)

    # Set up axes
    fig, axes = plt.subplots(ncols=1, nrows=1, figsize=(20, 20))

    total_rates = np.sum(plot_results, axis=0)

    mass_centers = (mass_bins[1:] + mass_bins[:-1]) / 2

    # Plot the data
    axes.plot(mass_centers, total_rates)

    #
    axes.set_xlabel("{} ".format(long_name_mass) + r"[M$_{\odot}$]")
    axes.set_ylabel("Rate [{}]".format(total_rates.unit))
    axes.set_title("Total rate as a function of {}".format(long_name_mass.lower()))

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
