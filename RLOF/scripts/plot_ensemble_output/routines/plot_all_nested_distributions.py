"""
Test function to automatically plot all the nested distributions
"""

import os
import time
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from ensemble_functions.ensemble_functions import (
    get_recursive_sum,
    return_keys_bins_and_bincenters,
    flatten_data_ensemble1d,
    get_recursive_sums_for_all_parameter,
)

from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)
from RLOF.scripts.plot_ensemble_output.settings import (
    parameter_name_dict,
    bins_integer_parametertypes,
)
from RLOF.scripts.plot_ensemble_output.functions import (
    handle_querylist,
    return_string_querylist,
)

# Pdf generating routines
from PyPDF2 import PdfFileMerger
from david_phd_functions.plotting.pdf_functions import add_pdf_and_bookmark
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()
matplotlib.rc("text", usetex=False)

from grav_waves.settings import convolution_settings


def plot_all_nested_distributions(
    dataset,
    plot_dir,
    output_pdf_filename,
    weighttype,
    testing,
    base_querylist=None,
    querylist=None,
    plot_settings=None,
    verbose=0,
):
    """
    Function to plot all the nested distributions. Each layer gets its own plot.

    Input:
        dataset: dictionary containing the location and metallicity of the data
        plot_dir: location where to write the intermediate plots to
        output_pdf_filename: full path filename for the combined pdf
        weighttype: type of weighting (either time or mass)
        testing: flag to enable testing mode, which will load a ensemble chunk if possible, rather than the full ensemble
        base_querylist (optional): List of queries that we will run on the ensemble, adopting the queried dataset as the current new total.
        querylist (optional): List of queries that we will run on the total ensemble (which might be queried already by the base_query). The results of the queried ensemble will be displayed alongside the total results
    """

    #
    if not plot_settings:
        plot_settings = {}

    os.makedirs(plot_dir, exist_ok=True)

    if not weighttype in ["mass", "time"]:
        raise ValueError(
            "Weighttype ({}) must be in {}".format(weighttype, ["mass", "time"])
        )

    # Get the correct ensemble data from the dict
    if verbose:
        print("\t\tLoading data")
    probability_to_yield_conversion_factor = (
        convolution_settings["binary_fraction"] * convolution_settings["mass_in_stars"]
    )
    ensemble_data = get_ensemble_data(
        dataset,
        testing,
        probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
    )["RLOF"]["fraction_separation_massratio"][weighttype]

    #######
    # Querying

    # Run base-query on the data
    # Do base query
    if not base_querylist is None:
        ensemble_data = handle_querylist(ensemble_data, base_querylist, "base-query")

    #
    print("Finding the unqueried recursive sum for all parameters")
    start = time.time()
    res = get_recursive_sums_for_all_parameter(ensemble_data)
    print("\ttook {}s".format(time.time() - start))

    # Run query on the data
    if not querylist is None:
        queried_data = ensemble_data
        queried_data = handle_querylist(queried_data, querylist, "query")

        #
        print("Finding the queried recursive sum for all parameters")
        start = time.time()
        res_queried = get_recursive_sums_for_all_parameter(queried_data)
        print("\ttook {}s".format(time.time() - start))

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    # go over the keys:
    for _, parameter_key in enumerate(res.keys()):
        # TODO: change the use of this, and abstract to a function to plot the results.
        # Turn into histogram results
        current_parameter = parameter_key
        print("\tPlotting {}".format(current_parameter))

        ###
        # calculate total and normalise it
        total_sum = get_recursive_sum(res[current_parameter])

        # If the number of keys in the res[current_parameter] is 1, then we use the backup ranges
        # if len(res[current_parameter].keys()) < 3:
        if current_parameter in bins_integer_parametertypes:
            total_bins = bins_integer_parametertypes[current_parameter]
            total_bincenter = (total_bins[1:] + total_bins[:-1]) / 2
        else:
            # Get the bins and bincenters based on the total
            _, total_bins, total_bincenter = return_keys_bins_and_bincenters(
                res, current_parameter
            )

        # Get binsize for the bars
        binsize = np.diff(total_bincenter).min()

        # Flatten, so we have arrays
        flattened_total = flatten_data_ensemble1d(res, current_parameter)

        # Put the counts in the histogram
        array_total, _ = np.histogram(
            flattened_total[0], bins=total_bins, weights=flattened_total[1]
        )

        # Normalise array. TODO: reconsider this
        array_total = array_total / total_sum

        # Get min and max val
        min_value = array_total[array_total > 0].min()
        max_value = array_total[array_total > 0].max()

        # Handle queried results
        if not querylist is None:
            flattened_queried = flatten_data_ensemble1d(res_queried, current_parameter)

            #
            array_queried, _ = np.histogram(
                flattened_queried[0], bins=total_bins, weights=flattened_queried[1]
            )

            # Normalise by total
            array_queried = array_queried / total_sum

            # Create fractions
            array_fraction_queried = np.divide(
                array_queried,
                array_total,
                out=np.zeros_like(array_queried),
                where=array_total != 0,
            )
            array_fraction_rest = np.divide(
                array_total - array_queried,
                array_total,
                out=np.zeros_like(array_queried),
                where=array_total != 0,
            )

            # Update min value with queried
            min_value = np.min([min_value, array_queried[array_queried > 0].min()])

        #########
        # Plotting
        scale_limits = 0.5

        fig = plt.figure(figsize=(20, 20))

        #
        gs = fig.add_gridspec(nrows=12, ncols=1)

        if querylist is None:
            ax = fig.add_subplot(gs[:7, :])
            ax_cdf = fig.add_subplot(gs[8:, :])

        else:
            fig.subplots_adjust(hspace=0)

            ax = fig.add_subplot(gs[:5, :])
            ax_ratio = fig.add_subplot(gs[5:8, :], sharex=ax)
            ax_cdf = fig.add_subplot(gs[8:, :])

        #########
        ax.scatter(total_bincenter, array_total, marker="o", c="blue")
        ax.bar(
            total_bincenter,
            array_total,
            binsize,
            label="total{}".format(
                " (base_query: {})".format(return_string_querylist(base_querylist))
                if base_querylist is not None
                else ""
            ),
            color="blue",
            alpha=0.5,
            hatch="/",
            antialiased=True,
            rasterized=True,
        )
        # Plot cumulative density function
        ax_cdf.plot(
            total_bincenter,
            np.cumsum(array_total) / np.sum(array_queried),
            label="total{}".format(
                " (base_query: {})".format(return_string_querylist(base_querylist))
                if base_querylist is not None
                else ""
            ),
        )

        # Plot the queried results
        if not querylist is None:
            ax.scatter(total_bincenter, array_queried, marker="o", c="orange")
            ax.bar(
                total_bincenter,
                array_queried,
                binsize,
                label="query: {}".format(return_string_querylist(querylist)),
                color="orange",
                alpha=0.5,
                hatch="\\",
                antialiased=True,
                rasterized=True,
            )

            # Plot cumulative density function
            ax_cdf.plot(
                total_bincenter,
                np.cumsum(array_queried) / np.sum(array_queried),
                label="query: {}".format(return_string_querylist(querylist)),
            )

            #
            ax_ratio.bar(
                total_bincenter,
                array_fraction_queried,
                binsize,
                label="Fraction query: {}".format(return_string_querylist(querylist)),
                color="orange",
                antialiased=True,
                rasterized=True,
            )
            ax_ratio.bar(
                total_bincenter,
                array_fraction_rest,
                binsize,
                bottom=array_fraction_queried,
                label="Fraction everything outside queried",
                color="blue",
                antialiased=True,
                rasterized=True,
            )

        #########
        # Make up

        # legends
        ax.legend(frameon=True, framealpha=0.5)

        # Axes labels
        ax.set_ylabel("Normalised yield (number per solar mass)")

        if not querylist is None:
            ax_ratio.set_ylabel("Fraction of total")
            ax_ratio.legend(frameon=True, framealpha=0.5)
            ax_ratio.set_xlim(ax.get_xlim())

        # Axes scales
        ax.set_yscale("log")
        ax_cdf.set_ylim([0, 1])
        ax_cdf.legend(frameon=True, framealpha=0.5)

        ax_cdf.set_xlim(ax.get_xlim())

        # Add label
        ax.set_xlabel(r"{}".format(parameter_name_dict[current_parameter]))
        ax_cdf.set_xlabel(r"{}".format(parameter_name_dict[current_parameter]))
        ax_ratio.set_xlabel(r"{}".format(parameter_name_dict[current_parameter]))

        # Set limits
        ax.set_ylim((min_value * (1 - scale_limits), max_value * (1 + scale_limits)))
        ax.set_title(
            "Normalised yield distribution of {}{}{}{}".format(
                parameter_name_dict[current_parameter],
                "\n" if (base_querylist is not None) or (querylist is not None) else "",
                " with base query {}".format(return_string_querylist(base_querylist))
                if base_querylist is not None
                else "",
                " with query {}".format(return_string_querylist(querylist))
                if querylist is not None
                else "",
            ),
            fontsize=24,
        )

        fig = add_plot_info(
            fig,
            plot_settings={
                "simulation_name": plot_settings.get("simulation_name", ""),
                "runname": "{} weighted".format(weighttype),
            },
        )

        #######################
        # Save and finish
        plot_name = os.path.join(plot_dir, "{}.pdf".format(parameter_key))
        show_and_save_plot({"output_name": plot_name})
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_name,
            pdf_page_number,
            bookmark_text="{} distribution".format(parameter_key),
            add_source_bookmarks=False,
        )

    # Round off the plot
    merger.write(output_pdf_filename)
    merger.close()
    print("Finished making plots. Wrote pdf to {}".format(output_pdf_filename))


# population_datasets_sana = generate_ensemble_dataset_dict('/home/david/projects/binary_c_root/results/RLOF/LOW_RES_RLOF_SANA_2021_ENSEMBLE_fractions')
# plot_all_nested_distributions(
#     population_datasets_sana['datasets']['0.02'],
#     plot_dir='output/',
#     output_pdf_filename='test.pdf',
#     weighttype='mass',
#     testing=True,
#     querylist=[{'keyname': 'st_acc', 'valuerange': [0.5, 1.5]}],
#     plot_settings={'show_plot': True, 'output_name': 'test.pdf', 'simulation_name': "{}: total".format(population_datasets_sana['simname'])},
#     verbose=1,
# )

# plot_all_nested_distributions(
#     population_datasets_sana['datasets']['0.02'],
#     plot_dir='/home/david/projects/binary_c_root/binary_c_python_scripts/RLOF/scripts/plot_ensemble_output/routines/nested_distribution_plots',
#     output_pdf_filename='test_stable_result.pdf',
#     weighttype='mass',
#     testing=False,
#     base_querylist=[{'keyname': 'st_acc', 'valuerange': [0.5, 1.5]}],
#     querylist=[{'keyname': 'stable', 'valuerange': [-0.5, 0.5]}],
#     plot_settings={'show_plot': True, 'output_name': 'test.pdf', 'simulation_name': "{}: accretion onto ms".format(population_datasets_sana['simname'])},
#     verbose=1,
# )

# plot_all_nested_distributions(
#     population_datasets_sana['datasets']['0.02'],
#     plot_dir='/home/david/projects/binary_c_root/binary_c_python_scripts/RLOF/scripts/plot_ensemble_output/routines/nested_distribution_plots',
#     output_pdf_filename='test_disk_result.pdf',
#     weighttype='mass',
#     testing=False,
#     base_querylist=[{'keyname': 'st_acc', 'valuerange': [0.5, 1.5]}, {'keyname': 'stable', 'valuerange': [-0.5, 0.5]}],
#     querylist=[{'keyname': 'disk', 'valuerange': [0.5, 1.5]}],
#     plot_settings={'show_plot': True, 'output_name': 'test.pdf', 'simulation_name': "{}: stable accretion onto ms".format(population_datasets_sana['simname'])},
#     verbose=1,
# )
