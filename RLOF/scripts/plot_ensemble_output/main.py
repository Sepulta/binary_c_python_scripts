"""
Main script to run all the routines to generate scripts

We only rely on the datasets being in their place and up to date
- The datasets should at max contain 3 metallicities

We can query the datasets with a set of global queries. These will be executed in each function. The output directory name will be set to the root dir + global_query_name
"""

import os

from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
)
from RLOF.scripts.plot_ensemble_output.general_plot_routine import general_plot_routine

# Register this file
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)

#############################################################
# Configuration
#############################################################

# Select the datasets
population_datasets_sana = generate_ensemble_dataset_dict(
    "/home/david/projects/binary_c_root/results/RLOF/LOW_RES_RLOF_SANA_2021_ENSEMBLE_fractions/"
)
# population_datasets_sana = generate_ensemble_dataset_dict('/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions/')
# population_datasets_ms = generate_ensemble_dataset_dict('/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_MOE_DISTEFANO_2021_ENSEMBLE_fractions/')

#
root_output_dir = os.path.join(this_file_dir, "output")

# Some switches to copy things or not
testing = True  # Flag whether we are testing the code. If false then we use the entire available datasets
verbose = 1

###############
# Generate global plots for the datasets
###############

# Idea as follows:
# A function that creates the 'big pdf'. No labels for the tables etc. not necessary. based on simname etc
general_plot_routine(
    sim_dict=population_datasets_sana,
    output_dir=os.path.join(
        root_output_dir, "{}".format(population_datasets_sana["simname"])
    ),
    verbose=verbose,
    testing=testing,
    do_tables=False,
    do_figures=False,
)


# Dict containing the queries that we want to perform. Currently not using a
global_query_dict = {
    # 'global_query_name': "only_massive_donors",
    # 'query_list': [
    #     {
    #         'parameter_key': 'log10mass_donor',
    #         'value_range': [1, 10]
    #     },
    # ]
}


# general_plot_routine(
#     sim_dict=population_datasets_ms,
#     global_query_dict=global_query_dict,
#     output_dir=os.path.join(root_output_dir, "{}{}".format(population_datasets_ms['simname'], "_query_{}".format(global_query_dict['global_query_name']) if global_query_dict else "")),
#     verbose=verbose,
#     testing=testing
# )
