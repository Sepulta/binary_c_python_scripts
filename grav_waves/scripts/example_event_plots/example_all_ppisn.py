"""
Routine to plot all ppisne pre-post eccentricity
"""

import os
import numpy as np
import pandas as pd
from grav_waves.functions.events import merge_with_event_dataframe, print_rows_and_cols


import matplotlib.pyplot as plt
from matplotlib import colors
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

import matplotlib
from plot_utils import get_vmin

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def example_all_ppisn_pre_post_eccentricity(
    total_SN_datafile, SN_counter=0, plot_settings={}
):
    """
    Example function to combine the DCO data with supernova events and
    """

    # Set bins
    numbins = 25
    x_bins = np.linspace(0, 1, numbins)
    y_bins = np.linspace(0, 1, numbins)

    all_hists = []
    all_limited_hists = []

    # use chunks!
    chunksize = 10**6
    for chunk_i, chunk in enumerate(
        pd.read_csv(
            total_SN_datafile,
            chunksize=chunksize,
            usecols=[
                "pre_SN_ecc",
                "SN_type",
                "post_SN_ecc",
                "SN_counter",
                "probability",
            ],
            sep="\s+",
        )
    ):
        print("Handling chunk {}".format(chunk_i))
        ppisn_SN = chunk.query("SN_type == 21")
        bound_PPISN_SN = ppisn_SN.query("pre_SN_ecc >= 0")

        #
        chunked_hist, _, _ = np.histogram2d(
            bound_PPISN_SN["pre_SN_ecc"],
            bound_PPISN_SN["post_SN_ecc"],
            bins=[x_bins, y_bins],
            weights=bound_PPISN_SN["probability"],
        )
        all_hists.append(chunked_hist)

        if SN_counter:
            limited_bound_PPISN_SN = bound_PPISN_SN.query(
                "SN_counter == {}".format(SN_counter)
            )

            #
            chunked_limited_hist, _, _ = np.histogram2d(
                limited_bound_PPISN_SN["pre_SN_ecc"],
                limited_bound_PPISN_SN["post_SN_ecc"],
                bins=[x_bins, y_bins],
                weights=limited_bound_PPISN_SN["probability"],
            )
            all_limited_hists.append(chunked_limited_hist)

    # Combine all hists
    combined_hist = np.zeros(all_hists[0].shape)
    for hist in all_hists:
        combined_hist += hist

    if SN_counter:
        combined_limited_hist = np.zeros(all_limited_hists[0].shape)
        for limited_hist in all_limited_hists:
            combined_limited_hist += limited_hist

    # Select the actual used histogram
    used_hist = combined_hist
    if SN_counter:
        used_hist = combined_limited_hist

    # Set norm
    norm = colors.LogNorm(
        vmin=get_vmin(combined_hist, plot_settings.get("min_prob_val", 1e-8)),
        vmax=np.max(combined_hist),
    )

    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=5)
    rates_axis = fig.add_subplot(gs[:, :-1])
    ax_cb = fig.add_subplot(gs[:, -1:])

    # make bincenters
    x_bincenters = (x_bins[1:] + x_bins[:-1]) / 2
    y_bincenters = (y_bins[1:] + y_bins[:-1]) / 2

    X, Y = np.meshgrid(x_bincenters, y_bincenters)

    _ = rates_axis.pcolormesh(
        X,
        Y,
        used_hist.T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # rates_axis.set_yscale('log')
    # rates_axis.set_xscale('log')
    rates_axis.set_xlabel("Pre-SN eccentricity")
    rates_axis.set_ylabel("Post-SN eccentricity")

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        norm=norm,
    )
    cbar.ax.set_ylabel(r"Number per formed solar mass")

    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    # total_sn_datafile = "/home/david/data_projects/binary_c_data/GRAV_WAVES/server_results/EVENTS_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/combined_total_SN_events.dat"
    total_sn_datafile = "/home/david/data_projects/binary_c_data/GRAV_WAVES/server_results/EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/combined_total_SN_BINARY_events.dat"

    output_dir = os.path.join(
        this_file_dir,
        "plots/EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED",
    )
    min_prob_val = 1e-12

    #
    plot_settings = {
        "min_prob_val": min_prob_val,
        "show_plot": False,
        "output_name": os.path.join(output_dir, "pre_post_ppisn_ecc.pdf"),
    }
    example_all_ppisn_pre_post_eccentricity(
        total_sn_datafile, plot_settings=plot_settings
    )

    #
    plot_settings = {
        "min_prob_val": min_prob_val,
        "show_plot": False,
        "output_name": os.path.join(output_dir, "pre_post_ppisn_ecc_first_only.pdf"),
    }
    example_all_ppisn_pre_post_eccentricity(
        total_sn_datafile, SN_counter=1, plot_settings=plot_settings
    )

    #
    plot_settings = {
        "min_prob_val": min_prob_val,
        "show_plot": False,
        "output_name": os.path.join(output_dir, "pre_post_ppisn_ecc_second_only.pdf"),
    }
    example_all_ppisn_pre_post_eccentricity(
        total_sn_datafile, SN_counter=2, plot_settings=plot_settings
    )
