"""
Main entrypoint for the main plot routine
"""

import numpy as np

from grav_waves.scripts.main_plot_function.main_plot_function import main_plot_function

from grav_waves.settings import (
    config_dict_cosmology,
    convolution_settings,
    liekes_channels_query_dict,
)

# Main simulation dir
main_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/MID_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED"

# Set stepping
stepsize = 0.5
convolution_settings["stepsize"] = 0.5
convolution_settings["time_bins"] = np.arange(
    0.0001 - 0.5 * stepsize,
    (convolution_settings["max_interpolation_redshift"] - 1) + 0.5 * stepsize,
    stepsize,
)
convolution_settings["time_centers"] = (
    convolution_settings["time_bins"][1:] + convolution_settings["time_bins"][:-1]
) / 2
convolution_settings[
    "global_query"
] = "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0)"

# Set queryset
queryset = {"channels_lieke": liekes_channels_query_dict}

main_plot_function(
    main_dir,
    cosmology_configuration=config_dict_cosmology,
    convolution_configuration=convolution_settings,
    combined_pdf_output_name="test.pdf",
    # querysets=queryset,
    do_prescription_plots=False,
    do_individual_dataset_plots=True,
    do_convolution_without_detector_probability=False,
    do_detection_probability_plots=False,
    do_convolution_with_detector_probability=False,
    do_volume_convolution=False,
    rebuild_results=False,
)
