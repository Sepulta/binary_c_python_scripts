import pickle

file = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/MID_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_without_detector_probability_results/convolution_without_detection_probability.p"

results = pickle.load(open(file, "rb"))

print(results.keys())
# print(results['results_dataframes_merger_rate'][1.0001])

# print(results['results_dataframes_merger_rate'][1.0001].keys())

# quit()
import json

output_test = "test_output.json"

with open(output_test, "w") as f:
    f.write(json.dumps(results["results_dataframes_merger_rate"][1.0001], indent=4))
