"""
Function to test running a system that has known issues.
"""

import os
import shutil

from binarycpython.utils.grid import Population

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)
data_dir = os.path.join(this_file_dir, "output")

pop = Population(verbosity=5)

# Create local tmp_dir
pop.set(data_dir=data_dir)

pop.set(tmp_dir=os.path.join(pop.custom_options["data_dir"], "local_tmp_dir"))
pop.set(log_args_dir=os.path.join(pop.custom_options["data_dir"], "local_tmp_dir"))

if os.path.isdir(pop.grid_options["tmp_dir"]):
    shutil.rmtree(pop.grid_options["tmp_dir"])
os.makedirs(pop.grid_options["tmp_dir"], exist_ok=True)


# pop.set(custom_timeout_time=1)
pop.set(evolution_type="source_file")
pop.set(do_dry_run=False)
# pop.set(source_file_filename=os.path.join(this_file_dir, 'list.txt'))
pop.set(source_file_filename=os.path.join(this_file_dir, "list.txt"))
pop.evolve()
