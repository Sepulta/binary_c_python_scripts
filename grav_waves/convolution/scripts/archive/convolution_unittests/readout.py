"""
Function to readout the merger rates
"""

import pickle

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors

from david_phd_functions.plotting.utils import show_and_save_plot, add_plot_info
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


####
redshift_convolution_without_detector_probability_filename = "convolution_output.dat"

results = pickle.load(
    open(redshift_convolution_without_detector_probability_filename, "rb")
)
convolution_settings = results["convolution_configuration"]

mass_key = "result_array_total_mass"
rate_result_dict = results["results"]["merger_rates"]


# get bins
timebins = convolution_settings["time_bins"]
time_values = (timebins[1:] + timebins[:-1]) / 2
mass_bins = convolution_settings["total_mass_bins"]

#
main_results = rate_result_dict[mass_key]
plot_settings = {"probability_floor": 3, "antialiased": True, "rasterized": True}
print(main_results[0])

# make dummy hist
_, xedges, yedges = np.histogram2d(
    np.array([]), np.array([]), bins=[timebins, mass_bins]
)

# Decide which results will be plotted
plot_results = main_results.T

X, Y = np.meshgrid(xedges, yedges)

# Set up axes
fig, axes = plt.subplots(ncols=1, nrows=1, figsize=(20, 20))

# Plot the results
pcm = axes.pcolormesh(
    X,
    Y,
    plot_results.value,
    norm=colors.LogNorm(
        vmin=10
        ** np.floor(
            np.log10(main_results.value.max())
            - plot_settings.get("probability_floor", 4)
        ),
        vmax=1,
    ),
    shading="auto",
    antialiased=plot_settings.get("antialiased", False),
    rasterized=plot_settings.get("rasterized", False),
)

cb = fig.colorbar(pcm, ax=axes, extend="min")
plt.show()
