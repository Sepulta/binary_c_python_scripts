import os

from grav_waves.gw_analysis.server_datasets import dataset_dict
from grav_waves.gw_analysis.functions.plot_functions import marchant_plot

main_result_dir = "/vol/ph/astro_code/dhendriks/astro_codes/binaryc/results/GRAV_WAVES"
key = "LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_ON"
sim_name = "LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_ON"
type_key = "bhbh"
display_log_string = True
type_sub_key = "Z0.00025118864315095795_bhbh"
plot_dir = "../plots/"


marchant_plot(
    main_result_dir,
    dataset_dict[key]["sim_name"],
    dataset_dict[key]["no_ppisn_companion_dataset"],
    type_sub_key,
    display_log_string=display_log_string,
    plot_settings={
        "output_name": os.path.join(
            plot_dir,
            type_key,
            "png",
            "marchant_plots/{}_systems.png".format(type_sub_key),
        ),
        "simulation_name": sim_name + "_{}".format(type_sub_key),
        "runname": "{}".format(type_sub_key),
        "block": False,
        "show_plot": False,
    },
)
