"""
Main function to run plots for in the paper
"""

import os

from TZO.functions.functions import load_combined_dataframe

from TZO.paper_scripts.functions.plot_initial_mass_comparison_no_query import (
    plot_initial_mass_comparison_no_query,
)
from TZO.paper_scripts.functions.plot_yield_distribution_over_metallicity import (
    plot_yield_distribution_over_metallicity,
)


def generate_paper_plots(plot_output_dir, verbose=0):
    """
    Function to run the plots for the paper
    """

    ##
    # Load combined dataframe
    combined_df = load_combined_dataframe(
        data_dir="/home/david/projects/binary_c_root/results/TZO/server_results/MID_RES_TESTING/population_results/"
    )

    #########
    # Plot the entire distribution of initial masses for the TZO objects
    basename_plot_initial_mass_comparison_no_query_all_metallicity = (
        "initial_mass_comparison_no_query_all_metallicity.pdf"
    )
    plot_initial_mass_comparison_no_query(
        combined_df,
        all_metallicity_mode=True,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                plot_output_dir,
                basename_plot_initial_mass_comparison_no_query_all_metallicity,
            ),
        },
    )

    #########
    # Plot the yield distribution all metallicities
    basename_plot_yield_distribution_over_metallicity = (
        "yield_distribution_over_metallicity.pdf"
    )
    plot_yield_distribution_over_metallicity(
        combined_df,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                plot_output_dir, basename_plot_yield_distribution_over_metallicity
            ),
        },
    )


#
if __name__ == "__main__":
    #
    output_dir = "/home/david/papers/paper_tzo/paper_tex/figures"

    generate_paper_plots(plot_output_dir=output_dir, verbose=1)
