single_logging_string = """
// make sure he core mass and CO core mass are set properly.

if(stardata->star[0].stellar_type>=NS)
{
    if (stardata->model.time < stardata->model.max_evolution_time)
    {
        Printf("DAVID_PPISN_SINGLE %30.12e " // 1
            "%g %g "  // 2-3
            "%d %d "  // 4-5
            "%g %g "  // 6-7
            "%g %g %g %g %d\\n", // 8-13

            //
            stardata->model.time,                                     // 1

            // Some masses
            stardata->star[0].mass,                                   // 2
            stardata->common.zero_age.mass[0],                        // 3

            // Stellar typ info
            stardata->star[0].stellar_type,                           // 4
            stardata->previous_stardata->star[0].stellar_type,        // 5

            // model stuff
            stardata->common.metallicity,                             // 6
            stardata->model.probability,                              // 7

            // Some info about the SN
            stardata->previous_stardata->star[0].mass,                //8
            stardata->previous_stardata->star[0].core_mass[ID_core(stardata->previous_stardata->star[0].stellar_type)],           //9

            stardata->previous_stardata->star[0].core_mass[CORE_CO],     //10
            stardata->previous_stardata->star[0].core_mass[CORE_He],    //11

            stardata->star[0].SN_type                                 // 12
        );
    }
    /* Kill the simulation to save time */
    stardata->model.max_evolution_time = stardata->model.time - stardata->model.dtm;
}
"""
