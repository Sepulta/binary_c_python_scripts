"""
Extra useful functions for the event based plots
"""

import numpy as np


def get_vmin(hist, custom_min):
    """
    Function to get the minimum value of a histogram OR the custom min if thats
    """

    hist_min = np.min(hist[hist > 0])

    return np.max([custom_min, hist_min])
