import h5py
import pandas as pd
import numpy as np
import numpy_indexed as npi
import time


##############
# Get the dataframes
file = "/home/david/data_projects/binary_c_data/GRAV_WAVES/TEST_2_LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5"
combined_dataframes = pd.read_hdf(file, key="data/combined_dataframes")
rlof_event_dataframe = pd.read_hdf(file, key="data/events/RLOF")

##############
# Merge dataframe with event df
merged_df = merge_with_event_dataframe(
    main_dataframe=combined_dataframes, event_dataframe=rlof_event_dataframe
)

##############
# print info
print_rows_and_cols(combined_dataframes, "combined_dataframes")
print_rows_and_cols(rlof_event_dataframe, "rlof_event_dataframe")
print_rows_and_cols(merged_df, "merged_df")

# #####
# # Get the uuid array from the merged df and find which initial indices they belong to
# indices_of_merged_dataframe_uuids_in_main_dataframe = get_indices_of_merged_dataframe_uuids_in_main_dataframe(
#     main_dataframe=combined_dataframes, merged_dataframe=merged_df
# )
