import os

import h5py
import pandas as pd

import matplotlib.pyplot as plt

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc
from david_phd_functions.plotting.custom_mpl_settings import LINESTYLE_TUPLE


def foo(name, obj):
    print(name, obj)
    return None


#####################
combined_sn_dataset = "/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/convolution/scripts/convolve_SN_rate/results/sn_convolution_tests/combined_sn_dataset.h5"
print("Original file: file size: ", os.path.getsize(combined_sn_dataset))

# Load data and visit
print("Hdf5 content: ")
combined_sn_data = h5py.File(combined_sn_dataset)
combined_sn_data.visititems(foo)

# load df an give info
combined_dataframe = pd.read_hdf(combined_sn_dataset, "/data/combined_dataframes")
print("Dataframe info")
print(
    "Rows ",
    len(combined_dataframe.index),
    "cols ",
    len(combined_dataframe.columns),
    "Memory size ",
    combined_dataframe.memory_usage(index=True).sum(),
)


print("\n")
###############################################
filtered_combined_sn_dataset = "/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/convolution/scripts/convolve_SN_rate/results/sn_convolution_tests/filtered_combined_sn_dataset.h5"
print("Filtered file: file size: ", os.path.getsize(filtered_combined_sn_dataset))

# Load data and visit
print("Hdf5 content: ")
filtered_combined_sn_data = h5py.File(filtered_combined_sn_dataset)
filtered_combined_sn_data.visititems(foo)

# load df an give info
filtered_combined_dataframe = pd.read_hdf(
    filtered_combined_sn_dataset, "/data/combined_dataframes"
)
print("Dataframe info")
print(
    "Rows ",
    len(filtered_combined_dataframe.index),
    "cols ",
    len(filtered_combined_dataframe.columns),
    "Memory size ",
    filtered_combined_dataframe.memory_usage(index=True).sum(),
)


quit()
load_mpl_rc()
import json

#


combined_dataframe = pd.read_hdf(combined_sn_dataset, "/data/combined_dataframes")
print(sorted(combined_dataframe["post_SN_SN_type"].unique()))
filtered_combined_sn_df = combined_dataframe.query(
    "POST_SN_SN_TYPE in [11, 12, 13, 21, 22, 23]"
)

print(combined_dataframe.keys())
quit()

# loop and plot
for sn_type in sorted(combined_dataframe["post_SN_SN_type"].unique()):
    sn_type_df = combined_dataframe[combined_dataframe.post_SN_SN_type == sn_type]

    result = sn_type_df.groupby(["metallicity"], as_index=False).agg(
        {"number_per_solar_mass": ["sum"]}
    )

    plt.plot(
        result["metallicity"], result["number_per_solar_mass"]["sum"], label=sn_type
    )

plt.legend()
plt.xscale("log")
plt.yscale("log")

plt.show()
