"""
Function to run the simulation
"""

import os
import sys
import shutil

from david_phd_functions.backup_functions.functions import backup_if_exists

from grav_waves.run_population_scripts.functions import combine_resultfiles

from binarycpython.utils.grid import Population

from TZO.run_population_scripts.parse_function import parse_function
from TZO.settings import LAMBDA_CE_prescription_dict
from TZO.run_population_scripts.functions import add_distribution_grid_variables

from TZO.settings import project_specific_settings


def generate_simname(simname_base, variation_dict):
    """
    Function to generate the simname
    """

    # Add lambda CE name
    simname = simname_base + "_{}".format(
        LAMBDA_CE_prescription_dict[variation_dict["lambda_ce"]]
    )

    return simname


def run_all(
    combine_all_resultfiles,
    metallicity_values,
    resolution,
    local_population_settings,
    simname_base,
    root_result_dir,
    backup_if_data_exists,
    run_simulations,
    variation_dict,
):
    """
    Function to run the simulation and handle some name settings
    """

    #
    simname = generate_simname(simname_base=simname_base, variation_dict=variation_dict)
    simname_dir = os.path.join(root_result_dir, simname)

    # Running the simulation
    if run_simulations:
        ####
        # Set up population object
        population_object = Population()
        population_object.set(verbosity=1)

        # Set project defaults:
        population_object.set(**project_specific_settings)

        # set local settings
        population_object.set(**local_population_settings)

        #
        population_object.set(parse_function=parse_function)

        # Set the distributions
        population_object = add_distribution_grid_variables(
            population_object, resolution
        )

        # Quit if the settings are wrong
        version_info = population_object.return_binary_c_version_info(parsed=True)
        if version_info["macros"]["NUCSYN"] == "on":
            print("NUSCYN should be off")
            sys.exit(1)

        # Check if the directory of the current simulation exists. If it does, then delete the old dir
        if backup_if_data_exists:
            backup_if_exists(simname_dir, remove_old_directory_after_backup=True)

        # Loop over all metallicities
        for metallicity in metallicity_values:
            population_data_dir = os.path.join(
                simname_dir, "population_results", "Z{}".format(metallicity)
            )

            #
            population_object.set(
                metallicity=metallicity,
                data_dir=population_data_dir,
                lambda_ce=variation_dict["lambda_ce"],
            )

            # Create local tmp_dir
            population_object.set(
                tmp_dir=os.path.join(
                    population_object.custom_options["data_dir"], "local_tmp_dir"
                )
            )
            population_object.set(
                log_args_dir=os.path.join(
                    population_object.custom_options["data_dir"], "local_tmp_dir"
                )
            )

            if os.path.isdir(population_object.grid_options["tmp_dir"]):
                shutil.rmtree(population_object.grid_options["tmp_dir"])
            os.makedirs(population_object.grid_options["tmp_dir"], exist_ok=True)

            # Export settings:
            population_object.export_all_info(use_datadir=True)

            # Evolve grid
            population_object.evolve()

            # Combine the result files
            if combine_all_resultfiles:
                combine_resultfiles(
                    population_object.custom_options["data_dir"],
                    "tzo_events",
                    "all_tzo_events.dat",
                    check_duplicates_and_all_present=False,
                )
            print(
                "Wrote all results to {}".format(
                    population_object.custom_options["data_dir"]
                )
            )

            # Clean the population so we can run it again
            population_object.clean()
