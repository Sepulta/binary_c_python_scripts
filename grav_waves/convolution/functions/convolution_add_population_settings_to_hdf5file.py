"""
function to add population settings to the hdf5 file
"""

import json


def add_population_settings_to_hdf5file(
    convolution_filehandle, population_settings_filename, settings_key="settings"
):
    """
    Fanction to add the population settings to convolution hdf5 file.

    Removes the metallicity value as it is varied between the simulations.

    Adds the following subsets of the population settings to <settings_key> subset
    - 'population_settings'
    - 'binary_c_defaults'
    - 'binary_c_version_info'
    - 'binary_c_help_all'

    input:
        - convolution_filehandle
        - population_settings dict
    """

    #
    with open(population_settings_filename, "r") as f:
        population_settings = json.load(f)

    # Loop over the subsets in the population settings
    settings_group = convolution_filehandle[settings_key]
    for population_settings_key in population_settings.keys():
        settings_group.create_dataset(
            "{}".format(population_settings_key),
            data=json.dumps(population_settings[population_settings_key]),
        )
