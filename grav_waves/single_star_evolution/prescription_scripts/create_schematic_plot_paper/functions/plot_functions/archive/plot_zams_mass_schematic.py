"""
Function to plot the ZAMS mass schematic information

TODO: generalise the routine that plots either variation. No need for the double function
"""

import matplotlib as mpl
import matplotlib.pyplot as plt

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.add_zams_mass_removal_schematic_to_axis import (
    add_zams_mass_removal_schematic_to_axis,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def plot_zams_mass_schematic(
    result_directory_mass_removal,
    old_prescription_result_directory_root,
    no_prescription_result_directory_root=None,
    add_preSN_mass_mass_removal=True,
    add_no_prescription_line=False,
    add_old_prescription_line=False,
    add_fiducial_mass_removal=True,
    add_mass_removed_mass_removal=False,
    add_bracket_extra_mass_loss=False,
    create_shaded_regions=True,
    create_supernova_lines=True,
    dataset_used_for_regions="fiducial",
    plot_settings={},
):
    """
    Function to plot the ZAMS mass to remnant mass with extra ppisn mass removal
    """

    # Set up figure
    fig = plt.figure(figsize=(20, 12))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=8, ncols=1)

    ax_zams_to_remnant_mass_mass_removal = fig.add_subplot(gs[:, 0])

    # Call function to add the plot to the axis
    fig, ax_zams_to_remnant_mass_mass_removal = add_zams_mass_removal_schematic_to_axis(
        fig,
        ax_zams_to_remnant_mass_mass_removal=ax_zams_to_remnant_mass_mass_removal,
        result_directory_mass_removal=result_directory_mass_removal,
        old_prescription_result_directory_root=old_prescription_result_directory_root,
        no_prescription_result_directory_root=no_prescription_result_directory_root,
        add_old_prescription_line=add_old_prescription_line,
        add_no_prescription_line=add_no_prescription_line,
        add_mass_removed_mass_removal=add_mass_removed_mass_removal,
        add_preSN_mass_mass_removal=add_preSN_mass_mass_removal,
        add_fiducial_mass_removal=add_fiducial_mass_removal,
        add_bracket_extra_mass_loss=add_bracket_extra_mass_loss,
        create_shaded_regions=create_shaded_regions,
        create_supernova_lines=create_supernova_lines,
        dataset_used_for_regions=dataset_used_for_regions,
    )

    # Override the limits if we have input:
    if plot_settings.get("min_xlim", None) is not None:
        ax_zams_to_remnant_mass_mass_removal.set_xlim(
            [
                plot_settings["min_xlim"],
                ax_zams_to_remnant_mass_mass_removal.get_xlim()[1],
            ]
        )
    if plot_settings.get("max_xlim", None) is not None:
        ax_zams_to_remnant_mass_mass_removal.set_xlim(
            [
                ax_zams_to_remnant_mass_mass_removal.get_xlim()[0],
                plot_settings["max_xlim"],
            ]
        )
    if plot_settings.get("min_ylim", None) is not None:
        ax_zams_to_remnant_mass_mass_removal.set_ylim(
            [
                plot_settings["min_ylim"],
                ax_zams_to_remnant_mass_mass_removal.get_ylim()[1],
            ]
        )
    if plot_settings.get("max_ylim", None) is not None:
        ax_zams_to_remnant_mass_mass_removal.set_ylim(
            [
                ax_zams_to_remnant_mass_mass_removal.get_ylim()[0],
                plot_settings["max_ylim"],
            ]
        )

    # Handle the plotting
    show_and_save_plot(fig, plot_settings)
