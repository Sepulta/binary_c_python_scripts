"""
Function to generate the MSSFR plot for the paper.

This plot does the following things:
- [X] Plot the MSSFR density plot with dp/dz * dz
- [X] Adds a colorbar to the plot
- [X] Adds red lines at the minimum and maximum z-bins we used
- [X] adds green short ticks at the metallicities we included
"""

import os
import json

import h5py
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from grav_waves.convolution.functions.compas_metallicity_distribution import (
    find_metallicity_distribution,
)
from grav_waves.gw_analysis.functions.cosmology_functions import (
    starformation_rate,
)
from grav_waves.convolution.functions.convolution_general_functions import (
    create_bins_from_centers,
)


from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def generate_mssfr_plot(dataset_filename, scale, plot_settings={}):
    """
    Function to generate the mssfr plot for the paper
    """

    ##################
    # Read out the dataset
    hdf5file = h5py.File(dataset_filename)

    cosmology_configuration = json.loads(
        hdf5file["settings/cosmology_configuration"][()]
    )
    convolution_configuration = json.loads(
        hdf5file["settings/convolution_configuration"][()]
    )
    metallicity_bin_centers = np.array(hdf5file["sfr_data/metallicity_bin_centers"][()])
    log10metallicity_values = np.array(sorted(np.log10(metallicity_bin_centers)))
    log10metallicity_bins = create_bins_from_centers(log10metallicity_values)
    metallicity_bins = 10**log10metallicity_bins

    hdf5file.close()

    ##################
    # Generate the data

    # Get sfr value
    sfr_values = starformation_rate(
        np.array(convolution_configuration["time_centers"]),
        cosmology_configuration=cosmology_configuration,
        verbosity=1,
    )

    # Calculate the metallicity distribution grid
    dPdlogZ, metallicities, _ = find_metallicity_distribution(
        np.array(convolution_configuration["time_centers"]),
        np.log(cosmology_configuration["min_value_metalprob"]),
        np.log(cosmology_configuration["max_value_metalprob"]),
        **cosmology_configuration["metallicity_distribution_args"],
        step_logZ=0.05,
    )

    # Calculate full probability
    P = dPdlogZ

    # Multiply by sfr:
    MSSFR = (sfr_values * P.T).T.value
    X, Y = np.meshgrid(convolution_configuration["time_centers"], metallicities)

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=11)

    ax = fig.add_subplot(gs[:, :-2])
    ax_cb = fig.add_subplot(gs[:, -1])

    ##################
    # Plot the data

    # Get the normalisation
    if scale == "linear":
        norm = colors.Normalize(vmin=MSSFR.min(), vmax=MSSFR.max())
    elif scale == "log":
        norm = colors.LogNorm(
            vmin=10 ** (np.log10(MSSFR.max()) - 3),
            vmax=MSSFR.max(),
        )

    ##################
    # Plot the MSSFR results
    _ = ax.pcolormesh(
        X,
        Y,
        MSSFR.T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # make colorbar
    cb = matplotlib.colorbar.ColorbarBase(
        ax_cb, norm=norm, extend="min" if scale == "log" else None
    )

    ##################
    # Plot the extent of metallicity bins
    ax.hlines(
        [metallicity_bins.min(), metallicity_bins.max()],
        color="red",
        lw=1,
        xmin=min(convolution_configuration["time_centers"]),
        xmax=max(convolution_configuration["time_centers"]),
    )

    ##################
    # Plot the included metallicities
    ax.hlines(
        metallicity_bin_centers,
        color="orange",
        lw=1,
        linestyle="--",
        alpha=1,
        xmax=0.1,
        xmin=0,
    )

    # Make up
    ax.set_yscale("log")
    ax.set_ylabel("Metallicity [Z]")
    ax.set_xlabel("Redshift [z]")
    ax.set_title("Metallicity specific star formation rate density", fontsize=28)
    cb.ax.set_ylabel(r"$\frac{d\ \mathrm{SFR}}{dZ}$ [$M_{\odot} yr^{-1} Gpc^{-3}$]")

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    fiducial_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/rebinned_convolution_results.h5"
    base_output_name = "mssfr_plot.pdf"

    #
    generate_mssfr_plot(
        dataset_filename=fiducial_dataset,
        scale="linear",
        plot_settings={
            "show_plot": True,
            # 'output_name': base_output_name
            "output_name": os.path.join(
                "/home/david/papers/paper_gw/paper_tex/figures/", base_output_name
            ),
        },
    )
