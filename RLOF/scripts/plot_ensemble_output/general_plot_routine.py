"""
Function containing the general routine to create all the tables and the plots, and creating a big PDF file out of that
"""

import os
from PyPDF2 import PdfFileMerger

from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    create_table_pdf,
)

from david_phd_functions.plotting.pdf_functions import add_pdf_and_bookmark

# Tables
from RLOF.scripts.plot_ensemble_output.routines.table_MS_fraction_all import (
    generate_table_MS_fraction_all,
)
from RLOF.scripts.plot_ensemble_output.routines.table_fraction_cases_stable_ms_disk import (
    generate_table_fraction_cases_stable_ms_disk,
)
from RLOF.scripts.plot_ensemble_output.routines.table_stable_fractions import (
    generate_table_stable_fractions,
)
from RLOF.scripts.plot_ensemble_output.routines.table_fraction_mt_regions_stable_ms_disk import (
    generate_table_fraction_mt_regions_stable_ms_disk,
)
from RLOF.scripts.plot_ensemble_output.routines.table_disk_fractions import (
    generate_table_disk_fractions,
)
from RLOF.scripts.plot_ensemble_output.routines.table_global_query_fractions import (
    generate_table_global_query_fractions,
)
from RLOF.scripts.plot_ensemble_output.routines.table_fraction_all_rlof_cases_stable_unstable_disk_nodisk import (
    generate_table_fraction_all_rlof_cases_stable_unstable_disk_nodisk,
)

# Plot routines
from RLOF.scripts.plot_ensemble_output.routines.plot_all_nested_distributions import (
    plot_all_nested_distributions,
)
from RLOF.scripts.plot_ensemble_output.routines.plot_massratio_separation_histogram_rlof_cases_one_metallicity import (
    plot_massratio_separation_histogram_rlof_cases_one_metallicity,
)
from RLOF.scripts.plot_ensemble_output.routines.plot_massratio_separation_histogram_mt_regions_one_metallicity import (
    plot_massratio_separation_histogram_mt_regions_one_metallicity,
)
from RLOF.scripts.plot_ensemble_output.routines.plot_massratio_separation_histogram_two_quantities_one_metallicity import (
    plot_massratio_separation_histogram_two_quantities_one_metallicity,
)
from RLOF.scripts.plot_ensemble_output.routines.plot_massratio_separation_histogram_one_quantity_three_metallicities import (
    plot_massratio_separation_histogram_one_quantity_three_metallicities,
)

#
def general_plot_routine(
    sim_dict,
    output_dir,
    combined_output_filename,
    global_query_dict=None,
    verbose=0,
    testing=True,
    do_plot_all_nested_distributions=True,
    do_tables=True,
    do_figures=True,
):
    """
    Function to plot all the convolved quantities

    plots all above plots multiple times with different probability floors
    """

    # Make output dir if necessary:
    os.makedirs(output_dir, exist_ok=True)

    # Register table directory here
    local_figure_directory = os.path.join(output_dir, "figures")
    local_nested_distribution_directory = os.path.join(
        local_figure_directory, "nested_distributions_1d"
    )
    local_tables_directory = os.path.join(output_dir, "tables")
    os.makedirs(local_figure_directory, exist_ok=True)
    os.makedirs(local_nested_distribution_directory, exist_ok=True)
    os.makedirs(local_tables_directory, exist_ok=True)

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    ################################################
    # All parameters distributions and fractions:
    if do_plot_all_nested_distributions:
        print("generating all the plots for the nested distributions")

        for weighttype in ["mass", "time"]:
            local_nested_distribution_directory_for_weighttype = os.path.join(
                local_nested_distribution_directory, weighttype
            )
            os.makedirs(
                local_nested_distribution_directory_for_weighttype, exist_ok=True
            )

            for metallicity in sim_dict["datasets"]:
                local_nested_distribution_directory_for_weighttype_for_metallicity = (
                    os.path.join(
                        local_nested_distribution_directory_for_weighttype, metallicity
                    )
                )
                os.makedirs(
                    local_nested_distribution_directory_for_weighttype_for_metallicity,
                    exist_ok=True,
                )

                # All nested
                plot_all_nested_distributions_plot_dir = os.path.join(
                    local_nested_distribution_directory_for_weighttype_for_metallicity,
                    "all_nested",
                )
                os.makedirs(plot_all_nested_distributions_plot_dir, exist_ok=True)

                plot_all_nested_distributions_filename = os.path.join(
                    local_figure_directory,
                    "all_nested_distributions_weighttype_{}_Z_{}.pdf".format(
                        weighttype, metallicity
                    ),
                )
                plot_all_nested_distributions(
                    sim_dict["datasets"][metallicity],
                    plot_dir=plot_all_nested_distributions_plot_dir,
                    output_pdf_filename=plot_all_nested_distributions_filename,
                    weighttype=weighttype,
                    testing=testing,
                    querylist=[{"keyname": "st_acc", "valuerange": [0.5, 1.5]}],
                    plot_settings={
                        "show_plot": True,
                        "simulation_name": "{}: total".format(sim_dict["simname"]),
                    },
                    verbose=verbose,
                )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    plot_all_nested_distributions_filename,
                    pdf_page_number,
                    bookmark_text="All nested distributions with weighttype {} and Z {}".format(
                        weighttype, metallicity
                    ),
                    add_source_bookmarks=True,
                )

                # Main sequence only
                plot_ms_accretion_only_nested_distributions_plot_dir = os.path.join(
                    local_nested_distribution_directory_for_weighttype_for_metallicity,
                    "ms_only",
                )
                os.makedirs(
                    plot_ms_accretion_only_nested_distributions_plot_dir, exist_ok=True
                )

                plot_ms_accretion_only_nested_distributions_filename = os.path.join(
                    local_figure_directory,
                    "ms_accretion_only_nested_distributions_weighttype_{}_Z_{}.pdf".format(
                        weighttype, metallicity
                    ),
                )
                plot_all_nested_distributions(
                    sim_dict["datasets"][metallicity],
                    plot_dir=plot_ms_accretion_only_nested_distributions_plot_dir,
                    output_pdf_filename=plot_ms_accretion_only_nested_distributions_filename,
                    weighttype=weighttype,
                    testing=testing,
                    base_querylist=[{"keyname": "st_acc", "valuerange": [0.5, 1.5]}],
                    querylist=[{"keyname": "stable", "valuerange": [-0.5, 0.5]}],
                    plot_settings={
                        "show_plot": True,
                        "simulation_name": "{}: total".format(sim_dict["simname"]),
                    },
                    verbose=verbose,
                )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    plot_ms_accretion_only_nested_distributions_filename,
                    pdf_page_number,
                    bookmark_text="MS accretion only nested distributions with weighttype {} and Z {}".format(
                        weighttype, metallicity
                    ),
                    add_source_bookmarks=True,
                )

                # Stable MS only
                plot_stable_ms_accretion_only_nested_distributions_plot_dir = os.path.join(
                    local_nested_distribution_directory_for_weighttype_for_metallicity,
                    "stable_ms_only",
                )
                os.makedirs(
                    plot_stable_ms_accretion_only_nested_distributions_plot_dir,
                    exist_ok=True,
                )

                plot_stable_ms_accretion_only_nested_distributions_filename = os.path.join(
                    local_figure_directory,
                    "stable_ms_accretion_only_nested_distributions_weighttype_{}_Z_{}.pdf".format(
                        weighttype, metallicity
                    ),
                )
                plot_all_nested_distributions(
                    sim_dict["datasets"][metallicity],
                    plot_dir=plot_stable_ms_accretion_only_nested_distributions_plot_dir,
                    output_pdf_filename=plot_stable_ms_accretion_only_nested_distributions_filename,
                    weighttype=weighttype,
                    testing=testing,
                    base_querylist=[
                        {"keyname": "st_acc", "valuerange": [0.5, 1.5]},
                        {"keyname": "stable", "valuerange": [-0.5, 0.5]},
                    ],
                    querylist=[{"keyname": "disk", "valuerange": [0.5, 1.5]}],
                    plot_settings={
                        "show_plot": True,
                        "simulation_name": "{}: total".format(sim_dict["simname"]),
                    },
                    verbose=verbose,
                )
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger,
                    plot_stable_ms_accretion_only_nested_distributions_filename,
                    pdf_page_number,
                    bookmark_text="Stable MS accretion only nested distributions with weighttype {} and Z {}".format(
                        weighttype, metallicity
                    ),
                    add_source_bookmarks=True,
                )

    ################################################
    # Tables
    if do_tables:
        #
        print(
            "Generating tables for {}".format(
                sim_dict["simname"],
                "(query: {})".format(global_query_dict["global_query_name"])
                if global_query_dict
                else "",
            )
        )

        ################
        # Table that can be used to translate between the global unqueried and queried dataset
        if global_query_dict:
            table_table_query_fractions_filename = "table_query_fractions.tex"
            generate_table_global_query_fractions(
                sim_dict["datasets"],
                global_query_dict=global_query_dict,
                output_filename=os.path.join(
                    local_tables_directory, table_table_query_fractions_filename
                ),
                caption="",
                label="",
                testing=testing,
                verbose=verbose,
            )
            pdfname = create_table_pdf(
                os.path.join(
                    local_tables_directory, table_table_query_fractions_filename
                )
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger, pdfname, pdf_page_number, bookmark_text="table_query_fractions"
            )

        # ####
        # Table for fraction of MT onto MS vs all MT
        # Fiducial
        table_MS_fraction_all_filename = "fractions_onto_ms_table_fiducial.tex"
        generate_table_MS_fraction_all(
            sim_dict["datasets"],
            output_filename=os.path.join(
                local_tables_directory, table_MS_fraction_all_filename
            ),
            global_query_dict=global_query_dict,
            caption="Fractions of mass transfer onto main-sequence stars normalised to all mass transfer, weighted by either mass transferred or time spent.",
            label="",
            testing=testing,
            verbose=verbose,
        )
        pdfname = create_table_pdf(
            os.path.join(local_tables_directory, table_MS_fraction_all_filename)
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger, pdfname, pdf_page_number, bookmark_text="fraction_MT_onto_MS"
        )

        ########################################
        # TABLE: TOTAL FRACTION STABLE MT ONTO MS FOR DISK AND NO DISK
        # Fiducial
        table_stable_fractions_output_normal_filename = (
            "fractions_stable_table_fiducial.tex"
        )
        print("Generating ", table_stable_fractions_output_normal_filename)
        generate_table_stable_fractions(
            sim_dict["datasets"],
            output_filename=os.path.join(
                local_tables_directory, table_stable_fractions_output_normal_filename
            ),
            global_query_dict=global_query_dict,
            caption="Fractions of stability at onset of mass transfer onto main-sequence stars for disk or direct impact mass transfer for the fiducial runs for a range of metallicities (Z). The results are normalised by the total probability of mass transfer onto main-sequence stars.",
            testing=testing,
            verbose=verbose,
        )
        pdfname = create_table_pdf(
            os.path.join(
                local_tables_directory, table_stable_fractions_output_normal_filename
            )
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger, pdfname, pdf_page_number, bookmark_text="table_stable_fractions"
        )

        ########################################
        # TABLE: TOTAL FRACTION STABLE/UNSTABLE DISK/DIRECT for all RLOF cases
        # Fiducial
        for key in sim_dict["datasets"].keys():
            table_all_cases_stable_unstable_disk_nodisk_fiducial_filename = (
                "table_all_cases_stable_unstable_disk_nodisk_fiducial_{}.tex".format(
                    key
                )
            )
            print(
                "Generating ",
                table_all_cases_stable_unstable_disk_nodisk_fiducial_filename,
            )
            generate_table_fraction_all_rlof_cases_stable_unstable_disk_nodisk(
                sim_dict["datasets"][key],
                output_filename=os.path.join(
                    local_tables_directory,
                    table_all_cases_stable_unstable_disk_nodisk_fiducial_filename,
                ),
                global_query_dict=global_query_dict,
                caption="",
                label="table:all_cases_stable_unstable_disk_nodisk_{}".format(key),
                testing=testing,
                verbose=verbose,
            )
            pdfname = create_table_pdf(
                os.path.join(
                    local_tables_directory,
                    table_all_cases_stable_unstable_disk_nodisk_fiducial_filename,
                )
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                pdfname,
                pdf_page_number,
                bookmark_text="table_fraction_all_rlof_cases_stable_unstable_disk_nodisk {}".format(
                    key
                ),
            )

        ########################################
        # TABLE: TOTAL FRACTION DISK MT ONTO MS STARS

        # fiducial
        table_disk_fractions_output_normal_filename = (
            "fractions_disk_table_fiducial.tex"
        )
        print("Generating ", table_disk_fractions_output_normal_filename)
        generate_table_disk_fractions(
            sim_dict["datasets"],
            output_filename=os.path.join(
                local_tables_directory, table_disk_fractions_output_normal_filename
            ),
            global_query_dict=global_query_dict,
            caption="Fractions of stable disk mass transfer onto main-sequence stars at onset of mass transfer, mass weighted, and time weighted, for the fiducial distributions. The results are normalised by the total probability of stable mass transfer onto main-sequence stars.",
            label="",
            testing=testing,
            verbose=verbose,
        )
        pdfname = create_table_pdf(
            os.path.join(
                local_tables_directory, table_disk_fractions_output_normal_filename
            )
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger, pdfname, pdf_page_number, bookmark_text="table_disk_fractions"
        )

        #####
        # Table for stable MT onto MS via disk in for RLOF cases
        # Fiducial
        for key in sim_dict["datasets"].keys():
            table_fraction_cases_stable_ms_disk_filename = (
                "table_fraction_cases_stable_ms_disk_{}.tex".format(key)
            )
            generate_table_fraction_cases_stable_ms_disk(
                sim_dict["datasets"][key],
                output_filename=os.path.join(
                    local_tables_directory, table_fraction_cases_stable_ms_disk_filename
                ),
                global_query_dict=global_query_dict,
                caption="Fractions of mass weighted RLOF cases onto either disks or direct impact for stable mass transfer onto main-sequence stars for the fiducial simulaton with Z = {}. The numbers are normalised by the total amount of stable mass transfer onto main-sequence stars.".format(
                    key
                ),
                label="",
                testing=testing,
                verbose=verbose,
            )
            pdfname = create_table_pdf(
                os.path.join(
                    local_tables_directory, table_fraction_cases_stable_ms_disk_filename
                )
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                pdfname,
                pdf_page_number,
                bookmark_text="table_fraction_cases_stable_ms_disk {}".format(key),
            )

        # Table for stable MT onto MS via disk for different critical MT regions
        # Fiducial
        for key in sim_dict["datasets"].keys():
            table_fraction_mt_regions_stable_ms_disk_filename = (
                "table_fraction_mt_regions_stable_ms_disk_{}.tex".format(key)
            )
            generate_table_fraction_mt_regions_stable_ms_disk(
                sim_dict["datasets"][key],
                output_filename=os.path.join(
                    local_tables_directory,
                    table_fraction_mt_regions_stable_ms_disk_filename,
                ),
                global_query_dict=global_query_dict,
                caption="Fractions of mass weighted mass transfer categories for stable mass transfer onto main-sequence stars via disk accretion for the fiducial simulaton with Z = {}. The numbers are normalised by the total amount of stable mass transfer onto main-sequence stars via disks.".format(
                    key
                ),
                label="table:fraction_mt_regions_stable_ms_disk_{}".format(key),
                testing=testing,
                verbose=verbose,
            )
            pdfname = create_table_pdf(
                os.path.join(
                    local_tables_directory,
                    table_fraction_mt_regions_stable_ms_disk_filename,
                )
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                pdfname,
                pdf_page_number,
                bookmark_text="table_fraction_mt_regions_stable_ms_disk {}".format(key),
            )

    ################################################
    # Figures:
    if do_figures:
        #
        print(
            "Generating plots for {}".format(
                sim_dict["simname"],
                "(query: {})".format(global_query_dict["global_query_name"])
                if global_query_dict
                else "",
            )
        )

        ####
        # Plot with RLOF cases for stable MT onto MS via disk
        for key in sim_dict["datasets"].keys():
            plot_massratio_separation_histogram_rlof_cases_one_metallicity_filename = "plot_massratio_separation_histogram_rlof_cases_one_metallicity_{}.pdf".format(
                key
            )
            plot_massratio_separation_histogram_rlof_cases_one_metallicity(
                sim_dict["datasets"][key],
                global_query_dict=global_query_dict,
                plot_settings={
                    "show_plot": False,
                    "output_name": os.path.join(
                        local_figure_directory,
                        plot_massratio_separation_histogram_rlof_cases_one_metallicity_filename,
                    ),
                },
                testing=testing,
                verbose=verbose,
            )
            pdfname = os.path.join(
                local_figure_directory,
                plot_massratio_separation_histogram_rlof_cases_one_metallicity_filename,
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                pdfname,
                pdf_page_number,
                bookmark_text="massratio_separation_histogram_rlof_cases_one_metallicity {}".format(
                    key
                ),
            )

        ####
        # Plot with MT regions for stable MT onto MS via disk
        for key in sim_dict["datasets"].keys():
            plot_massratio_separation_histogram_mt_regions_one_metallicity_filename = "plot_massratio_separation_histogram_mt_regions_one_metallicity_{}.pdf".format(
                key
            )
            plot_massratio_separation_histogram_mt_regions_one_metallicity(
                sim_dict["datasets"][key],
                global_query_dict=global_query_dict,
                plot_settings={
                    "show_plot": False,
                    "output_name": os.path.join(
                        local_figure_directory,
                        plot_massratio_separation_histogram_mt_regions_one_metallicity_filename,
                    ),
                },
                testing=testing,
                verbose=verbose,
            )
            pdfname = os.path.join(
                local_figure_directory,
                plot_massratio_separation_histogram_mt_regions_one_metallicity_filename,
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                pdfname,
                pdf_page_number,
                bookmark_text="massratio_separation_histogram_mt_regions_one_metallicity {}".format(
                    key
                ),
            )

        # ####
        # # Plot with MT regions for stable MT onto MS via disk
        # Generate canvas for massratio and racc for fiducial
        for key in sim_dict["datasets"].keys():
            figure_stable_mt_disk_massratio_seperation_mass_and_time_filename = (
                "massratio_separation_histogram_mass_and_time_{}.pdf".format(key)
            )
            plot_massratio_separation_histogram_two_quantities_one_metallicity(
                sim_dict["datasets"][key],
                global_query_dict=global_query_dict,
                plot_settings={
                    "show_plot": False,
                    "output_name": os.path.join(
                        local_figure_directory,
                        figure_stable_mt_disk_massratio_seperation_mass_and_time_filename,
                    ),
                },
                testing=testing,
                verbose=verbose,
            )
            pdfname = os.path.join(
                local_figure_directory,
                figure_stable_mt_disk_massratio_seperation_mass_and_time_filename,
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                pdfname,
                pdf_page_number,
                bookmark_text="stable_mt_disk_massratio_seperation_mass_and_time {}".format(
                    key
                ),
            )

        ########################################
        # PLOT: canvas for massratio and racc for normal distribution MASS WEIGHT
        if len(list(sim_dict["datasets"].keys())) == 3:
            figure_stable_mt_disk_massratio_seperation_massweighted_plot_filename = (
                "massratio_separation_histogram_mass_weighted.pdf"
            )
            print(
                "Plotting ",
                figure_stable_mt_disk_massratio_seperation_massweighted_plot_filename,
            )
            plot_massratio_separation_histogram_one_quantity_three_metallicities(
                sim_dict["datasets"],
                weight_type="mass",
                global_query_dict=global_query_dict,
                plot_settings={
                    "show_plot": False,
                    "output_name": os.path.join(
                        local_figure_directory,
                        figure_stable_mt_disk_massratio_seperation_massweighted_plot_filename,
                    ),
                },
                testing=testing,
                verbose=verbose,
            )
            pdfname = os.path.join(
                local_figure_directory,
                figure_stable_mt_disk_massratio_seperation_massweighted_plot_filename,
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                pdfname,
                pdf_page_number,
                bookmark_text="stable_mt_disk_massratio_seperation_massweighted_plot",
            )

        ########################################
        # PLOT: canvas for massratio and racc for normal distribution TIME WEIGHT
        if len(list(sim_dict["datasets"].keys())) == 3:
            figure_stable_mt_disk_massratio_seperation_timeweighted_plot_filename = (
                "massratio_separation_histogram_time_weighted.pdf"
            )
            print(
                "Plotting ",
                figure_stable_mt_disk_massratio_seperation_timeweighted_plot_filename,
            )
            plot_massratio_separation_histogram_one_quantity_three_metallicities(
                sim_dict["datasets"],
                weight_type="time",
                global_query_dict=global_query_dict,
                plot_settings={
                    "show_plot": False,
                    "output_name": os.path.join(
                        local_figure_directory,
                        figure_stable_mt_disk_massratio_seperation_timeweighted_plot_filename,
                    ),
                },
                testing=testing,
                verbose=verbose,
            )
            pdfname = os.path.join(
                local_figure_directory,
                figure_stable_mt_disk_massratio_seperation_timeweighted_plot_filename,
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                pdfname,
                pdf_page_number,
                bookmark_text="stable_mt_disk_massratio_seperation_timeweighted_plot",
            )

    #
    print(
        "Creating combined pdf for {}".format(
            sim_dict["simname"],
            "(query: {})".format(global_query_dict["global_query_name"])
            if global_query_dict
            else "",
        )
    )

    # wrap up the pdf
    merger.write(combined_output_filename)
    merger.close()
    print("\t wrote combined pdf to {}".format(combined_output_filename))

    print(
        "Finished generating plots for {}".format(
            sim_dict["simname"],
            "(query: {})".format(global_query_dict["global_query_name"])
            if global_query_dict
            else "",
        )
    )
