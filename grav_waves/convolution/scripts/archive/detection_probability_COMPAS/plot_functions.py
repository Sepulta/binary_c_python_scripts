"""
Function to store the plotting routines for the detection probability calculations
"""

import copy

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors

import astropy.units as u
from grav_waves.settings import cosmo

from COMPAS.utils.PythonScripts.CosmicIntegration.selection_effects import (
    detection_probability,
)

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

from grav_waves.convolution.scripts.detection_probability_COMPAS.functions import (
    find_largest_redshift_value,
)


def plot_z_domain(sensitivity, plot_settings=None):
    """
    Function to plot a 2-piece density map with the maximum redshift and the comoving volume that corresponds to
    """

    snr_threshold = 7

    min_mass = 10
    max_mass = 100
    mass_step = 2

    #
    mass_1_array = np.arange(min_mass, max_mass, mass_step)
    mass_2_array = np.arange(min_mass, max_mass, mass_step)

    #
    XX, YY = np.meshgrid(mass_1_array, mass_2_array)

    total_mass = XX + YY

    # Find the largest redshifts that can be detected for the masses
    XX_flat = XX.flatten()
    YY_flat = YY.flatten()

    largest_redshifts = np.array(
        [
            find_largest_redshift_value(
                XX_flat[i], YY_flat[i], snr_threshold, sensitivity=sensitivity
            )
            for i in range(len(XX_flat))
        ]
    )

    # Calculate the comoving volumes for this
    comoving_volumes = cosmo.comoving_volume(largest_redshifts).to(u.Gpc**3)

    #
    largest_redshifts = np.reshape(largest_redshifts, XX.shape)
    comoving_volumes = np.reshape(comoving_volumes, XX.shape)

    #######################
    # Plotting
    # Set up canvas and grid
    fig = plt.figure(figsize=(16, 16))

    gs = fig.add_gridspec(nrows=2, ncols=6)

    # Set up the subplots
    ax_max_z = fig.add_subplot(gs[0, :-2])
    ax_max_z_cb = fig.add_subplot(gs[0, -1])

    ax_max_volume = fig.add_subplot(gs[1, :-2])
    ax_max_volume_cb = fig.add_subplot(gs[1, -1])

    # Set normalisation and levels
    norm_max_z = colors.Normalize(vmin=0, vmax=largest_redshifts.max())
    norm_comoving = colors.Normalize(vmin=0, vmax=comoving_volumes.max().value)

    ##
    # Create the max redshift plot
    max_z_colormesh = ax_max_z.pcolormesh(
        XX,
        YY,
        largest_redshifts,
        norm=norm_max_z,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )
    cb_max_z = fig.colorbar(max_z_colormesh, cax=ax_max_z_cb)
    cb_max_z.set_label("Maximum redshift", fontsize=20)

    # Create contourlines
    _ = ax_max_z.contour(
        XX,
        YY,
        total_mass,
        levels=4,
        colors="k",
    )
    max_z_contour = ax_max_z.contour(
        XX,
        YY,
        largest_redshifts,
        levels=4,
        colors="k",
    )
    ax_max_z.clabel(max_z_contour)

    # Create the comoving volume spanned by the colormesh
    comoving_vol_colormesh = ax_max_volume.pcolormesh(
        XX,
        YY,
        comoving_volumes.value,
        norm=norm_comoving,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )
    cb_comoving_vol = fig.colorbar(comoving_vol_colormesh, cax=ax_max_volume_cb)
    cb_comoving_vol.set_label(
        "Detectable volume [{}]".format(comoving_volumes.unit), fontsize=20
    )

    # Create contourlines
    _ = ax_max_volume.contour(
        XX,
        YY,
        total_mass,
        levels=4,
        colors="k",
    )
    comoving_vol_contour = ax_max_volume.contour(
        XX,
        YY,
        comoving_volumes.value,
        levels=4,
        colors="k",
    )
    ax_max_z.clabel(comoving_vol_contour)

    # Some makeup
    ax_max_volume.set_xlabel(r"Mass 1 (detector frame) [M$_{\odot}$]", fontsize=20)
    ax_max_volume.set_ylabel(r"Mass 2 (detector frame) [M$_{\odot}$]", fontsize=20)
    ax_max_z.set_ylabel(r"Mass 2 (detector frame) [M$_{\odot}$]", fontsize=20)
    cb_max_z.set_label("Maximum redshift")

    # Set titles
    ax_max_z.set_title(
        "Maximum redshift for finite detectability of system. snr_threshold: {}".format(
            snr_threshold
        )
    )

    #
    ax_max_volume.set_title("Comoving volume spanned by max redshift")

    plot_settings["hspace"] = 0.2

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_detection_probability(
    redshift, snr_threshold, sensitivity, linear=1, plot_settings=None
):
    """
    Function to plot the detection probability for a grid of masses.

    Input:
        redshift: value of the redshift of the sources
        snr_threshold: minimum signal to noise ratio that the we allow for detection
    """

    if not plot_settings:
        plot_settings = {}

    # Calculate distance
    distance_in_mpc = cosmo.luminosity_distance(redshift)

    # Set some stuff for the mass arrays
    min_mass = 5
    max_mass = 100
    mass_stepsize = 1

    # Create arrays
    mass_1_detector_frame_array = np.arange(min_mass, max_mass, mass_stepsize)
    mass_2_detector_frame_array = np.arange(min_mass, max_mass, mass_stepsize)

    # Transform to source arrays
    mass_1_source_frame_array = mass_1_detector_frame_array * (1 + redshift)
    mass_2_source_frame_array = mass_2_detector_frame_array * (1 + redshift)

    # Create meshgrids for both source and detector frames
    XX_detector, YY_detector = np.meshgrid(
        mass_1_detector_frame_array, mass_2_detector_frame_array
    )
    XX_source, YY_source = np.meshgrid(
        mass_1_source_frame_array, mass_2_source_frame_array
    )

    # Calculate detection probabilities
    current_detection_probability = detection_probability(
        XX_source,
        YY_source,
        redshift,
        distance_in_mpc.value,
        snr_threshold,
        sensitivity=sensitivity,
    )

    # Make sure that those that are above 1 are set to 1
    current_detection_probability[current_detection_probability > 1] = 1

    # Find lowest value available
    if len(current_detection_probability[current_detection_probability != 0]) != 0:
        min_value = current_detection_probability[
            current_detection_probability != 0
        ].min()
    else:
        min_value = 1e-9

    # Set normalisation and levels
    if linear:
        norm = colors.Normalize(vmin=min_value, vmax=1)
        levels = [0.1, 0.25, 0.68, 0.95]
    else:
        norm = colors.LogNorm(vmin=min_value, vmax=1)
        levels = [1e-4, 1e-3, 1e-2, 1e-1]

    # Create plot
    fig = plt.figure(figsize=(16, 16))

    gs = fig.add_gridspec(nrows=1, ncols=6)

    # Set up the subplots
    ax_detection_prob = fig.add_subplot(gs[0, :-2])
    ax_detection_prob_cb = fig.add_subplot(gs[0, -1])

    # Set colormap
    cmap = copy.copy(plt.cm.jet)
    cmap.set_under(color="white")

    # Create colormesh
    detection_prob_colormesh = ax_detection_prob.pcolormesh(
        XX_detector,
        YY_detector,
        current_detection_probability,
        norm=norm,
        cmap=cmap,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )
    cb_detection_prob = fig.colorbar(detection_prob_colormesh, cax=ax_detection_prob_cb)
    cb_detection_prob.set_label("Detection probability", fontsize=20)

    # Create contourlines
    CS = ax_detection_prob.contour(
        XX_detector,
        YY_detector,
        current_detection_probability,
        levels=levels,
        colors="k",
    )
    try:
        ax_detection_prob.clabel(CS, levels)
    except ValueError:
        print("None of the levels {} are available".format(levels))

    # Some makeup
    ax_detection_prob.set_xlabel(r"Mass 1 (detector frame) [M$_{\odot}$]")
    ax_detection_prob.set_ylabel(r"Mass 2 (detector frame) [M$_{\odot}$]")

    ax_detection_prob.set_title(
        "Detection probability distribution at redshift z={} (d={:.2e})".format(
            redshift, distance_in_mpc
        )
    )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
