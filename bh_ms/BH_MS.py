import os
import json
import time
import pickle
import sys

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import (
    get_help_all,
    get_help,
    create_hdf5,
    output_lines,
)

###
# Script to generate BH MS systems.


def parse_function(self, output):
    # extract info from the population instance
    # TODO: think about whether this is smart. Passing around this object might be an overkill

    # Get some information from the
    data_dir = self.custom_options["data_dir"]
    base_filename = self.custom_options["base_filename"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # Create filename
    outfilename = os.path.join(data_dir, base_filename)

    # Go over the output.
    for el in output_lines(output):
        headerline = el.split()[0]

        # CHeck the header and act accordingly
        if headerline == "DAVID_BHMS":
            parameters = [
                "time",
                "mass_1",
                "zams_mass_1",
                "mass_2",
                "zams_mass_2",
                "stellar_type_1",
                "prev_stellar_type_1",
                "stellar_type_2",
                "prev_stellar_type_2",
                "metallicity",
                "probability",
                "separation",
                "eccentricity",
                "period",
                "zams_period",
                "zams_separation",
                "zams_eccentricity",
                "prev_eccentricity",
                "prev_period",
                "prev_separation",
                "prev_mass_bh",
                "prev_core_mass_bh",
                "sn_type_bh",
            ]
            values = el.split()[1:]
            seperator = "\t"

            if not os.path.exists(outfilename):
                with open(outfilename, "w") as f:
                    f.write(seperator.join(parameters) + "\n")

            with open(outfilename, "a") as f:
                f.write(seperator.join(values) + "\n")


## Set values
test_pop = Population()
test_pop.set(
    C_logging_code="""
Star_number k;

Starloop(k)
{
    if((stardata->star[k].stellar_type==BH) && (stardata->star[Other_star(k)].stellar_type==MS))
    {
        if (stardata->model.time < stardata->model.max_evolution_time)
        {
            //                       1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
            Printf("DAVID_BHMS %30.12e %g %g %g %g %d %d %d %d %g %g %g %g %g %g %g %g %g %g %g %g %g %d\\n",
                //
                stardata->model.time, // 1

                // Some masses
                stardata->star[0].mass, //2
                stardata->star[0].pms_mass, //3
                stardata->star[1].mass, //4
                stardata->star[1].pms_mass, //5

                // Stellar typ info
                stardata->star[0].stellar_type, //6
                stardata->previous_stardata->star[0].stellar_type, //7
                stardata->star[1].stellar_type, //8
                stardata->previous_stardata->star[1].stellar_type, //9

                // model stuff
                stardata->common.metallicity, //10
                stardata->model.probability, //11

                // orbit stuff
                stardata->common.orbit.separation, //12
                stardata->common.orbit.eccentricity, //13
                stardata->common.orbit.period, //14

                // Initial orbit parameters
                stardata->common.zams_period,           //15
                stardata->common.zams_separation,       //16
                stardata->common.zams_eccentricity,     //17

                // Previous orbit parameters
                stardata->previous_stardata->common.orbit.eccentricity,                 //18
                stardata->previous_stardata->common.orbit.period,               //19
                stardata->previous_stardata->common.orbit.separation,                   //20

                // Some info about the SN
                stardata->previous_stardata->star[k].mass,                // 21
                stardata->previous_stardata->star[k].core_mass,           // 22
                stardata->star[k].SN_type                                // 23
            );
        }
        /* Kill the simulation to save time */
        stardata->model.max_evolution_time = stardata->model.time - stardata->model.dtm;
    }
}
"""
)

# Set grid variables
resolution = {"M_1": 10, "q": 5, "per": 10}

test_pop.add_grid_variable(
    name="lnm1",
    longname="Primary mass",
    valuerange=[1, 150],
    resolution="{}".format(resolution["M_1"]),
    spacingfunc="const(math.log(1), math.log(150), {})".format(resolution["M_1"]),
    precode="M_1=math.exp(lnm1)",
    probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 150, -1.3, -2.3, -2.3)*M_1",
    dphasevol="dlnm1",
    parameter_name="M_1",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
)

test_pop.add_grid_variable(
    name="q",
    longname="Mass ratio",
    valuerange=["0.1/M_1", 1],
    resolution="{}".format(resolution["q"]),
    spacingfunc="const(0.1/M_1, 1, {})".format(resolution["q"]),
    probdist="flatsections(q, [{'min': 0.1/M_1, 'max': 0.8, 'height': 1}, {'min': 0.8, 'max': 1.0, 'height': 1.0}])",
    dphasevol="dq",
    precode="M_2 = q * M_1",
    parameter_name="M_2",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
)

test_pop.add_grid_variable(
    name="logper",
    longname="log(Orbital_Period)",
    valuerange=[-2, 12],
    resolution="{}".format(resolution["per"]),
    spacingfunc="np.linspace(-2, 12, {})".format(resolution["per"]),
    precode="orbital_period = 10** logper\n",  # TODO:
    probdist="gaussian(logper,4.8, 2.3, -2.0, 12.0)",
    parameter_name="orbital_period",
    dphasevol="dln10per",
)

metallicity = 0.002
test_pop.set(
    separation=1000000000,
    orbital_period=400000000,
    metallicity=metallicity,
    M_1=100,
    M_2=5,
    verbose=1,
    data_dir=os.path.join(os.environ["BINARYC_DATA_ROOT"], "testing_python", "BHMS"),
    base_filename="BH_MS_z{}.dat".format(metallicity),
    parse_function=parse_function,
    amt_cores=2,
)

out = test_pop.evolve_single()
print(out)

test_pop.evolve_population()

# # Export settings:
test_pop.export_all_info(use_datadir=True)

# # hdf5
create_hdf5(
    test_pop.custom_options["data_dir"], name="BH_MS_z{}.hdf5".format(metallicity)
)
