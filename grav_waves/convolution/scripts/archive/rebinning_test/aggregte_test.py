import pandas as pd

import numpy as np


df_1_data = {
    "mass_1": [1, 2, 3],
    "weight": [1, 2, 3],
    "local_index": [1, 2, 3],
}

df_2_data = {
    "mass_1": [3, 4, 5],
    "weight": [3, 4, 5],
    "local_index": [3, 4, 5],
}

combined_df = pd.DataFrame()
df_1 = pd.DataFrame.from_dict(df_1_data)
df_2 = pd.DataFrame.from_dict(df_2_data)


# combine
combined_df = pd.concat([combined_df, df_1])

aggregate_dict = {el: "first" for el in combined_df.columns}
aggregate_dict["weight"] = "sum"

# Combine the merger rates of the rows that match on local_index. Then the rest we take the first.
combined_df = (
    combined_df.groupby("local_index", as_index=False)
    .aggregate(aggregate_dict)
    .reindex(columns=combined_df.columns)
)
print("\t\tPost-groupby len combined_df: {}".format(len(combined_df.index)))

print(combined_df)

# combine
combined_df = pd.concat([combined_df, df_2])

aggregate_dict = {el: "first" for el in combined_df.columns}
aggregate_dict["weight"] = "sum"

# Combine the merger rates of the rows that match on local_index. Then the rest we take the first.
combined_df = (
    combined_df.groupby("local_index", as_index=False)
    .aggregate(aggregate_dict)
    .reindex(columns=combined_df.columns)
)
print("\t\tPost-groupby len combined_df: {}".format(len(combined_df.index)))

print(combined_df)
