"""
Main point for plotting
"""

import os
import numpy as np

from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.plot_functions.plot_intrinsic_rate_over_redshift import (
    plot_intrinsic_rate_over_redshift,
)
from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.plot_functions.plot_mass_quantity_over_redshift import (
    plot_mass_quantity_over_redshift,
)

## Setting
sim_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_10_EXTRA_MASSLOSS_PPISN/"
dco_type = "combined_dco"

# Targets
target_dir_convolution = os.path.join(sim_dir, "convolution_results_compas_structure")
target_filename_convolution = os.path.join(
    target_dir_convolution, "convolved_{}_compas_structure2.hdf5".format(dco_type)
)

# plot_intrinsic_rate_over_redshift(target_filename_convolution, plot_settings={'show_plot': True})
plot_mass_quantity_over_redshift(
    target_filename_convolution,
    mass_quantity="primary_mass",
    bins=np.arange(0, 60, 1),
    plot_settings={"show_plot": True},
)
