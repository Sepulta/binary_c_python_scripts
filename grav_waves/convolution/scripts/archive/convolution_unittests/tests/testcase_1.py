"""
Testcase 1: 1 system, merging at exactly the first z-bin
"""

import unittest
import pickle

import numpy as np
import astropy.units as u

from grav_waves.gw_analysis.functions.cosmology_functions import (
    lookback_time_to_redshift,
    redshift_to_lookback_time,
)
from grav_waves.gw_analysis.functions.functions import make_dataset_dict
from grav_waves.convolution.scripts.testing_convolution_quantitatively.functions import (
    write_dummy_dataset,
    Capturing,
)
from grav_waves.convolution.functions.convolution_functions import (
    new_convolution_main as convolution_main,
)


class test_case_1(unittest.TestCase):
    """
    Unittests for the convolution
    """

    def test_case_1(self):
        with Capturing() as output:
            self._test_case_1()

    def test_case_1(self):
        """
        Test making a temp directory and comparing that to what it should be
        """

        from grav_waves.convolution.scripts.testing_convolution_quantitatively.main import (
            output_dir,
            config_dict_cosmology,
            convolution_settings,
        )

        #####
        # System parameters: Here we fix the merger time (i.e formation + inspiral, so time it takes from the moment its born)
        #   to the center of the last redshift sample. In this way, we only expect mergers in the first redshift bin (by putting the merger time equal to the furthest redshift sample, it can only merger with that last one, into the first one))
        formation_time_in_years = 1e6
        inspiral_time_in_years = (
            redshift_to_lookback_time(convolution_settings["time_centers"][-1])
            .to(u.yr)
            .value
            - formation_time_in_years
        )
        merger_time_in_years = inspiral_time_in_years + formation_time_in_years

        convolution_settings["total_mass_bins"] = np.arange(0, 100, 5)

        total_mass_1 = 20
        system_parameters_per_metallicity_dict = {
            "0.02": {
                "probability": 1,
                "mass_1": 10,
                "mass_2": 10,
                "formation_time_in_years": formation_time_in_years,
                "merger_time_in_years": merger_time_in_years,
                "inspiral_time_in_years": inspiral_time_in_years,
            },
        }
        write_dummy_dataset(output_dir, system_parameters_per_metallicity_dict)
        dataset_dict_populations = make_dataset_dict(output_dir)
        output_testcase_1_filename = "output_testcase_1.dat"
        convolution_main(
            dataset_dict=dataset_dict_populations,
            convolution_configuration=convolution_settings,
            cosmology_configuration=config_dict_cosmology,
            output_filename=output_testcase_1_filename,
            dco_type="bhbh",
            save_time_bins=convolution_settings["time_bins"],
            store_each_timestep=True,
            verbosity=1,
        )

        ####
        results = pickle.load(open(output_testcase_1_filename, "rb"))
        convolution_settings = results["convolution_configuration"]

        mass_key = "result_array_total_mass"
        rate_result_dict = results["results"]["merger_rates"]

        # get bins
        timebins = convolution_settings["time_bins"]
        time_values = (timebins[1:] + timebins[:-1]) / 2
        mass_bins = convolution_settings["total_mass_bins"]

        #
        main_results = rate_result_dict[mass_key]

        # Check if we only have 1 column with results
        self.assertTrue(np.all(main_results[1:].value == 0))

        index = np.digitize([total_mass_1], mass_bins)[0]
        self.assertTrue(main_results[0][index - 1].value == 1)

        # Check total rate
        self.assertTrue(sum(main_results[0]).value == 1)

        # Check that there is only 1, in the correct place
        print(main_results[0])


if __name__ == "__main__":
    unittest.main()
