import os
import json
import time
import sys

import pandas as pd
import matplotlib.pyplot as plt

import numpy as np

from plot import *


from david_phd_functions.plotting.custom_mpl_settings import (
    load_mpl_rc,
    LINESTYLE_TUPLE,
)

load_mpl_rc()

os.makedirs("plots", exist_ok=True)

metallicity = 0.02
main_results_directory = os.path.join(
    os.environ["BINARYC_DATA_ROOT"],
    "RUNTIME_TESTING",
    "HIGH_RES_SCHNEIDER_MASS_LOSS_COMBINE",
    "Z{}".format(metallicity),
    "local_tmp_dir",
)
main_results_directory = "results/local_tmp_dir_lowres"
main_results_directory = "results/local_tmp_dir_lowres_queue"
main_results_directory = "results/local_tmp_dir_old_high_res"
main_results_directory = "results/local_tmp_dir_queue_high_res"


# print("Plotting barcharts")
# plot_barcharts(main_results_directory, plot_settings={
#     'simulation_name': main_results_directory.split('/')[-1],
# })

# print("plotting all runtimes")
# plot_all_runtimes(main_results_directory, plot_settings={
#     'simulation_name': main_results_directory.split('/')[-1],
# })

print("Plotting all the thread histograms")
plot_runtime_hists(
    main_results_directory,
    plot_settings={
        "simulation_name": main_results_directory.split("/")[-1],
    },
)
print("Done")
