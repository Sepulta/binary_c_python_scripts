"""
Script to plot the core mass distribution for DCO mergers merging at z=0

Steps:
- Update the He core masses for naked He stars
- first extract all the pre-SN CO and He core mass 1 and 2
- Calculate the He envelope mass
- generate plot

TODO: make general function to change something from _1, _2 to _primary, _secondary
"""

import os
import json

import h5py
import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

#
from grav_waves.convolution.functions.convolution_general_functions import (
    create_bins_from_centers,
)
from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    generate_mask,
    add_columns_to_df,
    quantity_name_dict,
    quantity_unit_dict,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def plot_core_mass_distributions(dataset_filename, plot_settings={}):
    """
    Function to plot the merger time vs metallicity for source distributions
    """

    # Read out info
    hdf5file = h5py.File(dataset_filename)

    metallicity_bin_centers = np.array(hdf5file["sfr_data/metallicity_bin_centers"][()])
    log10metallicity_values = np.array(sorted(np.log10(metallicity_bin_centers)))
    log10metallicity_bins = create_bins_from_centers(log10metallicity_values)
    metallicity_bins = 10**log10metallicity_bins

    redshift_shell_volume_dict = json.loads(
        hdf5file["shell_volumes/redshift_shell_volume_dict"][()]
    )

    # Get data from zero redshift
    zero_key = str(min([float(el) for el in list(hdf5file["data/merger_rate"].keys())]))
    print(
        "Extracting information for bhbh systems merging at redshift {}".format(
            zero_key
        )
    )
    rate_information_at_redshift_zero = hdf5file[
        "data/merger_rate/{}".format(zero_key)
    ][()]

    hdf5file.close()

    # Read out combined df
    combined_df = pd.read_hdf(dataset_filename, "/data/combined_dataframes")
    combined_df = add_columns_to_df(combined_df, columns=["primary_mass"])

    # Change the He core mass for the stars that are naked He
    naked_He_star_1_mask = combined_df.eval(
        "pre_SN_stellar_type_1 in [7,8,9]"
    ).to_numpy()
    naked_He_star_2_mask = combined_df.eval(
        "pre_SN_stellar_type_2 in [7,8,9]"
    ).to_numpy()

    combined_df.loc[naked_He_star_1_mask, "pre_SN_He_core_mass_1"] = combined_df[
        naked_He_star_1_mask
    ]["pre_SN_mass_1"]
    combined_df.loc[naked_He_star_2_mask, "pre_SN_He_core_mass_2"] = combined_df[
        naked_He_star_2_mask
    ]["pre_SN_mass_2"]

    # Create general mask
    mask = generate_mask(combined_df, general_query=None, dco_type="bhbh")

    #######
    # Get SN 1 info

    # TODO: add extra mask for PPISN SN_1
    sn_1_mask = mask

    # Get the core masses for the SN_1
    pre_SN_CO_core_mass_1 = combined_df[sn_1_mask]["pre_SN_CO_core_mass_1"].to_numpy()
    pre_SN_He_core_mass_1 = combined_df[sn_1_mask]["pre_SN_He_core_mass_1"].to_numpy()
    pre_SN_He_envelope_mass_1 = pre_SN_He_core_mass_1 - pre_SN_CO_core_mass_1
    merger_rate_information_at_redshift_zero_1 = rate_information_at_redshift_zero[
        sn_1_mask
    ]

    #######
    # Get SN 2 info

    # TODO: add extra mask for PPISN SN_2
    sn_2_mask = mask

    # Get the core masses for the SN_1
    pre_SN_CO_core_mass_2 = combined_df[sn_2_mask]["pre_SN_CO_core_mass_2"].to_numpy()
    pre_SN_He_core_mass_2 = combined_df[sn_2_mask]["pre_SN_He_core_mass_2"].to_numpy()
    pre_SN_He_envelope_mass_2 = pre_SN_He_core_mass_2 - pre_SN_CO_core_mass_2
    merger_rate_information_at_redshift_zero_2 = rate_information_at_redshift_zero[
        sn_2_mask
    ]

    #######
    # Combine the results
    pre_SN_CO_core_mass = np.concatenate([pre_SN_CO_core_mass_1, pre_SN_CO_core_mass_2])
    pre_SN_He_envelope_mass = np.concatenate(
        [pre_SN_He_envelope_mass_1, pre_SN_He_envelope_mass_2]
    )
    merger_rate_information_at_redshift_zero = np.concatenate(
        [
            merger_rate_information_at_redshift_zero_1,
            merger_rate_information_at_redshift_zero_2,
        ]
    )

    # helium envelope fraction:
    pre_SN_He_envelope_fraction_CO = pre_SN_He_envelope_mass / pre_SN_CO_core_mass

    # Set up bins
    pre_SN_CO_core_mass_bins = np.arange(0, np.ceil([pre_SN_CO_core_mass.max()]), 1)
    # pre_SN_He_core_mass_bins = np.arange(0, np.ceil([pre_SN_He_envelope_mass.max()]), 1)
    pre_SN_He_envelope_fraction_CO_bins = np.arange(
        0, np.ceil([pre_SN_He_envelope_fraction_CO.max()]), 0.01
    )

    pre_SN_CO_core_mass_centers = (
        pre_SN_CO_core_mass_bins[1:] + pre_SN_CO_core_mass_bins[:-1]
    ) / 2
    # pre_SN_He_core_mass_centers = (pre_SN_He_core_mass_bins[1:]+pre_SN_He_core_mass_bins[:-1])/2
    pre_SN_He_envelope_fraction_CO_centers = (
        pre_SN_He_envelope_fraction_CO_bins[1:]
        + pre_SN_He_envelope_fraction_CO_bins[:-1]
    ) / 2

    #
    hist = np.histogram2d(
        pre_SN_CO_core_mass,
        pre_SN_He_envelope_fraction_CO,
        bins=[pre_SN_CO_core_mass_bins, pre_SN_He_envelope_fraction_CO_bins],
        weights=merger_rate_information_at_redshift_zero,
    )

    X, Y = np.meshgrid(
        pre_SN_CO_core_mass_centers, pre_SN_He_envelope_fraction_CO_centers
    )

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=11)

    ax = fig.add_subplot(gs[:, :-2])
    ax_cb = fig.add_subplot(gs[:, -1])

    # ##################
    # # Plot the data

    # # Get the normalisation
    # if scale == 'linear':
    #     norm = colors.Normalize(
    #         vmin=hist[0].min(),
    #         vmax=hist[0].max()
    #     )
    # elif scale == 'log':
    norm = colors.LogNorm(
        vmin=10 ** (np.log10(hist[0].max()) - 3),
        vmax=hist[0].max(),
    )

    ##################
    # Plot the MSSFR results
    _ = ax.pcolormesh(
        X,
        Y,
        hist[0].T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # make colorbar
    cb = matplotlib.colorbar.ColorbarBase(ax_cb, norm=norm, extend="min")
    cb.ax.set_ylabel("Number per formed solar mass")

    # #
    # ax.set_yscale('log')
    ax.set_xlabel(r"CO core mass [$M_{\odot}$]")
    ax.set_ylabel(r"He envelope mass [$M_{\odot}$]")
    ax.set_title(
        "CO core mass vs He envelope mass for BHBH merging at Z=0", fontsize=28
    )

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":

    #
    result_root = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results"
    simname = "NEW_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED"
    filename = os.path.join(
        result_root, simname, "dco_convolution_results/dco_convolution_results.h5"
    )

    #
    output_dir = (
        "/home/david/papers/paper_gw/paper_tex/figures/core_mass_distributions/"
    )
    output_dir = "plots/"

    basename = "core_mass_distributions.pdf"
    output_name = os.path.join(output_dir, basename)

    #
    plot_core_mass_distributions(
        dataset_filename=filename,
        plot_settings={"show_plot": True, "output_name": output_name},
    )
