"""
File containing some global settings for this project
"""

#
project_specific_bse_settings = {
    "minimum_timestep": 1e-8,  # This is required by the new Hurley winds
    "BH_prescription": 3,  # Discussed that the delayed would probably be more appropriate
    "PPISN_prescription": 2,  # On default we should use out _new_ fits
    "wind_mass_loss": 4,  # This is Schneider but using the hurley LBV again
    "lambda_ce": -1,  # Use the dewi & tauris as the default for us
    "accretion_limit_thermal_multiplier": 10,  # Use the multiplier that was used in Hurley as default
}

#
project_specific_grid_settings = {
    # binarycpython control
    "max_queue_size": 20000,  # Allow the queue to be filled alot
    "failed_systems_threshold": 200000,  # Log as many failed systems as we can
}

#
project_specific_settings = {
    **project_specific_bse_settings,
    **project_specific_grid_settings,
}


#
variation_name_dicts = {
    "overspin_algorithm": {0: "BSE", 1: "OVERSPIN_MASSLOSS"},
    "distributions": {
        "normal": "SANA",
        "ms": "MOE_DISTEFANO",
    },
}
