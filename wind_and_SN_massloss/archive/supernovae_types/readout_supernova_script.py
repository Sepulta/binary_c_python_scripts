import h5py
import json
import pandas as pd
import numpy as np
import os

import matplotlib.pyplot as plt

from david_phd_functions.plotting import custom_mpl_settings

from binarycpython.utils.stellar_types import (
    STELLAR_TYPE_DICT_SHORT as stellar_type_dict_short,
)

custom_mpl_settings.load_mpl_rc()


"""
TODO: re-run these and see whether the output masses of the remnants actually make sense with the current day information
"""


def load_file_return_df(filename, dataset_name):
    # Load files
    # file_name = '/home/david/projects/binary_c_root/results/testing_python/supernova_mass_test/SN_types_z0.002.hdf5'
    f = h5py.File(filename, "r")

    # Show some keys
    for key in f.keys():
        print(key)  # Names of the groups in HDF5 file.

    for key in f["data"].keys():
        print(key)

    for key in f["settings"].keys():
        print(key)

    # Load data and put in dataframe
    data = f["data"][dataset_name][()]
    settings = f["settings"]["used_settings"][()]
    settings_json = json.loads(settings)

    headers = f["data"][dataset_name + "_header"][()]
    headers = [header.decode("utf-8") for header in headers]

    metallicity = json.loads(settings)["binary_c_defaults"]["metallicity"]

    # Make df
    df = pd.DataFrame(data)
    df.columns = headers
    df["mass_lost_during_evolution"] = df["zams_mass_1"] - df["prev_mass_1"]
    df["mass_lost_during_supernova"] = df["prev_mass_1"] - df["mass_1"]

    return df


df_002 = load_file_return_df(
    os.path.join(
        "/home/david/projects/binary_c_root/results/testing_python/supernova_mass_test/SN_types_z0.02",
        "SN_types_z0.02.hdf5",
    ),
    dataset_name="SN_types_z0.02",
)
df_001 = load_file_return_df(
    os.path.join(
        "/home/david/projects/binary_c_root/results/testing_python/supernova_mass_test/SN_types_z0.01",
        "SN_types_z0.01.hdf5",
    ),
    dataset_name="SN_types_z0.01",
)
df_0002 = load_file_return_df(
    os.path.join(
        "/home/david/projects/binary_c_root/results/testing_python/supernova_mass_test/SN_types_z0.002",
        "SN_types_z0.002.hdf5",
    ),
    dataset_name="SN_types_z0.002",
)


def plot_massloss(df, metallicity):
    """
    Function to plot the massloss
    """

    # Create a stack plot of where what mass is lost
    x = df["zams_mass_1"]
    y1 = df["mass_1"]
    y2 = df["mass_lost_during_supernova"]
    y3 = df["mass_lost_during_evolution"]

    # y = np.vstack([y1, y2, y3])
    labels = ["remnant mass ", "Mass lost during SN", "Mass lost during evolution"]
    fig, ax = plt.subplots()
    ax.stackplot(x, y1, y2, y3, labels=labels)
    ax.legend(loc="upper left")

    ax.set_title("Mass lost during evolution of stars at Z={}".format(metallicity))
    ax.set_xlabel(r"Mass ZAMS (M$_{\odot}$)")
    ax.set_ylabel(r"Mass lost (M$_{\odot}$)")

    plt.show()


plot_massloss(df_002, 0.02)
plot_massloss(df_001, 0.01)
plot_massloss(df_0002, 0.002)

# print(min(df_001['zams_mass_1']))


# quit()


def plot_SNtype_vs_time(df):
    """
    Function to plot the supernovae.
    """

    # Select differnt systems
    df_sn2 = df[df["SN_type"] == 12]
    df_sn1bc = df[df["SN_type"] == 11]

    #
    plt.plot(df_sn2["zams_mass_1"], df_sn2["time"], "bo", label="SN type 2")
    plt.plot(df_sn1bc["zams_mass_1"], df_sn1bc["time"], "r-", label="SN type 1bc")
    plt.title("Lifetime of star until supernova per mass at {}".format(metallicity))
    plt.yscale("log")
    plt.xlabel(r"Mass (solarmass)")
    plt.ylabel("Time (Myr)")
    plt.legend()
    plt.show()


# plot_massloss(df, metallicity)
# plot_SNtype_vs_time(df, metallicity)


def plot_timedelay_distribution():
    """
    Function to plot time delay distribution of different kinds of supernovae
    """

    # Select differnt systems
    df_sn2 = df[df["SN_type"] == 12]
    df_sn1bc = df[df["SN_type"] == 11]

    values_total, edges_total = np.histogram(
        df["time"], bins=50, weights=df["probability"]
    )
    centers_total = (edges_total[1:] + edges_total[:-1]) / 2

    values_sn2, edges_sn2 = np.histogram(
        df_sn2["time"], bins=edges_total, weights=df_sn2["probability"]
    )
    centers_sn2 = (edges_sn2[1:] + edges_sn2[:-1]) / 2

    values_sn1bc, edges_sn1bc = np.histogram(
        df_sn1bc["time"], bins=edges_total, weights=df_sn1bc["probability"]
    )
    centers_sn1bc = (edges_sn1bc[1:] + edges_sn1bc[:-1]) / 2

    plt.plot(centers_total, values_total, label="total")
    plt.plot(centers_sn2, values_sn2, label="SN2")
    plt.plot(centers_sn1bc, values_sn1bc, label="SN1bc")

    plt.legend()

    plt.xscale("log")
    plt.yscale("log")

    plt.title(
        "Time delay distribution for different kinds of supernovae at Z={}".format(
            metallicity
        )
    )
    plt.xlabel("Time (Myr)")
    plt.ylabel("Probability")

    plt.show()


# plot_timedelay_distribution()
