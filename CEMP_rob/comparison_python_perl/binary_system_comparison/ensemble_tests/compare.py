"""
Script to plot the differences in the ensemble output from binary_c with perl vs that of python

TODO: add plotting routine that shows the values of both datasets on a pane, and the ratio between them if they are both present
"""

import os
import time
import json

from PyPDF2 import PdfFileMerger

from CEMP_rob.ensemble_comparison_functions.functions import (
    getFromDict,
    show_differences_between_dicts,
    plot_differences_between_dicts,
)

import matplotlib.pyplot as plt

from david_phd_functions.plotting.custom_mpl_settings import (
    load_mpl_rc,
    LINESTYLE_TUPLE,
)

load_mpl_rc()

robs_ensemble_filename = "template_results/perl/results/ensemble.json"
davids_ensemble_filename = "template_results/python/results/ensemble_output.json"

# Load results
with open(robs_ensemble_filename, "r") as f_rob:
    robs_ensemble = json.loads(f_rob.read())

with open(davids_ensemble_filename, "r") as f_david:
    davids_ensemble = json.loads(f_david.read())

# # Print which keys exist
# print(robs_ensemble.keys())
# print(davids_ensemble.keys())

# # Print the 'mass into stars'
# print("mass into stars")
# print("Robs ensemble: {}".format(robs_ensemble['mass_into_stars']))
# print("davids ensemble: {}".format(davids_ensemble['metadata']['total_probability_weighted_mass']))

# # Print the ensemble keys
# print(robs_ensemble['ensemble'].keys())
# print(davids_ensemble['ensemble'].keys())

#
subkeys_counts = ["number_counts", "stellar_type", "0"]

# show_differences_between_dicts(robs_ensemble, davids_ensemble, subkeys_initial_mass)
pdf_output_files = []

# Compare initial mass distribution
subkeys_initial_mass = [
    "distributions",
    "initial",
    "log mass : labelint->dist",
    "star",
    "0",
    "log mass",
]
plotname = "plots/initial_mass.pdf"
plot_differences_between_dicts(
    robs_ensemble,
    davids_ensemble,
    subkeys_counts,
    plot_settings={"output_name": plotname},
    to_float=False,
    normalize=False,
)
pdf_output_files.append(plotname)


# Compare initial luminosity distribution
# TODO: ask rob why thats called log mass in the luminosity?
subkeys_luminosity = [
    "distributions",
    "initial",
    "log luminosity : labelint->dist",
    "star",
    "0",
    "log mass",
]
plotname = "plots/initial_luminosity.pdf"
plot_differences_between_dicts(
    robs_ensemble,
    davids_ensemble,
    subkeys_luminosity,
    plot_settings={"output_name": plotname},
    normalize=False,
)
pdf_output_files.append(plotname)

# Compare initial v_eq distribution
subkeys_veq = [
    "distributions",
    "initial",
    "log v_eq : labelint->dist",
    "star",
    "0",
    "log v_eq",
]
plotname = "plots/initial_veq.pdf"
plot_differences_between_dicts(
    robs_ensemble,
    davids_ensemble,
    subkeys_veq,
    plot_settings={"output_name": plotname},
    normalize=False,
)
pdf_output_files.append(plotname)


#
subkeys_cmd = ["CMD", "star", "0", "GBP-GRP", "-0.525", "G"]
plotname = "plots/cmd.pdf"
plot_differences_between_dicts(
    robs_ensemble,
    davids_ensemble,
    subkeys_cmd,
    plot_settings={"output_name": plotname},
    normalize=False,
)
pdf_output_files.append(plotname)


# After making the pdfs: join them into a big pdf
merger = PdfFileMerger()

for pdf in pdf_output_files:
    merger.append(pdf)

merger.write(os.path.join("plots/ensemble_comparison_plots.pdf"))
merger.close()
