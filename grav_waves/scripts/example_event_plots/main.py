"""
Test ground for plotting with events
"""

import numpy as np
import pandas as pd

from grav_waves.functions.events import print_rows_and_cols

from grav_waves.paper_scripts.primary_mass_distribution_at_redshift_with_variations.functions import (
    get_rate_data,
)

# import matplotlib.pyplot as plt
# from matplotlib import colors
# from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
# from david_phd_functions.plotting import custom_mpl_settings

# custom_mpl_settings.load_mpl_rc()

# import matplotlib

from example_pre_post_ppisn_eccentricity import example_pre_post_ppisn_eccentricity
from example_initial_final_period_unstable_RLOF_plot import (
    example_initial_final_period_unstable_RLOF_plot,
)
from example_unique_RLOF_histories import example_unique_RLOF_histories


##############
# Get the dataframes
# file = "/home/david/data_projects/binary_c_data/GRAV_WAVES/server_results/EVENTS_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5"
file = "/home/david/data_projects/binary_c_data/GRAV_WAVES/server_results/EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5"

combined_dataframes = pd.read_hdf(file, key="data/combined_dataframes")
rate_array = get_rate_data(
    file,
    rate_type="merger_rate",
    redshift_value=0,
    mask=np.ones(len(combined_dataframes.index), dtype=bool),
)[0]

combined_dataframes = combined_dataframes[combined_dataframes.mass_1 < 8]
# print(combined_dataframes.columns)
rlof_event_dataframe = pd.read_hdf(file, key="data/events/RLOF")
sn_event_dataframe = pd.read_hdf(file, key="data/events/SN_BINARY")
# print(sn_event_dataframe.columns)
# quit()
# Get rate array

##############
# print info
print_rows_and_cols(combined_dataframes, "combined_dataframes")
print_rows_and_cols(rlof_event_dataframe, "rlof_event_dataframe")
print_rows_and_cols(sn_event_dataframe, "sn_event_dataframe")

##############
# Merge dataframe with event df

# #######################################################
# # RLOF history plot
# example_unique_RLOF_histories(
#     combined_dataframes=combined_dataframes,
#     rate_array=rate_array,
#     rlof_event_dataframe=rlof_event_dataframe,
# )


# #######################################################
# # RLOF plot
# example_initial_final_period_unstable_RLOF_plot(combined_dataframes=combined_dataframes, rate_array=rate_array, rlof_event_dataframe=rlof_event_dataframe)

# #######################################################
# # SN plot
# example_pre_post_ppisn_eccentricity(combined_dataframes=combined_dataframes, rate_array=rate_array, sn_event_dataframe=sn_event_dataframe)
