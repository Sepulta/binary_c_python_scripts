"""
Function to generate the data for the Lacc/Lzams plots
"""

import os
import numpy as np

from david_phd_functions.binaryc.personal_defaults import personal_defaults
from RLOF.settings import project_specific_binary_c_results

from binarycpython.utils.grid import Population

from RLOF.scripts.theory_plots.routines.ratio_accretion_luminosity_zams_luminosity.parse_function import (
    parse_function,
)
from RLOF.scripts.theory_plots.routines.ratio_accretion_luminosity_zams_luminosity.custom_logging_string import (
    custom_logging_string,
)

from PyPDF2 import PdfFileMerger
from david_phd_functions.plotting.pdf_functions import add_pdf_and_bookmark

from RLOF.scripts.theory_plots.routines.ratio_accretion_luminosity_zams_luminosity.plot_functions import (
    plot_ratio_mdot_thermal_mdot_acc,
    plot_ratio_lacc_lzams,
)


def generate_data_function(
    output_dir, output_filename, extra_physics_settings=None, extra_grid_settings=None
):
    """
    function to generate the data
    """

    # Set resolution
    resolution = {"M_1": 100}

    if extra_physics_settings is None:
        extra_physics_settings = {}

    if extra_grid_settings is None:
        extra_grid_settings = {}

    # Set up population
    test_pop = Population(verbosity=1)

    # Load personal defaults
    test_pop.set(**personal_defaults)

    # Load project specific defaults
    test_pop.set(**project_specific_binary_c_results)

    # Add situational settings
    test_pop.set(
        C_logging_code=custom_logging_string,
        parse_function=parse_function,
        multiplicity=1,
        amt_cores=8,
        metallicity=0.02,
    )

    # Pass some extra physics if we want
    test_pop.set(**extra_physics_settings)

    # Pass some extra grid settings if necessary
    test_pop.set(**extra_grid_settings)

    test_pop.set(data_dir=output_dir, base_filename=output_filename)

    # Get version info of the binary_c build
    version_info = test_pop.return_binary_c_version_info(parsed=True)

    # create local tmp_dir
    test_pop.set(
        tmp_dir=os.path.join(test_pop.custom_options["data_dir"], "local_tmp_dir")
    )
    os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

    # Add the mass in logspace
    test_pop.add_grid_variable(
        name="lnm1",
        longname="Primary mass",
        valuerange=[0.1, 150],
        samplerfunc="const(math.log(0.1), math.log(150), {})".format(resolution["M_1"]),
        precode="M_1=math.exp(lnm1)",
        probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 301, -1.3, -2.3, -2.3)*M_1",
        dphasevol="dlnm1",
        parameter_name="M_1",
        condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
    )

    # Stop the script if the configuration is wrong
    if version_info["macros"]["NUCSYN"] == "on":
        print("Please disable NUCSYN")
        quit()

    if os.path.isfile(
        os.path.join(
            test_pop.custom_options["data_dir"],
            test_pop.custom_options["base_filename"],
        )
    ):
        print("removing previous file")
        os.remove(
            os.path.join(
                test_pop.custom_options["data_dir"],
                test_pop.custom_options["base_filename"],
            )
        )

    # Evolve
    test_pop.evolve()


def generate_data_and_create_plots(
    result_output_dir,
    plot_output_dir,
    output_pdf_filename,
    generate_plots=True,
    generate_results=True,
):
    """
    Function to run create the data and make the plots for the ZAMS accreting stars comparing the luminosity of the star with the boundary layer luminosity
    """

    # Set up some stuff
    result_output_filename = "data.dat"
    full_result_output_filename = os.path.join(
        result_output_dir, result_output_filename
    )

    # Run the scripts to generate the results:
    if generate_results:
        generate_data_function(result_output_dir, result_output_filename)

    # Run the routines to make the plots and add them to a combined pdf
    if generate_plots:
        # Set up the pdf stuff
        merger = PdfFileMerger()
        pdf_page_number = 0

        show_plots = False

        # Plot the ratio mdot thermal mdot accretion
        plot_ratio_mdot_thermal_mdot_acc_filename = os.path.join(
            plot_output_dir, "ratio_mdot_thermal_mdot_acc.pdf"
        )
        plot_ratio_mdot_thermal_mdot_acc(
            full_result_output_filename,
            plot_settings={
                "show_plot": show_plots,
                "output_name": plot_ratio_mdot_thermal_mdot_acc_filename,
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_ratio_mdot_thermal_mdot_acc_filename,
            pdf_page_number,
            bookmark_text="Ratio mdot thermal mdot acc",
        )

        # Plot the ratio Lzams Lbl
        plot_ratio_lacc_lzams_filename = os.path.join(
            plot_output_dir, "ratio_lacc_lzams.pdf"
        )
        plot_ratio_lacc_lzams(
            full_result_output_filename,
            plot_settings={
                "show_plot": show_plots,
                "output_name": plot_ratio_lacc_lzams_filename,
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            plot_ratio_lacc_lzams_filename,
            pdf_page_number,
            bookmark_text="Ratio lzams lacc",
        )

        # wrap up the pdf
        merger.write(output_pdf_filename)
        merger.close()
        print("\t wrote combined pdf to {}".format(output_pdf_filename))
