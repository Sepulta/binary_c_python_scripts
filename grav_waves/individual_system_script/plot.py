"""
Routine to plot the evolution of the system
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import astropy.units as u

import sys

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.linestyle_hatch_colorlist import color_list
from david_phd_functions.plotting import custom_mpl_settings

from binarycpython.utils import stellar_types

custom_mpl_settings.load_mpl_rc()

colors = []
label_fontsize = 20
framealpha = 0.5


def plot_system_evolution(datafile):
    """
    Function to plot the evolution of the system
    """

    df = pd.read_csv(datafile, sep="\s+")
    df["time_myr"] = df["time"] / 1e6

    ####################
    # Info of system
    sn_1_type = list(set(df["sn_type_1"].unique()) - set([0]))[0]
    sn_1_loc = df.query("sn_type_1 == {}".format(sn_1_type)).index

    print("Star 1: ")
    print(
        "Fallback fraction: {} fallback mass: {} SN type: {}".format(
            df["fallback_fraction_1"].max(), df["fallback_mass_1"].max(), sn_1_type
        )
    )
    print(
        "Eccentricity: pre-SN: {} post-SN: {}".format(
            df.iloc[sn_1_loc - 1]["eccentricity"].values[0],
            df.iloc[sn_1_loc]["eccentricity"].values[0],
        )
    )
    print(
        "pre-SN: stellar type: {} Mass: {} Core mass: {} He core mass: {} CO core mass: {}".format(
            df.iloc[sn_1_loc]["pre_event_stellar_type_1"].values[0],
            df.iloc[sn_1_loc]["pre_event_mass_1"].values[0],
            df.iloc[sn_1_loc]["pre_event_core_mass_1"].values[0],
            df.iloc[sn_1_loc]["pre_event_he_core_mass_1"].values[0],
            df.iloc[sn_1_loc]["pre_event_co_core_mass_1"].values[0],
        ),
    )
    print(
        "Post-SN: stellar type: {} Mass: {}".format(
            df.iloc[sn_1_loc]["stellar_type_1"].values[0],
            df.iloc[sn_1_loc]["mass_1"].values[0],
        ),
    )

    sn_2_type = list(set(df["sn_type_2"].unique()) - set([0]))[0]
    sn_2_loc = df.query("sn_type_2 == {}".format(sn_2_type)).index

    print("Star 2: ")
    print(
        "Fallback fraction: {} fallback mass: {} SN type: {}".format(
            df["fallback_fraction_2"].max(), df["fallback_mass_2"].max(), sn_2_type
        )
    )
    print(
        "Eccentricity: pre-SN: {} post-SN: {}".format(
            df.iloc[sn_2_loc - 1]["eccentricity"].values[0],
            df.iloc[sn_2_loc]["eccentricity"].values[0],
        )
    )
    print(
        "pre-SN: stellar type: {} Mass: {} Core mass: {} He core mass: {} CO core mass: {}".format(
            df.iloc[sn_1_loc]["pre_event_stellar_type_2"].values[0],
            df.iloc[sn_1_loc]["pre_event_mass_2"].values[0],
            df.iloc[sn_1_loc]["pre_event_core_mass_2"].values[0],
            df.iloc[sn_1_loc]["pre_event_he_core_mass_2"].values[0],
            df.iloc[sn_1_loc]["pre_event_co_core_mass_2"].values[0],
        ),
    )
    print(
        "Post-SN: stellar type: {} Mass: {}".format(
            df.iloc[sn_1_loc]["stellar_type_2"].values[0],
            df.iloc[sn_1_loc]["mass_2"].values[0],
        ),
    )

    #######################
    # Set up figure logic
    fig = plt.figure(figsize=(60, 20))
    gs = fig.add_gridspec(nrows=6, ncols=1)
    axes_list = []
    axis_index = 0

    ##############
    # Add masses
    masses_axis = fig.add_subplot(gs[axis_index, :])
    axes_list.append(masses_axis)

    masses_axis.plot(
        df["time_myr"],
        df["zams_mass_1"],
        "--",
        c=color_list[0],
        alpha=0.5,
        label="ZAMS M1",
    )
    masses_axis.plot(
        df["time_myr"],
        df["zams_mass_2"],
        "--",
        c=color_list[1],
        alpha=0.5,
        label="ZAMS M2",
    )
    masses_axis.plot(
        df["time_myr"],
        df["zams_mass_2"] + df["zams_mass_1"],
        "--",
        c=color_list[2],
        alpha=0.5,
        label="ZAMS Mtot",
    )
    masses_axis.plot(df["time_myr"], df["mass_1"], c=color_list[0], label="M1")
    masses_axis.plot(df["time_myr"], df["mass_2"], c=color_list[1], label="M2")
    masses_axis.plot(
        df["time_myr"], df["mass_1"] + df["mass_2"], c=color_list[2], label="Mtot"
    )

    masses_axis.set_ylabel("Mass [Msun]", fontsize=label_fontsize)
    masses_axis.legend(ncol=2, framealpha=framealpha, frameon=True)

    ##############
    # Add radii
    axis_index += 1
    radii_axis = fig.add_subplot(gs[axis_index, :], sharex=masses_axis)
    axes_list.append(radii_axis)

    radii_axis.plot(
        df["time_myr"],
        df["roche_radius_1"],
        "--",
        c=color_list[0],
        alpha=0.5,
        label="RL 1",
    )
    radii_axis.plot(
        df["time_myr"],
        df["roche_radius_2"],
        "--",
        c=color_list[1],
        alpha=0.5,
        label="RL 2",
    )

    radii_axis.plot(df["time_myr"], df["radius_1"], c=color_list[0], label="R1")
    radii_axis.plot(df["time_myr"], df["radius_2"], c=color_list[1], label="R2")

    radii_axis.plot(df["time_myr"], df["separation"], ":", c=color_list[2], label="Sep")
    radii_axis.set_ylabel("R & sep [Rsun]", fontsize=label_fontsize)
    radii_axis.set_yscale("log")
    radii_axis.legend(ncol=3, framealpha=framealpha, frameon=True)

    ##############
    # Add stellar type
    axis_index += 1
    stellartype_axis = fig.add_subplot(gs[axis_index, :], sharex=masses_axis)
    axes_list.append(stellartype_axis)

    stellartype_axis.plot(
        df["time_myr"],
        df["stellar_type_1"],
        # "--",
        c=color_list[0],
        # alpha=0.5,
        label="ST 1",
    )
    stellartype_axis.plot(
        df["time_myr"],
        df["stellar_type_2"],
        # "--",
        c=color_list[1],
        # alpha=0.5,
        label="ST 2",
    )
    stellartype_axis.set_ylabel("ST", fontsize=label_fontsize)

    # Get unique STs and make axhlines
    unique_stellar_types = np.unique(
        np.array(df["stellar_type_1"].tolist() + df["stellar_type_2"].tolist())
    )
    for st in unique_stellar_types:
        stellartype_axis.axhline(st, linestyle="--", alpha=0.25)

    y_2_range_st = range(unique_stellar_types.min(), unique_stellar_types.max() + 1)
    st_names = [
        stellar_types.STELLAR_TYPE_DICT_SHORT[el] if el in unique_stellar_types else ""
        for el in y_2_range_st
    ]

    stellartype_axis_twin_y = stellartype_axis.twinx()
    stellartype_axis_twin_y.set_ylim(stellartype_axis.get_ylim())  # same limits
    stellartype_axis_twin_y.set_yticks(list(y_2_range_st))
    stellartype_axis_twin_y.set_yticklabels(st_names, fontsize=15)

    ##############
    # Eccentricity
    axis_index += 1
    eccentricity_axis = fig.add_subplot(gs[axis_index, :], sharex=masses_axis)
    axes_list.append(eccentricity_axis)

    eccentricity_axis.plot(
        df["time_myr"],
        df["eccentricity"],
        # "--",
        c=color_list[0],
        # alpha=0.5,
        label="Ecc",
    )
    eccentricity_axis.set_ylabel("Ecc", fontsize=label_fontsize)
    eccentricity_axis.set_yscale("log")
    # ##############
    # # RLOF counter
    # rlof_counter_axis = fig.add_subplot(gs[4, :], sharex=masses_axis)
    # axes_list.append(rlof_counter_axis)

    # rlof_counter_axis.plot(
    #     df["time_myr"],
    #     df["comenv_count"],
    #     # "--",
    #     c=color_list[0],
    #     # alpha=0.5,
    #     label="MT CE count",
    # )
    # rlof_counter_axis.plot(
    #     df["time_myr"],
    #     df["stable_rlof_count"],
    #     # "--",
    #     c=color_list[1],
    #     # alpha=0.5,
    #     label="MT stable count",
    # )
    # rlof_counter_axis.plot(
    #     df["time_myr"],
    #     df["rlof_count"],
    #     "--",
    #     c=color_list[2],
    #     alpha=0.5,
    #     label="MT total count",
    # )
    # rlof_counter_axis.plot(
    #     df["time_myr"],
    #     df["RLOF_episode_number"],
    #     "--",
    #     c=color_list[3],
    #     alpha=0.5,
    #     label="RLOF episode",
    # )
    # rlof_counter_axis.set_ylabel("Counter", fontsize=label_fontsize)

    #######
    # Kicks
    axis_index += 1
    kick_axis = fig.add_subplot(gs[axis_index, :], sharex=masses_axis)
    axes_list.append(kick_axis)

    kick_axis.plot(
        df["time_myr"],
        df["kick_velocity_1"],
        # "--",
        c=color_list[0],
        alpha=0.5,
        label="vk1",
    )
    kick_axis.plot(
        df["time_myr"],
        df["kick_velocity_2"],
        # "--",
        c=color_list[1],
        alpha=0.5,
        label="vk2",
    )

    # Calculate orbital velocity of system
    sep_1 = (
        (df["mass_2"] / (df["mass_1"] + df["mass_2"])) * df["separation"]
    ).to_numpy() * u.Rsun
    sep_2 = (
        (df["mass_1"] / (df["mass_1"] + df["mass_2"])) * df["separation"]
    ).to_numpy() * u.Rsun

    v1 = (2 * np.pi * sep_1) / (df["orbital_period"].to_numpy() * u.yr)
    v2 = (2 * np.pi * sep_2) / (df["orbital_period"].to_numpy() * u.yr)
    vrel = (2 * np.pi * df["separation"].to_numpy() * u.Rsun) / (
        df["orbital_period"].to_numpy() * u.yr
    )

    kick_axis.plot(
        df["time_myr"],
        v1.to(u.km / u.s).value,
        "--",
        c=color_list[0],
        alpha=0.5,
        label="vorb1",
    )
    kick_axis.plot(
        df["time_myr"],
        v2.to(u.km / u.s).value,
        "--",
        c=color_list[1],
        alpha=0.5,
        label="vorb2",
    )
    kick_axis.plot(
        df["time_myr"],
        vrel.to(u.km / u.s).value,
        "--",
        c=color_list[2],
        alpha=0.5,
        label="vrel",
    )

    kick_axis.set_ylabel("vkick")
    kick_axis.legend(ncol=3, framealpha=framealpha, frameon=True)

    ######
    # Fallback
    axis_index += 1
    fallback_axis = fig.add_subplot(gs[axis_index, :], sharex=masses_axis)
    axes_list.append(fallback_axis)

    fallback_axis.plot(
        df["time_myr"],
        df["fallback_fraction_1"],
        # "--",
        c=color_list[0],
        alpha=0.5,
        label="fb frac 1",
    )
    fallback_axis.plot(
        df["time_myr"],
        df["fallback_fraction_2"],
        # "--",
        c=color_list[1],
        alpha=0.5,
        label="fb frac 2",
    )
    fallback_axis_twin_y = fallback_axis.twinx()
    fallback_axis_twin_y.plot(
        df["time_myr"],
        df["fallback_mass_1"],
        "--",
        c=color_list[0],
        alpha=0.5,
        label="frac mass 1",
    )
    fallback_axis_twin_y.plot(
        df["time_myr"],
        df["fallback_mass_2"],
        "--",
        c=color_list[1],
        alpha=0.5,
        label="frac mass 2",
    )
    fallback_axis_twin_y.legend(framealpha=framealpha, frameon=True)
    fallback_axis_twin_y.set_label("fb")

    # ######
    # # MT
    # axis_index += 1
    # MT_axis = fig.add_subplot(gs[axis_index, :], sharex=masses_axis)
    # axes_list.append(MT_axis)

    # MT_axis.plot(
    #     df["time_myr"],
    #     df["donor_MT_rate"],
    #     # "--",
    #     c=color_list[0],
    #     alpha=0.5,
    #     label="Donor MT",
    # )
    # MT_axis.plot(
    #     df["time_myr"],
    #     df["accretor_accretion_rate"],
    #     # "--",
    #     c=color_list[1],
    #     alpha=0.5,
    #     label="accretor MT",
    # )

    # don = df["donor_MT_rate"].to_numpy()
    # abs_don = np.abs(don)
    # min_don = np.min(abs_don[abs_don > 0])

    # acc = df["donor_MT_rate"].to_numpy()
    # abs_acc = np.abs(acc)
    # min_acc = np.min(abs_acc[abs_acc > 0])

    # min_val = np.min([min_don, min_acc])

    # MT_axis.set_yscale("symlog", linthresh=min_val)

    ##############
    # Makeup
    plt.show()
