import os


def parse_function_ppisn_mass_removal(self, output):
    """
    ouptut function to find the PPISN information
    """

    # Get some information from the object
    data_dir = self.custom_options["data_dir"]
    base_filename = self.custom_options["base_filename"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # set the separator
    seperator = " "

    # Create filename
    outfilename = os.path.join(data_dir, base_filename)

    # params
    params = [
        "time",
        "mass",
        "pre_sn_mass",
        "zams_mass",
        "sn_type",
        "stellar_type",
        "pre_sn_stellar_type",
        "probability",
        "pre_sn_core_mass",
        "pre_sn_co_core_mass",
        "pre_sn_he_core_mass",
        "fallback_fraction",
        "fallback_mass",
    ]

    if not os.path.exists(outfilename):
        with open(outfilename, "w") as f:
            f.write(seperator.join(params) + "\n")

    # Go over output
    if output:
        for line in output.splitlines():
            if line.startswith("DAVID_SN"):
                values = line.split()[1:]

                # Check for length
                if not len(values) == len(params):
                    raise ValueError(
                        "Length of values and expected parameters arent the same"
                    )

                with open(outfilename, "a") as f:
                    f.write(seperator.join(values) + "\n")
