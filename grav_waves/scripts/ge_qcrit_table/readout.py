"""
Function to read out and filter the Ge 2020 table for Giant-type stars
"""

import os
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib import colors
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

import matplotlib

from david_phd_functions.repo_info.functions import get_git_info_and_time

#
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def plot_table(df, plot_settings={}):
    """
    Function to plot the interpolation table results

    https://stackoverflow.com/questions/27004422/contour-imshow-plot-for-irregular-x-y-z-data
    """

    x_vals = df["Mass"]
    y_vals = df["logRad"]
    z_vals = np.log10(df["qad"])

    ####
    #
    fig = plt.figure(figsize=(20, 20))

    #
    gs = fig.add_gridspec(nrows=1, ncols=12)

    # create axes
    ax = fig.add_subplot(gs[:, :10])
    ax_cb = fig.add_subplot(gs[:, 11])

    # Plot scatter
    ax.scatter(x_vals, y_vals, c=z_vals, s=200)
    ax.set_xscale("log")
    ax.set_ylabel("LogRad [Rsun]")
    ax.set_xlabel("Mass [Msun]")
    # ax.tripcolor(x_vals,y_vals,z_vals, shading='flat', alpha=0.1)
    # ax.tricontourf(x_vals,y_vals,z_vals, 20) # choose 20 contour levels, just to show how good its interpolation is
    norm = colors.Normalize(vmin=z_vals.min(), vmax=z_vals.max())

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        norm=norm,
    )
    cbar.ax.set_ylabel(r"log10(q_crit)")

    # Handle saving
    show_and_save_plot(fig, plot_settings)


def load_table(cleaned=True):
    """
    Function to load the table

    Cleaned: boolean to choose the cleaned version (True) or the original table (False)
    """

    headers_Ad = [
        "Mass",
        "k",
        "logRad",
        "MKH",
        "logRKH",
        "logR*KH",
        "zetaad",
        "qad",
        "Dexp",
        "MKHic",
        "logRKHic",
        "logR*KHic",
        "zetaadic",
        "qadic",
    ]

    if cleaned:
        filename = os.path.join(
            this_file_dir, "Ge_etal_Adiabatic_giants_increasingrad_nomissing.txt"
        )
        df_Ge_Ad = pd.read_csv(
            filename, skiprows=41, header=None, sep="\,", names=headers_Ad
        )
    else:
        # Original
        filename = os.path.join(this_file_dir, "Ge_etal_Adiabatic_giants.txt")
        df_Ge_Ad = pd.read_csv(
            filename, skiprows=41, header=None, sep="\s+", names=headers_Ad
        )

    return df_Ge_Ad


def find_missing_lines(df):
    """
    Function to find the missing lines
    """

    ######
    # get unique interpolation parameter values
    unique_mass_values = df["Mass"].unique()
    unique_logRad_values = df["logRad"].unique()

    # find missing grid points
    for unique_mass in unique_mass_values:
        current_mass_df = df[df.Mass == unique_mass]
        # print(current_mass_df["logRad"].unique())

        # Get missing radii
        missing_radii = set()


def write_csv_and_header(df, output_dir):
    """
    Function to write the csv to the format binary_c knows how to deal with.

    TODO: can we include metadata? perhaps in the header
    """

    ########
    # Define input and output parameters
    input_parameter_list = [
        "Mass",
        "logRad",
    ]
    output_parameter_list = ["qad"]

    num_input_parameters = len(input_parameter_list)
    num_output_parameters = len(output_parameter_list)

    ########
    # Make sure the order is correct and sort
    filtered_df = df[input_parameter_list + output_parameter_list]
    filtered_df = filtered_df.sort_values(by=input_parameter_list)

    ########
    # Get indices of input parameters
    index_Mass = input_parameter_list.index("Mass")
    index_logRad = input_parameter_list.index("logRad")

    ########
    # Get indices of output parameters
    index_qad = output_parameter_list.index("qad")

    ########
    # Handle table CSV writing
    os.makedirs(output_dir, exist_ok=True)
    output_csv_name = os.path.join(output_dir, "RLOF_Ge_2020_table_data.csv")

    # Write to csv
    df.to_csv(output_csv_name, sep=",", index=False, header=True)

    ########
    # Handle header writing
    output_header_name = os.path.join(output_dir, "RLOF_Ge_2020_table_header.h")

    # Get git info
    git_info = get_git_info_and_time()

    # Write header output
    with open(output_header_name, "w") as f:
        # Write top-level explanation
        f.write(
            "// Header file for the Ge 2020 table implementation. Contains some definitions for the table.\n\n"
        )

        # Write git revision information
        f.write(
            "// Generated on: {} with git repository: {} branch: {} commit: {}\n\n".format(
                git_info["datetime_string"],
                git_info["repo_name"],
                git_info["branch_name"],
                git_info["commit_sha"],
            )
        )

        # Write defines
        f.write(
            "#define RLOF_GE2020_QCRIT_TABLE_NUM_INPUT_PARAMETERS {}\n\n".format(
                num_input_parameters
            )
        )
        f.write(
            "#define RLOF_GE2020_QCRIT_TABLE_NUM_OUTPUT_PARAMETERS {}\n\n".format(
                num_output_parameters
            )
        )

        # Add lines containing the input indices
        f.write(
            "#define RLOF_GE2020_QCRIT_TABLE_INPUT_INDEX_MASS {}\n\n".format(index_Mass)
        )
        f.write(
            "#define RLOF_GE2020_QCRIT_TABLE_INPUT_INDEX_LOGRAD {}\n\n".format(
                index_logRad
            )
        )

        # Add lines containing the output indices
        f.write(
            "#define RLOF_GE2020_QCRIT_TABLE_OUTPUT_INDEX_QAD {}\n\n".format(index_qad)
        )


if __name__ == "__main__":

    #########################
    #

    dummy_value = 1e99

    # Load table
    df_Ge_Ad = load_table(cleaned=True)

    # Select only the columns we want
    selected = df_Ge_Ad[["Mass", "logRad", "qad"]]

    # Sort
    selected = selected.sort_values(by=["Mass", "logRad"])

    #########
    # # plot the table
    # plot_table(selected, plot_settings={'show_plot': True})

    #########
    # Write table to output

    # set output file
    if os.getenv("BINARY_C"):
        output_dir = os.path.join(os.getenv("BINARY_C"), "src", "RLOF")
    # output_dir = "results/"

    write_csv_and_header(df=selected, output_dir=output_dir)
