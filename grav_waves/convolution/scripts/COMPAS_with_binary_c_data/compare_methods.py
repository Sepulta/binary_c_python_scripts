"""
Main script to compare the two convolution methods.
"""

import os
import numpy as np

from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.plot_functions.compare_mass_quantity_over_redshift import (
    compare_mass_quantity_over_redshift,
)
from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.plot_functions.compare_total_intrinsic_rate_density_over_redshift import (
    compare_total_intrinsic_rate_density_over_redshift,
)
from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.plot_functions.compare_intrinsic_rate_primary_mass_at_redshift_zero import (
    compare_intrinsic_rate_primary_mass_at_redshift_zero,
)

## Setting
sim_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_10_EXTRA_MASSLOSS_PPISN/"
sim_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/"

dco_type = "combined_dco"

plot_output_dir = os.path.join(sim_dir, "convolution_comparison_plots")
os.makedirs(plot_output_dir, exist_ok=True)

# compas convolution
compas_dir_convolution = os.path.join(sim_dir, "convolution_results_compas_structure")
compas_filename_convolution = os.path.join(
    compas_dir_convolution, "convolved_{}_compas_structure.hdf5".format(dco_type)
)

# custom convolution
custom_filename_convolution = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_10_EXTRA_MASSLOSS_PPISN/convolution_results/without_detection_probability/rebinned_convolution_results.h5"
custom_filename_convolution = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/without_detection_probability/rebinned_convolution_results.h5"

#
show_plots = False
include_custom_plots = True

#
compare_mass_quantity_over_redshift_filename = os.path.join(
    plot_output_dir, "compare_mass_quantity_over_redshift.pdf"
)
compare_mass_quantity_over_redshift(
    compas_filename_convolution,
    custom_filename_convolution,
    mass_quantity="primary_mass",
    bins=np.arange(0, 70, 1),
    include_custom_plot=include_custom_plots,
    plot_settings={
        "show_plot": show_plots,
        "hspace": 0.5,
        "runname": "with probability weighting in compas",
        "output_name": compare_mass_quantity_over_redshift_filename,
    },
)

#
compare_total_intrinsic_rate_density_over_redshift_filename = os.path.join(
    plot_output_dir, "compare_total_intrinsic_rate_density_over_redshift.pdf"
)
compare_total_intrinsic_rate_density_over_redshift(
    compas_filename_convolution,
    custom_filename_convolution,
    mass_quantity="primary_mass",
    bins=np.arange(0, 70, 1),
    include_custom_plot=include_custom_plots,
    plot_settings={
        "show_plot": show_plots,
        "hspace": 0.5,
        "runname": "with probability weighting in compas",
        "output_name": compare_total_intrinsic_rate_density_over_redshift_filename,
    },
)

#
compare_intrinsic_rate_primary_mass_at_redshift_zero_filename = os.path.join(
    plot_output_dir, "compare_intrinsic_rate_primary_mass_at_redshift_zero.pdf"
)
compare_intrinsic_rate_primary_mass_at_redshift_zero(
    compas_filename_convolution,
    custom_filename_convolution,
    mass_quantity="primary_mass",
    bins=np.arange(0, 70, 1),
    bootstraps=24,
    include_custom_plot=include_custom_plots,
    plot_settings={
        "show_plot": show_plots,
        "hspace": 0.5,
        "runname": "with probability weighting in compas",
        "output_name": compare_intrinsic_rate_primary_mass_at_redshift_zero_filename,
    },
)
