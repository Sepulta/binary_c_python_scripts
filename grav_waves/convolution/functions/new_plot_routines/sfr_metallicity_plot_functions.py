"""
Plot routines for the SFR and Z dist
"""

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

#
from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def plot_metallicity_probability_density_distribution(
    redshifts,
    dPdlogZ,
    metallicities,
    sampled_metallicities,
    distribution,
    plot_settings=None,
):
    """
    Function to plot the metallicity distribution
    """

    if plot_settings is None:
        plot_settings = {}

    #
    dPdLogZ_for_sampled_metallicities = dPdlogZ[
        :, np.digitize(sampled_metallicities, metallicities) - 1
    ]

    ###
    # plot metallicity distribution with the chosen distribution
    fig = plt.figure(figsize=(40, 40))
    gs = fig.add_gridspec(nrows=13, ncols=10)

    fig.subplots_adjust(hspace=0.5)

    # Set up axes
    high_res_axis = fig.add_subplot(gs[:6, :-2])
    current_sample_axis = fig.add_subplot(gs[7:, :-2])
    colorbar_axis = fig.add_subplot(gs[:, -1])

    # Set up norm
    norm = mpl.colors.Normalize(
        vmin=np.min([dPdlogZ.min(), dPdLogZ_for_sampled_metallicities.min()]),
        vmax=np.max([dPdlogZ.max(), dPdLogZ_for_sampled_metallicities.max()]),
    )

    ##########
    # High res metallicity probability density distribution
    high_res_axis.pcolormesh(
        redshifts,
        metallicities,
        dPdlogZ.T,
        norm=norm,
        antialiased=True,
        rasterized=True,
    )

    high_res_axis.set_ylabel("Metallicity (Z)")
    high_res_axis.set_title(r"Full dP/dlogZ" + "  " + "{}".format(distribution))

    ##########
    # Currently sampled metallicity probability density distribution
    current_sample_axis.pcolormesh(
        redshifts,
        sampled_metallicities,
        dPdLogZ_for_sampled_metallicities.T,
        norm=norm,
        antialiased=True,
        rasterized=True,
    )

    current_sample_axis.set_xlabel("Redshift (z)")
    current_sample_axis.set_ylabel("Metallicity (Z)")
    current_sample_axis.set_title(
        r"Sampled dP/dlogZ" + "  " + "{}".format(distribution)
    )

    current_sample_axis.set_ylim(high_res_axis.get_ylim())

    # Set up a colorbar
    cbar = mpl.colorbar.ColorbarBase(
        colorbar_axis,
        norm=norm,
    )
    cbar.ax.set_ylabel("dP/dlogZ")

    # Scale
    high_res_axis.set_yscale("log")
    current_sample_axis.set_yscale("log")

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


def sfr_full_dpdlogZ(
    redshifts, sfr_values, dPdlogZ, metallicities, distribution, plot_settings=None
):
    """
    Function to plot the SFR and the combination of dP/dlogZ * dlogZ
    """

    if plot_settings is None:
        plot_settings = {}

    # Multiply by sfr:
    SFR_dPdlogZ = (sfr_values * dPdlogZ.T).T

    ###
    # plot metallicity distribution with the chosen distribution
    fig = plt.figure(figsize=(30, 30))
    gs = fig.add_gridspec(nrows=13, ncols=10)

    fig.subplots_adjust(hspace=0.5)

    # Set up axes
    sfr_axis = fig.add_subplot(gs[:4, :-2])
    high_res_axis = fig.add_subplot(gs[5:, :-2])
    colorbar_axis = fig.add_subplot(gs[:, -1])

    # Plot the SFR
    sfr_axis.plot(redshifts, sfr_values)
    sfr_axis.set_title(r"SFR")
    sfr_axis.set_ylabel(
        "Star formation rate"
        + "  "
        + "[{}]".format(sfr_values.unit.to_string("latex_inline"))
    )

    # Set up norm
    norm = mpl.colors.Normalize(
        vmin=np.min(SFR_dPdlogZ).value, vmax=np.max(SFR_dPdlogZ).value
    )

    #
    high_res_axis.pcolormesh(
        redshifts,
        metallicities,
        SFR_dPdlogZ.T.value,
        norm=norm,
        antialiased=True,
        rasterized=True,
    )

    high_res_axis.set_yscale("log")

    high_res_axis.set_xlabel("Redshift (z)")
    high_res_axis.set_ylabel("Metallicity (Z)")
    high_res_axis.set_title(r"SFR * Full dP/dlogZ" + "  " + "{}".format(distribution))

    # Align axes
    high_res_axis.set_xlim(sfr_axis.get_xlim())

    # Set up a colorbar
    cbar = mpl.colorbar.ColorbarBase(
        colorbar_axis,
        norm=norm,
    )
    cbar.ax.set_ylabel(
        "SFR * dP/dlogZ"
        + "  "
        + "[{}]".format(SFR_dPdlogZ.unit.to_string("latex_inline"))
    )

    #
    contour_levels = [1e6, 1e7, 2 * 1e7]
    contour_linestyles = ["solid", "--", "-."]

    #
    _ = high_res_axis.contour(
        redshifts,
        metallicities,
        SFR_dPdlogZ.T.value,
        levels=contour_levels,
        colors="k",
        linestyles=contour_linestyles,
    )
    for contour_i, _ in enumerate(contour_levels):
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [contour_levels[contour_i]] * 2,
            "black",
            linestyle=contour_linestyles[contour_i],
        )

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


def sfr_full_dpdlogZ_dlogZ(
    redshifts, sfr_values, dPdlogZ, metallicities, distribution, plot_settings=None
):
    """
    Function to plot the SFR and the combination of dP/dlogZ * dlogZ
    """

    if plot_settings is None:
        plot_settings = {}

    # Plot the SFR * dP/dlogZ * dlogZ
    log_metallicities = np.log(metallicities)

    extended_log_metallicities = np.append(
        np.array(log_metallicities[0] - np.diff(log_metallicities)[0]),
        log_metallicities,
    )
    dlogZ = np.diff(extended_log_metallicities)

    # Calculate full probability
    P = dPdlogZ * dlogZ

    # Multiply by sfr:
    SFR_P = (sfr_values * P.T).T

    ###
    # plot metallicity distribution with the chosen distribution
    fig = plt.figure(figsize=(30, 30))
    gs = fig.add_gridspec(nrows=13, ncols=10)

    fig.subplots_adjust(hspace=0.5)

    # Set up axes
    sfr_axis = fig.add_subplot(gs[:4, :-2])
    high_res_axis = fig.add_subplot(gs[5:, :-2])
    colorbar_axis = fig.add_subplot(gs[:, -1])

    # Plot the SFR
    sfr_axis.plot(redshifts, sfr_values)
    sfr_axis.set_title(r"SFR")
    sfr_axis.set_ylabel(
        "Star formation rate"
        + "  "
        + "[{}]".format(sfr_values.unit.to_string("latex_inline"))
    )

    # Set up norm
    norm = mpl.colors.Normalize(vmin=np.min(SFR_P).value, vmax=np.max(SFR_P).value)

    #
    high_res_axis.pcolormesh(
        redshifts,
        metallicities,
        SFR_P.T.value,
        norm=norm,
        antialiased=True,
        rasterized=True,
    )

    high_res_axis.set_yscale("log")

    high_res_axis.set_xlabel("Redshift (z)")
    high_res_axis.set_ylabel("Metallicity (Z)")
    high_res_axis.set_title(
        r"SFR * Full dP/dlogZ * dlogZ" + "  " + "{}".format(distribution)
    )

    high_res_axis.set_xlim(sfr_axis.get_xlim())

    # Set up a colorbar
    cbar = mpl.colorbar.ColorbarBase(
        colorbar_axis,
        norm=norm,
    )
    cbar.ax.set_ylabel(
        "Metallicity weighted SFR"
        + "  "
        + "[{}]".format(SFR_P.unit.to_string("latex_inline"))
    )

    #
    contour_levels = [1e4, 1e5, 2 * 1e5]
    contour_linestyles = ["solid", "--", "-."]

    #
    _ = high_res_axis.contour(
        redshifts,
        metallicities,
        SFR_P.T.value,
        levels=contour_levels,
        colors="k",
        linestyles=contour_linestyles,
    )
    for contour_i, _ in enumerate(contour_levels):
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [contour_levels[contour_i]] * 2,
            "black",
            linestyle=contour_linestyles[contour_i],
        )

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


def sfr_sampled_dpdlogZ(
    redshifts,
    sfr_values,
    dPdlogZ,
    metallicities,
    sampled_metallicities,
    distribution,
    plot_settings=None,
):
    """
    Function to plot the SFR and the combination of dP/dlogZ * dlogZ
    """

    if plot_settings is None:
        plot_settings = {}

    #
    dPdLogZ_for_sampled_metallicities = dPdlogZ[
        :, np.digitize(sampled_metallicities, metallicities) - 1
    ]

    ###
    # plot metallicity distribution with the chosen distribution
    fig = plt.figure(figsize=(30, 30))
    gs = fig.add_gridspec(nrows=13, ncols=10)

    fig.subplots_adjust(hspace=0.5)

    # Set up axes
    sfr_axis = fig.add_subplot(gs[:4, :-2])
    sampled_res_axis = fig.add_subplot(gs[5:, :-2])
    colorbar_axis = fig.add_subplot(gs[:, -1])

    # Plot the SFR
    sfr_axis.plot(redshifts, sfr_values)
    sfr_axis.set_title(r"SFR")
    sfr_axis.set_ylabel(
        "Star formation rate"
        + "  "
        + "[{}]".format(sfr_values.unit.to_string("latex_inline"))
    )

    # Multiply by sfr:
    SFR_dPdlogZ = (sfr_values * dPdLogZ_for_sampled_metallicities.T).T

    # Set up norm
    norm = mpl.colors.Normalize(
        vmin=np.min(SFR_dPdlogZ).value, vmax=np.max(SFR_dPdlogZ).value
    )

    #
    sampled_res_axis.pcolormesh(
        redshifts,
        sampled_metallicities,
        SFR_dPdlogZ.T.value,
        norm=norm,
        antialiased=True,
        rasterized=True,
    )

    sampled_res_axis.set_yscale("log")

    sampled_res_axis.set_xlabel("Redshift (z)")
    sampled_res_axis.set_ylabel("Metallicity (Z)")
    sampled_res_axis.set_title(
        r"SFR * Sampled dP/dlogZ" + "  " + "{}".format(distribution)
    )

    # Align axes
    sampled_res_axis.set_xlim(sfr_axis.get_xlim())
    sampled_res_axis.set_ylim([metallicities.min(), metallicities.max()])

    # Set up a colorbar
    cbar = mpl.colorbar.ColorbarBase(
        colorbar_axis,
        norm=norm,
    )
    cbar.ax.set_ylabel(
        "SFR * dP/dlogZ"
        + "  "
        + "[{}]".format(SFR_dPdlogZ.unit.to_string("latex_inline"))
    )

    #
    contour_levels = [1e6, 1e7, 2 * 1e7]
    contour_linestyles = ["solid", "--", "-."]

    #
    _ = sampled_res_axis.contour(
        redshifts,
        sampled_metallicities,
        SFR_dPdlogZ.T.value,
        levels=contour_levels,
        colors="k",
        linestyles=contour_linestyles,
    )
    for contour_i, _ in enumerate(contour_levels):
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [contour_levels[contour_i]] * 2,
            "black",
            linestyle=contour_linestyles[contour_i],
        )

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


def sfr_sampled_dpdlogZ_dlogZ(
    redshifts,
    sfr_values,
    dPdlogZ,
    metallicities,
    sampled_metallicities,
    distribution,
    plot_settings=None,
):
    """
    Function to plot the SFR and the combination of dP/dlogZ * dlogZ
    """

    if plot_settings is None:
        plot_settings = {}

    #
    dPdLogZ_for_sampled_metallicities = dPdlogZ[
        :, np.digitize(sampled_metallicities, metallicities) - 1
    ]

    # Plot the SFR * dP/dlogZ * dlogZ
    log_sampled_metallicities = np.log(sampled_metallicities)

    extended_log_sampled_metallicities = np.append(
        np.array(log_sampled_metallicities[0] - np.diff(log_sampled_metallicities)[0]),
        log_sampled_metallicities,
    )
    dlogZ_sampled = np.diff(extended_log_sampled_metallicities)

    # Calculate full probability
    P = dPdLogZ_for_sampled_metallicities * dlogZ_sampled

    # Multiply by sfr:
    SFR_P = (sfr_values * P.T).T

    ###
    # plot metallicity distribution with the chosen distribution
    fig = plt.figure(figsize=(30, 30))
    gs = fig.add_gridspec(nrows=13, ncols=10)

    fig.subplots_adjust(hspace=0.5)

    # Set up axes
    sfr_axis = fig.add_subplot(gs[:4, :-2])
    sampled_res_axis = fig.add_subplot(gs[5:, :-2])
    colorbar_axis = fig.add_subplot(gs[:, -1])

    # Plot the SFR
    sfr_axis.plot(redshifts, sfr_values)
    sfr_axis.set_title(r"SFR")
    sfr_axis.set_ylabel(
        "Star formation rate"
        + "  "
        + "[{}]".format(sfr_values.unit.to_string("latex_inline"))
    )

    # Set up norm
    norm = mpl.colors.Normalize(vmin=np.min(SFR_P).value, vmax=np.max(SFR_P).value)

    #
    sampled_res_axis.pcolormesh(
        redshifts,
        sampled_metallicities,
        SFR_P.T.value,
        norm=norm,
        antialiased=True,
        rasterized=True,
    )

    sampled_res_axis.set_yscale("log")

    sampled_res_axis.set_xlabel("Redshift (z)")
    sampled_res_axis.set_ylabel("Metallicity (Z)")
    sampled_res_axis.set_title(
        r"SFR * Sampled dP/dlogZ * dlogZ" + "  " + "{}".format(distribution)
    )

    sampled_res_axis.set_xlim(sfr_axis.get_xlim())
    sampled_res_axis.set_ylim([metallicities.min(), metallicities.max()])

    # Set up a colorbar
    cbar = mpl.colorbar.ColorbarBase(
        colorbar_axis,
        norm=norm,
    )
    cbar.ax.set_ylabel(
        "Metallicity weighted SFR"
        + "  "
        + "[{}]".format(SFR_P.unit.to_string("latex_inline"))
    )

    #
    contour_levels = [1e4, 1e5, 2 * 1e5]
    contour_linestyles = ["solid", "--", "-."]

    #
    _ = sampled_res_axis.contour(
        redshifts,
        sampled_metallicities,
        SFR_P.T.value,
        levels=contour_levels,
        colors="k",
        linestyles=contour_linestyles,
    )
    for contour_i, _ in enumerate(contour_levels):
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [contour_levels[contour_i]] * 2,
            "black",
            linestyle=contour_linestyles[contour_i],
        )

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


def sfr_sampled_dpdlogZ_dlogZ_pdraw(
    redshifts,
    sfr_values,
    dPdlogZ,
    metallicities,
    sampled_metallicities,
    p_draw_metallicity,
    distribution,
    plot_settings=None,
):
    """
    Function to plot the SFR and the combination of dP/dlogZ * dlogZ
    """

    if plot_settings is None:
        plot_settings = {}

    #
    dPdLogZ_for_sampled_metallicities_pdraw = (
        dPdlogZ[:, np.digitize(sampled_metallicities, metallicities) - 1]
        / p_draw_metallicity
    )

    # Plot the SFR * dP/dlogZ * dlogZ
    log_sampled_metallicities = np.log(sampled_metallicities)

    extended_log_sampled_metallicities = np.append(
        np.array(log_sampled_metallicities[0] - np.diff(log_sampled_metallicities)[0]),
        log_sampled_metallicities,
    )
    dlogZ_sampled = np.diff(extended_log_sampled_metallicities)

    # Calculate full probability
    P = dPdLogZ_for_sampled_metallicities_pdraw * dlogZ_sampled

    # Multiply by sfr:
    SFR_P = (sfr_values * P.T).T

    ###
    # plot metallicity distribution with the chosen distribution
    fig = plt.figure(figsize=(30, 30))
    gs = fig.add_gridspec(nrows=13, ncols=10)

    fig.subplots_adjust(hspace=0.5)

    # Set up axes
    sfr_axis = fig.add_subplot(gs[:4, :-2])
    sampled_res_axis = fig.add_subplot(gs[5:, :-2])
    colorbar_axis = fig.add_subplot(gs[:, -1])

    # Plot the SFR
    sfr_axis.plot(redshifts, sfr_values)
    sfr_axis.set_title(r"SFR")
    sfr_axis.set_ylabel(
        "Star formation rate"
        + "  "
        + "[{}]".format(sfr_values.unit.to_string("latex_inline"))
    )

    # Set up norm
    norm = mpl.colors.Normalize(vmin=np.min(SFR_P).value, vmax=np.max(SFR_P).value)

    #
    sampled_res_axis.pcolormesh(
        redshifts,
        sampled_metallicities,
        SFR_P.T.value,
        norm=norm,
        antialiased=True,
        rasterized=True,
    )

    sampled_res_axis.set_yscale("log")

    sampled_res_axis.set_xlabel("Redshift (z)")
    sampled_res_axis.set_ylabel("Metallicity (Z)")
    sampled_res_axis.set_title(
        r"SFR * Sampled dP/dlogZ * dlogZ / p_draw_metallicity"
        + "  "
        + "{}".format(distribution)
    )

    sampled_res_axis.set_xlim(sfr_axis.get_xlim())
    sampled_res_axis.set_ylim([metallicities.min(), metallicities.max()])

    # Set up a colorbar
    cbar = mpl.colorbar.ColorbarBase(
        colorbar_axis,
        norm=norm,
    )
    cbar.ax.set_ylabel(
        "Metallicity weighted SFR"
        + "  "
        + "[{}]".format(SFR_P.unit.to_string("latex_inline"))
    )

    #
    contour_levels = [1e4, 1e5, 2 * 1e5]
    contour_linestyles = ["solid", "--", "-."]

    #
    _ = sampled_res_axis.contour(
        redshifts,
        sampled_metallicities,
        SFR_P.T.value,
        levels=contour_levels,
        colors="k",
        linestyles=contour_linestyles,
    )
    for contour_i, _ in enumerate(contour_levels):
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [contour_levels[contour_i]] * 2,
            "black",
            linestyle=contour_linestyles[contour_i],
        )

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)
