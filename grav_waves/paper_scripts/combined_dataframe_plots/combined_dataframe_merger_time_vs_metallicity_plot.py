"""
Function to plot the merger time vs metallicity

This plot does the following:
- [X] Read out combined dataframe information and plot the merger time vs metallicity
"""

import os

import h5py
import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

#
from grav_waves.convolution.functions.convolution_general_functions import (
    create_bins_from_centers,
)
from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    generate_mask,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def plot_merger_time_vs_metallicity(dataset_filename, scale, plot_settings={}):
    """
    Function to plot the merger time vs metallicity for source distributions
    """

    # Read out info
    hdf5file = h5py.File(dataset_filename)

    metallicity_bin_centers = np.array(hdf5file["sfr_data/metallicity_bin_centers"][()])
    log10metallicity_values = np.array(sorted(np.log10(metallicity_bin_centers)))
    log10metallicity_bins = create_bins_from_centers(log10metallicity_values)
    metallicity_bins = 10**log10metallicity_bins

    hdf5file.close()

    # Read out combined df
    combined_df = pd.read_hdf(dataset_filename, "/data/combined_dataframes")

    # Create mask and filter shit
    mask = generate_mask(combined_df, general_query=None, dco_type="bhbh")

    # Get the data from the dataframe
    merger_time_values_in_years = combined_df["merger_time_values_in_years"].to_numpy()[
        mask
    ]
    metallicity_values = combined_df["metallicity"].to_numpy()[mask]
    number_per_formed_solarmass = combined_df[
        "number_per_solar_mass_values"
    ].to_numpy()[mask]

    merger_time_values_in_myr = merger_time_values_in_years / 1e6

    min_log10_merger_time_values_in_myr = np.log10(merger_time_values_in_myr.min())
    max_log10_merger_time_values_in_myr = np.log10(merger_time_values_in_myr.max())

    #
    metallicity_centers = (metallicity_bins[1:] + metallicity_bins[:-1]) / 2

    merger_time_bins = 10 ** np.linspace(
        min_log10_merger_time_values_in_myr,
        np.ceil(max_log10_merger_time_values_in_myr * 10) / 10,
        120,
    )
    merger_time_centers = (merger_time_bins[1:] + merger_time_bins[:-1]) / 2

    hist = np.histogram2d(
        merger_time_values_in_myr,
        metallicity_values,
        bins=[merger_time_bins, metallicity_bins],
        weights=number_per_formed_solarmass,
    )

    X, Y = np.meshgrid(merger_time_centers, metallicity_centers)

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=11)

    ax = fig.add_subplot(gs[:, :-2])
    ax_cb = fig.add_subplot(gs[:, -1])

    ##################
    # Plot the data

    # Get the normalisation
    if scale == "linear":
        norm = colors.Normalize(vmin=hist[0].min(), vmax=hist[0].max())
    elif scale == "log":
        norm = colors.LogNorm(
            vmin=10 ** (np.log10(hist[0].max()) - 3),
            vmax=hist[0].max(),
        )

    ##################
    # Plot the MSSFR results
    _ = ax.pcolormesh(
        X,
        Y,
        hist[0].T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # make colorbar
    cb = matplotlib.colorbar.ColorbarBase(
        ax_cb, norm=norm, extend="min" if scale == "log" else None
    )
    cb.ax.set_ylabel("Number per formed solar mass")

    #
    ax.set_yscale("log")
    ax.set_xscale("log")

    ax.set_xlabel("Merger time [Myr]")
    ax.set_ylabel("Metallicity [Z]")

    ax.set_title(
        "Merger time vs metallicity distribution of source populations", fontsize=28
    )

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #
    fiducial_filename = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/rebinned_convolution_results.h5"
    base_output_name = "merger_time_vs_metallicity_plot.pdf"

    #
    plot_merger_time_vs_metallicity(
        dataset_filename=fiducial_filename,
        scale="log",
        plot_settings={
            "show_plot": True,
            # 'output_name': base_output_name
            "output_name": os.path.join(
                "/home/david/papers/paper_gw/paper_tex/figures/fiducial_combined_dataframe/",
                base_output_name,
            ),
        },
    )
