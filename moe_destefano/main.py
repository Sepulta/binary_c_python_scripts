"""
Scrip to run ensemble for CEMP calculations

TODO: Read paper Lewis
TODO: Clean up this code
TODO: Ask rob the range of metallicities and masses
TODO: Make sure that the
"""
import json
import os

from binarycpython.utils.grid import Population

test_pop = Population()
test_pop.Moe_de_Stefano_2017(
    options={
        "multiplicity_modulator": [1, 0, 0, 0],
        "resolutions": {"M": [10, 5, 0, 0], "logP": [5, 0, 0], "ecc": [0, 0, 0]},
    }
)

# Set some default values
test_pop.set(M_2=0.07, orbital_period=1e60, metallicity=0.02)
test_pop.set(
    verbosity=10,
    amt_cores=2,
)

# Set ensemble values
test_pop.set(
    # Activate ensemble
    ensemble=1,
    ensemble_defer=1,
    ensemble_filters_off=1,
    ensemble_filter_EMP=1,
    combine_ensemble_with_thread_joining=True,
)

# Short test to see if the probabilities add up:
test_pop.set(_actually_evolve_system=False)

analytics_dict = test_pop.evolve()
test_pop.Moe_de_Stefano_2017(
    options={
        "multiplicity_modulator": [1, 0, 0, 0],
        "resolutions": {"M": [20, 5, 0, 0], "logP": [5, 0, 0], "ecc": [0, 0, 0]},
    }
)
quit()


# Make sure MINT and NUCSYN are on:
version_info = test_pop.return_all_info()["binary_c_version_info"]
if not (version_info["macros"]["NUCSYN"] == "on"):
    print("Error: this script requires binary_c to be built with NUCSYN on")
    raise ValueError

# Set metallicity range
metallicities = [0.02, 0.01, 0.002]
metallicities = [0.02]

for metallicity in metallicities:
    test_pop.set(
        metallicity=metallicity,
        data_dir=os.path.join(
            os.environ["BINARYC_DATA_ROOT"],
            "CEMP",
            "DEFAULT",
            "Z{}".format(metallicity),
        ),
    )

    # create local tmp_dir
    test_pop.set(
        tmp_dir=os.path.join(test_pop.custom_options["data_dir"], "local_tmp_dir")
    )
    os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

    # Export settings:
    test_pop.export_all_info(use_datadir=True)

    # Evolve population
    test_pop.evolve()

    # Get ensemble output and write it to file
    ensemble_output = test_pop.grid_ensemble_results

    with open("ensemble_output.json", "w") as f:
        json.dump(ensemble_output, f, indent=4)


# test_pop.Moe_de_Stefano_2017(options={"multiplicity_modulator": [1,1,0,0]}, )
# test_pop.set(M_2=0.07, orbital_period=1e60)
# test_pop.evolve()
