"""
Function to plot the paper plot for the primary mass at zero redshift comparison between variations

TODO: improve the re-sampling; use replacement? use weights? see https://maxhalford.github.io/blog/weighted-sampling-without-replacement/
"""

import os

import numpy as np

import pandas as pd

import matplotlib.pyplot as plt
from matplotlib import colors

from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    extract_columns_from_querylist,
    quantity_name_dict,
    quantity_unit_dict,
    rate_type_name_dict,
    rate_density_units,
    add_columns_to_df,
    dco_type_query,
    readout_rate_array_specific_keys,
    create_centers_from_bins,
    get_closest_redshift_key,
    get_histogram_data,
    get_KDE_data,
    return_1d_plot_canvas,
    plot_1d_results,
    hatch_list,
    linestyle_list,
)
from grav_waves.convolution.functions.new_plot_routines.plot_kde_and_bootstrap_functions import (
    run_bootstrap,
)

from grav_waves.scripts.add_confidence_interval_powerlaw_peak.functions import (
    add_confidence_interval_powerlaw_peak_primary_mass,
    get_data_powerlaw_peak_primary_mass,
)

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

from grav_waves.paper_scripts.primary_mass_distribution.primary_mass_distribution_at_redshift_with_variations.functions import (
    handle_columns,
    get_mask,
    get_rate_data,
)


def observation_comparison_function(f_mod, f_obs):
    y = np.abs(((f_obs - f_mod) / f_obs))
    return y


def handle_comparison_to_observations(centers, rates, data_powerlaw_peak_primary_mass):
    """
    Function to handle the comparison to the observations
    """

    # Get bins where the values are non-zero
    non_zero_centers = centers[rates > 0]
    non_zero_rates = rates[rates > 0]

    centers_above_ten = non_zero_centers[non_zero_centers > 10]
    rates_above_ten = non_zero_rates[non_zero_centers > 10]

    used_centers = centers_above_ten
    used_rates = rates_above_ten

    # Get matching values observations
    observation_bin_indices = np.digitize(
        used_centers,
        bins=data_powerlaw_peak_primary_mass["mass_1_centers"],
        right=False,
    )
    observation_values = data_powerlaw_peak_primary_mass["mass_1_mean_rate"][
        observation_bin_indices
    ]

    # Create the comparison
    rates_comparison_values = observation_comparison_function(
        f_mod=used_rates, f_obs=observation_values
    )

    return used_centers, rates_comparison_values


def get_median_percentiles(value_array):
    """
    Function to get the median and the percentiles from the data
    """

    result_dict = {}

    result_dict["median"] = np.percentile(value_array, [50], axis=0)

    result_dict["90%_CI"] = np.percentile(value_array, [5, 95], axis=0)

    # result_dict["1_sigma"] = np.percentile(value_array, [15.89, 84.1], axis=0)

    # result_dict["2_sigma"] = np.percentile(value_array, [2.27, 97.725], axis=0)

    return result_dict


def get_data(
    filename,
    rate_type,
    dco_type,
    quantity,
    quantity_bins,
    divide_by_binsize,
    redshift_value,
    add_comparison_to_observations=False,
    bootstraps=0,
    num_procs=1,
    general_query=None,
    querylist=None,
):
    """
    Function to read out the data from the datafile
    """

    #
    data_powerlaw_peak_primary_mass = get_data_powerlaw_peak_primary_mass(
        data_root=os.path.join(os.environ["DATAFILES_ROOT"], "GW"),
    )

    ####
    # Read out dataframe
    combined_df = pd.read_hdf(filename, "/data/combined_dataframes")

    # Add columns
    handle_columns(
        combined_df,
        quantity,
        querylist,
        extra_columns=["primary_undergone_ppisn", second_quantity],
    )

    # Create masks and apply, taking into account that the query and general query can be None
    mask = get_mask(combined_df, dco_type, general_query, query=None)

    ###########
    # Get the quantity data from the dataframe
    quantity_data = combined_df[quantity]

    # mask the quantity data
    masked_quantity_data = quantity_data[mask].to_numpy()

    # Get second quantity
    if second_quantity:
        # Get the quantity data from the dataframe
        second_quantity_data = combined_df[second_quantity]

        # mask the quantity data
        second_masked_quantity_data = second_quantity_data[mask].to_numpy()

    ################
    # Read out rate data for this specific redshift
    rate_array = get_rate_data(filename, rate_type, redshift_value, mask)

    ################
    # Calculate the rate histogram
    hist, bincenter, truncated_bins = get_histogram_data(
        bins=quantity_bins, data_array=masked_quantity_data, weight_array=rate_array[0]
    )

    # Divide by binsize
    if divide_by_binsize:
        hist = hist / np.diff(quantity_bins)

    #
    rates_return_dict = {
        "centers": bincenter,
        "rates": hist,
    }

    ############
    # Handle comparison to observations
    if add_comparison_to_observations:
        (
            bincenters_comparison,
            rates_comparison_values,
        ) = handle_comparison_to_observations(
            centers=bincenter,
            rates=hist,
            data_powerlaw_peak_primary_mass=data_powerlaw_peak_primary_mass,
        )

        # Set up return dict
        observation_comparison_return_dict = {
            "centers": bincenters_comparison,
            "rates": rates_comparison_values,
        }

    # bootstrap
    if bootstraps:
        # Get a list of indices
        indices = np.arange(len(masked_quantity_data))

        # Set up array that stores all the boostrapped results
        bootstrapped_rates_hist_vals = np.zeros(
            (bootstraps, len(rates_return_dict["centers"]))
        )  # center_bins

        if add_comparison_to_observations:
            bootstrapped_observation_comparison_vals = np.zeros(
                (bootstraps, len(observation_comparison_return_dict["centers"]))
            )

        #
        for bootstrap_i in range(bootstraps):
            print("Bootstrap {}".format(bootstrap_i))
            ##############################
            # Get bootstrap indices
            boot_index = np.random.choice(
                indices,
                size=len(indices),
                replace=True,
                # p=rate_array[0][indices] / np.sum(rate_array[0][indices]),
            )

            # Select the quantity values with these indices
            bootstrapped_masked_quantity_data = masked_quantity_data[boot_index]

            # Select the rate values with these indices
            bootstrapped_rate_array = rate_array[:, boot_index]

            ##############################
            # Calculate the rate histogram
            (bootstrapped_hist, _, _,) = get_histogram_data(
                bins=quantity_bins,
                data_array=bootstrapped_masked_quantity_data,
                weight_array=bootstrapped_rate_array[0],
            )

            # Store unfiltered rate in array
            bootstrapped_rates_hist_vals[bootstrap_i] = bootstrapped_hist

            # Get bootstrapped
            if add_comparison_to_observations:
                _, rates_comparison_values = handle_comparison_to_observations(
                    centers=bincenter,
                    rates=bootstrapped_hist,
                    data_powerlaw_peak_primary_mass=data_powerlaw_peak_primary_mass,
                )
                bootstrapped_observation_comparison_vals[
                    bootstrap_i
                ] = rates_comparison_values

        ###########
        # Calculate median and percentiles
        bootstrapped_median_percentiles_dict = get_median_percentiles(
            bootstrapped_rates_hist_vals
        )
        rates_return_dict["median_percentiles"] = bootstrapped_median_percentiles_dict

        if add_comparison_to_observations:
            bootstrapped_observation_comparison_median_percentiles_dict = (
                get_median_percentiles(bootstrapped_observation_comparison_vals)
            )
            observation_comparison_return_dict[
                "median_percentiles"
            ] = bootstrapped_observation_comparison_median_percentiles_dict

    #
    return_dict = {
        "rates_results": rates_return_dict,
    }

    ################
    # Calculate second 2d plot histogram
    if add_comparison_to_observations:
        return_dict[
            "observation_comparison_results"
        ] = observation_comparison_return_dict

    return return_dict


def generate_primary_mass_at_redshift_zero_comparison_plot(
    convolved_datasets,
    primary_mass_bins,
    divide_by_binsize,
    add_rates_plots=True,
    add_comparison_to_observations=True,
    bootstraps=0,
    general_query=None,
    plot_settings={},
):
    """
    Function to plot the paper plot for the primary mass at zero redshift comparison between variations
    """

    cmap = plt.cm.get_cmap("viridis", len(convolved_datasets) - 1)
    color_list = [cmap(i) for i in range(cmap.N)]

    # Add other color type at the end
    color_list.append("orange")

    #####################################
    # Calculate the results
    result_dict_rates = {}
    for convolved_dataset_i, (
        convolved_dataset_key,
        convolved_dataset_filename,
    ) in enumerate(convolved_datasets.items()):

        result_dict_rates[convolved_dataset_key] = get_data(
            filename=convolved_dataset_filename,
            rate_type="merger_rate",
            dco_type="bhbh",
            quantity="primary_mass",
            quantity_bins=primary_mass_bins,
            divide_by_binsize=divide_by_binsize,
            redshift_value=0,
            add_comparison_to_observations=add_comparison_to_observations,
            bootstraps=bootstraps,
            general_query=general_query,
        )

    # Settings
    linewidth = 5
    fig_scale = 15
    gs_scale = 1
    levels = (2 * add_rates_plots) + (2 * add_comparison_to_observations)
    level = 0

    # Set up figure logic
    fig = plt.figure(figsize=(1.5 * fig_scale, 1 * fig_scale))
    gs = fig.add_gridspec(nrows=levels * gs_scale, ncols=1)
    axes_list = []

    if add_rates_plots:
        # set up the subplot
        rates_axis = fig.add_subplot(gs[level * gs_scale : (level + 2) * gs_scale, 0])
        level += 2
        axes_list.append(rates_axis)

    if add_comparison_to_observations:
        comparison_to_observations_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(comparison_to_observations_axis)

    # Add rates to the plot
    if add_rates_plots:
        # set up the subplot
        ylabel = "$\\frac{d\\mathcal{R}}{dm_{1}}$ [Gpc$^{-3}$yr$^{-1}M_{\\odot}^{-1}$]"  # ligo plot

        rates_axis.set_ylabel(
            ylabel,
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

        # Add results to plot
        for result_i, (result_name, result) in enumerate(result_dict_rates.items()):
            if (not add_kde) and (not bootstraps):
                # Plot histograms
                rates_axis.plot(
                    result["rates_results"]["centers"],
                    result["rates_results"]["rates"],
                    lw=linewidth,
                    c=color_list[result_i],
                    zorder=200,
                    alpha=1,
                    linestyle=linestyle_list[result_i],
                    label=result_name,
                )

            else:
                if bootstraps:
                    # Plot median and bootstrap
                    rates_axis.plot(
                        result["rates_results"]["centers"],
                        result["rates_results"]["median_percentiles"]["median"][0],
                        lw=linewidth,
                        c=color_list[result_i],
                        zorder=13,
                        linestyle=linestyle_list[result_i],
                        label=result_name,
                    )

                    #
                    rates_axis.fill_between(
                        result["rates_results"]["centers"],
                        result["rates_results"]["median_percentiles"]["90%_CI"][0],
                        result["rates_results"]["median_percentiles"]["90%_CI"][1],
                        alpha=0.4,
                        zorder=11,
                        color=color_list[result_i],
                    )  # 1-sigma

            # Add fraction of primary underwent PPISN
            if add_comparison_to_observations:
                #
                comparison_to_observations_axis.set_ylabel(
                    r"$\mathrm{log10}\left(\|\frac{\mathrm{obs}-\mathrm{mod}}{\mathrm{obs}}\|\right)$",
                    fontsize=plot_settings.get("axislabel_fontsize", 26),
                )

                if bootstraps:
                    # Plot median and bootstrap
                    comparison_to_observations_axis.plot(
                        result["observation_comparison_results"]["centers"],
                        np.log10(
                            result["observation_comparison_results"][
                                "median_percentiles"
                            ]["median"][0]
                        ),
                        lw=linewidth,
                        c=color_list[result_i],
                        zorder=13,
                        linestyle=linestyle_list[result_i],
                        label=result_name,
                    )

                    #
                    comparison_to_observations_axis.fill_between(
                        result["observation_comparison_results"]["centers"],
                        np.log10(
                            result["observation_comparison_results"][
                                "median_percentiles"
                            ]["90%_CI"][0]
                        ),
                        np.log10(
                            result["observation_comparison_results"][
                                "median_percentiles"
                            ]["90%_CI"][1]
                        ),
                        alpha=0.4,
                        zorder=11,
                        color=color_list[result_i],
                    )  # 1-sigma

        # set ylims
        rates_axis.set_ylim([1e-3, 1e1])

        # Make up
        rates_axis.set_yscale("log")

    #########
    # Add confidence interval
    fig, axes_list[0] = add_confidence_interval_powerlaw_peak_primary_mass(
        fig=fig,
        ax=axes_list[0],
        data_root=os.path.join(os.environ["DATAFILES_ROOT"], "GW"),
    )

    #########
    # Remove x-ticklabels
    for axes in axes_list[:-1]:
        axes.set_xticklabels([])
    for axes in axes_list:
        axes.set_xlim(2, 100)

    #########
    # Extra makeup
    axes_list[-1].set_xlabel(
        "{} [{}]".format(
            quantity_name_dict["primary_mass"],
            quantity_unit_dict["primary_mass"].to_string("latex"),
        ),
        fontsize=plot_settings.get("axislabel_fontsize", 26),
    )
    axes_list[0].legend(fontsize=plot_settings.get("legend_fontsize", 26))

    #########
    # Add plot dependent makeup
    if add_comparison_to_observations:
        #
        region_of_interest_bounds = np.array([32, 38])

        # Add plot to band of interest
        comparison_to_observations_axis_ylims = (
            comparison_to_observations_axis.get_ylim()
        )
        comparison_to_observations_axis.axvspan(
            region_of_interest_bounds[0],
            region_of_interest_bounds[1],
            ymin=comparison_to_observations_axis_ylims[0],
            ymax=comparison_to_observations_axis_ylims[1],
            alpha=0.1,
            color="red",
        )

        # Add line of threshold
        comparison_to_observations_axis.axhline(-1, alpha=0.5, linestyle="--")

    #########
    # Handle end

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #########
    #
    data_root = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"), "GRAV_WAVES/server_results/"
    )

    #########
    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"),
        "paper_gw/paper_tex/figures/primary_mass_distribution_at_redshift_zero_with_extra_2d_plot",
    )
    plot_dir = "plots"

    # configuration:
    primary_mass_bins = np.arange(0, 100, 2)
    divide_by_binsize = True
    add_rates_plots = True
    add_secondary_2d_plot = True
    add_kde = False
    bootstraps = 2
    show_plot = False
    second_quantity = "eccentricity"
    second_quantity_bins_stepsize = 0.05
    second_quantity_bins = np.arange(
        0, 1 + second_quantity_bins_stepsize, second_quantity_bins_stepsize
    )

    #######
    # Fiducial

    #
    convolved_datasets = {
        # Fiducial and variations
        "Fiducial (SEMI RES)": os.path.join(
            data_root,
            "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
        ),
        "Fiducial (SEMI mid RES)": os.path.join(
            data_root,
            "EVENTS_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
        ),
    }

    #
    basename = "fiducial_with_eccentricity_second_axis.pdf"
    general_query = None
    # general_query = "stable_rlof_counter == 0 &  comenv_counter == 0"
    generate_primary_mass_at_redshift_zero_comparison_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_comparison_to_observations=True,
        bootstraps=bootstraps,
        general_query=general_query,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 32,
        },
    )
