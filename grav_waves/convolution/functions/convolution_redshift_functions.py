"""
Functions for the convolution related to calculating the redshifts

TODO: Consider wheteher this is all necessary. While its a nice solution, we now have an interpolator that does basically what we do here.
"""

import time
import numpy as np

import astropy
import astropy.units as u

from grav_waves.settings import cosmo

from grav_waves.settings import AGE_UNIVERSE_IN_YEAR
from grav_waves.convolution.functions.general_multiprocessing_function import (
    general_multiprocessing_queue,
)

####
# Redshift functions
def time_to_redshift_job_handler(job_dict, job_index, verbosity=0):
    """
    Function that handles time_to_redshift jobs

    payload:
    - age_universe: age of universe in Gyr that we want to convert to redshift
    """

    if verbosity:
        print(job_index)

    # Calculate redshift
    if (job_dict["age_universe"] < AGE_UNIVERSE_IN_YEAR / 1e9) and (
        job_dict["age_universe"] > 0
    ):
        redshift_value = astropy.cosmology.z_at_value(
            cosmo.age, job_dict["age_universe"] * u.Gyr, zmin=0, zmax=10000
        )
    else:
        redshift_value = -1

    return redshift_value


def transform_time_array_to_redshift_array(lookbacktime_series, amt_cores):
    """
    Function to transform a time array to a redshift array.

    It is assumed that this array contains lookback times in units of years
    """

    # Transform ages to Gyr, assuming they are stored in years
    lookbacktime_series_in_gyr = lookbacktime_series * (u.yr).to(u.Gyr) * u.Gyr

    #
    final_age_universe = cosmo.age(0)

    # Calculate ages of universe
    age_universe_lookbacktimes = final_age_universe - lookbacktime_series_in_gyr

    #
    redshift_values = np.zeros(lookbacktime_series_in_gyr.shape)

    # Create job list
    job_list = []

    # Fill job list
    for time_i, age_universe in enumerate(age_universe_lookbacktimes):
        job_dict = {
            "index": time_i,
            "age_universe": age_universe.value,
        }

        job_list.append(job_dict)

    # Calculate stuff
    start = time.time()
    job_list_with_results = general_multiprocessing_queue(
        amt_cores, time_to_redshift_job_handler, job_list, verbosity=1
    )
    print(
        "Time taken to do {} systems with {} cores: {}".format(
            len(age_universe_lookbacktimes), amt_cores, time.time() - start
        )
    )

    for job_dict_with_result in job_list_with_results:
        redshift_values[job_dict_with_result["index"]] = job_dict_with_result["result"]

    return redshift_values


def transform_time_to_redshift(time_value):
    """
    Function to transform the time value to a redshift value
    """

    # Calculate some ages
    lookback_time = time_value * u.Gyr
    final_age_universe = cosmo.age(0)
    age_universe_at_lookback_time = final_age_universe - lookback_time

    # Calculate redshift
    redshift_value = astropy.cosmology.z_at_value(
        cosmo.age, age_universe_at_lookback_time, zmin=0, zmax=10000
    )

    return redshift_value
