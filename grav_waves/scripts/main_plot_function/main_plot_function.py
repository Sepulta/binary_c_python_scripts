"""
Function to run all the plotting routines.

Plots:
- individual and combined dataset with queries
- starformation convolution
- starformation convolution with queries
- combined query plots
- volumetric convolution plots
- volumetric convolution plots with queries

TODO: fix that the physics are passed to the plotting functions properly
TODO: fix having a big pdf containing all the results
TODO: fix passing verbosity to all the functions
"""

import os
import pickle

# Pdf generating routines
from PyPDF2 import PdfFileMerger
from david_phd_functions.plotting.pdf_functions import add_pdf_and_bookmark

#
from grav_waves.prescriptions.general_plot_routine import (
    general_plot_routine as general_prescription_plots_routine,
)

#
from grav_waves.convolution.functions.convolution_functions import (
    new_convolution_main as convolution_main,
)
from grav_waves.convolution.functions.volume_convolution_functions import (
    create_redshift_volume_convolution_result_dict,
)
from grav_waves.gw_analysis.functions.functions import make_dataset_dict

#
from grav_waves.gw_analysis.functions.general_plot_function import (
    general_plot_function as general_individual_datasets_plot_function,
)
from grav_waves.convolution.scripts.detection_probability_COMPAS.general_plot_function import (
    general_plot_routine as general_detection_probability_plot_function,
)
from grav_waves.convolution.functions.general_plot_function import (
    general_plot_routine as general_sfr_convolution_plot_function,
)
from grav_waves.convolution.functions.general_plot_function import (
    general_plot_routine_volume_convolved as general_volumetric_convolution_plot_function,
)
from grav_waves.general_scripts.general_sfr_convolution_plot_function_with_subsets import (
    general_sfr_convolution_plot_function_with_subsets,
)


def main_plot_function(
    main_dir,
    cosmology_configuration,
    convolution_configuration,
    combined_pdf_output_name,
    verbosity=0,
    querysets=None,
    do_prescription_plots=True,
    do_individual_dataset_plots=True,
    do_convolution_without_detector_probability=True,
    do_detection_probability_plots=True,
    do_convolution_with_detector_probability=True,
    do_volume_convolution=True,
    rebuild_results=True,
):
    """
    Function to plot all the plotting routines

    Parameters
    ----------
    main_dir : path
        main directory of this dataset (containing population_results/)
    cosmology_configuration : dict
        Dictionary containing the cosmology settings
    convolution_configuration: dict
        Dictionary containing the convolution settings
    combined_pdf_output_name: str, path
        output filename for the combined pdf

    querysets (optional) : None, dict
        Dictionary containing querysets. A queryset is structured like {queryname: query, .. }
    """

    # Get the result directory
    population_results_dir = os.path.join(main_dir, "population_results")

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    # Create dictionary containing some information about the population data
    dataset_dict_populations = make_dataset_dict(main_dir)

    ################
    # prescriptions and mapping plots
    if do_prescription_plots:
        prescription_plots_output_pdf = os.path.join(main_dir, "prescription_plots.pdf")
        prescription_plots_output_dir = os.path.join(main_dir, "prescription_plots")
        prescription_results_output_dir = os.path.join(main_dir, "prescription_results")
        os.makedirs(prescription_plots_output_dir, exist_ok=True)

        #
        general_prescription_plots_routine(
            combined_output_pdf=prescription_plots_output_pdf,
            result_dir=prescription_results_output_dir,
            plot_dir=prescription_plots_output_dir,
            plot_schematic_plots=True,
            plot_mapping_plots=True,
            rebuild_data=rebuild_results,
        )

        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            prescription_plots_output_pdf,
            pdf_page_number,
            bookmark_text="prescription plots",
            add_source_bookmarks=True,
        )

    ################
    # Individual and combined datasets
    if do_individual_dataset_plots:
        individual_datasets_plots_output_dir = os.path.join(
            main_dir, "individual_datasets_plots"
        )
        os.makedirs(individual_datasets_plots_output_dir, exist_ok=True)

        #
        individual_datasets_plot_output_pdf = os.path.join(
            main_dir, "individual_datasets_plots.pdf"
        )

        general_individual_datasets_plot_function(
            dataset_dict_populations,
            result_dir=dataset_dict_populations["population_result_dir"],
            plot_dir=dataset_dict_populations["population_plot_dir"],
            type_key="bhbh",
            combined_pdf_output_name=individual_datasets_plot_output_pdf,
            convolution_settings=convolution_configuration,
        )

        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            individual_datasets_plot_output_pdf,
            pdf_page_number,
            bookmark_text="dataset plots",
            add_source_bookmarks=True,
        )

        # Individual and combined datasets with queries. TODO: fix queries

    ################
    # Starformation convolution without detector probability
    if do_convolution_without_detector_probability:
        convolution_output_dir = os.path.join(
            main_dir, "convolution_without_detector_probability_results"
        )
        convolution_plot_dir = os.path.join(
            main_dir, "convolution_without_detector_probability_plots"
        )

        os.makedirs(convolution_output_dir, exist_ok=True)
        os.makedirs(convolution_plot_dir, exist_ok=True)

        convolution_without_detector_plot_output_pdf = os.path.join(
            main_dir, "convolution_without_detector_plots.pdf"
        )

        redshift_convolution_without_detector_probability_filename = os.path.join(
            convolution_output_dir, "convolution_without_detection_probability.p"
        )

        convolution_configuration["add_detection_probability"] = False

        if rebuild_results:
            convolution_main(
                dataset_dict=dataset_dict_populations,
                convolution_configuration=convolution_configuration,
                cosmology_configuration=cosmology_configuration,
                output_filename=redshift_convolution_without_detector_probability_filename,
                dco_type="bhbh",
                verbosity=verbosity,
            )

        # Load the results from the star formation convolution without detector probability
        with open(
            redshift_convolution_without_detector_probability_filename, "rb"
        ) as f:
            results_redshift_convolution_without_detector_probability = pickle.load(f)

        # Plot the results of the convolution without the detector probability
        general_sfr_convolution_plot_function(
            results_redshift_convolution_without_detector_probability,
            output_pdf_filename=convolution_without_detector_plot_output_pdf,
            main_plot_dir=os.path.join(convolution_plot_dir),
        )

        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            convolution_without_detector_plot_output_pdf,
            pdf_page_number,
            bookmark_text="starformation convolution without detector probability plots",
            add_source_bookmarks=True,
        )

        ################
        # Handle querysets for convolution
        if querysets is not None:
            querysets_merger = PdfFileMerger()
            querysets_pdf_page_number = 0
            querysets_convolution_without_detector_plot_output_pdf = os.path.join(
                main_dir,
                "querysets_convolution_without_detector_probability_plots",
                "querysets_plots.pdf",
            )

            # Get the queryset name
            for queryset_name in querysets.keys():
                # Set up the pdf object
                queryset_merger = PdfFileMerger()
                queryset_pdf_page_number = 0

                # Create dictionary
                queryset_convolution_output_dir = os.path.join(
                    main_dir,
                    "querysets_convolution_without_detector_probability_results",
                    queryset_name,
                )
                queryset_convolution_plot_dir = os.path.join(
                    main_dir,
                    "querysets_convolution_without_detector_probability_plots",
                    queryset_name,
                )
                queryset_convolution_without_detector_plot_output_pdf = os.path.join(
                    main_dir,
                    "querysets_convolution_without_detector_probability_plots",
                    queryset_name,
                    "{}_plots.pdf".format(queryset_name),
                )

                os.makedirs(queryset_convolution_output_dir, exist_ok=True)
                os.makedirs(queryset_convolution_plot_dir, exist_ok=True)

                # extended_queryset
                extended_queryset = querysets[queryset_name]
                extended_queryset["all"] = None

                #
                query_convolution_outputs_dict = {}

                # Loop over query
                # TODO: change the structure of the query dicts to handle some extra info
                for query in extended_queryset.keys():
                    # Set the paths and names
                    query_convolution_output_dir = os.path.join(
                        queryset_convolution_output_dir, query
                    )
                    query_convolution_plot_dir = os.path.join(
                        queryset_convolution_plot_dir, query
                    )

                    os.makedirs(query_convolution_output_dir, exist_ok=True)
                    os.makedirs(query_convolution_plot_dir, exist_ok=True)

                    query_convolution_without_detector_plot_output_pdf = os.path.join(
                        query_convolution_plot_dir,
                        "{}_convolution_without_detector_plots.pdf".format(query),
                    )

                    query_redshift_convolution_without_detector_probability_filename = (
                        os.path.join(
                            query_convolution_output_dir,
                            "convolution_without_detection_probability.p",
                        )
                    )
                    query_convolution_outputs_dict[query] = {
                        "result_filename": query_redshift_convolution_without_detector_probability_filename
                    }

                    convolution_configuration["add_detection_probability"] = False

                    # TODO: fix that we can specify any or global query
                    # Set the query in the convolution dict:
                    convolution_configuration["global_query"] = extended_queryset[query]

                    if rebuild_results:
                        convolution_main(
                            dataset_dict=dataset_dict_populations,
                            convolution_configuration=convolution_configuration,
                            cosmology_configuration=cosmology_configuration,
                            output_filename=query_redshift_convolution_without_detector_probability_filename,
                            dco_type="bhbh",
                            # save_time_bins=save_time_bins,
                            store_each_timestep=False,
                            verbosity=verbosity,
                        )

                    # Load the results from the star formation convolution without detector probability
                    with open(
                        query_redshift_convolution_without_detector_probability_filename,
                        "rb",
                    ) as f:
                        results_query_redshift_convolution_without_detector_probability = pickle.load(
                            f
                        )

                    # Plot the results of the convolution without the detector probability
                    general_sfr_convolution_plot_function(
                        results_query_redshift_convolution_without_detector_probability,
                        output_pdf_filename=query_convolution_without_detector_plot_output_pdf,
                        main_plot_dir=os.path.join(query_convolution_plot_dir),
                    )

                    # Add the plot to the pdf
                    queryset_merger, queryset_pdf_page_number = add_pdf_and_bookmark(
                        queryset_merger,
                        query_convolution_without_detector_plot_output_pdf,
                        queryset_pdf_page_number,
                        bookmark_text="SFR convolution with query: {}".format(query),
                        add_source_bookmarks=True,
                    )

                    # Clear out the query
                    convolution_configuration["global_query"] = None

                ###########
                # Do combined plots of the query results

                # Add the combined panels to this plot:
                subsets_convolution_without_detector_plot_output_pdf = os.path.join(
                    main_dir,
                    "querysets_convolution_without_detector_probability_plots",
                    queryset_name,
                    "combind_subset_plots.pdf".format(queryset_name),
                )
                subsets_convolution_without_detector_plot_dir = os.path.join(
                    main_dir,
                    "querysets_convolution_without_detector_probability_plots",
                    queryset_name,
                    "subset_plots",
                )

                # plot the subset panels for these queries
                general_sfr_convolution_plot_function_with_subsets(
                    dataset_dict=dataset_dict_populations,
                    query_dataset_dict=query_convolution_outputs_dict,
                    plot_dir=subsets_convolution_without_detector_plot_dir,
                    output_pdf_filename=subsets_convolution_without_detector_plot_output_pdf,
                    convolution_configuration=convolution_configuration,
                )

                # Add the plot to the pdf
                queryset_merger, queryset_pdf_page_number = add_pdf_and_bookmark(
                    queryset_merger,
                    subsets_convolution_without_detector_plot_output_pdf,
                    queryset_pdf_page_number,
                    bookmark_text="Combined subset plots for queryset: {}".format(
                        queryset_name
                    ),
                    add_source_bookmarks=True,
                )

                # Round off the plot for this queryset
                queryset_merger.write(
                    queryset_convolution_without_detector_plot_output_pdf
                )
                queryset_merger.close()

                # Add this plot then to the querysets merger pdf
                querysets_merger, querysets_pdf_page_number = add_pdf_and_bookmark(
                    querysets_merger,
                    queryset_convolution_without_detector_plot_output_pdf,
                    querysets_pdf_page_number,
                    bookmark_text="Querysets: {}".format(queryset_name),
                    add_source_bookmarks=True,
                )

            # Round off the plot for this querysets merger
            querysets_merger.write(
                querysets_convolution_without_detector_plot_output_pdf
            )
            querysets_merger.close()

            # Add this to the grand plot
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                querysets_convolution_without_detector_plot_output_pdf,
                pdf_page_number,
                bookmark_text="Querysets starformation convolution without detector probability plots",
                add_source_bookmarks=True,
            )

    ################
    # Detector probability plots
    if do_detection_probability_plots:
        detection_probability_plots_output_dir = os.path.join(
            main_dir, "detection_probability_plots"
        )
        os.makedirs(detection_probability_plots_output_dir, exist_ok=True)

        detector_output_pdf = os.path.join(main_dir, "detector_plots.pdf")

        #
        general_detection_probability_plot_function(
            detection_probability_plots_output_dir,
            convolution_configuration=convolution_configuration,
            combined_output_pdf=detector_output_pdf,
            verbose=verbosity,
        )

        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            detector_output_pdf,
            pdf_page_number,
            bookmark_text="Decector plots",
            add_source_bookmarks=True,
        )

    ################
    # Starformation convolution with detector probability
    if do_convolution_with_detector_probability:
        convolution_output_dir = os.path.join(
            main_dir, "convolution_with_detector_probability_results"
        )
        convolution_plot_dir = os.path.join(
            main_dir, "convolution_with_detector_probability_plots"
        )

        os.makedirs(convolution_output_dir, exist_ok=True)
        os.makedirs(convolution_plot_dir, exist_ok=True)

        convolution_with_detector_plot_output_pdf = os.path.join(
            main_dir, "convolution_with_detector_plots.pdf"
        )

        redshift_convolution_with_detector_probability_filename = os.path.join(
            convolution_output_dir, "convolution_with_detection_probability.p"
        )

        convolution_configuration["add_detection_probability"] = True
        convolution_configuration["include_formation_rates"] = False

        convolution_main(
            dataset_dict=dataset_dict_populations,
            convolution_configuration=convolution_configuration,
            cosmology_configuration=cosmology_configuration,
            output_filename=redshift_convolution_with_detector_probability_filename,
            dco_type="bhbh",
            # save_time_bins=save_time_bins,
            store_each_timestep=False,
        )

        # Load the results from the star formation convolution without detector probability
        with open(redshift_convolution_with_detector_probability_filename, "rb") as f:
            results_redshift_convolution_with_detector_probability = pickle.load(f)

        # Plot the results of the convolution without the detector probability
        general_sfr_convolution_plot_function(
            results_redshift_convolution_with_detector_probability,
            output_pdf_filename=convolution_with_detector_plot_output_pdf,
            main_plot_dir=os.path.join(convolution_plot_dir),
        )

        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            convolution_with_detector_plot_output_pdf,
            pdf_page_number,
            bookmark_text="starformation convolution with detector probability plots",
            add_source_bookmarks=True,
        )

        ###################
        # Do querysets
        # Querysets dict contains dictionaries with querysets. These contain a name and a set of queries, which we should all run
        #
        # if querysets is not None and isinstance(querysets, dict):

        # TODO: Fix queries

        # TODO: fix combined query plots

    ################
    # Volume convolution
    if do_volume_convolution:
        volumetric_convolution_output_dir = os.path.join(
            main_dir, "volumetric_convolution_results"
        )
        volumetric_convolution_plot_dir = os.path.join(
            main_dir, "volumetric_convolution_plots"
        )
        zoomed_volumetric_convolution_plot_dir = os.path.join(
            main_dir, "zoomed_volumetric_convolution_plots"
        )

        os.makedirs(volumetric_convolution_output_dir, exist_ok=True)
        os.makedirs(volumetric_convolution_plot_dir, exist_ok=True)
        os.makedirs(zoomed_volumetric_convolution_plot_dir, exist_ok=True)

        volumetric_convolution_with_detector_plot_output_pdf = os.path.join(
            main_dir, "volumetric_convolution.pdf"
        )
        zoomed_volumetric_convolution_with_detector_plot_output_pdf = os.path.join(
            main_dir, "zoomed_volumetric_convolution.pdf"
        )

        # Load result dict, add info and plot
        with open(redshift_convolution_with_detector_probability_filename, "rb") as f:
            results_redshift_convolution_with_detector_probability = pickle.load(f)

        # Do volume convolution
        redshift_volume_convolution_results = (
            create_redshift_volume_convolution_result_dict(
                results_redshift_convolution_with_detector_probability
            )
        )

        # Store convolved data in the result dataset
        results_redshift_convolution_with_detector_probability[
            "redshift_volume_convolution_results"
        ] = redshift_volume_convolution_results

        # Save to file
        redshift_convolution_with_detector_probability_and_volume_convolution_filename = os.path.join(
            volumetric_convolution_output_dir,
            "redshift_with_detection_probability_and_volume_convolution.p",
        )
        pickle.dump(
            results_redshift_convolution_with_detector_probability,
            open(
                redshift_convolution_with_detector_probability_and_volume_convolution_filename,
                "wb",
            ),
        )

        # Create plots
        general_volumetric_convolution_plot_function(
            convolution_dataset=results_redshift_convolution_with_detector_probability,
            main_plot_dir=volumetric_convolution_plot_dir,
            output_pdf_filename=volumetric_convolution_with_detector_plot_output_pdf,
            dco_type="bhbh",
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            volumetric_convolution_with_detector_plot_output_pdf,
            pdf_page_number,
            bookmark_text="Volumetric convolution plots",
            add_source_bookmarks=True,
        )

        # Create plots up to maximum redshift of detectors
        general_volumetric_convolution_plot_function(
            convolution_dataset=results_redshift_convolution_with_detector_probability,
            main_plot_dir=zoomed_volumetric_convolution_plot_dir,
            output_pdf_filename=zoomed_volumetric_convolution_with_detector_plot_output_pdf,
            max_plot_time=2.6,
            dco_type="bhbh",
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            zoomed_volumetric_convolution_with_detector_plot_output_pdf,
            pdf_page_number,
            bookmark_text="Zoomed volumetric convolution plots",
            add_source_bookmarks=True,
        )

        # TODO: Fix queries with volume convolution

    # Round off the plot
    merger.write(combined_pdf_output_name)
    merger.close()
    print("Finished making plots. Wrote pdf to {}".format(combined_pdf_output_name))
