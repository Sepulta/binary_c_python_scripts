"""
Main script to read out the TZO data

TODO: Check that the _1 is actually the neutron star
TODO: pre-process the datasets and create a combined dataset, stored as HDF5 or something that can be read out quickly
"""

import os

import matplotlib.pyplot as plt

import numpy as np
import pandas as pd

from TZO.functions.plot_functions.delay_time_function import plot_delay_time
from TZO.functions.plot_functions.plot_prev_total_mass_giant import (
    plot_prev_total_mass_giant,
)
from TZO.functions.plot_functions.plot_prev_envelope_mass_giant import (
    plot_prev_envelope_mass_giant,
)
from TZO.functions.plot_functions.plot_prev_core_mass_giant import (
    plot_prev_core_mass_giant,
)
from TZO.functions.plot_functions.plot_eccentricity_distribution import (
    plot_eccentricity_distribution,
)


from david_phd_functions.plotting.utils import show_and_save_plot, add_plot_info
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

#
root_dir = "/home/david/projects/binary_c_root/results/TZO/server_results/MID_RES_TESTING/population_results/"
metallicity_dirs = os.listdir(root_dir)

# Loop over the files and read out the data
combined_df = pd.DataFrame()
for metallicity_dir in metallicity_dirs:
    # main_dir = "/home/david/projects/binary_c_root/results/TZO/LOW_RES_TESTING/population_results/Z0.02"
    main_dir = os.path.join(root_dir, metallicity_dir)
    data_filename = os.path.join(main_dir, "all_tzo_events.dat")
    df = pd.read_csv(data_filename, sep="\s+")

    # Exclude channels where the other is a compact object:
    filtered_df = df[df.prev_stellar_type_2 < 10]
    combined_df = pd.concat([combined_df, filtered_df])

    # quit()

    # #
    # # print_total_rate_groups(df)
    # plot_delay_time(filtered_df, plot_settings={"show_plot": True})
    # plot_prev_total_mass_giant(filtered_df, plot_settings={"show_plot": True})
    # plot_prev_envelope_mass_giant(filtered_df, plot_settings={"show_plot": True})
    # plot_prev_core_mass_giant(filtered_df, plot_settings={"show_plot": True})

plot_eccentricity_distribution(
    combined_df, plot_ratio=True, plot_channels=False, plot_settings={"show_plot": True}
)
quit()

# print_total_rate_groups(df)
plot_delay_time(combined_df, plot_settings={"show_plot": True})
plot_prev_total_mass_giant(combined_df, plot_settings={"show_plot": True})
plot_prev_envelope_mass_giant(combined_df, plot_settings={"show_plot": True})
plot_prev_core_mass_giant(combined_df, plot_settings={"show_plot": True})
