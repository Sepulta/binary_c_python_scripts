"""
Function to plot the CO core: mass removal and core mass shift schematic
TODO: transition this all to the generalized version
"""
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.add_co_core_mass_removal_schematic_to_axis import (
    add_co_core_mass_removal_schematic_to_axis,
)
from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.add_co_core_core_mass_shift_schematic_to_axis import (
    add_co_core_core_mass_shift_schematic_to_axis,
)
from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_utils import (
    add_label_subplot,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def plot_co_core_mass_removal_and_core_mass_shift_schematic(
    result_directory_mass_removal,
    result_directory_core_mass_shift,
    old_prescription_result_directory_root,
    plot_settings=None,
):
    """
    Function to plot the ZAMS mass to remnant mass with extra ppisn mass removal and min co_core mass shift
    """

    #
    fig = plt.figure(figsize=(30, 18))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=8, ncols=1)

    ax_co_core_to_remnant_mass_mass_removal = fig.add_subplot(gs[:4, 0])
    ax_co_core_mass_to_remnant_mass_core_mass_shift = fig.add_subplot(gs[4:, 0])

    ax_invisible = fig.add_subplot(gs[:, 0], frame_on=False)
    ax_invisible.set_xticks([])
    ax_invisible.set_yticks([])

    # Add subplot labels
    for ax_i, ax in enumerate(
        [
            ax_co_core_to_remnant_mass_mass_removal,
            ax_co_core_mass_to_remnant_mass_core_mass_shift,
        ]
    ):
        fig, ax = add_label_subplot(fig=fig, ax=ax, label_i=ax_i, x_loc=0.01)

    # Call function to add the plot to the axis
    (
        fig,
        ax_co_core_to_remnant_mass_mass_removal,
    ) = add_co_core_mass_removal_schematic_to_axis(
        fig=fig,
        ax_co_core_to_remnant_mass_mass_removal=ax_co_core_to_remnant_mass_mass_removal,
        result_directory_mass_removal=result_directory_mass_removal,
        old_prescription_result_directory_root=old_prescription_result_directory_root,
    )

    # Call function to add the plot to the axis
    (
        fig,
        ax_co_core_mass_to_remnant_mass_core_mass_shift,
    ) = add_co_core_core_mass_shift_schematic_to_axis(
        fig=fig,
        ax_co_core_mass_to_remnant_mass_core_mass_shift=ax_co_core_mass_to_remnant_mass_core_mass_shift,
        result_directory_core_mass_shift=result_directory_core_mass_shift,
        old_prescription_result_directory_root=old_prescription_result_directory_root,
    )

    ####
    # Set the axes
    ax_zams_to_remnant_mass_mass_removal_xlims = (
        ax_co_core_to_remnant_mass_mass_removal.get_xlim()
    )
    ax_zams_to_remnant_mass_core_mass_shift_xlims = (
        ax_co_core_mass_to_remnant_mass_core_mass_shift.get_xlim()
    )

    ax_zams_to_remnant_mass_mass_removal_ylims = (
        ax_co_core_to_remnant_mass_mass_removal.get_ylim()
    )
    ax_zams_to_remnant_mass_core_mass_shift_ylims = (
        ax_co_core_mass_to_remnant_mass_core_mass_shift.get_ylim()
    )

    min_xlim = np.min(
        [
            ax_zams_to_remnant_mass_mass_removal_xlims[0],
            ax_zams_to_remnant_mass_core_mass_shift_xlims[0],
        ]
    )
    max_xlim = np.max(
        [
            ax_zams_to_remnant_mass_mass_removal_xlims[1],
            ax_zams_to_remnant_mass_core_mass_shift_xlims[1],
        ]
    )

    min_ylim = np.min(
        [
            ax_zams_to_remnant_mass_mass_removal_ylims[0],
            ax_zams_to_remnant_mass_core_mass_shift_ylims[0],
        ]
    )
    max_ylim = np.max(
        [
            ax_zams_to_remnant_mass_mass_removal_ylims[1],
            ax_zams_to_remnant_mass_core_mass_shift_ylims[1],
        ]
    )

    ax_co_core_to_remnant_mass_mass_removal.set_xlim([min_xlim, max_xlim])
    ax_co_core_to_remnant_mass_mass_removal.set_ylim([min_ylim, max_ylim])

    ax_co_core_mass_to_remnant_mass_core_mass_shift.set_xlim([min_xlim, max_xlim])
    ax_co_core_mass_to_remnant_mass_core_mass_shift.set_ylim([min_ylim, max_ylim])

    # Override the limits if we have input:
    if plot_settings.get("min_xlim", None) is not None:
        ax_co_core_to_remnant_mass_mass_removal.set_xlim(
            [
                plot_settings["min_xlim"],
                ax_co_core_to_remnant_mass_mass_removal.get_xlim()[1],
            ]
        )
        ax_co_core_mass_to_remnant_mass_core_mass_shift.set_xlim(
            [
                plot_settings["min_xlim"],
                ax_co_core_mass_to_remnant_mass_core_mass_shift.get_xlim()[1],
            ]
        )
    if plot_settings.get("max_xlim", None) is not None:
        ax_co_core_to_remnant_mass_mass_removal.set_xlim(
            [
                ax_co_core_to_remnant_mass_mass_removal.get_xlim()[0],
                plot_settings["max_xlim"],
            ]
        )
        ax_co_core_mass_to_remnant_mass_core_mass_shift.set_xlim(
            [
                ax_co_core_mass_to_remnant_mass_core_mass_shift.get_xlim()[0],
                plot_settings["max_xlim"],
            ]
        )
    if plot_settings.get("min_ylim", None) is not None:
        ax_co_core_to_remnant_mass_mass_removal.set_ylim(
            [
                plot_settings["min_ylim"],
                ax_co_core_to_remnant_mass_mass_removal.get_ylim()[1],
            ]
        )
        ax_co_core_mass_to_remnant_mass_core_mass_shift.set_ylim(
            [
                plot_settings["min_ylim"],
                ax_co_core_mass_to_remnant_mass_core_mass_shift.get_ylim()[1],
            ]
        )
    if plot_settings.get("max_ylim", None) is not None:
        ax_co_core_to_remnant_mass_mass_removal.set_ylim(
            [
                ax_co_core_to_remnant_mass_mass_removal.get_ylim()[0],
                plot_settings["max_ylim"],
            ]
        )
        ax_co_core_mass_to_remnant_mass_core_mass_shift.set_ylim(
            [
                ax_co_core_mass_to_remnant_mass_core_mass_shift.get_ylim()[1],
                plot_settings["max_ylim"],
            ]
        )

    # Remove the title
    ax_co_core_mass_to_remnant_mass_core_mass_shift.set_title("")

    # Remove the ticks
    ax_co_core_to_remnant_mass_mass_removal.set_xticklabels([])

    # Remove the xlabel
    ax_co_core_to_remnant_mass_mass_removal.set_xlabel("")

    # Remove the ylabels
    ylabel = ax_co_core_to_remnant_mass_mass_removal.get_ylabel()

    ax_co_core_to_remnant_mass_mass_removal.set_ylabel("")
    ax_co_core_mass_to_remnant_mass_core_mass_shift.set_ylabel("")

    ax_invisible.set_ylabel(ylabel, labelpad=80)

    # Handle the plotting
    show_and_save_plot(fig, plot_settings)
