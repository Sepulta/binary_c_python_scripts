import os
import json
import time
import pickle
import sys

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import (
    get_help_all,
    get_help,
    create_hdf5,
    output_lines,
)

from david_phd_functions.binaryc.personal_defaults import personal_defaults


def parse_function(self, output):
    # extract info from the population instance
    # TODO: think about whether this is smart. Passing around this object might be an overkill

    # Get some information from the
    data_dir = self.custom_options["data_dir"]
    base_filename = self.custom_options["base_filename"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    seperator = " "

    # Create filename
    outfilename = os.path.join(data_dir, base_filename)

    result_header = [
        "time",
        "mass_1",
        "prev_mass_1",
        "zams_mass_1",
        "SN_type",
        "probability",
        "metallicity",
        "stellar_type",
        "prev_stellar_type",
    ]

    mass_lost_dict = {}
    for i in range(16):
        mass_lost_dict["{}".format(i)] = 0

    # Go over the output.
    for el in output_lines(output):
        headerline = el.split()[0]

        # Check the header and act accordingly
        if headerline == "DAVID_COMPACT_OBJECTS_SINGLE":
            values = el.split()[1:]
            result_list = values

            if not os.path.exists(outfilename):
                with open(outfilename, "w") as f:
                    f.write(seperator.join(result_header) + "\n")

            with open(outfilename, "a") as f:
                f.write(seperator.join(result_list) + "\n")


## Set values
test_pop = Population()
test_pop.set(
    C_logging_code="""
if(stardata->star[0].stellar_type >= NS)
{
    if (stardata->model.time < stardata->model.max_evolution_time)
    {
        //                                         1  2  3  4  5  6  7  8  9
        Printf("DAVID_COMPACT_OBJECTS_SINGLE %30.12e %g %g %g %d %g %g %d %d\\n",
            //
            stardata->model.time, // 1
            stardata->star[0].mass, //2
            stardata->previous_stardata->star[0].mass, //3
            stardata->star[0].pms_mass, //4
            stardata->star[0].SN_type, //5
            stardata->model.probability, //6
            stardata->common.metallicity, // 7
            stardata->star[0].stellar_type, //8
            stardata->previous_stardata->star[0].stellar_type //9
      );
    };
    /* Kill the simulation to save time */
    stardata->model.max_evolution_time = stardata->model.time - stardata->model.dtm;
};
"""
)

test_pop.set(
    verbose=1,
    amt_cores=2,
    binary=0,
    parse_function=parse_function,
    separation=1000000000,
    M_1=100,
    orbital_period=400000000,
)


# Put in personal defaults (see david_phd_function.binaryc)
test_pop.set(**personal_defaults)

quit()

# Add grid variables
resolution = {"M_1": 300}

# test_pop.add_grid_variable(
#     name="lnm1",
#     longname="Primary mass",
#     valuerange=[1, 150],
#     resolution="{}".format(resolution["M_1"]),
#     spacingfunc="const(math.log(1), math.log(150), {})".format(resolution["M_1"]),
#     precode="M_1=math.exp(lnm1)",
#     probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 150, -1.3, -2.3, -2.3)*M_1",
#     dphasevol="dlnm1",
#     parameter_name="M_1",
#     condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
# )


test_pop.add_grid_variable(
    name="M_1",
    longname="Primary mass",
    valuerange=[1, 300],
    resolution="{}".format(resolution["M_1"]),
    spacingfunc="const(1, 300, {})".format(resolution["M_1"]),
    probdist="Kroupa2001(M_1)",
    dphasevol="dm1",
    parameter_name="M_1",
)

# test single:
# test_pop.evolve_single()


# Make metallicity loop.
metallicity_values = [0.02, 0.01, 0.002, 0.001, 0.0002]
for metallicity in metallicity_values:
    test_pop.set(
        metallicity=metallicity,
        data_dir=os.path.join(
            os.environ["BINARYC_DATA_ROOT"],
            "testing_python",
            "compact_objects_single",
            "default_settings",
            "CO_single_{}".format(metallicity),
        ),
        base_filename="CO_single_{}.dat".format(metallicity),
    )

    # Export settings:
    test_pop.export_all_info(use_datadir=True)

    # Evolve grid
    test_pop.evolve_population()

    # Put data (and settings) in hdf5
    create_hdf5(
        test_pop.custom_options["data_dir"], "CO_single_{}.hdf5".format(metallicity)
    )
