import json
from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict
import pandas as pd

#
simulation_dir = "/home/david/projects/binary_c_root/results/RLOF/LOW_RES_RLOF_SANA_2021_ENSEMBLE_fractions"
simulation_dir = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions"

rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(simulation_dir)

#
df = pd.read_csv(
    rlof_episode_dataset_dict["datasets"]["0.02"]["filename"], sep="\s+", header=0
)
# print(df.columns)

# df length
print(len(df.index))

# unique accretor types:
print(df["initial_stellar_type_accretor"].unique())
print(df["initial_stellar_type_donor"].unique())
# quit()

# # check if it adds up for time
# df['time_test'] = df['total_time_spent_disk_masstransfer_MT_category_0'] + df['total_time_spent_disk_masstransfer_MT_category_1'] + df['total_time_spent_disk_masstransfer_MT_category_2'] - df['total_time_spent_disk_masstransfer']
# print(df['time_test'].unique())

# Check if the categories add up for mass
df["mass_test"] = (
    df["total_mass_transferred_through_disk_MT_category_0"]
    + df["total_mass_transferred_through_disk_MT_category_1"]
    + df["total_mass_transferred_through_disk_MT_category_2"]
    - df["total_mass_transferred_through_disk"]
)
