"""
Function to plot the 2-d density plot for merger or formation rate per mass and per redshift
"""

import json
import h5py
import numpy as np
import pandas as pd

import astropy.units as u

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors

from david_phd_functions.grav_waves.dataframe_functions import add_chirpmass_column
from grav_waves.convolution.functions.convolution_functions import (
    generate_metallicity_sfr_array,
)
from grav_waves.convolution.functions.redshift_interpolation_functions import (
    test_redshift_to_lookback_time,
    test_lookback_time_to_redshift,
    create_interpolation_datasets,
)

from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    create_any_mass_df,
    filter_dco_type,
    color_list,
    hatch_list,
    long_name_dict,
    get_rate_type_names,
    linestyle_list,
    add_contour_levels,
    plot_2d_results,
    get_num_subsets,
    get_hists_multiprocessing,
)

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.canvas_functions import return_canvas_with_subsets

#
number_per_gigaparsec_cubed_per_year = 1 / u.Gpc**3 / u.yr
sfr_unit = u.Msun / u.Gpc**3 / u.yr


def get_data(
    hdf5_filename,
    dco_type,
    mass_bins,
    mass_quantity,
    rate_key_in_df,
    rate_type_hdf5_key,
    divide_by_binsize,
    query=None,
    num_procs=1,
):
    """
    Function to read out the data from the hdf5
    """

    mass_bins_size = np.diff(mass_bins)

    hdf5_file = h5py.File(hdf5_filename, "r")
    sorted_keys = sorted(
        hdf5_file["data"][rate_type_hdf5_key].keys(),
        key=lambda key: float(key),
        reverse=False,
    )
    hdf5_file.close()

    # Get combined df
    combined_df = pd.read_hdf(hdf5_filename, "/data/combined_dataframes")

    # Add columns
    combined_df = add_chirpmass_column(combined_df, m1_name="mass_1", m2_name="mass_2")
    combined_df["total_mass"] = combined_df["mass_1"] + combined_df["mass_2"]
    combined_df["primary_mass"] = combined_df[["mass_1", "mass_2"]].max(axis=1)
    combined_df["secondary_mass"] = combined_df[["mass_1", "mass_2"]].min(axis=1)

    ###################################
    # Loop over all datasets:
    res_dict = get_hists_multiprocessing(
        hdf5_filename=hdf5_filename,
        combined_df=combined_df,
        dco_type=dco_type,
        query=query,
        rate_key_in_df=rate_key_in_df,
        bins=mass_bins,
        quantity=mass_quantity,
        divide_by_binsize=divide_by_binsize,
        rate_type_hdf5_key=rate_type_hdf5_key,
        sorted_keys=sorted_keys,
        num_procs=num_procs,
    )

    # Put all the results in lists
    total_redshift_list = []
    total_merger_rate_list = []

    for redshift_value in res_dict:
        total_redshift_list.append(redshift_value)
        total_merger_rate_list.append(list(res_dict[redshift_value]))

    #
    total_redshift_array = np.array(total_redshift_list)
    total_merger_rate_array = np.array(total_merger_rate_list)

    ###########################################
    return {
        "total_redshift_array": total_redshift_array,
        "total_rate_array": total_merger_rate_array,
    }


def plot_convolved_rate_density_per_mass_quantity(
    convolution_dataset_hdf5_filename,
    dco_type,
    rate_type,
    mass_quantity,
    mass_bins,
    divide_by_binsize=False,
    querylist=None,
    time_bin_key=None,
    contourlevels=None,
    linestyles_contourlevels=None,
    add_ratio=False,
    num_procs=1,
    plot_settings=None,
):
    """
    Plot for the convolved rate density (i.e. rate per gigaparsec cubed)

    Args:
        - convolution_dataset_hdf5_filename: filename of the hdf5 file that contains all the data
        - rate_type: option to plot either merger rate or formation rate
        - mass_type: option to plot which mass

        - time_bin_key (Optional): If passed then we plot only the results of that certain time bin
    """

    #
    if not plot_settings:
        plot_settings = {}

    #
    ratio_contourlevels = [0.1, 0.5, 0.90]
    x_scale = "linear"
    y_scale = "log"
    long_name_quantity = long_name_dict[mass_quantity]
    (
        _,
        rate_key_in_df,
        long_name_rate_type,
        rate_type_hdf5_key,
        rate_time_key_in_df,
    ) = get_rate_type_names(rate_type)

    #
    mass_centers = (mass_bins[1:] + mass_bins[:-1]) / 2

    main_result_units = number_per_gigaparsec_cubed_per_year
    if divide_by_binsize:
        main_result_units = main_result_units / u.Msun

    # Calculate the data:
    results_dict = {}
    results_dict["all"] = get_data(
        hdf5_filename=convolution_dataset_hdf5_filename,
        dco_type=dco_type,
        mass_bins=mass_bins,
        mass_quantity=mass_quantity,
        rate_key_in_df=rate_key_in_df,
        rate_type_hdf5_key=rate_type_hdf5_key,
        divide_by_binsize=divide_by_binsize,
        num_procs=num_procs,
    )

    #
    max_val = results_dict["all"]["total_rate_array"].max()
    custom_min_val = 10 ** np.floor(
        np.log10(max_val) - plot_settings.get("probability_floor", 4)
    )

    norm = colors.LogNorm(vmin=custom_min_val, vmax=max_val)

    # Loop over queries
    if not querylist is None:
        for query_dict in querylist:
            results_dict[query_dict["name"]] = get_data(
                hdf5_filename=convolution_dataset_hdf5_filename,
                dco_type=dco_type,
                mass_bins=mass_bins,
                mass_quantity=mass_quantity,
                rate_key_in_df=rate_key_in_df,
                rate_type_hdf5_key=rate_type_hdf5_key,
                divide_by_binsize=divide_by_binsize,
                query=query_dict["query"],
                num_procs=num_procs,
            )

    X, Y = np.meshgrid(results_dict["all"]["total_redshift_array"], mass_centers)

    ####
    # Do plotting
    num_subsets = get_num_subsets(querylist)

    # Set up axes
    fig = plt.figure(figsize=(40, 40))
    fig, _, axes_dict = return_canvas_with_subsets(
        num_subsets, fig=fig, add_ratio_axes=add_ratio
    )

    ##
    # Plot 2-d results
    fig, axes_dict, cb, cb_ratio = plot_2d_results(
        fig=fig,
        axes_dict=axes_dict,
        X=X,
        Y=Y,
        long_name=long_name_quantity,
        add_ratio=add_ratio,
        results_dict=results_dict,
        querylist=querylist,
        plot_settings=plot_settings,
        x_scale=x_scale,
        y_scale=y_scale,
    )

    ##
    # Add contour levels
    fig, axes_dict = add_contour_levels(
        results_dict=results_dict,
        X=X,
        Y=Y,
        fig=fig,
        axes_dict=axes_dict,
        cb=cb,
        cb_ratio=cb_ratio,
        contourlevels=contourlevels,
        ratio_contourlevels=ratio_contourlevels,
        linestyle_list=linestyle_list,
        querylist=querylist,
        add_ratio=add_ratio,
    )

    ###
    #
    axes_dict["all_axis"].set_xlabel(r"Redshift")

    #
    axes_dict["all_axis"].set_ylabel(
        r"%s [M$_{\odot}$]" % long_name_mass_dict[mass_type]
    )

    #
    title_text = (
        "Intrinsic {} density of merging BHBH systems per {} over cosmic time".format(
            long_name_rate_type.lower(), long_name_mass_dict[mass_type].lower()
        )
    )
    axes_dict["all_axis"].set_title(title_text, fontsize=26)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    # Global settings
    rebinned_results_filename = "/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/convolution/scripts/new_main/output/array_test/convolution/without_detection_probability/rebinned_convolution_results.h5"
    convolution_plot_dir = "output/"
    divide_by_binsize = False

    querylist_lieke = [
        {"name": "CE channel", "query": "(comenv_counter >= 1)"},
        {
            "name": "No CE channel",
            "query": "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter==0)",
        },
    ]

    #
    plot_convolved_rate_density_per_mass_quantity(
        rebinned_results_filename,
        divide_by_binsize=False,
        rate_type="merger_rate",
        dco_type="bhbh",
        mass_quantity="primary_mass",
        mass_bins=np.arange(0, 60, 1),
        # add_ratio=True,
        querylist=querylist_lieke,
        plot_settings={
            "show_plot": True,
            "output_name": "output/plot_convolved_rate_density_per_mass_quantity.pdf",
            "simulation_name": "test",
            "antialiased": True,
            "rasterized": True,
            "runname": "plot_convolved_rate_density_per_mass_quantity",
        },
    )
else:
    matplotlib.use("Agg")
