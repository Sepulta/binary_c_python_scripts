import os
from binarycpython.utils.functions import (
    get_help_all,
    get_help,
    create_hdf5,
    output_lines,
)


def parse_function(self, output):
    """ """

    # Get some information from the
    data_dir = self.custom_options["data_dir"]
    base_filename = self.custom_options["base_filename"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # set the separator
    seperator = " "

    # Create filename
    outfilename = os.path.join(data_dir, base_filename)

    stellar_type_strings = ["st_{}".format(el) for el in range(16)]

    result_header = (
        ["zams_mass"]
        + stellar_type_strings
        + [  # Supernova information
            "WIND_MASSLOSS",
            "SN_TYPE",
            "SN_MASSLOSS",
            "STELLAR_TYPE_REMNANT",
            "STELLAR_TYPE_PRE_SN",
            "PREV_CORE_MASS",
            "PREV_CO_CORE_MASS",
            "PREV_HE_CORE_MASS",
            "FALLBACK",
            "FALLBACK_MASS",
        ]
    )

    # Create a dictionary that will contain the cumulative mass lost in a stellar type, and the supernova information
    output_dict = {}
    output_dict["zams_mass"] = -1

    for i in range(16):
        output_dict["st_{}".format(i)] = 0
    output_dict["WIND_MASSLOSS"] = 0

    output_dict["SN_TYPE"] = -1
    output_dict["SN_MASSLOSS"] = -1
    output_dict["STELLAR_TYPE_REMNANT"] = -1
    output_dict["STELLAR_TYPE_PRE_SN"] = -1
    output_dict["PREV_CORE_MASS"] = -1
    output_dict["PREV_CO_CORE_MASS"] = -1
    output_dict["PREV_HE_CORE_MASS"] = -1
    output_dict["FALLBACK"] = -1
    output_dict["FALLBACK_MASS"] = -1

    # Go over the output.
    for i, el in enumerate(output_lines(output)):
        headerline = el.split()[0]

        # Capture output of the
        if headerline == "DAVID_MASSLOSS":
            parameters_massloss = [
                "time",
                "mass_1",
                "prev_mass_1",
                "zams_mass_1",
                "stellar_type",
                "probability",
            ]
            values = el.split()[1:]

            if not float(values[0]) == 0.0:
                mass_lost = float(values[2]) - float(values[1])
                output_dict["st_{}".format(values[4])] += mass_lost
                output_dict["WIND_MASSLOSS"] += mass_lost

            if int(i) == 0:
                output_dict["zams_mass"] = values[3]

        # Capture output of the supernovae
        if headerline == "DAVID_SN":
            parameters_supernova = [
                "time",
                "mass_1",
                "prev_mass_1",
                "zams_mass_1",
                "supernova_type",
                "stellar_type_1",
                "prev_stellar_type_1",
                "probability",
                "prev_core_mass",
                "prev_CO_core_mass",
                "prev_He_core_mass",
                "fallback",
                "fallback_mass",
            ]
            values = el.split()[1:]

            mass_ejected = float(values[2]) - float(values[1])

            output_dict["SN_MASSLOSS"] = mass_ejected
            output_dict["SN_TYPE"] = int(values[4])
            output_dict["STELLAR_TYPE_REMNANT"] = int(values[5])
            output_dict["STELLAR_TYPE_PRE_SN"] = int(values[6])

            output_dict["PREV_CORE_MASS"] = float(values[8])
            output_dict["PREV_CO_CORE_MASS"] = float(values[9])
            output_dict["PREV_HE_CORE_MASS"] = float(values[10])
            output_dict["FALLBACK"] = float(values[11])
            output_dict["FALLBACK_MASS"] = float(values[12])

            # Remove the SN explosion ejecta from the mass loss dict
            output_dict["st_{}".format(int(values[5]))] -= mass_ejected
            output_dict["WIND_MASSLOSS"] -= mass_ejected

    # So, it could be that the run actually fails. We need to prevent that from being written to the file
    if not output_dict["zams_mass"] == -1:
        result_list = [str(output_dict.get(el, -1)) for el in result_header]

        if not os.path.exists(outfilename):
            with open(outfilename, "w") as f:
                f.write(seperator.join(result_header) + "\n")

        with open(outfilename, "a") as f:
            f.write(seperator.join(result_list) + "\n")
