def remove_zams_overflow(df_start, df_end):
    # https://www.shanelynn.ie/select-pandas-dataframe-rows-and-columns-using-iloc-loc-and-ix/
    # print(df_start.info())
    # print(df_end.info())

    # Flag those that overflow early
    df_start["overflow_time_zero"] = df_start["time"] == 0

    # Select both cases
    df_start_filtered = df_start[df_start.overflow_time_zero == False]
    # print(df_start.info())

    zero_time_overflow_start = df_start[df_start.overflow_time_zero == True]
    # print(zero_time_overflow_start.info())

    # End ones
    # Flag those that overflow early
    df_end["overflow_time_zero"] = df_end["time"] == 1e-6

    # Select both cases
    df_end_filtered = df_end[df_end.overflow_time_zero == False]
    # print(df_start.info())

    zero_time_overflow_end = df_end[df_end.overflow_time_zero == True]
    # print(zero_time_overflow_start.info())

    # print(df_start_filtered.info())
    # print(df_end_filtered.info())

    return df_start_filtered, df_end_filtered


def add_mass_donor(df):
    """
    Function to add the initial mass of the initial donor
    """

    # https://thispointer.com/python-how-to-use-if-else-elif-in-lambda-functions/
    df = df.assign(
        mass_donor=df.apply(
            lambda row: row.mass_1 if (row.donor == 0) else row.mass_2, axis=1
        )
    )
    return df


def add_mass_accretor(df):
    """
    Function to add the initial mass of the initial accretor
    """

    # https://thispointer.com/python-how-to-use-if-else-elif-in-lambda-functions/
    df = df.assign(
        mass_accretor=df.apply(
            lambda row: row.mass_1 if (row.accretor == 0) else row.mass_2, axis=1
        )
    )
    return df


def add_radius_donor(df):
    """
    Function to add the initial radius of the initial donor
    """

    df = df.assign(
        radius_donor=df.apply(
            lambda row: row.radius_1 if (row.donor == 0) else row.radius_2, axis=1
        )
    )
    return df


def add_radius_accretor(df):
    """
    Function to add the initial radius of the initial accretor
    """

    df = df.assign(
        radius_accretor=df.apply(
            lambda row: row.radius_1 if (row.accretor == 0) else row.radius_2, axis=1
        )
    )
    return df


def add_stellar_type_accretor(df):
    """
    Function to add the initial stellar type of the initial accretor
    """

    df = df.assign(
        stellar_type_accretor=df.apply(
            lambda row: row.stellar_type_2 if (row.donor == 0) else row.stellar_type_1,
            axis=1,
        )
    )
    return df


def add_stellar_type_donor(df):
    """
    Function to add the initial stellar type of the initial donor
    """

    df = df.assign(
        stellar_type_donor=df.apply(
            lambda row: row.stellar_type_1 if (row.donor == 0) else row.stellar_type_2,
            axis=1,
        )
    )
    return df


def add_mass_ratio_accretor_donor(df):
    """
    Function to add initial mass ratio of the rlof episode:
    """

    df["q_accretor_donor"] = df["mass_accretor"] / df["mass_donor"]
    return df


def add_luminosity_accretor(df):
    """
    Function to add initial luminosity accretor
    """
    df = df.assign(
        luminosity_accretor=df.apply(
            lambda row: row.lum_1 if (row.accretor == 0) else row.lum_2, axis=1
        )
    )

    return df


def add_luminosity_donor(df):
    """
    Function to add initial luminosity donor
    """
    df = df.assign(
        luminosity_donor=df.apply(
            lambda row: row.lum_1 if (row.donor == 0) else row.lum_2, axis=1
        )
    )

    return df


def add_teff_accretor(df):
    """
    Function to add initial teff accretor
    """
    df = df.assign(
        teff_accretor=df.apply(
            lambda row: row.teff_1 if (row.accretor == 0) else row.teff_2, axis=1
        )
    )

    return df


def add_teff_donor(df):
    """
    Function to add initial teff donor
    """
    df = df.assign(
        teff_donor=df.apply(
            lambda row: row.teff_1 if (row.donor == 0) else row.teff_2, axis=1
        )
    )

    return df


def add_duration(df):
    """
    Function to add the duration of a RLOF episode
    """
    df["duration"] = df["time_end"] - df["time_start"]

    return df


def add_case(row):
    """
    Function that adds column values to a dataframe
    requires that the df has a column containing filenames
    """

    result = row.copy()

    result["case"] = return_case_masstransfer(result["stellar_type_donor"])[-1]

    return result


def add_rl_radius_accretor(df):
    """
    Function to add rochelobe radius of accretor
    """

    df = df.assign(
        rl_radius_accretor=df.apply(
            lambda row: row.separation
            * rochelobe_radius_accretor(
                mass_donor=row.mass_donor, mass_accretor=row.mass_accretor
            ),
            axis=1,
        )
    )
    return df


def add_circ_radius(df):
    """
    Function to add the initial circularisation radius in terms of Rsun
    """
    df = df.assign(
        circ_radius=df.apply(
            lambda row: row.separation
            * circularisation_radius(
                mass_donor=row.mass_donor, mass_accretor=row.mass_accretor
            ),
            axis=1,
        )
    )
    return df


def add_radii(df, fraction_RL_disk):
    """
    Function that adds 3 radii to the dataframe:
    - initial roche lobe size accretor (in terms of Rsun)
    - initial circularisation radius of mass stream around accretor
    - initial estimated size of disk. takes RL radius times input <fraction_RL_disk>
    """

    df = add_rl_radius_accretor(df)
    df = add_circ_radius(df)
    df["disk_radius"] = fraction_RL_disk * df["rl_radius_accretor"]

    return df


def calc_fraction(mass_donor, mass_accretor, separation, Rtrunc_factor=0.9):
    """
    Function to calculate the ratio in angular momentum h_circ/h_outer
    using Rtrunc_factor * RL1 as outer edge
    """

    rcirc = separation * circularisation_radius(
        mass_donor=mass_donor, mass_accretor=mass_accretor
    )
    rl_accretor = separation * rochelobe_radius_accretor(
        mass_donor=mass_donor, mass_accretor=mass_accretor
    )
    fraction = math.sqrt((rcirc) / (Rtrunc_factor * rl_accretor))
    return fraction


def orbit_func_1(beta, q):
    """
    Orbit func with gamma as isotropic re-emission

    f lower than 1 means
    """
    f = beta / q + ((1 / q) + (1 / 2)) * (1 - beta) * (1 / (q + 1))

    return f


def orbit_func_circumbinary(beta, q, alpha):
    """
    Orbit func where mass is lost to a circumbinary right, taking with it the specific angular momentum of that ring.
    """

    f = (
        beta / q
        + ((1 + q) * (1 - beta) * (math.sqrt(alpha))) / (q)
        + ((1 - beta) * (math.sqrt(alpha))) / (2 * (1 + q))
    )

    return f


def calc_prob(df):
    """
    Calculates the occurences and probabilites of the systems initially having a disk RLOF or not
    """

    # Select systems, group by rlof type and sum the probability
    no_disk_prob = df[df.disk == 0]["probability"].sum()
    disk_prob = df[df.disk == 1]["probability"].sum()

    # Calculate total probability
    tot_prob = no_disk_prob + disk_prob

    # Normalise to total probability (so it becomes a percentage)
    no_disk_percent = no_disk_prob / tot_prob
    disk_percent = disk_prob / tot_prob

    return {
        "tot_prob": tot_prob,
        "no_disk_prob": no_disk_prob,
        "disk_prob": disk_prob,
        "no_disk_percent": no_disk_percent,
        "disk_percent": disk_percent,
        "tot_amt": len(df.index),
        "disk_amt": len(df[df.disk == 1].index),
        "no_disk_amt": len(df[df.disk == 0].index),
    }


def add_thermal_mass_transfer_rate(df):
    """
    Function to add the thermal mass transfer rate
    """

    lsun = df["luminosity_accretor"] * (u.Lsun.to(u.W))
    msun = df["mass_accretor"] * (u.Msun.to(u.kg))
    rsun = df["radius_accretor"] * (u.Rsun.to(u.m))
    gg = const.G
    mthermal = (rsun * lsun) / (gg * msun) * (u.kg / u.s).to(u.Msun / u.yr)

    df["thermal_mass_transfer_rate_donor"] = mthermal

    return df
