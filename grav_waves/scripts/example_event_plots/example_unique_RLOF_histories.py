"""
Routine to get the unique RLOF histories of systems merging at z=0.

Now based on the follow sequence of steps:
- group all events by uuid
- extract the chronological event histories and create the unique list from that
- use the unique list to filter the groups (or original df)

These steps effectively create a new groupby grouping based on the result of a previous groupby.
TODO: this can be done more efficient by index selection of the dataframe if we use the indices of the dataframes
"""

import numpy as np
import pandas as pd
from grav_waves.functions.events import merge_with_event_dataframe, print_rows_and_cols


import matplotlib.pyplot as plt
from matplotlib import colors
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

import matplotlib
from plot_utils import get_vmin


def example_unique_RLOF_histories(
    combined_dataframes, rate_array, rlof_event_dataframe
):
    """
    Example plot to show unique RLOF histories
    """

    merged_rlof_df = merge_with_event_dataframe(
        main_dataframe=combined_dataframes, event_dataframe=rlof_event_dataframe
    )
    print_rows_and_cols(merged_rlof_df, "merged_df")

    # Set rate array
    merged_rlof_df["rates"] = rate_array[merged_rlof_df["initial_indices"].to_numpy()]

    # Sort by time
    merged_rlof_df = merged_rlof_df.sort_values(by="initial_time")

    # Add RLOF type names
    merged_rlof_df.loc["RLOF_TYPE"] = 0
    merged_rlof_df.loc[merged_rlof_df["initial_stability"] == 0, "RLOF_TYPE"] = "SMT"
    merged_rlof_df.loc[merged_rlof_df["initial_stability"] != 0, "RLOF_TYPE"] = "CEE"

    # Make groups based on uuid
    grouped = merged_rlof_df.groupby("uuid")

    # Find unique stability combinations within all groups
    print("Finding unique RLOF event histories")
    RLOF_type_group_series = [el[1]["RLOF_TYPE"].to_list() for el in grouped]
    unique_RLOF_type_groups_series = []
    for RLOF_type_group_serie in RLOF_type_group_series:
        if not RLOF_type_group_serie in unique_RLOF_type_groups_series:
            unique_RLOF_type_groups_series.append(RLOF_type_group_serie)

    # Create groups (or combination of groups) that satisfy having the specific
    print("Filtering groups on unique RLOF event histories")
    filtered_df_groups = {}
    for unique_rlof_type_group in unique_RLOF_type_groups_series:
        num_events = len(unique_rlof_type_group)
        print("\tfiltering: {} ({})".format(unique_rlof_type_group, num_events))
        filtered = grouped.filter(
            lambda x: x["RLOF_TYPE"].to_list() == list(unique_rlof_type_group)
        )
        filtered_df_groups[str(unique_rlof_type_group)] = {
            "df": filtered,
            "total_rate": filtered["rates"].sum() / num_events,
        }

    for key, value in sorted(
        filtered_df_groups.items(), key=lambda x: x[1]["total_rate"]
    ):
        print("Combination: {} total rate: {}".format(key, value["total_rate"]))

    # # Find unique combinations and total rates
    # unique_combinations = {}
    # for group in grouped_merged_rlof_df:
    #     rate_system = group[1].iloc[0]["rates"]
    #     event_history = str(group[1]["RLOF_TYPE"].to_numpy())

    #     if event_history not in unique_combinations:
    #         unique_combinations[event_history] = 0

    #     unique_combinations[event_history] += rate_system

    # # Set bins
    # numbins = 50
    # x_bins = 10 ** np.linspace(-4, 2, numbins)
    # y_bins = 10 ** np.linspace(-4, 2, numbins)

    # #
    # hist, _, _ = np.histogram2d(
    #     unstable_rlofs["initial_orbital_period"],
    #     unstable_rlofs["final_orbital_period"],
    #     bins=[x_bins, y_bins],
    #     weights=unstable_rlofs["rates"],
    # )

    # # print(unstable_rlofs[['initial_orbital_period', 'final_orbital_period']])
    # # quit()
    # norm = colors.LogNorm(
    #     vmin=get_vmin(hist, 1e-6),
    #     vmax=np.max(hist),
    # )

    # # Set up figure logic
    # fig = plt.figure(figsize=(20, 20))
    # gs = fig.add_gridspec(nrows=1, ncols=5)
    # rates_axis = fig.add_subplot(gs[:, :-1])
    # ax_cb = fig.add_subplot(gs[:, -1:])

    # plot_settings = {}
    # # make bincenters
    # x_bincenters = (x_bins[1:] + x_bins[:-1]) / 2
    # y_bincenters = (y_bins[1:] + y_bins[:-1]) / 2

    # X, Y = np.meshgrid(x_bincenters, y_bincenters)
    # _ = rates_axis.pcolormesh(
    #     X,
    #     Y,
    #     hist.T,
    #     norm=norm,
    #     shading="auto",
    #     antialiased=plot_settings.get("antialiased", True),
    #     rasterized=plot_settings.get("rasterized", True),
    # )

    # #
    # rates_axis.set_yscale("log")
    # rates_axis.set_xscale("log")
    # rates_axis.plot([10**-4, 10**2], [10**-4, 10**2], "--", alpha=0.5)

    # # make colorbar
    # cbar = matplotlib.colorbar.ColorbarBase(
    #     ax_cb,
    #     norm=norm,
    # )
    # cbar.ax.set_ylabel(r"{} density [{}]".format(1, 2))

    # plt.show()
