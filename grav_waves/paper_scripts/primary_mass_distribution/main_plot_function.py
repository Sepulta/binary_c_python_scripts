"""
Function to plot the paper plot for the primary mass at zero redshift comparison between variations
TODO: Make sure that what we want to show is correct
"""

import os

import numpy as np

import pandas as pd

import matplotlib.pyplot as plt
from matplotlib import colors

from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    extract_columns_from_querylist,
    quantity_name_dict,
    quantity_unit_dict,
    rate_type_name_dict,
    rate_density_units,
    add_columns_to_df,
    dco_type_query,
    readout_rate_array_specific_keys,
    create_centers_from_bins,
    get_closest_redshift_key,
    get_histogram_data,
    get_KDE_data,
    return_1d_plot_canvas,
    plot_1d_results,
    hatch_list,
    linestyle_list,
)
from grav_waves.convolution.functions.new_plot_routines.plot_kde_and_bootstrap_functions import (
    run_bootstrap,
)

from grav_waves.paper_scripts.primary_mass_distribution.plot_utils import (
    handle_columns,
    get_mask,
    get_rate_data,
    handle_comparison_to_observations,
    get_median_percentiles,
    likelihood,
    handle_comparison_to_observations,
)

from grav_waves.scripts.add_confidence_interval_powerlaw_peak.functions import (
    add_confidence_interval_powerlaw_peak_primary_mass,
    get_data_powerlaw_peak_primary_mass,
)


from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.linestyle_hatch_colorlist import (
    color_list as color_list_rlofs,
)

custom_mpl_settings.load_mpl_rc()


def get_data(
    filename,
    rate_type,
    dco_type,
    quantity,
    quantity_bins,
    divide_by_binsize,
    redshift_value,
    add_rates_plots=True,
    add_secondary_2d_plot=True,
    second_quantity=None,
    second_quantity_bins=None,
    bootstraps=0,
    general_query=None,
    querylist=None,
    add_fraction_primary_underwent_ppisn_plot=False,
    add_fraction_secondary_underwent_ppisn_plot=False,
    add_comparison_to_observations=False,
    add_rlof_types_to_plot=False,
):
    """
    Function to read out the data from the datafile
    """

    ####
    # Read out dataframe
    combined_df = pd.read_hdf(filename, "/data/combined_dataframes")

    # Add columns
    handle_columns(
        combined_df,
        quantity,
        querylist,
        extra_columns=[
            "primary_undergone_ppisn",
            "secondary_undergone_ppisn",
            second_quantity,
        ],
    )

    # Create masks and apply, taking into account that the query and general query can be None
    mask = get_mask(combined_df, dco_type, general_query, query=None)

    # Get the quantity data from the dataframe
    quantity_data = combined_df[quantity]
    primary_undergone_ppisn = combined_df["primary_undergone_ppisn"]
    secondary_undergone_ppisn = combined_df["secondary_undergone_ppisn"]

    cee_data = combined_df[mask]["comenv_counter"].to_numpy()
    smt_data = combined_df[mask]["stable_rlof_counter"].to_numpy()

    # mask the quantity data
    masked_quantity_data = quantity_data[mask].to_numpy()
    masked_primary_undergone_ppisn = primary_undergone_ppisn[mask].to_numpy()
    masked_secondary_undergone_ppisn = secondary_undergone_ppisn[mask].to_numpy()

    ################
    # Read out rate data for this specific redshift
    rate_array = get_rate_data(filename, rate_type, redshift_value, mask)

    ################
    # Calculate the rate histogram
    if add_rates_plots:
        hist, bincenter, truncated_bins = get_histogram_data(
            bins=quantity_bins,
            data_array=masked_quantity_data,
            weight_array=rate_array[0],
        )

        # Divide by binsize
        if divide_by_binsize:
            hist = hist / np.diff(quantity_bins)

        #
        rates_return_dict = {
            "centers": bincenter,
            "rates": hist,
        }

    ############
    # Handle comparison to observations setup
    if add_comparison_to_observations:
        # Get all indices
        all_indices = np.arange(len(bincenter))

        # Select those that have rates above 0
        non_zero_indices = all_indices[hist > 0]
        non_zero_centers = bincenter[hist > 0]

        # Select those above 10
        indices_above_ten = non_zero_indices[non_zero_centers > 10]
        centers_above_ten = non_zero_centers[non_zero_centers > 10]

        #
        data_powerlaw_peak_primary_mass = get_data_powerlaw_peak_primary_mass(
            data_root=os.path.join(os.environ["DATAFILES_ROOT"], "GW"),
        )

        (rates_comparison_values) = handle_comparison_to_observations(
            indices=indices_above_ten,
            centers=centers_above_ten,
            rates=hist[indices_above_ten],
            data_powerlaw_peak_primary_mass=data_powerlaw_peak_primary_mass,
        )

        # Set up return dict
        observation_comparison_return_dict = {
            "centers": centers_above_ten,
            "rates": rates_comparison_values,
        }

    ############
    # Handle fraction primary undergoing PPISN setup
    if add_fraction_primary_underwent_ppisn_plot:
        #####
        # Calculate the quantity and rate data where the primary underwent ppisn
        masked_quantity_data_where_primary_undergone_ppisn = masked_quantity_data[
            masked_primary_undergone_ppisn == 1
        ]
        rate_array_where_primary_undergone_ppisn = rate_array[
            :, masked_primary_undergone_ppisn == 1
        ]

        # Calculate the rate histogram for the primary having undergone ppisn
        (
            hist_where_primary_undergone_ppisn,
            bincenter_where_primary_undergone_ppisn,
            truncated_bins_where_primary_undergone_ppisn,
        ) = get_histogram_data(
            bins=quantity_bins,
            data_array=masked_quantity_data_where_primary_undergone_ppisn,
            weight_array=rate_array_where_primary_undergone_ppisn[0],
        )

        # divide by binsize
        if divide_by_binsize:
            hist_where_primary_undergone_ppisn = (
                hist_where_primary_undergone_ppisn / np.diff(quantity_bins)
            )

        # Calculate the ratio between the two
        ratio_to_total_hist_where_primary_undergone_ppisn = np.divide(
            hist_where_primary_undergone_ppisn,
            hist,
            out=hist_where_primary_undergone_ppisn,
            where=hist != 0,
        )  # only divide nonzeros else 1

        #
        primary_fraction_return_dict = {
            "centers": bincenter_where_primary_undergone_ppisn,
            "rates": ratio_to_total_hist_where_primary_undergone_ppisn,
        }

    ############
    # Handle secondary quantity setup
    if add_secondary_2d_plot:
        # Get the quantity data from the dataframe
        second_quantity_data = combined_df[second_quantity]

        # mask the quantity data
        second_masked_quantity_data = second_quantity_data[mask].to_numpy()

        # create histogram
        (second_quantity_2d_hist, _, _) = np.histogram2d(
            masked_quantity_data,
            second_masked_quantity_data,
            bins=[quantity_bins, second_quantity_bins],
            weights=rate_array[0],
        )

        # TODO: handle binsize division

        # Divide by the rates array to normalise
        second_quantity_2d_hist = (
            second_quantity_2d_hist
            / np.sum(second_quantity_2d_hist, axis=1)[:, np.newaxis]
        )

        #
        secondary_2d_plot_return_dict = {
            "rates": second_quantity_2d_hist,
        }

    ############
    # Handle rlof type quantity setupcolor_li
    if add_rlof_types_to_plot:

        # Set up
        rlof_types_return_dict = {}

        query_list = [
            {
                "query_indices": ((cee_data > 0) & (smt_data == 0)),
                "name": "cee",
                "longname": "CEE only",
            },
            {
                "query_indices": ((smt_data > 0) & (cee_data == 0)),
                "name": "smt",
                "longname": "SMT only",
            },
            {
                "query_indices": ((smt_data > 0) & (cee_data > 0)),
                "name": "both",
                "longname": "CEE & SMT",
            },
            {
                "query_indices": ((smt_data == 0) & (cee_data == 0)),
                "name": "neither",
                "longname": "No MT",
            },
        ]

        # Loop over different types
        for query in query_list:
            query_indices = query["query_indices"]

            masked_quantity_data_query = masked_quantity_data[query_indices]
            rate_array_data_query = rate_array[:, query_indices]

            # Calculate the rate histogram for the primary having undergone ppisn
            (
                hist_query,
                bincenter_hist_query,
                truncated_bins_query,
            ) = get_histogram_data(
                bins=quantity_bins,
                data_array=masked_quantity_data_query,
                weight_array=rate_array_data_query[0],
            )

            # divide by binsize
            if divide_by_binsize:
                hist_query = hist_query / np.diff(quantity_bins)

            rlof_types_return_dict[query["name"]] = {
                "rates": hist_query,
                "centers": bincenter_hist_query,
                "name": query["longname"],
            }

    ############
    # Handle secondary PPISN in primary mass bin
    if add_fraction_secondary_underwent_ppisn_plot:
        #####
        # Calculate the quantity and rate data where the secondary underwent ppisn
        masked_quantity_data_where_secondary_undergone_ppisn = masked_quantity_data[
            masked_secondary_undergone_ppisn == 1
        ]
        rate_array_where_secondary_undergone_ppisn = rate_array[
            :, masked_secondary_undergone_ppisn == 1
        ]

        # Calculate the rate histogram for the secondary having undergone ppisn
        (
            hist_where_secondary_undergone_ppisn,
            bincenter_where_secondary_undergone_ppisn,
            truncated_bins_where_secondary_undergone_ppisn,
        ) = get_histogram_data(
            bins=quantity_bins,
            data_array=masked_quantity_data_where_secondary_undergone_ppisn,
            weight_array=rate_array_where_secondary_undergone_ppisn[0],
        )

        # divide by binsize
        if divide_by_binsize:
            hist_where_secondary_undergone_ppisn = (
                hist_where_secondary_undergone_ppisn / np.diff(quantity_bins)
            )

        # Calculate the ratio between the two
        ratio_to_total_hist_where_secondary_undergone_ppisn = np.divide(
            hist_where_secondary_undergone_ppisn,
            hist,
            out=hist_where_secondary_undergone_ppisn,
            where=hist != 0,
        )  # only divide nonzeros else 1

        #
        secondary_fraction_return_dict = {
            "centers": bincenter_where_secondary_undergone_ppisn,
            "rates": ratio_to_total_hist_where_secondary_undergone_ppisn,
        }

    #########
    # Handle Bootstrap
    if bootstraps:
        # Get a list of indices
        indices = np.arange(len(masked_quantity_data))

        #########
        # Set up bootstrap array for rates:
        if add_rates_plots:
            bootstrapped_rates_hist_vals = np.zeros(
                (bootstraps, len(rates_return_dict["centers"]))
            )  # center_bins

        #########
        # Set up bootstrap array for fraction undergoing PPISN:
        if add_fraction_primary_underwent_ppisn_plot:
            # Set up array that stores all the boostrapped results
            bootstrapped_fraction_primary_hist_vals = np.zeros(
                (bootstraps, len(primary_fraction_return_dict["centers"]))
            )  # center_bins

        #########
        # Set up bootstrap array for fraction secondary undergoing PPISN:
        if add_fraction_secondary_underwent_ppisn_plot:
            # Set up array that stores all the boostrapped results
            bootstrapped_fraction_secondary_hist_vals = np.zeros(
                (bootstraps, len(secondary_fraction_return_dict["centers"]))
            )  # center_bins

        #########
        # Set up bootstrap array for comparison to observation
        if add_comparison_to_observations:
            # Set up array that stores all the boostrapped results
            bootstrapped_observation_comparison_hist_vals = np.zeros(
                (bootstraps, len(observation_comparison_return_dict["centers"]))
            )  # center_bins

        ##########
        # Run bootstrap loop
        for bootstrap_i in range(bootstraps):
            print("Bootstrap {}".format(bootstrap_i))
            ##############################
            # Get bootstrap indices
            boot_index = np.random.choice(
                indices,
                size=len(indices),
                replace=True,
                # p=rate_array[0][indices] / np.sum(rate_array[0][indices]),
            )

            #########
            # Calculate rates data with the bootstrapped set of indices
            if add_rates_plots:
                # Select the quantity values with these indices
                bootstrapped_masked_quantity_data = masked_quantity_data[boot_index]

                # Select the rate values with these indices
                bootstrapped_rate_array = rate_array[:, boot_index]

                ##############################
                # Calculate the rate histogram
                (bootstrapped_hist, _, _,) = get_histogram_data(
                    bins=quantity_bins,
                    data_array=bootstrapped_masked_quantity_data,
                    weight_array=bootstrapped_rate_array[0],
                )

                # Store unfiltered rate in array
                bootstrapped_rates_hist_vals[bootstrap_i] = bootstrapped_hist

            #########
            # Calculate fraction undergoing PPISN data with the bootstrapped set of indices
            if add_fraction_primary_underwent_ppisn_plot:
                # Select the data for whether the primary underwent PPISN or not
                bootstrapped_masked_primary_undergone_ppisn = (
                    masked_primary_undergone_ppisn[boot_index]
                )

                # Calculate the quantity and rate data where the primary underwent ppisn
                bootstrapped_masked_quantity_data_where_primary_undergone_ppisn = (
                    bootstrapped_masked_quantity_data[
                        bootstrapped_masked_primary_undergone_ppisn == 1
                    ]
                )
                bootstrapped_rate_array_where_primary_undergone_ppisn = (
                    bootstrapped_rate_array[
                        :, bootstrapped_masked_primary_undergone_ppisn == 1
                    ]
                )

                # Calculate the rate histogram for the primary having undergone ppisn
                (
                    bootstrapped_hist_where_primary_undergone_ppisn,
                    _,
                    _,
                ) = get_histogram_data(
                    bins=quantity_bins,
                    data_array=bootstrapped_masked_quantity_data_where_primary_undergone_ppisn,
                    weight_array=bootstrapped_rate_array_where_primary_undergone_ppisn[
                        0
                    ],
                )

                # Calculate the ratio between the total rate and the one where primary underwent PPISN
                bootstrapped_ratio_to_total_hist_where_primary_undergone_ppisn = (
                    np.divide(
                        bootstrapped_hist_where_primary_undergone_ppisn,
                        bootstrapped_hist,
                        out=bootstrapped_hist_where_primary_undergone_ppisn,
                        where=bootstrapped_hist != 0,
                    )
                )  # only divide nonzeros else 1

                # Store in array
                bootstrapped_fraction_primary_hist_vals[
                    bootstrap_i
                ] = bootstrapped_ratio_to_total_hist_where_primary_undergone_ppisn

            #########
            # Calculate fraction undergoing secondary PPISN data with the bootstrapped set of indices
            if add_fraction_secondary_underwent_ppisn_plot:
                # Select the data for whether the secondary underwent PPISN or not
                bootstrapped_masked_secondary_undergone_ppisn = (
                    masked_secondary_undergone_ppisn[boot_index]
                )

                # Calculate the quantity and rate data where the secondary underwent ppisn
                bootstrapped_masked_quantity_data_where_secondary_undergone_ppisn = (
                    bootstrapped_masked_quantity_data[
                        bootstrapped_masked_secondary_undergone_ppisn == 1
                    ]
                )
                bootstrapped_rate_array_where_secondary_undergone_ppisn = (
                    bootstrapped_rate_array[
                        :, bootstrapped_masked_secondary_undergone_ppisn == 1
                    ]
                )

                # Calculate the rate histogram for the secondary having undergone ppisn
                (
                    bootstrapped_hist_where_secondary_undergone_ppisn,
                    _,
                    _,
                ) = get_histogram_data(
                    bins=quantity_bins,
                    data_array=bootstrapped_masked_quantity_data_where_secondary_undergone_ppisn,
                    weight_array=bootstrapped_rate_array_where_secondary_undergone_ppisn[
                        0
                    ],
                )

                # Calculate the ratio between the total rate and the one where secondary underwent PPISN
                bootstrapped_ratio_to_total_hist_where_secondary_undergone_ppisn = (
                    np.divide(
                        bootstrapped_hist_where_secondary_undergone_ppisn,
                        bootstrapped_hist,
                        out=bootstrapped_hist_where_secondary_undergone_ppisn,
                        where=bootstrapped_hist != 0,
                    )
                )  # only divide nonzeros else 1

                # Store in array
                bootstrapped_fraction_secondary_hist_vals[
                    bootstrap_i
                ] = bootstrapped_ratio_to_total_hist_where_secondary_undergone_ppisn

            #########
            # Calculate comparison to observation with the bootstrapped set of indices
            if add_comparison_to_observations:
                rates_comparison_values = handle_comparison_to_observations(
                    indices=indices_above_ten,
                    centers=centers_above_ten,
                    rates=bootstrapped_hist[indices_above_ten],
                    data_powerlaw_peak_primary_mass=data_powerlaw_peak_primary_mass,
                )
                bootstrapped_observation_comparison_hist_vals[
                    bootstrap_i
                ] = rates_comparison_values

        ###########
        # Calculate median and percentiles
        if add_rates_plots:
            bootstrapped_median_percentiles_dict = get_median_percentiles(
                bootstrapped_rates_hist_vals
            )
            rates_return_dict[
                "median_percentiles"
            ] = bootstrapped_median_percentiles_dict

        ###########
        # Calculate median and percentiles for the ratio which undergoes ppisn
        if add_fraction_primary_underwent_ppisn_plot:
            bootstrapped_primary_fraction_median_percentiles_dict = (
                get_median_percentiles(bootstrapped_fraction_primary_hist_vals)
            )

            # Store in results
            primary_fraction_return_dict[
                "median_percentiles"
            ] = bootstrapped_primary_fraction_median_percentiles_dict

        ###########
        # Calculate median and percentiles for the ratio which undergoes ppisn
        if add_fraction_secondary_underwent_ppisn_plot:
            bootstrapped_fraction_secondary_median_percentiles_dict = (
                get_median_percentiles(bootstrapped_fraction_secondary_hist_vals)
            )

            # Store in results
            secondary_fraction_return_dict[
                "median_percentiles"
            ] = bootstrapped_fraction_secondary_median_percentiles_dict

        ###########
        # Calculate median and percentiles for the observation comparison
        if add_comparison_to_observations:
            bootstrapped_observation_comparison_median_percentiles_dict = (
                get_median_percentiles(bootstrapped_observation_comparison_hist_vals)
            )

            # Store in results
            observation_comparison_return_dict[
                "median_percentiles"
            ] = bootstrapped_observation_comparison_median_percentiles_dict

    ############
    # Set up return dict
    return_dict = {}

    if add_rates_plots:
        return_dict["rates_results"] = rates_return_dict
    if add_fraction_primary_underwent_ppisn_plot:
        return_dict["primary_fraction_results"] = primary_fraction_return_dict
    if add_comparison_to_observations:
        return_dict[
            "observation_comparison_results"
        ] = observation_comparison_return_dict
    if add_secondary_2d_plot:
        return_dict["secondary_2d_plot_results"] = secondary_2d_plot_return_dict
    if add_rlof_types_to_plot:
        return_dict["rlof_type_plot_results"] = rlof_types_return_dict
    if add_fraction_secondary_underwent_ppisn_plot:
        return_dict["secondary_fraction_results"] = secondary_fraction_return_dict

    return return_dict


def generate_primary_mass_at_redshift_zero_plot(
    convolved_datasets,
    primary_mass_bins,
    divide_by_binsize,
    add_rates_plots=True,
    add_ratio_to_fiducial_plot=False,
    add_residual_to_fiducial_plot=False,
    add_fraction_primary_underwent_ppisn_plot=False,
    add_fraction_secondary_underwent_ppisn_plot=False,
    show_ppisn_fractions_in_log=False,
    add_comparison_to_observations=False,
    add_rlof_types_to_plot=False,
    add_secondary_2d_plot=False,
    second_quantity=None,
    second_quantity_bins=None,
    bootstraps=0,
    general_query=None,
    plot_settings={},
):
    """
    Function to plot the paper plot for the primary mass at zero redshift comparison between variations
    """

    cmap = plt.cm.get_cmap("viridis", len(convolved_datasets) - 1)
    color_list = [cmap(i) for i in range(cmap.N)]

    # Add other color type at the end
    color_list.append("orange")

    #####################################
    # Calculate the results
    result_dict_rates = {}
    for convolved_dataset_i, (
        convolved_dataset_key,
        convolved_dataset_filename,
    ) in enumerate(convolved_datasets.items()):

        result_dict_rates[convolved_dataset_key] = get_data(
            filename=convolved_dataset_filename,
            rate_type="merger_rate",
            dco_type="bhbh",
            quantity="primary_mass",
            quantity_bins=primary_mass_bins,
            divide_by_binsize=divide_by_binsize,
            redshift_value=0,
            bootstraps=bootstraps,
            general_query=general_query,
            # general_query=general_query if convolved_dataset_i > 0 else None,
            add_rates_plots=add_rates_plots,
            add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
            add_fraction_secondary_underwent_ppisn_plot=add_fraction_secondary_underwent_ppisn_plot,
            add_comparison_to_observations=add_comparison_to_observations,
            add_rlof_types_to_plot=add_rlof_types_to_plot,
            add_secondary_2d_plot=add_secondary_2d_plot,
            second_quantity=second_quantity,
            second_quantity_bins=second_quantity_bins,
        )

    # Settings
    linewidth = 5
    fig_scale = 15
    gs_scale = 1
    levels = (
        (2 * add_rates_plots)
        + add_ratio_to_fiducial_plot
        + add_residual_to_fiducial_plot
        + add_fraction_primary_underwent_ppisn_plot
        + add_fraction_secondary_underwent_ppisn_plot
        + add_comparison_to_observations
        + add_rlof_types_to_plot
        + add_secondary_2d_plot
    )
    level = 0

    #########
    # Set up figure logic
    fig = plt.figure(figsize=(1.5 * fig_scale, 1 * fig_scale))
    gs = fig.add_gridspec(nrows=levels * gs_scale, ncols=1)
    axes_list = []

    # Add rates plot
    if add_rates_plots:
        # set up the subplot
        rates_axis = fig.add_subplot(gs[level * gs_scale : (level + 2) * gs_scale, 0])
        level += 2
        axes_list.append(rates_axis)

        ylabel = "$\\frac{d\\mathcal{R}}{dm_{1}}$ [Gpc$^{-3}$yr$^{-1}M_{\\odot}^{-1}$]"  # ligo plot
        rates_axis.set_ylabel(
            ylabel,
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

    # Add ratio to fiducial to plot
    if add_ratio_to_fiducial_plot:
        ratio_to_fiducial_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(ratio_to_fiducial_axis)

    # add residual to fiducial plot:
    if add_residual_to_fiducial_plot:
        residual_to_fiducial_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(residual_to_fiducial_axis)

    # Add fraction of primary that underwent PPISN
    if add_fraction_primary_underwent_ppisn_plot:
        fraction_primary_underwent_ppisn_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(fraction_primary_underwent_ppisn_axis)

        #
        fraction_primary_underwent_ppisn_axis.set_ylabel(
            "Fraction primary\n underwent PPISN",
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

    # Add fraction of secondary that underwent PPISN
    if add_fraction_secondary_underwent_ppisn_plot:
        fraction_secondary_underwent_ppisn_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(fraction_secondary_underwent_ppisn_axis)

        #
        fraction_secondary_underwent_ppisn_axis.set_ylabel(
            "Fraction secondary\n underwent PPISN",
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

    # Add comparison to observations axis
    if add_comparison_to_observations:
        comparison_to_observations_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(comparison_to_observations_axis)

    # Add secondary 2d plot axis
    if add_secondary_2d_plot:
        secondary_2d_plot_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(secondary_2d_plot_axis)

        #
        secondary_2d_plot_axis.set_ylabel(
            "{} {}".format(
                quantity_name_dict[second_quantity],
                "[{}]".format(quantity_unit_dict[second_quantity].to_string("latex"))
                if quantity_unit_dict[second_quantity]
                else "",
            ),
            fontsize=plot_settings.get("axislabel_fontsize", 26),
        )

    # Add plot containing RLOF types
    if add_rlof_types_to_plot:
        rlof_type_plot_axis = fig.add_subplot(
            gs[level * gs_scale : (level + 1) * gs_scale, 0]
        )
        level += 1
        axes_list.append(rlof_type_plot_axis)

        #
        rlof_type_plot_axis.set_ylabel("RLOF type fraction")

    #####################
    # Loop over all the results of the sampling
    for result_i, (result_name, result) in enumerate(result_dict_rates.items()):

        ######
        # Handle rates plots
        if add_rates_plots:
            if not bootstraps:
                # Plot histograms
                rates_axis.plot(
                    result["rates_results"]["centers"],
                    result["rates_results"]["rates"],
                    lw=linewidth,
                    c=color_list[result_i],
                    zorder=200,
                    alpha=1,
                    linestyle=linestyle_list[result_i],
                    label=result_name,
                )

            else:
                # Plot median and bootstrap
                rates_axis.plot(
                    result["rates_results"]["centers"],
                    result["rates_results"]["median_percentiles"]["median"][0],
                    lw=linewidth,
                    c=color_list[result_i],
                    zorder=13,
                    linestyle=linestyle_list[result_i],
                    label=result_name,
                )

                #
                rates_axis.fill_between(
                    result["rates_results"]["centers"],
                    result["rates_results"]["median_percentiles"]["90%_CI"][0],
                    result["rates_results"]["median_percentiles"]["90%_CI"][1],
                    alpha=0.4,
                    zorder=11,
                    color=color_list[result_i],
                )  # 1-sigma

            # set ylims
            rates_axis.set_ylim([1e-3, 1e1])

            # Make up
            rates_axis.set_yscale("log")

        ######
        # Handle fraction primary that underwent PPISN plots
        if add_fraction_primary_underwent_ppisn_plot:
            # Plot histograms
            fraction_primary_underwent_ppisn_axis.plot(
                result["primary_fraction_results"]["centers"],
                result["primary_fraction_results"]["rates"],
                lw=linewidth,
                c=color_list[result_i],
                zorder=200,
                alpha=1,
                linestyle=linestyle_list[result_i],
            )

            if bootstraps:
                fraction_primary_underwent_ppisn_axis.plot(
                    result["primary_fraction_results"]["centers"],
                    result["primary_fraction_results"]["median_percentiles"]["median"][
                        0
                    ],
                    lw=linewidth,
                    c=color_list[result_i],
                    zorder=13,
                )

                fraction_primary_underwent_ppisn_axis.fill_between(
                    result["primary_fraction_results"]["centers"],
                    result["primary_fraction_results"]["median_percentiles"]["90%_CI"][
                        0
                    ],
                    result["primary_fraction_results"]["median_percentiles"]["90%_CI"][
                        1
                    ],
                    alpha=0.4,
                    zorder=11,
                    color=color_list[result_i],
                )  # 90% confidence interval

            # Normal fraction plot:
            fraction_primary_underwent_ppisn_axis.set_ylim(0, 1)

            # Display in log;
            if show_ppisn_fractions_in_log:
                values = result["primary_fraction_results"]["rates"]
                min_non_zero_value = np.min(values[values > 0])
                fraction_primary_underwent_ppisn_axis.set_ylim(min_non_zero_value, 1)
                fraction_primary_underwent_ppisn_axis.set_yscale("log")

        ######
        # Handle fraction secondary that underwent PPISN plots
        if add_fraction_secondary_underwent_ppisn_plot:
            # Plot histograms
            fraction_secondary_underwent_ppisn_axis.plot(
                result["secondary_fraction_results"]["centers"],
                result["secondary_fraction_results"]["rates"],
                lw=linewidth,
                c=color_list[result_i],
                zorder=200,
                alpha=1,
                linestyle=linestyle_list[result_i],
            )

            if bootstraps:
                fraction_secondary_underwent_ppisn_axis.plot(
                    result["secondary_fraction_results"]["centers"],
                    result["secondary_fraction_results"]["median_percentiles"][
                        "median"
                    ][0],
                    lw=linewidth,
                    c=color_list[result_i],
                    zorder=13,
                )

                fraction_secondary_underwent_ppisn_axis.fill_between(
                    result["secondary_fraction_results"]["centers"],
                    result["secondary_fraction_results"]["median_percentiles"][
                        "90%_CI"
                    ][0],
                    result["secondary_fraction_results"]["median_percentiles"][
                        "90%_CI"
                    ][1],
                    alpha=0.4,
                    zorder=11,
                    color=color_list[result_i],
                )  # 90% confidence interval

            # Normal fraction plot:
            fraction_secondary_underwent_ppisn_axis.set_ylim(0, 1)

            # Display in log;
            if show_ppisn_fractions_in_log:
                values = result["secondary_fraction_results"]["rates"]
                min_non_zero_value = np.min(values[values > 0])
                fraction_secondary_underwent_ppisn_axis.set_ylim(min_non_zero_value, 1)
                fraction_secondary_underwent_ppisn_axis.set_yscale("log")

        ######
        # Add fraction of primary underwent PPISN
        if add_comparison_to_observations:
            #
            comparison_to_observations_axis.set_ylabel(
                r"$\mathrm{log10}\left(\|\frac{\mathrm{obs}-\mathrm{mod}}{\mathrm{obs}}\|\right)$",
                fontsize=plot_settings.get("axislabel_fontsize", 26),
            )

            if bootstraps:
                # Plot median and bootstrap
                comparison_to_observations_axis.plot(
                    result["observation_comparison_results"]["centers"],
                    np.log10(
                        result["observation_comparison_results"]["median_percentiles"][
                            "median"
                        ][0]
                    ),
                    lw=linewidth,
                    c=color_list[result_i],
                    zorder=13,
                    linestyle=linestyle_list[result_i],
                    label=result_name,
                )

                #
                comparison_to_observations_axis.fill_between(
                    result["observation_comparison_results"]["centers"],
                    np.log10(
                        result["observation_comparison_results"]["median_percentiles"][
                            "90%_CI"
                        ][0]
                    ),
                    np.log10(
                        result["observation_comparison_results"]["median_percentiles"][
                            "90%_CI"
                        ][1]
                    ),
                    alpha=0.4,
                    zorder=11,
                    color=color_list[result_i],
                )  # 1-sigma

        ######
        # Add plot containing secondary (2-d) plot
        if add_secondary_2d_plot:
            secondary_2d_plot_data = result["secondary_2d_plot_results"]["rates"]

            norm = colors.LogNorm(
                vmin=1e-2,
                vmax=1e3,
            )

            norm = colors.Normalize(vmin=0, vmax=1)

            quantity_bins_centers = result["rates_results"]["centers"]
            second_quantity_bins_centers = (
                second_quantity_bins[1:] + second_quantity_bins[:-1]
            ) / 2

            quantity_bins_centers_X, second_quantity_bins_centers_Y = np.meshgrid(
                quantity_bins_centers, second_quantity_bins_centers
            )

            _ = secondary_2d_plot_axis.pcolormesh(
                quantity_bins_centers_X,
                second_quantity_bins_centers_Y,
                secondary_2d_plot_data.T,
                norm=norm,
                shading="auto",
                antialiased=plot_settings.get("antialiased", True),
                rasterized=plot_settings.get("rasterized", True),
            )

        ######
        # Add plot containing the RLOF type fractions
        if add_rlof_types_to_plot:
            rlof_type_plot_data = result["rlof_type_plot_results"]

            # Set up stacked array
            stacked = np.zeros(shape=(len(result["rates_results"]["centers"])))

            # Loop over the rlof types
            for rlof_type_i, rlof_type in enumerate(rlof_type_plot_data.keys()):
                fractions_rlof_type = np.divide(
                    rlof_type_plot_data[rlof_type]["rates"],
                    result["rates_results"]["rates"],
                    where=result["rates_results"]["rates"] != 0,
                    out=np.zeros_like(rlof_type_plot_data[rlof_type]["rates"]),
                )

                #
                rlof_type_plot_axis.bar(
                    rlof_type_plot_data[rlof_type]["centers"],
                    fractions_rlof_type,
                    width=np.diff(primary_mass_bins),
                    bottom=stacked,
                    label=rlof_type_plot_data[rlof_type]["name"],
                    color=color_list_rlofs[rlof_type_i],
                )
                stacked = stacked + fractions_rlof_type

        # Set y to log
        # rlof_type_plot_axis.set_yscale("log")
        rlof_type_plot_axis.legend()

    ####
    # Add confidence interval of observations
    fig, axes_list[0] = add_confidence_interval_powerlaw_peak_primary_mass(
        fig=fig,
        ax=axes_list[0],
        data_root=os.path.join(os.environ["DATAFILES_ROOT"], "GW"),
    )

    ##############
    # General make up of the plots
    for axes in axes_list[:-1]:
        axes.set_xticklabels([])
    for axes in axes_list:
        axes.set_xlim(2, 100)

    # Extra makeup
    axes_list[-1].set_xlabel(
        "{} [{}]".format(
            quantity_name_dict["primary_mass"],
            quantity_unit_dict["primary_mass"].to_string("latex"),
        ),
        fontsize=plot_settings.get("axislabel_fontsize", 26),
    )
    axes_list[0].legend(fontsize=plot_settings.get("legend_fontsize", 26))

    #########
    # Add plot dependent makeup
    if add_comparison_to_observations:
        #
        region_of_interest_bounds = np.array([32, 38])

        # Add plot to band of interest
        comparison_to_observations_axis_ylims = (
            comparison_to_observations_axis.get_ylim()
        )
        comparison_to_observations_axis.axvspan(
            region_of_interest_bounds[0],
            region_of_interest_bounds[1],
            ymin=comparison_to_observations_axis_ylims[0],
            ymax=comparison_to_observations_axis_ylims[1],
            alpha=0.1,
            color="red",
        )

        # Add line of threshold
        comparison_to_observations_axis.axhline(-1, alpha=0.5, linestyle="--")

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #########
    #
    data_root = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"), "GRAV_WAVES/server_results/"
    )
    # data_root = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/"

    #########
    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"),
        "paper_gw/paper_tex/figures/primary_mass_distribution_at_redshift_zero_with_variations",
    )
    plot_dir = "plots"

    # configuration:
    primary_mass_bins = np.arange(0, 100, 2)
    divide_by_binsize = True
    add_rates_plots = True
    add_ratio_to_fiducial_plot = False
    add_residual_to_fiducial_plot = False
    add_fraction_primary_underwent_ppisn_plot = True
    add_fraction_secondary_underwent_ppisn_plot = False
    show_ppisn_fractions_in_log = False
    add_comparison_to_observations = False
    add_rlof_types_to_plot = True
    bootstraps = 50
    show_plot = True

    #######
    # Test

    #
    convolved_datasets = {
        # Fiducial and variations
        r"Fiducial v2.2.1 pre-merge (Super mid res)": os.path.join(
            data_root,
            # "EVENTS_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
            "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/convolution_results.h5",
        ),
    }
    #
    basename = "test.pdf"
    general_query = None

    generate_primary_mass_at_redshift_zero_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=primary_mass_bins,
        divide_by_binsize=divide_by_binsize,
        add_rates_plots=add_rates_plots,
        add_comparison_to_observations=add_comparison_to_observations,
        add_fraction_primary_underwent_ppisn_plot=add_fraction_primary_underwent_ppisn_plot,
        add_fraction_secondary_underwent_ppisn_plot=add_fraction_secondary_underwent_ppisn_plot,
        show_ppisn_fractions_in_log=show_ppisn_fractions_in_log,
        add_rlof_types_to_plot=add_rlof_types_to_plot,
        bootstraps=bootstraps,
        general_query=general_query,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "hspace": 0,
            "axislabel_fontsize": 32,
            "legend_fontsize": 32,
        },
    )
