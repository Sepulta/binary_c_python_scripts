"""
Function to check duplicate uuid system output.
"""
import os
import json
import pandas as pd

# simname_dir = "/home/david/data_projects/binary_c_data/GRAV_WAVES/TEST_LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/population_results"
# simname_dir = "/home/david/data_projects/binary_c_data/GRAV_WAVES/TEST_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/population_results"
# simname_dir = "/home/david/data_projects/binary_c_data/GRAV_WAVES/TEST_2_LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED//population_results"
simname_dir = "/home/david/data_projects/binary_c_data/GRAV_WAVES/TEST_2_LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/population_results"

#
metallicity_count_dict = {}
for metallicity_dir in os.listdir(simname_dir):
    if metallicity_dir.startswith("Z"):
        metallicity_count_dict[metallicity_dir] = {}
        combined_dco_file = os.path.join(
            simname_dir, metallicity_dir, "combined_dco.dat"
        )

        for el in os.listdir(os.path.join(simname_dir, metallicity_dir)):
            if el.endswith("settings.json"):
                with open(os.path.join(simname_dir, metallicity_dir, el), "r") as f:
                    settings = json.loads(f.read())

        bse_options = settings["population_settings"]["bse_options"]

        df = pd.read_csv(combined_dco_file, sep="\s+")
        print(df.columns)
        print(
            "METALLICITY {}: unique uuids: {}".format(
                metallicity_dir, len(df["uuid"].unique())
            )
        )
        # open pandas file
        grouped = df.groupby(by="uuid")
        for el in grouped:
            if len(el[1]) > 1:
                print(el)
                num_duplicates = len(el[1])

                # print(el[1][['stellar_type_1', 'stellar_type_2', 'fallback_1', 'fallback_2']])
                # print(el[1].iloc[0])
                # print(el[1].iloc[0]['probability'])
                if not num_duplicates in metallicity_count_dict[metallicity_dir]:
                    metallicity_count_dict[metallicity_dir][num_duplicates] = 0
                metallicity_count_dict[metallicity_dir][num_duplicates] += 1

                first_system = el[1].iloc[0]

                #######
                # Create a system dict
                system_dict = {
                    **bse_options,
                    "M_1": first_system["zams_mass_1"],
                    "M_2": first_system["zams_mass_2"],
                    "orbital_period": first_system["zams_period"],
                    "random_seed": first_system["random_seed"],
                }
                print(system_dict)

                binary_cstring = " ".join(
                    ["{} {}".format(key, value) for key, value in system_dict.items()]
                )
                print(binary_cstring)

print(metallicity_count_dict)
