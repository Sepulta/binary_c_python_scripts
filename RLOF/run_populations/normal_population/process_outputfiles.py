import os
import time
import h5py
import json
import pandas as pd

from david_phd_functions.binaryc.process_rlof_files import (
    process_RLOF_episodes_stable_initial_final,
)

DATA_ROOT = os.getenv("BINARYC_DATA_ROOT", None)
DATA_DIR = os.path.join(DATA_ROOT, "RLOF/RLOFING_systems_z0.01")

process_RLOF_episodes_stable_initial_final(DATA_DIR)
