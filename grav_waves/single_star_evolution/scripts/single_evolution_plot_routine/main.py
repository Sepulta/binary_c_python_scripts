"""
Script containing functions to generate the single star detailed evolution plots
"""

import os
import numpy as np
import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams["axes.facecolor"] = ".9"
import proplot as pplt

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

from david_phd_functions.binaryc.personal_defaults import personal_defaults

from binarycpython.utils.grid import Population

from binarycpython.utils import stellar_types

import astropy.units as u
import astropy.constants as const

from david_phd_functions.plotting.pdf_functions import (
    add_pdf_and_bookmark,
    generate_chapterpage_pdf,
)
from PyPDF2 import PdfFileMerger

from functions import defaults_settings, generate_evolution_data
from plot_functions import (
    plot_HR_diagram,
    plot_detailed_evolution,
    plot_combined_detailed_evolution,
    plot_combined_HR_diagram,
)


def generate_detailed_evolution_plots(
    plot_output_dir, pdf_output_name, time_window=None, **kwargs
):
    """
    Function to plot all the plots
    """

    include_HR_diagram_plot = True
    include_detailed_evolution_plot = True
    show_plots = False

    # current values for run:
    values_current_star = {**defaults_settings, **kwargs}

    # Get the evolution dataframe
    supernova_result_list, evolution_dataframe = generate_evolution_data(**kwargs)

    if time_window:
        if len(time_window) == 2:
            if (time_window[1] <= evolution_dataframe["time"].max()) and (
                time_window[0] >= evolution_dataframe["time"].min()
            ):
                evolution_dataframe = evolution_dataframe[
                    evolution_dataframe.time <= time_window[1]
                ][evolution_dataframe.time > time_window[0]]

    # Make output plot dir
    os.makedirs(os.path.abspath(plot_output_dir), exist_ok=True)

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    # Plot HR diagram
    if include_HR_diagram_plot:
        hr_plot_name = os.path.join(os.path.abspath(plot_output_dir), "HR_diagram.pdf")
        plot_HR_diagram(
            evolution_dataframe,
            color_type="age",
            plot_settings={
                "output_name": hr_plot_name,
                "show_plot": show_plots,
                "runname": "{}".format(values_current_star),
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger, hr_plot_name, pdf_page_number, bookmark_text="HR diagram"
        )

    # Plot evolution over time
    if include_detailed_evolution_plot:
        detailed_plot_name = os.path.join(
            os.path.abspath(plot_output_dir), "detailed_evolution.pdf"
        )
        plot_detailed_evolution(
            evolution_dataframe,
            {
                "output_name": detailed_plot_name,
                "show_plot": show_plots,
                "runname": "{}".format(values_current_star),
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            detailed_plot_name,
            pdf_page_number,
            bookmark_text="Detailed evolution",
        )

    # wrap up the pdf
    output_pdf_filename = os.path.abspath(
        os.path.join(plot_output_dir, pdf_output_name)
    )
    merger.write(output_pdf_filename)
    merger.close()

    print("Wrote combined pdf to:\n\t{}".format(output_pdf_filename))


def generate_combined_plots(
    plot_output_dir, pdf_output_name, masses, time_window=None, **kwargs
):
    """
    Function to plot some of the quantities on the same figure
    """

    include_detailed_evolution_plot = True
    include_HR_diagram_plot = True
    show_plots = False

    # # current values for run:
    values_current_star = {**defaults_settings, **kwargs}
    del values_current_star["M_1"]

    results = {}

    for mass in masses:
        kwargs["M_1"] = mass

        # Get the evolution dataframe
        supernova_result_list, evolution_dataframe = generate_evolution_data(**kwargs)

        #
        if time_window:
            if len(time_window) == 2:
                evolution_dataframe = evolution_dataframe[
                    evolution_dataframe.time <= time_window[1]
                ][evolution_dataframe.time > time_window[0]]

        #
        results[mass] = (supernova_result_list, evolution_dataframe)

    # Make output plot dir
    os.makedirs(os.path.abspath(plot_output_dir), exist_ok=True)

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    if include_HR_diagram_plot:
        detailed_plot_name = os.path.join(
            os.path.abspath(plot_output_dir), "combined_HR_diagram.pdf"
        )
        plot_combined_HR_diagram(
            results,
            color_type="age",
            plot_settings={
                "output_name": detailed_plot_name,
                "show_plot": show_plots,
                "runname": "{}".format(values_current_star),
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            detailed_plot_name,
            pdf_page_number,
            bookmark_text="Combined HR diagram",
        )

    # Plot evolution over time
    if include_detailed_evolution_plot:
        detailed_plot_name = os.path.join(
            os.path.abspath(plot_output_dir), "combined_detailed_evolution.pdf"
        )
        plot_combined_detailed_evolution(
            results,
            plot_settings={
                "output_name": detailed_plot_name,
                "show_plot": show_plots,
                "runname": "{}".format(values_current_star),
            },
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            detailed_plot_name,
            pdf_page_number,
            bookmark_text="Combined detailed evolution",
        )

    # wrap up the pdf
    output_pdf_filename = os.path.abspath(
        os.path.join(plot_output_dir, pdf_output_name)
    )
    merger.write(output_pdf_filename)
    merger.close()

    print("Wrote combined pdf to:\n\t{}".format(output_pdf_filename))


# generate_detailed_evolution_plots('plots/', 'general_35.pdf', M_1=35, metallicity=0.0001)
# generate_detailed_evolution_plots('plots/', 'general_36.pdf', M_1=36, metallicity=0.0001)
# generate_detailed_evolution_plots('plots/', 'general_37.pdf', M_1=37, metallicity=0.0001)

# generate_combined_plots('plots/', 'multiple_masses_plot.pdf', masses=[35], time_window=[5, 7], metallicity=1e-4)
# generate_combined_plots('plots/', 'multiple_masses_plot.pdf', masses=[34, 35, 36, 38], time_window=[5, 7], metallicity=1e-4)
# generate_combined_plots('plots/', 'multiple_masses_plot.pdf', masses=[32.75, 33.25, 33.75], metallicity=10**(-2.86666666666))

generate_combined_plots(
    "plots/",
    "multiple_masses_plot.pdf",
    masses=[19, 20, 21, 22],
    time_window=[8.5, 12],
    metallicity=10 ** (-4),
)
