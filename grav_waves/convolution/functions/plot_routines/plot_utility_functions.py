"""
Script containing utility functions for the plotting routines
"""

import time
import sys

from multiprocessing import Pool

import numpy as np
import pandas as pd

from david_phd_functions.plotting.custom_mpl_settings import LINESTYLE_TUPLE

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors


def get_rate_type_names(rate_type):
    """
    Function to get the rate type names
    """

    # Select correct rate type:
    if rate_type == "merger_rate":
        rate_key_in_dict = "merger_rate_data"
        rate_key_in_df = "merger_rates"
        long_name_rate_type = "Merger rate"
        rate_type_hdf5_key = "merger_rate_data"
        rate_time_key_in_df = "merger_time_values_in_years"
    elif rate_type == "formation_rate":
        rate_key_in_dict = "formation_rate_data"
        rate_key_in_df = "formation_rates"
        long_name_rate_type = "Formation rate"
        rate_type_hdf5_key = "formation_rate_data"
        rate_time_key_in_df = "formation_time_values_in_years"

    return (
        rate_key_in_dict,
        rate_key_in_df,
        long_name_rate_type,
        rate_type_hdf5_key,
        rate_time_key_in_df,
    )


def filter_dco_type(df, dco_type):
    """
    Function to filter the correct dco type in a dataframe

    dco_type can be 'bhbh', 'bhns', 'nsns', 'combined_dco'
    """

    if dco_type == "bhbh":
        return df.query("stellar_type_1 == 14 & stellar_type_2 == 14")
    if dco_type == "nsns":
        return df.query("stellar_type_1 == 13 & stellar_type_2 == 13")
    if dco_type == "bhns":
        return df.query(
            "(stellar_type_1 == 14 & stellar_type_2 == 13) | (stellar_type_1 == 13 & stellar_type_2 == 14)"
        )
    if dco_type == "combined_dco":
        return df
    else:
        msg = "dco_type '{}' is unknown. Abort".format(dco_type)
        raise ValueError(msg)


def create_any_mass_df(df):
    """
    Function to take a dataframe and split the _1 and _2 to without
    """

    # Create df 1 column
    df_1 = df.copy(deep=True)
    df_1 = df_1.drop(columns=[el for el in df_1.columns if el.endswith("_2")])

    df_1_rename_columns_dict = {
        el: el[:-2] for el in [el for el in df_1.columns if el.endswith("_1")]
    }
    df_1 = df_1.rename(columns=df_1_rename_columns_dict)

    # Create df 2 column
    df_2 = df.copy(deep=True)
    df_2 = df_2.drop(columns=[el for el in df_2.columns if el.endswith("_1")])

    df_2_rename_columns_dict = {
        el: el[:-2] for el in [el for el in df_2.columns if el.endswith("_2")]
    }
    df_2 = df_2.rename(columns=df_2_rename_columns_dict)

    # create new df
    any_mass_df = pd.DataFrame()

    any_mass_df = pd.concat([any_mass_df, df_1], ignore_index=True)
    any_mass_df = pd.concat([any_mass_df, df_2], ignore_index=True)

    return any_mass_df


def progressbar(it, prefix="", size=60, file=sys.stdout):
    count = len(it)

    def show(j):
        x = int(size * j / count)
        file.write("%s[%s%s] %i/%i\r" % (prefix, "#" * x, "." * (size - x), j, count))
        file.flush()

    show(0)
    for i, item in enumerate(it):
        yield item
        show(i + 1)
    file.write("\n")
    file.flush()


def get_hists_worker_function(task_dict):
    """
    Worker function to calculate the histogram.
    """

    # readout the task_dict
    redshift = task_dict["redshift"]
    hdf5_filename = task_dict["hdf5_filename"]
    rate_type_hdf5_key = task_dict["rate_type_hdf5_key"]
    query = task_dict["query"]
    quantity = task_dict["quantity"]
    rate_key_in_df = task_dict["rate_key_in_df"]
    divide_by_binsize = task_dict["divide_by_binsize"]
    bins = task_dict["bins"]
    combined_df = task_dict["combined_df"]

    bins_size = np.diff(bins)

    # Calculate the histogram
    redshift_value = float(redshift)

    index_df = pd.read_hdf(
        hdf5_filename, "/data/{}/{}".format(rate_type_hdf5_key, str(redshift_value))
    )

    joined_df = index_df.join(combined_df, on="local_index")

    if not query is None:
        joined_df = joined_df.query(query)

    # Create histogram:
    hist = np.histogram(
        joined_df[quantity], bins=bins, weights=joined_df[rate_key_in_df]
    )

    # Divide by binsize if we want
    if divide_by_binsize:
        hist[0] = hist[0] / bins_size

    print("Finished calculation for redshift {}".format(redshift_value))

    # Store in dict
    return {"hist": hist[0], "redshift_value": redshift_value}


def get_hists_worker_function(task_dict):
    """
    Worker function to calculate the histogram.
    """

    # readout the task_dict
    redshift = task_dict["redshift"]
    hdf5_filename = task_dict["hdf5_filename"]
    rate_type_hdf5_key = task_dict["rate_type_hdf5_key"]
    query = task_dict["query"]
    quantity = task_dict["quantity"]
    rate_key_in_df = task_dict["rate_key_in_df"]
    divide_by_binsize = task_dict["divide_by_binsize"]
    bins = task_dict["bins"]
    combined_df = task_dict["combined_df"]
    dco_type = task_dict["dco_type"]

    # Calculate the histogram
    redshift_value = float(redshift)

    start_readout = time.time()
    index_df = pd.read_hdf(
        hdf5_filename, "/data/{}/{}".format(rate_type_hdf5_key, str(redshift_value))
    )
    stop_readout = time.time()

    start_join = time.time()
    joined_df = index_df.join(combined_df, on="local_index")
    stop_join = time.time()

    # Select correct dco type
    start_query_and_filter = time.time()
    joined_df = filter_dco_type(joined_df, dco_type)

    if not query is None:
        joined_df = joined_df.query(query)
    stop_query_and_filter = time.time()

    # Create histogram:
    hist = np.histogram(
        joined_df[quantity], bins=bins, weights=joined_df[rate_key_in_df]
    )

    # Divide by binsize if we want
    if divide_by_binsize:
        bins_size = np.diff(bins)
        hist[0] = hist[0] / bins_size

    time_str = (
        "readout took: {:2E}s join took: {:2E}s query_and_filter took: {:2E}s".format(
            stop_readout - start_readout,
            stop_join - start_join,
            stop_query_and_filter - start_query_and_filter,
        )
    )
    print(
        "Finished calculation for redshift {:2E}".format(redshift_value)
        + " "
        + time_str
    )

    # Store in dict
    return {"hist": hist[0], "redshift_value": redshift_value}


def get_hists_multiprocessing(
    hdf5_filename,
    combined_df,
    dco_type,
    query,
    rate_key_in_df,
    bins,
    quantity,
    divide_by_binsize,
    rate_type_hdf5_key,
    sorted_keys,
    num_procs=1,
):
    """
    Function to get the histograms for each redshift with a  multiprocessing method
    """

    print("Calculating hists with {} processes".format(num_procs))

    res_dict = {}

    # Set up pool an create result
    with Pool(processes=num_procs) as pool:
        res_list = pool.map(
            get_hists_worker_function,
            [
                (
                    {
                        "redshift": redshift,
                        "hdf5_filename": hdf5_filename,
                        "rate_type_hdf5_key": rate_type_hdf5_key,
                        "dco_type": dco_type,
                        "query": query,
                        "quantity": quantity,
                        "rate_key_in_df": rate_key_in_df,
                        "divide_by_binsize": divide_by_binsize,
                        "bins": bins,
                        "combined_df": combined_df,
                    }
                )
                for redshift in sorted_keys
            ],
        )

    for result in res_list:
        res_dict[result["redshift_value"]] = result["hist"]

    return res_dict
