"""
main script for the plot showing the ratio of accretion luminosity vs zams luminosity

Plots:
- Ratio Lacc/Lzams for range of accretion rates:
- Ratio of Mdot_thermal/mdot accretion for range of accretion rates
- Ratio of Tacc/Tzams for range of accretion rates

TODO: function to generate the data and run the plotting routines, stitching them together
"""

import os

from RLOF.scripts.theory_plots.routines.ratio_accretion_luminosity_zams_luminosity.functions import (
    generate_data_and_create_plots,
)

this_file_dir = os.path.dirname(os.path.abspath(__file__))

output_dir = "output"
output_filename = "data.dat"
generate_plots = True
generate_results = False

generate_data_and_create_plots(
    output_dir,
    output_dir,
    "test.pdf",
    generate_plots=generate_plots,
    generate_results=generate_results,
)
