"""
Controller script to do automatic plotting of a big range of things
"""
import os

from grav_waves.gw_analysis.functions.general_plot_function import general_plot_function
from grav_waves.settings import convolution_settings

#
dataset_dict = {
    "rebuild": False,
    "sim_name": "MID_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-5_CORE_MASS_SHIFT",
    "add_ppisn_plots": False,
    "no_ppisn_companion_dataset": None,
}

#
plots_output_name = "tmp/test.pdf"
plots_output_dir = "tmp/plots"
population_data_result_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/MID_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-5_CORE_MASS_SHIFT/population_results"
os.makedirs(plots_output_dir, exist_ok=True)
convolution_settings = convolution_settings

general_plot_function(
    dataset_dict=dataset_dict,
    result_dir=population_data_result_dir,
    plot_dir=plots_output_dir,
    dco_type="bhbh",
    combined_pdf_output_name=plots_output_name,
    convolution_settings=convolution_settings,
    include_plot_MWEG_merger_rates=False,
    include_plot_RVOL_merger_rates=False,
    include_plot_all_chirpmasses_with_observations=False,
    include_plot_total_masses=False,
    include_plot_any_mass_plot=False,
    include_plot_combined_massratio_triangle=False,
    include_plot_timescales_plot=False,
    include_plot_combined_merger_rate_per_mergertime_total_mass=False,
    include_plot_combined_merger_rate_per_mergertime_chirpmass=False,
    include_plot_merger_rate_by_metallicity_by_merger_time=False,
    include_marchant_plots=False,
    include_stevenson_plots=False,
    include_plot_merger_rate_by_metallicity_by_mass_quantity=True,
    include_plot_sn_rates=False,
    include_plot_neijssel19_figure_1=False,
    include_plot_neijsel_fig1_channels_lieke=False,
)
