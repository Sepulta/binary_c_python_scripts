"""
Script to run the CEMP grid for Rob
"""

import os
import json
import time
import pickle
import sys
import shutil

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population


def parse_function(self, output):
    """
    Parse function for binary compact objects
    """

    pass


## Make population and set value
test_pop = Population()
test_pop.set()

# Set values
test_pop.set(
    # Grid configuration
    max_evolution_time=15000,  # Myr
    combine_ensemble_with_thread_joining=True,
    minimum_timestep=1e-6,
    log_runtime_systems=1,
    amt_cores=20,  # number of threads,
    verbosity=0,
    # General physics
    no_thermohaline_mixing=0,
    CRAP_parameter=0,
    Bondi_Hoyle_accretion_factor=1.5,
    WRLOF_method=0,
    lambda_min=0.8,
    delta_mcmin=-0.1,
    minimum_envelope_mass_for_third_dredgeup=0.0,
    # Ensemble
    ensemble=1,
    ensemble_defer=1,
    ensemble_filters_off=1,
    ensemble_filter_STELLAR_TYPE_COUNTS=1,
    ensemble_filter_HRD=1,
    ensemble_filter_EMP=1,
    ensemble_dt=1e3,  # 1Gyr resolution
    CEMP_cfe_minimum=0.7,
    EMP_logg_maximum=4.0,
    EMP_minimum_age=10e3,
)

# Resolutions
resolutions = {"M": [400, 100, 0, 0], "logP": [100, 0, 0], "ecc": [0, 0, 0]}

resolutions = {"M": [200, 50, 0, 0], "logP": [50, 0, 0], "ecc": [0, 0, 0]}


### Dummy resolution. comment out if you want to do a serious run
resolutions = {"M": [10, 5, 0, 0], "logP": [5, 0, 0], "ecc": [0, 0, 0]}

test_pop.Moe_di_Stefano_2017(
    options={
        "normalize_multiplicities": "merge",
        "multiplicity": 2,
        "multiplicity_modulator": [0, 1, 0, 0],
        "resolutions": resolutions,
        "ranges": {
            "M": [
                0.4,
                6.0,
            ],
            "q": [
                0.1,
                1,
            ],
        },
    }
)

# Get version info of the binary_c build
version_info = test_pop.return_binary_c_version_info(parsed=True)

# This will
# Stop the script if the configuration is wrong
if version_info["macros"]["MINT"] == "on":
    print("Please disable MINT")
    quit()
# if version_info['macros']['NUCSYN']=='off':
#     print("Please enables NUCSYN")
#     quit()
# if version_info['macros']['NUCSYN_ALL_ISOTOPES']=='on':
#     print("Please disable NUCSYN_ALL_ISOTOPES")
#     quit()

metallicity_values = [0.0001]

for metallicity in metallicity_values:
    test_pop.set(
        metallicity=metallicity,
        parse_function=parse_function,
        data_dir="results/CEMP_script/",  # set the result directory to be local to this directory
    )

    # create local tmp_dir
    test_pop.set(
        tmp_dir=os.path.join(test_pop.custom_options["data_dir"], "local_tmp_dir")
    )

    # Create tmp dir
    os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

    # Export settings:
    test_pop.export_all_info(use_datadir=True)

    # Evolve grid
    test_pop.evolve()

    # Get ensemble output and write it to file
    ensemble_output = test_pop.grid_ensemble_results

    with open(
        os.path.join(test_pop.custom_options["data_dir"], "ensemble_output.json"), "w"
    ) as f:
        json.dump(ensemble_output, f, indent=4)
