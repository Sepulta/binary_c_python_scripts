"""
Parse function
"""

import os


def parse_function(self, output):
    """
    ouptut function to find the PPISN information
    """

    # Get some information from the object
    data_dir = self.custom_options["data_dir"]
    base_filename = self.custom_options["base_filename"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # set the separator
    seperator = " "

    # Create filename
    outfilename = os.path.join(data_dir, base_filename)

    # params
    params = [
        "time",
        "mass",
        "pre_sn_mass",
        "zams_mass",
        "sn_type",
        "stellar_type",
        "pre_sn_stellar_type",
        "probability",
        "pre_sn_core_mass",
        "pre_sn_co_core_mass",
        "pre_sn_he_core_mass",
        "fallback_fraction",
        "fallback_mass",
        "metallicity",
        "effective_metallicity",
    ]

    if not os.path.exists(outfilename):
        with open(outfilename, "w") as f:
            f.write(seperator.join(params) + "\n")

    # Go over output
    if output:
        for line in output.splitlines():
            if line.startswith("DAVID_SN"):
                values = line.split()[1:]
                # Check for lenght
                if not len(values) == len(params):
                    raise ValueError(
                        "Length of values and expected parameters arent the same"
                    )

                with open(outfilename, "a") as f:
                    f.write(seperator.join(values) + "\n")


def parse_function_ppisn_general(self, output):
    """
    ouptut function to find the PPISN information
    """

    # Get some information from the object
    data_dir = self.custom_options["data_dir"]
    base_filename = self.custom_options["base_filename"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # set the separator
    seperator = " "

    # Create filename
    outfilename = os.path.join(data_dir, base_filename)

    # params
    params = [
        "time",
        "mass",
        "pre_sn_mass",
        "zams_mass",
        "sn_type",
        "stellar_type",
        "pre_sn_stellar_type",
        "probability",
        "pre_sn_core_mass",
        "pre_sn_co_core_mass",
        "pre_sn_he_core_mass",
        "fallback_fraction",
        "fallback_mass",
    ]

    if not os.path.exists(outfilename):
        with open(outfilename, "w") as f:
            f.write(seperator.join(params) + "\n")

    # Go over output
    if output:
        for line in output.splitlines():
            if line.startswith("DAVID_SN"):
                values = line.split()[1:]

                # Check for lenght
                if not len(values) == len(params):
                    raise ValueError(
                        "Length of values and expected parameters arent the same"
                    )

                with open(outfilename, "a") as f:
                    f.write(seperator.join(values) + "\n")


def parse_function_ppisn_edge_general(self, output):
    """
    ouptut function to find the PPISN information
    """

    # params
    params = [
        "time",
        "mass",
        "pre_sn_mass",
        "zams_mass",
        "sn_type",
        "stellar_type",
        "pre_sn_stellar_type",
        "probability",
        "pre_sn_core_mass",
        "pre_sn_co_core_mass",
        "pre_sn_he_core_mass",
        "fallback_fraction",
        "fallback_mass",
    ]

    if output:
        for line in output.splitlines():
            if line.startswith("DAVID_SN"):
                values = line.split()[1:]

                output_dict = {params[i]: float(values[i]) for i in range(len(params))}

                return output_dict


def parse_function_ppisn_edge(self, output):
    """
    ouptut function to find the ppisn edge.

    Will return True if the supernova type is below PPISN, will return False if SN type is above PPISN
    """

    # Go over output
    if output:
        for line in output.splitlines():
            if line.startswith("DAVID_SN"):
                values = line.split()[1:]
                sn_type = int(values[4])
                if sn_type < 21:
                    return True
                else:
                    return False
    else:
        return True  # Mimicking
