"""
Function to plot all the episode plots
"""

import os

from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict
from RLOF.paper_scripts.functions.episode_plot_functions.fraction_overspin_excess.plot_fraction_overspin_excess import (
    plot_fraction_overspin_excess,
)
from RLOF.paper_scripts.functions.episode_plot_functions.fraction_stream_excess.plot_fraction_stream_excess import (
    plot_fraction_stream_excess,
)
from RLOF.paper_scripts.functions.episode_plot_functions.fraction_time_spent_transferring_disk.plot_fraction_time_spent_transferring_disk import (
    plot_fraction_time_spent_transferring_disk,
)
from RLOF.paper_scripts.functions.episode_plot_functions.fraction_mass_transferred_disk.plot_fraction_mass_transferred_disk import (
    plot_fraction_mass_transferred_disk,
)
from RLOF.paper_scripts.functions.episode_plot_functions.mass_weighted_synchronicity_factor.plot_mass_weighted_synchronicity_factor import (
    plot_mass_weighted_synchronicity_factor,
)
from RLOF.paper_scripts.functions.episode_plot_functions.fraction_stream_torque_orbit.plot_fraction_stream_torque_orbit import (
    plot_fraction_stream_torque_orbit,
)


def plot_all_episode_plots(result_root, paper_root):
    """
    Function to plot all the episode plots
    """

    simulation_dir = os.path.join(result_root, "MID_RES_SANA/")
    target_dir = os.path.join(paper_root, "figures/episode_plots")

    rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(simulation_dir)
    dataset_dict = rlof_episode_dataset_dict["datasets"]

    ###########
    # FRACTION MASS TRANSFERRED THROUGH DISK
    basename_plot_fraction_mass_transferred_disk = "fraction_mass_transferred_disk.pdf"
    plot_fraction_mass_transferred_disk(
        dataset_dict,
        add_cdf=True,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                target_dir, basename_plot_fraction_mass_transferred_disk
            ),
        },
    )

    ###########
    # FRACTION TIME SPENT TRANSFERRING DISK
    basename_plot_fraction_time_spent_transferring_disk = (
        "fraction_time_spent_transferring_disk.pdf"
    )
    plot_fraction_time_spent_transferring_disk(
        dataset_dict,
        add_cdf=True,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                target_dir, basename_plot_fraction_time_spent_transferring_disk
            ),
        },
    )

    ###########
    # FRACTION STREAM EXCESS
    basename_plot_fraction_stream_excess = "fraction_stream_excess.pdf"
    plot_fraction_stream_excess(
        dataset_dict,
        add_cdf=True,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                target_dir, basename_plot_fraction_stream_excess
            ),
        },
    )

    ###########
    # FRACTION OVERSPIN EXCESS
    basename_plot_fraction_overspin_excess = "fraction_stellar_overspin_excess.pdf"
    plot_fraction_overspin_excess(
        dataset_dict,
        add_cdf=True,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                target_dir, basename_plot_fraction_overspin_excess
            ),
        },
    )

    ###########
    # MASS WEIGHTED SYNCHRONICITY FACTOR
    basename_plot_mass_weighted_synchronicity_factor = (
        "mass_weighted_synchronicity_factor.pdf"
    )
    plot_mass_weighted_synchronicity_factor(
        dataset_dict,
        add_cdf=True,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                target_dir, basename_plot_mass_weighted_synchronicity_factor
            ),
        },
    )

    ###########
    # FRACTION TORQUE STREAM ORBIT
    basename_plot_fraction_stream_torque_orbit = "fraction_stream_torque_orbit.pdf"
    plot_fraction_stream_torque_orbit(
        dataset_dict,
        add_cdf=True,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                target_dir, basename_plot_fraction_stream_torque_orbit
            ),
        },
    )


if __name__ == "__main__":
    result_root = "/home/david/projects/binary_c_root/results/RLOF/server_results/"
    paper_root = "/home/david/Dropbox/Academic/PHD/papers/paper_disks/paper_tex/"
    plot_all_episode_plots(result_root, paper_root)
