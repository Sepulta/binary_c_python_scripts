"""
General plotting function for the RLOF episode plots
"""

import os
from PyPDF2 import PdfFileMerger
from david_phd_functions.plotting.pdf_functions import add_pdf_and_bookmark

#
from RLOF.scripts.plot_episode_output.routines.plot_fraction_time_spent_disk_initially_stable_mt import (
    plot_fraction_time_spent_disk_initially_stable_mt,
)
from RLOF.scripts.plot_episode_output.routines.plot_fraction_mass_transferred_disk_initially_stable_mt_onto_ms import (
    plot_fraction_mass_transferred_disk_initially_stable_mt_onto_ms,
)
from RLOF.scripts.plot_episode_output.routines.plot_fraction_stream_excess_disk_angmom_initial_angmom_stable_mt_onto_ms import (
    plot_fraction_stream_excess_disk_angmom_initial_angmom_stable_mt_onto_ms,
)
from RLOF.scripts.plot_episode_output.routines.plot_fraction_overspin_excess_disk_angmom_initial_angmom_stable_mt_onto_ms import (
    plot_fraction_overspin_excess_disk_angmom_initial_angmom_stable_mt_onto_ms,
)


def general_plot_routine(
    rlof_episode_dataset_dict, output_dir, combined_output_pdf, add_cdf=True, verbose=0
):
    """
    General plotting function for the RLOF episode plots
    """

    # Make output dir if necessary:
    output_pdfs_dir = os.path.join(output_dir, "pdf")

    os.makedirs(output_dir, exist_ok=True)
    os.makedirs(output_pdfs_dir, exist_ok=True)

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    #
    print("Generating RLOF episode plots")

    ########
    # Plot the fraction of time spent during disk MT
    plot_fraction_time_spent_disk_initially_stable_mt_filename = os.path.join(
        output_pdfs_dir, "plot_fraction_time_spent_disk_initially_stable_mt.pdf"
    )
    plot_fraction_time_spent_disk_initially_stable_mt(
        rlof_episode_dataset_dict["datasets"],
        add_cdf=add_cdf,
        plot_settings={
            "show_plot": False,
            "runname": "plot_fraction_time_spent_disk_initially_stable_mt",
            "output_name": plot_fraction_time_spent_disk_initially_stable_mt_filename,
            "simulation_name": rlof_episode_dataset_dict["simname"],
        },
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        plot_fraction_time_spent_disk_initially_stable_mt_filename,
        pdf_page_number,
        bookmark_text="fraction time spent disk MT",
    )

    ########
    # Plot the fraction of mass transferred through disk
    plot_fraction_mass_transferred_disk_initially_stable_mt_onto_ms_filename = (
        os.path.join(
            output_pdfs_dir,
            "plot_fraction_mass_transferred_disk_initially_stable_mt_onto_ms.pdf",
        )
    )
    plot_fraction_mass_transferred_disk_initially_stable_mt_onto_ms(
        rlof_episode_dataset_dict["datasets"],
        add_cdf=add_cdf,
        plot_settings={
            "show_plot": False,
            "runname": "plot_fraction_mass_transferred_disk_initially_stable_mt_onto_ms",
            "output_name": plot_fraction_mass_transferred_disk_initially_stable_mt_onto_ms_filename,
            "simulation_name": rlof_episode_dataset_dict["simname"],
        },
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        plot_fraction_mass_transferred_disk_initially_stable_mt_onto_ms_filename,
        pdf_page_number,
        bookmark_text="fraction mass transferred disk MT",
    )

    ########
    # Plot the fraction of initial angular momentum transferred back to orbit due to stream excess
    plot_fraction_stream_excess_disk_angmom_initial_angmom_stable_mt_onto_ms_filename = os.path.join(
        output_pdfs_dir,
        "plot_fraction_stream_excess_disk_angmom_initial_angmom_stable_mt_onto_ms.pdf",
    )
    plot_fraction_stream_excess_disk_angmom_initial_angmom_stable_mt_onto_ms(
        rlof_episode_dataset_dict["datasets"],
        add_cdf=add_cdf,
        plot_settings={
            "show_plot": False,
            "runname": "plot_fraction_stream_excess_disk_angmom_initial_angmom_stable_mt_onto_ms",
            "output_name": plot_fraction_stream_excess_disk_angmom_initial_angmom_stable_mt_onto_ms_filename,
            "simulation_name": rlof_episode_dataset_dict["simname"],
        },
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        plot_fraction_stream_excess_disk_angmom_initial_angmom_stable_mt_onto_ms_filename,
        pdf_page_number,
        bookmark_text="fraction initial angmom transferred back to orbit due to stream excess",
    )

    ########
    # Plot the fraction of initial angular momentum transferred back to orbit due to stream excess
    plot_fraction_overspin_excess_disk_angmom_initial_angmom_stable_mt_onto_ms_filename = os.path.join(
        output_pdfs_dir,
        "plot_fraction_overspin_excess_disk_angmom_initial_angmom_stable_mt_onto_ms.pdf",
    )
    plot_fraction_overspin_excess_disk_angmom_initial_angmom_stable_mt_onto_ms(
        rlof_episode_dataset_dict["datasets"],
        add_cdf=add_cdf,
        plot_settings={
            "show_plot": False,
            "runname": "plot_fraction_overspin_excess_disk_angmom_initial_angmom_stable_mt_onto_ms",
            "output_name": plot_fraction_overspin_excess_disk_angmom_initial_angmom_stable_mt_onto_ms_filename,
            "simulation_name": rlof_episode_dataset_dict["simname"],
        },
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger,
        plot_fraction_overspin_excess_disk_angmom_initial_angmom_stable_mt_onto_ms_filename,
        pdf_page_number,
        bookmark_text="fraction initial angmom transferred back to orbit due to overspin excess",
    )

    #
    print("Creating combined pdf for theory plots")

    # wrap up the pdf
    merger.write(combined_output_pdf)
    merger.close()
    print("\t wrote combined pdf to {}".format(combined_output_pdf))

    print("Finished generating plots")
