"""
Function to plot the pre-SN core mass vs post-SN eccentricity

https://arxiv.org/pdf/1810.13412.pdf fig 11 sort-off
TODO: fix plot for merging systems at Z=0
"""

import os
import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

from grav_waves.functions.events import merge_with_event_dataframe, print_rows_and_cols

custom_mpl_settings.load_mpl_rc()


this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def get_vmin(hist, custom_min):
    """
    Function to get the minimum value of a histogram OR the custom min if thats
    """

    hist_min = np.min(hist[hist > 0])

    return np.max([custom_min, hist_min])


def example_all_ppisn_pre_post_eccentricity(
    total_SN_datafile, testing=False, plot_settings={}
):
    """
    Example function to combine the DCO data with supernova events and
    """

    # Set bins
    x_bins = np.linspace(0, 70, 35)
    y_bins = np.linspace(-4, 0, 15)

    #
    metallicity_vs_mass_hists = []

    # use chunks!
    chunksize = 10**6
    for chunk_i, chunk in enumerate(
        pd.read_csv(
            total_SN_datafile,
            chunksize=chunksize,
            usecols=[
                "pre_SN_ecc",
                "pre_SN_CO_core_mass",
                "SN_type",
                "post_SN_ecc",
                "SN_counter",
                "probability",
                "post_SN_mass",
                "metallicity",
            ],
            sep="\s+",
        )
    ):
        print("Handling chunk {}".format(chunk_i))

        # Select PPISN and those that are bound
        ppisn_SN = chunk.query("SN_type == 21")
        # bound_PPISN_SN = ppisn_SN.query("pre_SN_ecc >= 0")

        # Get all PPISN SN values
        chunked_all_hist, _, _ = np.histogram2d(
            ppisn_SN["post_SN_mass"],
            np.log10(ppisn_SN["metallicity"]),
            bins=[x_bins, y_bins],
            weights=ppisn_SN["probability"],
        )
        metallicity_vs_mass_hists.append(chunked_all_hist)

        if testing:
            if chunk_i > 3:
                break

    ##########
    # Combine the histograms
    combined_metallicity_vs_mass_hist = np.zeros(metallicity_vs_mass_hists[0].shape)

    for _, metallicity_vs_mass_hist in enumerate(metallicity_vs_mass_hists):
        combined_metallicity_vs_mass_hist += metallicity_vs_mass_hist

    ######
    # Set norm
    second_max_val = sorted(combined_metallicity_vs_mass_hist.flatten())[-2]
    norm = colors.LogNorm(
        vmin=get_vmin(
            combined_metallicity_vs_mass_hist, plot_settings.get("min_prob_val", 1e-8)
        ),
        vmax=second_max_val,
    )

    ##########
    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=4)

    metallicity_vs_mass_hist_axis = fig.add_subplot(gs[:, :-1])

    ax_cb = fig.add_subplot(gs[:, -1:])

    # make bincenters
    x_bincenters = (x_bins[1:] + x_bins[:-1]) / 2
    y_bincenters = (y_bins[1:] + y_bins[:-1]) / 2

    X, Y = np.meshgrid(x_bincenters, y_bincenters)

    # All SN
    _ = metallicity_vs_mass_hist_axis.pcolormesh(
        X,
        Y,
        combined_metallicity_vs_mass_hist.T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(ax_cb, norm=norm, extend="both")
    cbar.ax.set_ylabel(r"Number per formed solar mass")

    ###
    # Make-up
    metallicity_vs_mass_hist_axis.set_xlabel("Remnant mass")
    metallicity_vs_mass_hist_axis.set_ylabel("Metallicity")

    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    ########
    # Configure

    # Settings
    min_prob_val = 1e-12
    show_plot = False
    testing = False

    # Result root
    result_root = "/home/david/data_projects/binary_c_data/GRAV_WAVES/server_results/"

    # Plot output dir
    output_dir = os.path.join(
        this_file_dir,
        "plots/EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED",
    )

    #
    # total_sn_datafile = os.path.join(result_root, "EVENTS_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/combined_total_SN_events.dat")
    total_sn_datafile = os.path.join(
        result_root,
        "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/combined_total_SN_BINARY_events.dat",
    )

    #
    basename = "metallicity_PPISN_transients.pdf"
    example_all_ppisn_pre_post_eccentricity(
        total_sn_datafile,
        testing=testing,
        plot_settings={
            "min_prob_val": min_prob_val,
            "show_plot": show_plot,
            "output_name": os.path.join(output_dir, basename),
        },
    )
