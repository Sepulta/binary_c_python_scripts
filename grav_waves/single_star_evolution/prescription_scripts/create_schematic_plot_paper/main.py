"""
Function to plot the mass range for the fiducial and for the extra mass removed
"""

import os

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.generate_schematic_plots_ppisn_prescription import (
    generate_schematic_plots_ppisn_prescription,
)

##############
# Configure plotting and result generation
generate_results = False
generate_plots = True

#############
physics_settings = {
    "wind_mass_loss_setting": 2,
    "ppisn_prescription": 2,
    "metallicity": 1e-3,
    "mass_removal_values": [0, 5],
    "CO_core_shift_values": [0, -2],
    "mass_removal_multiplier_values": [1, 2],
    "resolution": {"M_1": 600},
}

#####################
result_dir = os.path.abspath("results/schematic_overview_plots/")
plot_dir = os.path.abspath("plots/schematic_overview_plots/")

#
generate_schematic_plots_ppisn_prescription(
    result_dir=result_dir,
    plot_dir=plot_dir,
    physics_settings=physics_settings,
    combined_pdf_output="test.pdf",
    generate_results=generate_results,
    generate_plots=generate_plots,
)
