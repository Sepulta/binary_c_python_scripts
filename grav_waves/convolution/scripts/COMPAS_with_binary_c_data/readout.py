"""
Function to readout the results of the compas dataset
"""

import h5py

filename = "results/COMPAS_Output_wWeights.h5"

h5file = h5py.File(filename)


print(h5file.keys())

print(h5file["DoubleCompactObjects"]["Metallicity@ZAMS(1)"])

# print(len(h5file['DoubleCompactObjects']['Metallicity@ZAMS(1)'][()]))
