import h5py
import pandas as pd
import matplotlib.pyplot as plt


def load_file_return_df(filename, dataset_name):
    # Load files
    # file_name = '/home/david/projects/binary_c_root/results/testing_python/supernova_mass_test/SN_types_z0.002.hdf5'
    f = h5py.File(filename, "r")

    # Show some keys
    print("Top level keys: {}".format(list(f.keys())))
    print("Data keys: {}".format(list(f["data"].keys())))
    # print("Settings keys: {}".format(f['settings']['used_settings'][()]))

    # Load data and put in dataframe
    data = f["data"][dataset_name][()]
    # settings = f['settings']['used_settings'][()]
    # settings_json = json.loads(settings)

    headers = f["data"][dataset_name + "_header"][()]
    headers = [header.decode("utf-8") for header in headers]

    # metallicity = json.loads(settings)['binary_c_defaults']['metallicity']

    # Make df
    df = pd.DataFrame(data)
    df.columns = headers

    df = df.sort_values(by=["zams_mass_1"], ascending=True)

    return df


def load_file_return_settings(filename, individiual_setting=None):
    f = h5py.File(filename, "r")

    settings_dict = json.loads(f["settings"]["used_settings"][()])
    if individiual_setting:
        return settings_dict[individiual_setting]
    else:
        return settings_dict


def plot_initial_final_mass_dependency(df_002, df_0002, df_0001, df_00002):
    # Z=0.02
    plt.plot(df_002["zams_mass_1"], df_002["mass_1"], label="Remnant mass  (Z=0.02)")
    plt.plot(
        df_002["zams_mass_1"],
        df_002["previous_mass_1"],
        linestyle="--",
        alpha=0.5,
        label="pre-SN mass (Z=0.02)",
    )
    # plt.plot(df_002['zams_mass_1'], df_002['previous_core_mass_1'], linestyle='-.', alpha=0.5)
    # plt.plot(df_002['zams_mass_1'], df_002['prev_co_core_mass_1'], linestyle='-.', alpha=0.5, label='co core mass (Z=0.02)')
    plt.plot(
        df_002["zams_mass_1"],
        df_002["metal_core_mass_1"],
        linestyle="-.",
        alpha=0.5,
        label="co core mass (Z=0.02)",
    )

    # Z=0.002
    # plt.plot(df_0002['zams_mass_1'],
    #     df_0002['mass_1'],
    #     label='Remnant mass  (Z=0.002)')
    # plt.plot(df_0002['zams_mass_1'],
    #     df_0002['previous_mass_1'],
    #     linestyle='--', alpha=0.5,
    #     label='pre-SN mass  (Z=0.002)')
    # #plt.plot(df_0002['zams_mass_1'], df_0002['previous_core_mass_1'], linestyle='-.', alpha=0.5)
    # plt.plot(df_0002['zams_mass_1'], df_0002['prev_co_core_mass_1'], linestyle='-.', alpha=0.5, label='co core mass (Z=0.002)')
    # plt.plot(df_001['zams_mass_1'],
    #     df_001['metal_core_mass_1'],
    #     linestyle='-.',
    #     alpha=0.5,
    #     label='co core mass (Z=0.01)')

    # Z=0.001
    plt.plot(df_0001["zams_mass_1"], df_0001["mass_1"], label="Remnant mass  (Z=0.001)")
    plt.plot(
        df_0001["zams_mass_1"],
        df_0001["previous_mass_1"],
        linestyle="--",
        alpha=0.5,
        label="pre-SN mass  (Z=0.001)",
    )
    # #plt.plot(df_0001['zams_mass_1'], df_0001['previous_core_mass_1'], linestyle='-.', alpha=0.5)
    plt.plot(
        df_0001["zams_mass_1"],
        df_0001["metal_core_mass_1"],
        linestyle="-.",
        alpha=0.5,
        label="co core mass (Z=0.001)",
    )

    # Z=0.0002
    plt.plot(
        df_00002["zams_mass_1"], df_00002["mass_1"], label="Remnant mass  (Z=0.0002)"
    )
    plt.plot(
        df_00002["zams_mass_1"],
        df_00002["previous_mass_1"],
        linestyle="--",
        alpha=0.5,
        label="pre-SN mass  (Z=0.0002)",
    )
    # plt.plot(df_00002['zams_mass_1'], df_00002['previous_core_mass_1'], linestyle='-.', alpha=0.5)
    plt.plot(
        df_00002["zams_mass_1"],
        df_00002["metal_core_mass_1"],
        linestyle="-.",
        alpha=0.5,
        label="co core mass (Z=0.0002)",
    )

    plt.axhline(y=38)
    plt.legend()
    plt.xlabel(r"Initial mass [M$_{\odot}$]")
    plt.ylabel(r"Mass [M$_{\odot}$]")
    plt.show()


def add_detailed_final_to_canvas(fig, ax, df, extra_info):
    ax.plot(
        df["zams_mass_1"],
        df["mass_1"],
        label="Remnant mass {}".format(extra_info.get("metallicity", "")),
    )
    ax.plot(df["zams_mass_1"], df["mass_1"], "bo", alpha=0.5)
    ax.plot(
        df["zams_mass_1"],
        df["previous_mass_1"],
        linestyle="--",
        alpha=0.5,
        label="pre-SN mass (Z=0.02)",
    )
    ax.plot(
        df["zams_mass_1"],
        df["previous_core_mass_1"],
        linestyle="-.",
        alpha=0.5,
        label="prev core mass {}".format(extra_info.get("metallicity", "")),
    )
    ax.plot(
        df["zams_mass_1"],
        df["prev_co_core_mass_1"],
        linestyle=(0, (1, 10)),
        alpha=0.5,
        label="prev co core mass {}".format(extra_info.get("metallicity", "")),
    )
    ax.plot(
        df["zams_mass_1"],
        df["prev_he_core_mass_1"],
        "go",
        # linestyle='go',
        alpha=0.5,
        label="prev he core mass {}".format(extra_info.get("metallicity", "")),
    )

    return fig, ax


def add_pre_sn_stellar_type(fig, ax, df, extra_info):
    ax.plot(
        df["zams_mass_1"],
        df["prev_stellar_type_1"],
        label="Pre-SN stellar type {}".format(extra_info.get("metallicity", "")),
    )

    ax.plot(
        df["zams_mass_1"],
        df["stellar_type_1"],
        label="Stellar type {}".format(extra_info.get("metallicity", "")),
    )

    return fig, ax


def add_initial_final_to_canvas(fig, ax, df, extra_info):
    # Z=0.02
    ax.plot(
        df["zams_mass_1"],
        df["mass_1"],
        label="Remnant mass {}".format(extra_info.get("metallicity", "")),
    )
    ax.plot(df["zams_mass_1"], df["mass_1"], "bo", alpha=0.5)

    # plt.plot(df_002['zams_mass_1'],
    #     df_002['previous_mass_1'],
    #     linestyle='--', alpha=0.5,
    #     label='pre-SN mass (Z=0.02)')
    # #plt.plot(df_002['zams_mass_1'], df_002['previous_core_mass_1'], linestyle='-.', alpha=0.5)
    # # plt.plot(df_002['zams_mass_1'], df_002['prev_co_core_mass_1'], linestyle='-.', alpha=0.5, label='co core mass (Z=0.02)')
    # plt.plot(df_002['zams_mass_1'],
    #     df_002['metal_core_mass_1'],
    #     linestyle='-.',
    #     alpha=0.5,
    #     label='co core mass (Z=0.02)')

    return fig, ax


def plot_all(dfs):

    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(16, 16))

    fig, axes = add_initial_final_to_canvas(fig, axes, dfs[0], {"metallicity": "0.02"})
    fig, axes = add_initial_final_to_canvas(fig, axes, dfs[1], {"metallicity": "0.002"})

    axes.set_ylim(0, 200)

    plt.legend()
    plt.xlabel(r"Initial mass [M$_{\odot}$]")
    plt.ylabel(r"Mass [M$_{\odot}$]")
    plt.show()
