import h5py
import json
import pandas as pd
import numpy as np
import os

import matplotlib.pyplot as plt
from david_phd_functions.plotting import custom_mpl_settings

from binarycpython.utils.stellar_types import STELLAR_TYPE_DICT_SHORT

from readout_functions import *

custom_mpl_settings.load_mpl_rc()

"""
This script serves to generate the plots and output for the single star population.
"""

# Set the metallicities
Z002 = 0.02
Z0002 = 0.002
Z0001 = 0.001
Z00002 = 0.0002

# Select the filenames
df_002_filename = os.path.join(
    os.environ["BINARYC_DATA_ROOT"],
    "PPISN",
    "PPISN_TESTS_SINGLE",
    "PPISN_z{}".format(Z002),
    "PPISN_z{}.hdf5".format(Z002),
)
df_0002_filename = os.path.join(
    os.environ["BINARYC_DATA_ROOT"],
    "PPISN",
    "PPISN_TESTS_SINGLE",
    "PPISN_z{}".format(Z0002),
    "PPISN_z{}.hdf5".format(Z0002),
)
df_0001_filename = os.path.join(
    os.environ["BINARYC_DATA_ROOT"],
    "PPISN",
    "PPISN_TESTS_SINGLE",
    "PPISN_z{}".format(Z0001),
    "PPISN_z{}.hdf5".format(Z0001),
)
df_00002_filename = os.path.join(
    os.environ["BINARYC_DATA_ROOT"],
    "PPISN",
    "PPISN_TESTS_SINGLE",
    "PPISN_z{}".format(Z00002),
    "PPISN_z{}.hdf5".format(Z00002),
)

# Load the dataframes
df_002 = load_file_return_df(df_002_filename, "PPISN_z0.02")
df_0002 = load_file_return_df(df_0002_filename, "PPISN_z0.002")
df_0001 = load_file_return_df(df_0001_filename, "PPISN_z0.001")
df_00002 = load_file_return_df(df_00002_filename, "PPISN_z0.0002")

# plot_all([df_002, df_0002])


def plot_detailed(df, metallicity):
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(16, 16), sharex=True)

    fig, axes[0] = add_detailed_final_to_canvas(
        fig, axes[0], df, {"metallicity": metallicity}
    )

    fig, axes[1] = add_pre_sn_stellar_type(
        fig, axes[1], df, {"metallicity": metallicity}
    )

    plt.legend()
    axes[1].set_xlabel(r"Initial mass [M$_{\odot}$]")
    axes[0].set_ylabel(r"Mass [M$_{\odot}$]")

    axes[0].set_title(
        "Remnant and pre-Supernova mass properties of single stars at {}".format(
            metallicity
        )
    )

    plt.show()


plot_detailed(df_002, "Z = 0.02")


# plot_initial_final_mass_dependency(df_002, df_0002, df_0001, df_00002)
quit()
###################################
def max_min_masses():
    print(max(df_002["mass_1"]))


max_min_masses()
print(
    load_file_return_settings(
        df_002_filename, individiual_setting="population_settings"
    )
)
