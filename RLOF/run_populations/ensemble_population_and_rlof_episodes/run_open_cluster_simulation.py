"""
-*- mode: python-mode; python-black-on-save-mode: nil -*-

Script to run the RLOF simulations and output the ensemble information within a specific time window.

We run two different distribution types:
- normal distribution
- moe & distefano distribution

With the cluster-specifics settings we can control what
"""

import os

from RLOF.run_populations.ensemble_population_and_rlof_episodes.run_all import run_all

#############################################
# settings for the script
run_populations                                     = True
generate_plots                                      = False
backup_if_data_exists                               = True
root_result_dir                                     = os.path.join(os.environ["BINARYC_DATA_ROOT"], "RLOF")
RLOF_episode_logging                                = False

# Cluster specifics
open_cluster_simulation_clustername                 = "M35"
open_cluster_simulation_age_myr_lower_bound         = 100
open_cluster_simulation_age_myr_upper_bound         = 125
open_cluster_simulation_mode                        = 1
open_cluster_metallicity                            = 0.02
open_cluster_ensemble_log10_orbital_period_binsize  = 0.05
open_cluster_ensemble_orbital_eccentricity_binsize  = 0.05

#
MS_population_range = {
    # stellar masses (Msun)
    "M": [
        0.8,  # maximum brown dwarf mass == minimum stellar mass
        1.4,  # (rather arbitrary) upper mass cutoff
    ],
    "q": [
        None,  # artificial qmin : set to None to use default
        None,  # artificial qmax : set to None to use default
    ],
    "logP": [0.352, 3.05],  # 0 = log10(1 day)  # 8 = log10(10^8 days)
    "ecc": [0.06, 0.999],
}

# ###########
# # high res settings
# num_cores                                     = 48
# resolution                                    = {'M_1': 15, 'q': 15, 'per': 75, "e": 75}
# simname_base                                  = "HIGH_RES_OPEN_CLUSTER_{}".format(open_cluster_simulation_clustername}

# ###########
# # mid res settings
# num_cores                                     = 20
# resolution                                    = {'M_1': 15, 'q': 15, 'per': 40, "e": 40}
# simname_base                                  = "MID_RES_OPEN_CLUSTER_{}".format(open_cluster_simulation_clustername}

###########
# low res settings
num_cores                                       = 8
resolution                                      = {"M_1": 10, "q": 10, "per": 40, "e": 40}
simname_base                                    = "LOW_RES_OPEN_CLUSTER_{}".format(open_cluster_simulation_clustername)

# ###########
# # test res settings
# num_cores                                       = 4
# resolution                                      = {"M_1": 5, "q": 5, "per": 5, "e": 5}
# simname_base                                    = "TEST_RES_OPEN_CLUSTER_{}".format(open_cluster_simulation_clustername)

##############
# Settings for the population object
local_population_settings = {
    "ensemble":                                         1,
    "ensemble_defer":                                   1,
    "ensemble_filters_off":                             1,
    "ensemble_filter_RLOF":                             0,
    "combine_ensemble_with_thread_joining":             True,
    "david_rlof_episode_logging":                       0,
    "david_rlof_system_ensemble_logging":               0,
    "david_ensemble_ballistic_exploration_logging":     0,
    "david_open_cluster_system_ensemble_logging":       1,
    "CHE_enabled":                                      0,
    "CHE_determination_prescription":                   "CHE_DETERMINATION_PRESCRIPTION_MANDEL2016_1",
    "CHE_enable_ensemble_logging":                      0,
    "num_cores":                                        num_cores,
    "multiplicity":                                     2,
    "open_cluster_simulation_age_myr_lower_bound":      open_cluster_simulation_age_myr_lower_bound,
    "open_cluster_simulation_age_myr_upper_bound":      open_cluster_simulation_age_myr_upper_bound,
    "open_cluster_simulation_mode":                     open_cluster_simulation_mode,
    "open_cluster_ensemble_log10_orbital_period_binsize": open_cluster_ensemble_log10_orbital_period_binsize,
    "open_cluster_ensemble_orbital_eccentricity_binsize": open_cluster_ensemble_orbital_eccentricity_binsize,
    "max_evolution_time":                               open_cluster_simulation_age_myr_upper_bound + 1
}

##############
# Arguments for the run_all function
run_all_extra_arguments = {
    "run_populations":                                  run_populations,
    "backup_if_data_exists":                            backup_if_data_exists,
    "generate_plots":                                   generate_plots,
    "resolution":                                       resolution,
    "root_result_dir":                                  root_result_dir,
    "simname_base":                                     simname_base,
    "metallicity_values":                               [open_cluster_metallicity],
    "local_population_settings":                        local_population_settings,
    "MS_population_range":                              MS_population_range,
    "RLOF_episode_logging":                             RLOF_episode_logging,
}

# fiducial_settings
variation_dict_fiducial = {
    "distributions":                                    "ms",  # Choose 'normal' or 'ms'
}

#####
# Fiducial run
variation_dict = variation_dict_fiducial
run_all(variation_dict=variation_dict, **run_all_extra_arguments)
