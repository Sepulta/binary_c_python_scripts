import os
import json
import time
import pickle
import sys
import shutil

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import (
    get_help_all,
    get_help,
    create_hdf5,
    output_lines,
)

# Personal defaults
from david_phd_functions.binaryc.personal_defaults import personal_defaults

from logging_strings import single_logging_string

"""
Script to generate ranges of stars that go supernova.

Single star systems. With a mass range, linearly, from 1 to 300.
Tracking the point at which they go supernova and

I use the PPISN algorithm of Rob & Mathieus paper

Requires for now the PPISN branch in binary_c

Requires the NUCSYN to be turned off for now

TODO: Compare other PPISN routines
TODO: Add pulses and yields
"""


def parse_function(self, output):
    """
    Parse function for single stars which catches the lines that start with DAVID_PPISN_SINGLE

    The output gets written to the data_dir + base_filename.
    """

    # Get filenames
    data_dir = self.custom_options["data_dir"]
    base_filename = self.custom_options["base_filename"]
    outfilename = os.path.join(data_dir, base_filename)

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # The parameternames contained in the output line
    parameters = [
        "time",
        "mass_1",
        "zams_mass_1",
        "stellar_type_1",
        "prev_stellar_type_1",
        "metallicity",
        "probability",
        "previous_mass_1",
        "previous_core_mass_1",
        "prev_he_core_mass_1",
        "prev_co_core_mass_1",
        "SN_type",
    ]

    # Set separator
    separator = "\t"

    # Parse output
    for line in output_lines(output):
        headerline = line.split()[0]
        if headerline == "DAVID_PPISN_SINGLE":
            values = line.split()[1:]

            # Check if the file actually exists, if not: make it and put the parameter names as a first line
            if not os.path.exists(outfilename):
                with open(outfilename, "w") as f:
                    f.write(separator.join(parameters) + "\n")

            # write output to file
            with open(outfilename, "a") as f:
                f.write(separator.join(values) + "\n")


######################################################################
# Set up population and set values
ppisn_single_pop = Population()
ppisn_single_pop.set(verbose=1)

ppisn_single_pop.set(C_logging_code=single_logging_string)

# Set grid variables
resolution = {"M_1": 300}

# Masses in logspace
# ppisn_single_pop.add_grid_variable(
#     name="lnm1",
#     longname="Primary mass",
#     valuerange=[1, 300],
#     resolution="{}".format(resolution["M_1"]),
#     spacingfunc="const(math.log(1), math.log(300), {})".format(resolution["M_1"]),
#     precode="M_1=math.exp(lnm1)",
#     probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 300, -1.3, -2.3, -2.3)*M_1",
#     dphasevol="dlnm1",
#     parameter_name="M_1",
#     condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
# )

# # TODO:
# # Metallicities from an array
# ppisn_single_pop.add_grid_variable(
#     name="metallicity",
#     longname="Metallicity",
#     valuerange=[1, 150],
#     resolution="{}".format(resolution['M_1']),
#     spacingfunc="const(1, 150, {})".format(resolution['M_1']),
#     probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 150, -1.3, -2.3, -2.3)",
#     # probdist="Kroupa2001(M_1)",
#     dphasevol="dm1",
#     parameter_name="metallicity",
# )

# Masses in linspace
ppisn_single_pop.add_grid_variable(
    name="M_1",
    longname="Primary mass",
    valuerange=[1, 300],
    resolution="{}".format(resolution["M_1"]),
    spacingfunc="const(1, 300, {})".format(resolution["M_1"]),
    probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 300, -1.3, -2.3, -2.3)",
    # probdist="Kroupa2001(M_1)",
    dphasevol="dm1",
    parameter_name="M_1",
)

# Load personal defaults in it
ppisn_single_pop.set(**personal_defaults)

# Set some extra values.
ppisn_single_pop.set(
    # david_logging_function=10,
    binary=0,
    separation=10000000000000000000,  # to make sure they're regarded as separate stars
    orbital_period=10000000000000000000,
    parse_function=parse_function,  # pass the parsing function to the grid to use
    amt_cores=2,  # Use 2 cores to do this
    M_2=0.08,  # Put the other star as a very low mass
    BH_prescription=3,
)

######################################################################
# # single evol to check things
# ppisn_single_pop.set(M_1=150,
#     metallicity=0.00001
#     )
# out = ppisn_single_pop.evolve_single()
# print(out)

# Loop over a set of different metallicities.
metallicity_values = [0.02, 0.002, 0.001, 0.0002]
for metallicity in metallicity_values:
    ppisn_single_pop.set(
        metallicity=metallicity,
        data_dir=os.path.join(
            os.environ["BINARYC_DATA_ROOT"],
            "PPISN",
            "PPISN_TESTS_SINGLE",
            "PPISN_z{}".format(metallicity),
        ),
        base_filename="PPISN_z{}.dat".format(metallicity),
    )

    # Clean the datadir first: i.e. remove the directory (so we dont append to the files and create new hdf5files)
    if os.path.isdir(ppisn_single_pop.custom_options["data_dir"]):
        shutil.rmtree(ppisn_single_pop.custom_options["data_dir"])

    # Export settings:
    ppisn_single_pop.export_all_info(use_datadir=True)

    # Evolve grid
    ppisn_single_pop.evolve_population()

    # Create the hdf5 file which takes the settings that we just exported, and the datafiles
    create_hdf5(
        ppisn_single_pop.custom_options["data_dir"],
        name="PPISN_z{}.hdf5".format(metallicity),
    )
