"""
Function to generate the table containing the fractions of mass weighted stable MT onto MS via disks in different regions of critical mass transfer rate
    normalized by all stable MT onto MS via disks
"""

import os
import json
import pandas as pd

from ensemble_functions.ensemble_functions import (
    get_recursive_sum,
    merge_all_subdicts,
    query_dict,
)
from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)
from grav_waves.settings import convolution_settings


def generate_table_fraction_mt_regions_stable_ms_disk(
    dataset,
    output_filename,
    testing,
    caption="",
    label="",
    global_query_dict=None,
    verbose=0,
):
    """
    Function to generate the table containing the fractions of mass weighted stable MT onto MS via disks in different regions of critical mass transfer rate
        normalized by all stable MT onto MS via disks
    """

    if verbose:
        print(
            "Running generate_table_fraction_mt_regions_stable_ms_disk for Z = {}\n\toutput: {}".format(
                dataset["metallicity"], os.path.abspath(output_filename)
            )
        )

        if global_query_dict:
            print(
                "\twith global query: {}".format(global_query_dict["global_query_name"])
            )
            print(
                "\t\tqueries: {}".format(
                    ", ".join(
                        [
                            "{} between {}".format(
                                query["parameter_key"], query["value_range"]
                            )
                            for query in global_query_dict["query_list"]
                        ]
                    )
                )
            )

    #
    result_dict = {}

    # Get the correct ensemble data from the dict
    if verbose:
        print("\t\tLoading data")
    probability_to_yield_conversion_factor = (
        convolution_settings["binary_fraction"] * convolution_settings["mass_in_stars"]
    )
    ensemble_data = get_ensemble_data(
        dataset,
        testing,
        probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
    )["RLOF"]["fraction_separation_massratio"]["mass"]

    if global_query_dict:
        if verbose:
            print("\tPerforming queries")
        for query in global_query_dict["query_list"]:
            if verbose:
                print(
                    "\t\tPerforming query {}".format(
                        "{} between {}".format(
                            query["parameter_key"], query["value_range"]
                        )
                    )
                )
            ensemble_data = query_dict(
                ensemble_data, query["parameter_key"], query["value_range"]
            )

    # # Get all stable mass ensemble
    if verbose:
        print("\t\tSelecting stable mass transfer")
    mass_stable_ensemble = query_dict(ensemble_data, "stable", [-0.5, 0.5])

    # Select onto MS
    if verbose:
        print("\t\tSelecting mass transfer on MS data")
    ms_mass_stable_ensemble = query_dict(mass_stable_ensemble, "st_acc", [0.5, 1.5])

    # select via disk
    if verbose:
        print("\t\tSelecting MT on disk")
    ms_disk_mass_stable_ensemble = query_dict(
        ms_mass_stable_ensemble, "disk", [0.5, 1.5]
    )
    total_ms_disk_mass_stable_ensemble = get_recursive_sum(ms_disk_mass_stable_ensemble)

    # select MT regions:
    if verbose:
        print("\t\tSelecting MT categories")
    category_0_ms_disk_mass_stable_ensemble = query_dict(
        ms_disk_mass_stable_ensemble, "mt_cat", [-0.5, 0.5]
    )
    category_1_ms_disk_mass_stable_ensemble = query_dict(
        ms_disk_mass_stable_ensemble, "mt_cat", [0.5, 1.5]
    )
    category_2_ms_disk_mass_stable_ensemble = query_dict(
        ms_disk_mass_stable_ensemble, "mt_cat", [1.5, 2.5]
    )

    # Get totals
    if verbose:
        print("\t\tCalculating totals")
    total_category_0_ms_disk_mass_stable_ensemble = get_recursive_sum(
        category_0_ms_disk_mass_stable_ensemble
    )
    total_category_1_ms_disk_mass_stable_ensemble = get_recursive_sum(
        category_1_ms_disk_mass_stable_ensemble
    )
    total_category_2_ms_disk_mass_stable_ensemble = get_recursive_sum(
        category_2_ms_disk_mass_stable_ensemble
    )

    if verbose:
        print("\t\tCalculating fractions")
    fraction_category_0_ms_disk_mass_stable_ensemble = (
        total_category_0_ms_disk_mass_stable_ensemble
        / total_ms_disk_mass_stable_ensemble
        if total_ms_disk_mass_stable_ensemble != 0
        else 0
    )
    fraction_category_1_ms_disk_mass_stable_ensemble = (
        total_category_1_ms_disk_mass_stable_ensemble
        / total_ms_disk_mass_stable_ensemble
        if total_ms_disk_mass_stable_ensemble != 0
        else 0
    )
    fraction_category_2_ms_disk_mass_stable_ensemble = (
        total_category_2_ms_disk_mass_stable_ensemble
        / total_ms_disk_mass_stable_ensemble
        if total_ms_disk_mass_stable_ensemble != 0
        else 0
    )

    # Store in dict
    if verbose:
        print("\t\tStoring results")
    result_dict["Z = {}".format(dataset["metallicity"])] = [
        fraction_category_0_ms_disk_mass_stable_ensemble,
        fraction_category_1_ms_disk_mass_stable_ensemble,
        fraction_category_2_ms_disk_mass_stable_ensemble,
    ]

    #
    column_names = ["Category I", "Category II", "Category III"]

    # put in dataframe:
    result_df = pd.DataFrame.from_dict(
        result_dict, columns=column_names, orient="index"
    )
    result_df = result_df.transpose()

    #
    caption = caption
    label = label

    # Create latex string
    latex_string = result_df.to_latex(
        index=True, label=label, caption=caption, float_format="%.2f"
    )
    # print(latex_string)

    # Export
    with open(output_filename, "w") as output_filehandle:
        output_filehandle.write(latex_string)


# population_datasets_sana = generate_ensemble_dataset_dict('/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions/')
# generate_table_fraction_mt_regions_stable_ms_disk(population_datasets_sana['0.02'], 'test.tex', testing=True, label='', caption='', verbose=1)
