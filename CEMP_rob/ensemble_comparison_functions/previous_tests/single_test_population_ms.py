import os
import json
import time
import pickle
import sys
import shutil

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import (
    get_help_all,
    get_help,
    create_hdf5,
    output_lines,
)

# from david_phd_functions.binaryc.personal_defaults import personal_defaults

"""
Script to run the CEMP grid for rob
"""


def parse_function(self, output):
    """
    Parse function for binary compact objects
    """

    pass


## Make population and set value
test_pop = Population()
test_pop.set()

# Set values
test_pop.set(
    # Grid configuration
    max_evolution_time=15000,  # Myr
    combine_ensemble_with_thread_joining=True,
    minimum_timestep=1e-6,
    amt_cores=20,  # number of threads,
    # amt_cores=20, # number of threads,
    verbosity=5,
    # General physics
    no_thermohaline_mixing=0,
    CRAP_parameter=0,
    Bondi_Hoyle_accretion_factor=1.5,
    minimum_envelope_mass_for_third_dredgeup=0.0,
    # Ensemble
    ensemble=1,
    ensemble_defer=1,
    ensemble_filters_off=1,
    ensemble_filter_STELLAR_TYPE_COUNTS=1,
    ensemble_filter_HRD=1,
    ensemble_filter_EMP=1,
    ensemble_dt=1e3,  # 1Gyr resolution
    CEMP_cfe_minimum=0.7,
    EMP_logg_maximum=4.0,
    EMP_minimum_age=10e3,
    ensemble_logdt=0.1,
    ensemble_logtimes=1,
    ensemble_startlogtime=0.1,
    gaia_colour_transform_method="GAIA_CONVERSION_UBVRI_RIELLO2020",
    ensemble_filter_TIDES=1,
    vrot1="VROT_BSE",
    vrot2="VROT_BSE",
    wind_mass_loss="WIND_ALGORITHM_BINARY_C_2020",
    gbwind="GB_WIND_REIMERS",
    gb_reimers_eta=0.5,  # Reimers (BSE = Hurley et al. 2002, 0.5)
    eagbwind="EAGB_WIND_BSE",
    tpagbwind="TPAGB_WIND_VW93_KARAKAS",
    superwind_mira_switchon=500.0,
    wr_wind="WR_WIND_BSE",
    wr_wind_fac=1.0,
    rotationally_enhanced_mass_loss="ROTATIONALLY_ENHANCED_MASSLOSS_ANGMOM",
    rotationally_enhanced_exponent=1.0,
    wind_angular_momentum_loss="WIND_ANGMOM_LOSS_BSE",
    wind_djorb_fac=1.0,  # multiplies Tout's Jorbdot
    lw=1.0,  # multiplies lw Jorbdot
    WRLOF_method="WRLOF_Q_DEPENDENT",
    qcrit_LMMS=0.6944,
    qcrit_MS=1.6,
    qcrit_HG=4.0,
    qcrit_GB="QCRIT_GB_BSE",
    qcrit_CHeB=3.0,
    qcrit_EAGB="QCRIT_GB_BSE",
    qcrit_TPAGB="QCRIT_GB_BSE",
    qcrit_HeMS=3.0,
    qcrit_HeHG=0.784,
    qcrit_HeGB=0.784,
    qcrit_HeWD=3.0,
    qcrit_COWD=3.0,
    qcrit_ONeWD=3.0,
    qcrit_NS=3.0,
    qcrit_BH=3.0,
    qcrit_degenerate_LMMS=1.0,
    qcrit_degenerate_MS=1.0,
    qcrit_degenerate_HG=4.7619,
    qcrit_degenerate_GB=1.15,
    qcrit_degenerate_CHeB=3.0,
    qcrit_degenerate_EAGB=1.15,
    qcrit_degenerate_TPAGB=1.15,
    qcrit_degenerate_HeMS=3.0,
    qcrit_degenerate_HeHG=4.7619,
    qcrit_degenerate_HeGB=1.15,
    qcrit_degenerate_HeWD=0.625,
    qcrit_degenerate_COWD=0.625,
    qcrit_degenerate_ONeWD=0.625,
    qcrit_degenerate_NS=0.625,
    qcrit_degenerate_BH=0.625,
    comenv_prescription="COMENV_BSE",
    alpha_ce=1.0,  # 1
    lambda_ce="LAMBDA_CE_DEWI_TAURIS",
    lambda_ionisation=0.1,  # 0.0
    comenv_merger_spin_method="COMENV_MERGER_SPIN_METHOD_BREAKUP",
    comenv_ejection_spin_method="COMENV_EJECTION_SPIN_METHOD_DO_NOTHING",
    comenv_post_eccentricity=1e-5,
    comenv_disc_mass_fraction=1e-2,
    comenv_disc_angmom_fraction=0.1,
    cbdisc_gamma=1.4,
    cbdisc_alpha=1e-3,
    cbdisc_kappa=1e-2,
    cbdisc_torquef=1e-3,
    cbdisc_init_dM=0.0,
    cbdisc_init_dJdM=0.0,
    cbdisc_minimum_evaporation_timescale=0.0,
    cbdisc_mass_loss_constant_rate=0.0,
    cbdisc_mass_loss_inner_viscous_multiplier=1.0,
    cbdisc_mass_loss_inner_viscous_angular_momentum_multiplier=1.0,
    cbdisc_mass_loss_inner_L2_cross_multiplier=1.0,
    cbdisc_mass_loss_ISM_ram_pressure_multiplier=1.0,
    cbdisc_mass_loss_ISM_pressure=3000,
    cbdisc_mass_loss_FUV_multiplier=1.0,
    cbdisc_mass_loss_Xray_multiplier=1.0,
    cbdisc_minimum_luminosity=0.0,
    cbdisc_minimum_mass=1e-6,
    cbdisc_minimum_fRing=0.2,
    cbdisc_no_wind_if_cbdisc="False",
    cbdisc_eccentricity_pumping_method="CBDISC_ECCENTRICITY_PUMPING_DERMINE",
    cbdisc_resonance_multiplier=1.0,
    cbdisc_resonance_damping="True",
    cbdisc_mass_loss_inner_viscous_accretion_method="CBDISC_MASS_LOSS_INNER_VISCOUS_ACCRETION_METHOD_YOUNG_CLARKE_2015",
    cbdisc_viscous_photoevaporative_coupling="True",
    cbdisc_viscous_L2_coupling="True",
    cbdisc_fail_ring_inside_separation="False",
    cbdisc_inner_edge_stripping="True",
    cbdisc_outer_edge_stripping="True",
    cbdisc_inner_edge_stripping_timescale="DISC_STRIPPING_TIMESCALE_INSTANT",
    cbdisc_outer_edge_stripping_timescale="DISC_STRIPPING_TIMESCALE_INSTANT",
    cbdisc_max_lifetime=1e6,
    disc_timestep_factor=1.0,
    disc_log="DISC_LOG_LEVEL_NONE",
    disc_log2d="DISC_LOG_LEVEL_NONE",
    disc_log_dt=0.0,
    disc_n_monte_carlo_guesses=0,
    RLOF_method="RLOF_METHOD_CLAEYS",
    RLOF_mdot_factor=1.0,
    RLOF_interpolation_method="RLOF_INTERPOLATION_BINARY_C",
    RLOF_angular_momentum_transfer_model="RLOF_ANGULAR_MOMENTUM_TRANSFER_MODEL_BSE",
    nonconservative_angmom_gamma="RLOF_NONCONSERVATIVE_GAMMA_DONOR",
    max_HeWD_mass=0.7,
    WDWD_merger_algorithm="WDWD_MERGER_ALGORITHM_PERETS2019",
    nova_retention_method="NOVA_RETENTION_ALGORITHM_CONSTANT",
    nova_retention_fraction=1e-3,
    hachisu_disk_wind="False",
    hachisu_qcrit="HACHISU_IGNORE_QCRIT",
    WD_accretion_rate_novae_upper_limit_hydrogen_donor="DONOR_RATE_ALGORITHM_BSE",
    WD_accretion_rate_novae_upper_limit_helium_donor="DONOR_RATE_ALGORITHM_BSE",
    WD_accretion_rate_novae_upper_limit_other_donor="DONOR_RATE_ALGORITHM_BSE",
    WD_accretion_rate_new_giant_envelope_lower_limit_hydrogen_donor="DONOR_RATE_ALGORITHM_BSE",
    WD_accretion_rate_new_giant_envelope_lower_limit_helium_donor="DONOR_RATE_ALGORITHM_BSE",
    WD_accretion_rate_new_giant_envelope_lower_limit_other_donor="DONOR_RATE_ALGORITHM_BSE",
    wd_sigma=0.0,
    wd_kick_when="WD_KICK_END_AGB",
    wd_kick_pulse_number=0,
    wd_kick_direction="KICK_RANDOM",
    donor_limit_thermal_multiplier=1.0,
    donor_limit_dynamical_multiplier=1.0,
    donor_limit_envelope_multiplier=1.0,
    accretion_limit_eddington_steady_multiplier=-1.0,  # 1.0
    accretion_limit_eddington_WD_to_remnant_multiplier=-1.0,  # -1.0
    accretion_limit_eddington_LMMS_multiplier=-1.0,  # 1.0
    accretion_limit_thermal_multiplier=10.0,
    accretion_limit_dynamical_multiplier=1.0,
    type_Ia_MCh_supernova_algorithm="TYPE_IA_MCH_SUPERNOVA_ALGORITHM_SEITENZAHL2013_AUTOMATIC",
    type_Ia_sub_MCh_supernova_algorithm="TYPE_IA_SUB_MCH_SUPERNOVA_ALGORITHM_LIVNE_ARNETT_1995",
    Seitenzahl2013_model="N100",
    mass_accretion_for_eld=-1.0,
    post_SN_orbit_method="POST_SN_ORBIT_TT98",
    sn_kick_distribution_IBC="KICK_VELOCITY_MAXWELLIAN",
    sn_kick_distribution_GRB_COLLAPSAR="KICK_VELOCITY_FIXED",
    sn_kick_distribution_II="KICK_VELOCITY_MAXWELLIAN",
    sn_kick_distribution_ECAP="KICK_VELOCITY_FIXED",
    sn_kick_distribution_NS_NS="KICK_VELOCITY_FIXED",
    sn_kick_distribution_TZ="KICK_VELOCITY_FIXED",
    sn_kick_distribution_AIC_BH="KICK_VELOCITY_FIXED",
    sn_kick_distribution_BH_BH="KICK_VELOCITY_FIXED",
    sn_kick_distribution_BH_NS="KICK_VELOCITY_FIXED",
    sn_kick_distribution_IA_Hybrid_HeCOWD_subluminous="KICK_VELOCITY_FIXED",
    sn_kick_distribution_IA_Hybrid_HeCOWD="KICK_VELOCITY_FIXED",
    sn_kick_dispersion_IBC=190.0,
    sn_kick_dispersion_GRB_COLLAPSAR=0.0,
    sn_kick_dispersion_II=190.0,
    sn_kick_dispersion_ECAP=0.0,
    sn_kick_dispersion_NS_NS=0.0,
    sn_kick_dispersion_TZ=0.0,
    sn_kick_dispersion_AIC_BH=0.0,
    sn_kick_dispersion_BH_BH=0.0,
    sn_kick_dispersion_BH_NS=0.0,
    sn_kick_dispersion_IA_Hybrid_HeCOWD=0.0,
    sn_kick_dispersion_IA_Hybrid_HeCOWD_subluminous=0.0,
    sn_kick_companion_IA_He="SN_IMPULSE_NONE",
    sn_kick_companion_IA_ELD="SN_IMPULSE_NONE",
    sn_kick_companion_IA_CHAND="SN_IMPULSE_LIU2015",
    sn_kick_companion_AIC="SN_IMPULSE_NONE",
    sn_kick_companion_ECAP="SN_IMPULSE_NONE",
    sn_kick_companion_IA_He_Coal="SN_IMPULSE_NONE",
    sn_kick_companion_IA_CHAND_Coal="SN_IMPULSE_NONE",
    sn_kick_companion_NS_NS="SN_IMPULSE_NONE",
    sn_kick_companion_GRB_COLLAPSAR="SN_IMPULSE_LIU2015",
    sn_kick_companion_HeStarIa="SN_IMPULSE_NONE",
    sn_kick_companion_IBC="SN_IMPULSE_LIU2015",
    sn_kick_companion_II="SN_IMPULSE_LIU2015",
    sn_kick_companion_IIa="SN_IMPULSE_NONE",
    sn_kick_companion_WDKICK="SN_IMPULSE_NONE",
    sn_kick_companion_TZ="SN_IMPULSE_NONE",
    sn_kick_companion_AIC_BH="SN_IMPULSE_NONE",
    sn_kick_companion_BH_BH="SN_IMPULSE_NONE",
    sn_kick_companion_BH_NS="SN_IMPULSE_NONE",
    sn_kick_companion_IA_Hybrid_HeCOWD="SN_IMPULSE_NONE",
    sn_kick_companion_IA_Hybrid_HeCOWD_subluminous="SN_IMPULSE_NONE",
    BH_prescription="BH_SPERA2015",
    evolution_splitting=0,
    evolution_splitting_sn_n=10,
    evolution_splitting_maxdepth=1,
    maximum_timestep=1e3,
    maximum_nuclear_burning_timestep=1e2,
    maximum_timestep_factor=0.0,
    dtfac=1.0,
    timestep_solver_factor=1.0,
    timestep_modulator=1.0,
    reverse_time="False",
    start_time=0.0,
    AGB_core_algorithm="AGB_CORE_ALGORITHM_KARAKAS",
    AGB_3dup_algorithm="AGB_THIRD_DREDGE_UP_ALGORITHM_KARAKAS",
    AGB_luminosity_algorithm="AGB_LUMINOSITY_ALGORITHM_KARAKAS",
    AGB_radius_algorithm="AGB_RADIUS_ALGORITHM_KARAKAS",
    solver="SOLVER_FORWARD_EULER",
    magnetic_braking_algorithm="MAGNETIC_BRAKING_ALGORITHM_ANDRONOV_2003",
    magnetic_braking_factor=1.0,
    magnetic_braking_gamma=3.0,
    gravitational_radiation_model="GRAVITATIONAL_RADIATION_BSE",
    gravitational_radiation_modulator_J=1.0,
    gravitational_radiation_modulator_e=1.0,
    small_envelope_method="SMALL_ENVELOPE_METHOD_BSE",
    initial_abundance_mix="NUCSYN_INIT_ABUND_MIX_AG89",
    third_dup="True",
    delta_mcmin=0.0,
    lambda_min=0.0,
    lambda_multiplier=1.0,
    hbbtfac=1.0,
    lithium_hbb_multiplier=10.0,
    lithium_GB_post_1DUP=1.0,
    lithium_GB_post_Heflash=4.0,
    c13_eff=1.0,
    mc13_pocket_multiplier=1.0,
    mass_of_pmz=0.0,
)

# Resolutions
resolutions = {"M": [400, 100, 0, 0], "logP": [100, 0, 0], "ecc": [0, 0, 0]}
resolutions = {"M": [1000, 2, 0, 0], "logP": [2, 0, 0], "ecc": [0, 0, 0]}

test_pop.Moe_de_Stefano_2017(
    options={
        "normalize_multiplicities": "merge",
        "multiplicity": 1,
        "multiplicity_modulator": [1, 0, 0, 0],
        "resolutions": resolutions,
        "ranges": {
            "M": [
                0.1,
                20,
            ],
        },
    }
)

# Get version info of the binary_c build
version_info = test_pop._return_binary_c_version_info(parsed=True)

# Stop the script if the configuration is wrong
# [if version_info['macros']['MINT']=='on':
#     print("Please disable MINT")
#     quit()
if version_info["macros"]["NUCSYN"] == "off":
    print("Please enables NUCSYN")
    quit()
# if version_info['macros']['NUCSYN_ALL_ISOTOPES']=='on':
#     print("Please disable NUCSYN_ALL_ISOTOPES")
#     quit()

metallicity_values = [0.02]

for metallicity in metallicity_values:
    test_pop.set(
        metallicity=metallicity,
        parse_function=parse_function,
        # data_dir=os.path.join(os.environ['BINARYC_DATA_ROOT'], 'TESTS', 'CEMP_ROB', 'MOE_DISTEFANO_ON', 'Z{}'.format(metallicity)),
        # data_dir=os.path.join(os.environ['BINARYC_DATA_ROOT'], 'CEMP_ROB', 'MOE_DISTEFANO_ON', 'Z{}'.format(metallicity)),
        data_dir="results/single_test_ms/",
    )

    # create local tmp_dir
    test_pop.set(
        tmp_dir=os.path.join(test_pop.custom_options["data_dir"], "local_tmp_dir")
    )

    if os.path.isdir(test_pop.grid_options["tmp_dir"]):
        shutil.rmtree(test_pop.grid_options["tmp_dir"])
    os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

    # Export settings:
    test_pop.export_all_info(use_datadir=True)

    # Evolve grid
    test_pop.evolve()

    # Get ensemble output and write it to file
    ensemble_output = test_pop.grid_ensemble_results

    with open(
        os.path.join(test_pop.custom_options["data_dir"], "ensemble_output.json"), "w"
    ) as f:
        json.dump(ensemble_output, f, indent=4)
