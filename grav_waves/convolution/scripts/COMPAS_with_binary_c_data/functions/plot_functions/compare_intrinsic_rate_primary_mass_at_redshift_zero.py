"""
Routine to compare the primary mass distribution for the two convolution methods
"""

import h5py

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors


import numpy as np

from david_phd_functions.plotting.utils import (
    add_plot_info,
    show_and_save_plot,
    align_axes,
)
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

#
from grav_waves.convolution.scripts.COMPAS_with_binary_c_data.functions.plot_functions.plot_mass_quantity_over_redshift import (
    get_data_plot_mass_quantity_over_redshift as get_data_compas,
)
from grav_waves.convolution.functions.plot_routines.plot_rate_density_at_zero_redshift import (
    get_data as get_data_custom,
)

from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    linestyle_list,
)
from grav_waves.convolution.functions.plot_routines.plot_kde_and_bootstrap_functions import (
    calculate_kde_values,
    run_bootstrap,
)

matplotlib.use("tkAgg")

color_list = ["blue"]


def get_data_mass_quantity_at_redshift_zero(compas_filename, mass_quantity):
    """
    Function to get the distribution of mass quantity at redshift 0
    """

    # Open file
    target_h5file = h5py.File(compas_filename, "r")

    intrinsic_merger_rate_per_system_per_redshift = target_h5file[
        "convolution_data/merger_rate"
    ][()]
    intrinsic_merger_rate_per_system_at_redshift_zero = (
        intrinsic_merger_rate_per_system_per_redshift[:, 0]
    )

    mask = target_h5file["convolution_settings/DCOmask"][()]

    # Read out mass
    mass_1 = target_h5file["BSE_Double_Compact_Objects/Mass(1)"][()][mask]
    mass_2 = target_h5file["BSE_Double_Compact_Objects/Mass(2)"][()][mask]

    secondary_mass = np.min([mass_1, mass_2], axis=0)
    chirpmass = np.power(mass_1 * mass_2, 3.0 / 5.0) / np.power(
        mass_1 + mass_2, 1.0 / 5.0
    )

    # Handle the choice of mass quantity
    if mass_quantity == "primary_mass":
        primary_mass = np.max([mass_1, mass_2], axis=0)
        mass_array = primary_mass
    if mass_quantity == "total_mass":
        total_mass = np.add(mass_1, mass_2)
        mass_array = total_mass

    return mass_array, intrinsic_merger_rate_per_system_at_redshift_zero


def compare_intrinsic_rate_primary_mass_at_redshift_zero(
    compas_filename_convolution,
    custom_filename_convolution,
    mass_quantity,
    bins,
    include_compas_plot=True,
    include_custom_plot=True,
    include_kde=True,
    bootstraps=10,
    num_procs=8,
    plot_settings={},
):
    """
    Function to calculate the total intrinsic rate over redshift
    """

    DIFF_MAX_THRESHOLD = 3

    #######
    # Set up figure
    fig = plt.figure(figsize=(30, 20))

    #
    gs = fig.add_gridspec(nrows=2, ncols=10)

    # Create axes
    ax_compas = fig.add_subplot(gs[0, 0:10])
    ax_custom = fig.add_subplot(gs[1, 0:10])

    #####################
    # COMPAS
    if include_compas_plot:
        #####################
        # Get compas data
        x_data, weights = get_data_mass_quantity_at_redshift_zero(
            compas_filename_convolution, "primary_mass"
        )

        #
        hist = np.histogram(x_data, bins=bins, weights=weights)[0]

        bincenters = (bins[1:] + bins[:-1]) / 2

        ########################
        # KDE
        if include_kde:
            non_zero_bins_indices = np.nonzero(hist)[0]
            bins = bins[non_zero_bins_indices.min() : non_zero_bins_indices.max() + 1]

            bin_size = np.diff(bins)

            x_KDE_width = 0.1
            kde_width = 0.1 * bin_size[0]

            x_KDE = np.arange(bins.min(), bins.max() + x_KDE_width, x_KDE_width)
            center_KDEbins = (x_KDE[:-1] + x_KDE[1:]) / 2.0

            # calculate_kde_values
            kde_y_vals = calculate_kde_values(
                data=x_data,
                weights=weights,
                lower_bound=x_data.min(),
                upper_bound=x_data.max(),
                bw_method=kde_width,
                center_KDEbins=center_KDEbins,
                hist=hist,
            )

        if bootstraps:
            # TODO: Ask lieke why do we use x_KDE here instead of center_KDEbins
            _, median, percentiles = run_bootstrap(
                masses=x_data,
                weights=weights,
                indices=np.indices(x_data.shape)[0],
                kde_width=kde_width,
                mass_bins=bins,
                center_KDEbins=center_KDEbins,
                num_procs=num_procs,
                bootstraps=bootstraps,
            )

        # plot the histogram
        ax_compas.plot(
            bincenters,
            hist,
            lw=5 if not bootstraps else 2,
            c=color_list[0],
            zorder=200,
            linestyle="--",
            alpha=0.1 if include_kde else 1,
        )

        # plot the KDE values
        if include_kde:
            ax_compas.plot(
                center_KDEbins,
                kde_y_vals,
                lw=5 if not bootstraps else 2,
                c=color_list[0],
                label="All",
                zorder=13,
            )

        if bootstraps:
            ax_compas.fill_between(
                center_KDEbins,
                percentiles[0],
                percentiles[1],
                alpha=0.4,
                zorder=11,
                # label='1-sigma',
                color=color_list[0],
            )  # 1-sigma
            ax_compas.fill_between(
                center_KDEbins,
                percentiles[2],
                percentiles[3],
                alpha=0.2,
                zorder=10,
                # label='2-sigma',
                color=color_list[0],
            )  # 2-sgima

        # Set ylims
        current_ylim = ax_compas.get_ylim()
        custom_min_val = 10 ** (np.log10(current_ylim[1]) - DIFF_MAX_THRESHOLD)
        ax_compas.set_ylim([custom_min_val, current_ylim[1]])

    #####################
    # CUSTOM
    if include_custom_plot:
        rate_type_hdf5_key = "merger_rate_data"

        #
        hdf5_file = h5py.File(custom_filename_convolution, "r")
        zero_key = str(
            min(
                [float(el) for el in list(hdf5_file["data"][rate_type_hdf5_key].keys())]
            )
        )
        hdf5_file.close()

        #####################
        # Get custom data
        results_dict = {}
        results_dict["all"] = get_data_custom(
            hdf5_filename=custom_filename_convolution,
            dco_type="bhbh",
            bins=bins,
            mass_quantity=mass_quantity,
            rate_key_in_df="merger_rates",
            rate_type_hdf5_key="merger_rate_data",
            divide_by_mass_bins=False,
            redshift_key=zero_key,
            num_procs=num_procs,
            bootstraps=bootstraps,
        )

        #########
        # Plot the histograms
        ax_custom.plot(
            results_dict["all"]["mass_centers"],
            results_dict["all"]["rates"],
            lw=5 if not bootstraps else 2,
            c=color_list[0],
            zorder=200,
            linestyle="--",
            alpha=0.1 if include_kde else 1,
        )

        # plot the KDE values
        if include_kde:
            ax_custom.plot(
                results_dict["all"]["center_KDEbins"],
                results_dict["all"]["kde_yvals"],
                lw=5 if not bootstraps else 2,
                c=color_list[0],
                label="All",
                zorder=13,
            )

        if bootstraps:
            ax_custom.fill_between(
                results_dict["all"]["center_KDEbins"],
                results_dict["all"]["percentiles"][0],
                results_dict["all"]["percentiles"][1],
                alpha=0.4,
                zorder=11,
                # label='1-sigma',
                color=color_list[0],
            )  # 1-sigma
            ax_custom.fill_between(
                results_dict["all"]["center_KDEbins"],
                results_dict["all"]["percentiles"][2],
                results_dict["all"]["percentiles"][3],
                alpha=0.2,
                zorder=10,
                # label='2-sigma',
                color=color_list[0],
            )  # 2-sgima

        # Set ylims
        current_ylim = ax_custom.get_ylim()
        custom_min_val = 10 ** (np.log10(current_ylim[1]) - DIFF_MAX_THRESHOLD)
        ax_custom.set_ylim([custom_min_val, current_ylim[1]])

    ##########
    # Make up

    # Align axes
    align_axes(fig=fig, axes_list=[ax_compas, ax_custom], which_axis="x")

    # Add labels
    ax_compas.set_ylabel(r"rate Intrinsic [dN/dGpc3/dYr]")
    ax_custom.set_ylabel(r"rate Intrinsic [dN/dGpc3/dYr]")

    ax_custom.set_xlabel("Redshift [z]")

    #
    ax_compas.set_title("COMPAS convolution")
    ax_custom.set_title("CUSTOM convolution")

    #
    ax_compas.set_yscale("log")
    ax_custom.set_yscale("log")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    ## Setting
    sim_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/"
    dco_type = "combined_dco"

    # compas convolution
    compas_dir_convolution = os.path.join(
        sim_dir, "convolution_results_compas_structure"
    )
    compas_filename_convolution = os.path.join(
        compas_dir_convolution, "convolved_{}_compas_structure.hdf5".format(dco_type)
    )

    # custom convolution
    custom_filename_convolution = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/without_detection_probability/rebinned_convolution_results.h5"

    #
    show_plots = False
    include_custom_plots = True

    #
    compare_intrinsic_rate_primary_mass_at_redshift_zero(
        compas_filename_convolution,
        custom_filename_convolution,
        mass_quantity="primary_mass",
        bins=np.arange(0, 70, 1),
        bootstraps=24,
        include_custom_plot=include_custom_plots,
        plot_settings={
            "show_plot": show_plots,
            "hspace": 0.5,
            "runname": "with probability weighting in compas",
        },
    )
else:
    matplotlib.use("Agg")
