(TeX-add-style-hook
 "example_doc"
 (lambda ()
   (TeX-run-style-hooks
    "latex2e"
    "example_diagram"
    "article"
    "art10"
    "tikz"
    "tikzscale"
    "tikz-qtree")
   (LaTeX-add-labels
    "fig:tree"))
 :latex)
