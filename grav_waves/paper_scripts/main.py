"""
Main function to run all the plotting scripts

TODO: collect the datasets at the top
"""

import os
import numpy as np

from grav_waves.paper_scripts.MSSFR_plot.generate_mssfr_plot import generate_mssfr_plot
from grav_waves.paper_scripts.total_intrinsics_rate_over_redshift.plot_total_intrinsic_rate_density_over_redshift import (
    plot_total_intrinsic_rate_density_over_redshift,
)

from grav_waves.paper_scripts.combined_dataframe_plots.yield_per_metallicity import (
    plot_yield_vs_metallicity,
)
from grav_waves.paper_scripts.combined_dataframe_plots.combined_dataframe_primary_mass_vs_metallicity import (
    plot_primary_mass_vs_metallicity,
)
from grav_waves.paper_scripts.combined_dataframe_plots.combined_dataframe_merger_time_vs_primary_mass import (
    plot_merger_time_vs_primary_mass,
)
from grav_waves.paper_scripts.combined_dataframe_plots.combined_dataframe_merger_time_vs_metallicity_plot import (
    plot_merger_time_vs_metallicity,
)

from grav_waves.paper_scripts.redshift_zero_plots.birth_redshift_vs_birth_metallicity_at_redshift_zero import (
    plot_birth_redshift_vs_birth_metallicity_at_redshift_zero,
)
from grav_waves.paper_scripts.redshift_zero_plots.primary_mass_vs_birth_redshift_at_redshift_zero import (
    plot_primary_mass_vs_birth_redshift_at_redshift_zero,
)
from grav_waves.paper_scripts.redshift_zero_plots.primary_mass_vs_metallicity_at_redshift_zero import (
    plot_primary_mass_vs_metallicity_at_redshift_zero,
)

from grav_waves.paper_scripts.primary_mass_distribution_at_redshift_with_variations.generate_primary_mass_at_redshift_zero_comparison_plots import (
    generate_primary_mass_at_redshift_zero_comparison_plot,
)


def run_all_plotting_routines(output_root):
    """
    Function to run all the plotting routines
    """

    fiducial_dataset = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5"

    ##########
    # Set up specific directories
    figures_root = os.path.join(output_root, "figures")
    tables_root = os.path.join(output_root, "tables")

    os.makedirs(figures_root, exist_ok=True)
    os.makedirs(tables_root, exist_ok=True)

    ################
    # mssfr plot
    base_name_mssfr_plot = "mssfr_plot.pdf"
    generate_mssfr_plot(
        dataset_filename=fiducial_dataset,
        scale="linear",
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(figures_root, "mssfr", base_name_mssfr_plot),
        },
    )

    #################
    # Total intrinsic rate density over redshift
    base_name_total_rate_plot = "total_intrinsic_rate_density_over_redshift.pdf"
    plot_total_intrinsic_rate_density_over_redshift(
        dataset_filename=fiducial_dataset,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                figures_root, "fiducial_over_redshift", base_name_total_rate_plot
            ),
        },
    )

    #################
    # Combined dataframe plots

    # Yield vs metallicity
    base_name_yield_vs_metallicity_combined_dataframe = "yield_vs_metallicity.pdf"
    plot_yield_vs_metallicity(
        dataset_filename=fiducial_dataset,
        scale="linear",
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                figures_root,
                "fiducial_combined_dataframe",
                base_name_yield_vs_metallicity_combined_dataframe,
            ),
        },
    )

    # primary mass vs metallicity
    base_name_primary_mass_vs_metallicity_combined_dataframe = (
        "primary_mass_vs_metallicity.pdf"
    )
    plot_primary_mass_vs_metallicity(
        dataset_filename=fiducial_dataset,
        scale="log",
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                figures_root,
                "fiducial_combined_dataframe",
                base_name_primary_mass_vs_metallicity_combined_dataframe,
            ),
        },
    )

    #
    base_name_merger_time_vs_primary_mass_combined_dataframe = (
        "merger_time_vs_primary_mass.pdf"
    )
    plot_merger_time_vs_primary_mass(
        dataset_filename=fiducial_dataset,
        scale="log",
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                figures_root,
                "fiducial_combined_dataframe",
                base_name_merger_time_vs_primary_mass_combined_dataframe,
            ),
        },
    )

    #
    base_name_merger_time_vs_metallicity_combined_dataframe = (
        "merger_time_vs_metallicity_plot.pdf"
    )
    plot_merger_time_vs_metallicity(
        dataset_filename=fiducial_dataset,
        scale="log",
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                figures_root,
                "fiducial_combined_dataframe",
                base_name_merger_time_vs_metallicity_combined_dataframe,
            ),
        },
    )

    #################
    # Redshift zero plots

    # birth redshift vs birth metallicity
    base_name_birth_redshift_vs_birth_metallicity = (
        "birth_redshift_vs_birth_metallicity_at_redshift_zero.pdf"
    )
    plot_birth_redshift_vs_birth_metallicity_at_redshift_zero(
        dataset_filename=fiducial_dataset,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                figures_root,
                "fiducial_at_redshift_zero",
                base_name_birth_redshift_vs_birth_metallicity,
            ),
        },
    )

    # Primary mass vs birth redshift
    base_name_primary_mass_vs_birth_redshift = (
        "primary_mass_vs_birth_redshift_at_redshift_zero.pdf"
    )
    plot_primary_mass_vs_birth_redshift_at_redshift_zero(
        dataset_filename=fiducial_dataset,
        primary_mass_bins=np.arange(0, 70, 1),
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                figures_root,
                "fiducial_at_redshift_zero",
                base_name_primary_mass_vs_birth_redshift,
            ),
        },
    )

    # Primary mass vs metallicity
    base_name_primary_mass_vs_birth_metallicitys = (
        "primary_mass_vs_metallicity_at_redshift_zero.pdf"
    )
    plot_primary_mass_vs_metallicity_at_redshift_zero(
        dataset_filename=fiducial_dataset,
        primary_mass_bins=np.arange(0, 70, 1),
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                figures_root,
                "fiducial_at_redshift_zero",
                base_name_primary_mass_vs_birth_metallicitys,
            ),
        },
    )

    #################
    # Primary mass distribution at redshift zero with variations

    # primary mass distribution with extra mass loss variation
    convolved_datasets = {
        "fiducial": "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/rebinned_convolution_results.h5",
        "ten_extra_massloss": "/home/david/projects/binary_c_root/results/GRAV_WAVES/convolution_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_10_EXTRA_MASSLOSS_PPISN/convolution_results/rebinned_convolution_results.h5",
        "twenty_extra_massloss": "/home/david/projects/binary_c_root/results/GRAV_WAVES/convolution_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_20_EXTRA_MASSLOSS_PPISN/convolution_results/rebinned_convolution_results.h5",
    }
    base_name_primary_mass_distribution_extra_massloss_variation = (
        "primary_mass_distribution_at_redshift_zero_for_extra_massloss_variations.pdf"
    )
    generate_primary_mass_at_redshift_zero_comparison_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=np.arange(0, 100, 2),
        add_rates_plots=True,
        add_ratio_to_fiducial_plot=False,
        add_residual_to_fiducial_plot=False,
        add_fraction_primary_underwent_ppisn_plot=True,
        add_kde=False,
        bootstraps=50,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                figures_root,
                "primary_mass_distribution_at_redshift_zero_with_variations",
                base_name_primary_mass_distribution_extra_massloss_variation,
            ),
            "hspace": 0.5,
        },
    )

    # primary mass distribution with core mass shift variation
    convolved_datasets = {
        "fiducial": "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/rebinned_convolution_results.h5",
        "five_core_mass_shift": "/home/david/projects/binary_c_root/results/GRAV_WAVES/convolution_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-5_CORE_MASS_SHIFT/convolution_results/rebinned_convolution_results.h5",
        "ten_core_mass_shift": "/home/david/projects/binary_c_root/results/GRAV_WAVES/convolution_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-10_CORE_MASS_SHIFT/convolution_results/rebinned_convolution_results.h5",
    }
    base_name_primary_mass_distribution_core_mass_shift_variation = (
        "primary_mass_distribution_at_redshift_zero_for_core_mass_shift_variations.pdf"
    )

    #
    generate_primary_mass_at_redshift_zero_comparison_plot(
        convolved_datasets=convolved_datasets,
        primary_mass_bins=np.arange(0, 100, 2),
        add_rates_plots=True,
        add_ratio_to_fiducial_plot=False,
        add_residual_to_fiducial_plot=False,
        add_fraction_primary_underwent_ppisn_plot=True,
        add_kde=False,
        bootstraps=50,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                figures_root,
                "primary_mass_distribution_at_redshift_zero_with_variations",
                base_name_primary_mass_distribution_core_mass_shift_variation,
            ),
            "hspace": 0.5,
        },
    )


if __name__ == "__main__":

    output_root = "figures_and_tables"
    output_root = "/home/david/papers/paper_gw/paper_tex"

    run_all_plotting_routines(output_root)
