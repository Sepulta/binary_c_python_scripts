"""
Functions for a generalized multiprocessing queue handling.

What you need to supply:
- function that handles the 'job'
- list of job dicts.
- amount cores

Returns:
- 'job' with result attached to it
"""

import time
import multiprocessing


def general_multiprocessing_queue_filler(
    job_queue, amount_cores, job_list, verbosity=0
):
    """
    Functions that fills the queue
    """

    # Loop over the time values
    for job_dict in enumerate(job_list):

        # Put job in queue
        job_queue.put(job_dict)

    # Send stopping signals to the processes
    for _ in range(amount_cores):
        job_queue.put("STOP")


def general_multiprocessing_queue_handler(
    job_queue, result_queue, ID, job_handling_function, verbosity=0
):
    """
    Function that picks values from the queue and runs the given job handling function
    """

    # Go over the queue
    for job_index, job_dict in iter(job_queue.get, "STOP"):

        # Run the job handling function
        result = job_handling_function(job_dict, job_index, verbosity=verbosity)

        # Put the result in the dict
        job_dict["result"] = result

        # Put the updated job dict back in the result queue
        result_queue.put(job_dict)

    # Return to break out the loop
    return


def general_multiprocessing_queue(
    amount_cores, job_handling_function, job_list, maxsize=None, verbosity=0
):
    """
    Function that sets up a multiprocessing queue and runs it and then returns the list of results

    We need an amount of cores. Then we need to pass the job handling function and the list of jobs.
    """

    # Check value for maxsize:
    if not maxsize:
        maxsize = len(job_list)

    # Set up the manager object that can share info between processes
    manager = multiprocessing.Manager()
    job_queue = manager.Queue(maxsize=maxsize)
    result_queue = manager.Queue(maxsize=maxsize)

    # Create process instances
    processes = []
    for ID in range(amount_cores):
        processes.append(
            multiprocessing.Process(
                target=general_multiprocessing_queue_handler,
                args=(job_queue, result_queue, ID, job_handling_function, verbosity),
            )
        )

    #
    start_time = time.time()

    # Activate the processes
    for p in processes:
        p.start()

    # Set up the system_queue
    general_multiprocessing_queue_filler(
        job_queue, amount_cores, job_list, verbosity=verbosity
    )

    # Join the processes, which also forces them to finish. Everything after this is after the system queue is empty
    for p in processes:
        p.join()

    #
    stop_time = time.time()

    # Set up output list
    output_list = []

    # Loop over the result queue
    for output_dict in iter(result_queue.get, object()):
        output_list.append(output_dict)

        # Break out when the queue is empty
        if result_queue.empty():
            break

    return output_list
