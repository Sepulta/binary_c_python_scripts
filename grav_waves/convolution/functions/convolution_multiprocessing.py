"""
Functionality for the multiprocessing of the convolution

TODO: writing the pickle files to the hdf5 file could be done in parallel with the generating of the rate calculations.
"""

import os
import time
import shutil
import pickle
import multiprocessing

import h5py
import numpy as np
import setproctitle

from grav_waves.convolution.functions.convolution_calculate_birth_redshift_array import (
    calculate_birth_redshift_array,
)
from grav_waves.convolution.functions.convolution_calculate_detection_probability import (
    calculate_detection_probability_array,
)
from grav_waves.convolution.functions.convolution_calculate_rate_array import (
    calculate_rate_array,
)


def convolution_job_worker(job_queue, worker_ID, convolution_configuration):
    """
    Function that handles running the job
    """

    setproctitle.setproctitle(
        "PPISN multiprocessing worker process {}".format(worker_ID)
    )

    # Get items from the job_queue
    for job_dict in iter(job_queue.get, "STOP"):
        #########
        # Stopping or working
        if job_dict == "STOP":
            return None

        #
        time_value = job_dict["time_value"]

        ##########
        # Set up output dict
        output_dict = {}

        ##########
        # Calculate the birth rates
        if convolution_configuration["include_birth_rates"]:
            convolution_configuration["logger"].debug(
                "Worker {}: redshift: {}: Calculating birth rates".format(
                    worker_ID, time_value
                )
            )

            # The 'birth' redshifts are the current redshifts
            birth_rate_birth_redshift = (
                np.ones(job_dict["combined_data_array"]["mass_1"].shape) * time_value
            )

            #
            birth_rate_array = calculate_rate_array(
                metallicity_weighted_starformation_array=job_dict[
                    "metallicity_weighted_starformation_array"
                ],
                convolution_configuration=convolution_configuration,
                combined_data_array=job_dict["combined_data_array"],
                birth_redshift_array=birth_rate_birth_redshift,
            )

            # Create result dictionary
            birth_rate_dictionary = {
                "time_value": time_value,
                "rate_type": "birth_rate",
                "rate_array": birth_rate_array,
            }

            #
            output_dict["birth_rate_data"] = birth_rate_dictionary

        ##########
        # Calculate the formation rates
        if convolution_configuration["include_formation_rates"]:
            convolution_configuration["logger"].debug(
                "Worker {}: redshift: {}: Calculating formation rates".format(
                    worker_ID, time_value
                )
            )

            # The 'birth' redshifts are the current redshifts - formation time
            formation_rate_birth_redshift = calculate_birth_redshift_array(
                time_values_in_years_key="formation_time_values_in_years",
                convolution_configuration=convolution_configuration,
                redshift_value=time_value,
                combined_data_array=job_dict["combined_data_array"],
            )

            #
            formation_rate_array = calculate_rate_array(
                metallicity_weighted_starformation_array=job_dict[
                    "metallicity_weighted_starformation_array"
                ],
                convolution_configuration=convolution_configuration,
                combined_data_array=job_dict["combined_data_array"],
                birth_redshift_array=formation_rate_birth_redshift,
            )

            # Create result dictionary
            formation_rate_dictionary = {
                "time_value": time_value,
                "rate_type": "formation_rate",
                "rate_array": formation_rate_array,
            }

            #
            output_dict["formation_rate_data"] = formation_rate_dictionary

        ##########
        # Calculate the merger rates
        if convolution_configuration["include_merger_rates"]:
            convolution_configuration["logger"].debug(
                "Worker {}: redshift: {}: Calculating merger rates".format(
                    worker_ID, time_value
                )
            )

            # The 'birth' redshifts are the current redshifts - merger time
            merger_rate_birth_redshift = calculate_birth_redshift_array(
                time_values_in_years_key="merger_time_values_in_years",
                convolution_configuration=convolution_configuration,
                redshift_value=time_value,
                combined_data_array=job_dict["combined_data_array"],
            )

            #
            merger_rate_array = calculate_rate_array(
                metallicity_weighted_starformation_array=job_dict[
                    "metallicity_weighted_starformation_array"
                ],
                convolution_configuration=convolution_configuration,
                combined_data_array=job_dict["combined_data_array"],
                birth_redshift_array=merger_rate_birth_redshift,
            )

            # Create result dictionary
            merger_rate_dictionary = {
                "time_value": time_value,
                "rate_type": "merger_rate",
                "rate_array": merger_rate_array,
            }

            #
            output_dict["merger_rate_data"] = merger_rate_dictionary

            ##########
            # Calculate the detection rates
            if convolution_configuration["include_detection_rates"]:
                convolution_configuration["logger"].debug(
                    "Worker {}: redshift: {}: Calculating detection rates".format(
                        worker_ID, time_value
                    )
                )

                # Only store this if the redshift is below max detector redshift
                if (
                    time_value
                    < job_dict["convolution_configuration"]["max_detection_redshift"]
                ):
                    detection_probability_array = calculate_detection_probability_array(
                        redshift_value=time_value,
                        combined_data_array=job_dict["combined_data_array"],
                        convolution_configuration=convolution_configuration,
                    )

                    #
                    detection_rate_array = (
                        merger_rate_array * detection_probability_array
                    )

                    # Create result dictionary
                    detection_rate_dictionary = {
                        "time_value": time_value,
                        "rate_type": "detection_rate",
                        "rate_array": detection_rate_array,
                    }

                    #
                    output_dict["detection_rate_data"] = detection_rate_dictionary

        ##########
        # Calculate the custom rates: hook for custom rate calculation.
        # NOTE: requires custom_rate_function to be passed
        #   which should accept: output_dict, combined_data_array, convolution_configuration.
        #   should return: output_dict
        if convolution_configuration["include_custom_rates"]:
            # TODO: Make sure the function accepts the correct arguments, perhaps with inspect? well, it'll fail otherwise so i guess its fine.
            output_dict = convolution_configuration["custom_rates_function"](
                output_dict=output_dict,
                combined_data_array=job_dict["combined_data_array"],
                convolution_configuration=convolution_configuration,
            )

        #
        with open(
            os.path.join(job_dict["output_dir"], "{}.p".format(time_value)), "wb"
        ) as f:
            pickle.dump(output_dict, f)


def convolution_queue_filler(
    job_queue,
    num_cores,
    convolution_configuration,
    metallicity_weighted_starformation_array,
    combined_data_array,
):
    """
    Function to handle filling the queue for the multiprocessing
    """

    # Continuously fill the queue
    for time_value_i, time_value in enumerate(
        convolution_configuration["time_centers"]
    ):
        # Check if we're in the part that we want to calculate
        if (
            not convolution_configuration["min_loop_time"]
            <= time_value
            <= convolution_configuration["max_loop_time"]
        ):
            continue

        # Set up job dict
        job_dict = {
            "time_value": time_value,
            "metallicity_weighted_starformation_array": metallicity_weighted_starformation_array,
            "time_value_i": time_value_i,
            "output_dir": os.path.join(
                convolution_configuration["convolution_tmp_dir"]
            ),
            "combined_data_array": combined_data_array,
        }

        # Put job in queue
        job_queue.put(job_dict)

    # Signal stop to workers
    for _ in range(num_cores):
        job_queue.put("STOP")


def convolution_multiprocessing(
    convolution_configuration,
    output_filename,
    metallicity_weighted_starformation_array,
    combined_data_array,
):
    """
    Main process to handle the multiprocessing
    """

    ###################
    # check directory to write the pickle files to
    if convolution_configuration["convolution_tmp_dir"] is None:
        raise ValueError(
            "Please set the convolution_tmp_dir to do the convolution with multiprocessing"
        )
    tmp_output_dir = os.path.join(convolution_configuration["convolution_tmp_dir"])
    if os.path.isdir(tmp_output_dir):
        shutil.rmtree(tmp_output_dir)
    os.makedirs(tmp_output_dir, exist_ok=True)

    ###################
    # Run the convolution through multiprocessing
    convolution_configuration["logger"].info(
        "Running the convolution. Generating pickle files"
    )
    start = time.time()

    # Set process name
    setproctitle.setproctitle("Convolution parent process")

    # Set up the manager object that can share info between processes
    manager = multiprocessing.Manager()
    job_queue = manager.Queue(convolution_configuration["max_job_queue_size"])

    # Create process instances
    processes = []
    for worker_ID in range(convolution_configuration["num_cores"]):
        processes.append(
            multiprocessing.Process(
                target=convolution_job_worker,
                args=(job_queue, worker_ID, convolution_configuration),
            )
        )

    # Activate the processes
    for p in processes:
        p.start()

    # Set up the system_queue
    convolution_queue_filler(
        job_queue=job_queue,
        num_cores=convolution_configuration["num_cores"],
        convolution_configuration=convolution_configuration,
        metallicity_weighted_starformation_array=metallicity_weighted_starformation_array,
        combined_data_array=combined_data_array,
    )

    # Join the processes
    for p in processes:
        p.join()
    convolution_configuration["logger"].info(
        "Generated pickle files for each redshift. This took {}".format(
            time.time() - start
        )
    )

    ###################
    # Write redshift info to hdf5 and remove files if necessary
    if convolution_configuration["write_to_hdf5"]:
        convolution_configuration["logger"].info(
            "Writing to HDF5 file".format(time.time() - start)
        )

        # Open file
        output_hdf5file = h5py.File(output_filename, "a")

        # Loop over files in the pickle dir
        for file in sorted(os.listdir(tmp_output_dir)):
            full_path = os.path.join(tmp_output_dir, file)

            # Open file
            with open(full_path, "rb") as f:
                # read out data
                redshift_dict = pickle.load(f)

                # Loop over rate types
                for rate_type_key in redshift_dict.keys():
                    rate_type_dict = redshift_dict[rate_type_key]

                    # Read out the current data
                    rate_type = rate_type_dict["rate_type"]
                    redshift_value = rate_type_dict["time_value"]
                    rate_array = rate_type_dict["rate_array"]

                    # Add group if necessary
                    if not rate_type in output_hdf5file["data/"].keys():
                        output_hdf5file.create_group("data/{}".format(rate_type))

                    #
                    convolution_configuration["logger"].debug(
                        "Adding z={} rate_type={} to {}".format(
                            redshift_value, rate_type, output_filename
                        )
                    )

                    # write to hdf5
                    output_hdf5file["data/{}".format(rate_type)].create_dataset(
                        str(redshift_value), data=rate_array
                    )

            # remove the pickled file
            if convolution_configuration["remove_pickle_files"]:
                os.remove(full_path)

        # Remove the directory
        if convolution_configuration["remove_pickle_files"]:
            shutil.rmtree(tmp_output_dir)
