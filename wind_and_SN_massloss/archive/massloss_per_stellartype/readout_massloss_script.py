import h5py
import json
import pandas as pd
import numpy as np
import os

import matplotlib.pyplot as plt
from david_phd_functions.plotting import custom_mpl_settings

from binarycpython.utils.stellar_types import (
    STELLAR_TYPE_DICT_SHORT as stellar_type_dict_short,
)

custom_mpl_settings.load_mpl_rc()

"""
Script to look at mass lost during stellar evolution. Should look at how much mass they lose with the supernova
"""


def load_file_return_df(filename, dataset_name):
    # Load files
    # file_name = '/home/david/projects/binary_c_root/results/testing_python/supernova_mass_test/SN_types_z0.002.hdf5'
    f = h5py.File(filename, "r")

    # Show some keys
    # for key in f.keys():
    #     print(key) #Names of the groups in HDF5 file.

    # for key in f['data'].keys():
    #     print(key)

    # for key in f['settings'].keys():
    #     print(key)

    # Load data and put in dataframe
    data = f["data"][dataset_name][()]
    # settings = f['settings']['used_settings'][()]
    # settings_json = json.loads(settings)

    headers = f["data"][dataset_name + "_header"][()]
    headers = [header.decode("utf-8") for header in headers]

    # metallicity = json.loads(settings)['binary_c_defaults']['metallicity']

    # Make df
    df = pd.DataFrame(data)
    df.columns = headers

    for i in range(16):
        df["f_st_{}".format(i)] = df["st_{}".format(i)] / df["zams_mass"]

    df = df.sort_values(by=["zams_mass"])

    return df


def plot_massloss(df, metallicity):
    """
    Function to plot the massloss
    """

    # Create a stack plot of where what mass is lost
    x = df["zams_mass"]
    y1 = df["f_st_0"]
    y2 = df["f_st_1"]
    y3 = df["f_st_2"]
    y4 = df["f_st_3"]
    y5 = df["f_st_4"]
    y6 = df["f_st_5"]
    y7 = df["f_st_6"]
    y8 = df["f_st_7"]
    y9 = df["f_st_8"]
    y10 = df["f_st_9"]
    y11 = df["f_st_10"]
    y12 = df["f_st_11"]
    y13 = df["f_st_12"]
    y14 = df["f_st_13"]
    y15 = df["f_st_14"]
    y16 = df["f_st_15"]

    for el in [y1, y2, y3, y4, y5, y6, y7, y8, y9, y10, y11, y12, y13, y14, y15, y16]:
        print(el.sum())

    labels = ["ML {}".format(stellar_type_dict_short[i]) for i in range(16)]

    fig, ax = plt.subplots()
    ax.stackplot(
        x,
        y1,
        y2,
        y3,
        y4,
        y5,
        y6,
        y7,
        y8,
        y9,
        y10,
        y11,
        y12,
        y13,
        y14,
        y15,
        y16,
        labels=labels,
    )

    # Legend
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

    # Put a legend to the right of the current axis
    ax.legend(loc="center left", bbox_to_anchor=(1, 0.5))

    #
    ax.set_title("Mass lost during evolution of stars at Z={}".format(metallicity))
    ax.set_xlabel(r"Mass ZAMS (M$_{\odot}$)")
    ax.set_ylabel(r"Fraction of initial mass$)")

    #
    plt.show()


df_0002 = load_file_return_df(
    os.path.join(
        "/home/david/projects/binary_c_root/results/testing_python/massloss_test/massloss_0.002",
        "massloss_0.002.hdf5",
    ),
    dataset_name="massloss_0.002",
)
df_001 = load_file_return_df(
    os.path.join(
        "/home/david/projects/binary_c_root/results/testing_python/massloss_test/massloss_0.01",
        "massloss_0.01.hdf5",
    ),
    dataset_name="massloss_0.01",
)
df_002 = load_file_return_df(
    os.path.join(
        "/home/david/projects/binary_c_root/results/testing_python/massloss_test/massloss_0.02",
        "massloss_0.02.hdf5",
    ),
    dataset_name="massloss_0.02",
)


plot_massloss(df_0002, "0.002")
# plot_massloss(df_001, '0.01')
# plot_massloss(df_002, '0.02')
