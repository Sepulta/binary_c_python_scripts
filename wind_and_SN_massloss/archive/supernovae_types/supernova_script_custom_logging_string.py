supernova_custom_logging_string = """
if(stardata->star[0].SN_type != SN_NONE)
{
    if (stardata->model.time < stardata->model.max_evolution_time)
    {
        Printf("DAVID_SN %30.12e %g %g %g %d %g\\n",
            //
            stardata->model.time, // 1
            stardata->star[0].mass, //2
            stardata->previous_stardata->star[0].mass, //3
            stardata->star[0].pms_mass, //4
            stardata->star[0].SN_type, //5
            stardata->model.probability //6
      );
    };
    /* Kill the simulation to save time */
    stardata->model.max_evolution_time = stardata->model.time - stardata->model.dtm;
};
"""
