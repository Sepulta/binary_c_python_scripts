"""
General functions script for TZO readout
"""

import os
import pandas as pd

from grav_waves.settings import convolution_settings


def print_total_rate_groups(df):
    """
    Function to print the total rate of the groups
    """

    group = df.groupby(by="prev_stellar_type_2")

    print(group["probability"].sum())


def load_combined_dataframe(data_dir):
    """
    Function to get the combined dataframe of all results
    """

    #
    metallicity_dirs = os.listdir(data_dir)

    # Loop over the files and read out the data
    combined_df = pd.DataFrame()

    # Loop over metallicities
    for metallicity_dir in metallicity_dirs:
        # Construct dir
        main_dir = os.path.join(data_dir, metallicity_dir)

        # Get df
        data_filename = os.path.join(main_dir, "all_tzo_events.dat")
        df = pd.read_csv(data_filename, sep="\s+")

        # calculate number per formed solar mass
        df["number_per_solar_mass"] = (
            convolution_settings["binary_fraction"]
            * df["probability"]
            / convolution_settings["average_mass_system"]
        )

        # Exclude channels where the other is a compact object:
        filtered_df = df[df.prev_stellar_type_2 < 10]

        # Add to big df
        combined_df = pd.concat([combined_df, filtered_df])

    return combined_df
