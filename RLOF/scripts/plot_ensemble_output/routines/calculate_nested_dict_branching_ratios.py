"""
Function to calculate the nested dict for branching ratios:
- RLOF: Onto MS vs all
- RLOF Onto MS: stable vs unstable
- Stable RLOF Onto MS: disk no disk
"""

import json

from ensemble_functions.ensemble_functions import (
    get_recursive_sum,
)
from RLOF.scripts.plot_ensemble_output.functions import (
    handle_querylist,
)
from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)
from grav_waves.settings import convolution_settings


def calculate_nested_dict_branching_ratios(dataset, weighttype, testing, verbose=0):
    """
    Function to set up the nested dict to plot the branching ratios
    """

    #
    if not weighttype in ["mass", "time", "occurence"]:
        raise ValueError(
            "Weighttype ({}) must be in {}".format(weighttype, ["mass", "time"])
        )

    # Get the correct ensemble data from the dict
    if verbose:
        print("\t\tLoading data")
    probability_to_yield_conversion_factor = (
        convolution_settings["binary_fraction"] * convolution_settings["mass_in_stars"]
    )
    if not weighttype == "occurence":
        ensemble_data = get_ensemble_data(
            dataset,
            testing,
            probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
        )["RLOF"]["fraction_separation_massratio"][weighttype]
    else:
        ensemble_data = get_ensemble_data(
            dataset,
            testing,
            probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
        )["RLOF"]["starting_rlof"]

    #
    total_ensemble = get_recursive_sum(ensemble_data)
    # print(total_ensemble)

    # Get mass transfer onto MS
    ensemble_onto_ms = handle_querylist(
        ensemble_data,
        [{"keyname": "st_acc", "valuerange": [0.5, 1.5]}],
        "onto_ms",
        verbose=verbose,
    )
    total_ensemble_onto_ms = get_recursive_sum(ensemble_onto_ms)
    # print(total_ensemble_onto_ms)

    # Get stable mass transfer onto MS
    stable_ensemble_onto_ms = handle_querylist(
        ensemble_onto_ms,
        [{"keyname": "stable", "valuerange": [-0.5, 0.5]}],
        "stable_onto_ms",
        verbose=verbose,
    )
    total_stable_ensemble_onto_ms = get_recursive_sum(stable_ensemble_onto_ms)
    # print(total_stable_ensemble_onto_ms)

    # Get stable mass transfer onto MS via disk
    disk_stable_ensemble_onto_ms = handle_querylist(
        stable_ensemble_onto_ms,
        [{"keyname": "disk", "valuerange": [0.5, 1.5]}],
        "disk_stable_onto_ms",
        verbose=verbose,
    )
    total_disk_stable_ensemble_onto_ms = get_recursive_sum(disk_stable_ensemble_onto_ms)
    # print(total_disk_stable_ensemble_onto_ms)

    #
    result_dict = {
        "mt_onto_ms": {
            "total": total_ensemble_onto_ms,
            "fraction_of_total_mt": total_ensemble_onto_ms / total_ensemble,
            "stable": {
                "total": total_stable_ensemble_onto_ms,
                "fraction_of_total_mt_onto_ms": total_stable_ensemble_onto_ms
                / total_ensemble_onto_ms,
                "disk": {
                    "total": total_disk_stable_ensemble_onto_ms,
                    "fraction_of_total_stable_mt_onto_ms": total_disk_stable_ensemble_onto_ms
                    / total_stable_ensemble_onto_ms,
                },
                "direct": {
                    "total": total_stable_ensemble_onto_ms
                    - total_disk_stable_ensemble_onto_ms,
                    "fraction_of_total_stable_mt_onto_ms": total_disk_stable_ensemble_onto_ms
                    / total_stable_ensemble_onto_ms,
                },
            },
            "unstable": {
                "total": total_ensemble_onto_ms - total_stable_ensemble_onto_ms,
                "fraction_of_total_mt_onto_ms": (
                    total_ensemble_onto_ms - total_stable_ensemble_onto_ms
                )
                / total_ensemble_onto_ms,
            },
        },
        "rest": {
            "total": total_ensemble - total_ensemble_onto_ms,
            "fraction_of_total_mt": (total_ensemble - total_ensemble_onto_ms)
            / total_ensemble,
        },
    }

    print(json.dumps(result_dict, indent=4))

    return result_dict


#
population_datasets_sana = generate_ensemble_dataset_dict(
    "/home/david/projects/binary_c_root/results/RLOF/LOW_RES_RLOF_SANA_2021_ENSEMBLE_fractions"
)

#
calculate_nested_dict_branching_ratios(
    population_datasets_sana["datasets"]["0.02"], "occurence", testing=True, verbose=0
)
