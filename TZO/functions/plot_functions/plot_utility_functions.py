"""
File contianing extra functions and info for the plotting
"""

import astropy.units as u

#
dimensionless_unit = u.m / u.m

# Dictionary containing names for the quantities
quantity_name_dict = {
    "primary_mass": "Primary mass",
    "secondary_mass": "Secondary mass",
    "chirp_mass": "Chirp mass",
    "total_mass": "Total mass",
    "any_mass": "Mass",
    "mass_ratio": "Mass ratio",
    # ZAMS mass quantity
    "zams_chirpmass": "ZAMS chirp mass",
    "zams_total_mass": "ZAMS total mass",
    "zams_primary_mass": "ZAMS primary mass",
    "zams_secondary_mass": "ZAMS secondary mass",
    "zams_any_mass": "ZAMS mass",
}

# Dictionary containing units for the quantities
quantity_unit_dict = {
    "primary_mass": u.Msun,
    "secondary_mass": u.Msun,
    "total_mass": u.Msun,
    "mass_ratio": dimensionless_unit,
    "chirp_mass": u.Msun,
}
