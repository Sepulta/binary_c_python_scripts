"""
Function to plot either the mass weighted or time weighted distribution of racc and qacc for stable mass transfer onto MS via disks.

The results are normalised by the total amount of stable MT onto MS stars
"""

import os
import json
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

from ensemble_functions.ensemble_functions import (
    get_recursive_sum,
    merge_all_subdicts,
    return_keys_bins_and_bincenters,
    flatten_data_ensemble2d,
    query_dict,
    recursively_merge_all_subdicts,
    merge_recursive_until_stop,
)
from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)

from scipy.ndimage import gaussian_filter
from grav_waves.settings import convolution_settings


def plot_massratio_separation_histogram_one_quantity_three_metallicities(
    dataset_dict,
    testing,
    weight_type,
    verbose=0,
    global_query_dict=None,
    plot_settings=None,
):
    """
    Function to plot the mass weighted and time weighted distribution of racc and qacc for stable mass transfer onto MS via disks.

    The results are normalised by the total amount of stable MT onto MS stars
    """

    DIFF_MAX_THRESHOLD = 3
    contour_levels = [1e-5, 1e-4, 0.5 * 1e-3]

    if not plot_settings:
        plot_settings = {}

    #
    if len(list(dataset_dict.keys())) > 3:
        raise ValueError("Number of metallicities cannot exceed 3")

    #
    if not weight_type in ["mass", "time"]:
        raise ValueError("Chosen weight type not available")

    if verbose:
        print(
            "Running plot_massratio_separation_histogram_one_quantity_three_metallicities\n\toutput: {}".format(
                os.path.abspath(plot_settings["output_name"])
            )
        )

        if global_query_dict:
            print(
                "\twith global query: {}".format(global_query_dict["global_query_name"])
            )
            print(
                "\t\tqueries: {}".format(
                    ", ".join(
                        [
                            "{} between {}".format(
                                query["parameter_key"], query["value_range"]
                            )
                            for query in global_query_dict["query_list"]
                        ]
                    )
                )
            )

    # Loop over keys first time to get the data and
    subensemble_dict = {}
    for key in dataset_dict.keys():
        if verbose:
            print("\tZ = {}".format(key))
        current_dataset = dataset_dict[key]

        # Get the correct ensemble data from the dict
        if verbose:
            print("\t\tLoading data (weight_type: {})".format(weight_type))
        probability_to_yield_conversion_factor = (
            convolution_settings["binary_fraction"]
            * convolution_settings["mass_in_stars"]
        )
        ensemble_data = get_ensemble_data(
            current_dataset,
            testing,
            probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
        )["RLOF"]["fraction_separation_massratio"][weight_type]

        if global_query_dict:
            if verbose:
                print("\tPerforming queries")
            for query in global_query_dict["query_list"]:
                if verbose:
                    print(
                        "\t\tPerforming query {}".format(
                            "{} between {}".format(
                                query["parameter_key"], query["value_range"]
                            )
                        )
                    )
                ensemble_data = query_dict(
                    ensemble_data, query["parameter_key"], query["value_range"]
                )

        # Get stable MT
        if verbose:
            print("\t\tSelecting stable MT")
        stable_subensemble = query_dict(ensemble_data, "stable", [-0.5, 0.5])

        # Get MT onto MS
        if verbose:
            print("\t\tSelecting MT onto MS")
        ms_stable_subensemble = query_dict(stable_subensemble, "st_acc", [0.5, 1.5])
        total_ms_stable_subensemble = get_recursive_sum(ms_stable_subensemble)

        # Get MS via disk
        if verbose:
            print("\t\tSelecting disk MT")
        disk_ms_stable_subensemble = query_dict(
            ms_stable_subensemble, "disk", [0.5, 1.5]
        )

        if verbose:
            print("\t\tMerging until one-to-last parameter")
        merged_disk_ms_stable_subensemble = merge_recursive_until_stop(
            disk_ms_stable_subensemble, "r_acc_sep"
        )

        # Get bins and stuff
        if verbose:
            print("\t\tGetting the bins")
        (
            log10_fraction_accretor_separation_keys,
            bins_log10_accretor_separation,
            bincenters_log10_accretor_separation,
        ) = return_keys_bins_and_bincenters(
            merged_disk_ms_stable_subensemble, "r_acc_sep"
        )
        (
            log10_massratio_keys,
            bins_log10_massratio,
            bincenters_log10_massratio,
        ) = return_keys_bins_and_bincenters(
            merge_all_subdicts(merged_disk_ms_stable_subensemble["r_acc_sep"]),
            "q_acc_don",
        )

        # Set up dict to store the huistograms in
        if verbose:
            print("\t\tFlattening data")
        flattenend_disk_ms_stable_subensemble = flatten_data_ensemble2d(
            merged_disk_ms_stable_subensemble, "r_acc_sep", "q_acc_don"
        )

        # Create histograms
        if verbose:
            print("\t\tCreating histograms")
        hist_data_disk_ms_stable_subensemble, _, _ = np.histogram2d(
            flattenend_disk_ms_stable_subensemble[0],
            flattenend_disk_ms_stable_subensemble[1],
            bins=[bins_log10_accretor_separation, bins_log10_massratio],
            weights=flattenend_disk_ms_stable_subensemble[2],
        )

        # Normalize
        if verbose:
            print("\t\tNormalising histograms")
        normed_hist_data_disk_ms_stable_subensemble = (
            hist_data_disk_ms_stable_subensemble / total_ms_stable_subensemble
            if total_ms_stable_subensemble != 0
            else np.zeros(hist_data_disk_ms_stable_subensemble.shape)
        )

        # apply gaussian blur
        if verbose:
            print("\t\tApplying gaussian blur")
        normed_hist_data_disk_ms_stable_subensemble = gaussian_filter(
            normed_hist_data_disk_ms_stable_subensemble, sigma=2
        )

        subensemble_dict[key] = {
            "normed_hist_data": normed_hist_data_disk_ms_stable_subensemble,
            "bincenters_log10_accretor_separation": bincenters_log10_accretor_separation,
            "bincenters_log10_massratio": bincenters_log10_massratio,
        }

    # Loop over keys second time to find the max value
    global_max_val = -1
    for key in dataset_dict.keys():
        global_max_val = np.max(
            [global_max_val, subensemble_dict[key]["normed_hist_data"].max()]
        )

    # Set up figure
    if verbose:
        print("\t\tPlotting figure")
    fig = plt.figure(figsize=(20, 6))

    #
    gs = fig.add_gridspec(nrows=1, ncols=11)

    # create axes
    ax_high_metallicity = fig.add_subplot(gs[0, 0:3])
    ax_mid_metallicity = fig.add_subplot(gs[0, 3:6])
    ax_low_metallicity = fig.add_subplot(gs[0, 6:9])

    sorted_keys = list(dataset_dict.keys())
    axes_dict = {
        sorted_keys[0]: ax_high_metallicity,
        sorted_keys[1]: ax_mid_metallicity,
        sorted_keys[2]: ax_low_metallicity,
    }

    # Create colorbar
    ax_cb = fig.add_subplot(gs[0, 10])

    for key in sorted_keys:

        # Set up the colormesh plot
        X, Y = np.meshgrid(
            subensemble_dict[key]["bincenters_log10_accretor_separation"],
            subensemble_dict[key]["bincenters_log10_massratio"],
        )
        hist_disk = axes_dict[key].pcolormesh(
            X,
            Y,
            subensemble_dict[key]["normed_hist_data"].T,
            norm=colors.LogNorm(
                vmin=10 ** (np.log10(global_max_val) - DIFF_MAX_THRESHOLD),
                vmax=global_max_val,
            ),
            shading="auto",
            antialiased=True,
            rasterized=True,
        )

        #
        CS = axes_dict[key].contour(
            X,
            Y,
            subensemble_dict[key]["normed_hist_data"].T,
            levels=contour_levels,
            colors="k",
            linestyles=["solid", "dashed", "-."],
        )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        cmap=matplotlib.cm.viridis,
        norm=matplotlib.colors.LogNorm(
            vmin=10 ** (np.log10(global_max_val) - DIFF_MAX_THRESHOLD),
            vmax=global_max_val,
        ),
        extend="min",
    )
    cbar.ax.set_ylabel(
        "Fraction of all stable mass-\ntransfer onto main-sequence\nstars"
    )

    # Set titles
    ax_high_metallicity.set_title(r"$Z={}$".format(sorted_keys[0]))
    ax_mid_metallicity.set_title(r"$Z={}$".format(sorted_keys[1]))
    ax_low_metallicity.set_title(r"$Z={}$".format(sorted_keys[2]))

    # Set y axes labels
    ax_high_metallicity.set_ylabel(r"log$_{10}$($M_{\mathrm{acc}}$/$M_{\mathrm{don}}$)")
    ax_mid_metallicity.set_yticklabels([])
    ax_low_metallicity.set_yticklabels([])

    # set x axes labels over whole plot
    ax_mid_metallicity.set_xlabel(r"log$_{10}$($R_{\mathrm{acc}}$ / $a$)")

    # Remove the last tick(label) from the x axes for the first 2 subplots
    xticks_mid_metallicity = ax_mid_metallicity.xaxis.get_major_ticks()
    xticks_mid_metallicity[-1].label1.set_visible(False)

    xticks_high_metallicity = ax_high_metallicity.xaxis.get_major_ticks()
    xticks_high_metallicity[-1].label1.set_visible(False)

    # Set colorbar
    ax_cb.set_ylabel("Fraction of all mass transfer\nonto main-sequence stars")

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


# population_datasets_sana = generate_ensemble_dataset_dict('/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions/')
# plot_massratio_separation_histogram_one_quantity_three_metallicities(population_datasets_sana, weight_type='time', testing=True, verbose=1, plot_settings={'show_plot': True, 'output_name': 'test.pdf'})
