"""
Function to plot some stuff with the new structure
TODO: fix that the numbers that we plot match the total intrinsic rate numbers
https://en.wikipedia.org/wiki/Kernel_density_estimation
"""

import h5py
import numpy as np
import pandas as pd

import astropy.units as u

import matplotlib.pyplot as plt

from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    create_any_mass_df,
    filter_dco_type,
    color_list,
    hatch_list,
    get_rate_type_names,
)
from grav_waves.convolution.functions.plot_routines.plot_kde_and_bootstrap_functions import (
    calculate_kde_values,
    run_bootstrap,
)

from david_phd_functions.grav_waves.dataframe_functions import add_chirpmass_column
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def get_data(
    hdf5_filename,
    dco_type,
    redshift_key,
    mass_bins,
    rate_key_in_df,
    rate_type_hdf5_key,
    divide_by_mass_bins,
    bootstraps=0,
    num_procs=1,
    base_query=None,
    query=None,
):
    """
    Function to get the data out of the hdf5 file and query if necessary
    """

    ######
    # Settings

    # Get combined df
    combined_df = pd.read_hdf(hdf5_filename, "/data/combined_dataframes")

    # Add columns
    combined_df = add_chirpmass_column(combined_df, m1_name="mass_1", m2_name="mass_2")
    combined_df["total_mass"] = combined_df["mass_1"] + combined_df["mass_2"]
    combined_df["primary_mass"] = combined_df[["mass_1", "mass_2"]].max(axis=1)
    combined_df["secondary_mass"] = combined_df[["mass_1", "mass_2"]].min(axis=1)

    index_df = pd.read_hdf(
        hdf5_filename, "/data/{}/{}".format(rate_type_hdf5_key, str(redshift_key))
    )
    joined_df = index_df.join(combined_df, on="local_index")

    # Select correct dco type
    joined_df = filter_dco_type(joined_df, dco_type)

    # Perform base query
    if not base_query is None:
        joined_df = joined_df.query(base_query)

    # turn into any mass
    any_mass_joined_df = create_any_mass_df(joined_df)
    if not query is None:
        any_mass_joined_df = any_mass_joined_df.query(query)

    #
    data_array = any_mass_joined_df["mass"]
    weight_array = any_mass_joined_df[rate_key_in_df]

    # Determine the mass bins
    mass_bin_size = np.diff(mass_bins)
    mass_bincenter = (mass_bins[1:] + mass_bins[:-1]) / 2

    # bin and take into account the divison by mass
    hist = np.histogram(data_array, bins=mass_bins, weights=weight_array)[0]
    if divide_by_mass_bins:
        hist = hist / mass_bin_size

    # Select the non-zero bins and split off the empty ones
    # NOTE: without this, toms method does not work
    non_zero_bins_indices = np.nonzero(hist)[0]
    mass_bins = mass_bins[non_zero_bins_indices.min() : non_zero_bins_indices.max() + 1]

    #
    return_dict = {
        "mass_centers": mass_bincenter,
        "rates": hist,
    }

    ########################
    # KDE
    x_KDE_width = 0.1
    kde_width = 0.1 * mass_bin_size[0]

    x_KDE = np.arange(mass_bins.min(), mass_bins.max() + x_KDE_width, x_KDE_width)
    center_KDEbins = (x_KDE[:-1] + x_KDE[1:]) / 2.0

    # calculate_kde_values
    y_vals = calculate_kde_values(
        data=data_array,
        weights=weight_array,
        lower_bound=data_array.min(),
        upper_bound=data_array.max(),
        bw_method=kde_width,
        center_KDEbins=center_KDEbins,
        hist=hist,
    )

    #
    return_dict["kde_yvals"] = y_vals
    return_dict["center_KDEbins"] = center_KDEbins

    ########################
    # Bootstrap
    if bootstraps:

        # TODO: Ask lieke why do we use x_KDE here instead of center_KDEbins
        _, median, percentiles = run_bootstrap(
            masses=data_array,
            weights=weight_array,
            indices=data_array.index,
            kde_width=kde_width,
            mass_bins=mass_bins,
            center_KDEbins=x_KDE,
            num_procs=num_procs,
            bootstraps=bootstraps,
        )

        #
        return_dict["median"] = median
        return_dict["percentiles"] = percentiles

    #
    return return_dict


def plot_any_mass_at_zero_redshift(
    convolution_dataset_hdf5_filename,
    dco_type,
    rate_type,
    any_mass_bins,
    bootstraps=0,
    plot_xlim=None,
    base_querydict=None,
    querylist=None,
    divide_by_binsize=True,
    add_ratio=False,
    add_cdf=False,
    num_procs=1,
    plot_settings=None,
):
    """
    Test function to plot things with the new datastructure
    """

    # Select correct rate type:
    if rate_type == "merger_rate":
        rate_key_in_df = "merger_rates"
        long_name_rate_type = "Merger rate"
        rate_type_hdf5_key = "merger_rate_data"
    elif rate_type == "formation_rate":
        rate_key_in_df = "formation_rates"
        long_name_rate_type = "Formation rate"
        rate_type_hdf5_key = "formation_rate_data"

    # Read out first redshift key
    hdf5_file = h5py.File(convolution_dataset_hdf5_filename, "r")
    zero_key = str(
        min([float(el) for el in list(hdf5_file["data"][rate_type_hdf5_key].keys())])
    )
    hdf5_file.close()

    # Get the unit correct
    number_density_per_year = 1 / u.Gpc**3 / u.yr
    plotting_unit = number_density_per_year

    if divide_by_binsize:
        plotting_unit = plotting_unit / u.Msun

    # Calculate the data:
    results_dict = {}
    results_dict["all"] = get_data(
        convolution_dataset_hdf5_filename,
        dco_type=dco_type,
        redshift_key=zero_key,
        mass_bins=any_mass_bins,
        bootstraps=bootstraps,
        rate_key_in_df=rate_key_in_df,
        rate_type_hdf5_key=rate_type_hdf5_key,
        divide_by_mass_bins=divide_by_binsize,
        num_procs=num_procs,
        base_query=base_querydict["query"] if base_querydict else None,
    )
    if not querylist is None:
        for query_dict in querylist:
            results_dict[query_dict["name"]] = get_data(
                convolution_dataset_hdf5_filename,
                dco_type=dco_type,
                redshift_key=zero_key,
                mass_bins=any_mass_bins,
                bootstraps=bootstraps,
                rate_key_in_df=rate_key_in_df,
                rate_type_hdf5_key=rate_type_hdf5_key,
                divide_by_mass_bins=divide_by_binsize,
                num_procs=num_procs,
                query=query_dict["query"],
                base_query=base_querydict["query"] if base_querydict else None,
            )

    # Decide until where we plot
    shift_from_max = 3
    max_value = results_dict["all"]["rates"].max()
    custom_min_value = 10 ** (np.log10(max_value) - shift_from_max)

    # override
    max_value = 3 * 1e2
    custom_min_value = 1e-3

    xlabel_text = r"Mass [M$_{\odot}$]"

    #######################
    # Set up figure
    # Set up axes
    fig = plt.figure(figsize=(40, 40))
    gs = fig.add_gridspec(nrows=12, ncols=1)

    # Add subplots
    # TODO: put this in the canvas stuff or a function
    if add_ratio and add_cdf:
        ax = fig.add_subplot(gs[:5, :])
        ax_ratio = fig.add_subplot(gs[5:8, :], sharex=ax)
        ax_cdf = fig.add_subplot(gs[8:, :])
        ax_cdf.set_xlabel(xlabel_text)
    elif add_ratio:
        ax = fig.add_subplot(gs[:8, :])
        ax_ratio = fig.add_subplot(gs[8:, :], sharex=ax)
        ax_ratio.set_xlabel(xlabel_text)
    elif add_cdf:
        ax = fig.add_subplot(gs[:8, :])
        ax_cdf = fig.add_subplot(gs[8:, :])
        ax_cdf.set_xlabel(xlabel_text)
    else:
        ax = fig.add_subplot(gs[:, :])
        ax.set_xlabel(xlabel_text)

    #########
    # Plot the histograms
    ax.plot(
        results_dict["all"]["mass_centers"],
        results_dict["all"]["rates"],
        lw=5,
        c=colors[0],
        zorder=200,
        linestyle="--",
    )

    # plot the KDE values
    ax.plot(
        results_dict["all"]["center_KDEbins"],
        results_dict["all"]["kde_yvals"],
        lw=5,
        c=colors[0],
        label="All ({})".format(base_querydict["name"]) if base_querydict else "All",
        zorder=13,
    )

    if bootstraps:
        ax.fill_between(
            results_dict["all"]["center_KDEbins"],
            results_dict["all"]["percentiles"][0],
            results_dict["all"]["percentiles"][1],
            alpha=0.4,
            zorder=11,
            color=colors[0],
        )  # 1-sigma
        ax.fill_between(
            results_dict["all"]["center_KDEbins"],
            results_dict["all"]["percentiles"][2],
            results_dict["all"]["percentiles"][3],
            alpha=0.2,
            zorder=10,
            color=colors[0],
        )  # 2-sgima

    # Add cdf
    if add_cdf:
        ax_cdf.plot(
            results_dict["all"]["mass_centers"],
            np.cumsum(results_dict["all"]["rates"]),
            label="total",
        )

    # Plot queried results
    if not querylist is None:
        if add_ratio:
            # First make a empty shape that will contain the stacked results
            total_ratio_hist = np.zeros(results_dict["all"]["rates"].shape)

        for query_i, query_dict in enumerate(querylist):
            # Plot histogram
            ax.plot(
                results_dict[query_dict["name"]]["mass_centers"],
                results_dict[query_dict["name"]]["rates"],
                lw=5,
                c=colors[query_i + 1],
                zorder=query_i + 2,
                linestyle="--",
            )

            # Plot KDE and fill-between 0 and kde
            ax.plot(
                results_dict[query_dict["name"]]["center_KDEbins"],
                results_dict[query_dict["name"]]["kde_yvals"],
                lw=5,
                c=colors[query_i + 1],
                label=query_dict["name"],
                zorder=query_i + 2,
            )
            ax.fill_between(
                results_dict[query_dict["name"]]["center_KDEbins"],
                0,
                results_dict[query_dict["name"]]["kde_yvals"],
                color=colors[query_i + 1],
                zorder=query_i + 2,
                alpha=0.1,
            )
            ax.fill_between(
                results_dict[query_dict["name"]]["center_KDEbins"],
                0,
                results_dict[query_dict["name"]]["kde_yvals"],
                hatch=hatch_list[query_i],
                color=colors[query_i + 1],
                facecolor="none",
                edgecolor=colors[query_i + 1],
                zorder=query_i + 3,
            )

            # Plot the bootstrapped stuff
            if bootstraps:
                ax.fill_between(
                    results_dict[query_dict["name"]]["center_KDEbins"],
                    results_dict[query_dict["name"]]["percentiles"][0],
                    results_dict[query_dict["name"]]["percentiles"][1],
                    alpha=0.4,
                    zorder=11,
                    color=colors[query_i + 1],
                )  # 1-sigma
                ax.fill_between(
                    results_dict[query_dict["name"]]["center_KDEbins"],
                    results_dict[query_dict["name"]]["percentiles"][2],
                    results_dict[query_dict["name"]]["percentiles"][3],
                    alpha=0.2,
                    zorder=10,
                    color=colors[query_i + 1],
                )  # 2-sgima

            # Add cdf
            if add_cdf:
                ax_cdf.plot(
                    results_dict[query_dict["name"]]["mass_centers"],
                    np.cumsum(results_dict[query_dict["name"]]["rates"]),
                    label=query_dict["name"],
                    color=colors[query_i + 1],
                )

            # Add ratio
            if add_ratio:
                ratio_to_total = np.divide(
                    results_dict[query_dict["name"]]["rates"],
                    results_dict["all"]["rates"],
                    where=results_dict["all"]["rates"] != 0,
                    out=np.zeros_like(results_dict[query_dict["name"]]["rates"]),
                )
                ax_ratio.bar(
                    results_dict[query_dict["name"]]["mass_centers"],
                    ratio_to_total,
                    width=any_mass_bins,
                    bottom=total_ratio_hist,
                    color=colors[query_i + 1],
                    label=query_dict["name"],
                )
                total_ratio_hist += ratio_to_total

    # Set legends
    ax.legend()
    if add_ratio:
        ax_ratio.legend()
    if add_cdf:
        ax_cdf.legend()

    ax.set_ylabel(
        "{} density for {}".format(long_name_rate_type, "Any mass")
        + r" [{}]".format(plotting_unit.unit.to_string("latex_inline"))
    )
    ax.set_title(
        "{} density for {} at Z={}".format(long_name_rate_type, "Any mass", zero_key)
    )

    ax.set_yscale("log")
    ax.set_ylim([custom_min_value, max_value])

    if not plot_xlim is None:
        ax.set_xlim(plot_xlim)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


import matplotlib

matplotlib.use("Agg")

# # Global settings
# # rebinned_results_filename = '/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/convolution/scripts/new_main/output/output_high_res_five_coreshift/convolution/rebinned_convolution_without_detection_probability.h5'
# rebinned_results_filename = '/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/convolution/scripts/new_main/output/output_high_res/convolution/rebinned_convolution_without_detection_probability.h5'

# convolution_plot_dir = 'output/'
# divide_by_binsize = False

# #
# querylist = [
#     {'name': 'No PPISN', 'query': '(undergone_ppisn==0)'},
#     {'name': 'PPISN', 'query': '(undergone_ppisn==1)'}
# ]


# plot_any_mass_at_zero_redshift(
#     rebinned_results_filename,
#     divide_by_binsize=False,
#     rate_type='merger_rate',
#     dco_type='bhbh',

#     any_mass_bins=np.arange(0, 70, 1),
#     num_procs=4,

#     # add_ratio=True,
#     querylist=querylist,
#     plot_xlim=[0, 70],

#     plot_settings={
#         'show_plot': True,
#         'simulation_name': 'test',
#         'antialiased': True,
#         'rasterized': True,
#         'runname': 'plot_birth_redshift_distribution_for_mergers_at_specific_redshift_for_mass_quantity',
#     }
# )
