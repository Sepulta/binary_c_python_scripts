"""
Plot functions for the zams accretion ratio
"""

#
import numpy as np
import pandas as pd

#
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

import astropy.units as u
import astropy.constants as const


def luminosity_boundary_layer(M, Mdot, R):
    """
    Luminosity of boundary layer

    input:
        M: mass in solar mass
        Mdot: mass in solar mass per year
        R: radius in solar radius
    """

    lum = 0.5 * (
        (const.G * (M * u.Msun).to(u.kg) * (Mdot * u.Msun / u.yr).to(u.kg / u.s))
        / ((R * u.Rsun).to(u.m))
    )

    return lum.to(u.Lsun).value


def plot_ratio_mdot_thermal_mdot_acc(datafile, plot_settings=None):
    """
    Function to plot the mdot thermal vs mdot_acc
    """

    #
    if plot_settings is None:
        plot_settings = {}

    #
    df = pd.read_csv(datafile, sep="\s+", header=0)

    df["tkh_in_year"] = df["tkh"] * 1e6
    df["mdot_thermal"] = df["mass"] / df["tkh_in_year"]

    mass_array = df["mass"].to_numpy()
    mdot_acc_array = 10 ** np.arange(-20.0, 1.0, 0.5)
    mdot_thermal_array = df["mdot_thermal"].to_numpy()

    #
    mesh_mdot_acc_array, mesh_mdot_thermal_array = np.meshgrid(
        mdot_acc_array, mdot_thermal_array
    )
    _, mesh_mass_array = np.meshgrid(mdot_acc_array, mass_array)

    # Calculate the ratio
    mesh_mdot_ratio = mesh_mdot_acc_array / mesh_mdot_thermal_array
    min_val = mesh_mdot_ratio.min()
    max_val = mesh_mdot_ratio.max()

    ###
    #
    fig = plt.figure(figsize=(20, 20))

    #
    gs = fig.add_gridspec(nrows=1, ncols=11)

    ax = fig.add_subplot(gs[:, 0:9])
    ax_cb = fig.add_subplot(gs[0, 10])

    #
    norm = colors.LogNorm(
        vmin=10 ** (np.log10(min_val)), vmax=10 ** (np.log10(max_val))
    )

    #
    _ = ax.pcolormesh(
        mesh_mdot_acc_array,
        mesh_mass_array,
        mesh_mdot_ratio,
        norm=norm,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    contour_levels = [1e-1, 1e0, 1e1]
    linestyles = ["solid", "dashed", "-.", ":"]

    CS_mt_region_1 = ax.contour(
        mesh_mdot_acc_array,
        mesh_mass_array,
        mesh_mdot_ratio,
        levels=contour_levels,
        colors="k",
        linestyles=linestyles,
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        cmap=matplotlib.cm.viridis,
        norm=norm,
        extend="min",
    )
    cbar.ax.set_ylabel(r"$\dot{M}_{\mathrm{acc}}/\dot{M}_{\mathrm{kh,\ ZAMS}}$")

    for contour_i, _ in enumerate(contour_levels):
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [contour_levels[contour_i]] * 2,
            "black",
            linestyle=linestyles[contour_i],
        )

    # make up
    ax.set_xscale("log")
    ax.set_yscale("log")

    ax.set_xlim(mdot_acc_array.min(), mdot_acc_array.max())

    ax.set_xlabel(r"Accretion rate [$\mathrm{M}_{\odot} \mathrm{yr}^{-1}$]")
    ax.set_ylabel(r"ZAMS mass [$M_{\odot}$]")

    ax.set_title(
        "Ratio of thermal accretion rate limit at ZAMS and mass accretion rates for a range of ZAMS masses at Z=0.02"
    )

    fig.tight_layout()

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


def plot_ratio_lacc_lzams(datafile, plot_settings=None):
    """
    Function to plot the Lzams over Lbl
    """

    #
    if plot_settings is None:
        plot_settings = {}

    #
    df = pd.read_csv(datafile, sep="\s+", header=0)

    luminosity_array = df["luminosity"]
    mass_array = df["mass"]
    radius_array = df["radius"]

    #
    mdot_acc_array = 10 ** np.arange(-20.0, 1.0, 0.5)

    mesh_luminosity_array, mesh_mdot_acc_array = np.meshgrid(
        luminosity_array, mdot_acc_array
    )
    mesh_mass_array, mdot_acc_array = np.meshgrid(mass_array, mdot_acc_array)
    mesh_radius_array, mdot_acc_array = np.meshgrid(radius_array, mdot_acc_array)

    # Create empty array
    ratio_lum_zams_lum_bl = np.zeros(mesh_luminosity_array.shape)
    flattened_ratio_lum_zams_lum_bl = ratio_lum_zams_lum_bl.flatten()

    for i, (radius, mass, lum, mdot) in enumerate(
        zip(
            mesh_radius_array.flatten(),
            mesh_mass_array.flatten(),
            mesh_luminosity_array.flatten(),
            mesh_mdot_acc_array.flatten(),
        )
    ):
        lum_bl = luminosity_boundary_layer(mass, mdot, radius)

        # print("Radius {} mass {} lum {} mdot {} lum_bl {}".format(radius, mass, lum, mdot, lum_bl))

        flattened_ratio_lum_zams_lum_bl[i] = lum_bl / lum

    # shape back
    ratio_lum_zams_lum_bl = flattened_ratio_lum_zams_lum_bl.reshape(
        ratio_lum_zams_lum_bl.shape
    )
    min_val = ratio_lum_zams_lum_bl.min()
    max_val = ratio_lum_zams_lum_bl.max()

    ###
    #
    fig = plt.figure(figsize=(20, 20))

    #
    gs = fig.add_gridspec(nrows=1, ncols=11)

    ax = fig.add_subplot(gs[:, 0:9])
    ax_cb = fig.add_subplot(gs[0, 10])

    #
    norm = colors.LogNorm(
        vmin=10 ** (np.log10(min_val)), vmax=10 ** (np.log10(max_val))
    )

    #
    _ = ax.pcolormesh(
        mesh_mdot_acc_array,
        mesh_mass_array,
        ratio_lum_zams_lum_bl,
        norm=norm,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    contour_levels = [1e-1, 1e0, 1e1]
    linestyles = ["solid", "dashed", "-.", ":"]

    CS_mt_region_1 = ax.contour(
        mesh_mdot_acc_array,
        mesh_mass_array,
        ratio_lum_zams_lum_bl,
        levels=contour_levels,
        colors="k",
        linestyles=linestyles,
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        cmap=matplotlib.cm.viridis,
        norm=norm,
        extend="min",
    )
    cbar.ax.set_ylabel(r"$L_{\mathrm{bl}}/L_{\mathrm{ZAMS}}$")

    for contour_i, _ in enumerate(contour_levels):
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [contour_levels[contour_i]] * 2,
            "black",
            linestyle=linestyles[contour_i],
        )

    # make up
    ax.set_xscale("log")
    ax.set_yscale("log")

    ax.set_xlim(mdot_acc_array.min(), mdot_acc_array.max())

    ax.set_xlabel(r"Accretion rate [$\mathrm{M}_{\odot} \mathrm{yr}^{-1}$]")
    ax.set_ylabel(r"ZAMS mass [$M_{\odot}$]")

    ax.set_title(
        "Ratio of stellar luminosity at ZAMS and boundary layer luminosity for a range of ZAMS masses at Z=0.02"
    )

    fig.tight_layout()

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)
