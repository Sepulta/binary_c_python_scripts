"""
Script to run a grid of single stars in mass and metallicity and plot that
"""

import os

from david_phd_functions.binaryc.personal_defaults import personal_defaults

from grav_waves.settings import (
    project_specific_bse_settings,
    project_specific_grid_settings,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.generate_data_function import (
    generate_data_function,
)

#
this_file_dir = os.path.dirname(os.path.abspath(__file__))


def main(
    main_output_dir,
    sim_name,
    bse_settings,
    system_generator_settings,
    grid_settings,
    general_plot_config={},
    generate_data=False,
    generate_plot=False,
    generate_data_if_not_present=False,
):
    """
    main function to run a grid of stars in mass and metallicity
    """

    print("Called main function for m-Z grid.")
    print("Using physics settings:\n\t{}".format(bse_settings))
    print("Using system generator settings:\n\t{}".format(system_generator_settings))
    print("Using grid settings:\n\t{}".format(grid_settings))

    ##
    # Create names
    data_output_dir = os.path.join(main_output_dir, "data/")

    # Create directories
    os.makedirs(main_output_dir, exist_ok=True)
    os.makedirs(data_output_dir, exist_ok=True)

    # Generate the data
    if generate_data:

        # Generate grid
        generate_data_function(
            output_dir=data_output_dir,
            system_generator_settings=system_generator_settings,
            grid_settings=grid_settings,
            physics_settings=bse_settings,
        )


if __name__ == "__main__":

    ##
    # Setting for this script
    generate_data = True
    generate_plots = False
    generate_data_if_not_present = False

    ###
    # Set up the base settings
    system_generator_settings = {
        "mass_resolution": 233,
        "min_mass": 8,
        "max_mass": 300,
        "log10metallicity_resolution": 4 * 24,
        "min_log10_metallicity": -3.9,
        "max_log10_metallicity": -1.4,
    }

    # Create the BSE settings
    bse_settings = {
        **personal_defaults,
        **project_specific_bse_settings,
        "BH_prescription": 3,
        "multiplicity": 1,
        "minimum_timestep": 1e-8,
    }

    #
    grid_settings = {
        **project_specific_grid_settings,
        "combine_ensemble_with_thread_joining": True,
        "num_cores": 8,
        "verbosity": 1,
    }

    general_plot_config = {}

    #
    main_dir = this_file_dir

    ##
    # Vanilla/default model
    main(
        main_output_dir=os.path.join(main_dir, "output/output_vanilla/"),
        sim_name="vanilla",
        bse_settings=bse_settings,
        system_generator_settings=system_generator_settings,
        grid_settings=grid_settings,
        general_plot_config=general_plot_config,
        generate_data=generate_data,
        generate_plot=generate_plots,
        generate_data_if_not_present=generate_data_if_not_present,
    )

    ##
    # Variation: Robs method
    robs_prescription_setting = {**bse_settings, "PPISN_prescription": 1}
    main(
        main_output_dir=os.path.join(main_dir, "output/output_robs_prescription/"),
        sim_name="robs_prescription",
        bse_settings=robs_prescription_setting,
        system_generator_settings=system_generator_settings,
        grid_settings=grid_settings,
        general_plot_config=general_plot_config,
        generate_data=generate_data,
        generate_plot=generate_plots,
        generate_data_if_not_present=generate_data_if_not_present,
    )

    ##
    # Variation: 5 core mass shift
    core_shift_settings = {**bse_settings, "PPISN_core_mass_range_shift": -5}
    main(
        main_output_dir=os.path.join(main_dir, "output/output_five_core_mass_shift/"),
        sim_name="five_core_mass_shift",
        bse_settings=core_shift_settings,
        system_generator_settings=system_generator_settings,
        grid_settings=grid_settings,
        general_plot_config=general_plot_config,
        generate_data=generate_data,
        generate_plot=generate_plots,
        generate_data_if_not_present=generate_data_if_not_present,
    )

    ##
    # Variation: 10 solar mass removed
    additional_massloss_settings = {**bse_settings, "PPISN_additional_massloss": 10}
    main(
        main_output_dir=os.path.join(
            main_dir, "output/output_ten_solarmass_extra_removed/"
        ),
        sim_name="ten_solarmass_extra_removed",
        bse_settings=additional_massloss_settings,
        system_generator_settings=system_generator_settings,
        grid_settings=grid_settings,
        general_plot_config=general_plot_config,
        generate_data=generate_data,
        generate_plot=generate_plots,
        generate_data_if_not_present=generate_data_if_not_present,
    )
