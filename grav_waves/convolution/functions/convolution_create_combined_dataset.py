"""
Function to create the combined dataset and add relevant settings to the file
"""

import os
import json
import time
import h5py

import astropy.units as u

from grav_waves.gw_analysis.functions.functions import (
    load_info_dict,
    clean_dco_type_files_and_info_dict,
)
from grav_waves.convolution.functions.convolution_general_functions import (
    JsonCustomEncoder,
)
from grav_waves.convolution.functions.convolution_dataframe_functions import (
    load_dataframes,
    get_dataset_metallicity_info,
    create_combined_df,
)

#
dimensionless = u.m / u.m

####
# Main point
def create_combined_dataset(
    dataset_dict,
    convolution_configuration,
    cosmology_configuration,
    output_filename,
    dco_type="combined",
    limit_merging_time=True,
):
    """
    Function to create the combined dataset hdf5 file

    input:
        dataset_dict: dictionary containing information about the simulation/dataset: sim_name, population_result_dir, population_plot_dir, main_dir, add_ppisn_plots, rebuild. generated with the function make_dataset_dict from grav_waves.gw_analysis.functions.functions
        convolution_configuration: settings for the convolution
        cosmology_configuration; settings for the cosmology: SFR etc
        output_filename: target filename for the hdf5 file containing the combined data
        dco_type: choice of dco type to contain in the combined dataset. choices: 'nsns', 'bhns', 'bhbh', 'combined'. Default='combined' (all dco type)
    """

    #
    convolution_configuration["logger"].info("Started combining the datasets")

    # Clean the DCO type files
    if convolution_configuration["clean_dco_type_files_and_info_dict"]:
        clean_dco_type_files_and_info_dict(
            dataset_dict["population_result_dir"], dry_run=False
        )

    ###########
    # Set up the output file and groups

    # Create the output directory
    if os.path.dirname(output_filename):
        os.makedirs(os.path.dirname(output_filename), exist_ok=True)

    # create the main HDF5 file
    hdf5_file = h5py.File(output_filename, "w")

    # Create groups
    settings_grp = hdf5_file.create_group("settings")
    _ = hdf5_file.create_group("data")

    settings_grp.create_dataset(
        "dataset_dict", data=json.dumps(dataset_dict, cls=JsonCustomEncoder)
    )

    ##################
    # Load dataset info and write all the settings to the output hdf5
    info_dict = load_info_dict(
        dataset_dict["population_result_dir"], rebuild=dataset_dict["rebuild"]
    )
    settings_grp.create_dataset(
        "input_datasets", data=json.dumps(info_dict, cls=JsonCustomEncoder)
    )

    ##################
    # Load dataset population settings and binary_c settings etc and add to settings group
    # TODO: write the population settings to a group

    ##################
    # get metallicity info and add to settings group
    (
        sorted_metallicity_keys,
        sorted_metallicity_keys_type,
        sorted_metallicity_values,
        sorted_log10metallicity_values,
        stepsizes_log10metallicity_values,
        bins_log10metallicity_values,
    ) = get_dataset_metallicity_info(
        info_dict=info_dict,
        type_key=dco_type,
        testing=cosmology_configuration["testing_mode"],
    )
    settings_grp.create_dataset(
        "metallicity_settings",
        data=json.dumps(
            {
                "sorted_metallicity_keys": sorted_metallicity_keys,
                "sorted_metallicity_keys_type": sorted_metallicity_keys_type,
                "sorted_metallicity_values": sorted_metallicity_values,
                "sorted_log10metallicity_values": sorted_log10metallicity_values,
                "stepsizes_log10metallicity_values": stepsizes_log10metallicity_values,
                "bins_log10metallicity_values": bins_log10metallicity_values,
            },
            cls=JsonCustomEncoder,
        ),
    )

    ##################
    # Preload all the individual datasets belonging to this simulation

    # Use a function to load the pandas dataframes into a dict
    convolution_configuration["logger"].info("Loading dataframes")
    start_dataframe = time.time()
    dataframe_dict = load_dataframes(
        info_dict=info_dict,
        convolution_configuration=convolution_configuration,
        dco_type=dco_type,
        limit_merging_time=limit_merging_time,
    )
    convolution_configuration["logger"].info(
        "\ttook {:.2e}s".format(time.time() - start_dataframe)
    )
    start_dataframe = time.time()

    #######################
    # Close hdf5 file to be able to write the pandas stuff in there
    hdf5_file.close()

    ##################
    # Create a combined dataframe of all datasets and add to data group
    convolution_configuration["logger"].info("Combining dataframes and writing to hdf5")
    start_combining = time.time()

    # Create combined dataframe from all individual dataframes
    combined_df = create_combined_df(
        dataframe_dict=dataframe_dict,
        sorted_metallicity_keys_type=sorted_metallicity_keys_type,
        convolution_configuration=convolution_configuration,
    )

    # Reset the index to the 'local_index' TODO: this might be unneccesary now
    reset_combined_df = combined_df.set_index("local_index")
    reset_combined_df["local_index"] = reset_combined_df.index
    reset_combined_df.to_hdf(output_filename, "data/combined_dataframes")

    convolution_configuration["logger"].info(
        "\ttook {:.2e}s".format(time.time() - start_combining)
    )

    #
    convolution_configuration["logger"].info(
        "Finished building the combined dataset for:\n\t{}.\nWrote results to:\n\t{}".format(
            dataset_dict["sim_name"], output_filename
        )
    )
