import shutil

from grav_waves.convolution.functions.convolution_functions import new_convolution_main

from grav_waves.gw_analysis.functions.general_plot_function import (
    general_plot_function as dataset_general_plot_function,
)
from grav_waves.convolution.functions.general_plot_function import (
    general_plot_routine as sfr_convolution_general_plot_function,
)
from grav_waves.convolution.functions.general_plot_function import (
    general_plot_routine_volume_convolved as volume_convolution_general_plot_function,
)
from grav_waves.convolution.functions.volume_convolution_functions import (
    create_redshift_volume_convolution_result_dict,
)
import matplotlib.font_manager

matplotlib.font_manager._rebuild()
