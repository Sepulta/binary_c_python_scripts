"""
Function to add CO core mass to remnant mass to plot
"""

import os
import pandas as pd

import matplotlib as mpl

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.extra_plot_functions import (
    place_arrows,
    add_bracket,
)
from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_utils import (
    get_df,
)
from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_utils import (
    add_pre_SN_mass_plot,
    add_no_prescription_plot,
    add_old_prescription_plot,
    add_fiducial_plot,
    add_mass_removal_plot,
    add_core_mass_shift_plot,
    get_df_bh,
    calculate_min_max_zams_ppisn_masses,
    add_makeup_plot,
    get_dataframes,
)
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def add_co_core_schematic_to_axis(
    fig,
    ax,
    result_directory_variation,
    old_prescription_result_directory_root,
    no_prescription_result_directory_root=None,
    variation_name="mass_removal",
    add_no_prescription_line=False,
    add_old_prescription_line=True,
    add_fiducial_line=True,
    add_variation_line=True,
    add_bracket_extra_mass_loss=True,
    create_shaded_regions=True,
    create_supernova_lines=True,
    dataset_used_for_regions="fiducial",
):
    """
    Function to plot the CO core mass vs the remnant mass schematic plot for the additional mass removal and CO core mass shift
    """

    # Settings
    min_zams_mass = 0
    max_zams_mass = 250
    alpha_regions = 0.2

    # Get metallicity values
    metallicity_value = float(
        os.path.basename(result_directory_variation).split("_Z")[-1]
    )

    ########################
    # Load old prescription data
    old_prescription_df = get_df(
        os.path.join(old_prescription_result_directory_root, "0")
    )
    old_prescription_df = old_prescription_df[old_prescription_df.stellar_type == 14]

    ########################
    # Load fiducial
    fiducial_df = get_df(os.path.join(result_directory_variation, "0"))
    fiducial_df = fiducial_df[fiducial_df.stellar_type == 14]

    ########################
    # Load variation data
    variation_dirs = {
        subdir: os.path.join(result_directory_variation, subdir)
        for subdir in os.listdir(result_directory_variation)
        if not subdir == "0"
    }
    variation_df = get_df(variation_dirs[list(variation_dirs.keys())[0]])
    variation_df = variation_df[variation_df.stellar_type == 14]

    ########################
    # get no-prescription df
    if no_prescription_result_directory_root is not None:
        no_prescription_df = get_df(
            os.path.join(no_prescription_result_directory_root, "0")
        )
        no_prescription_df = no_prescription_df[no_prescription_df.stellar_type == 14]

    ########################
    # Get max value for ppisn
    zams_mass_for_max_ppisn = fiducial_df.loc[fiducial_df["mass"].idxmax()][
        "pre_sn_co_core_mass"
    ]
    fiducial_max_mass_ppisn = fiducial_df[
        fiducial_df.pre_sn_co_core_mass == zams_mass_for_max_ppisn
    ]["mass"].to_numpy()[0]
    mass_removed_max_mass_ppisn = variation_df[
        variation_df.pre_sn_co_core_mass == zams_mass_for_max_ppisn
    ]["mass"].to_numpy()[0]

    ########################
    # Add remnant mass plots
    if add_no_prescription_line:
        ax.plot(
            no_prescription_df["pre_sn_co_core_mass"],
            no_prescription_df["mass"],
            linestyle="-.",
            alpha=0.8,
            label="No PPISN prescription",
        )

    # Add old_prescription line
    if add_old_prescription_line:
        ax.plot(
            old_prescription_df["pre_sn_co_core_mass"],
            old_prescription_df["mass"],
            linestyle="-.",
            alpha=0.8,
            label="Farmer et al. '19 prescription",
        )

    # Add fiducial (i.e. new prescription) line
    if add_fiducial_line:
        ax.plot(
            fiducial_df["pre_sn_co_core_mass"],
            fiducial_df["mass"],
            # 'ro'
            color="black",
            label="New prescription",
        )

        #####
        # Add bracket indicating the extra PPISN loss
        if add_bracket_extra_mass_loss:
            location_max_fiducial_mass = (
                zams_mass_for_max_ppisn,
                fiducial_max_mass_ppisn,
            )
            location_max_mass_removal_mass = (
                zams_mass_for_max_ppisn,
                mass_removed_max_mass_ppisn,
            )
            location_startpoint = (
                zams_mass_for_max_ppisn + 5,
                (fiducial_max_mass_ppisn + mass_removed_max_mass_ppisn) / 2,
            )
            fig, ax = add_bracket(
                fig,
                ax,
                location_max_fiducial_mass,
                location_max_mass_removal_mass,
                location_startpoint,
                -1,
                linestyle="solid",
                color="black",
                linewidth=5,
            )
            ax.text(
                location_startpoint[0] + 1,
                location_startpoint[1] - 1,
                s=r"$\Delta$ $M_{\mathrm{Extra\ mass\ loss}}$",
                fontsize=20,
            )

    # Add variation line
    if add_variation_line:
        ax.plot(
            variation_df["pre_sn_co_core_mass"],
            variation_df["mass"],
            linestyle="--",
            alpha=0.8,
            label=r"New prescription - $\Delta$ $M_{\mathrm{CO\ core\ mass\ shift}}$",
        )

    ##############
    # Create shaded region under the main line that indicates which prescription
    if create_shaded_regions:
        if dataset_used_for_regions == "no_prescription":
            region_dataset = no_prescription_df
        if dataset_used_for_regions == "fiducial":
            region_dataset = fiducial_df
        if dataset_used_for_regions == "old_prescription":
            region_dataset = old_prescription_df
        if dataset_used_for_regions == "variation":
            region_dataset = variation_df

        # Get mass first ppisn
        first_ppisn_mass = max_zams_mass
        if dataset_used_for_regions != "no_prescription":
            first_ppisn_mass = region_dataset[region_dataset.sn_type == 21].iloc[0][
                "zams_mass"
            ]

        #
        fryer_area_df = region_dataset[
            region_dataset.zams_mass < first_ppisn_mass
        ].copy(deep=True)

        #
        ax.fill_between(
            fryer_area_df["pre_sn_co_core_mass"],
            fryer_area_df["mass"],
            y2=0,
            color="yellow",
            alpha=alpha_regions,
        )

        # Plot region of PPISN
        if not dataset_used_for_regions == "no_prescription":
            ax.fill_between(
                region_dataset[region_dataset.zams_mass >= first_ppisn_mass][
                    "pre_sn_co_core_mass"
                ],
                region_dataset[region_dataset.zams_mass >= first_ppisn_mass]["mass"],
                y2=0,
                color="pink",
                alpha=alpha_regions,
            )

        ###########
        # Add lines on the top to indicate which supernova type
        if create_supernova_lines:
            y_value_sn_mechanism_line = fiducial_max_mass_ppisn + 2.5

            #################
            # Plot the CC SN
            min_below_ppisn_zams = fryer_area_df["pre_sn_co_core_mass"].to_numpy().min()
            max_below_ppisn_zams = fryer_area_df["pre_sn_co_core_mass"].to_numpy().max()

            size_between_below_ppisn = max_below_ppisn_zams - min_below_ppisn_zams
            midpoint_below_ppisn = (max_below_ppisn_zams + min_below_ppisn_zams) / 2

            #
            fig, ax = place_arrows(
                fig,
                ax,
                midpoint_below_ppisn,
                y_value_sn_mechanism_line,
                size_between_below_ppisn,
                text="core-collapse",
            )

            #################
            # Plot the PPI SN
            if dataset_used_for_regions != "no_prescription":
                ppisn_area = region_dataset[
                    region_dataset.zams_mass >= first_ppisn_mass
                ][region_dataset.stellar_type == 14]

                min_ppisn_zams = ppisn_area["pre_sn_co_core_mass"].to_numpy().min()
                max_ppisn_zams = ppisn_area["pre_sn_co_core_mass"].to_numpy().max()

                #
                size_between_ppisn = max_ppisn_zams - min_ppisn_zams
                midpoint_ppisn = (max_ppisn_zams + min_ppisn_zams) / 2

                #
                fig, ax = place_arrows(
                    fig,
                    ax,
                    midpoint_ppisn,
                    y_value_sn_mechanism_line,
                    size_between_ppisn,
                    text="pulsational pair-instability + core-collapse",
                )

    #################
    # Make up
    add_makeup_plot(
        ax,
        min_zams_mass,
        max_zams_mass,
        fiducial_max_mass_ppisn,
        metallicity_value,
    )

    return fig, ax
