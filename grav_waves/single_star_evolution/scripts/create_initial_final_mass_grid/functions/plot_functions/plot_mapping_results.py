"""
Function to create all the plots for the mapping at a specific metallicity
"""

import os
import json
import numpy as np
import pandas as pd

from PyPDF2 import PdfFileMerger
import fpdf


import matplotlib.pyplot as plt
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

import matplotlib as mpl

mpl.rcParams["axes.facecolor"] = ".9"
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.utility_functions import (
    fill_missing_values,
    SN_type_dict,
)


from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.pdf_functions import (
    add_pdf_and_bookmark,
    generate_chapterpage_pdf,
)
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def plot_mapping_for_specific_metallicity(df, plot_settings=None):
    """
    Function to handle the mapping plot for a specific metallicity
    """

    if plot_settings is None:
        plot_settings = {}

    #
    df = df.sort_values(by="zams_mass")

    #
    fig = plt.figure(figsize=(30, 20))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=11, ncols=1)

    # Create axis
    ax_zams_to_pre_sn_masses = fig.add_subplot(gs[:2, 0])
    ax_zams_to_remnant_masses = fig.add_subplot(
        gs[2:4, 0], sharex=ax_zams_to_pre_sn_masses
    )
    ax_zams_to_pre_SN_stellar_type = fig.add_subplot(
        gs[4:6, 0], sharex=ax_zams_to_pre_sn_masses
    )
    ax_zams_to_fallback_fraction = fig.add_subplot(
        gs[6:8, 0], sharex=ax_zams_to_pre_sn_masses
    )

    #
    ax_co_core_to_remnant_mass = fig.add_subplot(gs[9:, 0])

    ## ZAMS TO PRE_SN
    ax_zams_to_pre_sn_masses.plot(
        df["zams_mass"], df["pre_sn_mass"], label="Pre-SN mass"
    )
    ax_zams_to_pre_sn_masses.plot(
        df["zams_mass"], df["pre_sn_co_core_mass"], label="Pre-SN CO core mass"
    )
    ax_zams_to_pre_sn_masses.plot(
        df["zams_mass"], df["pre_sn_he_core_mass"], label="Pre-SN He core mass"
    )
    ax_zams_to_pre_sn_masses.legend(loc=2, fontsize=16, framealpha=0.5)
    ax_zams_to_pre_sn_masses.set_ylabel(r"Mass [M$_{\odot}$]")

    # Stellar type pre and Post
    ax_zams_to_pre_SN_stellar_type.scatter(
        df["zams_mass"], df["pre_sn_stellar_type"], label="pre-sn stellar types"
    )
    ax_zams_to_pre_SN_stellar_type.scatter(
        df["zams_mass"], df["stellar_type"], label="post stellar types"
    )
    ax_zams_to_pre_SN_stellar_type.legend(loc=2, fontsize=16, framealpha=0.5)
    ax_zams_to_pre_SN_stellar_type.set_ylabel(r"Stellar type")

    ## ZAMS TO REMNANT
    ax_zams_to_remnant_masses.plot(df["zams_mass"], df["mass"], label="Remnant mass")
    ax_zams_to_remnant_masses.legend(framealpha=0.5, fontsize=15)
    ax_zams_to_remnant_masses.set_ylabel(r"Mass [M$_{\odot}$]")

    ## ZAMS TO FALLBACK FRACTION
    ax_zams_to_fallback_fraction.plot(
        df["zams_mass"], df["fallback_fraction"], label="Fallback fraction"
    )
    ax_zams_to_fallback_fraction.set_xlabel(r"ZAMS Mass [M$_{\odot}$]")
    ax_zams_to_fallback_fraction.set_ylabel(r"Fallback fraction")

    ## CO CORE TO REMNANT
    ax_co_core_to_remnant_mass.scatter(
        df["pre_sn_co_core_mass"], df["mass"], label="Remnant mass"
    )
    ax_co_core_to_remnant_mass.set_xlabel(r"pre-SN CO core Mass [M$_{\odot}$]")
    ax_co_core_to_remnant_mass.set_ylabel(r"Mass [M$_{\odot}$]")

    # handle scale
    if plot_settings["mass_scale"] == "log":
        ax_zams_to_remnant_masses.set_xlim([1, df["zams_mass"].max()])
        ax_zams_to_remnant_masses.set_xlim([1, df["zams_mass"].max()])
        ax_zams_to_pre_SN_stellar_type.set_xlim([1, df["zams_mass"].max()])
        ax_zams_to_fallback_fraction.set_xlim([1, df["zams_mass"].max()])

        ax_zams_to_pre_sn_masses.set_xscale("log")
        ax_zams_to_remnant_masses.set_xscale("log")
        ax_zams_to_pre_SN_stellar_type.set_xscale("log")
        ax_zams_to_fallback_fraction.set_xscale("log")

    #
    ax_zams_to_pre_sn_masses.set_title(
        "Mass mapping for single stars at metallicity Log10(Z) = {}".format(
            np.log10(df["metallicity"].iloc[0])
        )
    )
    plot_settings["hspace"] = 2

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
    plt.close()


def generate_mass_mapping_plots(
    datafile, output_dir, combined_output_filename, plot_settings=None
):
    """
    Function to generate the mass mapping plot
    """

    if not plot_settings:
        plot_settings = {}

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_page_number = 0

    #
    print("Generating plots for dataset in {}".format(datafile))

    #
    pdf_output_dir = os.path.join(output_dir, "pdf")
    os.makedirs(pdf_output_dir, exist_ok=True)

    #
    df = pd.read_table(datafile, sep="\s+")
    df["log10metallicity"] = np.log10(df["metallicity"])
    df = df.sort_values(by=["zams_mass", "log10metallicity"])
    other_columns = [
        col for col in df.columns if not col in ["zams_mass", "metallicity"]
    ]

    # Fill missing values
    df = fill_missing_values(df, other_columns)

    # Loop over all metallicities that we have and make the plots
    for log10metallicity in df["log10metallicity"].unique():
        #
        current_metallicity_df = df[df.log10metallicity == log10metallicity]

        # Sort on zams mass
        current_metallicity_df.sort_values(by=["zams_mass"])

        # pass to function to plot
        if not current_metallicity_df.empty:
            plot_mapping_for_specific_metallicity_filename = os.path.join(
                pdf_output_dir,
                "plot_mapping_for_specific_metallicity_log10Z{}.pdf".format(
                    log10metallicity
                ),
            )
            print("Plotting {}".format(plot_mapping_for_specific_metallicity_filename))
            plot_mapping_for_specific_metallicity(
                current_metallicity_df,
                plot_settings={
                    "show_plot": False,
                    "output_name": plot_mapping_for_specific_metallicity_filename,
                    "simulation_name": plot_settings["simulation_name"],
                    "runname": "plot_mapping_for_specific_metallicity log10Z = {}".format(
                        log10metallicity
                    ),
                    "mass_scale": plot_settings["mass_scale"],
                },
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                plot_mapping_for_specific_metallicity_filename,
                pdf_page_number,
                bookmark_text="Mapping log10Z = {}".format(log10metallicity),
            )

    # wrap up the pdf
    merger.write(combined_output_filename)
    merger.close()

    print(
        "Finished generating plots for dataset {}. Wrote results to {}".format(
            datafile, combined_output_filename
        )
    )


if __name__ == "__main__":
    #
    generate_mass_mapping_plots(
        "../../output/output_vanilla/data/output.dat",
        output_dir="test",
        combined_output_filename="test.pdf",
        plot_settings={"show_plot": True},
    )


# generate_mass_mapping_plot(datafile, metallicity, plot_settings={'show_plot': True, 'output_name': 'test.pdf'})
