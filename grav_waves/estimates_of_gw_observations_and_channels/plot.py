"""
Function to plot the estimates of GW detections and theoretical predictions of GW merger rate for binary black hole systems
"""

import matplotlib.pyplot as plt

from grav_waves.estimates_of_gw_observations_and_channels.data import data_dict

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

#
observation_estimates_bbh_mergers = data_dict["observation_estimates_bbh_mergers"]
