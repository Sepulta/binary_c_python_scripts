"""
Plot function for the metallicity and starformation rate plot
TODO: add custom_mpl here
"""

import copy
import json
import h5py
import numpy as np

import astropy.units as u

import matplotlib.pyplot as plt

from grav_waves.convolution.functions.convolution_functions import (
    generate_metallicity_sfr_array,
)
from grav_waves.convolution.functions.redshift_interpolation_functions import (
    test_lookback_time_to_redshift,
    create_interpolation_datasets,
)
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot

#
number_per_gigaparsec_cubed_per_year = 1 / u.Gpc**3 / u.yr
sfr_unit = u.Msun / u.Gpc**3 / u.yr


def plot_all_rates_and_metallicity_distribution(
    convolution_dataset_hdf5_filename, detailed_plot=False, plot_settings=None
):
    """
    Function to plot the metallicity evolution and the star formation rate as a function of either lookback time or redshift

    detailed_plot: if True we calculate the metallicity + sfr with a resolution based on:
        amt_time_values_sfr_metallicity_plot = config_dict_cosmology['amt_time_values_sfr_metallicity_plot']
        amt_metallicity_values_sfr_metallicity_plot = config_dict_cosmology['amt_metallicity_values_sfr_metallicity_plot']
    """

    if not plot_settings:
        plot_settings = {}

    hdf5_file = h5py.File(convolution_dataset_hdf5_filename, "r")

    # Retrieve info
    settings = hdf5_file["settings"]
    convolution_settings = json.loads(settings["convolution_configuration"][()])
    config_dict_cosmology = json.loads(settings["cosmology_configuration"][()])
    metallicity_data = json.loads(settings["metallicity_settings"][()])
    dataset_dict = json.loads(settings["dataset_dict"][()])
    sfr_data = hdf5_file["sfr_data"]

    # Decide what we're doing. Detailed plot or what we actually used in the data generation.
    if detailed_plot:
        time_centers = np.linspace(
            np.array(convolution_settings["time_centers"]).min(),
            np.array(convolution_settings["time_centers"]).max(),
            config_dict_cosmology["amt_time_values_sfr_metallicity_plot"],
        )

        sorted_log10metallicity_values = np.linspace(
            np.log10(config_dict_cosmology["min_value_metalprob"]),
            np.log10(config_dict_cosmology["max_value_metalprob"]),
            config_dict_cosmology["amt_metallicity_values_sfr_metallicity_plot"],
        )
        # Create hist edges for the log10metallicity values:
        bins_log10metallicity_values = (
            sorted_log10metallicity_values[1:] + sorted_log10metallicity_values[:-1]
        ) / 2

        # Add one to the left and one to the right:
        bins_log10metallicity_values = np.array(
            [
                sorted_log10metallicity_values[0]
                + (sorted_log10metallicity_values[0] - bins_log10metallicity_values[0])
            ]
            + list(bins_log10metallicity_values)
        )
        bins_log10metallicity_values = np.array(
            list(bins_log10metallicity_values)
            + [
                sorted_log10metallicity_values[-1]
                + (
                    sorted_log10metallicity_values[-1]
                    - bins_log10metallicity_values[-1]
                )
            ]
        )

        stepsizes_log10metallicity_values = np.diff(bins_log10metallicity_values)

        # Calculate the new big array
        (
            metallicity_weighted_starformation_array,
            metallicity_fraction_array,
            starformation_array,
        ) = generate_metallicity_sfr_array(
            dataset_dict,
            config_dict_cosmology,
            convolution_settings,
            time_centers=time_centers,
            stepsizes_log10metallicity_values=stepsizes_log10metallicity_values,
            sorted_metallicity_logvalues=sorted_log10metallicity_values,
        )
        metallicity_weighted_starformation_array = (
            metallicity_weighted_starformation_array.T
        )
    else:
        time_centers = convolution_settings["time_centers"]
        sorted_log10metallicity_values = metallicity_data[
            "sorted_log10metallicity_values"
        ]

        starformation_array = sfr_data["starformation_array"][()] * sfr_unit
        metallicity_fraction_array = sfr_data["metallicity_fraction_array"][()]
        metallicity_weighted_starformation_array = (
            sfr_data["metallicity_weighted_starformation_array"][()] * sfr_unit
        )

    # Chop off the two edges
    metallicity_weighted_starformation_array = metallicity_weighted_starformation_array[
        :, 1:-1
    ]
    print(metallicity_weighted_starformation_array.max())

    # Set up the meshgrid that we will use
    time_mesh, metallicity_mesh = np.meshgrid(
        time_centers,
        (10 ** np.array(sorted_log10metallicity_values))
        / config_dict_cosmology["solar_value"],
    )

    #######################
    # Plotting
    # Set up canvas and grid
    fig = plt.figure(figsize=(40, 40))

    gs = fig.add_gridspec(nrows=3, ncols=12)

    ax_rates = fig.add_subplot(gs[0, :-2])
    ax_metallicity = fig.add_subplot(gs[1, :-2], sharex=ax_rates)
    ax_combined = fig.add_subplot(gs[2, :-2], sharex=ax_rates)

    ax_metallicity_cb = fig.add_subplot(gs[1, -1])
    ax_combined_cb = fig.add_subplot(gs[2, -1])

    ########################################
    # 1: Plot all rates
    sfr_rate_density = ax_rates.plot(
        time_centers, starformation_array, label="Star formation rate"
    )
    ax_rates_copy = ax_rates.twinx()

    #
    ax_rates.axvline(
        x=time_centers[np.argmax(starformation_array)],
        ymax=starformation_array.max().value,
        color=sfr_rate_density[0].get_color(),
        linestyle="--",
        linewidth=1,
        alpha=0.5,
        label="Peak SFR rate density location",
    )

    # get bins
    timebins = np.array(convolution_settings["time_bins"])
    time_values = (timebins[1:] + timebins[:-1]) / 2

    # # Plot merger rate density results
    # any_mass_merger_rate_results = result_dataset['results']['merger_rates']['result_array_any_mass']
    # any_mass_summed_merger_rate_results = np.sum(any_mass_merger_rate_results, axis=1)
    # merger_rate_density_lines = ax_rates_copy.plot(time_values, any_mass_summed_merger_rate_results, label='Merger rate density', alpha=0.5)

    # #
    # ax_rates_copy.axvline(
    #     x=time_values[np.argmax(any_mass_summed_merger_rate_results)],
    #     ymax=any_mass_summed_merger_rate_results.max().value,
    #     color=merger_rate_density_lines[0].get_color(),
    #     linestyle='--',
    #     linewidth=1,
    #     alpha=0.5,
    #     label='peak merger rate density location'
    # )

    # # Plot formation rate density results
    # if convolution_settings['include_formation_rates']:
    #     any_mass_formation_rate_results = result_dataset['results']['formation_rates']['result_array_any_mass']
    #     any_mass_summed_formation_rate_results = np.sum(any_mass_formation_rate_results, axis=1)
    #     formation_rate_density_lines = ax_rates_copy.plot(
    #         time_values,
    #         any_mass_summed_formation_rate_results,
    #         label='Formation rate density',
    #         alpha=0.5
    #     )

    #     #
    #     ax_rates_copy.axvline(
    #         x=time_values[np.argmax(any_mass_summed_formation_rate_results)],
    #         ymax=any_mass_summed_formation_rate_results.max().value,
    #         color=formation_rate_density_lines[0].get_color(),
    #         linestyle='--',
    #         linewidth=1,
    #         alpha=0.5,
    #         label='peak formation rate density location'
    #     )

    # ########################################
    # # 2: Plot with metallicity evolution

    cmap_metallicity_fraction_hist = copy.copy(plt.cm.jet)
    cmap_metallicity_fraction_hist.set_under(color="white")
    metallicity_fraction_hist_plot = ax_metallicity.pcolormesh(
        time_mesh,
        metallicity_mesh,
        metallicity_fraction_array.T,
        norm=colors.Normalize(vmin=1e-8, vmax=1),
        cmap=cmap_metallicity_fraction_hist,
        shading="auto",
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )
    ax_metallicity.set_ylim(1e-5, 2e1)
    ax_metallicity.set_yscale("log")

    # # Add metallicity lines
    # for dataset_metallicity in data_info['metallicity_values']:
    #     ax_metallicity.plot(
    #         lookbacktime_array,
    #         [dataset_metallicity/config_dict_cosmology['solar_value'] for el in range(len(lookbacktime_array))],
    #         '-.',
    #         color='green',
    #         alpha=0.25,
    #         linewidth=1
    #     )

    fig.colorbar(metallicity_fraction_hist_plot, cax=ax_metallicity_cb)

    # Copy and show how much we cover
    ax_metallicity_copy = ax_metallicity.twinx()
    ax_metallicity_copy.plot(
        time_centers,
        np.sum(metallicity_fraction_array, axis=1),
        label="summed metallicity fraction",
    )
    ax_metallicity_copy.set_xlim(ax_rates.get_xlim())
    ax_metallicity_copy.legend()

    # ########################################
    # # 3: Contour plot with the sfr and the metallicity combined

    cmap_combined_plot = copy.copy(plt.cm.jet)
    cmap_combined_plot.set_under(color="white")
    combined_plot = ax_combined.pcolormesh(
        time_mesh,
        metallicity_mesh,
        metallicity_weighted_starformation_array.value,
        norm=colors.Normalize(
            vmin=metallicity_weighted_starformation_array[
                metallicity_weighted_starformation_array != 0
            ].value.min(),
            vmax=metallicity_weighted_starformation_array.value.max(),
        ),
        cmap=cmap_combined_plot,
        shading="auto",
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )
    ax_combined.set_ylim(1e-5, 2e1)
    ax_combined.set_yscale("log")
    fig.colorbar(combined_plot, cax=ax_combined_cb)

    # # Add metallicity lines
    # for dataset_metallicity in data_info['metallicity_values']:
    #     ax_combined.plot(
    #         lookbacktime_array,
    #         [dataset_metallicity/config_dict_cosmology['solar_value'] for el in range(len(lookbacktime_array))],
    #         '-.',
    #         color='green',
    #         alpha=0.25,
    #         linewidth=1
    #     )

    # Make up
    fontsize = 16

    #
    ax_metallicity.set_yscale("log")
    ax_metallicity.set_ylabel(r"Log10 Metallicity [Z$_{\odot}$]", fontsize=fontsize)
    ax_metallicity.set_xlim(ax_rates.get_xlim())
    ax_metallicity.set_ylim(1e-5, 2e1)

    ax_combined.set_ylabel(r"Log10 Metallicity [Z$_{\odot}$]", fontsize=fontsize)
    ax_combined.set_xlim(ax_rates.get_xlim())

    ax_metallicity.set_title("Metallicity distribution at redshifts", fontsize=fontsize)
    ax_combined.set_xlabel("Redshift")
    ax_combined.set_title(
        "Metallicity weighted star formation rate density at\nredshifts",
        fontsize=fontsize,
    )
    ax_rates.set_title("Intrinsic rate densities at redshifts", fontsize=fontsize)

    #
    ax_rates.set_ylabel(
        r"d$^{2}$M$_{\mathcal{SFR}}$/dt$_{s}$dV$_{c}$"
        + "\n[%s]" % starformation_array.unit,
        fontsize=fontsize,
    )
    ax_rates.legend(loc=2, framealpha=0.5, fontsize=12)

    #
    ax_rates_copy.set_ylabel(
        r"Intrinsic event rate density"
        + "\n[%s]" % (starformation_array / u.Msun).unit,
        fontsize=fontsize,
    )
    ax_rates_copy.legend(loc=6, framealpha=0.5, fontsize=12)

    # Override some settings for this specific plot
    plot_settings["hspace"] = 0.5

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
