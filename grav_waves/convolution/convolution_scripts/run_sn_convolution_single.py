"""
Main function to handle the convolution of the single star SN data
"""

import os
import logging

import numpy as np

from grav_waves.settings import convolution_settings, config_dict_cosmology

from grav_waves.gw_analysis.functions.functions import make_dataset_dict

from grav_waves.convolution.scripts.convolve_SN_rate_single.functions import (
    create_combined_single_sn_dataset,
)
from grav_waves.convolution.scripts.convolve_SN_rate.functions import (
    filter_combined_sn_dataset_function,
)

from grav_waves.convolution.functions.convolution_functions import convolution_main


#
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def run_single_sn_convolution(
    main_dir,
    sn_convolution_output_dir,
    convolution_settings,
    cosmology_settings,
    run_combine_datasets=False,
    run_filter_combined_dataset=True,
    filter_combined_dataset_function=None,
    run_convolution=True,
    verbosity=1,
):
    """
    Function to handle all the steps for the convolution of the SN datasets
    """

    # Get the correct directories
    dataset_dict_single_sn = make_dataset_dict(main_dir)
    os.makedirs(sn_convolution_output_dir, exist_ok=True)

    # Create names for the output files
    combined_single_sn_dataset_filename = os.path.join(
        sn_convolution_output_dir, "combined_single_sn_dataset.h5"
    )
    filtered_combined_single_sn_dataset_filename = os.path.join(
        sn_convolution_output_dir, "filtered_combined_single_sn_dataset.h5"
    )
    redshift_single_sn_convolution_filename = os.path.join(
        sn_convolution_output_dir, "single_sn_convolution_results.h5"
    )

    ##############
    # Build input dataset
    used_combined_single_sn_dataset_filename = combined_single_sn_dataset_filename
    if run_combine_datasets:
        create_combined_single_sn_dataset(
            dataset_dict=dataset_dict_single_sn,
            convolution_configuration=convolution_settings,
            cosmology_configuration=config_dict_cosmology,
            output_filename=used_combined_single_sn_dataset_filename,
        )

    ##############
    # Build combined dataset merging systems
    if run_filter_combined_dataset:
        filter_combined_dataset_function(
            input_hdf5file=used_combined_single_sn_dataset_filename,
            output_hdf5file=filtered_combined_single_sn_dataset_filename,
        )
        used_combined_single_sn_dataset_filename = (
            filtered_combined_single_sn_dataset_filename
        )

    ##############
    # Run convolution
    if run_convolution:
        convolution_main(
            input_filename=used_combined_single_sn_dataset_filename,
            convolution_configuration=convolution_settings,
            cosmology_configuration=cosmology_settings,
            output_filename=redshift_single_sn_convolution_filename,
            verbosity=verbosity,
        )


#
if __name__ == "__main__":
    ##
    #
    output_root = os.path.join(this_file_dir)

    #######################
    # Select simulation

    # Fiducial
    main_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/NEW_LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED"
    single_sn_convolution_output_dir = os.path.join(
        main_dir, "single_sn_convolution_results/"
    )

    #
    convolution_stepsize = 0.1
    convolution_settings["stepsize"] = convolution_stepsize
    convolution_settings["time_bins"] = np.arange(
        1e-6 - 0.5 * convolution_stepsize,
        (convolution_settings["max_interpolation_redshift"] - 1)
        + 0.5 * convolution_stepsize,
        convolution_stepsize,
    )
    convolution_settings["time_centers"] = (
        convolution_settings["time_bins"][1:] + convolution_settings["time_bins"][:-1]
    ) / 2
    convolution_settings["amt_cores"] = 1
    convolution_settings["convolution_tmp_dir"] = os.path.join(
        single_sn_convolution_output_dir, "tmp"
    )

    convolution_settings["include_birth_rates"] = False
    convolution_settings["include_formation_rates"] = True
    convolution_settings["include_merger_rates"] = False
    convolution_settings["include_detection_rates"] = False

    convolution_settings["combined_data_array_columns"] = [
        "number_per_solar_mass_values",
        "metallicity",
    ]
    convolution_settings["logger"].setLevel(logging.DEBUG)

    #
    run_single_sn_convolution(
        main_dir=main_dir,
        sn_convolution_output_dir=single_sn_convolution_output_dir,
        convolution_settings=convolution_settings,
        cosmology_settings=config_dict_cosmology,
        run_combine_datasets=True,
        run_filter_combined_dataset=True,
        filter_combined_dataset_function=filter_combined_sn_dataset_function,
        run_convolution=True,
        verbosity=0,
    )
