import h5py
import json
import pandas as pd
import numpy as np
import os

import matplotlib.pyplot as plt

from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.grav_waves.merger_rate_functions import *

from binarycpython.utils.stellar_types import stellar_type_dict_short

custom_mpl_settings.load_mpl_rc()

data_dir = os.path.join(os.environ["BINARYC_DATA_ROOT"], "testing_python", "BHMS")
datafile = os.path.join(data_dir, "BH_MS_z0.002.hdf5")


def load_file_return_df(filename, dataset_name):
    # Load files
    # file_name = '/home/david/projects/binary_c_root/results/testing_python/supernova_mass_test/SN_types_z0.002.hdf5'
    f = h5py.File(filename, "r")

    # Show some keys
    # for key in f.keys():
    #     print(key) #Names of the groups in HDF5 file.

    for key in f["data"].keys():
        print(key)

    # for key in f['settings'].keys():
    #     print(key)

    # Load data and put in dataframe
    data = f["data"][dataset_name][()]
    # settings = f['settings']['used_settings'][()]
    # settings_json = json.loads(settings)

    headers = f["data"][dataset_name + "_header"][()]
    headers = [header.decode("utf-8") for header in headers]

    # metallicity = json.loads(settings)['binary_c_defaults']['metallicity']

    # Make df
    df = pd.DataFrame(data)
    df.columns = headers

    # for i in range(16):
    #     df['f_st_{}'.format(i)] = df['st_{}'.format(i)] / df['zams_mass']

    # df = df.sort_values(by=['zams_mass'])

    return df


df = load_file_return_df(datafile, "BH_MS_z0.002")

calculate_mergerrate_per_myr_and_rvol(df, "dingdong", amt_stars=10000)
