"""
Script to plot the results for mathieus requested sequence of plots

Mathieu asked for the following sequence of plots:
- [X] 1) empty plot with pre-sn mass only
- [X] 2) pre-sn mass and remnant mass using no ppisn prescription
- [X] 3) pre-sn mass and remnant mass using old ppisn prescription
- [X] 4) pre-sn mass and remnant mass using old ppisn prescription and new ppisn prescription
- [X] 5) pre-sn mass and remnant mass using old ppisn prescription, new ppisn prescription, and one of the variations

I will do this for the ZAMS to remnant mass
"""

import os

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_zams_mass_schematic import (
    plot_zams_mass_schematic,
)
from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_zams_mass_schematic_generalised import (
    plot_zams_mass_schematic_generalised,
)

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_co_core_schematic_generalised import (
    plot_co_core_schematic_generalised,
)


################
# Settings
plot_zams_to_remnant_mass_plots = False  # I
plot_CO_core_to_remnant_mass_plots = True  # II

#
plot_pre_sn_mass_plot = True  # 1
plot_no_prescription_plot = True  # 2

plot_old_prescription_plot = True  # 3
plot_new_prescription_plot = True  # 4

plot_mass_removal_variation_plot = True  # 5
plot_core_mass_shift_variation_plot = True  # 6

################
#
this_dir = os.path.dirname(__file__)

# Set the root dirs
result_root = os.path.join(this_dir, "results")
plot_root = os.path.join(this_dir, "plots")
os.makedirs(plot_root, exist_ok=True)

# Get the specific result directories
core_mass_shift_result_dir = os.path.join(
    result_root,
    "core_mass_shift/single_mass_removal/WIND_ALGORITHM_SCHNEIDER2018_PPISN_NEW_FIT_21_Z0.001",
)
mass_removal_result_dir = os.path.join(
    result_root,
    "mass_removal/single_mass_removal/WIND_ALGORITHM_SCHNEIDER2018_PPISN_NEW_FIT_21_Z0.001",
)
no_ppisn_prescription_result_dir = os.path.join(
    result_root,
    "no_ppisn_prescription/single_mass_removal/WIND_ALGORITHM_SCHNEIDER2018_PPISN_NONE_Z0.001",
)
old_prescription_result_dir = os.path.join(
    result_root,
    "old_prescription/single_mass_removal/WIND_ALGORITHM_SCHNEIDER2018_PPISN_FARMER19_Z0.001",
)

################
# Plotting

# Plots with ZAMS to remnant mass
if plot_zams_to_remnant_mass_plots:
    if plot_pre_sn_mass_plot:
        plot_zams_mass_schematic(
            result_directory_mass_removal=mass_removal_result_dir,
            old_prescription_result_directory_root=old_prescription_result_dir,
            add_old_prescription_line=False,
            add_mass_removed_mass_removal=False,
            add_preSN_mass_mass_removal=True,
            add_fiducial_mass_removal=False,
            add_bracket_extra_mass_loss=False,
            create_shaded_regions=False,
            create_supernova_lines=False,
            plot_settings={
                "output_name": os.path.join(plot_root, "zams_pre_sn_mass_plot.pdf")
            },
        )
    if plot_no_prescription_plot:
        plot_zams_mass_schematic(
            result_directory_mass_removal=mass_removal_result_dir,
            old_prescription_result_directory_root=old_prescription_result_dir,
            no_prescription_result_directory_root=no_ppisn_prescription_result_dir,
            add_no_prescription_line=True,
            add_old_prescription_line=False,
            add_mass_removed_mass_removal=False,
            add_preSN_mass_mass_removal=True,
            add_fiducial_mass_removal=False,
            add_bracket_extra_mass_loss=False,
            create_shaded_regions=True,
            create_supernova_lines=True,
            dataset_used_for_regions="no_prescription",
            plot_settings={
                "output_name": os.path.join(
                    plot_root, "zams_no_prescription_remnant_mass_plot.pdf"
                )
            },
        )
    if plot_old_prescription_plot:
        plot_zams_mass_schematic(
            result_directory_mass_removal=mass_removal_result_dir,
            old_prescription_result_directory_root=old_prescription_result_dir,
            add_old_prescription_line=True,
            add_mass_removed_mass_removal=False,
            add_preSN_mass_mass_removal=True,
            add_fiducial_mass_removal=False,
            add_bracket_extra_mass_loss=False,
            create_shaded_regions=True,
            create_supernova_lines=True,
            dataset_used_for_regions="old_prescription",
            plot_settings={
                "output_name": os.path.join(
                    plot_root, "zams_old_prescription_remnant_mass_plot.pdf"
                )
            },
        )
    if plot_new_prescription_plot:
        plot_zams_mass_schematic(
            result_directory_mass_removal=mass_removal_result_dir,
            old_prescription_result_directory_root=old_prescription_result_dir,
            add_old_prescription_line=True,
            add_mass_removed_mass_removal=False,
            add_preSN_mass_mass_removal=True,
            add_fiducial_mass_removal=True,
            add_bracket_extra_mass_loss=True,
            create_shaded_regions=True,
            create_supernova_lines=True,
            dataset_used_for_regions="fiducial",
            plot_settings={
                "output_name": os.path.join(
                    plot_root, "zams_new_prescription_remnant_mass_plot.pdf"
                )
            },
        )
    if plot_mass_removal_variation_plot:
        plot_zams_mass_schematic(
            result_directory_mass_removal=mass_removal_result_dir,
            old_prescription_result_directory_root=old_prescription_result_dir,
            add_old_prescription_line=True,
            add_mass_removed_mass_removal=True,
            add_preSN_mass_mass_removal=True,
            add_fiducial_mass_removal=True,
            add_bracket_extra_mass_loss=False,
            create_shaded_regions=True,
            create_supernova_lines=True,
            dataset_used_for_regions="variation",
            plot_settings={
                "output_name": os.path.join(
                    plot_root, "zams_mass_removal_remnant_mass_plot.pdf"
                )
            },
        )
    if plot_core_mass_shift_variation_plot:
        plot_zams_mass_schematic_generalised(
            result_directory_variation=core_mass_shift_result_dir,
            old_prescription_result_directory_root=old_prescription_result_dir,
            variation_name="core_mass_shift",
            add_old_prescription_line=True,
            add_variation_line=True,
            add_preSN_mass_mass_removal=True,
            add_fiducial_line=True,
            add_bracket_extra_mass_loss=True,
            create_shaded_regions=True,
            create_supernova_lines=True,
            dataset_used_for_regions="variation",
            plot_settings={
                "output_name": os.path.join(
                    plot_root, "core_shift_remnant_mass_plot.pdf"
                )
            },
        )
if plot_CO_core_to_remnant_mass_plots:
    if plot_no_prescription_plot:
        plot_co_core_schematic_generalised(
            result_directory_variation=mass_removal_result_dir,
            old_prescription_result_directory_root=old_prescription_result_dir,
            no_prescription_result_directory_root=no_ppisn_prescription_result_dir,
            variation_name="mass_removal",
            add_no_prescription_line=True,
            add_old_prescription_line=False,
            add_variation_line=False,
            add_fiducial_line=False,
            add_bracket_extra_mass_loss=False,
            create_shaded_regions=True,
            create_supernova_lines=True,
            dataset_used_for_regions="no_prescription",
            plot_settings={
                "output_name": os.path.join(
                    plot_root, "CO_core_no_prescription_plot.pdf"
                )
            },
        )
    if plot_old_prescription_plot:
        plot_co_core_schematic_generalised(
            result_directory_variation=mass_removal_result_dir,
            old_prescription_result_directory_root=old_prescription_result_dir,
            no_prescription_result_directory_root=no_ppisn_prescription_result_dir,
            variation_name="mass_removal",
            add_no_prescription_line=False,
            add_old_prescription_line=True,
            add_variation_line=False,
            add_fiducial_line=False,
            add_bracket_extra_mass_loss=False,
            create_shaded_regions=True,
            create_supernova_lines=True,
            dataset_used_for_regions="old_prescription",
            plot_settings={
                "output_name": os.path.join(
                    plot_root, "CO_core_old_prescription_plot.pdf"
                )
            },
        )
    if plot_new_prescription_plot:
        plot_co_core_schematic_generalised(
            result_directory_variation=mass_removal_result_dir,
            old_prescription_result_directory_root=old_prescription_result_dir,
            no_prescription_result_directory_root=no_ppisn_prescription_result_dir,
            variation_name="mass_removal",
            add_no_prescription_line=False,
            add_old_prescription_line=True,
            add_fiducial_line=True,
            add_variation_line=False,
            add_bracket_extra_mass_loss=False,
            create_shaded_regions=True,
            create_supernova_lines=True,
            dataset_used_for_regions="fiducial",
            plot_settings={
                "output_name": os.path.join(
                    plot_root, "CO_core_new_prescription_plot.pdf"
                )
            },
        )
    if plot_mass_removal_variation_plot:
        plot_co_core_schematic_generalised(
            result_directory_variation=mass_removal_result_dir,
            old_prescription_result_directory_root=old_prescription_result_dir,
            no_prescription_result_directory_root=no_ppisn_prescription_result_dir,
            variation_name="mass_removal",
            add_no_prescription_line=False,
            add_old_prescription_line=True,
            add_fiducial_line=True,
            add_variation_line=True,
            add_bracket_extra_mass_loss=False,
            create_shaded_regions=True,
            create_supernova_lines=False,
            dataset_used_for_regions="variation",
            plot_settings={
                "output_name": os.path.join(plot_root, "CO_core_mass_removal_plot.pdf")
            },
        )
    if plot_core_mass_shift_variation_plot:
        plot_co_core_schematic_generalised(
            result_directory_variation=core_mass_shift_result_dir,
            old_prescription_result_directory_root=old_prescription_result_dir,
            no_prescription_result_directory_root=no_ppisn_prescription_result_dir,
            variation_name="core_mass_shift",
            add_no_prescription_line=False,
            add_old_prescription_line=True,
            add_fiducial_line=True,
            add_variation_line=True,
            add_bracket_extra_mass_loss=False,
            create_shaded_regions=True,
            create_supernova_lines=True,
            dataset_used_for_regions="variation",
            plot_settings={
                "output_name": os.path.join(
                    plot_root, "CO_core_core_mass_shift_plot.pdf"
                )
            },
        )
