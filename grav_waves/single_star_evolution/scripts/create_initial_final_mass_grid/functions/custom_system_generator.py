"""
Custom grid generator function
"""

import numpy as np


def custom_system_generator(system_generator_settings, physics_settings):
    """
    Custom grid generator function
    """

    #
    if physics_settings is None:
        physics_settings = {}

    #
    mass_array = np.linspace(
        system_generator_settings["min_mass"],
        system_generator_settings["max_mass"],
        system_generator_settings["mass_resolution"],
    )
    log10metallicity_array = np.linspace(
        system_generator_settings["min_log10_metallicity"],
        system_generator_settings["max_log10_metallicity"],
        system_generator_settings["log10metallicity_resolution"],
    )

    for mass in mass_array:
        for log10metallicity in log10metallicity_array:
            system_dict = {
                "M_1": mass,
                "metallicity": 10**log10metallicity,
                "effective_metallicity": 10**log10metallicity,
            }

            system_dict.update(physics_settings)

            yield system_dict

    # Handle zero metallicity systems as well.
    if system_generator_settings["include_zero_metallicity"]:
        for mass in mass_array:
            system_dict = {
                "M_1": mass,
                "metallicity": 10 ** system_generator_settings["min_log10_metallicity"],
                "effective_metallicity": 0,
            }

            system_dict.update(physics_settings)

            yield system_dict
