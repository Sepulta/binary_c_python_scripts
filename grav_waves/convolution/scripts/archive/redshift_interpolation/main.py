"""
Test script for interpolation
"""


import os
import time
import copy
import pickle
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors


import astropy
import astropy.units as u
import astropy.constants as const

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot


from grav_waves.gw_analysis.functions.cosmology_functions import (
    starformation_rate,
    mean_metallicity,
    create_metallicity_redshift_dataframe,
    create_metallicity_lookback_dataframe,
)
from grav_waves.settings import convolution_settings
from grav_waves.gw_analysis.functions.cosmology_functions import (
    redshift_to_lookback_time,
)
from grav_waves.settings import cosmo
from grav_waves.convolution.functions.convolution_functions import (
    generate_metallicity_sfr_array,
)
from grav_waves.convolution.functions.redshift_interpolation_functions import (
    test_redshift_to_lookback_time,
    test_lookback_time_to_redshift,
    create_interpolation_datasets,
)


# Set global settings for the tests
convolution_dict = {}

convolution_dict["redshift_interpolation_stepsize"] = 0.00001
convolution_dict["min_interpolation_redshift"] = 0
convolution_dict["max_interpolation_redshift"] = 10
convolution_dict["interpolator_data_output_filename"] = os.path.abspath(
    "interpolator_data_dict.p"
)
convolution_dict[
    "min_redshift_change_if_log_sampling"
] = 1e-5  # If the redshift is 0 and we have log sampling then we need to shift it
convolution_dict["log_redshift_interpolation"] = True
convolution_dict["rebuild_interpolation_data"] = False
convolution_dict["rebuild_interpolation_data_when_settings_not_match"] = True

print("The range of times we consider are:")
print(
    "min_redshift: {} min_lookback_time: {}".format(
        convolution_dict["min_interpolation_redshift"],
        redshift_to_lookback_time(convolution_dict["min_interpolation_redshift"]),
    )
)
print(
    "max_redshift: {} max_lookback_time: {}".format(
        convolution_dict["max_interpolation_redshift"],
        redshift_to_lookback_time(convolution_dict["max_interpolation_redshift"]),
    )
)

# Generate data both ways:
# create_interpolation_datasets(
#     interpolator_data_output_filename,
#     min_redshift,
#     max_redshift,
#     redshift_stepsize
# )


def plot_interpolation_datasets(
    convolution_dict,
    size_test_samples,
    plot_log=False,
    test_log=False,
    plot_settings=None,
):
    """
    Function to plot the datasets that are used in the interpolation
    """

    if plot_settings is None:
        plot_settings = {}

    # Rebuild datasets if wanted:
    if convolution_dict["rebuild_interpolation_data"]:
        create_interpolation_datasets(convolution_dict)

    # Generate test data for redshift to lookback time
    test_results_redshift_to_lookback_time = test_redshift_to_lookback_time(
        convolution_dict, size_test_samples, test_log=test_log
    )

    # Generate test data for redshift to lookback time
    test_results_lookback_time_to_redshift = test_lookback_time_to_redshift(
        convolution_dict, size_test_samples, test_log=test_log
    )

    # Load interpolation data dict
    interpolation_data_dict = pickle.load(
        open(convolution_dict["interpolator_data_output_filename"], "rb")
    )

    # Open datafiles
    redshift_data = interpolation_data_dict["redshift_data"]
    lookback_time_data = interpolation_data_dict["lookback_time_data"].value

    ####
    # Plotting
    fig = plt.figure(figsize=(20, 10))
    fig.subplots_adjust(hspace=0.5)

    gs = fig.add_gridspec(nrows=2, ncols=2)

    # Create subplots
    ax_redshift_to_time = fig.add_subplot(gs[0, 0])
    ax_redshift_to_time_tests = fig.add_subplot(gs[1, 0], sharex=ax_redshift_to_time)

    ax_time_to_redshift = fig.add_subplot(gs[0, 1])
    ax_time_to_redshift_tests = fig.add_subplot(gs[1, 1], sharex=ax_time_to_redshift)

    # Redshift to time
    ax_redshift_to_time.scatter(redshift_data, lookback_time_data)
    ax_redshift_to_time.set_xlabel("Redshift")
    ax_redshift_to_time.set_ylabel("Lookback time")
    ax_redshift_to_time.set_title("Redshift to lookback time")
    ax_redshift_to_time.set_xlim(
        [redshift_data[redshift_data > 0].min(), redshift_data[redshift_data > 0].max()]
    )
    ax_redshift_to_time.set_ylim(
        [
            lookback_time_data[lookback_time_data > 0].min(),
            lookback_time_data[lookback_time_data > 0].max(),
        ]
    )

    # Lookback time to redshift
    ax_time_to_redshift.scatter(lookback_time_data, redshift_data)
    ax_time_to_redshift.set_xlabel("Lookback time")
    ax_time_to_redshift.set_ylabel("Redshift")
    ax_time_to_redshift.set_title("Redshift to lookback time inverted")
    ax_time_to_redshift.set_xlim(
        [
            lookback_time_data[lookback_time_data > 0].min(),
            lookback_time_data[lookback_time_data > 0].max(),
        ]
    )
    ax_time_to_redshift.set_ylim(
        [redshift_data[redshift_data > 0].min(), redshift_data[redshift_data > 0].max()]
    )

    # Redshift to time errors
    ax_redshift_to_time_tests.scatter(
        test_results_redshift_to_lookback_time["redshift_sample"],
        test_results_redshift_to_lookback_time["fractional_error"],
    )
    ax_redshift_to_time_tests.set_yscale("log")
    ax_redshift_to_time_tests.set_ylim(
        test_results_redshift_to_lookback_time["fractional_error"][
            test_results_redshift_to_lookback_time["fractional_error"] > 1e-30
        ].min(),
        test_results_redshift_to_lookback_time["fractional_error"].max(),
    )
    ax_redshift_to_time_tests.set_xlabel("Redshift test sample")
    ax_redshift_to_time_tests.set_ylabel("Fractional error\n(True-interpolated)/True")
    ax_redshift_to_time_tests.set_title("Redshift to time interpolation test")

    # Redshift to time errors
    ax_time_to_redshift_tests.scatter(
        test_results_lookback_time_to_redshift["lookback_times_sample"],
        test_results_lookback_time_to_redshift["fractional_error"],
    )
    ax_time_to_redshift_tests.set_yscale("log")
    ax_time_to_redshift_tests.set_ylim(
        test_results_lookback_time_to_redshift["fractional_error"][
            test_results_lookback_time_to_redshift["fractional_error"] > 1e-30
        ].min(),
        test_results_lookback_time_to_redshift["fractional_error"].max(),
    )
    ax_time_to_redshift_tests.set_xlabel("Lookback time test sample")
    ax_time_to_redshift_tests.set_ylabel("Fractional error\n(True-interpolated)/True")
    ax_time_to_redshift_tests.set_title("Lookback time to redshift interpolation test")

    if plot_log:
        ax_redshift_to_time.set_xscale("log")
        ax_time_to_redshift.set_xscale("log")

        ax_redshift_to_time.set_yscale("log")
        ax_time_to_redshift.set_yscale("log")

        ax_redshift_to_time_tests.set_xscale("log")
        ax_time_to_redshift_tests.set_xscale("log")
    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


# # Plot the interpolation datasets and show the interpolation accuracy
# plot_interpolation_datasets(
#     convolution_dict,
#     size_test_samples=200,
#     plot_log=True,
#     test_log=True,
#     plot_settings={'show_plot': True}
# )
