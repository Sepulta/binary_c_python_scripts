import os
import time
import copy
import json

import numpy as np

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

from binarycpython.utils.stellar_types import STELLAR_TYPE_DICT_SHORT
from binarycpython.utils.functions import merge_dicts

from functions import (
    getFromDict,
    get_recursive_sum,
    flatten_data_ensemble2d,
    flatten_data_ensemble1d,
    generate_bins_from_keys,
    return_float_list_of_dictkeys,
    merge_all_subdicts,
    return_string,
    combine_or_filter_stellar_types,
    recursively_merge_all_subdicts,
    return_keys_bins_and_bincenters,
)

from RLOF_logging.fractions_separation_massratio.comparison_specific_angmoms.functions import (
    ratio_disk_iso,
)

from david_phd_functions.masstransfer.functions import (
    rochelobe_radius_accretor,
    circularisation_radius,
    minimum_radius,
)
