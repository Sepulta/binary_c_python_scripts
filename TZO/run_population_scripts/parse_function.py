"""
Parse function for the TZO project
"""

import os

from binarycpython.utils.functions import output_lines


def check_length(values, parameters, offset=0):
    """
    Function to check the length of the output versus the number of parameters we have.

    Optionally we can give an offset value for info we add but which is not part of the output of binary_c
    """

    # Check if the length of the output is the same as we expect
    if not len(values) == len(parameters) - offset:
        print(
            "Length of readout values ({}) is not equal to length of parameters ({}){}".format(
                len(values),
                len(parameters),
                " with offset {}".format(offset) if offset else "",
            )
        )
        raise ValueError


def parse_function(self, output):
    """
    Parse function for binary compact objects
    """

    # Get some information from the object
    data_dir = self.custom_options["data_dir"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # Set outfile name
    outfilename = os.path.join(data_dir, "tzo_events-{}.dat".format(self.process_ID))

    # Create filename
    parameters = [
        #
        "time",  # 1: time
        # Current system information
        "mass_1",
        "mass_2",  # 2-3: current masses
        "stellar_type_1",
        "stellar_type_2",  # 4-5: current stellar types
        # Previous model information
        "prev_eccentricity",
        "prev_period",
        "prev_separation",  # 6-8: previous orbit parameters
        "prev_mass_1",
        "prev_mass_2",  # 9-10: previous masses
        "prev_stellar_type_1",
        "prev_stellar_type_2",  # 11-12: previous stellar types
        "prev_core_mass_1",
        "prev_core_mass_2",  # 13-14: previous core masses
        # ZAMS information
        "zams_mass_1",
        "zams_mass_2",  # 15-16: ZAMS masses
        "zams_period",
        "zams_separation",
        "zams_eccentricity",  # 17-19: ZAMS orbit parameters
        # general model information
        "metallicity",
        "probability",
        "random_seed",  # 20-22: model info
        # Extra TZO information (not sure if fully reliable)
        "TZ_mass",
        "TZ_NS_mass",
        "TZ_channel",
        "TZ_starnumber",  # 23-26: extra TZO informatio
        # collision info
        "collision",  # 27: collision info
        # RLOF info
        "total_rlofs",
        "unstable_rlofs",
        "stable_rlofs",  # 28-30: RLOF info
    ]

    #
    separator = "\t"

    # Check if the file exists
    if not os.path.exists(outfilename):
        with open(outfilename, "w") as f:
            f.write(separator.join(parameters) + "\n")

    # Loop over the output
    for line in output_lines(output):
        if line.startswith("DAVID_TZO"):
            values = line.split()[1:]

            # Check the length
            check_length(values, parameters)

            # Write to file
            with open(outfilename, "a") as output_filehandle:
                output_filehandle.write(separator.join(values) + "\n")
