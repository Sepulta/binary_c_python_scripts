"""
Function to do a triangle plot. We can use an optional base-query and a

TODO: Consider showing the CDF on the histogram plots
TODO: display queried results
TODO: store in phd modules
TODO: Add query
TODO: hatches, linestyles and colors for queried

TODO: allow us to limit the lower range
"""

import itertools
import numpy as np
import matplotlib

from david_phd_functions.plotting.canvas_functions import (
    general_corner_plot_with_colorbar,
)

from david_phd_functions.plotting.utils import show_and_save_plot, add_plot_info
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def triangle_plot(
    df,
    plot_columns,
    bins,
    scales,
    zscale,
    queried_df=None,
    probability_column="number_per_solar_mass",
    plot_settings=None,
):
    """
    Function to do the triangle plot based on a dataframe. all the columns except for the probability_column will be plotted and compared.

    xscales: scales in which the parameter axis is plotted
    zscale: scale in which the probability density is plotted
    """

    if plot_settings is None:
        plot_settings = {}

    # Basic checks:
    assert len(scales) == len(df.columns) - 1
    assert len(scales) == len(bins)
    for bin_array in bins:
        if not np.isfinite(bin_array).all():
            raise ValueError("nan values found in the bins. Abort")

    #
    parameter_columns = plot_columns
    n_parameters = len(parameter_columns)

    # Get indices to permute
    indices = list(range(n_parameters))

    # Create permutations on the columns
    combinations = itertools.combinations(indices, 2)

    # Create matrix plot with colorbar
    fig, diagonal_axes, colorbar_axis, axis_matrix = general_corner_plot_with_colorbar(
        n_parameters
    )

    ####
    # Plot the diagonals
    for column_i, column_name in enumerate(parameter_columns):
        # TODO: replace this into a function on itself
        diagonal_axes[column_i].hist(
            df[column_name], bins=bins[column_i], weights=df[probability_column]
        )

        diagonal_axes[column_i].set_title(column_name, fontsize=16)

        diagonal_axes[column_i].set_xscale(scales[column_i])

        diagonal_axes[column_i].set_yscale(zscale)

        # #
        # print(column_i, column_name)
        # print(bins[column_i])
        # print(diagonal_axes[column_i].get_xlim())

    #
    row_col_list = []
    for i in range(n_parameters):
        for j in range(i, n_parameters):
            col = i
            row = j

            if not i == j:
                row_col_list.append([row, col])
                # print("Column: {} Row: {}".format(column, row))

    #
    norm_min = 1e90
    norm_max = -1e90

    # Pre calculate the min and max
    for row_col, combination in zip(row_col_list, itertools.combinations(indices, 2)):

        row = row_col[0]
        col = row_col[1]

        x_col_i = combination[0]
        y_col_i = combination[1]

        x_col = parameter_columns[x_col_i]
        y_col = parameter_columns[y_col_i]

        df_cur = df[[x_col, y_col, probability_column]]

        hist = np.histogram2d(
            x=df_cur[x_col],
            y=df_cur[y_col],
            bins=[bins[x_col_i], bins[y_col_i]],
            weights=df_cur[probability_column],
        )

        hist_density_vals = hist[0]
        filtered = hist_density_vals[hist_density_vals != 0]

        #
        norm_min = np.min([norm_min, filtered.min()])
        norm_max = np.max([norm_max, filtered.max()])

    # Take into account the max diff threshold
    if plot_settings.get("max_diff_threshold", None) is not None:
        norm_min = 10 ** (np.log10(norm_max) - plot_settings["max_diff_threshold"])

    norm = matplotlib.colors.LogNorm(vmin=norm_min, vmax=norm_max)

    # Create permutations on the columns
    for row_col, combination in zip(row_col_list, itertools.combinations(indices, 2)):
        row = row_col[0]
        col = row_col[1]

        x_col_i = combination[0]
        y_col_i = combination[1]

        x_col = parameter_columns[x_col_i]
        y_col = parameter_columns[y_col_i]

        # print("Row {} col {}: x_col: {} y_col: {}".format(row, col, x_col, y_col))
        df_cur = df[[x_col, y_col, probability_column]]

        hist = axis_matrix[row][col].hist2d(
            x=df_cur[x_col],
            y=df_cur[y_col],
            bins=[bins[x_col_i], bins[y_col_i]],
            weights=df_cur[probability_column],
            norm=norm,
        )

        # Set the correct scales
        axis_matrix[row][col].set_xscale(scales[x_col_i])
        axis_matrix[row][col].set_yscale(scales[y_col_i])

        # Set the correct lims
        axis_matrix[row][col].set_xlim(diagonal_axes[x_col_i].get_xlim())
        axis_matrix[row][col].set_ylim(diagonal_axes[y_col_i].get_xlim())

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        colorbar_axis,
        cmap=matplotlib.cm.viridis,
        norm=matplotlib.colors.LogNorm(vmin=norm_min, vmax=norm_max),
    )
    cbar.ax.set_ylabel("Number of systems")

    # # Fix labels
    # for i in range(n_parameters):
    #     for j in range(n_parameters):
    #         # Column:
    #         if j == 0:
    #             if not i == n_parameters-1:
    #                 axis_matrix[i][j].set_xticklabels([])
    #         elif i == n_parameters-1:
    #             if not j == 0:
    #                 axis_matrix[i][j].set_yticklabels([])
    #         else:
    #             if axis_matrix[i][j]:
    #                 axis_matrix[i][j].set_xticklabels([])
    #                 axis_matrix[i][j].set_yticklabels([])

    #######################
    # Save and finish
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def generate_triangle_plot(
    dataframe,
    plot_columns,
    scales=None,
    zscale="log",
    bins=None,
    base_query=None,
    query=None,
    probability_column="number_per_solar_mass",
    plot_settings=None,
):
    """
    Wrapper function to generate a triangle plot
    """

    # Check input for scales
    if scales is None:
        scales = ["linear" for _ in range(len(plot_columns))]

    # Automise this to bin in log as well
    if bins is None:
        bins = [
            np.linspace(dataframe[el].min(), dataframe[el].max(), 50)
            if scales[i] == "linear"
            else 10
            ** np.linspace(
                np.log10(dataframe[el].min()), np.log10(dataframe[el].max()), 50
            )
            for i, el in enumerate(plot_columns)
        ]

    # query
    if not base_query is None:
        dataframe = dataframe.query(base_query)

    # Drop columns that we dont want
    drop_columns = [
        el for el in dataframe.columns if not el in plot_columns + [probability_column]
    ]

    # if no extra query
    if query is None:
        reduced_base_dataframe = dataframe.drop(columns=drop_columns)

        triangle_plot(
            df=reduced_base_dataframe,
            plot_columns=plot_columns,
            bins=bins,
            scales=scales,
            zscale=zscale,
            plot_settings=plot_settings,
            probability_column=probability_column,
        )

    else:
        queried_extra_dataframe = dataframe.query(query)

        reduced_base_dataframe = df.drop(columns=drop_columns)
        reduced_extra_dataframe = queried_extra_dataframe.drop(columns=drop_columns)

        #
        triangle_plot(
            df=reduced_base_dataframe,
            plot_columns=plot_columns,
            bins=bins,
            scales=scales,
            zscale=zscale,
            queried_df=reduced_extra_dataframe,
            plot_settings=plot_settings,
            probability_column=probability_column,
        )
