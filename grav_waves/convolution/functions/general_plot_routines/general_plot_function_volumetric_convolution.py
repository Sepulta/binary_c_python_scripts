def general_plot_routine_volume_convolved(
    convolution_dataset,
    dco_type,
    main_plot_dir,
    output_pdf_filename,
    min_plot_time=None,
    max_plot_time=None,
):
    """
    Function to plot all the convolved quantities

    plots all above plots multiple times with different probability floors
    """

    # limit time
    plot_time_limit_dict = {}
    zoom_string = ""
    if not min_plot_time is None:
        plot_time_limit_dict["min_plot_time"] = min_plot_time
        zoom_string = "_zoomed"
    if not max_plot_time is None:
        plot_time_limit_dict["max_plot_time"] = max_plot_time
        zoom_string = "_zoomed"

    #
    probability_floors = [2, 3]
    format_types = ["png", "pdf"]

    #
    dataset_dict = convolution_dataset["dataset_dict"]
    config_dict_cosmology = convolution_dataset["cosmology_configuration"]
    config_dict_convolution = convolution_dataset["convolution_configuration"]

    #
    sim_name = dataset_dict["sim_name"]
    type_key = dco_type

    # Set rate types
    rate_types = ["merger_rate"]

    #
    plot_dir = os.path.abspath(main_plot_dir)

    # Set up the pdf stuff
    merger = PdfFileMerger()
    pdf_list = []
    pdf_page_number = 0

    #
    print("Generating volume convolved plots for {}".format(sim_name))

    ###########################################################################
    # Redshift volume shells
    # plot_comoving_shell_volumes with the used dataset
    print("\tPlotting plot_comoving_shell_volumes")
    for format_type in format_types:
        print("\t\tSaving as {}".format(format_type))
        plot_settings = {
            "output_name": os.path.join(
                plot_dir,
                type_key,
                format_type,
                "comoving_shell_volumes.{}".format(format_type),
            ),
            "block": False,
            "show_plot": False,
            "simulation_name": sim_name + "_{}".format(type_key),
            "antialiased": True,
            "rasterized": True,
            "runname": "comoving_shell_volumes",
        }
        plot_settings.update(plot_time_limit_dict)

        plot_comoving_shell_volumes(convolution_dataset, plot_settings=plot_settings)
    pdf_list.append(
        os.path.join(plot_dir, type_key, "pdf", "comoving_shell_volumes.pdf")
    )
    merger, pdf_page_number = add_pdf_and_bookmark(
        merger, pdf_list[-1], pdf_page_number, bookmark_text="comoving_shell_volumes"
    )

    ###########################################################################
    # Volume convolved merger rates at all redshifts
    plot_volume_convolved_rate
    for mass_type in ["chirpmass", "total_mass", "primary_mass", "any_mass"]:
        print("\tPlotting volume convolved rates for {}".format(mass_type))
        for probability_floor in probability_floors:
            print("\t\tUsing probability floor {}".format(probability_floor))
            for format_type in format_types:
                print("\t\tSaving as {}".format(format_type))

                plot_settings = {
                    "show_plot": False,
                    "probability_floor": probability_floor,
                    "output_name": os.path.join(
                        plot_dir,
                        type_key,
                        format_type,
                        "plot_volume_convolved_rate_{}_{}floor.{}".format(
                            mass_type, probability_floor, format_type
                        ),
                    ),
                    "simulation_name": sim_name,
                    "antialiased": True,
                    "rasterized": True,
                    "runname": "plot_volume_convolved_rate: mass quanitity: {} probability floor: {}".format(
                        mass_type, probability_floor
                    ),
                }
                plot_settings.update(plot_time_limit_dict)

                plot_volume_convolved_rate(
                    convolution_dataset, mass_type, plot_settings=plot_settings
                )
            pdf_list.append(
                os.path.join(
                    plot_dir,
                    type_key,
                    "pdf",
                    "plot_volume_convolved_rate_{}_{}floor.pdf".format(
                        mass_type, probability_floor
                    ),
                )
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                pdf_list[-1],
                pdf_page_number,
                bookmark_text="Volume convolved merger rate for {}: floor={}".format(
                    mass_type, probability_floor
                ),
            )

        # Predicted observed merger rate
        print("\tPlotting total_observable_rates for {}".format(mass_type))
        for format_type in format_types:
            print("\t\tSaving as {}".format(format_type))
            plot_total_observable_rates(
                convolution_dataset,
                mass_type=mass_type,
                plot_settings={
                    "show_plot": False,
                    "output_name": os.path.join(
                        plot_dir,
                        type_key,
                        format_type,
                        "total_observable_rates_{}.{}".format(mass_type, format_type),
                    ),
                    "simulation_name": sim_name,
                    "antialiased": True,
                    "rasterized": True,
                    "runname": "plot_mergers_at_specific_lookback",
                },
            )
        pdf_list.append(
            os.path.join(
                plot_dir,
                type_key,
                "pdf",
                "total_observable_rates_{}.pdf".format(mass_type),
            )
        )
        merger, pdf_page_number = add_pdf_and_bookmark(
            merger,
            pdf_list[-1],
            pdf_page_number,
            bookmark_text="total_observable_rates for {}".format(mass_type),
        )

    # wrap up the pdf
    merger.write(output_pdf_filename)
    merger.close()

    print(
        "Finished generating volume convolution plots for dataset {}. Wrote results to {}".format(
            sim_name, output_pdf_filename
        )
    )
