"""
Function to plot the zams: mass removal and core mass shift schematic
"""

import matplotlib as mpl
import matplotlib.pyplot as plt

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.add_zams_variation_to_axis import (
    add_zams_variation_to_axis,
)

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_utils import (
    add_label_subplot,
    handle_overriding_xlim,
    align_axes,
    add_ax_invisible,
    add_labels_subplots,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def plot_zams_mass_removal_and_core_mass_shift_schematic(
    result_directory_mass_removal,
    result_directory_core_mass_shift,
    old_prescription_result_directory_root,
    plot_settings=None,
):
    """
    Function to plot the ZAMS mass to remnant mass with extra ppisn mass removal and min co_core mass shift
    """

    #
    fig = plt.figure(figsize=(30, 18))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=8, ncols=1)

    ax_mass_removal = fig.add_subplot(gs[:4, 0])
    ax_core_mass_shift = fig.add_subplot(gs[4:, 0])
    axes_list = [
        ax_mass_removal,
        ax_core_mass_shift,
    ]

    # Add invisible axis
    ax_invisible = add_ax_invisible(fig, gs[:, 0])

    #######
    # Add mass removal plot
    fig, ax_mass_removal = add_zams_variation_to_axis(
        fig=fig,
        ax=ax_mass_removal,
        result_directory_variation=result_directory_mass_removal,
        old_prescription_result_directory_root=old_prescription_result_directory_root,
        #
        variation_name="mass_removal",
        dataset_used_for_regions="fiducial",
        #
        add_pre_SN_mass_line=True,
        add_no_prescription_line=False,
        add_old_prescription_line=True,
        add_fiducial_line=True,
        add_variation_line=True,
        #
        add_variation_bracket=True,
        add_shading_regions=True,
        add_supernova_lines=True,
    )

    #######
    # Add core mass shift plot
    fig, ax_mass_removal = add_zams_variation_to_axis(
        fig=fig,
        ax=ax_core_mass_shift,
        result_directory_variation=result_directory_core_mass_shift,
        old_prescription_result_directory_root=old_prescription_result_directory_root,
        #
        variation_name="core_mass_shift",
        dataset_used_for_regions="fiducial",
        #
        add_pre_SN_mass_line=True,
        add_no_prescription_line=False,
        add_old_prescription_line=True,
        add_fiducial_line=True,
        add_variation_line=True,
        #
        add_variation_bracket=True,
        add_shading_regions=True,
        add_supernova_lines=True,
    )

    ###########
    # add sublplots
    add_labels_subplots(
        fig=fig, axes_list=axes_list, label_function_kwargs={"x_loc": 0.01}
    )

    # Align the axes
    align_axes(fig=fig, axes_list=axes_list, which_axis="x")
    align_axes(fig=fig, axes_list=axes_list, which_axis="y")

    # Updated the axes limits if user provides update
    handle_overriding_xlim(
        fig=fig,
        axes_list=axes_list,
        plot_settings=plot_settings,
    )

    # Make up
    ax_core_mass_shift.set_title("")
    ax_mass_removal.set_xticklabels([])
    ax_mass_removal.set_xlabel("")

    ylabel = ax_mass_removal.get_ylabel()
    ax_invisible.set_ylabel(ylabel, labelpad=80)

    ax_mass_removal.set_ylabel("")
    ax_core_mass_shift.set_ylabel("")

    # Handle the plotting
    show_and_save_plot(fig, plot_settings)
