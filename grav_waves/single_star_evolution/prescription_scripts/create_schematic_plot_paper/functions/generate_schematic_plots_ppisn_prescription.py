"""
function to generate the plots for the schematic overview
"""

import os
import shutil
from PyPDF2 import PdfFileMerger

from david_phd_functions.binaryc.personal_defaults import personal_defaults

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.custom_logging_string import (
    mass_removal_custom_logging_string,
)
from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.parse_functions import (
    parse_function_ppisn_mass_removal,
)

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_co_core_one_variation import (
    plot_co_core_one_variation,
)
from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_co_core_two_variations import (
    plot_co_core_two_variations,
)
from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_zams_one_variation import (
    plot_zams_one_variation,
)
from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_zams_two_variations import (
    plot_zams_two_variations,
)


from grav_waves.settings import (
    project_specific_settings,
)  # TODO: That should not happen here!

#
from david_phd_functions.plotting.pdf_functions import (
    add_pdf_and_bookmark,
    generate_chapterpage_pdf,
)

###
WIND_MASS_LOSS_DICT = {
    1: "WIND_ALGORITHM_HURLEY2002",
    2: "WIND_ALGORITHM_SCHNEIDER2018",
    3: "WIND_ALGORITHM_BINARY_C_2020",
    4: "WIND_ALGORITHM_HENDRIKS_2021",
}
PPISN_DICT = {0: "NONE", 1: "FARMER19", 2: "NEW_FIT_21"}


del project_specific_settings["david_logging_function"]


def generate_schematic_plots_ppisn_prescription(
    result_dir,
    plot_dir,
    physics_settings,
    combined_pdf_output=None,
    generate_results=True,
    generate_plots=True,
    generate_old_ppisn_prescription_data=True,
    generate_no_ppisn_prescription_data=False,
):
    """
    Function to generate the data and the plots for the schematic ppisn prescription
    """

    if generate_plots is not None:
        if combined_pdf_output is None:
            raise ValueError("cant generate plots without a pdf output name")

    #############
    # Readout settings
    wind_mass_loss_setting = physics_settings.get("wind_mass_loss_setting", 2)
    ppisn_prescription = physics_settings.get("ppisn_prescription", 2)
    metallicity = physics_settings.get("metallicity", 1e-3)
    mass_removal_values = physics_settings.get("mass_removal_values", [0, 5])
    CO_core_shift_values = physics_settings.get("CO_core_shift_values", [0, -2])
    mass_removal_multiplier_values = physics_settings.get(
        "mass_removal_multiplier_values", [1, 2]
    )
    resolution = physics_settings.get("resolution", {"M_1": 600})

    fiducial_reset_dict = {
        "PPISN_core_mass_range_shift": 0,
        "PPISN_additional_massloss": 0,
        "PPISN_massloss_multiplier": 1,
    }

    ############
    # Run the simulations and generate the plots

    # Create simname
    simname = "{}_PPISN_{}_Z{}".format(
        WIND_MASS_LOSS_DICT[wind_mass_loss_setting],
        PPISN_DICT[ppisn_prescription],
        metallicity,
    )

    # set up directories
    plot_dir_simname = os.path.join(plot_dir, simname)
    result_dir_simname_mass_removal = os.path.join(result_dir, "mass_removal", simname)
    result_dir_simname_core_mass_shift = os.path.join(
        result_dir, "core_mass_shift", simname
    )
    result_dir_simname_mass_removal_multiplier = os.path.join(
        result_dir, "mass_removal_multiplier", simname
    )

    #
    ppisn_prescription_old = 1
    simname_old = "{}_PPISN_{}_Z{}".format(
        WIND_MASS_LOSS_DICT[wind_mass_loss_setting],
        PPISN_DICT[ppisn_prescription_old],
        metallicity,
    )
    result_dir_simname_old = os.path.join(result_dir, "old_prescription", simname_old)

    # No ppisn
    if generate_no_ppisn_prescription_data:
        ppisn_prescription_no_ppisn = 0
        simname_no_ppisn = "{}_PPISN_{}_Z{}".format(
            WIND_MASS_LOSS_DICT[wind_mass_loss_setting],
            PPISN_DICT[ppisn_prescription_no_ppisn],
            metallicity,
        )
        result_dir_simname_no_ppisn = os.path.join(
            result_dir, "no_ppisn_prescription", simname_no_ppisn
        )

    # Flag to actually generate the results
    if generate_results:
        from binarycpython.utils.grid import Population

        #####################
        # Set up population
        test_pop = Population(verbosity=1)

        # Load personal defaults
        test_pop.set(**personal_defaults)

        # Load project specific defaults
        test_pop.set(**project_specific_settings)

        #
        test_pop.set(
            multiplicity=1,
            num_cores=4,
            #
            david_ppisn_logging=0,
            C_logging_code=mass_removal_custom_logging_string,
            parse_function=parse_function_ppisn_mass_removal,
            # Add our own settings in here
            metallicity=metallicity,
            PPISN_prescription=ppisn_prescription,
            log_args=0,
        )

        # Add the mass in logspace
        test_pop.add_grid_variable(
            name="M_1",
            longname="Primary mass",
            valuerange=[1, 300],
            # resolution="{}".format(resolution["M_1"]),
            samplerfunc="const(1, 300, {})".format(resolution["M_1"]),
            probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 301, -1.3, -2.3, -2.3)*M_1",
            dphasevol="dM_1",
            parameter_name="M_1",
            condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
        )

        # Get version info of the binary_c build
        version_info = test_pop.return_binary_c_version_info(parsed=True)

        # Stop the script if the configuration is wrong
        if version_info["macros"]["NUCSYN"] == "on":
            print("Please disable NUCSYN")
            quit()

        # Remove plot and result dir
        if os.path.isdir(plot_dir_simname):
            print("Removing {}".format(plot_dir_simname))
            shutil.rmtree(plot_dir_simname)
        if os.path.isdir(result_dir_simname_mass_removal):
            print("Removing {}".format(result_dir_simname_mass_removal))
            shutil.rmtree(result_dir_simname_mass_removal)
        if os.path.isdir(result_dir_simname_core_mass_shift):
            print("Removing {}".format(result_dir_simname_core_mass_shift))
            shutil.rmtree(result_dir_simname_core_mass_shift)
        if os.path.isdir(result_dir_simname_mass_removal_multiplier):
            print("Removing {}".format(result_dir_simname_mass_removal_multiplier))
            shutil.rmtree(result_dir_simname_mass_removal_multiplier)
        if generate_old_ppisn_prescription_data:
            if os.path.isdir(result_dir_simname_old):
                print("Removing {}".format(result_dir_simname_old))
                shutil.rmtree(result_dir_simname_old)
        if generate_no_ppisn_prescription_data:
            if os.path.isdir(result_dir_simname_no_ppisn):
                print("Removing {}".format(result_dir_simname_no_ppisn))
                shutil.rmtree(result_dir_simname_no_ppisn)

        ###################
        # Run simulation removing extra mass
        if mass_removal_values:
            test_pop.set(**fiducial_reset_dict)
            for mass_removal_value in mass_removal_values:
                print(
                    "Running code with mass_removal_value={}".format(mass_removal_value)
                )

                # Add situational settings
                test_pop.set(
                    PPISN_additional_massloss=mass_removal_value,
                )
                test_pop.set(
                    data_dir=os.path.join(
                        result_dir_simname_mass_removal, "{}".format(mass_removal_value)
                    ),
                    base_filename="output.dat",
                )
                test_pop.set(
                    tmp_dir=os.path.join(
                        test_pop.custom_options["data_dir"], "local_tmp_dir"
                    )
                )

                # create local tmp_dir
                os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

                # Remove pre-existing file
                filename = os.path.join(
                    test_pop.custom_options["data_dir"],
                    test_pop.custom_options["base_filename"],
                )
                if os.path.isfile(filename):
                    os.remove(filename)

                # Export settings:
                test_pop.export_all_info(use_datadir=True)

                # Evolve grid
                test_pop.evolve()

                # Clean results
                test_pop.clean()

        ###################
        # Run simulations shifting the CO core range
        if CO_core_shift_values:
            test_pop.set(**fiducial_reset_dict)
            for CO_core_shift_value in CO_core_shift_values:
                print(
                    "Running code with CO_core_shift_values={}".format(
                        CO_core_shift_value
                    )
                )

                # Add situational settings
                test_pop.set(
                    PPISN_core_mass_range_shift=CO_core_shift_value,
                )
                test_pop.set(
                    data_dir=os.path.join(
                        result_dir_simname_core_mass_shift,
                        "{}".format(CO_core_shift_value),
                    ),
                    base_filename="output.dat",
                )
                test_pop.set(
                    tmp_dir=os.path.join(
                        test_pop.custom_options["data_dir"], "local_tmp_dir"
                    )
                )

                # create local tmp_dir
                os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

                # Remove pre-existing file
                filename = os.path.join(
                    test_pop.custom_options["data_dir"],
                    test_pop.custom_options["base_filename"],
                )
                if os.path.isfile(filename):
                    os.remove(filename)

                # Export settings:
                test_pop.export_all_info(use_datadir=True)

                # Evolve grid
                test_pop.evolve()

                # Clean results
                test_pop.clean()

        ###################
        # Run simulations multiplying the mass loss predicted by PPISN
        if mass_removal_multiplier_values:
            test_pop.set(**fiducial_reset_dict)
            for mass_removal_mulitplier_value in mass_removal_multiplier_values:
                print(
                    "Running code with mass_removal_mulitplier_values={}".format(
                        mass_removal_mulitplier_value
                    )
                )

                # Add situational settings
                test_pop.set(
                    PPISN_massloss_multiplier=mass_removal_mulitplier_value,
                )
                test_pop.set(
                    data_dir=os.path.join(
                        result_dir_simname_mass_removal_multiplier,
                        "{}".format(mass_removal_mulitplier_value),
                    ),
                    base_filename="output.dat",
                )
                test_pop.set(
                    tmp_dir=os.path.join(
                        test_pop.custom_options["data_dir"], "local_tmp_dir"
                    )
                )

                # create local tmp_dir
                os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

                # Remove pre-existing file
                filename = os.path.join(
                    test_pop.custom_options["data_dir"],
                    test_pop.custom_options["base_filename"],
                )
                if os.path.isfile(filename):
                    os.remove(filename)

                # Export settings:
                test_pop.export_all_info(use_datadir=True)

                # Evolve grid
                test_pop.evolve()

                # Clean results
                test_pop.clean()

        ######
        # Run the results for the 'old' prescription
        if generate_old_ppisn_prescription_data:
            test_pop.set(
                PPISN_prescription=ppisn_prescription_old,
            )
            for mass_removal_value in [0]:
                print(
                    "Running code with mass_removal_value={}".format(mass_removal_value)
                )

                # Add situational settings
                test_pop.set(
                    PPISN_additional_massloss=mass_removal_value,
                )

                test_pop.set(
                    data_dir=os.path.join(
                        result_dir_simname_old, "{}".format(mass_removal_value)
                    ),
                    base_filename="output.dat",
                )
                test_pop.set(
                    tmp_dir=os.path.join(
                        test_pop.custom_options["data_dir"], "local_tmp_dir"
                    )
                )

                # create local tmp_dir
                os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

                # Remove pre-existing file
                filename = os.path.join(
                    test_pop.custom_options["data_dir"],
                    test_pop.custom_options["base_filename"],
                )
                if os.path.isfile(filename):
                    os.remove(filename)

                # Export settings:
                test_pop.export_all_info(use_datadir=True)

                # Evolve grid
                test_pop.evolve()

                # Clean results
                test_pop.clean()

        ######
        # Run the results with PPISN mechanism disabled
        if generate_no_ppisn_prescription_data:
            mass_removal_value = 0
            test_pop.set(
                PPISN_prescription=ppisn_prescription_no_ppisn,
                PPISN_additional_massloss=mass_removal_value,
            )
            test_pop.set(
                data_dir=os.path.join(
                    result_dir_simname_no_ppisn, "{}".format(mass_removal_value)
                ),
                base_filename="output.dat",
            )
            test_pop.set(
                tmp_dir=os.path.join(
                    test_pop.custom_options["data_dir"], "local_tmp_dir"
                )
            )

            # create local tmp_dir
            os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

            # Remove pre-existing file
            filename = os.path.join(
                test_pop.custom_options["data_dir"],
                test_pop.custom_options["base_filename"],
            )
            if os.path.isfile(filename):
                os.remove(filename)

            # Export settings:
            test_pop.export_all_info(use_datadir=True)

            # Evolve grid
            test_pop.evolve()

            # Clean results
            test_pop.clean()

    # Create plots
    if generate_plots:
        do_plot_zams_mass_schematic = True
        do_plot_zams_mass_removal_schematic = True
        do_plot_zams_core_mass_shift_schematic = True
        do_plot_zams_mass_removal_multiplier_schematic = True
        do_plot_zams_mass_removal_and_core_mass_shift_schematic = True

        do_plot_co_core_schematic = True
        do_plot_co_core_mass_removal_schematic = True
        do_plot_co_core_core_mass_shift_schematic = True
        do_plot_co_core_mass_removal_multiplier_schematic = True

        do_plot_co_core_mass_removal_and_core_mass_shift_schematic = True

        show_plot = False

        # Set up the pdf stuff
        merger = PdfFileMerger()
        pdf_page_number = 0

        # Plot zams mass vs remnant mass without the variations
        if do_plot_zams_mass_schematic:
            plot_zams_mass_schematic_filename = os.path.join(
                plot_dir_simname, "zams_mass_schematic.pdf"
            )
            plot_zams_one_variation(
                variation_name="no_variation",
                result_directory_variation=result_dir_simname_mass_removal,
                old_prescription_result_directory_root=result_dir_simname_old,
                plot_settings={
                    "show_plot": show_plot,
                    "output_name": plot_zams_mass_schematic_filename,
                },
                extra_variation_configuration={
                    "add_variation_line": False,
                    "dataset_used_for_regions": "fiducial",
                },
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                plot_zams_mass_schematic_filename,
                pdf_page_number,
                bookmark_text="ZAMS mass to remnant mass",
            )

        # Plot zams mass vs remnant mass with the extra mass removed and the old presciption
        if do_plot_zams_mass_removal_schematic:
            plot_zams_mass_removal_schematic_filename = os.path.join(
                plot_dir_simname, "zams_mass_removal_schematic.pdf"
            )

            plot_zams_one_variation(
                variation_name="mass_removal",
                result_directory_variation=result_dir_simname_mass_removal,
                old_prescription_result_directory_root=result_dir_simname_old,
                plot_settings={
                    "show_plot": show_plot,
                    "output_name": plot_zams_mass_removal_schematic_filename,
                },
                extra_variation_configuration={"dataset_used_for_regions": "variation"},
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                plot_zams_mass_removal_schematic_filename,
                pdf_page_number,
                bookmark_text="ZAMS mass to remnant mass with PPISN mass removal",
            )

        # Plot zams mass vs remnant mass with the core mass shift and the old presciption
        if do_plot_zams_core_mass_shift_schematic:
            plot_zams_core_mass_shift_schematic_filename = os.path.join(
                plot_dir_simname, "zams_core_mass_shift_schematic.pdf"
            )
            plot_zams_one_variation(
                variation_name="core_mass_shift",
                result_directory_variation=result_dir_simname_core_mass_shift,
                old_prescription_result_directory_root=result_dir_simname_old,
                plot_settings={
                    "show_plot": show_plot,
                    "output_name": plot_zams_core_mass_shift_schematic_filename,
                },
                extra_variation_configuration={"dataset_used_for_regions": "variation"},
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                plot_zams_core_mass_shift_schematic_filename,
                pdf_page_number,
                bookmark_text="ZAMS mass to remnant mass with CO core mass shift",
            )

        # Plot zams mass vs remnant mass with the mass removal multiplier and the old presciption
        if do_plot_zams_mass_removal_multiplier_schematic:
            plot_zams_mass_removal_multiplier_schematic_filename = os.path.join(
                plot_dir_simname, "zams_mass_removal_multiplier_schematic.pdf"
            )
            plot_zams_one_variation(
                variation_name="mass_removal_multiplier",
                result_directory_variation=result_dir_simname_mass_removal_multiplier,
                old_prescription_result_directory_root=result_dir_simname_old,
                plot_settings={
                    "show_plot": show_plot,
                    "output_name": plot_zams_mass_removal_multiplier_schematic_filename,
                },
                extra_variation_configuration={"dataset_used_for_regions": "variation"},
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                plot_zams_mass_removal_multiplier_schematic_filename,
                pdf_page_number,
                bookmark_text="ZAMS mass to remnant mass mass removal multiplier",
            )

        # Plot 2 panels: zams mass vs remnant mass with the core mass shift and the extra mass removed
        if do_plot_zams_mass_removal_and_core_mass_shift_schematic:
            plot_zams_mass_removal_and_core_mass_shift_schematic_filename = (
                os.path.join(
                    plot_dir_simname,
                    "zams_mass_removal_and_core_mass_shift_schematic.pdf",
                )
            )
            plot_zams_two_variations(
                result_directory_mass_removal=result_dir_simname_mass_removal,
                result_directory_core_mass_shift=result_dir_simname_core_mass_shift,
                old_prescription_result_directory_root=result_dir_simname_old,
                plot_settings={
                    "show_plot": show_plot,
                    "output_name": plot_zams_mass_removal_and_core_mass_shift_schematic_filename,
                },
                extra_variation_configuration={"dataset_used_for_regions": "variation"},
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                plot_zams_mass_removal_and_core_mass_shift_schematic_filename,
                pdf_page_number,
                bookmark_text="ZAMS mass to remnant mass with PPISN mass removal and CO core mass shift",
            )

        # Plot pre-sn CO core vs remnant mass without variations
        if do_plot_co_core_schematic:
            plot_co_core_schematic_filename = os.path.join(
                plot_dir_simname, "CO_core_schematic.pdf"
            )
            plot_co_core_one_variation(
                variation_name="no_variation",
                result_directory_variation=result_dir_simname_mass_removal,
                old_prescription_result_directory_root=result_dir_simname_old,
                plot_settings={
                    "show_plot": show_plot,
                    "output_name": plot_co_core_schematic_filename,
                },
                extra_variation_configuration={
                    "add_variation_line": False,
                    "dataset_used_for_regions": "fiducial",
                },
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                plot_co_core_schematic_filename,
                pdf_page_number,
                bookmark_text="CO core to remnant mass",
            )

        #
        if do_plot_co_core_mass_removal_schematic:
            plot_co_core_mass_removal_schematic_filename = os.path.join(
                plot_dir_simname, "co_core_mass_removal_schematic.pdf"
            )
            plot_co_core_one_variation(
                variation_name="mass_removal",
                result_directory_variation=result_dir_simname_mass_removal,
                old_prescription_result_directory_root=result_dir_simname_old,
                plot_settings={
                    "show_plot": show_plot,
                    "output_name": plot_co_core_mass_removal_schematic_filename,
                },
                extra_variation_configuration={
                    "dataset_used_for_regions": "variation",
                },
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                plot_co_core_mass_removal_schematic_filename,
                pdf_page_number,
                bookmark_text="CO core mass to remnant mass with PPISN mass removal",
            )

        if do_plot_co_core_core_mass_shift_schematic:
            plot_co_core_core_mass_shift_schematic_filename = os.path.join(
                plot_dir_simname, "co_core_core_mass_shift_schematic.pdf"
            )
            plot_co_core_one_variation(
                variation_name="co_core_shift",
                result_directory_variation=result_dir_simname_core_mass_shift,
                old_prescription_result_directory_root=result_dir_simname_old,
                plot_settings={
                    "show_plot": show_plot,
                    "output_name": plot_co_core_core_mass_shift_schematic_filename,
                },
                extra_variation_configuration={
                    "dataset_used_for_regions": "variation",
                },
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                plot_co_core_core_mass_shift_schematic_filename,
                pdf_page_number,
                bookmark_text="CO core mass to remnant mass with CO core mass shift",
            )

        if do_plot_co_core_mass_removal_multiplier_schematic:
            plot_co_core_mass_removal_multiplier_schematic_filename = os.path.join(
                plot_dir_simname, "co_core_mass_removal_multiplier_schematic.pdf"
            )
            plot_co_core_one_variation(
                variation_name="mass_removal_multiplier",
                result_directory_variation=result_dir_simname_mass_removal_multiplier,
                old_prescription_result_directory_root=result_dir_simname_old,
                plot_settings={
                    "show_plot": show_plot,
                    "output_name": plot_co_core_mass_removal_multiplier_schematic_filename,
                },
                extra_variation_configuration={
                    "dataset_used_for_regions": "variation",
                },
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                plot_co_core_mass_removal_multiplier_schematic_filename,
                pdf_page_number,
                bookmark_text="CO core mass to remnant mass with mass removal multiplier",
            )

        ###
        # 2-panel
        if do_plot_co_core_mass_removal_and_core_mass_shift_schematic:
            plot_co_core_mass_removal_and_core_mass_shift_schematic_filename = (
                os.path.join(
                    plot_dir_simname,
                    "co_core_mass_removal_and_core_mass_shift_schematic.pdf",
                )
            )
            plot_co_core_two_variations(
                result_directory_mass_removal=result_dir_simname_mass_removal,
                result_directory_core_mass_shift=result_dir_simname_core_mass_shift,
                old_prescription_result_directory_root=result_dir_simname_old,
                plot_settings={
                    "show_plot": show_plot,
                    "output_name": plot_co_core_mass_removal_and_core_mass_shift_schematic_filename,
                },
                extra_variation_configuration={"dataset_used_for_regions": "variation"},
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                plot_co_core_mass_removal_and_core_mass_shift_schematic_filename,
                pdf_page_number,
                bookmark_text="CO core mass to remnant mass with PPISN mass removal and CO core mass shift",
            )

        # Round off the plot
        pdf_output_name = os.path.join(combined_pdf_output)
        merger.write(pdf_output_name)
        merger.close()
        print("Finished making plots. Wrote pdf to {}".format(pdf_output_name))
