"""
Function to plot the 2-d density plot for merger or formation rate per metallicity and per redshift
"""

import copy
import pickle
import json
import h5py
import numpy as np
import pandas as pd

import astropy.units as u

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors

from david_phd_functions.grav_waves.dataframe_functions import add_chirpmass_column
from grav_waves.convolution.functions.convolution_functions import (
    generate_metallicity_sfr_array,
)
from grav_waves.convolution.functions.redshift_interpolation_functions import (
    test_redshift_to_lookback_time,
    test_lookback_time_to_redshift,
    create_interpolation_datasets,
)
from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    get_hists_multiprocessing,
    filter_dco_type,
)

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.canvas_functions import return_canvas_with_subsets

#
number_per_gigaparsec_cubed_per_year = 1 / u.Gpc**3 / u.yr
sfr_unit = u.Msun / u.Gpc**3 / u.yr


def get_data(
    hdf5_filename,
    dco_type,
    metallicity_bins,
    rate_key_in_df,
    rate_type_hdf5_key,
    divide_by_binsize,
    num_procs=1,
    query=None,
):
    """
    Function to read out the data from the hdf5
    """

    metallicity_bins_size = np.diff(metallicity_bins)

    hdf5_file = h5py.File(hdf5_filename, "r")
    sorted_keys = sorted(
        hdf5_file["data"][rate_type_hdf5_key].keys(),
        key=lambda key: float(key),
        reverse=False,
    )
    hdf5_file.close()

    # Get combined df
    combined_df = pd.read_hdf(hdf5_filename, "/data/combined_dataframes")

    # Add columns
    combined_df = add_chirpmass_column(combined_df, m1_name="mass_1", m2_name="mass_2")
    combined_df["total_mass"] = combined_df["mass_1"] + combined_df["mass_2"]
    combined_df["primary_mass"] = combined_df[["mass_1", "mass_2"]].max(axis=1)

    ###################################
    # Loop over all datasets:
    res_dict = get_hists_multiprocessing(
        hdf5_filename=hdf5_filename,
        combined_df=combined_df,
        dco_type=dco_type,
        query=query,
        rate_key_in_df=rate_key_in_df,
        bins=metallicity_bins,
        quantity="metallicity",
        divide_by_binsize=divide_by_binsize,
        rate_type_hdf5_key=rate_type_hdf5_key,
        sorted_keys=sorted_keys,
        num_procs=num_procs,
    )

    # Put all the results in lists
    total_redshift_list = []
    total_rate_list = []

    for redshift_value in res_dict:
        total_redshift_list.append(redshift_value)
        total_rate_list.append(list(res_dict[redshift_value]))

    #
    total_redshift_array = np.array(total_redshift_list)
    total_rate_array = np.array(total_rate_list)

    ###########################################
    return {
        "total_redshift_array": total_redshift_array,
        "total_rate_array": total_rate_array,
    }


def plot_convolved_rate_density_per_metallicity(
    convolution_dataset_hdf5_filename,
    dco_type,
    rate_type,
    divide_by_binsize=False,
    querylist=None,
    time_bin_key=None,
    contourlevels=None,
    linestyles_contourlevels=None,
    add_ratio=False,
    num_procs=1,
    plot_settings=None,
):
    """
    Plot for the convolved rate density (i.e. rate per gigaparsec cubed)

    Args:
        - convolution_dataset_hdf5_filename: filename of the hdf5 file that contains all the data
        - rate_type: option to plot either merger rate or formation rate
        - mass_type: option to plot which mass

        - time_bin_key (Optional): If passed then we plot only the results of that certain time bin
    """

    linestyles_ratio_contourlevels = ["solid", "--", "-."]

    #
    if not plot_settings:
        plot_settings = {}

    # Get correct rate keyname:
    if rate_type == "merger_rate":
        rate_key = "merger_rates"
        long_name_rate_type = "Merger rate"
        rate_key_in_df = "merger_rates"
        rate_type_hdf5_key = "merger_rate_data"
    elif rate_type == "formation_rate":
        rate_key = "formation_rates"
        long_name_rate_type = "Formation rate"
        rate_key_in_df = "formation_rates"
        rate_type_hdf5_key = "formation_rate_data"

    main_result_units = number_per_gigaparsec_cubed_per_year
    if divide_by_binsize:
        main_result_units = main_result_units / u.Msun

    ####
    # Read out the data
    hdf5_file = h5py.File(convolution_dataset_hdf5_filename, "r")

    # Retrieve info
    settings = hdf5_file["settings"]
    convolution_settings = json.loads(settings["convolution_configuration"][()])
    metallicity_settings = json.loads(settings["metallicity_settings"][()])
    metallicity_bins = 10 ** np.array(
        metallicity_settings["bins_log10metallicity_values"]
    )
    hdf5_file.close()

    #
    metallicity_bins_size = np.diff(metallicity_bins)
    metallicity_centers = (metallicity_bins[1:] + metallicity_bins[:-1]) / 2

    # Calculate the data:
    results_dict = {}
    results_dict["all"] = get_data(
        hdf5_filename=convolution_dataset_hdf5_filename,
        dco_type=dco_type,
        metallicity_bins=metallicity_bins,
        rate_key_in_df=rate_key_in_df,
        rate_type_hdf5_key=rate_type_hdf5_key,
        divide_by_binsize=divide_by_binsize,
        num_procs=num_procs,
    )

    #
    max_val = results_dict["all"]["total_rate_array"].max()
    custom_min_val = 10 ** np.floor(
        np.log10(max_val) - plot_settings.get("probability_floor", 4)
    )

    norm = colors.LogNorm(vmin=custom_min_val, vmax=max_val)

    # norm_ratio = colors.LogNorm(
    #     vmin=10**np.floor(np.log10(1)-plot_settings.get('probability_floor', 4)),
    #     vmax=1
    # )
    # ratio_contourlevels = [1e-2, 1e-1, 9e-1]

    norm_ratio = colors.Normalize(vmin=0, vmax=1)
    ratio_contourlevels = [0.1, 0.5, 0.90]

    if not querylist is None:
        for query_dict in querylist:
            results_dict[query_dict["name"]] = get_data(
                hdf5_filename=convolution_dataset_hdf5_filename,
                dco_type=dco_type,
                metallicity_bins=metallicity_bins,
                rate_key_in_df=rate_key_in_df,
                rate_type_hdf5_key=rate_type_hdf5_key,
                divide_by_binsize=divide_by_binsize,
                num_procs=num_procs,
                query=query_dict["query"],
            )

    # Set up axes
    fig = plt.figure(figsize=(40, 40))

    # Get the autogenerated canvas
    amt_subsets = 0
    if not querylist is None:
        amt_subsets = len(querylist)

    fig, gs, axes_dict = return_canvas_with_subsets(
        amt_subsets, fig=fig, add_ratio_axes=add_ratio
    )

    X, Y = np.meshgrid(results_dict["all"]["total_redshift_array"], metallicity_centers)

    # Plot the results
    pcm = axes_dict["all_axis"].pcolormesh(
        X,
        Y,
        results_dict["all"]["total_rate_array"].T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )

    axes_dict["all_axis"].set_yscale("log")

    # Plot the subsets
    if not querylist is None:
        for query_i, query_dict in enumerate(querylist):
            X, Y = np.meshgrid(
                results_dict[query_dict["name"]]["total_redshift_array"],
                metallicity_centers,
            )
            pcm = axes_dict["subset_axes"][query_i].pcolormesh(
                X,
                Y,
                results_dict[query_dict["name"]]["total_rate_array"].T,
                norm=norm,
                shading="auto",
                antialiased=plot_settings.get("antialiased", False),
                rasterized=plot_settings.get("rasterized", False),
            )
            axes_dict["subset_axes"][0].set_ylabel(r"Metallicity [Z]")
            axes_dict["subset_axes"][query_i].set_title(
                "{}: {}".format(query_dict["name"], long_name_rate_type), fontsize=18
            )
            axes_dict["subset_axes"][query_i].set_yscale("log")

            if query_i > 0:
                axes_dict["subset_axes"][query_i].set_yticklabels([])

            # Add ratio plots
            if add_ratio:
                ratio_to_total = np.divide(
                    results_dict[query_dict["name"]]["total_rate_array"],
                    results_dict["all"]["total_rate_array"],
                    out=np.zeros_like(
                        results_dict[query_dict["name"]]["total_rate_array"]
                    ),
                    where=results_dict["all"]["total_rate_array"] != 0,
                )  # only divide nonzeros else 1

                ratio_to_total[ratio_to_total == 0] = np.nan
                pcm_ratio = axes_dict["ratio_axes"][query_i].pcolormesh(
                    X,
                    Y,
                    ratio_to_total.T,
                    norm=norm_ratio,
                    shading="auto",
                    antialiased=plot_settings.get("antialiased", False),
                    rasterized=plot_settings.get("rasterized", False),
                )
                axes_dict["ratio_axes"][0].set_ylabel(r"Metallicity [Z]")
                axes_dict["ratio_axes"][query_i].set_title(
                    "{}: Ratio".format(query_dict["name"]), fontsize=18
                )
                axes_dict["ratio_axes"][query_i].set_yscale("log")

                if query_i > 0:
                    axes_dict["ratio_axes"][query_i].set_yticklabels([])

    # make colorbar
    cb = matplotlib.colorbar.ColorbarBase(
        axes_dict["colorbar_axis"], norm=norm, extend="min"
    )
    cb.ax.set_ylabel(
        "Rate density [{}]".format(main_result_units.unit.to_string("latex_inline"))
    )

    if add_ratio:
        # Consider using a logscale for the ratio
        cb_ratio = matplotlib.colorbar.ColorbarBase(
            axes_dict["colorbar_axis_ratio"],
            norm=norm_ratio,
        )
        cb_ratio.ax.set_ylabel("Ratio to total")

    # Set contourlevels and lines on the colorbar
    if (
        (not contourlevels is None)
        and (not linestyles_contourlevels is None)
        and (len(contourlevels) == len(linestyles_contourlevels))
    ):
        _ = axes_dict["all_axis"].contour(
            X,
            Y,
            results_dict["all"]["total_rate_array"].T,
            levels=contourlevels,
            colors="k",
            linestyles=linestyles_contourlevels,
        )

        # Set lines on the colorbar
        for contour_i, _ in enumerate(contourlevels):
            cb.ax.plot(
                [cb.ax.get_xlim()[0], cb.ax.get_xlim()[1]],
                [contourlevels[contour_i]] * 2,
                "black",
                linestyle=linestyles_contourlevels[contour_i],
            )

        # Add contours on the subplots
        if not querylist is None:
            for query_i, query_dict in enumerate(querylist):
                _ = axes_dict["subset_axes"][query_i].contour(
                    X,
                    Y,
                    results_dict[query_dict["name"]]["total_rate_array"].T,
                    levels=contourlevels,
                    colors="k",
                    linestyles=linestyles_contourlevels,
                )

                ratio_to_total = np.divide(
                    results_dict[query_dict["name"]]["total_rate_array"],
                    results_dict["all"]["total_rate_array"],
                    out=np.zeros_like(
                        results_dict[query_dict["name"]]["total_rate_array"]
                    ),
                    where=results_dict["all"]["total_rate_array"] != 0,
                )  # only divide nonzeros else 1

                if add_ratio:
                    # Add contourlines
                    if (
                        (not ratio_contourlevels is None)
                        and (not linestyles_ratio_contourlevels is None)
                        and (
                            len(ratio_contourlevels)
                            == len(linestyles_ratio_contourlevels)
                        )
                    ):
                        _ = axes_dict["ratio_axes"][query_i].contour(
                            X,
                            Y,
                            ratio_to_total.T,
                            levels=ratio_contourlevels,
                            colors="k",
                            linestyles=linestyles_ratio_contourlevels,
                        )

                    # Set lines on the colorbar
                    for contour_i, _ in enumerate(ratio_contourlevels):
                        cb_ratio.ax.plot(
                            [cb_ratio.ax.get_xlim()[0], cb_ratio.ax.get_xlim()[1]],
                            [ratio_contourlevels[contour_i]] * 2,
                            "black",
                            linestyle=linestyles_ratio_contourlevels[contour_i],
                        )

    ###
    #
    axes_dict["all_axis"].set_xlabel(r"Redshift")
    axes_dict["all_axis"].set_ylabel(r"Metallicity [Z]")

    #
    title_text = "Intrinsic {} density of merging {} systems\nper metallicity over cosmic time".format(
        long_name_rate_type.lower(), dco_type
    )
    axes_dict["all_axis"].set_title(title_text, fontsize=20)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


matplotlib.use("Agg")
