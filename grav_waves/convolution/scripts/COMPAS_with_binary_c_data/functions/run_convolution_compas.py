"""
Function to run the convolution via the COMPAS code
"""

import os
import sys
import pickle
import shutil

from grav_waves.settings import logger

import h5py

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.gridspec as gridspec
from matplotlib import rc
import astropy.units as u
from astropy.cosmology import WMAP9 as cosmology
from scipy import stats
from scipy.optimize import newton
import warnings

# Import COMPAS root directory and set data
compasRootDir = "/home/david/projects/binary_c_root/other_codes/COMPAS/utils"
imageDir = compasRootDir + "/postProcessing/Tutorial/media/"

# Import COMPAS specific scripts
sys.path.append(compasRootDir + "/PythonScripts/CosmicIntegration/")
import ClassCOMPAS, ClassMSSFR, ClassCosmicIntegrator
import FastCosmicIntegration as FCI
import selection_effects


def run_convolution_compas(
    source_filename,
    target_filename,
    use_binary_c_weight_method=False,
    binary_c_weights_column=None,
):
    """
    Function to run the convolution via the compas code. Current all the configuration happens in here
    """

    ########################
    # Copy the input file to the output file location

    #
    if os.path.isfile(target_filename):
        os.remove(target_filename)
    shutil.copy(
        source_filename, target_filename
    )  # dst can be a folder; use shutil.copy2() to preserve timestamp

    ########################
    # Create the cosmic integrator class and configure
    logger.debug("Creating Cosmic Integrator class")

    CI = ClassCosmicIntegrator.CosmicIntegrator(
        pathCOMPAS=source_filename,
        Cosmology="WMAP",
        hubbleConstant=67.8,
        omegaMatter=0.308,
        redshiftFirstSFR=10.0,
        minRedshift=0.0,
        maxRedshift=10.0,
        nrRedshiftBins=100,
        # maxRedshift=2,
        # nrRedshiftBins=20,
        RedshiftTabulated=True,
        RedshiftTabulatedResolution=100000,
        GWdetector_sensitivity="O1",
        GWdetector_snrThreshold=8,
        verbose=False,
        use_binary_c_weight_method=use_binary_c_weight_method,
    )

    ########################
    # configure the data class
    logger.debug("Configuring data class")

    CI.COMPAS.Mlower = 7.0
    CI.COMPAS.Mupper = 300.0
    CI.COMPAS.binaryFraction = 0.7

    # Calculate the mass evolved for each metallicity
    CI.COMPAS.setGridAndMassEvolved()

    # which DCOs I want
    CI.COMPAS.setCOMPASDCOmask(types="BBH", pessimistic=True)

    # Check if we have any system meeting the criteria
    print("Number of DCO systems = ", np.sum(CI.COMPAS.DCOmask))
    CI.COMPAS.setCOMPASData()

    ####
    # NOTE: DAVID: flag to set the column for the weights
    if use_binary_c_weight_method:
        CI.COMPAS.set_binary_c_weights(binary_c_weights_column)

    ########################
    # Create the MSSFR class and set a metallicity grid
    logger.debug("Configuring MSSFR")

    # Pick the SFR model
    CI.MSSFR.SFRprescription = "Neijssel et al. (2019)"

    # metallicity
    CI.MSSFR.Zprescription = "logNormal"
    CI.MSSFR.logNormalPrescription = "Neijssel Phenomenological"

    # Use the grid of the simulation
    CI.MSSFR.metallicityGrid = CI.COMPAS.metallicityGrid
    CI.MSSFR.calculateMetallicityBinEdges()

    # Set the birth for all systems for all redshifts
    logger.debug("Calculating birth times and redshifts")
    CI.setBirthTimesAnd2Darrays()

    # Set all the quantities we need to compare the SFR grid
    CI.set_output_quantities()

    redshift_bin_centers = CI.redshift_bin_centers
    redshift_bin_edges = CI.redshift_bin_edges
    metallicity_bin_centers = CI.metallicity_bin_centers
    metallicity_bin_edges = CI.metallicity_bin_edges

    starformation_array = CI.starformation_array
    metallicity_fraction_array = CI.metallicity_fraction_array
    metallicity_weighted_starformation_array = (
        CI.metallicity_weighted_starformation_array
    )

    # everthing is set so now so calculate :)
    logger.debug("Running convolution")
    CI.cosmologicalIntegration()

    ########################
    # Write results into the output file

    # add groups to
    target_h5file = h5py.File(target_filename, "a")

    # Rate data
    merger_rate = CI.PerSystemPerRedshift_ratesIntrinsic
    detection_rate = CI.PerSystemPerRedshift_ratesObserved

    # Write the results to the file
    target_h5file.create_dataset(
        "/convolution_data/detection_rate", data=detection_rate
    )
    target_h5file.create_dataset("/convolution_data/merger_rate", data=merger_rate)

    ####
    # SFR and grid data
    target_h5file.create_dataset(
        "/convolution_settings/redshift_bin_centers", data=redshift_bin_centers
    )
    target_h5file.create_dataset(
        "/convolution_settings/redshift_bin_edges", data=redshift_bin_edges
    )
    target_h5file.create_dataset(
        "/convolution_settings/metallicity_bin_centers", data=metallicity_bin_centers
    )
    target_h5file.create_dataset(
        "/convolution_settings/metallicity_bin_edges", data=metallicity_bin_edges
    )

    #
    target_h5file.create_dataset(
        "/convolution_settings/starformation_array", data=starformation_array
    )
    target_h5file.create_dataset(
        "/convolution_settings/metallicity_fraction_array",
        data=metallicity_fraction_array,
    )
    target_h5file.create_dataset(
        "/convolution_settings/metallicity_weighted_starformation_array",
        data=metallicity_weighted_starformation_array,
    )

    target_h5file.create_dataset(
        "/convolution_settings/DCOmask", data=CI.COMPAS.DCOmask
    )

    target_h5file.close()

    logger.debug("Finished")
