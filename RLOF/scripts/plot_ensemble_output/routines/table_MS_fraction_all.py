"""
Function to generate the table that show the fraction of mass transfer onto MS compared to all
"""

import os
import json
import pandas as pd

from ensemble_functions.ensemble_functions import (
    get_recursive_sum,
    merge_all_subdicts,
    query_dict,
)
from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)
from grav_waves.settings import convolution_settings


def generate_table_MS_fraction_all(
    dataset_dict,
    output_filename,
    testing,
    caption="",
    label="",
    global_query_dict=None,
    verbose=0,
):
    """
    Function to create the table to shows the fraction of MS to all MT
    """

    if verbose:
        print(
            "Running generate_table_MS_fraction_all\n\toutput: {}".format(
                output_filename
            )
        )

        if global_query_dict:
            print(
                "\twith global query: {}".format(global_query_dict["global_query_name"])
            )
            print(
                "\t\tqueries: {}".format(
                    ", ".join(
                        [
                            "{} between {}".format(
                                query["parameter_key"], query["value_range"]
                            )
                            for query in global_query_dict["query_list"]
                        ]
                    )
                )
            )

    #
    result_dict = {}

    #
    for key in dataset_dict.keys():
        if verbose:
            print("\tZ = {}".format(key))
        current_dataset = dataset_dict[key]

        # Get the correct ensemble data from the dict
        probability_to_yield_conversion_factor = (
            convolution_settings["binary_fraction"]
            * convolution_settings["mass_in_stars"]
        )
        ensemble_data = get_ensemble_data(
            current_dataset,
            testing,
            probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
        )["RLOF"]["fraction_separation_massratio"]

        # select mass and time
        if verbose:
            print("\t\tGetting mass and time subsets")
        mass_all = ensemble_data["mass"]
        time_all = ensemble_data["time"]

        if global_query_dict:
            if verbose:
                print("\tPerforming queries")
            for query in global_query_dict["query_list"]:
                if verbose:
                    print(
                        "\t\tPerforming query {}".format(
                            "{} between {}".format(
                                query["parameter_key"], query["value_range"]
                            )
                        )
                    )
                mass_all = query_dict(
                    mass_all, query["parameter_key"], query["value_range"]
                )
                time_all = query_dict(
                    time_all, query["parameter_key"], query["value_range"]
                )

        # Select stellar type accretor only
        if verbose:
            print("\t\tSelecting accretor only subsets")
        mass_ms_only = query_dict(mass_all, "st_acc", [0.5, 1.5])
        time_ms_only = query_dict(time_all, "st_acc", [0.5, 1.5])

        # Get totals
        if verbose:
            print("\t\tGetting totals")
        total_mass_all = get_recursive_sum(mass_all)
        total_time_all = get_recursive_sum(time_all)

        total_mass_ms_only = get_recursive_sum(mass_ms_only)
        total_time_ms_only = get_recursive_sum(time_ms_only)

        # Get fractions
        if verbose:
            print("\t\tGetting fractions")
        fraction_mass_ms_only = (
            total_mass_ms_only / total_mass_all if total_mass_all != 0 else 0
        )
        fraction_time_ms_only = (
            total_time_ms_only / total_time_all if total_time_all != 0 else 0
        )

        # Store in dict
        result_dict["$Z = {}$".format(key)] = [
            fraction_mass_ms_only,
            fraction_time_ms_only,
        ]

    #
    column_names = ["Mass weighted", "Time weighted"]

    # put in dataframe:
    result_df = pd.DataFrame.from_dict(
        result_dict, columns=column_names, orient="index"
    )
    result_df = result_df.transpose()

    #
    caption = caption
    label = label

    # Create latex string
    latex_string = result_df.to_latex(
        index=True, label=label, caption=caption, float_format="%.2f", escape=False
    )
    # print(latex_string)

    # Export
    with open(output_filename, "w") as output_filehandle:
        output_filehandle.write(latex_string)
