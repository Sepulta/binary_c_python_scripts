"""
script to try out varying the metallicity from within the BCP object

so, a sampling of metallicity values between some values. I could try to do that randomly, and have a constant probability factor, or just sample a grid without the assigning more
"""

from binarycpython import Population

from david_phd_functions.binaryc.personal_defaults import personal_defaults

# Set up population
test_pop = Population()
# test_pop.set(C_logging_code=custom_logging_string)

# Load personal defaults
test_pop.set(**personal_defaults)

# Set resolution
resolution = {"M_1": 300, "metallicity": 12}


# Add the mass: Zoomed in
test_pop.add_grid_variable(
    name="log10metallicity",
    longname="Metallicity",
    valuerange=[-4, -1.6],
    resolution="{}".format(resolution["metallicity"]),
    spacingfunc="const(-4, -1.6, {})".format(resolution["metallicity"]),
    precode="metallicity=10**log10metallicity",
    probdist="number(1)",
    dphasevol="log10metallicity",
    parameter_name="metallicity",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
)


# Add the mass: Zoomed in
test_pop.add_grid_variable(
    name="lnm1",
    longname="Primary mass",
    valuerange=[100, 150],
    resolution="{}".format(resolution["M_1"]),
    spacingfunc="const(math.log(100), math.log(150), {})".format(resolution["M_1"]),
    precode="M_1=math.exp(lnm1)",
    probdist="three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 301, -1.3, -2.3, -2.3)*M_1",
    dphasevol="dlnm1",
    parameter_name="M_1",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
)

# Add situational settings
test_pop.set(
    verbosity=2,
    num_cores=2,
    # parse_function=parse_function,
    BH_prescription=4,
    wind_mass_loss=2,  # 0 for no wind mass loss. 1 for hurley 2002, 2 for schneider 2018. 3 for binaryc  2021
    ppisn_prescription=1,
    save_pre_events_stardata=1,  # To catch the pre-event stuff
    multiplicity=1,
)

# Get version info of the binary_c build
version_info = test_pop._return_binary_c_version_info(parsed=True)
