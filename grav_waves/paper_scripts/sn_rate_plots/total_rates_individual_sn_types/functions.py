"""
Utility functions for the SN rates plot
"""

import h5py
import pandas as pd
import numpy as np

SN_type_dict = {
    0: "None",
    1: "IA_He",
    2: "IA_ELD",
    3: "IA_CHAND",
    4: "AIC",
    5: "ECAP",
    6: "IA_He_Coal",
    7: "IA_CHAND_Coal",
    8: "NS_NS",
    9: "GRB_COLLAPSAR",
    10: "HeStarIa",
    11: "IBC",
    12: "II",
    13: "IIa",
    14: "WDKICK",
    15: "TZ",
    16: "AIC_BH",
    17: "BH_BH",
    18: "BH_NS",
    19: "IA_Hybrid_HeCOWD",
    20: "IA_Hybrid_HeCOWD_subluminous",
    21: "PPISN",
    22: "PISN",
    23: "PHDIS",
}
inverse_SN_type_dict = {value: key for key, value in SN_type_dict.items()}


def combine_dicts(dict_1, dict_2):
    """
    Function to add the values in two dictionaries
    """

    new_dict = {}

    # Add values from dict_1
    for key, value in dict_1.items():
        new_dict[key] = value

    # Add values from dict_2
    for key, value in dict_2.items():
        if not key in new_dict:
            new_dict[key] = 0
        new_dict[key] += value

    #
    return new_dict


def get_redshift_and_values(rate_dict):
    """
    Function to turn dict into redshift and values
    """

    rate_dict_keys = np.array(sorted(list(rate_dict.keys()), key=float))
    rate_dict_redshift = [float(el) for el in rate_dict_keys]
    rate_dict_values = [rate_dict[el] for el in rate_dict_keys]

    return np.array(rate_dict_redshift), np.array(rate_dict_values)


def readout_binary_dco_rates(dco_convolution_filename, filtered=False):
    """
    Function to read out the binary dco rates
    """

    #
    total_BHBH_rate_dict = {}
    total_BHNS_rate_dict = {}
    total_NSBH_rate_dict = {}
    total_NSNS_rate_dict = {}

    # Get dataframe
    dco_convolution_dataframe = pd.read_hdf(
        dco_convolution_filename, "/data/combined_dataframes"
    )

    # Get BHBH DCO
    BHBH_indices = dco_convolution_dataframe.eval(
        "stellar_type_1 == 14 & stellar_type_2 == 14"
    ).to_numpy()
    BHNS_indices = dco_convolution_dataframe.eval(
        "stellar_type_1 == 14 & stellar_type_2 == 13"
    ).to_numpy()
    NSBH_indices = dco_convolution_dataframe.eval(
        "stellar_type_1 == 13 & stellar_type_2 == 14"
    ).to_numpy()
    NSNS_indices = dco_convolution_dataframe.eval(
        "stellar_type_1 == 13 & stellar_type_2 == 13"
    ).to_numpy()

    # Handle filtering
    if filtered:
        # TODO: Add filter
        pass

    #
    dco_convolution_datafile = h5py.File(dco_convolution_filename)
    redshifts = np.array(
        sorted(list(dco_convolution_datafile["data/merger_rate"].keys()), key=float)
    )

    for redshift_key in redshifts:
        redshift = float(redshift_key)

        # Readout rate data
        rate_data = dco_convolution_datafile[
            "data/merger_rate/{}".format(redshift_key)
        ][()]

        # get BHBH rate
        BHBH_rate = rate_data[BHBH_indices]
        total_BHBH_rate_dict[redshift] = np.sum(BHBH_rate)

        # get BHNS rate
        BHNS_rate = rate_data[BHNS_indices]
        total_BHNS_rate_dict[redshift] = np.sum(BHNS_rate)

        # get NSBH rate
        NSBH_rate = rate_data[NSBH_indices]
        total_NSBH_rate_dict[redshift] = np.sum(NSBH_rate)

        # get NSNS rate
        NSNS_rate = rate_data[NSNS_indices]
        total_NSNS_rate_dict[redshift] = np.sum(NSNS_rate)

    # Combine dicts:
    combined_total_BHNS_rate_dict = combine_dicts(
        total_BHNS_rate_dict, total_NSBH_rate_dict
    )

    # get BHBH rate data in correct shape
    (
        total_BHBH_rate_dict_redshift,
        total_BHBH_rate_dict_values,
    ) = get_redshift_and_values(total_BHBH_rate_dict)

    # get BHNS rate data in correct shape
    (
        combined_total_BHNS_rate_dict_redshift,
        combined_total_BHNS_rate_dict_values,
    ) = get_redshift_and_values(combined_total_BHNS_rate_dict)

    # get NSNS rate data in correct shape
    (
        total_NSNS_rate_dict_redshift,
        total_NSNS_rate_dict_values,
    ) = get_redshift_and_values(total_NSNS_rate_dict)

    #
    return_dict = {
        "bhbh": {
            "redshifts": total_BHBH_rate_dict_redshift,
            "rates": total_BHBH_rate_dict_values,
            "label": "BHBH merger",
        },
        "bhns": {
            "redshifts": combined_total_BHNS_rate_dict_redshift,
            "rates": combined_total_BHNS_rate_dict_values,
            "label": "BHNS merger",
        },
        "nsns": {
            "redshifts": total_NSNS_rate_dict_redshift,
            "rates": total_NSNS_rate_dict_values,
            "label": "NSNS merger",
        },
    }

    #
    return return_dict


def readout_sn_rates(
    sn_convolution_filename, filter_function=False, event_based=False, filter_fb=False
):
    """
    Function to read out binary sn rates
    """

    total_IBC_rate_dict = {}
    total_II_rate_dict = {}
    total_PPISN_rate_dict = {}
    total_PISN_rate_dict = {}

    # Get dataframe
    sn_convolution_dataframe = pd.read_hdf(
        sn_convolution_filename, "/data/combined_dataframes"
    )

    if event_based:
        sn_type_column_string = "SN_type"
    else:
        sn_type_column_string = "post_SN_SN_type"

    if filter_fb:
        filter_fb_string = "& fallback_fraction < 1"
    else:
        filter_fb_string = ""

    # get IBC SN
    IBC_indices = sn_convolution_dataframe.eval(
        "{} == {}{}".format(
            sn_type_column_string, inverse_SN_type_dict["IBC"], filter_fb_string
        )
    ).to_numpy()

    # get II SN
    II_indices = sn_convolution_dataframe.eval(
        "{} == {}{}".format(
            sn_type_column_string, inverse_SN_type_dict["II"], filter_fb_string
        )
    ).to_numpy()

    # get PPISN SN
    PPISN_indices = sn_convolution_dataframe.eval(
        "{} == {}".format(sn_type_column_string, inverse_SN_type_dict["PPISN"])
    ).to_numpy()

    # get PISN SN
    PISN_indices = sn_convolution_dataframe.eval(
        "{} == {}".format(sn_type_column_string, inverse_SN_type_dict["PISN"])
    ).to_numpy()

    # Handle filtering
    if filter_function is not None:
        # TODO: Add filter
        pass

    # Open HDF5 file again
    sn_convolution_datafile = h5py.File(sn_convolution_filename)

    #
    redshifts = np.array(
        sorted(list(sn_convolution_datafile["data/formation_rate"].keys()), key=float)
    )

    for redshift_key in redshifts:
        redshift = float(redshift_key)

        # Readout rate data
        rate_data = sn_convolution_datafile[
            "data/formation_rate/{}".format(redshift_key)
        ][()]

        # get IBC rate
        IBC_rate = rate_data[IBC_indices]
        total_IBC_rate_dict[redshift] = np.sum(IBC_rate)

        # get II rate
        II_rate = rate_data[II_indices]
        total_II_rate_dict[redshift] = np.sum(II_rate)

        # get PPISN rate
        PPISN_rate = rate_data[PPISN_indices]
        total_PPISN_rate_dict[redshift] = np.sum(PPISN_rate)

        # get PISN rate
        PISN_rate = rate_data[PISN_indices]
        total_PISN_rate_dict[redshift] = np.sum(PISN_rate)

    # Combine CC rates
    combined_total_CC_rate_dict = combine_dicts(total_IBC_rate_dict, total_II_rate_dict)

    # get IBC rate data in correct shape
    (
        combined_total_CC_rate_dict_redshift,
        combined_total_CC_rate_dict_values,
    ) = get_redshift_and_values(combined_total_CC_rate_dict)

    # get PPISN rate data in correct shape
    (
        total_PPISN_rate_dict_redshift,
        total_PPISN_rate_dict_values,
    ) = get_redshift_and_values(total_PPISN_rate_dict)

    # get PPISN rate data in correct shape
    (
        total_PISN_rate_dict_redshift,
        total_PISN_rate_dict_values,
    ) = get_redshift_and_values(total_PISN_rate_dict)

    #
    return_dict = {
        "CC": {
            "redshifts": combined_total_CC_rate_dict_redshift,
            "rates": combined_total_CC_rate_dict_values,
            "label": "CCSN",
        },
        "PPISN": {
            "redshifts": total_PPISN_rate_dict_redshift,
            "rates": total_PPISN_rate_dict_values,
            "label": "PPISN",
        },
        "PISN": {
            "redshifts": total_PISN_rate_dict_redshift,
            "rates": total_PISN_rate_dict_values,
            "label": "PISN",
        },
    }

    #
    return return_dict


def calculate_min_max_values(all_rates_dict):
    """
    Function to calculate the min and max values of all the rates present in the dictionary
    """

    all_rates_list = []

    # Loop over the datasets
    for dataset in all_rates_dict:
        # Loop over the DCO types
        for dco_type in all_rates_dict[dataset]["dco_rates"]:
            all_rates_list.append(
                all_rates_dict[dataset]["dco_rates"][dco_type]["rates"]
            )

        # Loop over the SN types
        for sn_type in all_rates_dict[dataset]["sn_rates"]:
            all_rates_list.append(all_rates_dict[dataset]["sn_rates"][sn_type]["rates"])

    #
    all_rates = np.array(all_rates_list)

    #
    min_val = np.min(all_rates[all_rates > 0])
    max_val = np.max(all_rates[all_rates > 0])

    return min_val, max_val
