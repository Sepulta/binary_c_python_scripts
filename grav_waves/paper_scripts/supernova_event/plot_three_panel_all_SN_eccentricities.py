"""
Function to handle the plotting with all pre/post SN, first PPISN and second PPISN

TODO: find the second max value and set the vmax to that
"""

import os
import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

from grav_waves.functions.events import merge_with_event_dataframe, print_rows_and_cols

custom_mpl_settings.load_mpl_rc()


this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def get_vmin(hist, custom_min):
    """
    Function to get the minimum value of a histogram OR the custom min if thats
    """

    hist_min = np.min(hist[hist > 0])

    return np.max([custom_min, hist_min])


def example_all_ppisn_pre_post_eccentricity(
    total_SN_datafile, testing=False, plot_settings={}
):
    """
    Example function to combine the DCO data with supernova events and
    """

    # Set bins
    numbins = 25
    x_bins = np.linspace(0, 1, numbins)
    y_bins = np.linspace(0, 1, numbins)

    #
    all_SN_hists = []
    first_SN_hists = []
    second_SN_hists = []

    # use chunks!
    chunksize = 10**6
    for chunk_i, chunk in enumerate(
        pd.read_csv(
            total_SN_datafile,
            chunksize=chunksize,
            usecols=[
                "pre_SN_ecc",
                "SN_type",
                "post_SN_ecc",
                "SN_counter",
                "probability",
            ],
            sep="\s+",
        )
    ):
        print("Handling chunk {}".format(chunk_i))

        # Select PPISN and those that are bound
        ppisn_SN = chunk.query("SN_type == 21")
        bound_PPISN_SN = ppisn_SN.query("pre_SN_ecc >= 0")

        # Select first and second
        first_PPISN_SN = bound_PPISN_SN.query("SN_counter == 1")
        second_PPISN_SN = bound_PPISN_SN.query("SN_counter == 2")

        # Get all PPISN SN values
        chunked_all_hist, _, _ = np.histogram2d(
            bound_PPISN_SN["pre_SN_ecc"],
            bound_PPISN_SN["post_SN_ecc"],
            bins=[x_bins, y_bins],
            weights=bound_PPISN_SN["probability"],
        )
        all_SN_hists.append(chunked_all_hist)

        # First PPISN
        chunked_first_hist, _, _ = np.histogram2d(
            first_PPISN_SN["pre_SN_ecc"],
            first_PPISN_SN["post_SN_ecc"],
            bins=[x_bins, y_bins],
            weights=first_PPISN_SN["probability"],
        )
        first_SN_hists.append(chunked_first_hist)

        # Second PPISN
        chunked_second_hist, _, _ = np.histogram2d(
            second_PPISN_SN["pre_SN_ecc"],
            second_PPISN_SN["post_SN_ecc"],
            bins=[x_bins, y_bins],
            weights=second_PPISN_SN["probability"],
        )
        second_SN_hists.append(chunked_second_hist)

        if testing:
            if chunk_i > 3:
                break

    ##########
    # Combine the histograms
    combined_all_SN_hist = np.zeros(all_SN_hists[0].shape)
    combined_first_SN_hist = np.zeros(first_SN_hists[0].shape)
    combined_second_SN_hist = np.zeros(second_SN_hists[0].shape)

    for _, (all_SN_hist, first_SN_hist, second_SN_hist) in enumerate(
        zip(all_SN_hists, first_SN_hists, second_SN_hists)
    ):
        combined_all_SN_hist += all_SN_hist
        combined_first_SN_hist += first_SN_hist
        combined_second_SN_hist += second_SN_hist

    ######
    # Set norm
    second_max_val = sorted(combined_all_SN_hist.flatten())[-2]
    norm = colors.LogNorm(
        vmin=get_vmin(combined_all_SN_hist, plot_settings.get("min_prob_val", 1e-8)),
        vmax=second_max_val,
    )

    ##########
    # Set up figure logic
    fig = plt.figure(figsize=(26, 8))
    gs = fig.add_gridspec(nrows=1, ncols=10)

    all_SN_axis = fig.add_subplot(gs[:, 0:3])
    first_SN_axis = fig.add_subplot(gs[:, 3:6])
    second_SN_axis = fig.add_subplot(gs[:, 6:9])

    ax_cb = fig.add_subplot(gs[:, -1:])

    # make bincenters
    x_bincenters = (x_bins[1:] + x_bins[:-1]) / 2
    y_bincenters = (y_bins[1:] + y_bins[:-1]) / 2

    X, Y = np.meshgrid(x_bincenters, y_bincenters)

    # All SN
    _ = all_SN_axis.pcolormesh(
        X,
        Y,
        combined_all_SN_hist.T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # First SN
    _ = first_SN_axis.pcolormesh(
        X,
        Y,
        combined_first_SN_hist.T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # Second SN
    _ = second_SN_axis.pcolormesh(
        X,
        Y,
        combined_second_SN_hist.T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(ax_cb, norm=norm, extend="both")
    cbar.ax.set_ylabel(r"Number per formed solar mass")

    ###
    # Make-up
    first_SN_axis.set_xlabel("Pre-SN eccentricity")
    all_SN_axis.set_ylabel("Post-SN eccentricity")

    first_SN_axis.set_yticklabels([])
    second_SN_axis.set_yticklabels([])

    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    ########
    # Configure

    # Settings
    min_prob_val = 1e-12
    show_plot = False
    testing = False

    # Result root
    result_root = "/home/david/data_projects/binary_c_data/GRAV_WAVES/server_results/"

    # Plot output dir
    output_dir = os.path.join(
        this_file_dir,
        "plots/EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED",
    )

    #
    total_sn_datafile = os.path.join(
        result_root,
        "EVENTS_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/combined_total_SN_events.dat",
    )
    # total_sn_datafile = os.path.join(
    #     result_root,
    #     "EVENTS_SEMI_HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/dco_convolution_results/combined_total_SN_BINARY_events.dat",
    # )

    #
    basename = "2222pre_post_ppisn_ecc.pdf"
    example_all_ppisn_pre_post_eccentricity(
        total_sn_datafile,
        testing=testing,
        plot_settings={
            "min_prob_val": min_prob_val,
            "show_plot": show_plot,
            "output_name": os.path.join(output_dir, basename),
        },
    )
