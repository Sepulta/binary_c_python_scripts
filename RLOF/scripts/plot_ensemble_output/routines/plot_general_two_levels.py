"""
General function to plot two (non-conscecutive) parameter levels.

Requires getting the data.
"""

import numpy as np

import matplotlib
import matplotlib.pyplot as plt

from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)
from RLOF.scripts.plot_ensemble_output.functions import (
    return_string_querylist,
    get_bins_bincenters,
    handle_querylist,
    add_2d_density_plot_to_axis,
)
from RLOF.scripts.plot_ensemble_output.settings import parameter_name_dict

from ensemble_functions.ensemble_functions import (
    merge_all_subdicts,
    flatten_data_ensemble2d,
    merge_and_recurse_two_levels,
)
from grav_waves.settings import convolution_settings

#
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()
matplotlib.rc("text", usetex=False)


def plot_general_two_levels(
    dataset,
    first_parameter,
    second_parameter,
    weighttype,
    testing,
    base_querylist=None,
    querylist=None,
    plot_settings=None,
    contourlevels=None,
    contour_linestyles=None,
    verbose=0,
    diff_max_threshold=None,
):
    """
    Function to make a 2d density plot of two given parameters from the ensemble

    TODO: add some of the query info to the plot
    TODO: add option to do gaussian blur
    TODO: add min level override
    """

    #
    if not plot_settings:
        plot_settings = {}
    if not weighttype in ["mass", "time"]:
        raise ValueError(
            "Weighttype ({}) must be in {}".format(weighttype, ["mass", "time"])
        )

    # Get the data
    if verbose:
        print("\t\tLoading data")
    probability_to_yield_conversion_factor = (
        convolution_settings["binary_fraction"] * convolution_settings["mass_in_stars"]
    )
    ensemble_data = get_ensemble_data(
        dataset,
        testing,
        probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
    )["RLOF"]["fraction_separation_massratio"][weighttype]

    #######
    # Querying

    # Run base-query on the data
    # Do base query
    if not base_querylist is None:
        ensemble_data = handle_querylist(ensemble_data, base_querylist, "base-query")

    # Run query on the data
    if not querylist is None:
        queried_data = ensemble_data
        queried_data = handle_querylist(queried_data, querylist, "query")

    ########
    # Selecting the data and flatten

    # Select the correct levels and merge/recurse sum
    merged_ensemble_data = merge_and_recurse_two_levels(
        ensemble_data, first_parameter, second_parameter
    )

    # Flatten the data
    flattened_data = flatten_data_ensemble2d(
        merged_ensemble_data, first_parameter, second_parameter
    )
    total = np.sum(flattened_data[-1])
    flattened_data[-1] = flattened_data[-1] / total

    # Get the values
    min_val = flattened_data[-1][flattened_data[-1] != 0].min()
    max_val = flattened_data[-1][flattened_data[-1] != 0].max()

    # Get bins and bincenters
    first_parameter_total_bins, _ = get_bins_bincenters(
        merged_ensemble_data, first_parameter
    )
    second_parameter_total_bins, _ = get_bins_bincenters(
        merge_all_subdicts(merged_ensemble_data[first_parameter]), second_parameter
    )

    # Do the same for the queried results
    if not querylist is None:
        # Select the correct levels and merge/recurse sum
        merged_queried_data = merge_and_recurse_two_levels(
            queried_data, first_parameter, second_parameter
        )

        # Flatten the data
        flattened_queried_data = flatten_data_ensemble2d(
            merged_queried_data, first_parameter, second_parameter
        )
        flattened_queried_data[-1] = flattened_queried_data[-1] / total

        min_val = np.min(
            [min_val, flattened_queried_data[-1][flattened_queried_data[-1] != 0].min()]
        )

    if not diff_max_threshold is None:
        min_val = 10 ** (np.log10(max_val) - diff_max_threshold)

    ########
    # Do plotting
    fig = plt.figure(figsize=(20, 10))

    #
    gs = fig.add_gridspec(nrows=1, ncols=10)

    if querylist is None:
        ax = fig.add_subplot(gs[:, :-2])
        title_axis = ax
        labelpad = 0
    else:
        fig.subplots_adjust(hspace=0)

        ax = fig.add_subplot(gs[:, :4])
        ax_query = fig.add_subplot(gs[:, 4:8])

        # invisible axis
        axes_invisible = fig.add_subplot(gs[:, :-2], frame_on=False)
        axes_invisible.set_xticks([])
        axes_invisible.set_yticks([])
        title_axis = axes_invisible
        labelpad = 20

    # Colorbar axis
    ax_cb = fig.add_subplot(gs[:, -1])

    # Plot the total
    fig, ax = add_2d_density_plot_to_axis(
        fig,
        ax,
        flattened_data,
        first_parameter_total_bins,
        second_parameter_total_bins,
        min_val,
        max_val,
        contourlevels,
        contour_linestyles,
    )

    # Plot the queried
    if not querylist is None:
        fig, ax_query = add_2d_density_plot_to_axis(
            fig,
            ax_query,
            flattened_queried_data,
            first_parameter_total_bins,
            second_parameter_total_bins,
            min_val,
            max_val,
            contourlevels,
            contour_linestyles,
        )

    # Set up a colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        cmap=matplotlib.cm.viridis,
        norm=matplotlib.colors.LogNorm(vmin=min_val, vmax=max_val),
        extend="min",
    )
    cbar.ax.set_ylabel("Normalised probability")

    for contour_i, _ in enumerate(contourlevels):
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [contourlevels[contour_i]] * 2,
            "black",
            linestyle=contour_linestyles[contour_i],
        )

    #####
    # Make up

    # Axes labels
    ax.set_xlabel(r"{}".format(parameter_name_dict[first_parameter]))
    ax.set_ylabel(r"{}".format(parameter_name_dict[second_parameter]))

    if not querylist is None:
        ax_query.set_xlabel(r"{}".format(parameter_name_dict[first_parameter]))
        ax_query.set_yticklabels([])

    # Set title
    title_axis.set_title(
        "Probability distribution of {} and {}{}{}{}".format(
            parameter_name_dict[first_parameter],
            parameter_name_dict[second_parameter],
            "\n" if (base_querylist is not None) or (querylist is not None) else "",
            " with base query {}".format(return_string_querylist(base_querylist))
            if base_querylist is not None
            else "",
            " with query {}".format(return_string_querylist(querylist))
            if querylist is not None
            else "",
        ),
        fontsize=24,
        pad=labelpad,
    )

    # Round off
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


# #
# population_datasets_sana = generate_ensemble_dataset_dict('/home/david/projects/binary_c_root/results/RLOF/LOW_RES_RLOF_SANA_2021_ENSEMBLE_fractions')
# weighttype='time'
# plot_general_two_levels(
#     population_datasets_sana['datasets']['0.02'],
#     first_parameter='mt_rate',
#     second_parameter='r_acc_sep',
#     weighttype=weighttype,
#     testing=False,
#     base_querylist=[{'keyname': 'st_acc', 'valuerange': [0.5, 1.5]}, {'keyname': 'stable', 'valuerange': [-0.5, 0.5]}],

#     # querylist=[{'keyname': 'disk', 'valuerange': [0.5, 1.5]}],
#     plot_settings={'show_plot': True, 'output_name': 'test.pdf', 'runname': weighttype, 'simulation_name': "{}: total".format(population_datasets_sana['simname'])},
#     verbose=1,
#     contourlevels=[1e-5, 1e-4, 0.5 * 1e-3],
#     contour_linestyles=['solid', 'dashed', '-.'],
#     diff_max_threshold=3
# )
