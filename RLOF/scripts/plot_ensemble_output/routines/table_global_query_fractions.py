"""
This function will generate the table that contains the information to translate the queried datasets to the unqueried datasets

This function requires the query_list

It will create the fraction between queried dataset and unqueried dataset
"""

import os
import json
import copy
import pandas as pd

from ensemble_functions.ensemble_functions import (
    get_recursive_sum,
    merge_all_subdicts,
    query_dict,
)
from RLOF.scripts.generate_paper_figures_and_tables_scripts.functions import (
    generate_ensemble_dataset_dict,
    get_ensemble_data,
)
from grav_waves.settings import convolution_settings


def generate_table_global_query_fractions(
    dataset_dict,
    global_query_dict,
    output_filename,
    testing,
    caption="",
    label="",
    verbose=0,
):
    """
    Function to create the table to shows the fraction of MS to all MT
    """

    if verbose:
        print(
            "Running generate_table_query_fractions\n\toutput: {}".format(
                output_filename
            )
        )
        print("\twith global query: {}".format(global_query_dict["global_query_name"]))
        print(
            "\t\tqueries: {}".format(
                ", ".join(
                    [
                        "{} between {}".format(
                            query["parameter_key"], query["value_range"]
                        )
                        for query in global_query_dict["query_list"]
                    ]
                )
            )
        )

    #
    result_dict = {}

    #
    for key in dataset_dict.keys():
        if verbose:
            print("\tZ = {}".format(key))
        current_dataset = dataset_dict[key]

        # Get the correct ensemble data from the dict
        if verbose:
            print("\tLoading data and selecting RLOF project")
        probability_to_yield_conversion_factor = (
            convolution_settings["binary_fraction"]
            * convolution_settings["mass_in_stars"]
        )
        ensemble_data = get_ensemble_data(
            current_dataset,
            testing,
            probability_to_yield_conversion_factor=probability_to_yield_conversion_factor,
        )["RLOF"]

        #
        if verbose:
            print("\tSelecting sub-projects")
        start_ensemble = ensemble_data["starting_rlof"]
        mass_ensemble = ensemble_data["fraction_separation_massratio"]["mass"]["log10"]
        time_ensemble = ensemble_data["fraction_separation_massratio"]["time"]["log10"]

        total_start_ensemble = get_recursive_sum(start_ensemble)
        total_mass_ensemble = get_recursive_sum(mass_ensemble)
        total_time_ensemble = get_recursive_sum(time_ensemble)

        # Copy to do the querying
        if verbose:
            print("\tCopying ensembles to start querying")
        queried_start_ensemble = copy.deepcopy(start_ensemble)
        queried_mass_ensemble = copy.deepcopy(mass_ensemble)
        queried_time_ensemble = copy.deepcopy(time_ensemble)

        #
        if verbose:
            print("\tPerforming queries")
        for query in global_query_dict["query_list"]:
            if verbose:
                print(
                    "\t\tPerforming query {}".format(
                        "{} between {}".format(
                            query["parameter_key"], query["value_range"]
                        )
                    )
                )
            queried_start_ensemble = query_dict(
                queried_start_ensemble, query["parameter_key"], query["value_range"]
            )
            queried_mass_ensemble = query_dict(
                queried_mass_ensemble, query["parameter_key"], query["value_range"]
            )
            queried_time_ensemble = query_dict(
                queried_time_ensemble, query["parameter_key"], query["value_range"]
            )

        total_queried_start_ensemble = get_recursive_sum(queried_start_ensemble)
        total_queried_mass_ensemble = get_recursive_sum(queried_mass_ensemble)
        total_queried_time_ensemble = get_recursive_sum(queried_time_ensemble)

        #
        if verbose:
            print("\tGetting fractions")
        fraction_queried_start_ensemble = (
            total_queried_start_ensemble / total_start_ensemble
            if total_start_ensemble != 0
            else 0
        )
        fraction_queried_mass_ensemble = (
            total_queried_mass_ensemble / total_mass_ensemble
            if total_mass_ensemble != 0
            else 0
        )
        fraction_queried_time_ensemble = (
            total_queried_time_ensemble / total_time_ensemble
            if total_time_ensemble != 0
            else 0
        )

        # Store in dict
        result_dict["Z = {}".format(key)] = [
            fraction_queried_start_ensemble,
            fraction_queried_mass_ensemble,
            fraction_queried_time_ensemble,
        ]

    #
    column_names = ["Start", "Mass weighted", "Time weighted"]

    # put in dataframe:
    result_df = pd.DataFrame.from_dict(
        result_dict, columns=column_names, orient="index"
    )
    result_df = result_df.transpose()

    #
    caption = (
        caption
        + "Added automatically: Fraction between the queryied global datasets and the unqueried global datasets. Using query {}: {}".format(
            global_query_dict["global_query_name"].replace("_", "\\_"),
            ", ".join(
                [
                    "{} between {}".format(
                        query["parameter_key"].replace("_", "\\_"), query["value_range"]
                    )
                    for query in global_query_dict["query_list"]
                ]
            ),
        )
    )

    # Create latex string
    latex_string = result_df.to_latex(
        index=True, label=label, caption=caption, float_format="%.2e"
    )
    # print(latex_string)

    # Export
    with open(output_filename, "w") as output_filehandle:
        output_filehandle.write(latex_string)


# global_query_dict = {
#     'global_query_name': "only_massive_donors",
#     'query_list': [
#         {
#             'parameter_key': 'log10mass_donor',
#             'value_range': [1, 10]
#         },
#     ]
# }

# #
# population_datasets_sana = generate_ensemble_dataset_dict('/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions/')
# generate_table_query_fractions(population_datasets_sana, global_query_dict, 'test.tex', testing=True, label='', caption='', verbose=1)
