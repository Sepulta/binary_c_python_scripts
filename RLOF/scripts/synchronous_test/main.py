"""
Function to process the output of the binary-c run that has the information about the synchronisity and the ratio explicity to implicit information
"""

import matplotlib.pyplot as plt

import matplotlib
from matplotlib import colors
import numpy as np
from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

filename = "/home/david/projects/binary_c_root/binary_c/disk_test_system/output_disk_system.txt"
columns = [
    "time",
    "synchronicity",
    "accretion_rate",
    "dj_dt_from_donor_formula",
    "dj_dt_onto_accretor_formula",
    "dj_dt_from_donor_binaryc",
    "dj_dt_onto_accretor_binaryc",
]

data = []

with open(filename, "r") as datafile:
    for line in datafile.readlines():
        if line.startswith("HEADERLINE"):

            values = line.strip().split()[1:]
            values = [float(el) for el in values]

            data.append(values)


import pandas as pd

df = pd.DataFrame(data, columns=columns)
df = df[df.synchronicity > 1e-10]
df = df.sort_values(by="time")


plt.plot(
    df["time"], df["dj_dt_from_donor_binaryc"], "o", label="dj_dt_from_donor_binaryc"
)
plt.plot(
    df["time"], df["dj_dt_from_donor_formula"], "o", label="dj_dt_from_donor_formula"
)
plt.plot(
    df["time"],
    df["dj_dt_onto_accretor_binaryc"],
    "o",
    label="dj_dt_onto_accretor_binaryc",
)
plt.plot(
    df["time"],
    df["dj_dt_onto_accretor_formula"],
    "o",
    label="dj_dt_onto_accretor_formula",
)

plt.plot(df["time"], df["synchronicity"], "o", label="sync")
plt.legend()
plt.yscale("log")

plt.show()
