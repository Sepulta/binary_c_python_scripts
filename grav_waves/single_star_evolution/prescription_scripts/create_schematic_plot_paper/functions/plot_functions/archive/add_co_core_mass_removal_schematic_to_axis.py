import os
import numpy as np
import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.extra_plot_functions import (
    place_arrows,
    add_bracket,
)
from grav_waves.single_star_evolution.prescription_scripts.create_schematic_plot_paper.functions.plot_functions.plot_utils import (
    add_pre_SN_mass_plot,
    add_no_prescription_plot,
    add_old_prescription_plot,
    add_fiducial_plot,
    add_mass_removal_plot,
    add_core_mass_shift_plot,
    get_df_bh,
    calculate_min_max_zams_ppisn_masses,
    add_makeup_plot,
    get_dataframes,
)


from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def add_co_core_mass_removal_schematic_to_axis(
    fig,
    ax_co_core_to_remnant_mass_mass_removal,
    result_directory_mass_removal,
    old_prescription_result_directory_root,
    add_old_prescription_line=True,
    add_mass_removed_mass_removal=True,
    add_fiducial_mass_removal=True,
    add_bracket_extra_mass_loss=True,
    create_shaded_regions=True,
    create_supernova_lines=True,
):
    """
    Function to plot the CO core mass vs the remnant mass schematic plot for the additional mass removal and CO core mass shift
    """

    # Select BHs only
    old_prescription_df = old_prescription_df[old_prescription_df.stellar_type == 14]

    ########################
    # Load mass removal data
    mass_removal_fiducial_dir = os.path.join(result_directory_mass_removal, "0")
    mass_removal_dirs = {
        subdir: os.path.join(result_directory_mass_removal, subdir)
        for subdir in os.listdir(result_directory_mass_removal)
        if not subdir == "0"
    }

    #
    mass_removal_fiducial_datafile = os.path.join(
        mass_removal_fiducial_dir, "output.dat"
    )
    mass_removal_fiducial_df = pd.read_table(mass_removal_fiducial_datafile, sep="\s+")
    mass_removal_fiducial_df = mass_removal_fiducial_df.sort_values(by="zams_mass")

    #
    mass_removal_datafile = os.path.join(
        mass_removal_dirs[list(mass_removal_dirs.keys())[0]], "output.dat"
    )
    mass_removal_df = pd.read_table(mass_removal_datafile, sep="\s+")
    mass_removal_df = mass_removal_df.sort_values(by="zams_mass")

    # select only BHs
    mass_removal_fiducial_df = mass_removal_fiducial_df[
        mass_removal_fiducial_df.stellar_type == 14
    ]
    mass_removal_df = mass_removal_df[mass_removal_df.stellar_type == 14]

    # Get max value for ppisn
    zams_mass_for_max_ppisn = mass_removal_fiducial_df.loc[
        mass_removal_fiducial_df["mass"].idxmax()
    ]["pre_sn_co_core_mass"]
    fiducial_max_mass_ppisn = mass_removal_fiducial_df[
        mass_removal_fiducial_df.pre_sn_co_core_mass == zams_mass_for_max_ppisn
    ]["mass"].to_numpy()[0]
    mass_removed_max_mass_ppisn = mass_removal_df[
        mass_removal_df.pre_sn_co_core_mass == zams_mass_for_max_ppisn
    ]["mass"].to_numpy()[0]

    #########
    # Plot the mass removal sublot

    # Add old_prescription line
    if add_old_prescription_line:
        ax_co_core_to_remnant_mass_mass_removal.plot(
            old_prescription_df["pre_sn_co_core_mass"],
            old_prescription_df["mass"],
            linestyle="-.",
            alpha=0.8,
            label="Farmer et al. '19 prescription",
        )

    ################
    # Mass removal plot
    if add_mass_removed_mass_removal:
        ax_co_core_to_remnant_mass_mass_removal.plot(
            mass_removal_df["pre_sn_co_core_mass"],
            mass_removal_df["mass"],
            linestyle="--",
            alpha=0.8,
            label=r"New prescription - $\Delta$ $M_{\mathrm{CO\ core\ mass\ shift}}$",
        )

    ###############
    # CO core mass to remnant mass for new prescription without extra mass removed
    if add_fiducial_mass_removal:
        ax_co_core_to_remnant_mass_mass_removal.plot(
            mass_removal_fiducial_df["pre_sn_co_core_mass"],
            mass_removal_fiducial_df["mass"],
            # 'ro'
            color="black",
            label="New prescription",
        )

    ###########
    # Add bracket indicating the extra PPISN loss
    if add_bracket_extra_mass_loss:
        location_max_fiducial_mass = (zams_mass_for_max_ppisn, fiducial_max_mass_ppisn)
        location_max_mass_removal_mass = (
            zams_mass_for_max_ppisn,
            mass_removed_max_mass_ppisn,
        )
        location_startpoint = (
            zams_mass_for_max_ppisn + 5,
            (fiducial_max_mass_ppisn + mass_removed_max_mass_ppisn) / 2,
        )
        fig, ax_co_core_to_remnant_mass_mass_removal = add_bracket(
            fig,
            ax_co_core_to_remnant_mass_mass_removal,
            location_max_fiducial_mass,
            location_max_mass_removal_mass,
            location_startpoint,
            -1,
            linestyle="solid",
            color="black",
            linewidth=5,
        )
        ax_co_core_to_remnant_mass_mass_removal.text(
            location_startpoint[0] + 1,
            location_startpoint[1] - 1,
            s=r"$\Delta$ $M_{\mathrm{Extra\ mass\ loss}}$",
            fontsize=20,
        )

    ##############
    # Create shaded region under the main line that indicates which prescription
    if create_shaded_regions:
        # Get mass first ppisn
        first_ppisn_mass = mass_removal_fiducial_df[
            mass_removal_fiducial_df.sn_type == 21
        ].iloc[0]["pre_sn_co_core_mass"]
        fryer_area_df = mass_removal_fiducial_df[
            mass_removal_fiducial_df.pre_sn_co_core_mass < first_ppisn_mass
        ].copy(deep=True)
        new_prescription_area = mass_removal_df[
            mass_removal_df.pre_sn_co_core_mass >= first_ppisn_mass
        ].copy(deep=True)

        #
        ax_co_core_to_remnant_mass_mass_removal.fill_between(
            fryer_area_df["pre_sn_co_core_mass"],
            fryer_area_df["mass"],
            y2=0,
            color="yellow",
            alpha=alpha_regions,
        )

        #
        if add_mass_removed_mass_removal:
            ax_co_core_to_remnant_mass_mass_removal.fill_between(
                new_prescription_area["pre_sn_co_core_mass"],
                new_prescription_area["mass"],
                y2=0,
                color="pink",
                alpha=alpha_regions,
            )
        else:
            ax_co_core_to_remnant_mass_mass_removal.fill_between(
                mass_removal_fiducial_df[
                    mass_removal_fiducial_df.pre_sn_co_core_mass >= first_ppisn_mass
                ]["pre_sn_co_core_mass"],
                mass_removal_fiducial_df[
                    mass_removal_fiducial_df.pre_sn_co_core_mass >= first_ppisn_mass
                ]["mass"],
                y2=0,
                color="pink",
                alpha=alpha_regions,
            )

    #################
    # Add lines on the top to indicate which supernova type
    if create_supernova_lines:
        y_value_sn_mechanism_line = fiducial_max_mass_ppisn + 2.5

        ##
        # PPISN
        ppisn_area = mass_removal_fiducial_df[
            mass_removal_fiducial_df.pre_sn_co_core_mass >= first_ppisn_mass
        ][mass_removal_fiducial_df.stellar_type == 14]

        min_ppisn_zams = ppisn_area["pre_sn_co_core_mass"].to_numpy().min()
        max_ppisn_zams = ppisn_area["pre_sn_co_core_mass"].to_numpy().max()
        size_between_ppisn = max_ppisn_zams - min_ppisn_zams
        midpoint_ppisn = (max_ppisn_zams + min_ppisn_zams) / 2

        #
        fig, ax_co_core_to_remnant_mass_mass_removal = place_arrows(
            fig,
            ax_co_core_to_remnant_mass_mass_removal,
            midpoint_ppisn,
            y_value_sn_mechanism_line,
            size_between_ppisn,
            text="pulsational pair-instability + core-collapse",
        )

        #################
        # Plot the CC SN

        below_ppisn_area = mass_removal_fiducial_df[
            mass_removal_fiducial_df.pre_sn_co_core_mass < min_ppisn_zams
        ]

        min_below_ppisn_zams = below_ppisn_area["pre_sn_co_core_mass"].to_numpy().min()
        max_below_ppisn_zams = below_ppisn_area["pre_sn_co_core_mass"].to_numpy().max()

        size_between_below_ppisn = max_below_ppisn_zams - min_below_ppisn_zams
        midpoint_below_ppisn = (max_below_ppisn_zams + min_below_ppisn_zams) / 2

        #
        fig, ax_co_core_to_remnant_mass_mass_removal = place_arrows(
            fig,
            ax_co_core_to_remnant_mass_mass_removal,
            midpoint_below_ppisn,
            y_value_sn_mechanism_line,
            size_between_below_ppisn,
            text="core-collapse",
        )

    #################
    # Make up
    add_makeup_plot(
        ax_co_core_to_remnant_mass_mass_removal,
        min_zams_mass,
        max_zams_mass,
        fiducial_max_mass_ppisn,
        metallicity_value,
    )

    return fig, ax_co_core_to_remnant_mass_mass_removal
