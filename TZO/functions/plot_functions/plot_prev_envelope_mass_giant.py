"""
Function to plot the delay time given a dataframe
"""

import os

import matplotlib.pyplot as plt

import numpy as np
import pandas as pd

from david_phd_functions.plotting.utils import show_and_save_plot, add_plot_info
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()


def plot_prev_envelope_mass_giant(df, plot_settings):
    """
    Function to plot the delay time given a dataframe
    """

    ####
    # Delay time plot
    prev_envelope_mass_bins = np.arange(0, 60, 0.5)

    df["prev_envelope_mass_2"] = df["prev_mass_2"] - df["prev_core_mass_2"]

    ######
    # Plot
    fig = plt.figure(figsize=(20, 20))

    #
    gs = fig.add_gridspec(nrows=10, ncols=1)

    #
    ax = fig.add_subplot(gs[:, :])

    #
    ax.hist(
        df["prev_envelope_mass_2"],
        bins=prev_envelope_mass_bins,
        weights=df["probability"],
    )
    ax.set_yscale("log")

    #######################
    # Save and finish
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
