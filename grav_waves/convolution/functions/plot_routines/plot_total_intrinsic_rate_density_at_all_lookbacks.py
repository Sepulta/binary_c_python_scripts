"""
Plot routine for the total intrinsic rate density at all redshifts
"""

import copy
import pickle
import json
import h5py
import numpy as np
import pandas as pd

import astropy.units as u

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors

from david_phd_functions.grav_waves.dataframe_functions import add_chirpmass_column
from grav_waves.convolution.functions.convolution_functions import (
    generate_metallicity_sfr_array,
)
from grav_waves.convolution.functions.redshift_interpolation_functions import (
    test_redshift_to_lookback_time,
    test_lookback_time_to_redshift,
    create_interpolation_datasets,
)
from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    filter_dco_type,
)

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot

#
# matplotlib.use('Agg')

#
number_per_gigaparsec_cubed_per_year = 1 / u.Gpc**3 / u.yr
sfr_unit = u.Msun / u.Gpc**3 / u.yr


def get_data(hdf5_filename, dco_type, rate_key_in_df, rate_type_hdf5_key, query=None):
    """
    Function to get the data out of the hdf5 file and query if necessary
    """

    # Get combined df
    combined_df = pd.read_hdf(hdf5_filename, "/data/combined_dataframes")

    # Add columns
    combined_df = add_chirpmass_column(combined_df, m1_name="mass_1", m2_name="mass_2")
    combined_df["total_mass"] = combined_df["mass_1"] + combined_df["mass_2"]
    combined_df["primary_mass"] = combined_df[["mass_1", "mass_2"]].max(axis=1)
    combined_df["secondary_mass"] = combined_df[["mass_1", "mass_2"]].min(axis=1)

    #################
    # Loop over all datasets:
    res_dict = {}

    hdf5_file = h5py.File(hdf5_filename, "r")
    sorted_keys = sorted(
        hdf5_file["data"][rate_type_hdf5_key].keys(),
        key=lambda key: float(key),
        reverse=False,
    )
    hdf5_file.close()

    for redshift in sorted_keys:
        redshift_value = float(redshift)

        # Load df
        # TODO: multiprocess this
        index_df = pd.read_hdf(
            hdf5_filename, "/data/{}/{}".format(rate_type_hdf5_key, str(redshift_value))
        )
        joined_df = index_df.join(combined_df, on="local_index")

        # Filter the dco type:
        joined_df = filter_dco_type(joined_df, dco_type)

        if not query is None:
            joined_df = joined_df.query(query)

        res_dict[redshift_value] = joined_df[rate_key_in_df].sum()

    # Turn into lists/arrays
    total_redshift_list = []
    total_rate_list = []

    for redshift_value in res_dict:
        total_redshift_list.append(redshift_value)
        total_rate_list.append(res_dict[redshift_value])

    total_redshift_array = np.array(total_redshift_list)
    total_rate_array = np.array(total_rate_list)

    return {
        "total_redshift_array": total_redshift_array,
        "total_rate_array": total_rate_array,
    }


# Total intrinsic rate density
def plot_total_intrinsic_rate_density_at_all_lookbacks(
    convolution_dataset_hdf5_filename,
    dco_type,
    rate_type,
    querylist=None,
    divide_by_binsize=False,
    time_bin_key=None,
    add_cdf=False,
    add_ratio=False,
    plot_settings=None,
):
    """
    Function to plot the total intrinsic merger rate density at all the lookback values
    """

    if not plot_settings:
        plot_settings = {}

    # Select correct rate type:
    if rate_type == "merger_rate":
        rate_key_in_dict = "merger_rate_data"
        rate_key_in_df = "merger_rates"
        long_name_rate_type = "Merger rate"
        rate_type_hdf5_key = "merger_rate_data"
    elif rate_type == "formation_rate":
        rate_key_in_dict = "formation_rate_data"
        rate_key_in_df = "formation_rates"
        long_name_rate_type = "Formation rate"
        rate_type_hdf5_key = "formation_rate_data"

    #
    number_density_per_year = 1 / u.Gpc**3 / u.yr
    plotting_unit = number_density_per_year

    ####
    # Read out the data
    hdf5_file = h5py.File(convolution_dataset_hdf5_filename, "r")

    # Retrieve info
    settings = hdf5_file["settings"]
    convolution_settings = json.loads(settings["convolution_configuration"][()])

    hdf5_file.close()

    # get time info
    timebins = np.array(convolution_settings["time_bins"])
    time_values = (timebins[1:] + timebins[:-1]) / 2
    time_binsizes = np.diff(timebins)

    # Get
    results_dict = {}
    results_dict["all"] = get_data(
        convolution_dataset_hdf5_filename,
        dco_type=dco_type,
        rate_key_in_df=rate_key_in_df,
        rate_type_hdf5_key=rate_type_hdf5_key,
    )

    if not querylist is None:
        for query_dict in querylist:
            results_dict[query_dict["name"]] = get_data(
                convolution_dataset_hdf5_filename,
                dco_type=dco_type,
                rate_key_in_df=rate_key_in_df,
                rate_type_hdf5_key=rate_type_hdf5_key,
                query=query_dict["query"],
            )

    xlabel_text = "Redshift"

    # Set up axes
    fig = plt.figure(figsize=(40, 40))

    #
    gs = fig.add_gridspec(nrows=12, ncols=1)

    if add_ratio and add_cdf:
        ax = fig.add_subplot(gs[:5, :])
        ax_ratio = fig.add_subplot(gs[5:8, :], sharex=ax)
        ax_cdf = fig.add_subplot(gs[8:, :])
        ax_cdf.set_xlabel(xlabel_text)

    elif add_ratio:
        ax = fig.add_subplot(gs[:8, :])
        ax_ratio = fig.add_subplot(gs[8:, :], sharex=ax)
        ax_ratio.set_xlabel(xlabel_text)

    elif add_cdf:
        ax = fig.add_subplot(gs[:8, :])
        ax_cdf = fig.add_subplot(gs[8:, :])
        ax_cdf.set_xlabel(xlabel_text)
    else:
        ax = fig.add_subplot(gs[:, :])
        ax.set_xlabel(xlabel_text)

    # Plot the unqueried results
    ax.plot(
        results_dict["all"]["total_redshift_array"],
        results_dict["all"]["total_rate_array"],
        label="Total merger rate",
        alpha=0.5,
    )

    if add_cdf:
        ax_cdf.plot(
            results_dict["all"]["total_redshift_array"],
            np.cumsum(results_dict["all"]["total_rate_array"]),
        )

    if not querylist is None:
        if add_ratio:
            # First make a empty shape that will contain the stacked results
            total_ratio_hist = np.zeros(results_dict["all"]["total_rate_array"].shape)

        # Loop over the query results
        for query_dict in querylist:
            main_line = ax.plot(
                results_dict[query_dict["name"]]["total_redshift_array"],
                results_dict[query_dict["name"]]["total_rate_array"],
                label=query_dict["name"],
                alpha=0.5,
            )
            color_plot = main_line[0].get_color()

            # Add cdf
            if add_cdf:
                ax_cdf.plot(
                    results_dict[query_dict["name"]]["total_redshift_array"],
                    np.cumsum(results_dict[query_dict["name"]]["total_rate_array"]),
                    label=query_dict["name"],
                )

            # Add ratio
            if add_ratio:
                ratio_to_total = np.divide(
                    results_dict[query_dict["name"]]["total_rate_array"],
                    results_dict["all"]["total_rate_array"],
                    where=results_dict["all"]["total_rate_array"] != 0,
                    out=np.zeros_like(
                        results_dict[query_dict["name"]]["total_rate_array"]
                    ),
                )

                ax_ratio.bar(
                    results_dict[query_dict["name"]]["total_redshift_array"],
                    ratio_to_total,
                    width=time_binsizes,
                    bottom=total_ratio_hist,
                    color=color_plot,
                    label=query_dict["name"],
                )
                total_ratio_hist += ratio_to_total

    # Add legend
    ax.legend(frameon=True, framealpha=0.5)

    if add_ratio:
        ax_ratio.legend(frameon=True, framealpha=0.5)

    if add_cdf:
        ax_cdf.legend(frameon=True, framealpha=0.5)

    #
    ax.set_ylabel(
        "{} density [{}]".format(
            long_name_rate_type, number_density_per_year.unit.to_string("latex_inline")
        )
    )
    ax.set_xlabel(r"Redshift")

    #
    title_text = "Intrinsic {} density as a function of redshift".format(
        long_name_rate_type.lower()
    )
    ax.set_title(title_text, fontsize=20)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    # Global settings
    rebinned_results_filename = "/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/convolution/scripts/new_main/output/array_test/convolution/without_detection_probability/rebinned_convolution_results.h5"
    convolution_plot_dir = "output/"
    divide_by_binsize = False

    querylist_lieke = [
        {"name": "CE channel", "query": "(comenv_counter >= 1)"},
        {
            "name": "No CE channel",
            "query": "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter==0)",
        },
    ]

    #
    plot_total_intrinsic_rate_density_at_all_lookbacks(
        rebinned_results_filename,
        divide_by_binsize=False,
        rate_type="merger_rate",
        dco_type="bhbh",
        # mass_quantity='primary_mass',
        # mass_bins=np.arange(0, 60, 1),
        # add_ratio=True,
        # querylist=querylist_lieke,
        plot_settings={
            "show_plot": True,
            "output_name": "output/plot_total_intrinsic_rate_density_at_all_lookbacks.pdf",
            "simulation_name": "test",
            "antialiased": True,
            "rasterized": True,
            "runname": "plot_total_intrinsic_rate_density_at_all_lookbacks",
        },
    )
else:
    matplotlib.use("Agg")
