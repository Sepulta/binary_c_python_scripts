"""
Functions that handle the calculation of stuff we actually want to output
"""

import time
import copy
import warnings

import numpy as np
import astropy.units as u
import pandas as pd

from tables import NaturalNameWarning

from grav_waves.settings import AGE_UNIVERSE_IN_YEAR

warnings.filterwarnings("ignore", category=NaturalNameWarning)


def create_birthrate_array(
    metallicity_weighted_starformation_array,
    dataframe_dict,
    sorted_metallicity_keys_type,
    time_centers,
    convolution_configuration,
    verbose=0,
):
    """
    Function to calculate the birth rate array

    We take the total number per unit mass in each dataframe for systems that merge in t<t_hubble
    and multiply that that by the total metallicity weighted star formation rate of the current redshift and metallicity bin
    """

    # Copy shape of the SFR array
    birthrate_array = np.zeros(metallicity_weighted_starformation_array.shape)

    # Calculate total number_rate_densities
    total_number_per_solarmass_dict = {}
    total_number_per_solarmass_array = []

    for _, metallicity_key in enumerate(sorted_metallicity_keys_type):
        df = dataframe_dict[metallicity_key]

        # make sure we only include systems in hubble time
        df = df[df.merger_time_in_years < AGE_UNIVERSE_IN_YEAR]

        #
        total_number_per_solarmass_dict[metallicity_key] = df[
            "number_per_solar_mass"
        ].sum()
        total_number_per_solarmass_array.append(df["number_per_solar_mass"].sum())

    total_number_per_solarmass_array = np.array(total_number_per_solarmass_array) * (
        1 / u.Msun
    )

    # Multiply the starformation rate by the total_number_per_solarmass_array to get the birthrate
    birthrate_array = (
        metallicity_weighted_starformation_array.T * total_number_per_solarmass_array
    ).T

    return {
        "birthrate_data": birthrate_array,
        "time_centers": time_centers,
        "metallicity_centers": sorted_metallicity_keys_type,
    }
