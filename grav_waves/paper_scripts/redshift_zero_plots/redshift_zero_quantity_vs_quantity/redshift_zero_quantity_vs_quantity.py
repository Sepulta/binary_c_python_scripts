"""
Function to plot 2-d density of two given quantities, and display a contourline on that same plot for a given query, displaying the contours as fraction of that part

TODO: consider introducing an interpolator for the grid results
"""

import os
import h5py
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    quantity_name_dict,
    quantity_unit_dict,
    rate_type_name_dict,
    rate_density_units,
    create_centers_from_bins,
)
from grav_waves.paper_scripts.redshift_zero_plots.redshift_zero_quantity_vs_quantity.functions import (
    get_data,
)
from grav_waves.convolution.functions.convolution_general_functions import (
    create_bins_from_centers,
)

from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def plot_rate_density_quantity_at_redshift_zero_with_variations_and_ppisn_fraction(
    dataset_dict,
    x_quantity,
    x_quantity_bins,
    y_quantity,
    y_quantity_bins,
    redshift_value,
    rate_type="merger_rate",
    dco_type="bhbh",
    querydict=None,
    x_scale="linear",
    y_scale="linear",
    verbose=0,
    base_querydict=None,
    plot_settings={},
):
    """
    Function to plot quantity vs redshift
    """

    all_results_dict = {}

    # #
    ratio_contourlevels = [0.25, 0.5, 0.75]

    # Loop over the datasets
    for dataset in dataset_dict:
        results_dict = get_data(
            filename=dataset_dict[dataset]["filename"],
            rate_type=rate_type,
            dco_type=dco_type,
            x_quantity=x_quantity,
            x_quantity_bins=x_quantity_bins,
            y_quantity=y_quantity,
            y_quantity_bins=y_quantity_bins,
            redshift_value=redshift_value,
            general_query=base_querydict["query"]
            if base_querydict is not None
            else None,
            querydict=querydict,
        )

        #
        all_results_dict[dataset] = results_dict

    # Find maximum value
    max_val = np.array(
        [all_results_dict[result_dict]["all"] for result_dict in all_results_dict]
    ).max()
    custom_min_val = 10 ** (
        np.log10(max_val) - plot_settings.get("probability_floor", 4)
    )

    norm = colors.LogNorm(vmin=custom_min_val, vmax=max_val)

    # ####################
    # Plot data

    # Some preparation
    x_quantity_bincenters = create_centers_from_bins(x_quantity_bins)
    y_quantity_bincenters = create_centers_from_bins(y_quantity_bins)

    X, Y = np.meshgrid(x_quantity_bincenters, y_quantity_bincenters)

    #
    x_label = r"{} [{}]".format(
        quantity_name_dict[x_quantity],
        quantity_unit_dict[x_quantity].to_string("latex"),
    )
    y_label = r"{} [{}]".format(
        quantity_name_dict[y_quantity],
        quantity_unit_dict[y_quantity].to_string("latex"),
    )

    # get number of datasets
    num_datasets = len(list(dataset_dict.keys()))

    ####
    # create Figure and axes
    fig = plt.figure(figsize=(2 * (num_datasets * 4 + 2), 2 * 4))
    gs = fig.add_gridspec(nrows=1, ncols=(num_datasets * 4 + 2))

    axes_list = []
    for i, key in enumerate(dataset_dict.keys()):
        ax = fig.add_subplot(gs[:, (4 * i) : (4 * (i + 1))])
        axes_list.append(ax)

        ax.set_yscale(y_scale)
        ax.set_xscale(x_scale)

        if i > 0:
            ax.set_yticklabels([])

    # Create invisible axis
    ax_invisible = fig.add_subplot(gs[0, : 4 * num_datasets], frame_on=False)
    ax_invisible.set_xticks([])
    ax_invisible.set_yticks([])

    #
    ax_cb = fig.add_subplot(gs[:, -1:])

    ##########################
    # Actual plotting
    for dataset_i, dataset in enumerate(dataset_dict):
        #
        axes_list[dataset_i].set_title(dataset_dict[dataset]["name"])

        # Plot the results
        _ = axes_list[dataset_i].pcolormesh(
            X,
            Y,
            all_results_dict[dataset]["all"],
            norm=norm,
            shading="auto",
            antialiased=plot_settings.get("antialiased", True),
            rasterized=plot_settings.get("rasterized", True),
        )

        #############
        # Calculate the ratio
        all_results_dict[dataset]["ratio"] = np.divide(
            all_results_dict[dataset]["queried"],
            all_results_dict[dataset]["all"],
            out=all_results_dict[dataset]["queried"],
            where=all_results_dict[dataset]["all"] != 0,
        )  # only divide nonzeros else 1

        # Plot the contour
        cs = axes_list[dataset_i].contour(
            X,
            Y,
            all_results_dict[dataset]["ratio"],
            levels=ratio_contourlevels,
            cmap=plt.get_cmap("Reds"),
            # linewidths=1
        )
        axes_list[dataset_i].clabel(cs, cs.levels, inline=True, fontsize=10)

    ###
    # Make up
    ax_invisible.set_xlabel(x_label, labelpad=50)
    axes_list[0].set_ylabel(y_label)

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        norm=norm,
    )
    cbar.ax.set_ylabel(
        r"{} density [{}]".format(
            rate_type_name_dict[rate_type], rate_density_units.to_string("latex")
        )
    )

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #
    result_root = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results"

    ########
    #
    dataset_dict = {
        "fiducial": {
            "filename": os.path.join(
                result_root,
                "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/convolution_results/convolution_results.h5",
            ),
            "name": "Fiducial",
        },
        "Farmer19": {
            "filename": os.path.join(
                result_root,
                "HIGH_RES_SCHNEIDER_WIND_PPISN_FARMER19_FRYER_DELAYED/convolution_results/convolution_results.h5",
            ),
            "name": "Farmer+19",
        },
        "core_mass_shift": {
            "filename": os.path.join(
                result_root,
                "HIGH_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_-10_PPISN_core_mass_range_shift/convolution_results/convolution_results.h5",
            ),
            "name": "-10 Core mass shift",
        },
    }

    #########
    #
    target_dir = "/home/david/Dropbox/Academic/PHD/papers/paper_gw/paper_tex/figures/redshift_zero"
    target_dir = "plots"

    ######
    # Test version
    basename = "zams_mass.pdf"
    plot_rate_density_quantity_at_redshift_zero_with_variations_and_ppisn_fraction(
        dataset_dict=dataset_dict,
        redshift_value=0,
        x_quantity="primary_mass",
        x_quantity_bins=np.arange(0, 60, 2),
        y_quantity="zams_primary_mass",
        y_quantity_bins=10 ** np.linspace(1, np.log10(300), 50),
        x_scale="linear",
        y_scale="log",
        verbose=0,
        querydict={"query": "primary_undergone_ppisn == 1"},
        plot_settings={
            "show_plot": False,
            "probability_floor": 3,
            "output_name": os.path.join(target_dir, basename),
        },
    )

    quit()

    ######
    # Test version
    basename = "test.pdf"
    plot_rate_density_quantity_at_redshift_zero_with_variations_and_ppisn_fraction(
        dataset_dict=dataset_dict,
        redshift_value=0,
        x_quantity="primary_mass",
        x_quantity_bins=np.arange(0, 60, 2),
        y_quantity="secondary_mass",
        y_quantity_bins=np.arange(0, 60, 2),
        x_scale="linear",
        y_scale="linear",
        verbose=0,
        querydict={"query": "primary_undergone_ppisn == 1"},
        plot_settings={
            "show_plot": False,
            "probability_floor": 3,
            "output_name": os.path.join(target_dir, basename),
        },
    )

    ######
    # Test version
    basename = "primary_mass_vs_mass_ratio.pdf"
    plot_rate_density_quantity_at_redshift_zero_with_variations_and_ppisn_fraction(
        dataset_dict=dataset_dict,
        redshift_value=0,
        x_quantity="primary_mass",
        x_quantity_bins=np.arange(0, 60, 2),
        y_quantity="mass_ratio",
        y_quantity_bins=np.linspace(0, 1.0, 50),
        x_scale="linear",
        y_scale="linear",
        verbose=0,
        querydict={"query": "primary_undergone_ppisn == 1"},
        plot_settings={
            "show_plot": False,
            "probability_floor": 3,
            "output_name": os.path.join(target_dir, basename),
        },
    )

    ######
    # Test version
    basename = "primary_mass_vs_birth_redshift.pdf"
    plot_rate_density_quantity_at_redshift_zero_with_variations_and_ppisn_fraction(
        dataset_dict=dataset_dict,
        redshift_value=0,
        x_quantity="primary_mass",
        x_quantity_bins=np.arange(0, 60, 2),
        y_quantity="birth_redshift",
        y_quantity_bins=10.0 ** np.linspace(-3.0, 1.0, 50),
        x_scale="linear",
        y_scale="log",
        verbose=0,
        querydict={"query": "primary_undergone_ppisn == 1"},
        plot_settings={
            "show_plot": False,
            "probability_floor": 3,
            "output_name": os.path.join(target_dir, basename),
        },
    )

    ######
    # Birth metallicity vs primary mass

    ################
    # Read out info
    hdf5file = h5py.File(dataset_dict["fiducial"]["filename"])
    metallicity_bin_centers = np.array(hdf5file["sfr_data/metallicity_bin_centers"][()])
    log10metallicity_values = np.array(sorted(np.log10(metallicity_bin_centers)))
    log10metallicity_bins = create_bins_from_centers(log10metallicity_values)
    metallicity_bins = 10**log10metallicity_bins
    metallicity_centers = (metallicity_bins[1:] + metallicity_bins[:-1]) / 2
    hdf5file.close()

    basename = "primary_mass_vs_birth_metallicity.pdf"
    plot_rate_density_quantity_at_redshift_zero_with_variations_and_ppisn_fraction(
        dataset_dict=dataset_dict,
        redshift_value=0,
        x_quantity="primary_mass",
        x_quantity_bins=np.arange(0, 60, 2),
        y_quantity="metallicity",
        y_quantity_bins=metallicity_bins,
        x_scale="linear",
        y_scale="log",
        verbose=0,
        querydict={"query": "primary_undergone_ppisn == 1"},
        plot_settings={
            "show_plot": False,
            "probability_floor": 3,
            "output_name": os.path.join(target_dir, basename),
        },
    )
