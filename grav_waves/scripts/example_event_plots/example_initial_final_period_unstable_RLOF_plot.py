"""
Plot routine to plot the pre and post CE period
"""

import numpy as np
import pandas as pd
from grav_waves.functions.events import merge_with_event_dataframe, print_rows_and_cols


import matplotlib.pyplot as plt
from matplotlib import colors
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()

import matplotlib
from plot_utils import get_vmin


def example_initial_final_period_unstable_RLOF_plot(
    combined_dataframes, rate_array, rlof_event_dataframe
):
    """
    Example plot to show the initial vs final period for systems that undergo CE, survive and merge at z=0
    """

    merged_rlof_df = merge_with_event_dataframe(
        main_dataframe=combined_dataframes, event_dataframe=rlof_event_dataframe
    )
    print_rows_and_cols(merged_rlof_df, "merged_df")

    # Set rate array
    merged_rlof_df["rates"] = rate_array[merged_rlof_df["initial_indices"].to_numpy()]
    stable_rlofs = merged_rlof_df.query("initial_stability == 0")
    unstable_rlofs = merged_rlof_df.query("initial_stability > 0")

    # Set bins
    numbins = 50
    x_bins = 10 ** np.linspace(-4, 2, numbins)
    y_bins = 10 ** np.linspace(-4, 2, numbins)

    #
    hist, _, _ = np.histogram2d(
        unstable_rlofs["initial_orbital_period"],
        unstable_rlofs["final_orbital_period"],
        bins=[x_bins, y_bins],
        weights=unstable_rlofs["rates"],
    )

    # print(unstable_rlofs[['initial_orbital_period', 'final_orbital_period']])
    # quit()
    norm = colors.LogNorm(
        vmin=get_vmin(hist, 1e-6),
        vmax=np.max(hist),
    )

    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=5)
    rates_axis = fig.add_subplot(gs[:, :-1])
    ax_cb = fig.add_subplot(gs[:, -1:])

    plot_settings = {}
    # make bincenters
    x_bincenters = (x_bins[1:] + x_bins[:-1]) / 2
    y_bincenters = (y_bins[1:] + y_bins[:-1]) / 2

    X, Y = np.meshgrid(x_bincenters, y_bincenters)
    _ = rates_axis.pcolormesh(
        X,
        Y,
        hist.T,
        norm=norm,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    #
    rates_axis.set_yscale("log")
    rates_axis.set_xscale("log")
    rates_axis.plot([10**-4, 10**2], [10**-4, 10**2], "--", alpha=0.5)

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_cb,
        norm=norm,
    )
    cbar.ax.set_ylabel(r"{} density [{}]".format(1, 2))

    plt.show()
