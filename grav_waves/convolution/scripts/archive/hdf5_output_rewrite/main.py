"""
Script to test the new dataframe method
"""

import os
import numpy as np

import astropy.units as u


from grav_waves.settings import convolution_settings, config_dict_cosmology
from grav_waves.gw_analysis.functions.functions import make_dataset_dict

from grav_waves.convolution.functions.general_plot_function import (
    general_plot_routine as general_sfr_convolution_plot_function,
)
from grav_waves.convolution.functions.convolution_functions import (
    new_convolution_main as convolution_main,
)

#######################
# Do simulation

# Main simulation dir
main_dir = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/MID_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED"

#
dataset_dict_populations = make_dataset_dict(main_dir)

convolution_output_dir = os.path.join(
    "output/", "convolution_without_detector_probability_results"
)
convolution_plot_dir = os.path.join(
    "output/", "convolution_without_detector_probability_plots"
)

os.makedirs(convolution_output_dir, exist_ok=True)
os.makedirs(convolution_plot_dir, exist_ok=True)

# Set stepping
stepsize = 0.005

convolution_settings["stepsize"] = stepsize
convolution_settings["time_bins"] = np.arange(
    0.0001 - 0.5 * stepsize,
    (convolution_settings["max_interpolation_redshift"] - 1) + 0.5 * stepsize,
    stepsize,
)
convolution_settings["time_centers"] = (
    convolution_settings["time_bins"][1:] + convolution_settings["time_bins"][:-1]
) / 2
convolution_settings["use_new_dataframe_method"] = True
convolution_settings["add_detection_probability"] = False
convolution_settings["include_formation_rates"] = False
convolution_settings[
    "global_query"
] = "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0)"
# convolution_settings['global_query'] = '(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter >= 1)'
# convolution_settings['global_query'] = '(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter==0)'

#
redshift_convolution_without_detector_probability_filename = os.path.join(
    convolution_output_dir, "convolution_without_detection_probability.h5"
)

# # Run convolution
# convolution_main(
#     dataset_dict=dataset_dict_populations,
#     convolution_configuration=convolution_settings,
#     cosmology_configuration=config_dict_cosmology,
#     output_filename=redshift_convolution_without_detector_probability_filename,
#     dco_type='bhbh',
#     verbosity=1
# )
# quit()


#######################
# Plot output
rate_type = "merger_rate"
mass_type = "chirpmass"
divide_by_binsize = False
querylist = [
    {"name": "channel_1", "query": "comenv_counter >= 1"},
    {"name": "channel_2", "query": "comenv_counter==0"},
]

#####
# total merger rate plot
from grav_waves.convolution.functions.plot_routines.plot_total_intrinsic_rate_density_at_all_lookbacks import (
    plot_total_intrinsic_rate_density_at_all_lookbacks,
)

print("running plot_total_intrinsic_rate_density_at_all_lookbacks")
plot_total_intrinsic_rate_density_at_all_lookbacks(
    redshift_convolution_without_detector_probability_filename,
    rate_type=rate_type,
    querylist=[
        {"name": "channel_1", "query": "comenv_counter >= 1"},
        {"name": "channel_2", "query": "comenv_counter==0"},
    ],
    add_cdf=True,
    add_ratio=True,
    plot_settings={
        "output_name": "plot_total_intrinsic_rate_density_at_all_lookbacks_test.pdf",
        "show_plot": True,
        "antialiased": True,
        "rasterized": True,
    },
)

# #####
# # plot_rate_density_at_zero_redshift
# from grav_waves.convolution.functions.plot_routines.plot_rate_density_at_zero_redshift import (
#     plot_rate_density_at_zero_redshift,
# )
# plot_rate_density_at_zero_redshift(
#     redshift_convolution_without_detector_probability_filename,
#     mass_quantity=mass_type,
#     divide_by_binsize=divide_by_binsize,
#     add_cdf=True,
#     add_ratio=True,
#     querylist=[{'name': 'channel_1', 'query': 'comenv_counter >= 1'}, {'name': 'channel_2', 'query': 'comenv_counter==0'}],
#     plot_settings={
#         'show_plot': True,
#         'output_name': 'plot_rate_density_at_zero_redshift.pdf',
#         'antialiased': True,
#         'rasterized': True,
#     }
# )

# #####
# # plot_convolved_rate_density_per_mass_quantity
# from grav_waves.convolution.functions.plot_routines.plot_convolved_rate_density_per_mass_quantity import (
#     plot_convolved_rate_density_per_mass_quantity,
# )
# plot_convolved_rate_density_per_mass_quantity(
#     redshift_convolution_without_detector_probability_filename,
#     rate_type=rate_type,
#     mass_type=mass_type,
#     divide_by_binsize=divide_by_binsize,
#     querylist=[{'name': 'channel_1', 'query': 'comenv_counter >= 1'}, {'name': 'channel_2', 'query': 'comenv_counter==0'}],
#     contourlevels=[1e-1, 1e0, 1e1, 1e2],
#     linestyles_contourlevels=['solid', 'dashed', '-.', ':'],
#     plot_settings={
#         'show_plot': True,
#         'output_name': "plot_convolved_rate_density_per_mass_quantity.pdf",
#         'antialiased': True,
#         'rasterized': True,
#     }
# )

# #####
# # 2-d plot for certain mass quantity
# plot_convolved_rate_density_per_mass_quantity(
#     redshift_convolution_without_detector_probability_filename,
#     rate_type='merger_rate',
#     mass_type='chirpmass',
#     divide_by_binsize=True,
#     plot_settings={
#         'show_plot': True,
#         'output_name': 'plot_convolved_rate_density_per_mass_quantity_test.pdf',
#         'antialiased': True,
#         'rasterized': True,
#     }
# )

# convolution_settings['global_query'] = '(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter >= 1)'
# convolution_settings['global_query'] = '(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter==0)'

# from grav_waves.gw_analysis.functions.general_plot_function import general_plot_function as general_individual_datasets_plot_function

# individual_datasets_plot_output_pdf = 'individual_datasets_plots.pdf'
# general_individual_datasets_plot_function(
#     dataset_dict_populations,

#     result_dir=dataset_dict_populations['population_result_dir'],
#     plot_dir=dataset_dict_populations['population_plot_dir'],

#     type_key='bhbh',
#     combined_pdf_output_name=individual_datasets_plot_output_pdf,
#     convolution_settings=convolution_settings
# )
# quit()
# convolution_settings['global_query'] = '(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter >= 1)'
# individual_datasets_plot_output_pdf = 'individual_datasets_plots_channel_1.pdf'
# general_individual_datasets_plot_function(
#     dataset_dict_populations,

#     result_dir=dataset_dict_populations['population_result_dir'],
#     plot_dir=dataset_dict_populations['population_plot_dir'],

#     type_key='bhbh',
#     combined_pdf_output_name=individual_datasets_plot_output_pdf,
#     convolution_settings=convolution_settings
# )

# convolution_settings['global_query'] = '(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter==0)'
# individual_datasets_plot_output_pdf = 'individual_datasets_plots_channel_2.pdf'
# general_individual_datasets_plot_function(
#     dataset_dict_populations,

#     result_dir=dataset_dict_populations['population_result_dir'],
#     plot_dir=dataset_dict_populations['population_plot_dir'],

#     type_key='bhbh',
#     combined_pdf_output_name=individual_datasets_plot_output_pdf,
#     convolution_settings=convolution_settings
# )
# quit()
# Plot the combined pdf
convolution_without_detector_plot_output_pdf = "dataset_plots.pdf"
general_sfr_convolution_plot_function(
    redshift_convolution_without_detector_probability_filename,
    divide_by_binsize=False,
    output_pdf_filename=convolution_without_detector_plot_output_pdf,
    main_plot_dir=os.path.join(convolution_plot_dir),
    querylist=querylist,
    include_plot_all_rates_and_metallicity_distribution=True,
    include_plot_all_rates_and_metallicity_distribution_detailed=True,
    include_plot_birthrate=True,
    include_merger_and_formation_rate_densities_over_redshift_plots=True,
    include_merger_and_formation_rate_densities_at_redshift_zero_plots=True,
    include_total_intrinsic_merger_rates_over_redshift_plots=True,
    include_interpolation_error_plot=False,
)
