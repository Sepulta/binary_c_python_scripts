"""
Example for the triangle plot routine
"""

import pandas as pd

from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict

from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

from grav_waves.settings import convolution_settings

from triangle_plot.plot_triangle_plot import generate_triangle_plot


load_mpl_rc()


# Read out the data
simulation_dir = "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_RLOF_SANA_2021_ENSEMBLE_fractions"
# simulation_dir = '/home/david/projects/binary_c_root/results/RLOF/LOW_RES_RLOF_SANA_2021_ENSEMBLE_fractions'
rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(simulation_dir)
dataset_dict = rlof_episode_dataset_dict["datasets"]

# Read out data and create dataframe
df = pd.read_csv(dataset_dict["0.002"]["filename"], sep="\s+", header=0)

# Add columns
df["time_spent_mt"] = df["final_time"] - df["initial_time"]
df["ratio_angular_momentum_stream_excess_returned_to_orbit"] = df[
    "total_angular_momentum_stream_excess_returned_to_orbit"
] / (df["initial_orbital_angular_momentum"])
df["ratio_angular_momentum_stellar_overspin_returned_to_orbit"] = df[
    "total_angular_momentum_stellar_overspin_returned_to_orbit"
] / (df["initial_orbital_angular_momentum"])
df["ratio_time_spent_disk_masstransfer"] = (
    df["total_time_spent_disk_masstransfer"] / df["time_spent_mt"]
)
df["ratio_mass_transferred_through_disk"] = df[
    "total_mass_transferred_through_disk"
] / (df["total_mass_lost"] + df["total_mass_accreted"])

## convert to yield per solarmass
# Multiply the probability by a binary fraction
df["probability"] *= convolution_settings["binary_fraction"]

# Multiply the probability by a conversion factor to get the number per solar mass
df["number_per_solar_mass"] = df["probability"] * convolution_settings["mass_in_stars"]


plot_columns = [
    "initial_separation",
    "total_mass_lost",
    "total_mass_transferred_through_disk",
    "zams_mass_1",
]
scales = ["linear", "linear", "linear", "log"]

plot_columns = [
    "initial_separation",
    "zams_mass_1",
    "total_mass_lost",
    "total_mass_transferred_through_disk",
]
scales = ["log", "log", "linear", "linear"]

#
generate_triangle_plot(
    df,
    scales=scales,
    plot_columns=plot_columns,
    plot_settings={"show_plot": True, "max_diff_threshold": 4},
)
