import os
import json
import time
import pickle
import sys

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import (
    get_help_all,
    get_help,
    create_hdf5,
    output_lines,
)

# Load personal defaults:
from david_phd_functions.binarcy.personal_defaults import personal_defaults

# Import the custom logging string:
import supernova_script_custom_logging_string

## Quick script to get some output about which stars go supernova when.


def parse_function(self, output):
    # extract info from the population instance
    # TODO: think about whether this is smart. Passing around this object might be an overkill

    # Get some information from the
    data_dir = self.custom_options["data_dir"]
    base_filename = self.custom_options["base_filename"]

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # Create filename
    outfilename = os.path.join(data_dir, base_filename)

    # Go over the output.
    for el in output_lines(output):
        headerline = el.split()[0]

        # CHeck the header and act accordingly
        if headerline == "DAVID_SN":
            parameters = [
                "time",
                "mass_1",
                "prev_mass_1",
                "zams_mass_1",
                "SN_type",
                "probability",
            ]
            values = el.split()[1:]
            seperator = "\t"

            if not os.path.exists(outfilename):
                with open(outfilename, "w") as f:
                    f.write(seperator.join(parameters) + "\n")

            with open(outfilename, "a") as f:
                f.write(seperator.join(values) + "\n")


## Set values
test_pop = Population()
test_pop.set(
    verbose=1,
    C_logging_code=supernova_script_custom_logging_string.supernova_custom_logging_string,
)

# Put personal_defaults in there
test_pop.set(**personal_defaults)


# Make metallicity loop.
metallicity_values = [0.02, 0.01, 0.002, 0.001, 0.0002]
for metallicity in metallicity_values:
    test_pop.set(
        metallicity=metallicity,
        # data_dir=os.path.join(os.environ['BINARYC_DATA_ROOT'], 'testing_python', 'compact_objects_single', 'default_settings', 'CO_single_{}'.format(metallicity)),
        # base_filename="CO_single_{}.dat".format(metallicity),
        data_dir=os.path.join(
            os.environ["BINARYC_DATA_ROOT"],
            "supernova_types",
            "SN_types_z{}".format(metallicity),
        ),
        base_filename="SN_types_z{}.dat".format(metallicity),
    )

    # Export settings:
    test_pop.export_all_info(use_datadir=True)

    # Evolve grid
    test_pop.evolve_population()

    # Put data (and settings) in hdf5
    create_hdf5(
        test_pop.custom_options["data_dir"], "CO_single_{}.hdf5".format(metallicity)
    )

metallicity = 0.002
test_pop.set(
    separation=1000000000,
    orbital_period=400000000,
    metallicity=metallicity,
    parse_function=parse_function,
)

# Export settings:
test_pop.export_all_info(use_datadir=True)

masses = range(1, 101)
for mass in masses:
    test_pop.set(M_1=mass)
    test_pop.evolve_single(parse_function)

# hdf5
create_hdf5(test_pop.custom_options["data_dir"])
