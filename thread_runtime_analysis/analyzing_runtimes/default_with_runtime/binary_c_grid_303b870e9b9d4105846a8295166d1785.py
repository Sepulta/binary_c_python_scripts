import math
import numpy as np
from binarycpython.utils.distribution_functions import *
from binarycpython.utils.spacing_functions import *
from binarycpython.utils.useful_funcs import *


def grid_code(self, print_results=True):
    # Grid code generated on 2021-01-15T23:46:21.206155
    # This function generates the systems that will be evolved with binary_c

    # Setting initial values
    _total_starcount = 0
    starcounts = [0 for i in range(3)]
    probabilities = {}
    probabilities_list = [0 for i in range(3)]
    probabilities_sum = [0 for i in range(3)]
    parameter_dict = {}
    phasevol = 1

    # setting probability lists
    probabilities["M_1"] = 0
    probabilities["M_2"] = 0
    probabilities["orbital_period"] = 0

    # for loop for M_1
    sampled_values_lnm1 = const(math.log(1), math.log(100), 40)
    phasevol_lnm1 = sampled_values_lnm1[1] - sampled_values_lnm1[0]
    for lnm1 in sampled_values_lnm1:
        M_1 = math.exp(lnm1)
        phasevol *= phasevol_lnm1

        # Setting probabilities
        dlnm1 = (
            phasevol_lnm1
            * three_part_powerlaw(M_1, 0.1, 0.5, 1.0, 100, -1.3, -2.3, -2.3)
            * M_1
        )
        probabilities_sum[0] += dlnm1
        probabilities_list[0] = dlnm1

        # Increment starcount for M_1
        starcounts[0] += 1
        parameter_dict["M_1"] = M_1

        # for loop for M_2
        sampled_values_q = const(0.1 / M_1, 1, 40)
        phasevol_q = sampled_values_q[1] - sampled_values_q[0]
        for q in sampled_values_q:
            M_2 = q * M_1
            phasevol *= phasevol_q

            # Setting probabilities
            dq = phasevol_q * flatsections(
                q, [{"min": 0.1 / M_1, "max": 1.0, "height": 1}]
            )
            probabilities_sum[1] += dq
            probabilities_list[1] = probabilities_list[0] * dq

            # Increment starcount for M_2
            starcounts[1] += 1
            parameter_dict["M_2"] = M_2

            # for loop for orbital_period
            sampled_values_log10per = const(0.15, 5.5, 40)
            phasevol_log10per = sampled_values_log10per[1] - sampled_values_log10per[0]
            for log10per in sampled_values_log10per:
                orbital_period = 10**log10per
                sep = calc_sep_from_period(M_1, M_2, orbital_period)
                sep_min = calc_sep_from_period(M_1, M_2, 10**0.15)
                sep_max = calc_sep_from_period(M_1, M_2, 10**5.5)
                phasevol *= phasevol_log10per

                # Setting probabilities
                dlog10per = phasevol_log10per * sana12(
                    M_1,
                    M_2,
                    sep,
                    orbital_period,
                    sep_min,
                    sep_max,
                    math.log10(10**0.15),
                    math.log10(10**5.5),
                    -0.55,
                )
                probabilities_sum[2] += dlog10per
                probabilities_list[2] = probabilities_list[1] * dlog10per

                # Increment starcount for orbital_period
                starcounts[2] += 1
                parameter_dict["orbital_period"] = orbital_period

                ########################################
                # Code below will get evaluated for every generated system
                probability = self.grid_options["weight"] * probabilities_list[2]
                probability = probability / self.grid_options["repeat"]
                for _ in range(self.grid_options["repeat"]):
                    _total_starcount += 1
                    parameter_dict["probability"] = probability
                    parameter_dict["phasevol"] = phasevol
                    self._increment_probtot(probability)
                    yield (parameter_dict)
                ########################################

                ########################################
                # Code below is for finalising the handling of this iteration of the parameter
                phasevol /= phasevol_log10per

            ########################################
            # Code below is for finalising the handling of this iteration of the parameter
            phasevol /= phasevol_q

        ########################################
        # Code below is for finalising the handling of this iteration of the parameter
        phasevol /= phasevol_lnm1

    ########################################
    if print_results:
        print("Grid has handled {} stars".format(_total_starcount))
        print("with a total probability of {}".format(self.grid_options["_probtot"]))
