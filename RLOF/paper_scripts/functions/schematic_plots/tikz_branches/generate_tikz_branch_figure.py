"""
Function to generate the branch figure
"""

from RLOF.scripts.plot_episode_output.functions import get_rlof_episode_dataset_dict

from RLOF.paper_scripts.functions.schematic_plots.tikz_branches.functions import (
    generate_branch_data_dict,
    calculate_branching_ratios,
)


def get_branching_data_and_generate_tikz_figure(dataset_dict):
    """
    Function to calculate all the branching data and generate the tikz figure
    """

    # Read out the data and the probabilities of the subsets
    probabilities_dict = {}
    for metallicity in dataset_dict:
        result = generate_branch_data_dict(dataset_dict[metallicity]["filename"])
        probabilities_dict[metallicity] = result

    # Calculate the branching ratios
    branching_ratios_dict = calculate_branching_ratios(probabilities_dict)

    # Use the brancing ratios in the template


if __name__ == "__main__":
    ########
    # Set up data
    simulation_dir = (
        "/home/david/projects/binary_c_root/results/RLOF/server_results/MID_RES_SANA"
    )
    rlof_episode_dataset_dict = get_rlof_episode_dataset_dict(simulation_dir)
    dataset_dict = rlof_episode_dataset_dict["datasets"]

    get_branching_data_and_generate_tikz_figure(dataset_dict)
