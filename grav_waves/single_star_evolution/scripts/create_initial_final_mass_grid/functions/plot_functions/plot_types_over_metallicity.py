"""
Script to generate plot showing SN types over metallicity
TODO: add the option to write the exact SN types in the colorbar labels
TODO: add the option to fix
"""

import os
import json
import numpy as np
import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot

from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.utility_functions import (
    fill_missing_values,
    SN_type_dict,
)

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def plot_sn_types_over_metallicities(datafile, plot_settings=None):
    """
    Function to generate the mass mapping plot
    """

    if not plot_settings:
        plot_settings = {}

    # Open the data as a pandas dataframe
    df = pd.read_csv(datafile, header=0, sep="\s+")
    df["log10metallicity"] = np.log10(df["metallicity"])
    df = df.sort_values(by=["zams_mass", "log10metallicity"])
    other_columns = [
        col for col in df.columns if not col in ["zams_mass", "metallicity"]
    ]

    # Filter out effective_metallicity == 0:
    df = df[df.effective_metallicity > 0]

    # Fill missing values
    df = fill_missing_values(df, other_columns)

    #
    sn_type_df = df[["zams_mass", "log10metallicity", "sn_type"]]

    unique_zams_mass_values = df["zams_mass"].sort_values().unique()
    unique_metallicity_values = df["metallicity"].sort_values().unique()
    unique_log10unique_metallicity_values = np.log10(unique_metallicity_values)
    unique_sn_types = sn_type_df["sn_type"].unique()
    unique_sn_types = sorted(unique_sn_types[~np.isnan(unique_sn_types)])

    unique_sn_types_names_dict = {el: SN_type_dict[el] for el in unique_sn_types}

    #
    sn_types = (
        df["sn_type"]
        .to_numpy()
        .reshape(
            len(unique_zams_mass_values), len(unique_log10unique_metallicity_values)
        )
    )

    cmap = mpl.cm.get_cmap("magma", int(max(unique_sn_types) - min(unique_sn_types)))
    norm = mpl.colors.Normalize(
        vmin=min(unique_sn_types), vmax=max(unique_sn_types) + 1
    )

    ##########
    #
    fig = plt.figure(figsize=(16, 16))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=1, ncols=10)

    #
    ax_types = fig.add_subplot(gs[:, :8])
    ax_types_cb = fig.add_subplot(gs[:, 9])

    # Set up the colormesh plot
    _ = ax_types.pcolormesh(
        unique_log10unique_metallicity_values,
        unique_zams_mass_values,
        sn_types,
        norm=norm,
        shading="auto",
        antialiased=True,
        rasterized=True,
        cmap=cmap,
    )

    # make colorbar
    cbar = mpl.colorbar.ColorbarBase(ax_types_cb, norm=norm, cmap=cmap)
    cbar.ax.set_ylabel(r"Remnant mass [M$_{\odot}$]")

    #
    ax_types.set_yscale(plot_settings["mass_scale"])

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #
    plot_sn_types_over_metallicities(
        "../../output/output_vanilla/data/output.dat", plot_settings={"show_plot": True}
    )
