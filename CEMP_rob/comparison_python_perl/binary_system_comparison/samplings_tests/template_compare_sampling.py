"""
Script to compare the sampling files
"""

import os
import pandas as pd
import numpy as np

result_dir = "template_results"

# Set filenames
python_sampling_file = os.path.join(result_dir, "python", "results", "sampling.txt")
perl_sampling_file = os.path.join(result_dir, "perl", "results", "sampling.txt")

# Make
python_primary_masses = []
python_secondary_masses = []
python_orbital_periods = []
python_probabilities = []

perl_primary_masses = []
perl_secondary_masses = []
perl_orbital_periods = []
perl_probabilities = []

# Load files
with open(python_sampling_file, "r") as pythonfile:
    pythonfile.readline()

    for line in pythonfile:
        split_line = line.strip().split()

        python_primary_masses.append(float(split_line[0]))
        python_secondary_masses.append(float(split_line[1]))
        python_orbital_periods.append(float(split_line[2]))
        python_probabilities.append(float(split_line[3]))

with open(perl_sampling_file, "r") as perlfile:
    perlfile.readline()

    for line in perlfile:
        split_line = line.strip().split()

        perl_primary_masses.append(float(split_line[0]))
        perl_secondary_masses.append(float(split_line[1]))
        perl_orbital_periods.append(float(split_line[2]))
        perl_probabilities.append(float(split_line[3]))

#
assert len(python_primary_masses) == len(perl_primary_masses)
assert len(python_secondary_masses) == len(perl_secondary_masses)
assert len(python_orbital_periods) == len(perl_orbital_periods)


# Create dataframe
df = pd.DataFrame(
    np.array(
        [
            python_primary_masses,
            python_secondary_masses,
            python_orbital_periods,
            python_probabilities,
            perl_primary_masses,
            perl_secondary_masses,
            perl_orbital_periods,
            perl_probabilities,
        ]
    ).T,
    columns=[
        "python_primary_masses",
        "python_secondary_masses",
        "python_orbital_periods",
        "python_probabilities",
        "perl_primary_masses",
        "perl_secondary_masses",
        "perl_orbital_periods",
        "perl_probabilities",
    ],
)
df["primary_mass_differences"] = df["python_primary_masses"] - df["perl_primary_masses"]
df["secondary_mass_differences"] = (
    df["python_secondary_masses"] - df["perl_secondary_masses"]
)
df["primary_orbital_periods"] = (
    df["python_orbital_periods"] - df["perl_orbital_periods"]
)
df["probability_differences"] = df["python_probabilities"] - df["perl_probabilities"]

print(df["primary_mass_differences"].unique())
print(df["secondary_mass_differences"].unique())
print(df["primary_orbital_periods"].unique())
print(df["probability_differences"].unique())

if not df["primary_mass_differences"].unique() == [0.0]:
    print("error. there are sampling differences in primary mass")

if not df["secondary_mass_differences"].unique() == [0.0]:
    print("error. there are sampling differences in secondary mass")

if not df["probability_differences"].unique() == [0.0]:
    print("error. there are sampling differences in orbital period")

if not df["probability_differences"].unique() == [0.0]:
    print("error. there are probability differences")
