"""
Extra functions to handle the GW data
"""

import os
import json
import glob
import copy
import datetime
import pandas as pd
import numpy as np


from david_phd_functions.grav_waves.dataframe_functions import add_chirpmass_column
from david_phd_functions.grav_waves.merger_rate_functions import (
    calculate_probabilty_per_solarmass,
)
from david_phd_functions.grav_waves.datafile_functions import (
    split_compact_objects_datafiles,
)

from grav_waves.settings import AGE_UNIVERSE_IN_YEAR


def make_df(
    datafile,
    metallicity="0.02",
    binary_fraction_factor=1,
    average_mass_system=1,
    **kwargs,
):
    """
    Function to calculate total merger probability for systems contained in a datafile.

    TODO: This function is getting a bit dangerous. Need to be able to
    """

    # Load into memory
    df = pd.read_table(datafile, sep="\s+")

    # Make sure the time columns are good.
    if (
        ("formation_time_in_years" in df.columns)
        and ("inspiral_time_in_years" in df.columns)
        and ("merger_time_in_years" in df.columns)
    ):
        df["formation_time_values_in_years"] = df["formation_time_in_years"]
        df["inspiral_time_values_in_years"] = df["inspiral_time_in_years"]
        df["merger_time_values_in_years"] = df["merger_time_in_years"]
    else:
        df["formation_time_values_in_years"] = df["time"] * 1e6
        if "merger_time" in set(df.columns):
            df["inspiral_time_values_in_years"] = df["merger_time"]
            df["merger_time_values_in_years"] = (
                df["formation_time_values_in_years"]
                + df["inspiral_time_values_in_years"]
            )
        else:
            df["merger_time_values_in_years"] = df["merger_time"]

    # Remove some columns
    if "time" in df.columns:
        df = df.drop(columns=["time"])
    if "merger_time" in df.columns:
        df = df.drop(columns=["merger_time"])

    # Limit output to merging only
    limit_merging_time = kwargs.get("limit_merging_time", True)
    filtered_df = df
    if limit_merging_time:
        filtered_df = df[df.merger_time_values_in_years < AGE_UNIVERSE_IN_YEAR]

    # Make sure the eccentricity s bounded (i.e. its still a binary system)
    if "eccentricity" in filtered_df.columns:
        filtered_df = filtered_df[filtered_df.eccentricity >= 0]
        filtered_df = filtered_df[filtered_df.eccentricity < 1]

    # Add some columns
    filtered_df = add_chirpmass_column(filtered_df, m1_name="mass_1", m2_name="mass_2")

    ####################
    # Convert the probability to a number of systems i formed per solar mass of SFR

    # Multiply the probability by a binary fraction
    filtered_df["probability"] *= binary_fraction_factor

    # Multiply the probability by a conversion factor to get the number per solar mass
    filtered_df["number_per_solar_mass"] = (
        filtered_df["probability"] / average_mass_system
    )

    return filtered_df


def calc_total_probability_merging_systems(datafile):
    """
    Function to calculate total merger probability for systems contained in a datafile.
    """

    # Create datafile and select merging
    merging_df = make_df(datafile)

    return calculate_probabilty_per_solarmass(
        merging_df, amt_stars=10000000, fbin=0.5, print_info=False
    )


def generate_info_dictionary(result_root_dir):
    """
    Function to generate the dictionary that contains information about datasets and the mergers

    Writes this file to the provided result_root_dir

    TODO: add option to re-split the files
    """

    datafile_dict_new = {}

    # Loop over the result dirs and generate the info dict
    for el in os.listdir(result_root_dir):
        if el.startswith("Z"):
            fullpath = os.path.join(os.path.abspath(result_root_dir), el)

            # Split dataset
            print("Splitting files for {}".format(fullpath))
            try:
                # if 'total_compact_objects.dat' in os.listdir(fullpath):
                split_compact_objects_datafiles(
                    os.path.join(fullpath, "total_compact_objects.dat")
                )
                # else:
                #     split_compact_objects_datafiles(os.path.join(fullpath, 'compact_objects.dat'))

            except ValueError:
                # TODO: this is not true
                print("Files in {} were already split".format(fullpath))

            metallicity = float(el[1:])
            contents_fullpath = os.listdir(fullpath)

            for file in contents_fullpath:
                if file in ["bhbh.dat", "nsns.dat", "bhns.dat", "combined_dco.dat"]:
                    CO_type = file.split(".")[0]
                    full_CO_filename = os.path.join(fullpath, file)

                    # Add information
                    # galactic_info = calculate_mergerrate_per_myr_and_rvol(merger_info['prob_per_solarmass'], "")

                    # Set up dict
                    datafile_dict_new["{}_{}".format(el, CO_type)] = {
                        "filename": full_CO_filename,
                        "type": CO_type,
                        "metallicity": metallicity,
                        # "merger_rate_MWEG_myr": galactic_info['merger_rate_MWEG_myr'],
                        # "rvol": galactic_info['rvol']
                    }

    # Write the info dict
    with open(
        os.path.join(os.path.abspath(result_root_dir), "info_dict.json"), "w"
    ) as f:
        f.write(json.dumps(datafile_dict_new, indent=4))


def load_info_dict(result_root_dir, rebuild=False, create_if_not_present=True):
    """
    Function that will load the info dict and handles some situations:

    - If it does not exists it will try to make it.
    - Will rebuild if so desired
    """

    # We can choose to rebuild the dictionary (and calculate all the necessary stuff around it)
    if rebuild:
        generate_info_dictionary(result_root_dir)

    info_dict_filename = os.path.join(
        os.path.abspath(result_root_dir), "info_dict.json"
    )

    # Load it if its there
    if not os.path.isfile(info_dict_filename):
        if create_if_not_present:
            generate_info_dictionary(result_root_dir)
        else:
            raise FileNotFoundError(
                "{} does not exist and you chose not to rebuild it".format(
                    info_dict_filename
                )
            )

    # load file and return it
    with open(info_dict_filename, "r") as f:
        info_dict = json.loads(f.read())

    return info_dict


def return_argline(parameter_dict):
    """
    Function to create the string for the arg line from a parameter dict
    """

    argline = "binary_c "

    for param_name in sorted(parameter_dict):
        argline += "{} {} ".format(param_name, parameter_dict[param_name])
    argline = argline.strip()
    return argline


def get_cmdline_arg_single_system(dataset, system):
    """
    Function that can generate the cmdline arg for a single system from a dataframe. using the dataset dict.
    """

    settings_files = [
        el
        for el in os.listdir(os.path.dirname(dataset["filename"]))
        if el.endswith("_settings.json")
    ]
    settings_files.sort(
        key=lambda date: datetime.datetime.strptime(
            "_".join(date.split("_")[1:-1]), "%Y%m%d_%H%M%S"
        ),
        reverse=True,
    )

    with open(
        os.path.join(os.path.dirname(dataset["filename"]), settings_files[0]), "r"
    ) as f:
        settings = json.loads(f.read())

    specific_system_settings = {
        "M_1": system["zams_mass_1"],
        "M_2": system["zams_mass_2"],
        "orbital_period": system["zams_period"],
        "eccentricity": system["zams_eccentricity"],
        "separation": system["zams_separation"],
    }
    specific_system_dict = copy.deepcopy(settings["population_settings"]["bse_options"])
    specific_system_dict.update(specific_system_settings)

    cmdline_string = return_argline(specific_system_dict)
    return cmdline_string, specific_system_dict


def combine_queries(query_1, query_2, operator="&"):
    """
    Function to combine queries with AND
    """

    if query_1 in ["", None]:
        return "({})".format(query_2)
    if query_2 in ["", None]:
        return "({})".format(query_1)

    return "({}) {} ({})".format(query_1, operator, query_2)


def calculate_numbers(df, convolution_settings):
    """
    TODO: write docstring
    """

    #
    probabilities = (
        df["probability"].to_numpy() * convolution_settings["binary_fraction"]
    )
    probabilities_to_per_unit_mass = (
        probabilities / convolution_settings["mass_in_stars"]
    )

    probability_sum = probabilities.sum()
    probabilities_to_per_unit_mass_sum = probabilities_to_per_unit_mass.sum()

    errors = np.sqrt(np.sum(probabilities_to_per_unit_mass**2))

    return [probability_sum, probabilities_to_per_unit_mass_sum, errors]


def make_dataset_dict(main_dir):
    """
    Function to create a dictionary for the datasets
    """

    dataset_dict = {}

    # Get simname
    simname = os.path.basename(main_dir)
    dataset_dict["sim_name"] = simname
    dataset_dict["population_result_dir"] = os.path.join(main_dir, "population_results")
    dataset_dict["population_plot_dir"] = os.path.join(main_dir, "population_plots")
    dataset_dict["main_dir"] = main_dir
    dataset_dict["add_ppisn_plots"] = False

    #
    dataset_dict["rebuild"] = False

    return dataset_dict


def clean_dco_type_files_and_info_dict(simulation_dir, dry_run=False):
    """
    Function to clean the DCO type files from a directory, recursively
    """

    # Find all the files
    other_files = sorted(glob.glob(simulation_dir + "/**/other.dat", recursive=True))
    nsns_files = sorted(glob.glob(simulation_dir + "/**/nsns.dat", recursive=True))
    bhns_files = sorted(glob.glob(simulation_dir + "/**/bhns.dat", recursive=True))
    bhbh_files = sorted(glob.glob(simulation_dir + "/**/bhbh.dat", recursive=True))
    combined_dco_files = sorted(
        glob.glob(simulation_dir + "/**/combined_dco.dat", recursive=True)
    )

    for fileset in [
        other_files,
        nsns_files,
        bhns_files,
        bhbh_files,
        combined_dco_files,
    ]:
        for filename in fileset:
            print("removing {}".format(filename))
            if not dry_run:
                os.remove(filename)

    # Find and delete info dicts:
    info_dict_files = sorted(
        glob.glob(simulation_dir + "/**/info_dict.json", recursive=True)
    )
    for filename in info_dict_files:
        print("removing {}".format(filename))
        if not dry_run:
            os.remove(filename)


# #
# clean_dco_type_files_and_info_dict('/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/population_results', dry_run=True)
