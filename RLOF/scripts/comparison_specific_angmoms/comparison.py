import matplotlib.pyplot as plt
import numpy as np
import astropy.units as u
import matplotlib.colors as colors

from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.masstransfer.functions import *

custom_mpl_settings.load_mpl_rc()

from functions import *

# Initial settings
Mdonor = 1
Maccretor = 1
a = 1
r_loss_frac_a = 0.5
r_loss = r_loss_frac_a * a
G = 1
factor_outer_edge_rl_accretor = 0.9

rloss_steps = 0.01
logq_steps = 0.01

# Set up arrays with radii and mass ratios
r_loss_array = np.arange(0.01, 1 + rloss_steps, rloss_steps)
q_array = 10 ** np.arange(-1, 1 + logq_steps, logq_steps)

# plot_disk_vs_circumbinary(r_loss_array, q_array, factor_outer_edge_rl_accretor, include_iso=True)

# plot_disk_vs_isotropic(r_loss_array, q_array, factor_outer_edge_rl_accretor, include_iso=False)
plot_disk_vs_isotropic(
    r_loss_array, q_array, factor_outer_edge_rl_accretor, include_iso=True
)

# plot_disk_vs_jeans(r_loss_array, q_array, factor_outer_edge_rl_accretor, include_iso=True)
