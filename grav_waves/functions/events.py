import h5py
import pandas as pd
import numpy as np
import numpy_indexed as npi
import time


def print_rows_and_cols(df, dfname):
    """
    Function to print the rows and columns of the dataframe
    """

    num_cols = len(df.columns)
    num_rows = len(df.index)
    unique_uuids = len(df["uuid"].unique())
    print(
        "Dataframe {} has {} columns, {} rows and {} unique uuids".format(
            dfname, num_cols, num_rows, unique_uuids
        )
    )


def merge_with_event_dataframe(main_dataframe, event_dataframe):
    """
    Function to merge an event dataframe to the main dataframe
    """

    # Add initial indices of main df to main_df
    main_dataframe["initial_indices"] = list(range(len(main_dataframe.index)))

    # Merge tables
    merged_dataframe = main_dataframe.merge(event_dataframe, how="inner", on="uuid")

    return merged_dataframe


def get_indices_of_merged_dataframe_uuids_in_main_dataframe(
    main_dataframe, merged_dataframe
):
    """
    Function to get the indices of the merged dataframe uuids in the main dataframe

    Currently uses the indices method from numpy_indexed (https://github.com/EelcoHoogendoorn/Numpy_arraysetops_EP).
    """

    # # Slower method probably
    # main_dataframe_uuids_list = list(main_dataframe['uuid'].to_numpy())
    # merged_dataframe_uuids = merged_dataframe['uuid'].to_numpy()
    # indices_of_merged_dataframe_uuids_in_main_dataframe = np.array(
    #     [main_dataframe_uuids_list.index(x) for x in merged_dataframe_uuids])

    #
    indices_of_merged_dataframe_uuids_in_main_dataframe = npi.indices(
        main_dataframe["uuid"].to_numpy(),
        merged_dataframe["uuid"].to_numpy(),
        missing=-1,
    )

    return indices_of_merged_dataframe_uuids_in_main_dataframe
