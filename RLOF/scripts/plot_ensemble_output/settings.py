"""
Settings for the plot ensemble scripts
"""

import numpy as np

rlof_a_stellar_type_range = [0.5, 1.5]
rlof_b_stellar_type_range = [1.5, 3.5]
rlof_c_stellar_type_range = [3.5, 15.5]

parameter_name_dict = {
    "stable": "Stability of accretion",
    "disk": "Disk accretion",
    "st_acc": "Stellar type accretor",
    "st_don": "Stellar type donor",
    "rlof_counter": "RLOF counter",
    "mt_cat": "Mass transfer rate category",
    "m_don_zams": "Log10(ZAMS mass donor)",
    "m_acc_zams": "Log10(ZAMS mass accretor)",
    "p_orb_zams": "Log10(ZAMS orbital period)",
    "m_don": "Log10(Mass donor)",
    "p_orb": "Log10(Orbital period)",
    "frac_omega_crit": "Fraction omega crit",
    "mt_rate": "Log10(Mass transfer rate)",
    "r_acc_sep": "Log10(radius accretor / separation)",
    "q_acc_don": "Log10(Mass accretor/Mass donor)",
}

bins_integer_parametertypes = {
    "stable": np.array([-0.5, 0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5]),
    "disk": np.array([-0.5, 0.5, 1.5]),
    "st_acc": np.array(
        [
            -0.5,
            0.5,
            1.5,
            2.5,
            3.5,
            4.5,
            5.5,
            6.5,
            7.5,
            8.5,
            9.5,
            10.5,
            11.5,
            12.5,
            13.5,
            14.5,
            15.5,
            16.5,
            17.5,
        ]
    ),
    "st_don": np.array(
        [
            -0.5,
            0.5,
            1.5,
            2.5,
            3.5,
            4.5,
            5.5,
            6.5,
            7.5,
            8.5,
            9.5,
            10.5,
            11.5,
            12.5,
            13.5,
            14.5,
            15.5,
            16.5,
            17.5,
        ]
    ),
    "rlof_counter": np.array(
        [
            -0.5,
            0.5,
            1.5,
            2.5,
            3.5,
            4.5,
            5.5,
            6.5,
            7.5,
            8.5,
            9.5,
            10.5,
            11.5,
            12.5,
            13.5,
            14.5,
            15.5,
            16.5,
            17.5,
        ]
    ),
    "mt_cat": np.array([-0.5, 0.5, 1.5, 2.5, 3.5, 4.5, 5.5]),
}
