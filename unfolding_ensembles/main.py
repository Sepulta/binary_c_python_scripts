"""
Script to test unfolding ensembles
"""

import json
from collections import OrderedDict

import numpy as np
import pandas as pd

from ensemble_functions.ensemble_functions import (
    inflate_ensemble,
    find_columnames_recursively,
)
from triangle_plot.plot_triangle_plot import generate_triangle_plot

# Get the filename
filename = "ensemble_output.json"

with open(filename, "r") as f:
    ensemble_data = json.loads(f.read())

#
mass_ensemble_data = ensemble_data["ensemble"]["RLOF"]["fraction_separation_massratio"][
    "mass"
]

# reduced_ensemble_data = mass_ensemble_data['stable']['0']['disk']['0']['st_acc']['1']['st_don']['1']['rlof_counter']['1']['mt_cat']['0']['m_don_zams']['0.85']['m_acc_zams']['0.75']['p_orb_zams']['0.25']['m_don']['0.65']
reduced_ensemble_data = mass_ensemble_data

# Get column names
columnnames = find_columnames_recursively(reduced_ensemble_data)

# Get the inflated results
inflated_ensemble = inflate_ensemble(reduced_ensemble_data)

# Put into dataframe
df = pd.DataFrame(inflated_ensemble, columns=columnnames + ["probability"])

# TODO: compare the total probabilities
plot_columns = ["frac_omega_crit", "r_acc_sep", "q_acc_don"]
scales = ["log", "linear", "linear"]

generate_triangle_plot(
    df,
    scales=scales,
    plot_columns=plot_columns,
    plot_settings={"show_plot": True, "max_diff_threshold": 4},
    probability_column="probability",
)
