"""
Function to readout the results of the evolution of an individual system. We will run this with the input of detailed_DCO binary_c logging
"""

import os

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import (
    output_lines,
)

# Personal defaults
from david_phd_functions.binaryc.personal_defaults import personal_defaults
from grav_waves.settings import project_specific_settings

from readout_population_settings import readout_settings_file_and_return_bse_options
from readout_specific_system_settings import readout_specific_system_settings
from plot import plot_system_evolution

import numpy as np
import matplotlib.pyplot as plt

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.linestyle_hatch_colorlist import color_list
from david_phd_functions.plotting import custom_mpl_settings

from binarycpython.utils import stellar_types

custom_mpl_settings.load_mpl_rc()


def parse_function(self, output):
    """
    Parse function for single stars which catches the lines that start with DAVID_PPISN_SINGLE

    The output gets written to the data_dir + base_filename.
    """

    # Get filenames
    data_dir = self.custom_options["data_dir"]
    base_filename = self.custom_options["base_filename"]
    outfilename = os.path.join(data_dir, base_filename)

    # Check directory, make if necessary
    os.makedirs(data_dir, exist_ok=True)

    # The parameternames contained in the output line
    parameters = [
        "time",
        # /* ZAMS values */
        "zams_mass_1",
        "zams_mass_2",
        "zams_period",
        # /* Current values */
        "mass_1",
        "mass_2",
        "stellar_type_1",
        "stellar_type_2",
        "orbital_period",
        "eccentricity",
        "radius_1",
        "radius_2",
        "separation",
        "roche_radius_1",
        "roche_radius_2",
        "pre_event_mass_1",
        "pre_event_stellar_type_1",
        "pre_event_core_mass_1",
        "pre_event_co_core_mass_1",
        "pre_event_he_core_mass_1",
        "pre_event_mass_2",
        "pre_event_stellar_type_2",
        "pre_event_core_mass_2",
        "pre_event_co_core_mass_2",
        "pre_event_he_core_mass_2",
        "fallback_fraction_1",
        "fallback_mass_1",
        "fallback_fraction_2",
        "fallback_mass_2",
        "sn_type_1",
        "sn_type_2",
        "kick_velocity_1",
        "kick_velocity_2",
        "donor_MT_rate",
        "accretor_accretion_rate",
        # /* counters */
        "comenv_count",
        "rlof_count",
        "stable_rlof_count",
        # Extra info
        "RLOF_episode_number",
        "probability",
        "random_seed",
    ]

    # Set separator
    separator = "\t"
    # Check if the file actually exists, if not: make it and put the parameter names as a first line
    if os.path.exists(outfilename):
        os.remove(outfilename)

    if not os.path.exists(outfilename):
        with open(outfilename, "w") as f:
            f.write(separator.join(parameters) + "\n")

    # Parse output
    for line in output_lines(output):
        headerline = line.split()[0]
        if headerline == "GRAV_WAVE_DETAILED":
            values = line.split()[1:]

            # Check if the length of the output is the same as we expect
            if not len(values) == len(parameters):
                print(
                    "Length of readout values ({}) is not equal to length of parameters ({})".format(
                        len(values), len(parameters)
                    )
                )

            # write output to file
            with open(outfilename, "a") as f:
                f.write(separator.join(values) + "\n")


def change_dict_key(system_dict, rename_dict):
    """
    rename_dict: {source_keyname: target_keyname}
    """

    for key, value in rename_dict.items():
        system_dict[value] = system_dict[key]
        del system_dict[key]


#########
# a
run_system = True
plot_system = True

#########
# Read out the settings from the population that generated the weird systems
# Settings file populations:
settings_file = "example_settings.json"
population_settings = readout_settings_file_and_return_bse_options(settings_file)

########
# Convolution file and extract the systems
data_root = os.path.join(os.getenv("BINARYC_DATA_ROOT"), "GRAV_WAVES/server_results/")
convolution_filename = os.path.join(
    data_root,
    "SUPER_MID_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED_max_mass_150/dco_convolution_results/convolution_results.h5",
)
# general_query = "stable_rlof_counter == 0 & comenv_counter == 0 & primary_mass > 20 & primary_mass < 22"
general_query = "stable_rlof_counter == 0 & comenv_counter == 0 & primary_mass > 45"
# general_query = "stable_rlof_counter == 0 & comenv_counter == 0 & primary_mass > 50"
# general_query = "stable_rlof_counter == 0 & comenv_counter == 0 & primary_mass > 40 & eccentricity > 0.9 & mass_2 > 15"
# general_query = "stable_rlof_counter > 1 & comenv_counter == 0 & primary_mass > 20 & primary_mass < 22 & eccentricity < 0.2"

queried_system_df, queried_df = readout_specific_system_settings(
    convolution_filename,
    "primary_mass",
    "bhbh",
    "merger_rate",
    general_query=general_query,
)

system_id = 5
system_dict = queried_system_df.iloc[system_id].to_dict()
final_system_info = queried_df.iloc[system_id].to_dict()

# binsize = 0.01
# bins = np.arange(0, 1 + binsize, binsize)

# # plt.hist(
# #     queried_df["eccentricity"],
# #     bins=bins,
# #     weights=queried_df["number_per_solar_mass_values"],
# # )
# # plt.show()

print(final_system_info)
print(
    """
State at DCO formation:
\tM_1: {} Msun
\tM_2: {} Msun
\tPeriod: {} yr
\tEccentricity: {}
\tTime: {} yr
\tInspiral time: {} yr
""".format(
        final_system_info["mass_1"],
        final_system_info["mass_2"],
        final_system_info["period"],
        final_system_info["eccentricity"],
        final_system_info["formation_time_values_in_years"],
        final_system_info["merger_time_values_in_years"]
        - final_system_info["formation_time_values_in_years"],
    )
)

change_dict_key(
    system_dict,
    {"zams_mass_1": "M_1", "zams_mass_2": "M_2", "zams_period": "orbital_period"},
)


# import pandas as pd
# df = pd.read_csv("results/single_system.dat", sep="\s+")
# print(df['kick_velocity_2'].max())
# quit()

if run_system:

    ######################################################################
    # Set up population and set values
    ppisn_single_pop = Population()
    ppisn_single_pop.set(verbose=1)

    # Set total population settings
    ppisn_single_pop.set(**population_settings)

    # Set total population settings
    ppisn_single_pop.set(**system_dict)

    # Set more settings
    ppisn_single_pop.set(
        david_ppisn_logging=0,
        evolution_splitting=0,
        evolution_splitting_maxdepth=0,
        evolution_splitting_sn_n=1,
        parse_function=parse_function,
        david_logging_function=13,
        data_dir="results",
        base_filename="single_system.dat",
        accretion_limit_thermal_multiplier=10,
        accretion_limit_eddington_steady_multiplier=1,
        prevent_LWN_kick=1,
        # sn_kick_dispersion_II=0,
        # sn_kick_dispersion_IBC=0,
        # wind_mass_loss="WIND_ALGORITHM_NONE"
    )

    # print(ppisn_single_pop._return_argline())
    # quit()
    ppisn_single_pop.evolve_single()

if plot_system:
    plot_system_evolution("results/single_system.dat")
