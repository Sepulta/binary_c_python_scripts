"""
Star formation rate and metallicity distribution functions along with some other cosmology functions
"""

import numpy as np
import pandas as pd

import astropy
import astropy.units as u

from grav_waves.settings import cosmo

from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def starformation_rate_coen19(z, a, b, c, d):
    """
    Function from Neijsel et al 2019 (https://ui.adsabs.harvard.edu/abs/2019MNRAS.490.3740N/abstract)
    """

    rate = a * np.power(1 + z, b) / (1 + np.power((1 + z) / c, d))

    return rate * (u.Msun / (u.yr * (u.Mpc**3)))


def starformation_rate_dummy(constant=1):
    """
    Dummy starformation rate function. returns <constant> * (u.Msun/(u.yr * (u.Mpc**3)))
    """

    return constant * (u.Msun / (u.yr * (u.Gpc**3)))


def starformation_rate(z, cosmology_configuration, verbosity=0):
    """
    Function to determine the star formation rate.

    Input:
        z: redshift value
        cosmology_configuration: dict containing the choice of star formation rate function ('star_formation_rate_function') and a set of arguments ('star_formation_rate_args')

    Output:
        starformation rate in u.Msun/(u.yr * (u.Gpc**3))
    """

    if cosmology_configuration["star_formation_rate_function"] == "madau_dickinson_sfr":
        return starformation_rate_coen19(
            z, **cosmology_configuration["star_formation_rate_args"]
        ).to(u.Msun / (u.yr * (u.Gpc**3)))
    elif cosmology_configuration["star_formation_rate_function"] == "dummy":
        return starformation_rate_dummy().to(u.Msun / (u.yr * (u.Gpc**3)))
    else:
        raise ValueError("Unknown SFR function")


def mean_metallicity(z, z0, alpha):
    """
    Function for mean metallicity
    """

    return z0 * np.power(10, alpha * z)


def mean_mu(z, z0, alpha, sigma):
    """
    Function to calculate mu
    """

    return np.log(mean_metallicity(z, z0, alpha)) - np.power(sigma, 2) / 2


def metallicity_distribution_coen19(Z, z, z0, alpha, sigma):
    """
    Function to calculate the metallicity distribution fraction at a given redshift according to Neijsel et al 2019 (https://ui.adsabs.harvard.edu/abs/2019MNRAS.490.3740N/abstract)
    """

    return (1 / (Z * sigma * np.power(2 * np.pi, 0.5))) * np.exp(
        -np.power(np.log(Z) - mean_mu(z, z0, alpha, sigma), 2)
        / (2 * np.power(sigma, 2))
    )


def metallicity_distribution_dummy(constant=1):
    """
    Dummy metallicity distribution, based on the same functional form as neijsel et al 2019 (https://ui.adsabs.harvard.edu/abs/2019MNRAS.490.3740N/abstract)
    """

    # override redshift to have it not change

    return constant


def metallicity_distribution(metallicity, redshift, cosmology_configuration):
    """
    Function that calculates the metallicity probability distribution for a given metallicity and redshift

    Will call the chosen function with the arguments (metallicity, redshift, **cosmology_configuration['metallicity_distribution_args'])

    Input:
        metallicity: metallicty (non-log) value
        redshift: current redshift
        cosmology_configuration: dict containing information on the choice of prescription: 'metallicity_distribution_function' and a set of arguments for that function ('metallicity_distribution_args')

    Output:
        metallicity probability distribution: dP/dZ (will always be in in linear Z, not in log Z)
    """

    if cosmology_configuration["metallicity_distribution_function"] == "coen19":
        return metallicity_distribution_coen19(
            metallicity,
            redshift,
            **cosmology_configuration["metallicity_distribution_args"],
        )
    elif cosmology_configuration["metallicity_distribution_function"] == "dummy":
        return metallicity_distribution_dummy()
    elif cosmology_configuration["metallicity_distribution_function"] == "vanSon21":
        # TODO: implement Z-dist lieke
        raise NotImplementedError(
            "van Son et al 2021 metallicity distribution is not implemented yet"
        )
    else:
        raise ValueError("metallicity distribution option not known")


def create_metallicity_redshift_dataframe(
    amt_z_values, amt_metallicity_values, z_values, metallicities, config_dict_cosmology
):
    """
    Function to create the metallicity redshift dataframe
    """

    solar_value = config_dict_cosmology["solar_value"]
    min_value_metalprob = config_dict_cosmology["min_value_metalprob"]

    #
    all_redshifts = np.zeros(amt_z_values * amt_metallicity_values)
    all_metallicity_values = np.zeros(amt_z_values * amt_metallicity_values)
    all_probabilities = np.zeros(amt_z_values * amt_metallicity_values)

    #
    for i, redshift in enumerate(z_values):
        metallicity_distribution_values = metallicity_distribution(
            metallicities, redshift, config_dict_cosmology
        )
        normed_metallicity_distribution_values = (
            metallicity_distribution_values / np.sum(metallicity_distribution_values)
        )

        normed_metallicity_distribution_values[
            normed_metallicity_distribution_values < min_value_metalprob
        ] = 0
        renormed_normed_metallicity_distribution_values = (
            normed_metallicity_distribution_values
            / np.sum(normed_metallicity_distribution_values)
        )

        all_redshifts[
            i * amt_metallicity_values : (i + 1) * amt_metallicity_values
        ] = redshift
        all_metallicity_values[
            i * amt_metallicity_values : (i + 1) * amt_metallicity_values
        ] = (metallicities / solar_value)
        all_probabilities[
            i * amt_metallicity_values : (i + 1) * amt_metallicity_values
        ] = renormed_normed_metallicity_distribution_values

    # Put the metallicity evolution in a dataframe
    new_dataframe = pd.DataFrame(
        data=np.array([all_redshifts, all_metallicity_values, all_probabilities]).T,
        columns=["redshift", "metallicity", "probability"],
    )

    return new_dataframe


def create_metallicity_lookback_dataframe(
    amt_lookback_values,
    amt_metallicity_values,
    lookback_values,
    metallicities,
    config_dict_cosmology,
):
    """
    Function to create the metallicity redshift dataframe
    """

    solar_value = config_dict_cosmology["solar_value"]
    min_value_metalprob = config_dict_cosmology["min_value_metalprob"]

    #
    all_lookbacks = np.zeros(amt_lookback_values * amt_metallicity_values)
    all_metallicity_values = np.zeros(amt_lookback_values * amt_metallicity_values)
    all_probabilities = np.zeros(amt_lookback_values * amt_metallicity_values)

    for i, lookback in enumerate(lookback_values):

        # Turn it around
        universe_age = cosmo.age(0).value - lookback

        # calc redshift
        redshift = astropy.cosmology.z_at_value(cosmo.age, float(universe_age) * u.Gyr)

        # Calculate rest of the values
        metallicity_distribution_values = metallicity_distribution(
            metallicities, redshift, config_dict_cosmology
        )
        normed_metallicity_distribution_values = (
            metallicity_distribution_values / np.sum(metallicity_distribution_values)
        )

        normed_metallicity_distribution_values[
            normed_metallicity_distribution_values < min_value_metalprob
        ] = 0
        renormed_normed_metallicity_distribution_values = (
            normed_metallicity_distribution_values
            / np.sum(normed_metallicity_distribution_values)
        )

        all_lookbacks[
            i * amt_metallicity_values : (i + 1) * amt_metallicity_values
        ] = lookback
        all_metallicity_values[
            i * amt_metallicity_values : (i + 1) * amt_metallicity_values
        ] = (metallicities / solar_value)
        all_probabilities[
            i * amt_metallicity_values : (i + 1) * amt_metallicity_values
        ] = renormed_normed_metallicity_distribution_values

    # Put the metallicity evolution in a dataframe
    new_dataframe = pd.DataFrame(
        data=np.array([all_lookbacks, all_metallicity_values, all_probabilities]).T,
        columns=["lookback", "metallicity", "probability"],
    )

    return new_dataframe


def normalize_metallicities_distribution(
    redshift_value,
    cosmology_configuration,
    min_log10_Z=-12,
    max_log10_Z=0,
    steps_log10_Z=10000,
):
    """
    Function that calculates the normalisation constant for the metallicities_distribution at a given redshift
    """

    #
    stepsize_log10_Z = (max_log10_Z - min_log10_Z) / steps_log10_Z

    # Do numpy:
    # start_numpy = time.time()
    logz_array = np.linspace(min_log10_Z, max_log10_Z, steps_log10_Z)
    z_array = 10**logz_array

    dPdZ_array = metallicity_distribution(
        metallicity=z_array,
        redshift=redshift_value,
        cosmology_configuration=cosmology_configuration,
    )
    dPdlog10Z_array = dPdZ_array * np.log(10) * z_array

    P_array = dPdlog10Z_array * stepsize_log10_Z

    total_numpy = np.sum(P_array)
    # end_numpy = time.time()

    #     print(
    #         """
    # normalize_metallicities_distribution:
    #         redshift_value: {}
    #         min_log10_Z: {}
    #         max_log10_Z: {}
    #         steps_log10_Z: {}

    #         loop method: totalP: {} (took {}s)
    #         numpy method: totalP: {} (took {}s)
    # """.format(
    #         redshift_value,
    #         min_log10_Z,
    #         max_log10_Z,
    #         steps_log10_Z,
    #         total, end_loop-start_loop,
    #         total_numpy, end_numpy-start_numpy,
    #         )
    #     )

    return total_numpy


##############
# Functions that handle the conversion
def age_of_universe_to_redshift(age_of_universe):
    """
    Function to turn age of universe to redshift
    """

    # Calculate redshift
    redshift_value = astropy.cosmology.z_at_value(
        cosmo.age, age_of_universe * u.Gyr, zmin=0, zmax=10000
    )

    return redshift_value


def lookback_time_to_redshift(lookback_time):
    """
    Function to calculate the redshift corresponding to a lookback time

    input:
        lookback_time: lookback time in Gyr
    """

    if lookback_time == 0:
        lookback_time = 1e-6

    # Calculate redshift
    redshift_value = astropy.cosmology.z_at_value(
        cosmo.age, cosmo.age(0) - lookback_time * u.Gyr, zmin=0, zmax=10000
    )

    return redshift_value


def redshift_to_lookback_time(redshift):
    """
    Function to calculate the lookback time corresponding to a certain redshift
    """

    return cosmo.age(0) - cosmo.age(redshift)


def redshift_to_age_of_universe(redshift):
    """
    Function to calculate the age of the universe corresponding to a certain redshift
    """

    return cosmo.age(redshift)
