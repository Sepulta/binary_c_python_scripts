import h5py
import pandas as pd


combined_sn_file = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/sn_convolution_results/combined_sn_dataset.h5"
filtered_combined_sn_file = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/LOW_RES_SCHNEIDER_WIND_PPISN_NEW_FRYER_DELAYED/sn_convolution_results/filtered_combined_sn_dataset.h5"


combined_sn_df = pd.read_hdf(combined_sn_file, "/data/combined_dataframes")
filtered_combined_sn_file = pd.read_hdf(
    filtered_combined_sn_file, "/data/combined_dataframes"
)

print(filtered_combined_sn_file)
print(filtered_combined_sn_file.columns)
