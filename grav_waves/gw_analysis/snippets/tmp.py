import pandas as pd
import matplotlib.pyplot as plt

import time
import os
import numpy as np
import json

from david_phd_functions.grav_waves.dataframe_functions import (
    add_chirpmass_column,
    add_inspiral_time_column,
)
from david_phd_functions.grav_waves.merger_rate_functions import (
    calculate_probabilty_per_solarmass,
    calculate_mergerrate_per_myr_and_rvol,
)
from david_phd_functions.grav_waves.datafile_functions import (
    split_compact_objects_datafiles,
)

from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

from plot_functions import *

from david_phd_functions.binaryc.extra_funcs import print_defaults

age_universe_in_year = 13.7e9


# settings_file = 'results/results_bigsim/DEFAULT/GW_z0.02/simulation_20210324_172310_settings.json'

# print_defaults(settings_file)


# ### List the mergers in the catalog
# for merger_name in catalog.Catalog():
#     print(merger_name)


c = pycbc.catalog.Catalog(source="gwtc-2")
# print(dir(pycbc.catalog.Catalog(sou)))

print(pycbc.catalog.catalog.list_catalogs())
# mchirp, elow, ehigh = c.median1d('mchirp', return_errors=True)
# spin = c.median1d('chi_eff')

# pylab.errorbar(mchirp, spin, xerr=[-elow, ehigh], fmt='o', markersize=7)
# pylab.xlabel('Chirp Mass')
# pylab.xscale('log')
# pylab.ylabel('Effective Spin')
# pylab.show()
