"""
Function to plot the 2-d density plot for merger or formation rate per redshift per massratio
"""

import json
import h5py
import numpy as np
import pandas as pd

import astropy.units as u

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from grav_waves.convolution.functions.redshift_interpolation_functions import (
    test_lookback_time_to_redshift,
    create_interpolation_datasets,
)
from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    get_hists_multiprocessing,
)
from grav_waves.convolution.functions.plot_routines.plot_utility_functions import (
    filter_dco_type,
    get_rate_type_names,
    linestyle_list,
    add_contour_levels,
    get_num_subsets,
    plot_2d_results,
    long_name_dict,
)

from david_phd_functions.grav_waves.dataframe_functions import add_chirpmass_column
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.canvas_functions import return_canvas_with_subsets

#
number_per_gigaparsec_cubed_per_year = 1 / u.Gpc**3 / u.yr
sfr_unit = u.Msun / u.Gpc**3 / u.yr


def get_data(
    hdf5_filename,
    dco_type,
    massratio_bins,
    rate_key_in_df,
    rate_type_hdf5_key,
    divide_by_binsize,
    num_procs=1,
    query=None,
):
    """
    Function to read out the data from the hdf5
    """

    massratio_bins_size = np.diff(massratio_bins)

    hdf5_file = h5py.File(hdf5_filename, "r")
    sorted_keys = sorted(
        hdf5_file["data"][rate_type_hdf5_key].keys(),
        key=lambda key: float(key),
        reverse=False,
    )
    hdf5_file.close()

    # Get combined df
    combined_df = pd.read_hdf(hdf5_filename, "/data/combined_dataframes")

    # Add columns
    combined_df = add_chirpmass_column(combined_df, m1_name="mass_1", m2_name="mass_2")
    combined_df["total_mass"] = combined_df["mass_1"] + combined_df["mass_2"]
    combined_df["primary_mass"] = combined_df[["mass_1", "mass_2"]].max(axis=1)
    combined_df["secondary_mass"] = combined_df[["mass_1", "mass_2"]].min(axis=1)
    combined_df["mass_ratio"] = (
        combined_df["secondary_mass"] / combined_df["primary_mass"]
    )

    # Get the res dict
    res_dict = get_hists_multiprocessing(
        hdf5_filename=hdf5_filename,
        combined_df=combined_df,
        dco_type=dco_type,
        query=query,
        rate_key_in_df=rate_key_in_df,
        bins=massratio_bins,
        quantity="mass_ratio",
        divide_by_binsize=divide_by_binsize,
        rate_type_hdf5_key=rate_type_hdf5_key,
        sorted_keys=sorted_keys,
        num_procs=num_procs,
    )

    # Put all the results in lists
    total_redshift_list = []
    total_merger_rate_list = []

    for redshift_value in res_dict:
        total_redshift_list.append(redshift_value)
        total_merger_rate_list.append(list(res_dict[redshift_value]))

    #
    total_redshift_array = np.array(total_redshift_list)
    total_merger_rate_array = np.array(total_merger_rate_list)

    ###########################################
    return {
        "total_redshift_array": total_redshift_array,
        "total_rate_array": total_merger_rate_array,
    }


def plot_convolved_rate_density_per_mass_ratio(
    convolution_dataset_hdf5_filename,
    dco_type,
    rate_type,
    divide_by_binsize=False,
    querylist=None,
    time_bin_key=None,
    contourlevels=None,
    linestyles_contourlevels=None,
    add_ratio=False,
    num_procs=1,
    plot_settings=None,
):
    """
    Plot for the convolved rate density (i.e. rate per gigaparsec cubed)

    Args:
        - convolution_dataset_hdf5_filename: filename of the hdf5 file that contains all the data
        - rate_type: option to plot either merger rate or formation rate
        - mass_type: option to plot which mass

        - time_bin_key (Optional): If passed then we plot only the results of that certain time bin
    """

    #
    if not plot_settings:
        plot_settings = {}

    #
    linestyles_ratio_contourlevels = ["solid", "--", "-."]
    ratio_contourlevels = [0.1, 0.5, 0.90]
    x_scale = "linear"
    y_scale = "log"
    # long_name_quantity = long_name_dict[mass_quantity]
    (
        _,
        rate_key_in_df,
        long_name_rate_type,
        rate_type_hdf5_key,
        rate_time_key_in_df,
    ) = get_rate_type_names(rate_type)

    #
    main_result_units = number_per_gigaparsec_cubed_per_year
    if divide_by_binsize:
        main_result_units = main_result_units / u.Msun

    #
    massratio_bins = np.arange(0, 1, 0.01)
    massratio_bins_size = np.diff(massratio_bins)
    massratio_centers = (massratio_bins[1:] + massratio_bins[:-1]) / 2

    ####
    # Read out the data
    hdf5_file = h5py.File(convolution_dataset_hdf5_filename, "r")
    settings = hdf5_file["settings"]
    convolution_settings = json.loads(settings["convolution_configuration"][()])
    hdf5_file.close()

    # Calculate the data:
    results_dict = {}
    results_dict["all"] = get_data(
        hdf5_filename=convolution_dataset_hdf5_filename,
        dco_type=dco_type,
        massratio_bins=massratio_bins,
        rate_key_in_df=rate_key_in_df,
        rate_type_hdf5_key=rate_type_hdf5_key,
        divide_by_binsize=divide_by_binsize,
        num_procs=num_procs,
    )
    if not querylist is None:
        for query_dict in querylist:
            results_dict[query_dict["name"]] = get_data(
                hdf5_filename=convolution_dataset_hdf5_filename,
                dco_type=dco_type,
                massratio_bins=massratio_bins,
                rate_key_in_df=rate_key_in_df,
                rate_type_hdf5_key=rate_type_hdf5_key,
                divide_by_binsize=divide_by_binsize,
                num_procs=num_procs,
                query=query_dict["query"],
            )

    X, Y = np.meshgrid(results_dict["all"]["total_redshift_array"], massratio_centers)

    # Set up axes
    fig = plt.figure(figsize=(40, 40))

    # Get the autogenerated canvas
    num_subsets = get_num_subsets(querylist)
    fig, _, axes_dict = return_canvas_with_subsets(
        num_subsets, fig=fig, add_ratio_axes=add_ratio
    )

    # # Plot the results
    # pcm = axes_dict['all_axis'].pcolormesh(
    #     X,
    #     Y,
    #     results_dict['all']['total_rate_array'].T,
    #     norm=norm,
    #     shading='auto',
    #     antialiased=plot_settings.get('antialiased', False),
    #     rasterized=plot_settings.get('rasterized', False),
    # )

    # # Plot the subsets
    # if not querylist is None:
    #     for query_i, query_dict in enumerate(querylist):
    #         X, Y = np.meshgrid(results_dict[query_dict['name']]['total_redshift_array'], massratio_centers)
    #         pcm = axes_dict['subset_axes'][query_i].pcolormesh(
    #             X,
    #             Y,
    #             results_dict[query_dict['name']]['total_rate_array'].T,
    #             norm=norm,
    #             shading='auto',
    #             antialiased=plot_settings.get('antialiased', False),
    #             rasterized=plot_settings.get('rasterized', False),
    #         )

    #         axes_dict['subset_axes'][query_i].set_title("{}: {}".format(query_dict['name'], long_name_rate_type), fontsize=18)

    #         # Add ratio plots
    #         if add_ratio:
    #             ratio_to_total = np.divide(
    #                 results_dict[query_dict['name']]['total_rate_array'],
    #                 results_dict['all']['total_rate_array'],
    #                 out=np.zeros_like(results_dict[query_dict['name']]['total_rate_array']),
    #                 where=results_dict['all']['total_rate_array']!=0
    #             ) #only divide nonzeros else 1

    #             ratio_to_total[ratio_to_total == 0] = np.nan
    #             pcm_ratio = axes_dict['ratio_axes'][query_i].pcolormesh(
    #                 X,
    #                 Y,
    #                 ratio_to_total.T,
    #                 norm=norm_ratio,
    #                 shading='auto',
    #                 antialiased=plot_settings.get('antialiased', False),
    #                 rasterized=plot_settings.get('rasterized', False),
    #             )

    #             axes_dict['ratio_axes'][query_i].set_title("{}: Ratio".format(query_dict['name']), fontsize=18)

    # # make colorbar
    # cb = matplotlib.colorbar.ColorbarBase(
    #     axes_dict['colorbar_axis'],
    #     norm=norm,
    #     extend='min'
    # )
    # cb.ax.set_ylabel("Rate density [{}]".format(main_result_units.unit.to_string('latex_inline')))

    # if add_ratio:
    #     # Consider using a logscale for the ratio
    #     cb_ratio = matplotlib.colorbar.ColorbarBase(
    #         axes_dict['colorbar_axis_ratio'],
    #         norm=norm_ratio,
    #     )
    #     cb_ratio.ax.set_ylabel("Ratio to total")

    # # Set contourlevels and lines on the colorbar
    # if (not contourlevels is None) and (not linestyles_contourlevels is None) and (len(contourlevels)==len(linestyles_contourlevels)):
    #     _ = axes_dict['all_axis'].contour(
    #         X,
    #         Y,
    #         results_dict['all']['total_rate_array'].T,
    #         levels=contourlevels,
    #         colors='k',
    #         linestyles=linestyles_contourlevels,
    #     )

    #     # Set lines on the colorbar
    #     for contour_i, _ in enumerate(contourlevels):
    #         cb.ax.plot([cb.ax.get_xlim()[0], cb.ax.get_xlim()[1]], [contourlevels[contour_i]]*2, 'black', linestyle=linestyles_contourlevels[contour_i])

    #     # Add contours on the subplots
    #     if not querylist is None:
    #         for query_i, query_dict in enumerate(querylist):
    #             _ = axes_dict['subset_axes'][query_i].contour(
    #                 X,
    #                 Y,
    #                 results_dict[query_dict['name']]['total_rate_array'].T,
    #                 levels=contourlevels,
    #                 colors='k',
    #                 linestyles=linestyles_contourlevels,
    #             )

    #             ratio_to_total = np.divide(
    #                 results_dict[query_dict['name']]['total_rate_array'],
    #                 results_dict['all']['total_rate_array'],
    #                 out=np.zeros_like(results_dict[query_dict['name']]['total_rate_array']),
    #                 where=results_dict['all']['total_rate_array']!=0
    #             ) #only divide nonzeros else 1

    #             if add_ratio:
    #                 # Add contourlines
    #                 if (not ratio_contourlevels is None) and (not linestyles_ratio_contourlevels is None) and (len(ratio_contourlevels)==len(linestyles_ratio_contourlevels)):
    #                     _ = axes_dict['ratio_axes'][query_i].contour(
    #                         X,
    #                         Y,
    #                         ratio_to_total.T,
    #                         levels=ratio_contourlevels,
    #                         colors='k',
    #                         linestyles=linestyles_ratio_contourlevels,
    #                     )

    #                 # Set lines on the colorbar
    #                 for contour_i, _ in enumerate(ratio_contourlevels):
    #                     cb_ratio.ax.plot([cb_ratio.ax.get_xlim()[0], cb_ratio.ax.get_xlim()[1]], [ratio_contourlevels[contour_i]]*2, 'black', linestyle=linestyles_ratio_contourlevels[contour_i])

    ##
    # Plot 2-d results
    fig, axes_dict, cb, cb_ratio = plot_2d_results(
        fig=fig,
        axes_dict=axes_dict,
        X=X,
        Y=Y,
        long_name=long_name_quantity,
        add_ratio=add_ratio,
        results_dict=results_dict,
        querylist=querylist,
        plot_settings=plot_settings,
        x_scale=x_scale,
        y_scale=y_scale,
    )

    ##
    # Add contour levels
    fig, axes_dict = add_contour_levels(
        results_dict=results_dict,
        X=X,
        Y=Y,
        fig=fig,
        axes_dict=axes_dict,
        cb=cb,
        cb_ratio=cb_ratio,
        contourlevels=contourlevels,
        ratio_contourlevels=ratio_contourlevels,
        linestyle_list=linestyle_list,
        querylist=querylist,
        add_ratio=add_ratio,
    )

    ###
    #
    axes_dict["all_axis"].set_xlabel(r"Redshift")
    axes_dict["all_axis"].set_ylabel(
        "Mass ratio\n" + r"(q = $M_{\mathrm{secondary}}/M_{\mathrm{primary}}$)"
    )

    #
    title_text = (
        "Intrinsic {} density of merging BHBH systems per {} over cosmic time".format(
            long_name_rate_type.lower(), "mass ratio"
        )
    )
    axes_dict["all_axis"].set_title(title_text, fontsize=26)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    # Global settings
    rebinned_results_filename = "/home/david/projects/binary_c_root/binary_c_python_scripts/grav_waves/convolution/scripts/new_main/output/array_test/convolution/without_detection_probability/rebinned_convolution_results.h5"
    convolution_plot_dir = "output/"
    divide_by_binsize = False

    querylist_lieke = [
        {"name": "CE channel", "query": "(comenv_counter >= 1)"},
        {
            "name": "No CE channel",
            "query": "(undergone_CE_with_HG_donor==0 & undergone_CE_with_MS_donor==0 & comenv_counter==0)",
        },
    ]

    #
    plot_convolved_rate_density_per_mass_ratio(
        rebinned_results_filename,
        divide_by_binsize=False,
        rate_type="merger_rate",
        dco_type="bhbh",
        # add_ratio=True,
        querylist=querylist_lieke,
        plot_settings={
            "show_plot": True,
            "output_name": "output/plot_convolved_rate_density_per_mass_ratio.pdf",
            "simulation_name": "test",
            "antialiased": True,
            "rasterized": True,
            "runname": "plot_convolved_rate_density_per_mass_ratio",
        },
    )
else:
    matplotlib.use("Agg")
