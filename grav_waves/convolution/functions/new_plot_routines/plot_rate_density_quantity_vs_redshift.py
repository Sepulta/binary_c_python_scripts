"""
Function to plot a certain quantity vs redshift

TODO: consider making a 2-d KDE plot out of this
    https://stackoverflow.com/questions/41577705/how-does-2d-kernel-density-estimation-in-python-sklearn-work
TODO: add the divide_by_binsize
"""

import h5py
import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from grav_waves.convolution.functions.new_plot_routines.plot_utility_functions import (
    get_num_subsets,
    extract_columns_from_querylist,
    quantity_name_dict,
    quantity_unit_dict,
    rate_type_name_dict,
    rate_density_units,
    add_columns_to_df,
    dco_type_query,
    readout_rate_array,
    histogram_with_2d_weights,
    create_centers_from_bins,
    plot_2d_results,
    plot_contourlevels,
    linestyle_list,
)

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.canvas_functions import return_canvas_with_subsets
from david_phd_functions.plotting import custom_mpl_settings

custom_mpl_settings.load_mpl_rc()


def readout_rate_data_and_make_hist(
    filename,
    combined_df,
    rate_type,
    dco_type,
    quantity,
    quantity_bins,
    general_query=None,
    query=None,
):
    """
    Function to read out the rate data and make the histogram
    """

    # Get the quantity data from the dataframe
    quantity_data = combined_df[quantity]

    # Create masks and apply, taking into account that the query and general query can be None
    dco_mask = combined_df.eval(dco_type_query(dco_type)).to_numpy()
    general_query_mask = (
        combined_df.eval(general_query)
        if general_query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    query_mask = (
        combined_df.eval(query)
        if query is not None
        else np.ones(shape=dco_mask.shape, dtype=bool)
    )
    mask = dco_mask * general_query_mask * query_mask

    # mask the quantity data
    masked_quantity_data = quantity_data[mask]

    # Read out rate data
    datafile = h5py.File(filename)
    rate_array, redshifts = readout_rate_array(
        datafile=datafile,
        rate_key=rate_type,
        mask=mask,
        max_redshift=10,  # TODO: add this as a parameter to this function
    )
    datafile.close()

    # Make histogram
    hist = histogram_with_2d_weights(
        quantity_array=masked_quantity_data,
        quantity_bins=quantity_bins,
        weight_array=rate_array,
    ).T

    return hist, redshifts


def get_data(
    filename,
    rate_type,
    dco_type,
    quantity,
    quantity_bins,
    divide_by_binsize,
    general_query=None,
    querylist=None,
):
    """
    Function to read out the data from the datafile
    """

    ####
    # Read out dataframe
    combined_df = pd.read_hdf(filename, "/data/combined_dataframes")

    # Add columns
    used_columns = [quantity] + extract_columns_from_querylist(querylist)
    columns_to_add = [
        column for column in used_columns if not column in combined_df.columns
    ]
    combined_df = add_columns_to_df(combined_df, columns_to_add)

    # Create result dict
    result_dict = {}

    #
    result_dict["all"], redshifts = readout_rate_data_and_make_hist(
        filename=filename,
        combined_df=combined_df,
        rate_type=rate_type,
        dco_type=dco_type,
        quantity=quantity,
        quantity_bins=quantity_bins,
        general_query=general_query,
        query=None,
    )

    # Loop over queries
    if querylist is not None:
        for query_dict in querylist:
            result_dict[query_dict["name"]], _ = readout_rate_data_and_make_hist(
                filename=filename,
                combined_df=combined_df,
                rate_type=rate_type,
                dco_type=dco_type,
                quantity=quantity,
                quantity_bins=quantity_bins,
                general_query=general_query,
                query=query_dict["query"],
            )

    return result_dict, redshifts


def plot_quantity_vs_redshift(
    filename,
    rate_type,
    dco_type,
    quantity,
    quantity_bins,
    general_query=None,
    querylist=None,
    add_ratio=False,
    x_scale="linear",
    y_scale="linear",
    verbose=0,
    plot_settings={},
):
    """
    Function to plot quantity vs redshift
    """

    #
    x_label = "Redshift"
    y_label = r"{} [{}]".format(
        quantity_name_dict[quantity], quantity_unit_dict[quantity].to_string("latex")
    )

    #
    contourlevels = [1e-1, 1e0, 1e1]
    ratio_contourlevels = [0.1, 0.5, 0.9]

    #
    quantity_bincenters = create_centers_from_bins(quantity_bins)

    #####################
    # Get results
    results_dict, redshifts = get_data(
        filename=filename,
        rate_type=rate_type,
        dco_type=dco_type,
        quantity=quantity,
        quantity_bins=quantity_bins,
        divide_by_binsize=False,
        general_query=general_query,
        querylist=querylist,
    )

    #
    max_val = results_dict["all"].max()
    custom_min_val = 10 ** (
        np.log10(max_val) - plot_settings.get("probability_floor", 4)
    )

    norm = colors.LogNorm(vmin=custom_min_val, vmax=max_val)

    #####################
    # Plot data

    ##
    # Figure out the number of subplots
    num_subsets = get_num_subsets(querylist)

    ##
    # Set up axes
    fig = plt.figure(figsize=(40, 40))
    fig, _, axes_dict = return_canvas_with_subsets(
        num_subsets, fig=fig, add_ratio_axes=add_ratio
    )

    ##
    # Plot 2-d results
    X, Y = np.meshgrid(redshifts, quantity_bincenters)
    fig, axes_dict, cb, cb_ratio = plot_2d_results(
        fig=fig,
        axes_dict=axes_dict,
        X=X,
        Y=Y,
        x_label=x_label,
        y_label=y_label,
        add_ratio=add_ratio,
        results_dict=results_dict,
        querylist=querylist,
        plot_settings=plot_settings,
        x_scale=x_scale,
        y_scale=y_scale,
        norm=norm,
    )

    ##
    # Add contour levels
    fig, axes_dict = plot_contourlevels(
        results_dict=results_dict,
        X=X,
        Y=Y,
        fig=fig,
        axes_dict=axes_dict,
        cb=cb,
        cb_ratio=cb_ratio,
        contourlevels=contourlevels,
        ratio_contourlevels=ratio_contourlevels,
        linestyle_list=linestyle_list,
        querylist=querylist,
        add_ratio=add_ratio,
    )

    ###
    # Make up
    axes_dict["all_axis"].set_xlabel(x_label)
    axes_dict["all_axis"].set_ylabel(y_label)
    axes_dict["colorbar_axis"].set_ylabel(
        r"{} density [{}]".format(
            rate_type_name_dict[rate_type], rate_density_units.to_string("latex")
        )
    )

    #
    title_text = "Intrinsic {} density of {} systems for {} vs redshift".format(
        rate_type_name_dict[rate_type].lower(),
        dco_type.upper(),
        quantity_name_dict[quantity].lower(),
    )
    axes_dict["all_axis"].set_title(title_text, fontsize=26)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #
    filename = "/home/david/projects/binary_c_root/results/GRAV_WAVES/server_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED/convolution_results/rebinned_convolution_results.h5"
    # filename = "/home/david/projects/binary_c_root/results/GRAV_WAVES/convolution_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-5_CORE_MASS_SHIFT/convolution_results/rebinned_convolution_results.h5"
    # filename = "/home/david/projects/binary_c_root/results/GRAV_WAVES/convolution_results/HIGH_RES_LOGSPACED_HIGH_RES_SCHNEIDER_MASS_PPISN_NEW_FRYER_DELAYED_-10_CORE_MASS_SHIFT/convolution_results/rebinned_convolution_results.h5"

    querylist_lieke = [
        {"name": "CE channel", "query": "(comenv_counter >= 1)"},
        {"name": "No CE channel", "query": "(comenv_counter == 0)"},
    ]

    querylist_ppisn = [
        {"name": "Primary ppisn", "query": "(primary_undergone_ppisn == 1)"},
        {"name": "No Primary ppisn", "query": "(primary_undergone_ppisn == 0)"},
    ]

    #
    plot_quantity_vs_redshift(
        filename=filename,
        rate_type="merger_rate",
        dco_type="bhbh",
        # quantity='primary_mass',
        # quantity_bins=np.arange(0, 70, 1),
        # quantity='mass_ratio',
        # quantity_bins=np.arange(0, 1, 0.025),
        quantity="total_mass",
        quantity_bins=np.arange(0, 100, 2),
        # quantity='chirp_mass',
        # quantity_bins=np.arange(0, 50, 1),
        querylist=querylist_ppisn,
        add_ratio=True,
        x_scale="linear",
        y_scale="linear",
        verbose=0,
        plot_settings={"show_plot": True, "probability_floor": 3},
    )

else:
    matplotlib.use("Agg")
