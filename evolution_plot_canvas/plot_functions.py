def plot_entire_evolution_binary_system(df, plot_settings=None):
    """
    Function to plot the entire evolution of a binary system

    TODO: limit to when they're not changing anymore
    """

    if not plot_settings:
        plot_settings = {}

    amt_plots = 4
    ylabel_fontsize = 20
    legend_fontsize = 6

    # Get info about rlof episodes
    summary_df = get_rlof_start_end(df)
    rlofs_df = summary_df[summary_df.rlofing != 0]

    #
    stable_rlofs = rlofs_df[rlofs_df.rlof_stability_start == 0]
    unstable_rlofs = rlofs_df[rlofs_df.rlof_stability_start > 0]

    # Set up canvas and grid
    fig3 = plt.figure(figsize=(12, amt_plots * 12))
    gs = fig3.add_gridspec(
        amt_plots * 5,
        4,
    )

    # Set up axes
    ax_mass = fig3.add_subplot(gs[0:4, :])
    ax_radii = fig3.add_subplot(gs[4:8, :], sharex=ax_mass)
    ax_periods = fig3.add_subplot(gs[8:12, :], sharex=ax_mass)
    ax_v_eq = fig3.add_subplot(gs[12:16, :], sharex=ax_mass)
    ax_stellar_types = fig3.add_subplot(gs[17:20, :], sharex=ax_mass)

    axes = [ax_mass, ax_radii, ax_periods, ax_v_eq, ax_stellar_types]

    #
    print(df.columns)

    ######################################
    # Axis mass
    ax_mass.plot(df["time"], df["mass_1"], color="r", label="mass 1")
    ax_mass.plot(df["time"], df["mass_2"], color="b", label="mass 2")

    ax_mass.plot(
        df["time"],
        df["zams_mass_1"],
        color="r",
        linestyle="--",
        label="initial mass 1",
        alpha=0.5,
    )
    ax_mass.plot(
        df["time"],
        df["zams_mass_2"],
        color="b",
        linestyle="--",
        label="initial mass 2",
        alpha=0.5,
    )

    ax_mass.plot(
        df["time"], df["mass_1"] + df["mass_2"], color="black", label="Total mass"
    )
    ax_mass.plot(
        df["time"],
        df["zams_mass_1"] + df["zams_mass_2"],
        color="black",
        linestyle="--",
        alpha=0.5,
        label="Total mass",
    )

    ax_mass.set_ylabel(r"Mass [M$_{\odot}$]", fontsize=ylabel_fontsize)
    ax_mass.legend(loc="best", fontsize=legend_fontsize)

    ######################################
    # Axis Radius
    df["initial_radius_1"] = df.iloc[0, list(df.columns).index("radius_1")]
    df["initial_radius_2"] = df.iloc[0, list(df.columns).index("radius_2")]
    ax_radii.plot(df["time"], df["radius_1"], color="r", label="mass 1")
    ax_radii.plot(df["time"], df["radius_2"], color="b", label="mass 2")
    ax_radii.plot(
        df["time"],
        df["initial_radius_1"],
        color="r",
        linestyle="--",
        label="initial radius 1",
        alpha=0.5,
    )
    ax_radii.plot(
        df["time"],
        df["initial_radius_2"],
        color="b",
        linestyle="--",
        label="initial radius 2",
        alpha=0.5,
    )

    ax_radii.plot(
        df["time"],
        df["roche_radius_1"],
        color="r",
        linestyle=":",
        label="roche radius 1",
        alpha=0.5,
        linewidth=2,
    )
    ax_radii.plot(
        df["time"],
        df["roche_radius_2"],
        color="b",
        linestyle=":",
        label="roche radius 2",
        alpha=0.5,
        linewidth=2,
    )

    ax_radii.set_yscale("log")
    ax_radii.set_ylim([10**-4, ax_radii.get_ylim()[1]])
    ax_radii.set_ylabel(r"Radius [R$_{\odot}$]", fontsize=ylabel_fontsize)
    ax_radii.legend(loc="best", fontsize=legend_fontsize)

    ######################################
    # Angular frequencies
    ax_periods.plot(
        df["time"],
        (2 * np.pi) / df["period"],
        color="black",
        label="orbital angular frequency",
        alpha=0.5,
    )
    ax_periods.plot(
        df["time"], df["omega_1"], color="r", label="stellar angular frequency 1"
    )
    ax_periods.plot(
        df["time"], df["omega_2"], color="b", label="stellar angular frequency 2"
    )

    ax_periods_twin = ax_periods.twinx()
    ax_periods_twin.plot(
        df["time"],
        df["omega_crit_1"],
        linestyle=":",
        color="r",
        label="critical stellar angular frequency 1",
        alpha=0.5,
    )
    ax_periods_twin.plot(
        df["time"],
        df["omega_crit_2"],
        linestyle=":",
        color="b",
        label="critical stellar angular frequency 2",
        alpha=0.5,
    )
    ax_periods.plot(
        np.nan,
        linestyle=":",
        color="r",
        label="critical stellar angular frequency 1",
        alpha=0.5,
    )
    ax_periods.plot(
        np.nan,
        linestyle=":",
        color="b",
        label="critical stellar angular frequency 2",
        alpha=0.5,
    )

    ax_periods.set_yscale("log")
    ax_periods.set_ylabel(r"Frequency [yr$^{-1}$]", fontsize=ylabel_fontsize)

    ######################################
    # equatorial Rotation rate
    ax_v_eq_twin = ax_v_eq.twinx()
    ax_v_eq.plot(df["time"], df["v_eq_1"], color="r", label="Veq 1")
    ax_v_eq.plot(df["time"], df["v_eq_2"], color="b", label="Veq 2")
    ax_v_eq_twin.plot(
        df["time"],
        df["v_eq_ratio_1"],
        color="r",
        linestyle="--",
        label="Veq ratio 1",
        alpha=0.5,
    )
    ax_v_eq_twin.plot(
        df["time"],
        df["v_eq_ratio_2"],
        color="b",
        linestyle="--",
        label="Veq ratio 2",
        alpha=0.5,
    )

    ax_v_eq.plot(np.nan, color="r", linestyle="--", label="Veq ratio 1", alpha=0.5)
    ax_v_eq.plot(np.nan, color="b", linestyle="--", label="Veq ratio 2", alpha=0.5)

    ax_v_eq.set_ylabel(r"V$_{\mathrm{eq}}$ []", fontsize=ylabel_fontsize)

    ax_v_eq.legend(loc="best", fontsize=legend_fontsize)
    ax_v_eq.set_yscale("log")

    ######################################
    # stellar types
    ax_stellar_types.plot(df["time"], df["stellar_type_1"], color="r", label="mass 1")
    ax_stellar_types.plot(df["time"], df["stellar_type_2"], color="b", label="mass 2")
    ax_stellar_types.set_ylabel("Stellar type", fontsize=ylabel_fontsize)

    # #
    # for ax in axes[:-2]:
    #     # ax.get_xaxis().set_ticks([])
    #     ax.tick_params(axis='x', which='both',
    #                     bottom=False)
    #
    axes[-1].set_xlabel(r"Time [Myr]")

    # Make a color on the plots:
    for stable_i in range(len(stable_rlofs.index)):
        begin_time = stable_rlofs.iloc[stable_i]["BeginTime"]
        end_time = stable_rlofs.iloc[stable_i]["EndTime"]

        for ax in axes:
            ylims = ax.get_ylim()
            ax.fill_between(
                df[df.time <= end_time][df.time >= begin_time]["time"],
                ylims[0],
                ylims[1],
                color="green",
                alpha=0.5,
                hatch="\\",
            )

    # Make a color on the plots:
    for unstable_i in range(len(unstable_rlofs.index)):
        begin_time = unstable_rlofs.iloc[unstable_i]["BeginTime"]
        end_time = unstable_rlofs.iloc[unstable_i]["EndTime"]

        for ax in axes:
            ylims = ax.get_ylim()
            ax.fill_between(
                df[df.time <= end_time][df.time >= begin_time]["time"],
                ylims[0],
                ylims[1],
                color="red",
                alpha=0.5,
                hatch="//",
            )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def generate_plots(df, limit_end_df=True, pdf_output_dir=None):
    """
    Function that wraps the plotting routines to do some work on the dataframe:
    - cut off the end that does not change
    - identify the different RLOF episodes and generate sub-df's for that
    """

    #
    if limit_end_df:
        last_st_1 = df["stellar_type_1"].to_list()[-1]
        last_st_2 = df["stellar_type_2"].to_list()[-1]

        first_time_last_st_1 = df[df.stellar_type_1 == last_st_1]["time"].to_list()[0]
        first_time_last_st_2 = df[df.stellar_type_2 == last_st_2]["time"].to_list()[0]

        #
        last_time_things_changed = np.max([first_time_last_st_1, first_time_last_st_2])

        df = df[df.time <= last_time_things_changed]

    # Get info about rlof episodes
    summary_df = get_rlof_start_end(df)

    #
    # plot_entire_evolution_binary_system(df, plot_settings={'show_plot': True})

    # Plot individual stable rlofs
    rlofs_df = summary_df[summary_df.rlofing != 0]

    #
    stable_rlofs = rlofs_df[rlofs_df.rlof_stability_start == 0]

    for stable_i in range(len(stable_rlofs.index)):
        begin_time = stable_rlofs.iloc[stable_i]["BeginTime"]
        end_time = stable_rlofs.iloc[stable_i]["EndTime"]

        stable_rlof_df = df[df.time >= begin_time][df.time <= end_time]

        block = False if not stable_i == len(stable_rlofs.index) - 1 else True
        plot_entire_evolution_binary_system(
            stable_rlof_df, plot_settings={"show_plot": True, "block_plot": block}
        )
