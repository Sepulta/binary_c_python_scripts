import numpy as np


def new_fit_mass_removal(Z, mc_co):
    """
    Prescription to calculate the mass removal according to mathieus new fit
    """

    # removal = (0.0006 * np.log10(Z) + 0.0054) * np.power((mc_co + 34.8), 3) - 0.0013 * np.power(mc_co + 34.8, 2)
    removal = (5.72629809e-04 * np.log10(Z) + 5.43166453e-03) * np.power(
        (mc_co - 3.47840387e01), 3
    ) - 1.26709046e-03 * np.power(mc_co - 3.47840387e01, 2)
    return removal


print(new_fit_mass_removal(1e-4, 51.4164))
