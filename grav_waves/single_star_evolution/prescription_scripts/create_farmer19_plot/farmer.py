import math
import matplotlib.pyplot as plt
import numpy as np

from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import save_loop

custom_mpl_settings.load_mpl_rc()


def farmer19_fit(Mco, Z):
    """
    Function that gives the remnant BH mass for a given Mco and Z
    """

    a1 = -0.096
    a2 = 8.564
    a3 = -2.07
    a4 = -152.97

    if Mco < 38:
        Mbh = 4 + Mco

    elif 38 <= Mco <= 65:
        Mbh = (a1 * Mco**2) + a2 * Mco + a3 * math.log10(Z) + a4

    else:
        Mbh = 0

    return Mbh


print(farmer19_fit(38, 1e-3))
quit()
# def plot_farmer19(MCo_range, Z):
#     """
#     Function that plots the value of the remnant BH for a range of CO core masses at a given metallicity
#     """

Mco_range = np.linspace(22, 80, 1000)

Z = 0.001
Z_range = [0.02, 0.01, 0.002, 0.001, 0.0002, 0.0001]

result_dict = {}
for Z in Z_range:
    Mbh_vals = [farmer19_fit(Mco, Z) for Mco in Mco_range]
    result_dict[Z] = Mbh_vals


for Z_val in result_dict:
    plt.plot(Mco_range, result_dict[Z_val], "ro", label="Z={}".format(str(Z_val)))


plt.xlabel(r"CO core mass [M$_{\odot}$]")
plt.ylabel(r"Remnant BH mass [M$_{\odot}$]")
plt.title("PPISN remnant BH mass")
plt.legend()
save_loop(name="farmer19_ppisn.{format}", formats=["png"])
plt.show()
