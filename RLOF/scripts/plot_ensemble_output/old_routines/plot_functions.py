"""
File containing the plotting functions for the
"""

import os
import time
import copy
import json

import numpy as np

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors

from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()

from binarycpython.utils.stellar_types import STELLAR_TYPE_DICT_SHORT
from binarycpython.utils.functions import merge_dicts

from RLOF_logging.fractions_separation_massratio.functions import (
    getFromDict,
    get_recursive_sum,
    flatten_data_ensemble2d,
    flatten_data_ensemble1d,
    generate_bins_from_keys,
    return_float_list_of_dictkeys,
    merge_all_subdicts,
    return_string,
    return_keys_bins_and_bincenters,
    recursively_merge_all_subdicts,
    combine_or_filter_stellar_types,
)

from david_phd_functions.masstransfer.functions import (
    rochelobe_radius_accretor,
    circularisation_radius,
    minimum_radius,
)
from RLOF_logging.fractions_separation_massratio.comparison_specific_angmoms.functions import (
    ratio_disk_iso,
)


def plot_general_probability_fractions_accretors(
    ensemble_data, sublist, weight_type, bin_type, plot_settings=None
):
    """
    Function to get the fractions for all the accretor, for both the disk and the non disk
    """

    if not plot_settings:
        plot_settings = {}

    # handle the type selections
    if weight_type == "mass":
        longname = "Mass weighted"
    elif weight_type == "time":
        longname = "Time weighted"
    else:
        raise ValueError("Unknown weight type")

    if bin_type == "linear":
        longname = "Linear spacing"
    elif bin_type == "log10":
        longname = "Log10 spacing"
    else:
        raise ValueError("Unknown bin type")

    # Select sub ensemble
    new_sublist = sublist + [weight_type, bin_type]
    sub_ensemble = getFromDict(ensemble_data, new_sublist)

    # Set up arrays
    direct_accretor_array = np.array(np.zeros(16))
    disk_accretor_array = np.array(np.zeros(16))

    # Direct impact
    for accretor_n in sorted(sub_ensemble["disk"]["0"]["stellar_type_accretor"]):
        sum_of_donors = get_recursive_sum(
            sub_ensemble["disk"]["0"]["stellar_type_accretor"][accretor_n]
        )
        direct_accretor_array[int(accretor_n)] = sum_of_donors

    # disk impact
    for accretor_n in sorted(sub_ensemble["disk"]["1"]["stellar_type_accretor"]):
        sum_of_donors = get_recursive_sum(
            sub_ensemble["disk"]["1"]["stellar_type_accretor"][accretor_n]
        )
        disk_accretor_array[int(accretor_n)] = sum_of_donors

    # Calculate normalisation: In this case we will normalize by ALL the masstransfer for this bin and weight
    normalisation_value = get_recursive_sum(sub_ensemble["disk"])
    normalisation_string = return_string(
        weight_type=weight_type, bin_type=bin_type, disk_type="a", accretor_type="a"
    )

    # Combine
    combined_accretor_array = direct_accretor_array + disk_accretor_array

    # Normalize
    normalized_combined_accretor_array = combined_accretor_array / normalisation_value
    normalized_direct_accretor_array = direct_accretor_array / normalisation_value
    normalized_disk_accretor_array = disk_accretor_array / normalisation_value

    # get stellar type info
    stellar_type_shorts = [
        STELLAR_TYPE_DICT_SHORT[key] for key in sorted(STELLAR_TYPE_DICT_SHORT.keys())
    ]
    stellar_type_numbers = np.array(list(range(17)))

    ###########
    # Plotting
    fig = plt.figure(constrained_layout=False, figsize=(20, 20))

    gs = fig.add_gridspec(nrows=8, ncols=8)

    ax_hist = fig.add_subplot(gs[:6, :-2])
    ax_fractions = fig.add_subplot(gs[6:, :-2], sharex=ax_hist)
    ax_piechart = fig.add_subplot(gs[0:2, -2:])

    combined_hist = ax_hist.hist(
        stellar_type_numbers[:-1],
        bins=stellar_type_numbers - 0.5,
        weights=normalized_combined_accretor_array,
        alpha=1,
        histtype="step",
        color="black",
        label="Normalized total",
        linewidth=4,
    )
    direct_hist = ax_hist.hist(
        stellar_type_numbers[:-1],
        bins=stellar_type_numbers - 0.5,
        weights=normalized_direct_accretor_array,
        histtype="step",
        color="red",
        linestyle="-.",
        label="Direct impact accretion",
        linewidth=3,
    )
    disk_hist = ax_hist.hist(
        stellar_type_numbers[:-1],
        bins=stellar_type_numbers - 0.5,
        weights=normalized_disk_accretor_array,
        histtype="step",
        color="blue",
        linestyle=":",
        label="Disk accretion",
        linewidth=3,
    )

    #
    histogram_string = return_string(
        weight_type=weight_type, bin_type=bin_type, disk_type="a", accretor_type="i"
    )
    ax_hist.set_title(
        "Fractions of probability weighted {} for range each accretor type\n".format(
            weight_type
        )
        + "{}/{}".format(histogram_string, normalisation_string),
        fontsize=16,
    )
    ax_hist.set_ylabel("Fraction", fontsize=16)
    ax_hist.set_yscale("log")
    ax_hist.legend(loc="best", framealpha=0.5)

    #
    ratio_direct_total = np.zeros(normalized_direct_accretor_array.shape)
    np.divide(
        normalized_direct_accretor_array,
        normalized_combined_accretor_array,
        out=ratio_direct_total,
        where=normalized_combined_accretor_array != 0,
    )  # only divide nonzeros else 1
    ratio_disk_total = np.zeros(normalized_disk_accretor_array.shape)
    np.divide(
        normalized_disk_accretor_array,
        normalized_combined_accretor_array,
        out=ratio_disk_total,
        where=normalized_combined_accretor_array != 0,
    )  # only divide nonzeros else 1

    ax_fractions.plot(
        stellar_type_numbers[:-1][ratio_direct_total != np.nan],
        ratio_direct_total[ratio_direct_total != np.nan],
        label="Fraction direct to total",
    )
    ax_fractions.plot(
        stellar_type_numbers[:-1][ratio_disk_total != np.nan],
        ratio_disk_total[ratio_disk_total != np.nan],
        label="Fraction disk to total",
    )
    ax_fractions.legend(loc="best", framealpha=0.5)

    # Plot the piechart
    total_fraction_direct = np.sum(direct_accretor_array) / np.sum(
        combined_accretor_array
    )
    total_fraction_disk = np.sum(disk_accretor_array) / np.sum(combined_accretor_array)

    ratio_string = return_string(
        weight_type=weight_type, bin_type=bin_type, disk_type="i", accretor_type="i"
    )
    ax_fractions.set_ylabel("Fraction of total\nin bin", fontsize=16)
    ax_fractions.set_title(
        "Fractions of total in bins\n" + "{}/{}".format(ratio_string, histogram_string),
        fontsize=16,
    )

    # Pie chart, where the slices will be ordered and plotted counter-clockwise:
    labels = "via disk", "direct"
    sizes = []
    sizes.append(total_fraction_disk)
    sizes.append(total_fraction_direct)

    ax_piechart.pie(sizes, labels=labels, autopct="%1.1f%%", shadow=True, startangle=45)
    ax_piechart.axis(
        "equal"
    )  # Equal aspect ratio ensures that pie is drawn as a circle.

    # Set the labels
    plt.setp(ax_hist.get_xticklabels(), visible=False)

    ax_fractions.set_xticks(stellar_type_numbers[:-1])
    ax_fractions.set_xticklabels(stellar_type_shorts, rotation="vertical")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_general_probability_fractions_donors(
    ensemble_data, sublist, weight_type, bin_type, plot_settings=None
):
    """
    Function to get the fractions for all the accretor, for both the disk and the non disk
    """

    if not plot_settings:
        plot_settings = {}

    # handle the type selections
    if weight_type == "mass":
        longname = "Mass weighted"
    elif weight_type == "time":
        longname = "Time weighted"
    else:
        raise ValueError("Unknown weight type")

    if bin_type == "linear":
        longname = "Linear spacing"
    elif bin_type == "log10":
        longname = "Log10 spacing"
    else:
        raise ValueError("Unknown bin type")

    # Select sub ensemble
    new_sublist = sublist + [weight_type, bin_type]
    sub_ensemble = getFromDict(ensemble_data, new_sublist)

    # Set up array for donor and accretors
    direct_accretor_donor_array = np.array(np.zeros(shape=(16, 16)))
    disk_accretor_donor_array = np.array(np.zeros(shape=(16, 16)))

    #
    direct_donor_array = np.array(np.zeros(16))
    disk_donor_array = np.array(np.zeros(16))

    # Direct impact
    direct_donor_dict = {}
    for accretor_n in sub_ensemble["disk"]["0"]["stellar_type_accretor"]:
        direct_accretor_dict = sub_ensemble["disk"]["0"]["stellar_type_accretor"][
            accretor_n
        ]
        direct_donor_dict = merge_dicts(direct_donor_dict, direct_accretor_dict)

    for donor_n in sorted(direct_donor_dict["stellar_type_donor"]):
        direct_donor_array[int(donor_n)] = get_recursive_sum(
            direct_donor_dict["stellar_type_donor"][donor_n]
        )

    # disk impact
    disk_donor_dict = {}
    for accretor_n in sub_ensemble["disk"]["1"]["stellar_type_accretor"]:
        disk_accretor_dict = sub_ensemble["disk"]["1"]["stellar_type_accretor"][
            accretor_n
        ]
        disk_donor_dict = merge_dicts(disk_donor_dict, disk_accretor_dict)

    for donor_n in sorted(disk_donor_dict["stellar_type_donor"]):
        disk_donor_array[int(donor_n)] = get_recursive_sum(
            disk_donor_dict["stellar_type_donor"][donor_n]
        )

    # Combine values
    combined_donor_array = direct_donor_array + disk_donor_array

    # Calculate normalisation: In this case we will normalize by ALL the masstransfer for this bin and weight
    normalisation_value = get_recursive_sum(sub_ensemble["disk"])
    normalisation_string = return_string(
        weight_type=weight_type, bin_type=bin_type, disk_type="a", accretor_type="a"
    )

    # Normalize values
    normalized_combined_donor_array = combined_donor_array / normalisation_value
    normalized_direct_donor_array = direct_donor_array / normalisation_value
    normalized_disk_donor_array = disk_donor_array / normalisation_value

    #
    stellar_type_shorts = [
        STELLAR_TYPE_DICT_SHORT[key] for key in sorted(STELLAR_TYPE_DICT_SHORT.keys())
    ]
    stellar_type_numbers = np.array(list(range(17)))

    ###########
    # Plotting
    fig = plt.figure(constrained_layout=False, figsize=(20, 20))

    gs = fig.add_gridspec(nrows=8, ncols=8)

    ax_hist = fig.add_subplot(gs[:6, :-2])
    ax_fractions = fig.add_subplot(gs[6:, :-2], sharex=ax_hist)
    ax_piechart = fig.add_subplot(gs[0:2, -2:])

    combined_hist = ax_hist.hist(
        stellar_type_numbers[:-1],
        bins=stellar_type_numbers - 0.5,
        weights=normalized_combined_donor_array,
        alpha=1,
        histtype="step",
        color="black",
        label="Normalized total",
        linewidth=4,
    )
    direct_hist = ax_hist.hist(
        stellar_type_numbers[:-1],
        bins=stellar_type_numbers - 0.5,
        weights=normalized_direct_donor_array,
        histtype="step",
        color="red",
        linestyle="-.",
        label="Normalized direct impact",
        linewidth=3,
    )
    disk_hist = ax_hist.hist(
        stellar_type_numbers[:-1],
        bins=stellar_type_numbers - 0.5,
        weights=normalized_disk_donor_array,
        histtype="step",
        color="blue",
        linestyle=":",
        label="Normalized direct impact",
        linewidth=3,
    )
    histogram_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type="a",
        accretor_type="a",
        donor_type="i",
    )

    ax_hist.set_title(
        "Fractions of probability weighted time for range each donor type\n"
        + "{}/{}".format(histogram_string, normalisation_string),
        fontsize=16,
    )
    ax_hist.set_ylabel("Fraction", fontsize=16)
    ax_hist.set_yscale("log")
    ax_hist.legend(loc="best", framealpha=0.5)

    # plot the ratios
    ratio_direct_total = np.zeros(normalized_direct_donor_array.shape)
    np.divide(
        normalized_direct_donor_array,
        normalized_combined_donor_array,
        out=ratio_direct_total,
        where=normalized_combined_donor_array != 0,
    )  # only divide nonzeros else 1
    ratio_disk_total = np.zeros(normalized_disk_donor_array.shape)
    np.divide(
        normalized_disk_donor_array,
        normalized_combined_donor_array,
        out=ratio_disk_total,
        where=normalized_combined_donor_array != 0,
    )  # only divide nonzeros else 1

    ax_fractions.plot(
        stellar_type_numbers[:-1][ratio_direct_total != np.nan],
        ratio_direct_total[ratio_direct_total != np.nan],
        label="Fraction direct to total",
    )
    ax_fractions.plot(
        stellar_type_numbers[:-1][ratio_disk_total != np.nan],
        ratio_disk_total[ratio_disk_total != np.nan],
        label="Fraction disk to total",
    )

    # Plot the piechart
    total_fraction_direct = np.sum(direct_donor_array) / np.sum(combined_donor_array)
    total_fraction_disk = np.sum(disk_donor_array) / np.sum(combined_donor_array)

    ratio_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type="i",
        accretor_type="a",
        donor_type="i",
    )

    ax_fractions.set_ylabel("Fraction of total\nin bin", fontsize=16)
    ax_fractions.set_title(
        "Fractions of total in bins\n" + "{}/{}".format(ratio_string, histogram_string),
        fontsize=16,
    )
    ax_fractions.legend(loc="best", framealpha=0.5)

    # Pie chart, where the slices will be ordered and plotted counter-clockwise:
    labels = "via disk", "direct"
    sizes = []
    sizes.append(total_fraction_disk)
    sizes.append(total_fraction_direct)

    ax_piechart.pie(sizes, labels=labels, autopct="%1.1f%%", shadow=True, startangle=45)
    ax_piechart.axis(
        "equal"
    )  # Equal aspect ratio ensures that pie is drawn as a circle.

    # Set the labels
    plt.setp(ax_hist.get_xticklabels(), visible=False)

    ax_fractions.set_xticks(stellar_type_numbers[:-1])
    ax_fractions.set_xticklabels(stellar_type_shorts, rotation="vertical")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_general_probability_fractions_accretors_and_donors(
    ensemble_data, sublist, weight_type, bin_type, plot_settings=None
):
    """
    Function to get the fractions for all accretors and donors in a 2d grid,
    for both the disk and the non disk.
    """

    if not plot_settings:
        plot_settings = {}

    # handle the type selections
    if weight_type == "mass":
        longname_weights = "Mass weighted"
    elif weight_type == "time":
        longname_weights = "Time weighted"
    else:
        raise ValueError("Unknown weight type")

    if bin_type == "linear":
        longname_bins = "Linear"
    elif bin_type == "log10":
        longname_bins = "Log10"
    else:
        raise ValueError("Unknown bin type")

    # Select sub ensemble
    new_sublist = sublist + [weight_type, bin_type]
    sub_ensemble = getFromDict(ensemble_data, new_sublist)

    # Set up array for donor and accretors
    direct_accretor_donor_array = np.array(np.zeros(shape=(16, 16)))
    disk_accretor_donor_array = np.array(np.zeros(shape=(16, 16)))

    # Direct impact
    for accretor_n in sub_ensemble["disk"]["0"]["stellar_type_accretor"]:
        direct_accretor_dict = sub_ensemble["disk"]["0"]["stellar_type_accretor"][
            accretor_n
        ]

        for donor_n in direct_accretor_dict["stellar_type_donor"]:
            sum_dict = get_recursive_sum(
                direct_accretor_dict["stellar_type_donor"][donor_n]
            )
            direct_accretor_donor_array[int(accretor_n)][int(donor_n)] += sum_dict

    # disk impact
    for accretor_n in sub_ensemble["disk"]["1"]["stellar_type_accretor"]:
        disk_accretor_dict = sub_ensemble["disk"]["1"]["stellar_type_accretor"][
            accretor_n
        ]

        for donor_n in disk_accretor_dict["stellar_type_donor"]:
            sum_dict = get_recursive_sum(
                disk_accretor_dict["stellar_type_donor"][donor_n]
            )
            disk_accretor_donor_array[int(accretor_n)][int(donor_n)] += sum_dict

    # Combine
    combined_accretor_donor_array = (
        direct_accretor_donor_array + disk_accretor_donor_array
    )

    # Calculate normalisation: In this case we will normalize by ALL the masstransfer for this bin and weight
    normalisation_value = get_recursive_sum(sub_ensemble["disk"])
    normalisation_string = return_string(
        weight_type=weight_type, bin_type=bin_type, disk_type="a", accretor_type="a"
    )

    # Normalize
    normalized_combined_accretor_donor_array = (
        combined_accretor_donor_array / normalisation_value
    )
    normalized_direct_accretor_donor_array = (
        direct_accretor_donor_array / normalisation_value
    )
    normalized_disk_accretor_donor_array = (
        disk_accretor_donor_array / normalisation_value
    )

    stellar_type_shorts = [
        STELLAR_TYPE_DICT_SHORT[key] for key in sorted(STELLAR_TYPE_DICT_SHORT.keys())
    ]
    stellar_type_numbers = np.array(list(range(17)))

    # Rotate dataset and set up meshgrid
    normalized_combined_accretor_donor_array_transp = (
        normalized_combined_accretor_donor_array.T
    )
    normalized_disk_accretor_donor_array_transp = normalized_disk_accretor_donor_array.T
    normalized_direct_accretor_donor_array_transp = (
        normalized_direct_accretor_donor_array.T
    )

    X, Y = np.meshgrid(stellar_type_numbers - 0.5, stellar_type_numbers - 0.5)

    # Plotting
    fig = plt.figure(constrained_layout=False, figsize=(20, 20))

    gs = fig.add_gridspec(nrows=18, ncols=12 + 4)

    size = 6

    ax_hist_combined = fig.add_subplot(gs[:size, :size])
    ax_hist_combined_cb = fig.add_subplot(gs[:size, size])

    ax_hist_direct = fig.add_subplot(gs[size : 2 * size, :size])
    ax_hist_direct_cb = fig.add_subplot(gs[size : 2 * size, size])

    ax_hist_disk = fig.add_subplot(gs[2 * size : 3 * size, :size])
    ax_hist_disk_cb = fig.add_subplot(gs[2 * size : 3 * size, size])

    # find max values and min values:
    min_fraction = np.min(
        [
            normalized_combined_accretor_donor_array_transp[
                normalized_combined_accretor_donor_array_transp != 0
            ].min(),
            normalized_direct_accretor_donor_array_transp[
                normalized_direct_accretor_donor_array_transp != 0
            ].min(),
            normalized_disk_accretor_donor_array_transp[
                normalized_disk_accretor_donor_array_transp != 0
            ].min(),
        ]
    )
    max_fraction = np.max(
        [
            normalized_combined_accretor_donor_array_transp.max(),
            normalized_direct_accretor_donor_array_transp.max(),
            normalized_disk_accretor_donor_array_transp.max(),
        ]
    )

    # Plot the combined hist
    # Set up colormesh
    hist_combined = ax_hist_combined.pcolormesh(
        X,
        Y,
        normalized_combined_accretor_donor_array_transp,
        norm=colors.LogNorm(vmin=min_fraction, vmax=max_fraction),
        shading="auto",
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )
    fig.colorbar(hist_combined, cax=ax_hist_combined_cb, extend="max")

    hist_direct = ax_hist_direct.pcolormesh(
        X,
        Y,
        normalized_direct_accretor_donor_array_transp,
        norm=colors.LogNorm(vmin=min_fraction, vmax=max_fraction),
        shading="auto",
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )
    fig.colorbar(hist_direct, cax=ax_hist_direct_cb, extend="max")

    hist_disk = ax_hist_disk.pcolormesh(
        X,
        Y,
        normalized_disk_accretor_donor_array_transp,
        norm=colors.LogNorm(vmin=min_fraction, vmax=max_fraction),
        shading="auto",
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )
    fig.colorbar(hist_disk, cax=ax_hist_disk_cb, extend="max")

    #
    ax_hist_combined.set_yticks(stellar_type_numbers[:-1])
    ax_hist_combined.set_yticklabels(stellar_type_shorts, rotation=45, fontsize=10)

    ax_hist_direct.set_yticks(stellar_type_numbers[:-1])
    ax_hist_direct.set_yticklabels(stellar_type_shorts, rotation=45, fontsize=10)

    ax_hist_disk.set_yticks(stellar_type_numbers[:-1])
    ax_hist_disk.set_yticklabels(stellar_type_shorts, rotation=45, fontsize=10)

    ax_hist_disk.set_xticks(stellar_type_numbers[:-1])
    ax_hist_disk.set_xticklabels(stellar_type_shorts, rotation=45, fontsize=10)

    #
    combined_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type="a",
        accretor_type="i",
        donor_type="i",
    )
    direct_hist_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type="0",
        accretor_type="i",
        donor_type="i",
    )
    disk_hist_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type="1",
        accretor_type="i",
        donor_type="i",
    )

    #
    ax_hist_combined.set_title(
        "Combined\n" + "{}/{}".format(combined_string, normalisation_string),
        fontsize=16,
    )
    ax_hist_direct.set_title(
        "Direct accretion\n" + "{}/{}".format(direct_hist_string, normalisation_string),
        fontsize=16,
    )
    ax_hist_disk.set_title(
        "Disk accretion\n" + "{}/{}".format(disk_hist_string, normalisation_string),
        fontsize=16,
    )

    ax_hist_combined.set_xticklabels([])
    ax_hist_direct.set_xticklabels([])

    # Set some axes labels spanning the whole grid
    fig.text(0.04, 0.5, "Stellar type donor", va="center", rotation="vertical")
    fig.text(0.2, 0.04, "Stellar type accretor", va="center")

    #############
    # Ratios
    fraction_direct_combined = np.zeros(
        normalized_direct_accretor_donor_array_transp.shape
    )
    np.divide(
        normalized_direct_accretor_donor_array_transp,
        normalized_combined_accretor_donor_array_transp,
        out=fraction_direct_combined,
        where=normalized_combined_accretor_donor_array_transp != 0,
    )  # only divide nonzeros else 1

    fraction_disk_combined = np.zeros(normalized_disk_accretor_donor_array_transp.shape)
    np.divide(
        normalized_disk_accretor_donor_array_transp,
        normalized_combined_accretor_donor_array_transp,
        out=fraction_disk_combined,
        where=normalized_combined_accretor_donor_array_transp != 0,
    )  # only divide nonzeros else 1

    shift = 3
    ax_hist_ratio_direct = fig.add_subplot(
        gs[size : 2 * size, size + shift : 2 * size + shift]
    )
    ax_hist_ratio_direct_cb = fig.add_subplot(gs[size : 2 * size, 2 * size + shift])

    ax_hist_ratio_disk = fig.add_subplot(
        gs[2 * size : 3 * size, size + shift : 2 * size + shift]
    )
    ax_hist_ratio_disk_cb = fig.add_subplot(gs[2 * size : 3 * size, 2 * size + shift])

    cmap = copy.copy(plt.cm.viridis)
    cmap.set_under(color="white")

    # Plot histograms
    hist_ratio_direct = ax_hist_ratio_direct.pcolormesh(
        X,
        Y,
        fraction_direct_combined,
        norm=matplotlib.colors.Normalize(vmin=1e-10, vmax=1),
        cmap=cmap,
        shading="auto",
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )
    fig.colorbar(hist_ratio_direct, cax=ax_hist_ratio_direct_cb, extend="max")

    #
    hist_ratio_disk = ax_hist_ratio_disk.pcolormesh(
        X,
        Y,
        fraction_disk_combined,
        norm=matplotlib.colors.Normalize(vmin=1e-10, vmax=1),
        shading="auto",
        cmap=cmap,
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )
    fig.colorbar(hist_ratio_disk, cax=ax_hist_ratio_disk_cb, extend="max")

    # Set labels etc
    ax_hist_ratio_direct.set_yticks(stellar_type_numbers[:-1])
    ax_hist_ratio_direct.set_yticklabels(stellar_type_shorts, rotation=45, fontsize=10)

    ax_hist_ratio_disk.set_yticks(stellar_type_numbers[:-1])
    ax_hist_ratio_disk.set_yticklabels(stellar_type_shorts, rotation=45, fontsize=10)

    ax_hist_ratio_disk.set_xticks(stellar_type_numbers[:-1])
    ax_hist_ratio_disk.set_xticklabels(stellar_type_shorts, rotation=45, fontsize=10)

    #
    ratio_direct_hist_string = "{}/{}".format(direct_hist_string, combined_string)
    ratio_disk_hist_string = "{}/{}".format(disk_hist_string, combined_string)

    #
    ax_hist_ratio_direct.set_title(
        "Ratio direct and total per bin\n" + "{}".format(ratio_direct_hist_string),
        fontsize=16,
    )
    ax_hist_ratio_disk.set_title(
        "Ratio disk and total per bin\n" + "{}".format(ratio_disk_hist_string),
        fontsize=16,
    )

    ax_hist_ratio_direct.set_xticklabels([])

    #
    plt.suptitle(
        "{} probability distribution of per accretor and donor\n(using {} spacing for racc/sep)".format(
            weight_type, bin_type
        ),
        fontsize=16,
        y=0.9,
    )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    fig.subplots_adjust(hspace=2)
    show_and_save_plot(fig, plot_settings)


def plot_log_massfraction_separation_fraction(
    ensemble_data,
    sublist,
    weight_type,
    bin_type,
    accretor_stellar_type=-1,
    donor_stellar_type=-1,
    plot_settings=None,
):
    """
    Function to plot the log10 radius_accretor vs log10 massratio for a specific accretor type.

    Will show combined, disk and direct

    we can also select accretors or donors
    """

    if not plot_settings:
        plot_settings = {}

    # handle the type selections
    if weight_type == "mass":
        longname_weights = "Mass weighted"
    elif weight_type == "time":
        longname_weights = "Time weighted"
    else:
        raise ValueError("Unknown weight type")

    if bin_type == "linear":
        longname_bins = "Linear"
    elif bin_type == "log10":
        longname_bins = "Log10"
    else:
        raise ValueError("Unknown bin type")

    # Select sub ensemble
    new_sublist = sublist + [weight_type, bin_type]
    sub_ensemble = getFromDict(ensemble_data, new_sublist)

    ###################
    # Select the data

    # Fold all the data onto eachother to get the bins
    # So all of the disk and direct, accretor stellar types and donor stellar types are folded onto eachother to get the bins for the fractions
    # and then we fold the fractions to get the bins for the massratios
    all_merged_donor_types = recursively_merge_all_subdicts(
        sub_ensemble, ["disk", "stellar_type_accretor", "stellar_type_donor"]
    )
    all_merged_fraction_accretor_seperation = merge_all_subdicts(
        all_merged_donor_types["fraction_accretor_separation"]
    )

    # Get keys bins and bincenters
    (
        fraction_accretor_separation_keys,
        bins_accretor_separation,
        bincenters_accretor_separation,
    ) = return_keys_bins_and_bincenters(
        all_merged_donor_types, "fraction_accretor_separation"
    )
    (
        log10_massratio_keys,
        bins_log10_massratio,
        bincenters_log10_massratio,
    ) = return_keys_bins_and_bincenters(
        all_merged_fraction_accretor_seperation, "log10massratio_accretor_donor"
    )

    ###############
    # From here we get the data we actually will use in this routine:
    # It can differ from above if we limit the accretor or donor types
    combined_merged_disk_nodisk = merge_all_subdicts(sub_ensemble["disk"])

    # Select the specific accretor type or merge all of them into 1
    combined_merged_accretor_types = combine_or_filter_stellar_types(
        combined_merged_disk_nodisk, "stellar_type_accretor", accretor_stellar_type
    )
    direct_merged_accretor_types = combine_or_filter_stellar_types(
        sub_ensemble["disk"]["0"], "stellar_type_accretor", accretor_stellar_type
    )
    disk_merged_accretor_types = combine_or_filter_stellar_types(
        sub_ensemble["disk"]["1"], "stellar_type_accretor", accretor_stellar_type
    )

    # Select the specific donor type or merge all of them
    combined_merged_donor_types = combine_or_filter_stellar_types(
        combined_merged_accretor_types, "stellar_type_donor", donor_stellar_type
    )
    direct_merged_donor_types = combine_or_filter_stellar_types(
        direct_merged_accretor_types, "stellar_type_donor", donor_stellar_type
    )
    disk_merged_donor_types = combine_or_filter_stellar_types(
        disk_merged_accretor_types, "stellar_type_donor", donor_stellar_type
    )

    # readout the data
    data_combined = flatten_data_ensemble2d(
        combined_merged_donor_types,
        "fraction_accretor_separation",
        "log10massratio_accretor_donor",
    )
    data_direct = flatten_data_ensemble2d(
        direct_merged_donor_types,
        "fraction_accretor_separation",
        "log10massratio_accretor_donor",
    )
    data_disk = flatten_data_ensemble2d(
        disk_merged_donor_types,
        "fraction_accretor_separation",
        "log10massratio_accretor_donor",
    )

    # pre-bin
    hist_combined_data, _, _ = np.histogram2d(
        data_combined[0],
        data_combined[1],
        bins=[bins_accretor_separation, bins_log10_massratio],
        weights=data_combined[2],
    )
    hist_direct_data, _, _ = np.histogram2d(
        data_direct[0],
        data_direct[1],
        bins=[bins_accretor_separation, bins_log10_massratio],
        weights=data_direct[2],
    )
    hist_disk_data, _, _ = np.histogram2d(
        data_disk[0],
        data_disk[1],
        bins=[bins_accretor_separation, bins_log10_massratio],
        weights=data_disk[2],
    )

    # Calculate normalisation: In this case we will normalize by ALL the masstransfer for this bin and weight
    normalisation_value = get_recursive_sum(combined_merged_donor_types)
    normalisation_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type="a",
        accretor_type="a" if accretor_stellar_type == -1 else accretor_stellar_type,
        donor_type="a" if donor_stellar_type == -1 else donor_stellar_type,
        fraction_racc_separation="a",
        log10_massratio_value="a",
    )

    # Normalise the data
    normalized_hist_combined = hist_combined_data / normalisation_value
    normalized_hist_direct = hist_direct_data / normalisation_value
    normalized_hist_disk = hist_disk_data / normalisation_value

    #
    min_val = np.min(
        [
            np.min(normalized_hist_combined[normalized_hist_combined != 0]),
            np.min(normalized_hist_direct[normalized_hist_direct != 0]),
            np.min(normalized_hist_disk[normalized_hist_disk != 0]),
        ]
    )
    max_val = np.max(
        [
            np.max(normalized_hist_combined),
            np.max(normalized_hist_direct),
            np.max(normalized_hist_disk),
        ]
    )

    X, Y = np.meshgrid(bincenters_accretor_separation, bincenters_log10_massratio)

    # Plotting
    fig = plt.figure(constrained_layout=False, figsize=(20, 20))
    gs = fig.add_gridspec(nrows=18, ncols=12 + 4)

    size = 6  # size of the panes

    # Set up the gridspec subplots
    ax_hist_combined = fig.add_subplot(gs[:size, :size])
    ax_hist_combined_cb = fig.add_subplot(gs[:size, size])

    ax_hist_direct = fig.add_subplot(gs[size : 2 * size, :size])
    ax_hist_direct_cb = fig.add_subplot(gs[size : 2 * size, size])

    ax_hist_disk = fig.add_subplot(gs[2 * size : 3 * size, :size])
    ax_hist_disk_cb = fig.add_subplot(gs[2 * size : 3 * size, size])

    # Plot the combined hist
    # Set up colormesh
    max_diff_log = 4
    hist_combined = ax_hist_combined.pcolormesh(
        X,
        Y,
        normalized_hist_combined.T,
        norm=colors.LogNorm(
            vmin=10 ** (np.log10(max_val) - max_diff_log), vmax=max_val
        ),
        shading="auto",
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )
    fig.colorbar(hist_combined, cax=ax_hist_combined_cb, extend="max")

    hist_direct = ax_hist_direct.pcolormesh(
        X,
        Y,
        normalized_hist_direct.T,
        norm=colors.LogNorm(
            vmin=10 ** (np.log10(max_val) - max_diff_log), vmax=max_val
        ),
        shading="auto",
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )
    fig.colorbar(hist_direct, cax=ax_hist_direct_cb, extend="max")

    hist_disk = ax_hist_disk.pcolormesh(
        X,
        Y,
        normalized_hist_disk.T,
        norm=colors.LogNorm(
            vmin=10 ** (np.log10(max_val) - max_diff_log), vmax=max_val
        ),
        shading="auto",
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )
    fig.colorbar(hist_disk, cax=ax_hist_disk_cb, extend="max")

    if bin_type == "linear":
        ax_hist_combined.set_xlim(0, 1)
        ax_hist_direct.set_xlim(0, 1)
        ax_hist_disk.set_xlim(0, 1)

    # Set some axes labels spanning the whole grid
    fig.text(
        0.04, 0.5, r"Log10 M$_{accretor}$/M$_{donor}$", va="center", rotation="vertical"
    )
    fig.text(
        0.2,
        0.04,
        r"%sR$_{accretor}$/separation" % ("Log10 " if bin_type == "log10" else ""),
        va="center",
    )

    #
    ax_hist_combined.set_xticklabels([])
    ax_hist_direct.set_xticklabels([])

    #
    combined_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type="a",
        accretor_type="a" if accretor_stellar_type == -1 else accretor_stellar_type,
        donor_type="a" if donor_stellar_type == -1 else donor_stellar_type,
        fraction_racc_separation="i",
        log10_massratio_value="i",
    )
    direct_accretion_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type="0",
        accretor_type="a" if accretor_stellar_type == -1 else accretor_stellar_type,
        donor_type="a" if donor_stellar_type == -1 else donor_stellar_type,
        fraction_racc_separation="i",
        log10_massratio_value="i",
    )
    disk_accretion_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type="1",
        accretor_type="a" if accretor_stellar_type == -1 else accretor_stellar_type,
        donor_type="a" if donor_stellar_type == -1 else donor_stellar_type,
        fraction_racc_separation="i",
        log10_massratio_value="i",
    )

    #
    ax_hist_combined.set_title(
        "Combined\n" + "{}/{}".format(combined_string, normalisation_string),
        fontsize=16,
    )
    ax_hist_direct.set_title(
        "Direct accretion\n"
        + "{}/{}".format(direct_accretion_string, normalisation_string),
        fontsize=16,
    )
    ax_hist_disk.set_title(
        "Disk accretion\n"
        + "{}/{}".format(disk_accretion_string, normalisation_string),
        fontsize=16,
    )

    #
    plt.suptitle(
        "{} probability distribution per log10 massratio and {} racc/sep sampling {}{}{}".format(
            weight_type,
            bin_type,
            "\n" if (accretor_stellar_type > -1 or donor_stellar_type > -1) else "",
            "Onto {} acccretors ".format(STELLAR_TYPE_DICT_SHORT[accretor_stellar_type])
            if accretor_stellar_type > -1
            else "",
            "From {} donors".format(STELLAR_TYPE_DICT_SHORT[donor_stellar_type])
            if donor_stellar_type > -1
            else "",
        ),
        fontsize=16,
        y=0.9,
    )

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    fig.subplots_adjust(hspace=2)
    show_and_save_plot(fig, plot_settings)


def plot_massratio_histogram_with_specific_angmom_increase(
    ensemble_data,
    sublist,
    weight_type,
    bin_type,
    accretor_stellar_type,
    fraction_separation_value,
    plot_settings=None,
):
    """
    Function that plots the massratio histogram on its side,
    together with the overview of the specific angmom increases

    The first panel on the left, rotated:
        - is the massratio distribution that we found at this specific value for the fraction of r_acc/separation.
        - the histograms are normalized to the total probability of masstransfer with this specific weight type and bin type, onto this specific accretor

    The second panel in the middle:
        - density plot showing the ratio of specific angular momentum carried away by the mass loss from a disk compared to that just by isotropic mass loss
        - contains shaded areas to indicate parts that are impossible to access
        - contains dots on the edge where the histogram bins of the mass ratio are nonzero to indicate the values and the

    Accepts as arguments
        the standard list of arguments
        accretor type
        fraction racc/separation
    """

    if not plot_settings:
        plot_settings = {}

    # Some settings for the plot
    factor_outer_edge_rl_accretor = 0.9
    include_rcirc = True
    include_iso = True
    include_rmin = True
    plot_log = False
    donor_stellar_type = -1
    r_loss_array = np.linspace(0, 1, 100)

    # Select sub ensemble
    new_sublist = sublist + [weight_type, bin_type]
    sub_ensemble = getFromDict(ensemble_data, new_sublist)

    # Fold all the data onto eachother to get the bins
    # So all of the disk and direct, accretor stellar types and donor stellar types are folded onto eachother to get the bins for the fractions
    # and then we fold the fractions to get the bins for the massratios
    all_merged_donor_types = recursively_merge_all_subdicts(
        sub_ensemble, ["disk", "stellar_type_accretor", "stellar_type_donor"]
    )
    all_merged_fraction_accretor_seperation = merge_all_subdicts(
        all_merged_donor_types["fraction_accretor_separation"]
    )

    # Get keys bins and bincenters
    (
        fraction_accretor_separation_keys,
        bins_accretor_separation,
        bincenters_accretor_separation,
    ) = return_keys_bins_and_bincenters(
        all_merged_donor_types, "fraction_accretor_separation"
    )
    (
        log10_massratio_keys,
        bins_log10_massratio,
        bincenters_log10_massratio,
    ) = return_keys_bins_and_bincenters(
        all_merged_fraction_accretor_seperation, "log10massratio_accretor_donor"
    )

    # Get the disk only data for this specific accretor type
    combined_merged_disk_nodisk = merge_all_subdicts(sub_ensemble["disk"])

    # Select the specific accretor type or merge all of them into 1
    combined_merged_accretor_types = combine_or_filter_stellar_types(
        combined_merged_disk_nodisk, "stellar_type_accretor", accretor_stellar_type
    )
    direct_merged_accretor_types = combine_or_filter_stellar_types(
        sub_ensemble["disk"]["0"], "stellar_type_accretor", accretor_stellar_type
    )
    disk_merged_accretor_types = combine_or_filter_stellar_types(
        sub_ensemble["disk"]["1"], "stellar_type_accretor", accretor_stellar_type
    )

    # Select the specific donor type or merge all of them
    combined_merged_donor_types = combine_or_filter_stellar_types(
        combined_merged_accretor_types, "stellar_type_donor", donor_stellar_type
    )
    direct_merged_donor_types = combine_or_filter_stellar_types(
        direct_merged_accretor_types, "stellar_type_donor", donor_stellar_type
    )
    disk_merged_donor_types = combine_or_filter_stellar_types(
        disk_merged_accretor_types, "stellar_type_donor", donor_stellar_type
    )

    # Get the total probability of all the cases of thsi specific accretor & donor combination:
    # This includes disk and direct
    total_prob_current_selection = get_recursive_sum(combined_merged_accretor_types)
    normalisation_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type="a",
        accretor_type=accretor_stellar_type,
    )

    # Select all the massratio values and probabilities
    log10massratios_at_specific_fraction_accretor_separation = disk_merged_donor_types[
        "fraction_accretor_separation"
    ][str(fraction_separation_value)]
    flattened_log10massratios_at_specific_fraction_accretor_separation = (
        flatten_data_ensemble1d(
            log10massratios_at_specific_fraction_accretor_separation,
            "log10massratio_accretor_donor",
        )
    )

    # Put the bins and the results of the log10 massratio in normal massratio values, so it matches the other plots
    bins_massratio = 10**bins_log10_massratio
    bincenters_massratio = 10**bincenters_log10_massratio

    flattened_massratios_at_specific_fraction_accretor_separation = (
        10 ** flattened_log10massratios_at_specific_fraction_accretor_separation[0]
    )
    flattened_probabilities_at_specific_fraction_accretor_separation = (
        flattened_log10massratios_at_specific_fraction_accretor_separation[1]
    )

    # Record the non-null massratio
    non_null_massratios = flattened_massratios_at_specific_fraction_accretor_separation[
        flattened_probabilities_at_specific_fraction_accretor_separation != 0
    ]

    # Pre bin and normalize
    np_bin_massratios, _ = np.histogram(
        flattened_massratios_at_specific_fraction_accretor_separation,
        bins=sorted(bins_massratio),
        weights=flattened_probabilities_at_specific_fraction_accretor_separation,
    )
    normalized_bin_massratios = np_bin_massratios / total_prob_current_selection

    # Finally decide the massratio array to sample the massratios from in the panels
    q_acc_don_array = 10 ** np.linspace(
        min(bins_log10_massratio), max(bincenters_log10_massratio), 100
    )

    #############
    # Figures

    # Setting up the figure
    fig = plt.figure(constrained_layout=False, figsize=(20, 20))

    gs = fig.add_gridspec(nrows=2, ncols=18)

    massratio_ax = fig.add_subplot(gs[:, :4])
    specific_angmom_ax = fig.add_subplot(gs[:, 4:12], sharey=massratio_ax)
    specific_angmom_ax_cb = fig.add_subplot(gs[:, 17])
    max_angmom_value_ax = fig.add_subplot(gs[:, 12:16], sharey=massratio_ax)

    ##############################
    # massratio histogram plot
    massratio_ax.set_ylabel(r"Mass ratio (M$_{accretor}$/M$_{donor}$)")
    massratio_ax.hist(
        bincenters_massratio,
        bins=sorted(bins_massratio),
        weights=normalized_bin_massratios,
        orientation="horizontal",
    )
    massratio_ax.set_xscale("log")
    massratio_ax.set_xlim(massratio_ax.get_xlim()[::-1])
    massratio_ax.set_yscale("log")
    massratio_ax.set_ylim(
        np.min(sorted(bins_massratio)), np.max(sorted(bins_massratio))
    )
    massratio_ax.set_xlabel("Fraction", fontsize=22)

    histogram_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type=1,
        accretor_type=accretor_stellar_type,
        donor_type="a" if donor_stellar_type == -1 else donor_stellar_type,
        fraction_racc_separation=fraction_separation_value,
        log10_massratio_value="i",
    )
    massratio_ax.set_title(histogram_string + "/\n" + normalisation_string, fontsize=16)

    ##############################
    # Canvas plot
    Mdonor = 1

    #####
    # Create meshgrid and calculate ratio
    X, Y = np.meshgrid(r_loss_array, q_acc_don_array)
    Z = ratio_disk_iso(
        ma=Mdonor * Y,
        md=Mdonor,
        a=1,
        rloss=X,
        inner_disk_radius=fraction_separation_value,
        include_iso=include_iso,
    )

    # Make sure Z is not smaller than 1:
    Z[Z < 1] = 1

    # Colormesh and contourplotratio_disk_iso
    c = specific_angmom_ax.pcolormesh(
        X, Y, Z, cmap="RdBu", norm=colors.LogNorm(vmin=Z.min(), vmax=Z.max())
    )
    CS = specific_angmom_ax.contour(
        X,
        Y,
        Z,
        levels=[0.1, 0.5, 1, 1.5, 2, 5, 25, 50, 100],
        colors="k",
    )

    # Add the rl_radii and some
    rl_radii = factor_outer_edge_rl_accretor * rochelobe_radius_accretor(
        mass_donor=Mdonor, mass_accretor=Mdonor * q_acc_don_array
    )
    specific_angmom_ax.plot(rl_radii, q_acc_don_array)
    specific_angmom_ax.fill_betweenx(
        q_acc_don_array,
        rl_radii,
        x2=r_loss_array.max(),
        interpolate=True,
        hatch="x",
        color="#b8b894",
        zorder=5,
        alpha=0.5,
        label=r"R$_{disk}$ > %s * R$_{RL\ acc}$" % (factor_outer_edge_rl_accretor),
    )

    # Add dots on the line of RL radii at the corresponding massratio values on the left plot
    rl_radii_non_null = factor_outer_edge_rl_accretor * rochelobe_radius_accretor(
        mass_donor=Mdonor, mass_accretor=Mdonor * non_null_massratios
    )

    #
    specific_angmom_ax.scatter(
        rl_radii_non_null, non_null_massratios, marker="3", s=100
    )

    # circ radius
    if include_rcirc:
        r_circs = circularisation_radius(
            mass_donor=Mdonor, mass_accretor=Mdonor * q_acc_don_array
        )
        specific_angmom_ax.plot(r_circs, q_acc_don_array, label=r"R$_{circ}$", zorder=5)

    if include_rmin:
        r_mins = minimum_radius(
            mass_donor=Mdonor, mass_accretor=Mdonor * q_acc_don_array
        )
        specific_angmom_ax.plot(r_mins, q_acc_don_array, label=r"R$_{min}$", zorder=5)

    # If there is an inner disk radius:
    if fraction_separation_value:
        specific_angmom_ax.axvline(
            fraction_separation_value, linestyle="--", label=r"R$_{acc}$", zorder=5
        )

        specific_angmom_ax.fill_betweenx(
            q_acc_don_array,
            x1=r_loss_array.min(),
            x2=fraction_separation_value,
            interpolate=True,
            hatch="//",
            color="#b8b894",
            zorder=5,
            alpha=0.5,
        )

    specific_angmom_ax.set_xlim(np.min(r_loss_array), 1)

    # Set up title etc
    specific_angmom_ax.clabel(CS, inline=1, fontsize=10)
    specific_angmom_ax.set_title(
        r"Ratio h$_{disk}$(r$_{loss})$ vs h$_{isotropic}$", fontsize=22
    )
    specific_angmom_ax.set_xlabel("Radius mass loss [a (separation)]", fontsize=22)

    # Set colorbar
    fig.colorbar(
        c, cax=specific_angmom_ax_cb, label=r"h$_{disk(r loss)}$/h$_{isotropic}$"
    )

    if plot_log:
        specific_angmom_ax.set_xscale("log")

    specific_angmom_ax.set_yscale("log")
    specific_angmom_ax.legend(loc=4)
    specific_angmom_ax.get_yaxis().set_ticks([])
    ########
    # max_angmom_value plot

    # Calculate max values at rl_radii_non_null, non_null_massratios,
    if bin_type == "linear":
        disk_iso_ratios = ratio_disk_iso(
            non_null_massratios,
            1,
            1,
            rl_radii_non_null,
            inner_disk_radius=float(fraction_key),
            include_iso=True,
        )
    else:
        disk_iso_ratios = ratio_disk_iso(
            non_null_massratios,
            1,
            1,
            rl_radii_non_null,
            inner_disk_radius=10 ** float(fraction_key),
            include_iso=True,
        )

    # Plot the values
    max_angmom_value_ax.scatter(disk_iso_ratios, non_null_massratios)
    max_angmom_value_ax.set_xlim(1, max(disk_iso_ratios) * 2)
    max_angmom_value_ax.set_xscale("log")

    max_angmom_value_ax.yaxis.set_label_position("right")
    max_angmom_value_ax.yaxis.tick_right()
    max_angmom_value_ax.set_title("Ratio at outer edge disk", fontsize=18)

    #
    for tick in max_angmom_value_ax.get_xaxis().get_major_ticks():
        tick.set_pad(20)

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def calculate_all_angmom_fractions(
    ensemble_data,
    sublist,
    weight_type,
    bin_type,
    accretor_stellar_type=-1,
    donor_stellar_type=-1,
):
    """
    Function that plots the massratio histogram on its side,
    together with the overview of the specific angmom increases
    """

    # Some settings for the plot
    factor_outer_edge_rl_accretor = 0.9

    # Select sub ensemble
    new_sublist = sublist + [weight_type, bin_type]
    sub_ensemble = getFromDict(ensemble_data, new_sublist)

    # Fold all the data onto eachother to get the bins
    # So all of the disk and direct, accretor stellar types and donor stellar types are folded onto eachother to get the bins for the fractions
    # and then we fold the fractions to get the bins for the massratios
    all_merged_donor_types = recursively_merge_all_subdicts(
        sub_ensemble, ["disk", "stellar_type_accretor", "stellar_type_donor"]
    )
    all_merged_fraction_accretor_seperation = merge_all_subdicts(
        all_merged_donor_types["fraction_accretor_separation"]
    )

    # Get keys bins and bincenters
    (
        fraction_accretor_separation_keys,
        bins_accretor_separation,
        bincenters_accretor_separation,
    ) = return_keys_bins_and_bincenters(
        all_merged_donor_types, "fraction_accretor_separation"
    )
    (
        log10_massratio_keys,
        bins_log10_massratio,
        bincenters_log10_massratio,
    ) = return_keys_bins_and_bincenters(
        all_merged_fraction_accretor_seperation, "log10massratio_accretor_donor"
    )

    # Get the disk only data for this specific accretor type
    combined_merged_disk_nodisk = merge_all_subdicts(sub_ensemble["disk"])

    # Select the specific accretor type or merge all of them into 1
    combined_merged_accretor_types = combine_or_filter_stellar_types(
        combined_merged_disk_nodisk, "stellar_type_accretor", accretor_stellar_type
    )
    direct_merged_accretor_types = combine_or_filter_stellar_types(
        sub_ensemble["disk"]["0"], "stellar_type_accretor", accretor_stellar_type
    )
    disk_merged_accretor_types = combine_or_filter_stellar_types(
        sub_ensemble["disk"]["1"], "stellar_type_accretor", accretor_stellar_type
    )

    # Select the specific donor type or merge all of them
    combined_merged_donor_types = combine_or_filter_stellar_types(
        combined_merged_accretor_types, "stellar_type_donor", donor_stellar_type
    )
    direct_merged_donor_types = combine_or_filter_stellar_types(
        direct_merged_accretor_types, "stellar_type_donor", donor_stellar_type
    )
    disk_merged_donor_types = combine_or_filter_stellar_types(
        disk_merged_accretor_types, "stellar_type_donor", donor_stellar_type
    )

    # Get the total probability of all the cases of this specific accretor & donor combination:
    # This includes disk and direct
    total_prob_current_selection = get_recursive_sum(direct_merged_donor_types)

    fraction_dict = {}

    # Loop over the keys
    for fraction_key in sorted(
        disk_merged_donor_types["fraction_accretor_separation"].keys()
    ):

        # Select all the massratio values and probabilities
        log10massratios_at_specific_fraction_accretor_separation = (
            disk_merged_donor_types["fraction_accretor_separation"][str(fraction_key)]
        )
        flattened_log10massratios_at_specific_fraction_accretor_separation = (
            flatten_data_ensemble1d(
                log10massratios_at_specific_fraction_accretor_separation,
                "log10massratio_accretor_donor",
            )
        )

        # Put the bins and the results of the log10 massratio in normal massratio values, so it matches the other plots
        bins_massratio = 10**bins_log10_massratio
        bincenters_massratio = 10**bincenters_log10_massratio

        flattened_massratios_at_specific_fraction_accretor_separation = (
            10 ** flattened_log10massratios_at_specific_fraction_accretor_separation[0]
        )
        flattened_probabilities_at_specific_fraction_accretor_separation = (
            flattened_log10massratios_at_specific_fraction_accretor_separation[1]
        )

        # Record the non-null massratio
        non_null_massratios = (
            flattened_massratios_at_specific_fraction_accretor_separation[
                flattened_probabilities_at_specific_fraction_accretor_separation != 0
            ]
        )

        # Finally decide the massratio array to sample the massratios from in the panels
        q_acc_don_array = 10 ** np.linspace(
            min(bins_log10_massratio), max(bincenters_log10_massratio), 100
        )

        ##############################
        # Canvas plot
        Mdonor = 1

        # Add dots on the line of RL radii at the corresponding massratio values on the left plot
        rl_radii_non_null = factor_outer_edge_rl_accretor * rochelobe_radius_accretor(
            mass_donor=Mdonor, mass_accretor=Mdonor * non_null_massratios
        )

        # Calculate max values at rl_radii_non_null, non_null_massratios,
        if bin_type == "linear":
            disk_iso_ratios = ratio_disk_iso(
                non_null_massratios,
                1,
                1,
                rl_radii_non_null,
                inner_disk_radius=float(fraction_key),
                include_iso=True,
            )
        else:
            disk_iso_ratios = ratio_disk_iso(
                non_null_massratios,
                1,
                1,
                rl_radii_non_null,
                inner_disk_radius=10 ** float(fraction_key),
                include_iso=True,
            )

        # add to dictionary but dont change the value
        fraction_dict[fraction_key] = {
            "non_null_massratios": non_null_massratios,
            "rl_radii_non_null": rl_radii_non_null,
            "disk_iso_ratios": disk_iso_ratios,
            "probabilities": flattened_probabilities_at_specific_fraction_accretor_separation
            / total_prob_current_selection,
        }

    return fraction_dict


def plot_specific_angmom_ratio_distribution(
    ensemble_data,
    sublist,
    weight_type,
    bin_type,
    accretor_stellar_type=-1,
    donor_stellar_type=-1,
    plot_settings=None,
):
    """
    Function to plot the distribution of ratio disk/iso vs fraction racc/separation.
    """

    if not plot_settings:
        plot_settings = {}

    # Some settings for the plot
    factor_outer_edge_rl_accretor = 0.9

    # Select sub ensemble
    new_sublist = sublist + [weight_type, bin_type]
    sub_ensemble = getFromDict(ensemble_data, new_sublist)

    # Fold all the data onto eachother to get the bins
    # So all of the disk and direct, accretor stellar types and donor stellar types are folded onto eachother to get the bins for the fractions
    # and then we fold the fractions to get the bins for the massratios
    all_merged_donor_types = recursively_merge_all_subdicts(
        sub_ensemble, ["disk", "stellar_type_accretor", "stellar_type_donor"]
    )
    all_merged_fraction_accretor_seperation = merge_all_subdicts(
        all_merged_donor_types["fraction_accretor_separation"]
    )

    # Get keys bins and bincenters
    (
        fraction_accretor_separation_keys,
        bins_accretor_separation,
        bincenters_accretor_separation,
    ) = return_keys_bins_and_bincenters(
        all_merged_donor_types, "fraction_accretor_separation"
    )
    (
        log10_massratio_keys,
        bins_log10_massratio,
        bincenters_log10_massratio,
    ) = return_keys_bins_and_bincenters(
        all_merged_fraction_accretor_seperation, "log10massratio_accretor_donor"
    )

    # Get the dictionary containing all the slices and the probabilities and ratios
    fraction_dictionary = calculate_all_angmom_fractions(
        ensemble_data, sublist, weight_type, bin_type, accretor_stellar_type
    )

    #
    normalisation_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type="a",
        accretor_type="a" if accretor_stellar_type == -1 else accretor_stellar_type,
        donor_type="a" if donor_stellar_type == -1 else donor_stellar_type,
    )
    histogram_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type="a",
        accretor_type="a" if accretor_stellar_type == -1 else accretor_stellar_type,
        donor_type="a" if donor_stellar_type == -1 else donor_stellar_type,
        fraction_racc_separation="i",
        log10_massratio_value="a",
    )

    grand_fraction_keys_array = np.array([])
    grand_ratios_array = np.array([])
    grand_probabilities_array = np.array([])

    #
    for fraction_key in sorted(fraction_dictionary.keys()):
        ratios = fraction_dictionary[fraction_key]["disk_iso_ratios"]
        probabilities = fraction_dictionary[fraction_key]["probabilities"]
        fraction_keys_arr = np.array([float(fraction_key) for _ in range(len(ratios))])

        # Combine them
        grand_fraction_keys_array = np.concatenate(
            (grand_fraction_keys_array, fraction_keys_arr)
        )
        grand_ratios_array = np.concatenate((grand_ratios_array, ratios))
        grand_probabilities_array = np.concatenate(
            (grand_probabilities_array, probabilities)
        )

    bins_ratios = 10 ** np.linspace(0, np.log10(grand_ratios_array.max() * 2), 50)

    # Set up figure for
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=5, ncols=1)

    histo_ax = fig.add_subplot(gs[:3, :])
    cdf_ax = fig.add_subplot(gs[3:, :], sharex=histo_ax)

    #
    histo_ax.hist(
        grand_ratios_array, bins=bins_ratios, weights=grand_probabilities_array
    )
    histo_ax.set_xscale("log")

    #
    histogram_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type=1,
        accretor_type=accretor_stellar_type,
        donor_type="a" if donor_stellar_type == -1 else donor_stellar_type,
        fraction_racc_separation="a",
        log10_massratio_value="a",
    )
    histo_ax.set_title(
        "{}\n".format(histogram_string + "/" + normalisation_string)
        + r"Weighted distribution of Ratio h$_{disk}$/h$_{iso}$"
        + "\nusing weight type {} and bintype {}".format(weight_type, bin_type),
        fontsize=22,
    )
    histo_ax.set_ylabel(
        "{}/\n{}".format(histogram_string, normalisation_string), fontsize=18
    )

    # plot the cumulative histogram
    n, bins, patches = cdf_ax.hist(
        grand_ratios_array,
        bins=bins_ratios,
        density=True,
        histtype="step",
        cumulative=True,
    )

    #
    cdf_ax.set_title("CDF")
    cdf_ax.set_xlabel(r"Ratio h$_{disk}$/h$_{iso}$")

    plot_settings["shift"] = -0.05
    plot_settings["top"] = 0.8

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


def plot_specific_angmom_ratio_fraction_accretor_separation_distribution(
    ensemble_data,
    sublist,
    weight_type,
    bin_type,
    accretor_stellar_type=-1,
    donor_stellar_type=-1,
    plot_settings=None,
):
    """
    Function to plot the distribution of ratio disk/iso vs fraction racc/separation.
    """

    if not plot_settings:
        plot_settings = {}

    # Some settings for the plot
    factor_outer_edge_rl_accretor = 0.9

    # Select sub ensemble
    new_sublist = sublist + [weight_type, bin_type]
    sub_ensemble = getFromDict(ensemble_data, new_sublist)

    # Fold all the data onto eachother to get the bins
    # So all of the disk and direct, accretor stellar types and donor stellar types are folded onto eachother to get the bins for the fractions
    # and then we fold the fractions to get the bins for the massratios
    all_merged_donor_types = recursively_merge_all_subdicts(
        sub_ensemble, ["disk", "stellar_type_accretor", "stellar_type_donor"]
    )
    all_merged_fraction_accretor_seperation = merge_all_subdicts(
        all_merged_donor_types["fraction_accretor_separation"]
    )

    # Get keys bins and bincenters
    (
        fraction_accretor_separation_keys,
        bins_accretor_separation,
        bincenters_accretor_separation,
    ) = return_keys_bins_and_bincenters(
        all_merged_donor_types, "fraction_accretor_separation"
    )
    (
        log10_massratio_keys,
        bins_log10_massratio,
        bincenters_log10_massratio,
    ) = return_keys_bins_and_bincenters(
        all_merged_fraction_accretor_seperation, "log10massratio_accretor_donor"
    )

    # Get the dictionary containing all the slices and the probabilities and ratios
    fraction_dictionary = calculate_all_angmom_fractions(
        ensemble_data, sublist, weight_type, bin_type, accretor_stellar_type
    )

    #
    normalisation_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type="a",
        accretor_type="a" if accretor_stellar_type == -1 else accretor_stellar_type,
        donor_type="a" if donor_stellar_type == -1 else donor_stellar_type,
    )
    histogram_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type="a",
        accretor_type="a" if accretor_stellar_type == -1 else accretor_stellar_type,
        donor_type="a" if donor_stellar_type == -1 else donor_stellar_type,
        fraction_racc_separation="i",
        log10_massratio_value="a",
    )

    grand_fraction_keys_array = np.array([])
    grand_ratios_array = np.array([])
    grand_probabilities_array = np.array([])

    #
    for fraction_key in sorted(fraction_dictionary.keys()):
        ratios = fraction_dictionary[fraction_key]["disk_iso_ratios"]
        probabilities = fraction_dictionary[fraction_key]["probabilities"]
        fraction_keys_arr = np.array([float(fraction_key) for _ in range(len(ratios))])

        # Combine them
        grand_fraction_keys_array = np.concatenate(
            (grand_fraction_keys_array, fraction_keys_arr)
        )
        grand_ratios_array = np.concatenate((grand_ratios_array, ratios))
        grand_probabilities_array = np.concatenate(
            (grand_probabilities_array, probabilities)
        )

    bins_ratios = 10 ** np.linspace(0, np.log10(grand_ratios_array.max() * 2), 50)

    # Set up figure for
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=5)

    histo_ax = fig.add_subplot(gs[:, :4])
    histo_ax_cb = fig.add_subplot(gs[:, 4])

    #
    if bin_type == "log10":
        grand_fraction_keys_array = 10**grand_fraction_keys_array
        bins_accretor_separation = 10**bins_accretor_separation

    cmap = copy.copy(plt.cm.viridis)
    cmap.set_under(color="white")

    #
    hist = histo_ax.hist2d(
        grand_fraction_keys_array,
        grand_ratios_array,
        bins=[bins_accretor_separation, bins_ratios],
        weights=grand_probabilities_array,
        norm=colors.LogNorm(
            vmin=10 ** (grand_probabilities_array.max() - 6),
            vmax=grand_probabilities_array.max(),
        ),
        cmap=cmap,
        antialiased=plot_settings.get("antialiased", False),
        rasterized=plot_settings.get("rasterized", False),
    )
    histo_ax.set_yscale("log")
    if bin_type == "log10":
        histo_ax.set_xscale("log")

    # Set colorbar
    fig.colorbar(
        hist[3],
        cax=histo_ax_cb,
        label=r"h$_{disk(r loss)}$/h$_{isotropic}$",
        extend="min",
    )

    #
    histogram_string = return_string(
        weight_type=weight_type,
        bin_type=bin_type,
        disk_type=1,
        accretor_type=accretor_stellar_type,
        donor_type="a" if donor_stellar_type == -1 else donor_stellar_type,
        fraction_racc_separation="a",
        log10_massratio_value="a",
    )
    histo_ax.set_title(
        "{}\n".format(histogram_string + "/" + normalisation_string)
        + r"Weighted distribution of Ratio h$_{disk}$/h$_{iso}$"
        + "\nusing weight type {} and bintype {}".format(weight_type, bin_type),
        fontsize=22,
    )
    histo_ax.set_ylabel(
        "{}/\n{}".format(histogram_string, normalisation_string), fontsize=18
    )

    # #
    # cdf_ax.set_title("CDF")
    # cdf_ax.set_xlabel(r"Ratio h$_{disk}$/h$_{iso}$")

    plot_settings["shift"] = -0.05
    plot_settings["top"] = 0.8

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)
