import os
import json
import time
import pickle
import sys
import shutil

import matplotlib.pyplot as plt

from binarycpython.utils.grid import Population
from binarycpython.utils.functions import (
    get_help_all,
    get_help,
    create_hdf5,
    output_lines,
)

# from david_phd_functions.binaryc.personal_defaults import personal_defaults

"""
Script to run the CEMP grid for rob
"""


def parse_function(self, output):
    """
    Parse function for binary compact objects
    """

    output_file = os.path.join(self.custom_options["data_dir"], "sampling.txt")

    headers = ["mass", "probability"]
    seperator = "\t"

    if not os.path.isfile(output_file):
        with open(output_file, "w") as output_filehandle:
            output_filehandle.write(seperator.join(headers) + "\n")

    #
    with open(output_file, "a+") as output_filehandle:
        #
        if output:
            for line in output.splitlines():
                if line.startswith("DAVID_COMPARE_TEST"):
                    values = line.split()[1:]

                    output_filehandle.write(seperator.join(values[1:]) + "\n")


## Make population and set value
test_pop = Population()
test_pop.set()

# Set values
test_pop.set(
    # Grid configuration
    max_evolution_time=15000,  # Myr
    combine_ensemble_with_thread_joining=True,
    minimum_timestep=1e-6,
    amt_cores=1,  # number of threads,
    # amt_cores=20, # number of threads,
    verbosity=5,
    metallicity=0.02,  # mass fraction of "metals"
    tmp_dir="template_results/python/local_tmp_dir",
    data_dir="template_results/python/results",
    parse_function=parse_function,
    # # Ensemble
    # ensemble=1,
    # ensemble_defer=1,
    # ensemble_filters_off=0,
    # ensemble_filter_STELLAR_TYPE_COUNTS=1,
    # ensemble_filter_HRD=1,
    # ensemble_filter_EMP=1,
    # ensemble_dt=1e3, # 1Gyr resolution
)

# To log the initial sampling
test_pop.set(
    C_logging_code="""
if (stardata->model.time > 0)
{
    Printf("DAVID_COMPARE_TEST %30.12e " // 1
        "%g %g\\n", // 2-5

        //
        stardata->model.time, // 1
        stardata->common.zero_age.mass[0], // 2
        stardata->model.probability // 3
    );
    /* Kill the simulation to save time */
    stardata->model.max_evolution_time = stardata->model.time - stardata->model.dtm;
};
"""
)

#
resolutions = {"M_1": 100}

#
test_pop.add_grid_variable(
    name="lnm1",
    longname="Primary mass",
    valuerange=[0.1, 80],
    resolution="{}".format(resolutions["M_1"]),
    spacingfunc="const(math.log(0.1), math.log(80.0), {})".format(resolutions["M_1"]),
    precode="M_1=math.exp(lnm1)",
    probdist="Kroupa2001(M_1)*M_1",
    dphasevol="dlnm1",
    parameter_name="M_1",
    gridtype="centred",
    condition="",  # Impose a condition on this grid variable. Mostly for a check for yourself
)

# create local tmp_dir
test_pop.set(tmp_dir=os.path.join(test_pop.custom_options["data_dir"], "local_tmp_dir"))

# Delete if it doesnt exist
if os.path.isdir(test_pop.grid_options["tmp_dir"]):
    shutil.rmtree(test_pop.grid_options["tmp_dir"])
os.makedirs(test_pop.grid_options["tmp_dir"], exist_ok=True)

# Remove the old result file
os.remove(os.path.join(test_pop.custom_options["data_dir"], "sampling.txt"))

# Export settings:
test_pop.export_all_info(use_datadir=True)

# Evolve grid
test_pop.evolve()

# # Get ensemble output and write it to file
# ensemble_output = test_pop.grid_ensemble_results

# with open(os.path.join(test_pop.custom_options['data_dir'], 'ensemble_output.json'), 'w') as f:
#     json.dump(ensemble_output, f, indent=4)
