#!/usr/bin/env perl
use strict; # highly recommended
use 5.16.0; # highly recommended
use distribution_functions;
use binary_grid2; # required
use binary_grid::C; # required (uses C backend )
use POSIX qw/strftime log10/;
use File::Basename;
use JSON::PP;
use JSON::Parse qw/parse_json json_file_to_perl/;
use Sort::Key qw/nsort/;
use Sort::Naturally qw/ncmp/;
use Sys::Hostname;
use Term::ANSIColor;
use rob_misc qw/dumpfile force_numeric_elements MAX mkdirhier ncpus slurp/;
use Data::Dumper;

#
my $nthreads = 2;

my $norm_output='none';

# make a new stellar population
my $population = binary_grid2->new(
    # options can be given now ...
    metallicity => 0.02, # mass fraction of "metals"
    max_evolution_time => 15000, # Myr
    nthreads=>$nthreads, # number of threads,
    check_args=>0, # check that args exist
    vb=>1, # turn on verbose logging (can be 0,1,2...)
    return_array_refs=>1, # quicker data parsing mode
    log_args=>1,
    sort_args=>1,
    save_args=>1,
    log_args_dir=>'template_results/perl/tmp_dir',
    minimum_timestep=>1e-6,
);

# Ensemble
$population->set(
    # turn on (C)EMP filters
    ensemble=>1,
    ensemble_defer=>1,
    ensemble_filters_off=>0,
    ensemble_filter_EMP=>1,
    ensemble_filter_STELLAR_TYPE_COUNTS=>1,
    ensemble_filter_HRD=>1,
    ensemble_dt=>1e3, # 1Gyr resolution
);

# check structures are up to date
exit if($population->check_structure_sizes());

############################################################
#
# Now we set up the grid of stars.
#
# duplicity is 0 for single stars, 1 for binary stars
# and 2 for a mixed population sampled at equal times
my $duplicity = 1;
$population->{_grid_options}{multiplicity} =
    $duplicity == 0 ? 1 : 2;

# make a population of binary stars
# my $nres = 10;
my $resolution = {
    m1 => 10,
    q => 5,
    P => 5,
};
my $mmin = 0.1;
my $mmax = 5.0;

# Mass
$population->add_grid_variable(
    'name'       =>'lnm1',
    'longname'   =>'Primary mass',
    'range'      =>[log($mmin),log($mmax)],
    'resolution' => $resolution->{m1}, # just a counter for the grid
    'spacingfunc'=>"const(log($mmin),log($mmax),$resolution->{m1})",
    'precode'    =>'$m1=exp($lnm1);',
    'probdist'   =>'Kroupa2001($m1)*$m1',
    'method'     =>'grid',
    'gridtype'   =>'centred',
);

# q=M1/M2 distribution flat in q between 0.1/M1 and 1.0
$population->add_grid_variable
    (
    'name'       =>'q',
    'longname'   =>'Mass ratio',
    'range'      =>['0.1/$m1',1.0],
    'resolution'=>$resolution->{q},
    'spacingfunc'=>"const(0.1/\$m1,1.0,$resolution->{q})",
    'precode'     =>'$m2=$q*$m1;',
    'probdist'   =>'
flatsections($q,[{min=>0.1/$m1,max=>1.0,height=>1.0}])',
    'method'     => 'grid',
    'gridtype'   => 'centred',
);

# orbital period Duquennoy and Mayor 1991 distribution
my $Prange = [-2.0,12.0];
$population->add_grid_variable
     (
      'name'       =>'logper',
      'longname'   =>'log(Orbital_Period)',
      'range'      =>$Prange,
      'resolution' =>$resolution->{P},
      'spacingfunc'=>"const($Prange->[0],$Prange->[1],$resolution->{P})",
      'precode'=>'
$per = 10.0 ** $logper;
my $eccentricity = 0.0;
$sep = calc_sep_from_period($m1,$m2,$per) if(defined $m1 && defined $m2);
',
      'probdist'=>"gaussian(\$logper,4.8,2.3,$Prange->[0],$Prange->[1])",
      'method'     =>'grid',
      'gridtype'   => 'centred',
);

############################################################
# link population to custom data parser function
$population->set(
    parse_bse_function_pointer => \&main::parse_data
    );

my %init = $population->initial_abundance_hash('Karakas2002',0.02);
my %isotope_hash = $population->isotope_hash();
my @isotope_list = $population->isotope_list();
my %nuclear_mass_hash = $population->nuclear_mass_hash();
my @nuclear_mass_list = $population->nuclear_mass_list();
my @sources = $population->source_list();
my @ensemble = $population->ensemble_list();

# you can use Data::Dumper to see the contents
# of the above lists and hashes
if(0){
    print Data::Dumper->Dump([
        #\%init,
        #\%isotope_hash,
        #\@isotope_list,
        #\%nuclear_mass_hash,
        \@nuclear_mass_list,
        #\@sources,
        #\@ensemble
                         ]);
}

# evolution the stellar population (this takes some time)
$population->evolve();

# output ND JSON data
output_json($population);

# done : exit
exit;

############################################################
# subroutines
############################################################

sub parse_data
{
    my ($population,$h) = @_;
    while(1)
    {
        my $linearray = $population->tbse_line();
        my $header = shift @$linearray;
        last if ($header eq 'fin');
        if($header eq 'ENSEMBLE_JSON')
        {
            $population->add_ensemble($h,$linearray);
            print "Added ensemble : pop=$population, h=$h\n";
        }
    }
}

sub output_json
{
    my ($population) = @_;
    my $h = $population->results;
    print "Output json pop=$population, results=$h\n";

    # output formatted JSON to ensemble.json, and normalise by $idenom
    my $denom = denominator($population);
    if($denom == 0.0)
    {
        #  should be considered a bug
        $denom = 1.0;
        print "WARNING: denominator is 0 : did you run any stars?\n";
    }
    rob_misc::force_numeric_elements($h->{'ensemble'},
                                     {
                                         vb => 0,
                                         modulator => 1.0 / $denom,
                                         numeric_keyformat => '%g'
                                     }
        );

    # add info hash
    # $h->{'ensemble'}->{'info'} = $population->info_hash($population);

    # open(FP,'>','/tmp/test.hash');
    # print FP Data::Dumper::Dumper($h);
    # close FP;

    # make text version of the data, sort numerically, pretty print etc.
    my $json = JSON::PP->new->utf8->pretty->allow_nonref->allow_blessed->allow_unknown->sort_by(
        sub
        {
            Sort::Naturally::ncmp($JSON::PP::a,
                                  $JSON::PP::b)
        })->encode($h);

    # and output
    my $file = "template_results/perl/results/ensemble.json";
    open(FP,'>',$file)||die("opening $file failed");
    print FP $json,"\n";
    close FP;
    print "See $file\n";
}


sub denominator
{
    # global divider for the yields, rates, etc.
    my ($population) = @_;
    return
        $norm_output eq 'mass_into_stars' ? $population->results->{'mass_into_stars'} :
        $norm_output eq 'none' ? 1.0 :
        0.0;
}

sub denominator_string
{
    # string to explain how the denominator is calculated
    return
        $norm_output eq 'mass_into_stars' ? 'mass into stars' :
        $norm_output eq 'none' ? 'none' :
        'unknown';
}
